#!/bin/bash

#####################################################################################################
# Script name  : test-diff                                                                          #
# Usage        : ./test-diff.sh [=watch] [<base-branch>]                                            #
# Description  : This script runs 'test' or 'testWatch' task either only for current specs changes, #
#                or both with specs changes introduced by the current branch into the base branch.  #
# Author/Email : Dmytro Denysenko / dmytro.denysenko@symphony-solutions.eu                          #
#####################################################################################################

TEST_FILE_PATH="src/test.ts"
TEST_TASK=${TEST_TASK:-test}                    # dynamic test package.json task
TESTWATCH_TASK=${TESTWATCH_TASK:-testWatch}     # dynamic testWatch package.json task

printf "\e[37mStarting dedicated testing process...\e[0m\n"
if [ ! -f "$TEST_FILE_PATH" ]; then
  printf "\n\e[91mOops!\e[0m Could not resolve path '$TEST_FILE_PATH'. \e[91mTerminating...\e[0m\n"
else
  CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)

  BASE_BRANCH=$CURRENT_BRANCH
  BRANCH_SPECIFIED=false
  WATCH=false
  PARAMS_ERRORS=()

  for PARAM in $@; do
    if [[ $PARAM =~ ^= ]]; then
      if [ $PARAM == "=watch" ]; then
        if ! $WATCH; then
          WATCH=true
        else
          PARAMS_ERRORS+=("It's enough to provide '$PARAM' option only once")
        fi
      else
        PARAMS_ERRORS+=("Unknown option ignored: $PARAM")
      fi
    else
      if ! $BRANCH_SPECIFIED; then
        BASE_BRANCH=$PARAM
        BRANCH_SPECIFIED=true
      else
        PARAMS_ERRORS+=("Redundant argument ignored: $PARAM")
      fi
    fi
  done

  if [ ${#PARAMS_ERRORS[@]} -gt 0 ]; then
    printf "\e[91mOops!\e[0m Some parameters were wrong:\n"
    printf "\e[37m"
    printf "  %s\n" "${PARAMS_ERRORS[@]}"
    printf "\e[0m"
  fi

  CHANGED_SPEC_FILES=""
  if $BRANCH_SPECIFIED; then
    printf "\nFetching repository to update base branch \e[1m\e[33m$BASE_BRANCH\e[0m before running tests:\n"
    git fetch --quiet origin $BASE_BRANCH:$BASE_BRANCH > /dev/null 2>&1

    if [ $? == 0 ]; then
      printf "  Base branch \e[1m\e[33m$BASE_BRANCH\e[0m is up to date.\n"
    else
      if [ $BASE_BRANCH == $CURRENT_BRANCH ]; then
        printf "  Skipped fetching into base branch \e[1m\e[33m$BASE_BRANCH\e[0m since it is the \e[33mcurrently checked out\e[0m branch.\n"
      else
        printf "  \e[91mOops!\e[0m Could not update base branch \e[1m\e[33m$BASE_BRANCH\e[0m!\n"
      fi
    fi

    BASE_REVISION=$(git rev-parse --quiet --verify $BASE_BRANCH)
    CURRENT_REVISION=$(git rev-parse --quiet --verify $CURRENT_BRANCH)
    printf "\nRunning quick testing based on current spec changes and spec diff between branches:\n"
    printf "  \e[37mbase:\e[0m \e[1m\e[33m$BASE_BRANCH\e[0m    \e[37m${BASE_REVISION:-\e[91mnot found}\e[0m\n"
    printf "  \e[37mcurrent:\e[0m \e[1m\e[33m$CURRENT_BRANCH\e[0m    \e[37m${CURRENT_REVISION:-\e[91mnot found}\e[0m\n"

    if [ -z "$BASE_REVISION" ]; then
      printf "\n\e[91mOops!\e[0m Could not find local branch.\n"
    else
      CHANGED_SPEC_FILES+=$(git diff --name-only $BASE_BRANCH...$CURRENT_BRANCH | grep -E "\.spec\.ts$")
      if [ ! -z "$CHANGED_SPEC_FILES" ]; then
        printf "\nCommitted spec files changes relative to base branch \e[1m\e[33m$BASE_BRANCH\e[0m:\n"
        printf "\e[94m$CHANGED_SPEC_FILES\e[0m" | sed "s/^/  /"
      else
        printf "\nCommitted files does not contain any spec changes relative to base branch \e[1m\e[33m$BASE_BRANCH\e[0m.\n"
      fi
    fi
  else
    printf "\nRunning quick testing based on current spec files changes.\n"
  fi

  STAGED_SPEC_FILES=$(git diff --name-only --staged | grep -E "\.spec\.ts$")
  if [ ! -z "$STAGED_SPEC_FILES" ]; then
    printf "Current staged spec files changes:\n"
    printf "\e[94m$STAGED_SPEC_FILES\e[0m" | sed "s/^/  /"
    CHANGED_SPEC_FILES+=$'\n'$STAGED_SPEC_FILES
  fi

  UNSTAGED_SPEC_FILES=$(git diff --name-only | grep -E "\.spec\.ts$")
  if [ ! -z "$UNSTAGED_SPEC_FILES" ]; then
    printf "Current unstaged spec files changes:\n"
    printf "\e[94m$UNSTAGED_SPEC_FILES\e[0m" | sed "s/^/  /"
    CHANGED_SPEC_FILES+=$'\n'$UNSTAGED_SPEC_FILES
  fi

  if [ -z "$CHANGED_SPEC_FILES" ]; then
    printf "\nNo changes in spec files were found. \e[91mTerminating...\e[0m\n"
  else
    SPECS_PATHS_REGEXP=$(echo "$CHANGED_SPEC_FILES" |
      sed -E "/^$/d" |                                 # exclude empty strings
      sort -u |                                        # filter unique entries
      sed -E "s/src\///g" |                            # remove "src/"
      sed -E "s/\//\\\\\//g" |                         # escape / occurrences to \/
      sed -E "s/\./\\\./g" |                           # escape . occurrences to \.
      tr "\n" "|" |                                    # join parts with pipe "|"
      sed -E "s/(.*)\|/(\1)/" |                        # wrap with parentheses () and trim last pipe "|"
      sed -E "s/(.*)/\\/\1\\//"                        # wrap with // for RegExp final form
    )

    printf "\nCombined spec search RegExp:\n"
    printf "  \e[33m$SPECS_PATHS_REGEXP\e[0m\n"

    printf "\nUpdating search RegExp entry in \e[94m$TEST_FILE_PATH\e[0m.\n"
    sed -i "" -E "s~require\.context\(.*\);~require.context('./', true, $(printf %q "$SPECS_PATHS_REGEXP"));~" $TEST_FILE_PATH

    if $WATCH; then
      printf "\nStarting \e[33mnpm run start $TESTWATCH_TASK\e[0m task...\n"
      printf "  As soon as it is terminated, the changes in \e[94m$TEST_FILE_PATH\e[0m should be reverted, but you'd better double-check it \e[33m¯\_(ツ)_/¯\e[0m.\n"
      npm run start $TESTWATCH_TASK
    else
      printf "\nStarting \e[33mnpm run start $TEST_TASK\e[0m task...\n"
      npm run start $TEST_TASK
      if [ $? == 0 ]; then
        printf "\n\e[32mYeah!\e[0m Unit tests passed successfully.\n"
      else
        printf "\n\e[91mOops!\e[0m Unit tests seem to have failed.\n"
      fi
    fi

    printf "\nRolling back changes in \e[94m$TEST_FILE_PATH\e[0m.\n"
    git checkout HEAD $TEST_FILE_PATH
  fi
fi
