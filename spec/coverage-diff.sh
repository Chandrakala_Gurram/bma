#!/bin/bash

#################################################################################################
# Script name  : coverage-diff                                                                  #
# Usage        : ./coverage-diff.sh [<base-branch>] [<diff-branch>]                             #
# Description  : This script is designed to generate quick coverage report for the changes,     #
#                which are introduced by "diff" branch into "base" branch (develop by default). #
#                If "diff" branch is not specified, the current branch and changes are used.    #
#                Based on git-diff, the coverage-report is executed only for targeted tests.    #
#                The visual representation is refined, the changed lines are highlighted.       #
#                IMPORTANT: Script is unable to detect uncovered if/else path. Check manually!  #
# Author/Email : Dmytro Denysenko / dmytro.denysenko@symphony-solutions.eu                      #
#################################################################################################

TEST_FILE_PATH="src/test.ts"
COVERAGE_REPORT_DIR="coverage"
COVERAGE_REPORT_INDEX_PATH="$COVERAGE_REPORT_DIR/index.html"
SED_REGEX_LENGTH_THRESHOLD=1000   # bash has limit for regex, thus we'll need to split a long one into parts down there
COVERAGE_TASK=${COVERAGE_TASK:-coverage-report}    # dynamic coverage-report package.json task

printf "\e[37mStarting coverage calculation process...\e[0m\n"
if [ ! -f "$TEST_FILE_PATH" ]; then
  printf "\n\e[91mOops!\e[0m Could not resolve path '$TEST_FILE_PATH'. \e[91mTerminating...\e[0m\n"
else
  STASHED_FILES=false
  SWITCHED_BRANCH=false
  CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)

  BASE_BRANCH=${1:-develop}
  DIFF_BRANCH=${2:-$CURRENT_BRANCH}

  printf "\nFetching repository to update local branches:\n"
  git fetch --quiet origin $BASE_BRANCH:$BASE_BRANCH > /dev/null 2>&1
  if [ $? == 0 ]; then
    printf "  Base branch \e[1m\e[33m$BASE_BRANCH\e[0m is up to date.\n"
  else
    printf "  \e[91mOops!\e[0m Could not update base branch \e[1m\e[33m$BASE_BRANCH\e[0m!\n"
  fi

  if [ ! -z "$2" ]; then
    git fetch --quiet origin $DIFF_BRANCH:$DIFF_BRANCH > /dev/null 2>&1
    if [ $? == 0 ]; then
      printf "  Diff branch \e[1m\e[33m$DIFF_BRANCH\e[0m is up to date.\n"
    else
      if [ $DIFF_BRANCH == $CURRENT_BRANCH ]; then
        printf "  Skipped fetching into diff branch \e[1m\e[33m$DIFF_BRANCH\e[0m since it is the \e[33mcurrently checked out\e[0m branch.\n"
      else
        printf "  \e[91mOops!\e[0m Could not update diff branch \e[1m\e[33m$DIFF_BRANCH\e[0m!\n"
      fi
    fi

    STAGED_FILES=$(git diff --name-only --staged)
    UNSTAGED_FILES=$(git diff --name-only)
    if [ ! -z "$STAGED_FILES" ] || [ ! -z "$UNSTAGED_FILES" ]; then
      printf "\nStashing current uncommitted files before switching to diff branch \e[1m\e[33m$DIFF_BRANCH\e[0m.\n"
      printf "\e[37m  "
      git stash --include-untracked
      printf "\e[0m"
      STASHED_FILES=true
    fi

    printf "\nSwitching to diff branch \e[1m\e[33m$DIFF_BRANCH\e[0m:\n"
    git checkout $DIFF_BRANCH > /dev/null 2>&1
    if [ $? == 0 ]; then
      printf "  Checked out diff branch \e[1m\e[33m$DIFF_BRANCH\e[0m successfully.\n"
      SWITCHED_BRANCH=true
    else
      printf "  \e[91mOops!\e[0m Could not check out diff branch \e[1m\e[33m$DIFF_BRANCH\e[0m!\n"
    fi
  fi

  BASE_REVISION=$(git rev-parse --quiet --verify $BASE_BRANCH)
  DIFF_REVISION=$(git rev-parse --quiet --verify $DIFF_BRANCH)

  printf "\nCreating quick coverage report based on diff between branches:\n"
  printf "  \e[37mbase:\e[0m \e[1m\e[33m$BASE_BRANCH\e[0m\e[33m$([[ -z "$1" ]] && echo " (default)")\e[0m    \e[37m${BASE_REVISION:-\e[91mnot found}\e[0m\n"
  printf "  \e[37mdiff:\e[0m \e[1m\e[33m$DIFF_BRANCH\e[0m\e[33m$([[ -z "$2" ]] && echo " (current)")\e[0m    \e[37m${DIFF_REVISION:-\e[91mnot found}\e[0m\n"

  if [ -z "$BASE_REVISION" ] || [ -z "$DIFF_REVISION" ]; then
    printf "\n\e[91mOops!\e[0m Could not find local branch. \e[91mTerminating...\e[0m\n"
  else
    CHANGED_FILES=$(git diff --name-only $BASE_BRANCH...$DIFF_BRANCH)
    if [ ! -z "$CHANGED_FILES" ]; then
      printf "\nFiles changes introduced within branch \e[1m\e[33m$DIFF_BRANCH\e[0m:\n"
      printf "\e[94m$CHANGED_FILES\e[0m" | sed "s/^/  /"
    fi

    if [ -z "$2" ]; then
      STAGED_FILES=$(git diff --name-only --staged)
      if [ ! -z "$STAGED_FILES" ]; then
        printf "Current staged changes:\n"
        printf "\e[94m$STAGED_FILES\e[0m" | sed "s/^/  /"
        CHANGED_FILES+=$'\n'$STAGED_FILES
      fi

      UNSTAGED_FILES=$(git diff --name-only)
      if [ ! -z "$UNSTAGED_FILES" ]; then
        printf "Current unstaged changes:\n"
        printf "\e[94m$UNSTAGED_FILES\e[0m" | sed "s/^/  /"
        CHANGED_FILES+=$'\n'$UNSTAGED_FILES
      fi
    fi

    CHANGED_TS_FILES=$(echo "$CHANGED_FILES" |
      grep -E "^src\/.*\.ts$" |                               # filter changed app TS files and specs
      grep -vE "^src\/[^/]*\.ts$" |                           # exclude TS files from root folder
      grep -vE "\.(module|lang|model|mock|constants?)\.ts$"   # exclude unnecessary TS files
    )

    UNIQUE_FOLDERS=$(echo "$CHANGED_TS_FILES" |
      sed -E "s/^(src\/.*)\/.*$/\1/" |                        # trim file name
      sed -E "/^$/d" |                                        # exclude empty strings
      sort -u                                                 # filter unique entries
    )

    if [ -z "$UNIQUE_FOLDERS" ]; then
      printf "\nNo changes in files affecting coverage were found. \e[91mTerminating...\e[0m\n"
    else
      printf "\nAffected coverage-related file paths:\n"
      printf "\e[94m$UNIQUE_FOLDERS\e[0m" | sed "s/^/  /"

      FOLDERS_PATTERNS=()
      for FOLDER in $UNIQUE_FOLDERS; do
        LENGTH=${#FOLDERS_PATTERNS[@]}
        INDEX=$(($LENGTH > 0 ? $LENGTH - 1 : 0))
        ESCAPED_FOLDER=$(echo $FOLDER |
          sed -E "s/\//\\\\\//g" |                                    # escape / occurrences to \/
          sed -E "s/\./\\\./g"                                        # escape . occurrences to \.
        )
        if [ $((${#FOLDERS_PATTERNS[INDEX]} + ${#ESCAPED_FOLDER} + 1)) -lt $SED_REGEX_LENGTH_THRESHOLD ]; then
          FOLDERS_PATTERNS[INDEX]+="$ESCAPED_FOLDER|"                 # add substring to current item with pipe "|" delimiter
        else
          FOLDERS_PATTERNS[INDEX]="(${FOLDERS_PATTERNS[INDEX]%|})"    # remove last pipe "|" and wrap with parentheses "()"
          FOLDERS_PATTERNS+=("$ESCAPED_FOLDER")                       # add substring to the next item of the array
        fi
      done
      FOLDERS_PATTERNS[INDEX]="(${FOLDERS_PATTERNS[INDEX]%|})"        # finalize the last iteration

      SPEC_FILES_LIST=()
      for FOLDER in $UNIQUE_FOLDERS; do
        if [ -d $FOLDER ]; then
          IFS_backup=$IFS
          IFS=$'\n'
          SPEC_FILES_LIST+=($(find "$FOLDER" -maxdepth 1 -name "*.spec.ts"))
          IFS=$IFS_backup
        fi
      done

      printf "\nCombined coverage-related spec files:\n"
      printf "\e[94m"
      printf "%s\n" "${SPEC_FILES_LIST[@]}" | sed "s/^/  /"
      printf "\e[0m"

      SPECS_PATHS_REGEXP=$(echo "${SPEC_FILES_LIST[@]}" |
        sed -E "s/^src\///" |                                # remove "src/"
        sed -E "s/ src\//|/g" |                              # and join substrings with pipe "|"
        sed -E "s/\//\\\\\//g" |                             # escape / occurrences to \/
        sed -E "s/\./\\\./g" |                               # escape . occurrences to \.
        tr " " "|" |                                         # join parts with pipe "|"
        sed -E "s/(.*)/(\1)/" |                              # wrap with parentheses ()
        sed -E "s/(.*)/\\/\1\\//"                            # wrap with // for RegExp final form
      )

      printf "\nCombined spec search RegExp:\n"
      printf "  \e[33m$SPECS_PATHS_REGEXP\e[0m\n"

      printf "\nUpdating search RegExp entry in \e[94m$TEST_FILE_PATH\e[0m.\n"
      sed -i "" -E "s~require\.context\(.*\);~require.context('./', true, $(printf %q "$SPECS_PATHS_REGEXP"));~" $TEST_FILE_PATH

      printf "\nStarting \e[33mnpm run start $COVERAGE_TASK\e[0m task...\n"
      npm run start $COVERAGE_TASK
      COVERAGE_RESULT=$?

      if [ $COVERAGE_RESULT -eq 0 ]; then
        printf "\n\e[32mYeah!\e[0m Unit tests passed successfully.\n"

        if [ ! -f "$COVERAGE_REPORT_INDEX_PATH" ]; then
          printf "\n\e[91mOops!\e[0m Coverage report has not been generated or index file is missing in \e[94m$COVERAGE_REPORT_INDEX_PATH\e[0m. \e[91mTerminating...\e[0m\n"
        else
          printf "\nAnalyzing and modifying generated coverage report in \e[94m$COVERAGE_REPORT_INDEX_PATH\e[0m...\n"
          sed -i "" -E "/#show-hidden:not\(:checked\)/! s/(<style.*>)/\1 #show-hidden:not(:checked) ~ table tbody tr:not(.shown) { display:none; } table tbody tr:not(.shown) { opacity: 0.5; } /" $COVERAGE_REPORT_INDEX_PATH
          sed -i "" -E "/<input id=\"show-hidden\"/! s/(<table class=\"coverage-summary\">)/<input id=\"show-hidden\" type=\"checkbox\"\/><label for=\"show-hidden\"> Show Hidden<\/label> \1/" $COVERAGE_REPORT_INDEX_PATH

          for PATTERN in ${FOLDERS_PATTERNS[@]}; do
            sed -i "" -E "/<tr>/N; s/<tr>([[:space:]]+<td class=\"file.*\" data-value=\"$PATTERN\")/<tr class=\"shown\">\1/g" $COVERAGE_REPORT_INDEX_PATH
          done

          CHANGED_SCRIPTS=$(grep -vE "\.spec\.ts$" <<< "$CHANGED_TS_FILES")
          UNCOVERED_LINES_DETECTED=false
          UNCOVERED_PATHS_FILES=()

          for SCRIPT_PATH in $CHANGED_SCRIPTS; do
            COVERAGE_FILE_PATH="$COVERAGE_REPORT_DIR/$SCRIPT_PATH.html"
            if [ -f "$COVERAGE_FILE_PATH" ]; then
              IFS_backup=$IFS
              IFS=$'\n'
              COVERAGE_MARKERS=( $(grep -oE "<span class=\"cline-any.*<\/span>" $COVERAGE_FILE_PATH) )
              IFS=$IFS_backup

              CHANGED_BLOCKS=$(git diff -U0 $BASE_BRANCH -- $SCRIPT_PATH |
                grep -E "^@@" |
                sed -E "s/@@.*\+([0-9]+,?[0-9]*).*$/\1/" |
                grep -vE "^[0-9]+,0$" |
                sed -E "s/^([0-9]+)$/\1,1/"
              )
              CHANGED_LINES=()
              UNCOVERED_LINES=()

              for BLOCK in $CHANGED_BLOCKS; do
                START=$(cut -d, -f1 <<< $BLOCK)
                LENGTH=$(cut -d, -f2 <<< $BLOCK)
                for (( LINE=$START; LINE < $START + $LENGTH; LINE++ )); do
                  CHANGED_LINES+=($LINE)
                  if [ $(grep -o "cline-no" <<< "${COVERAGE_MARKERS[$LINE - 1]}") ]; then
                    UNCOVERED_LINES+=($LINE)
                  fi
                done
              done

              PATHS_COVERAGE=$(sed -nE "/<span class=\"strong\">.*<\/span>/N; s/<span class=\"strong\">(.*)<\/span>.*<span class=\"quiet\">Branches<\/span>/\1/p" $COVERAGE_FILE_PATH)
              if [ $PATHS_COVERAGE != "100%" ]; then
                UNCOVERED_PATHS_FILES+=($SCRIPT_PATH)
              fi

              if [ ${#UNCOVERED_LINES[@]} -gt 0 ]; then
                if ! $UNCOVERED_LINES_DETECTED; then
                  printf "\n\e[91mOops!\e[0m Uncovered lines detected:"
                fi

                LINES_LIST=${UNCOVERED_LINES[@]}
                printf "\n  \e[94m$SCRIPT_PATH:\e[0m ${LINES_LIST}"
                UNCOVERED_LINES_DETECTED=true
              fi

              SELECTORS=$(echo ${CHANGED_LINES[@]} | tr " " "," | sed -E "s/([0-9]+)/span.cline-any:nth-child(\1)::before/g")
              HIGHLIGHT_STYLE="{ content: \"\"; display: block; position: absolute; z-index: -1; width: 70%; height: 1.3em; background: linear-gradient(90deg, skyblue, transparent); }"
              sed -i "" -E "s/<\/style>/$SELECTORS $HIGHLIGHT_STYLE&/" $COVERAGE_FILE_PATH
            fi
          done

          if ! $UNCOVERED_LINES_DETECTED && [ ${#UNCOVERED_PATHS_FILES[@]} == 0 ]; then
            printf "\n\e[32mYeah!\e[0m No uncovered lines or if/else paths were found in changed files. But don't relax yet - I'm not the pipeline \e[33m¯\_(ツ)_/¯\e[0m\n"
          elif [ ${#UNCOVERED_PATHS_FILES[@]} != 0 ]; then
            if ! $UNCOVERED_LINES_DETECTED; then
              printf "\n\e[33mHmmmm...\e[0m No uncovered lines were found, but following files contain \e[33muncovered if/else paths\e[0m. Check manually your changes:\n"
            else
              printf "\n\e[33mBonus!\e[0m Following files contain \e[33muncovered if/else paths\e[0m. Check manually your changes:\n"
            fi
            printf "\e[94m"
            printf "%s\n" "${UNCOVERED_PATHS_FILES[@]}" | sed "s/^/  /"
            printf "\e[0m"
          fi

          printf "\nOpening coverage report in your default browser.\n"
          open $COVERAGE_REPORT_INDEX_PATH
        fi
      else
        printf "\n\e[91mOops!\e[0m Unit tests seem to have failed. \e[91mTerminating...\e[0m\n"
      fi

      printf "\nRolling back changes in \e[94m$TEST_FILE_PATH\e[0m.\n"
      git checkout HEAD $TEST_FILE_PATH
    fi
  fi

  if $SWITCHED_BRANCH; then
    printf "\nSwitching back to initial branch \e[1m\e[33m$CURRENT_BRANCH\e[0m:\n"
    git checkout $CURRENT_BRANCH > /dev/null 2>&1

    if [ $? == 0 ]; then
      printf "  Checked out the initial branch \e[1m\e[33m$CURRENT_BRANCH\e[0m successfully.\n"
    else
      printf "  \e[91mOops!\e[0m Could not check out initial branch \e[1m\e[33m$CURRENT_BRANCH\e[0m!\n"
    fi
  fi

  if $STASHED_FILES; then
    printf "\nRestoring the workspace to its initial state:\n"
    git stash pop --quiet
    git status --short
  fi
fi
