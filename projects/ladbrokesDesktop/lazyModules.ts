export default [
  'src/platforms/ladbrokesDesktop/lazy-modules/competitionsSportTab/competitionsSportTab.module',
  'src/platforms/ladbrokesDesktop/lazy-modules/forecastTricast/forecastTricast.module',
  'src/app/lazy-modules/locale/translation.module',
  'src/app/lazy-modules/devLog/dev-log.module',
  'src/app/stats/stats.module',
  'src/platforms/ladbrokesDesktop/ukTote/uk-tote.module',
  'src/platforms/ladbrokesDesktop/lazy-modules/eventVideoStream/event-video-stream.module',
  'src/app/lazy-modules/superButton/super-button.module',
  'src/platforms/ladbrokesDesktop/lazy-modules/sortByOptions/sort-by-options.module',
  'src/platforms/ladbrokesDesktop/lazy-modules/racingFeatured/racing-featured.module',
  'src/platforms/ladbrokesDesktop/lazy-modules/bybHistory/byb-history.module',
  'src/app/lazy-modules/quiz/quiz-dialog.module.ts',
  'src/platforms/ladbrokesDesktop/lazy-modules/betRadarProvider/bet-radar.module',
  'src/platforms/ladbrokesDesktop/lazy-modules/racingFeatured/racing-featured.module',
  'src/app/lazy-modules/performanceMark/performanceMark.module',
  'src/platforms/ladbrokesDesktop/shared/components/marketSelector/market-selector.module'
];
