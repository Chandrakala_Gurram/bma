export default {
  production: [
    {
      'type': 'bundle',
      'name': 'styles',
      'baseline': '1.65kb',
      'maximumError': '1%',
      'minimumError': '1%'
    },
    {
      'type': 'bundle',
      'name': 'polyfills',
      'baseline': '186kb',
      'maximumError': '1%',
      'minimumError': '1%'
    },
    {
      'type': 'bundle',
      'name': 'vendor',
      'baseline': '1.75Mb',
      'maximumError': '1%',
      'minimumError': '1%'
    },
    {
      'type': 'bundle',
      'name': 'app',
      'baseline': '3.34Mb',
      'maximumError': '1%',
      'minimumError': '1%'
    }
  ]
};
