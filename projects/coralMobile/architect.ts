export default {
  'extract-i18n': {
    'builder': '@angular-devkit/build-angular:extract-i18n',
    'options': {
      'browserTarget': 'coralMobile:build'
    }
  },
  'test': {
    'builder': '@angular-devkit/build-angular:karma',
    'options': {
      'main': 'src/test.ts',
      'polyfills': 'src/polyfills.ts',
      'tsConfig': 'src/tsconfig.spec.json',
      'karmaConfig': 'src/karma.conf.js',
      'styles': [
        'src/assets/testStyles/global.variables.less',
        'src/assets/styles/main.less'
      ],
      'stylePreprocessorOptions': {
        'includePaths': [
          'src/assets/testStyles'
        ]
      },
      'fileReplacements': [
        {
          'replace': 'src/environments/oxygenEnvConfig.ts',
          'with': 'src/environments/environment.mock.ts'
        }
      ],
      'scripts': [],
      'assets': [
        'src/favicon.ico',
        'src/assets'
      ]
    }
  },
  'lint': {
    'builder': '@angular-devkit/build-angular:tslint',
    'options': {
      'tsConfig': [
        'src/tsconfig.coralMobile.json',
        'src/tsconfig.spec.json'
      ],
      'exclude': [
        '**/node_modules/**'
      ]
    }
  }
};
