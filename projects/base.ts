export default {
  'root': '',
  'sourceRoot': 'src',
  'projectType': 'application',
  'schematics': {
    '@schematics/angular:component': {
      'styleext': 'less'
    }
  }
};
