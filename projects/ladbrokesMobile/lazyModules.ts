export default [
  'src/platforms/ladbrokesMobile/lazy-modules/competitionsSportTab/competitionsSportTab.module',
  'src/platforms/ladbrokesMobile/lazy-modules/forecastTricast/forecastTricast.module',
  'src/app/lazy-modules/locale/translation.module',
  'src/app/lazy-modules/devLog/dev-log.module',
  'src/platforms/ladbrokesMobile/sb/sport/sport.module',
  'src/app/sb/event/event.module',
  'src/app/lazy-modules/couponsPage/coupons-page.module.ts',
  'src/app/olympics/olympics.module.ts',
  'src/app/stats/stats.module',
  'src/app/lazy-modules/rpg/rpg.module',
  'src/platforms/ladbrokesMobile/ukTote/uk-tote.module',
  'src/platforms/ladbrokesMobile/lazy-modules/eventVideoStream/event-video-stream.module',
  'src/app/lazy-modules/superButton/super-button.module',
  'src/platforms/ladbrokesMobile/lazy-modules/sortByOptions/sort-by-options.module',
  'src/app/lazy-modules/quiz/quiz-dialog.module.ts',
  'src/app/lazy-modules/gamingOverlay/gaming-overlay.module',
  'src/platforms/ladbrokesMobile/lazy-modules/racingFeatured/racing-featured.module',
  'src/platforms/ladbrokesMobile/lazy-modules/bybHistory/byb-history.module',
  'src/platforms/ladbrokesMobile/lazy-modules/betRadarProvider/bet-radar.module',
  'src/platforms/ladbrokesMobile/lazy-modules/racingFeatured/racing-featured.module',
  'src/app/lazy-modules/performanceMark/performanceMark.module'
];
