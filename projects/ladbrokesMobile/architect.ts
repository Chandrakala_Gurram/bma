export default {
  'extract-i18n': {
    'builder': '@angular-devkit/build-angular:extract-i18n',
    'options': {
      'browserTarget': 'ladbrokesMobile:build'
    }
  },
  'test': {
    'builder': '@angular-devkit/build-angular:karma',
    'options': {
      'main': 'src/test.ts',
      'polyfills': 'src/polyfills.ts',
      'tsConfig': 'src/tsconfig.spec.json',
      'karmaConfig': 'src/karma.conf.js',
      'assets': [
        'src/platforms/ladbrokesMobile/assets/favicon.ico',
        'src/assets'
      ],
      'styles': [
        'src/platforms/ladbrokesMobile/assets/styles/global.variables.less',
        'src/assets/styles/main.less',
        'src/platforms/ladbrokesMobile/assets/styles/main.less'
      ],
      'stylePreprocessorOptions': {
        'includePaths': [
          'src/platforms/ladbrokesMobile/assets/styles'
        ]
      },
      'scripts': []
    }
  },
  'lint': {
    'builder': '@angular-devkit/build-angular:tslint',
    'options': {
      'tsConfig': [
        'src/tsconfig.ladbrokesMobile.json',
        'src/tsconfig.spec.json'
      ],
      'exclude': [
        '**/node_modules/**'
      ]
    }
  }
};
