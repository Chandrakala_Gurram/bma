export default [
  'src/platforms/coralDesktop/lazy-modules/competitionsSportTab/competitionsSportTab.module',
  'src/app/lazy-modules/forecastTricast/forecastTricast.module',
  'src/app/lazy-modules/locale/translation.module',
  'src/app/lazy-modules/devLog/dev-log.module',
  'src/app/lazy-modules/eventVideoStream/event-video-stream.module',
  'src/app/stats/stats.module',
  'src/app/ukTote/uk-tote.module',
  'src/app/lazy-modules/superButton/super-button.module',
  'src/app/lazy-modules/sortByOptions/sort-by-options.module',
  'src/app/lazy-modules/bybHistory/byb-history.module',
  'src/app/lazy-modules/quiz/quiz-dialog.module.ts',
  'src/app/lazy-modules/betRadarProvider/bet-radar.module',
  'src/platforms/coralDesktop/lazy-modules/racingFeatured/racing-featured.module',
  'src/app/lazy-modules/performanceMark/performanceMark.module',
  'src/platforms/coralDesktop/shared/components/marketSelector/market-selector.module'
];
