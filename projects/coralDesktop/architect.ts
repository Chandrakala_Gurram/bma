export default {
  'extract-i18n': {
    'builder': '@angular-devkit/build-angular:extract-i18n',
    'options': {
      'browserTarget': 'coralDesktop:build'
    }
  },
  'test': {
    'builder': '@angular-devkit/build-angular:karma',
    'options': {
      'main': 'src/test.ts',
      'polyfills': 'src/polyfills.ts',
      'tsConfig': 'src/tsconfig.spec.json',
      'karmaConfig': 'src/karma.conf.js',
      'styles': [
        'src/assets/testStyles/global.variables.less',
        'src/assets/styles/main.less',
        'src/platforms/coralDesktop/assets/styles/index.less'
      ],
      'stylePreprocessorOptions': {
        'includePaths': [
          'src/assets/testStyles'
        ]
      },
      'scripts': [],
      'assets': [
        'src/favicon.ico',
        'src/assets'
      ]
    }
  },
  'lint': {
    'builder': '@angular-devkit/build-angular:tslint',
    'options': {
      'tsConfig': [
        'src/tsconfig.coralDesktop.json',
        'src/tsconfig.spec.json'
      ],
      'exclude': [
        '**/node_modules/**',
        '**/app/**'
      ]
    }
  }
};
