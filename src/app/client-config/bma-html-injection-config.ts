import { ClientConfig } from '@vanilla/core/core';

@ClientConfig('bmaHtmlInjectionConfig')
export class HtmlInjectionConfig {
  headTags: any;
}
