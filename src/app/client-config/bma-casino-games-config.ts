import { ClientConfig } from '@vanilla/core/core';

@ClientConfig('bmaCasinoGamesConfig')
export class CasinoGamesClientConfig {
  miniGamesEnabled: boolean;
  miniGamesHost: string;
  miniGamesTemplate: string;
  recentlyPlayedGamesEnabled: boolean;
  recentlyPlayedGamesUrl: string;
  userHostAddress: string;
  seeAllEnabled: boolean;
  seeAllUrl: string;
}
