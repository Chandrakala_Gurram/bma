import { ClientConfig } from '@vanilla/core/core';

@ClientConfig('bmaUserInterfaceConfig')
export class UserInterfaceClientConfig {
  rtsLink: string;
  accountUpgradeLink: {
    imc: string;
    omc: string;
  };
  preloadPortalModule: boolean;
  portalModuleLoadDelay: number;
}
