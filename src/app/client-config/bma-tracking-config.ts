import { ClientConfig } from '@vanilla/core/core';

@ClientConfig('bmaTrackingConfig')
export class TrackingConfig {
  gtmConfigs: any;
  newRelicConfigs: any;
}
