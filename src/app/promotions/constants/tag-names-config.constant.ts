export const TAG_NAMES_CONFIG = [
  {
    marketFlag: 'MKTFLAG_FI',
    eventFlag: 'EVFLAG_FIN',
    flagName: 'FI',
    promoName: 'fallersInsurance',
    iconId: '#icon-promotion-offers'
  },
  {
    marketFlag: 'MKTFLAG_BBAL',
    eventFlag: 'EVFLAG_BBL',
    flagName: 'BBAL',
    promoName: 'beatenByLength',
    iconId: '#icon-promotion-offers'
  },
  {
    marketFlag: 'MKTFLAG_EPR',
    eventFlag: 'EVFLAG_EPR',
    flagName: 'EPR',
    promoName: 'extraPlace',
    iconId: '#extra-place-icon'
  },
  {
    marketFlag: 'MKTFLAG_PB',
    eventFlag: 'EVFLAG_PB',
    flagName: 'PB',
    promoName: 'priceBoost',
    iconId: '#price-boost'
  },
  {
    marketFlag: 'MKTFLAG_MB',
    eventFlag: 'EVFLAG_MB',
    flagName: 'MB',
    promoName: 'moneyBack',
    iconId: '#money-back'
  },
  {
    marketFlag: 'MKTFLAG_DYW',
    eventFlag: 'EVFLAG_DYW',
    flagName: 'DYW',
    promoName: 'doubleWinnings',
    iconId: '#icon-promotion-offers'
  },
  {
    marketFlag: 'YOUR_CALL',
    eventFlag: 'YOUR_CALL',
    flagName: 'YOUR_CALL',
    promoName: 'yourCall',
    iconId: '#yourcall-icon'
  }
];
