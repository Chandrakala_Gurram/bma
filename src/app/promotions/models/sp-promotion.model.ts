import { IPromotion } from '@core/services/cms/models/promotion/promotion.model';
import { SafeHtml } from '@angular/platform-browser';

export interface ISpPromotion extends IPromotion {
  marketLevelFlag: string;
  eventLevelFlag: string;
  useDirectFileUrl: boolean;
  directFileUrl: string;
  overlayBetNowUrl: string;
  flagName: string;
  safeDescription?: SafeHtml;
  safeHtmlMarkup?: SafeHtml;
  iconId: string;
  openBetId?: string;
}
