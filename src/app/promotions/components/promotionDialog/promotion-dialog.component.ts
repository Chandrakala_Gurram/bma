import { Component, ViewChild } from '@angular/core';
import * as _ from 'underscore';

import { AbstractDialog } from '@shared/components/oxygenDialogs/abstract-dialog';
import { DeviceService } from '@core/services/device/device.service';
import { ISpPromotion } from '../../models/sp-promotion.model';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

@Component({
  selector: 'promotion-overlay-dialog',
  templateUrl: './promotion-dialog.component.html'
})
export class PromotionDialogComponent extends AbstractDialog {
  @ViewChild('dialog') dialog;

  loaded: boolean;
  promo: ISpPromotion;
  flag: string;
  cmsContent: { htmlMarkup: string};
  constructor(
    device: DeviceService,
    protected windowRef: WindowRefService
  ) {
    super(device, windowRef);
  }

  open(): void {
    this.windowRef.document.body.classList.add('promotion-modal-open');
    super.open();
    this.getPromotion();
  }

  /**
   * Get promotion data and process it
   */
  getPromotion(): void {
    const { flag, getSpPromotionData } = this.params;

    getSpPromotionData(false)
      .subscribe((data: ISpPromotion[]) => {
        this.promo = _.find(data, p => p.marketLevelFlag === flag || p.eventLevelFlag === flag);

        if (!this.promo) { return; }
        const baseUrlRegEx = new RegExp(`href="${location.origin}`, 'g');
        const htmlMarkup  = this.promo.promotionText ? this.promo.promotionText.replace(baseUrlRegEx, `data-routerlink="`) : '';

        this.cmsContent = { htmlMarkup };
        this.loaded = true;
      });
  }

  /**
   * Open overlay
   */
  openOverlay(): void {
    this.windowRef.document.body.classList.remove('promotion-modal-open');
    this.params.openPromotionOverlay();
  }

  /**
   * Close dialog
   */
  closeThisDialog(): void {
    this.windowRef.document.body.classList.remove('promotion-modal-open');
    super.closeDialog();
  }

}
