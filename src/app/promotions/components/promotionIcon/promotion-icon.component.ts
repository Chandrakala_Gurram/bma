import { from as observableFrom, of as observableOf, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import * as _ from 'underscore';
import { PromotionsService } from '@promotions/services/promotions/promotions.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { CoreToolsService } from '@core/services/coreTools/core-tools.service.ts';
import { ISpPromotion } from '@promotions/models/sp-promotion.model';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { CommandService } from '@core/services/communication/command/command.service';

@Component({
  selector: 'promotion-icon',
  templateUrl: './promotion-icon.component.html',
  styleUrls: ['./promotion-icon.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PromotionIconComponent implements OnInit, OnDestroy {

  @Input() type: string;
  @Input() eventId: number;
  @Input() display: string;
  @Input() mode: string = 'md';
  @Input() typeId: number;
  @Input() cashoutAvailable: boolean = false;
  @Input() buildYourBetAvailable: boolean;
  @Input() isGpAvailable: boolean;
  @Input() bogIconStyle: boolean;

  @Output() readonly setPromotionIconStatus: EventEmitter<boolean> = new EventEmitter<boolean>();

  iconsCount: number = 0;
  isPromoSignpostingEnabled: boolean = false;
  available: boolean = false;
  promoIcons: ISpPromotion[] = [];
  isFlagChecked: boolean = false;
  isBYBChecked: boolean = false;
  isBogCmsEnabled: boolean = false;
  BOG_MARKET_FLAG: string = 'MKTFLAG_BOG';
  private componentName;

  constructor(
    private cmsService: CmsService,
    private promotionsService: PromotionsService,
    private pubSubService: PubSubService,
    private coreToolsService: CoreToolsService,
    private commandService: CommandService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.componentName = `PromotionIconComponent${this.coreToolsService.uuid()}`;
  }

  checkBybIcon(): Observable<boolean> {
    // Check if Byb icon available for Featured module
    if (this.buildYourBetAvailable !== undefined) {
      if (this.buildYourBetAvailable) {
        this.updateIconsCount(1);
      }
      return observableOf(this.buildYourBetAvailable);
    }

    if (!this.typeId) {
      return observableOf(this.buildYourBetAvailable);
    }
    // Check if Byb icon available for other places

    if (this.eventId) {
      return observableFrom(this.commandService
        .executeAsync(this.commandService.API.DS_WHEN_YC_READY, ['isEnabledYCIcon', true], {}))
        .pipe(mergeMap(() => observableFrom(this.commandService
          .executeAsync(this.commandService.API.DS_IS_AVAILABLE_FOR_EVENTS, [this.eventId], {}))));
    }

    return observableFrom(this.commandService
      .executeAsync(this.commandService.API.DS_WHEN_YC_READY, ['isEnabledYCIcon', true], {}))
      .pipe(mergeMap(() => observableFrom(this.commandService
        .executeAsync(this.commandService.API.DS_IS_AVAILABLE_FOR_COMPETITION, [this.typeId], {}))));
  }

  /**
   * OnInit controller function
   */
  ngOnInit(): void {
    if (this.cashoutAvailable) {
      this.updateIconsCount(1);
    }

    this.cmsService.getToggleStatus('PromoSignposting')
      .subscribe((toggleStatus: boolean) => {
        this.isPromoSignpostingEnabled = toggleStatus;

        if (!this.isPromoSignpostingEnabled) {
          return;
        }

        this.pubSubService.subscribe(
            this.componentName, [this.pubSubService.API.SESSION_LOGOUT, this.pubSubService.API.SESSION_LOGIN], () => this.processFlags()
        );
        this.processFlags();
      }, () => {}, () => {
        this.isFlagChecked = true;
        this.changeDetectorRef.markForCheck();
      });

    this.checkBybIcon().subscribe((isByBAvailable: boolean) => {
      this.buildYourBetAvailable = isByBAvailable;
      if (!this.buildYourBetAvailable) {
        return;
      }
      this.updateIconsCount(1);
    }, () => {}, () => {
      this.isBYBChecked = true;
      this.changeDetectorRef.markForCheck();
    });

    if (this.isGpAvailable) {
      this.cmsService.isBogFromCms().subscribe((isBog: boolean) => {
        this.updateIconsCount(1);
        this.isBogCmsEnabled = isBog;
      });
    }
  }

  /**
   * Destructor
   */
  ngOnDestroy(): void {
    if (this.isPromoSignpostingEnabled) {
      this.pubSubService.unsubscribe(this.componentName);
    }
  }

  /**
   * On click promotion icon action
   * @param event
   * @param {Object} icon
   */
  iconAction(event: MouseEvent, icon: ISpPromotion): void {
    event.stopPropagation();
    event.preventDefault();
    this.promotionsService.openPromotionDialog( icon[`${this.type}LevelFlag`]);
  }

  trackByPromoIcon(index: number, item: ISpPromotion): string {
    return `${index}${item.flagName}${item.iconId}${item.promoKey}`;
  }

  /**
   * On click bog icon action
   */
  bogAction(event: MouseEvent): void {
    event.stopPropagation();
    event.preventDefault();
    this.promotionsService.openPromotionDialog(this.BOG_MARKET_FLAG);
    this.promotionsService.trackBogDialog(this.BOG_MARKET_FLAG, 'ok');
  }

  /**
   * Find promotion icons
   */
  private processFlags(): void {
    this.available = false;
    const previousPromoIconsCount = this.promoIcons.length;
    const promoIcons = this.promoIcons = [];

    const flags = this.parseFlags(this.display);

    this.promotionsService.getSpPromotionData()
      .subscribe((data: ISpPromotion[]) => {
        _.each(flags, (flagName: string) => {
          _.each(data, (promo: ISpPromotion) => {
            if (promo[`${this.type}LevelFlag`] === flagName) {
              promoIcons.push(promo);
              this.available = true;
              this.setPromotionIconStatus.emit(this.available);
            }
          });
        });
        this.promoIcons = promoIcons.filter(icon => icon.iconId);

        this.updateIconsCount(this.promoIcons.length - previousPromoIconsCount);
      });
  }

  /**
   * Parsing drilldownTagNames
   * @param {string} drilldownTagNames
   * @returns {Array} array with available flags
   */
  private parseFlags(drilldownTagNames: string): string[] {
    return drilldownTagNames ? _.without(drilldownTagNames.split(','), '') : [];
  }

  private updateIconsCount(iconsCount) {
    this.iconsCount += iconsCount;
    this.mode = (this.mode === 'sm' && this.iconsCount > 1) ? 'mini' : this.mode;

    this.changeDetectorRef.markForCheck();
  }

}
