import { of as observableOf, throwError } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';
import { PromotionIconComponent } from '@promotions/components/promotionIcon/promotion-icon.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('PromoLabelsComponent', () => {
  let cmsService;
  let promotionsService;
  let pubSubService;
  let coreToolsService;
  let component: PromotionIconComponent;
  let commandService;
  let changeDetectorRef;

  beforeEach(() => {
    cmsService = {
      getToggleStatus: jasmine.createSpy('getToggleStatus').and.returnValue(observableOf(true)),
      isBogFromCms: jasmine.createSpy('isBogFromCms').and.returnValue(observableOf(true))
    };
    promotionsService = {
      getSpPromotionData: jasmine.createSpy().and.returnValue(observableOf([
        {
          eventLevelFlag: 'EVFLAG_EPR',
          iconId: '#EPR_ICON'
        },
        {
          eventLevelFlag: 'EVFLAG_FIN'
        }
      ])),
      openPromotionDialog: jasmine.createSpy(),
      trackBogDialog: jasmine.createSpy()
    };
    pubSubService = {
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: pubSubApi
    };
    coreToolsService = {
      uuid: () => '1234'
    };
    commandService = {
      executeAsync: jasmine.createSpy('executeAsync').and.returnValue(Promise.resolve({})),
      API: {
        DS_WHEN_YC_READY: 'DS_WHEN_YC_READY',
        DS_IS_AVAILABLE_FOR_COMPETITION: 'DS_IS_AVAILABLE_FOR_COMPETITION',
        DS_IS_AVAILABLE_FOR_EVENTS: 'DS_IS_AVAILABLE_FOR_EVENTS'
      }
    };
    changeDetectorRef = {
      markForCheck: jasmine.createSpy('markForCheck')
    };

    component = new PromotionIconComponent(
      cmsService,
      promotionsService,
      pubSubService,
      coreToolsService,
      commandService,
      changeDetectorRef
    );
    component.display = 'EVFLAG_EPR,,,,EVFLAG_FIN';
    component.type = 'event';
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  describe('processFlags', () => {
    it('should initialize promoIcons property with icons' +
      ' with promoIcons which have iconId', fakeAsync(() => {
      spyOn(component.setPromotionIconStatus, 'emit');

      component['processFlags']();
      tick();

      expect(component.promoIcons.length).toEqual(1);
      expect(component.setPromotionIconStatus.emit).toHaveBeenCalledWith(true);
    }));
  });

  describe('#ngOnInit', () => {
    beforeEach(() => {
      component['updateIconsCount'] = jasmine.createSpy();
      component['checkBybIcon'] = jasmine.createSpy('checkBybIcon').and.returnValue(observableOf(true));
    });

    it('should update icons count if cahsout is available', () => {
      component.cashoutAvailable = true;

      component.ngOnInit();

      expect(component['updateIconsCount']).toHaveBeenCalledWith(1);
    });

    it('should return if toggle status of false', () => {
      component['processFlags'] = jasmine.createSpy('processFlags');
      component['cmsService'].getToggleStatus = jasmine.createSpy('getToggleStatus').and.returnValue(observableOf(false));

      component.ngOnInit();

      expect(component['processFlags']).not.toHaveBeenCalledTimes(2);
      expect(pubSubService.subscribe)
        .not
        .toHaveBeenCalledWith('PromotionIconComponent1234', ['SESSION_LOGOUT', 'SESSION_LOGIN'], jasmine.any(Function));
    });

    it('should call subscribe for SESSION_LOGOUT and SESSION_LOGIN', () => {
      let callback;

      pubSubService.subscribe.and.callFake((name: string, apis: string[], cb) => callback = cb);
      component['processFlags'] = jasmine.createSpy('processFlags');

      component.ngOnInit();

      callback && callback();

      expect(component['processFlags']).toHaveBeenCalledTimes(2);
      expect(pubSubService.subscribe)
        .toHaveBeenCalledWith('PromotionIconComponent1234', ['SESSION_LOGOUT', 'SESSION_LOGIN'], jasmine.any(Function));
    });

    it('should set isFlagChecked true', () => {
      const componentName = `PromotionIconComponent${ coreToolsService.uuid() }`;

      component.ngOnInit();

      expect(component.isFlagChecked).toBe(true);
      expect(pubSubService.subscribe)
        .toHaveBeenCalledWith(componentName, [pubSubService.API.SESSION_LOGOUT, pubSubService.API.SESSION_LOGIN], jasmine.any(Function));
      expect(changeDetectorRef.markForCheck).toHaveBeenCalledTimes(2);
    });

    it('should not set isFlagChecked true in case of error', () => {
      component['cmsService'].getToggleStatus = jasmine.createSpy('getToggleStatus').and.returnValue(throwError('error'));
      component.ngOnInit();

      expect(component.isFlagChecked).toBe(false);
      expect(changeDetectorRef.markForCheck).toHaveBeenCalledTimes(1);
    });

    it('should set buildYourBetAvailable', () => {
      component.ngOnInit();

      expect(component.buildYourBetAvailable).toBe(true);
      expect(component['updateIconsCount']).toHaveBeenCalled();
      expect(component.isBYBChecked).toBe(true);
      expect(changeDetectorRef.markForCheck).toHaveBeenCalledTimes(2);
    });

    it('should not call updateIconsCount if buildYourBetAvailable is false', () => {
      component['checkBybIcon'] = jasmine.createSpy('checkBybIcon').and.returnValue(observableOf(false));
      component['processFlags'] = jasmine.createSpy();
      component.ngOnInit();

      expect(component.isBYBChecked).toBe(true);
      expect(component['updateIconsCount']).not.toHaveBeenCalled();
      expect(changeDetectorRef.markForCheck).toHaveBeenCalledTimes(2);
    });

    it('should not set isBYBChecked to true in case of error', () => {
      component['checkBybIcon'] = jasmine.createSpy('checkBybIcon').and.returnValue(throwError('error'));
      component.ngOnInit();

      expect(component.isBYBChecked).toBe(false);
    });

    it('case when buildYourBetAvailable is defined', () => {
      component.buildYourBetAvailable = true;
      component.typeId = 123;
      component.ngOnInit();

      expect(component.isBYBChecked).toBe(true);
    });

    it('case when typeId is not defined', () => {
      component.buildYourBetAvailable = undefined;
      component.typeId = undefined;
      component.ngOnInit();

      expect(component.isBYBChecked).toBe(true);
    });
  });

  describe('#checkBybIcon', () => {
    it('should return Byb value',  (done) =>  {
      component.buildYourBetAvailable = false;
      component['updateIconsCount'] = jasmine.createSpy('updateIconsCount');

      component.checkBybIcon().subscribe((data: boolean) => {
        expect(data).toBeFalsy();
        expect(component['updateIconsCount']).not.toHaveBeenCalled();
        done();
      });
    });

    it('should return Byb value and update icons count',  (done) =>  {
      component.buildYourBetAvailable = true;
      component['updateIconsCount'] = jasmine.createSpy('updateIconsCount');

      component.checkBybIcon().subscribe((data: boolean) => {
        expect(data).toBeTruthy();
        expect(component['updateIconsCount']).toHaveBeenCalledWith(1);
        done();
      });
    });

    it('should call executeAsync two times with relevant parameters for competition', fakeAsync(() => {
      component.buildYourBetAvailable = undefined;
      component.eventId = undefined;
      component.typeId = 123;

      component.checkBybIcon().subscribe();
      tick();

      expect(commandService.executeAsync)
        .toHaveBeenCalledWith(commandService.API.DS_WHEN_YC_READY, [ 'isEnabledYCIcon', true ], {});
      expect(commandService.executeAsync)
        .toHaveBeenCalledWith(commandService.API.DS_IS_AVAILABLE_FOR_COMPETITION, [component.typeId], {});
    }));

    it('should call executeAsync two times with relevant parameters for events', fakeAsync(() => {
      component.buildYourBetAvailable = undefined;
      component.typeId = 123;
      component.eventId = 12;

      component.checkBybIcon().subscribe();
      tick();

      expect(commandService.executeAsync)
        .toHaveBeenCalledWith(commandService.API.DS_WHEN_YC_READY, [ 'isEnabledYCIcon', true ], {});
      expect(commandService.executeAsync)
        .toHaveBeenCalledWith(commandService.API.DS_IS_AVAILABLE_FOR_EVENTS, [component.eventId], {});
    }));
  });

  describe('ngOnDestroy', () => {
    it('should unsync and unsubscribe from events if isPromoSignpostingEnabled', () => {
      const componentName = `PromotionIconComponent${coreToolsService.uuid()}`;

      component.isPromoSignpostingEnabled = true;

      component.ngOnDestroy();
      expect(pubSubService.unsubscribe).toHaveBeenCalledWith(componentName);
    });

    it('should not unsync and unsubscribe from events if no isPromoSignpostingEnabled', () => {
      component.isPromoSignpostingEnabled = false;

      component.ngOnDestroy();
      expect(pubSubService.unsubscribe).not.toHaveBeenCalled();
    });
  });

  it('iconAction should open promotion dialog', () => {
    const event = {
        preventDefault: jasmine.createSpy('preventDefault'),
        stopPropagation: jasmine.createSpy('stopPropagation')
      } as any,
      icon = {
        customTypeLevelFlag: 'customTypeLevelFlag'
      } as any;

    component.type = 'customType';

    component.iconAction(event, icon);

    expect(event.stopPropagation).toHaveBeenCalled();
    expect(event.preventDefault).toHaveBeenCalled();
    expect(promotionsService.openPromotionDialog).toHaveBeenCalledWith('customTypeLevelFlag');
  });

  it('trackByPromoIcon should return promo icon index', () => {
    const item = { flagName: 'flagName', iconId: 'iconId', promoKey: 'promoKey' } as any;

    expect(component.trackByPromoIcon(1, item)).toEqual('1flagNameiconIdpromoKey');
  });

  describe('Tests for BOG', () => {
    it('should check isBogCmsEnabled when isGpAvailable = undefined', () => {
      component.isGpAvailable = undefined;
      expect(cmsService.isBogFromCms).not.toHaveBeenCalled();
    });

    it('should check isBogCmsEnabled when isGpAvailable = false', () => {
      component.isGpAvailable = false;
      expect(cmsService.isBogFromCms).not.toHaveBeenCalled();
    });

    it('should check isBogCmsEnabled when isGpAvailable = true', () => {
      component.isGpAvailable = true;
      component['cmsService'].isBogFromCms = jasmine.createSpy('isBogFromCms').and.returnValue(observableOf(true));

      component.ngOnInit();
      expect(component.isBogCmsEnabled).toBe(true);
    });

    it('should check isBogCmsEnabled when isGpAvailable = true and isBogFromCms = false', () => {
      component.isGpAvailable = true;
      component['cmsService'].isBogFromCms = jasmine.createSpy('isBogFromCms').and.returnValue(observableOf(false));

      component.ngOnInit();
      expect(component.isBogCmsEnabled).toBe(false);
    });

    it('should call bogAction()', () => {
      const event = {
        stopPropagation: jasmine.createSpy('stopPropagation'),
        preventDefault: jasmine.createSpy('preventDefault')
      } as any;
      component['promotionsService'].openPromotionDialog = jasmine.createSpy('openPromotionDialog');
      component['promotionsService'].trackBogDialog = jasmine.createSpy('trackBogDialog');
      component.bogAction(event);

      expect(event.stopPropagation).toHaveBeenCalledTimes(1);
      expect(event.preventDefault).toHaveBeenCalledTimes(1);
      expect(component['promotionsService'].openPromotionDialog).toHaveBeenCalled();
      expect( component['promotionsService'].trackBogDialog).toHaveBeenCalledWith('MKTFLAG_BOG', 'ok');
    });
  });

  describe('updateIconsCount', () => {
    it('should update icons count and do not change mode', () => {
      component['updateIconsCount'](1);

      expect(component.iconsCount).toEqual(1);
      expect(component.mode).toEqual('md');
    });

    it('should update icons count and change mode', () => {
      component.mode = 'sm';

      component['updateIconsCount'](2);

      expect(component.iconsCount).toEqual(2);
      expect(component.mode).toEqual('mini');
    });

    it('should call markForCheck after count updated', () => {
      component.iconsCount = 5;
      component['updateIconsCount'](5);

      expect(component.iconsCount).toBe(10);
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    });
  });
});
