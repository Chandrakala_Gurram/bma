import { PromotionDetailsComponent } from './promotion-details.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';
import { of, throwError } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';

describe('PromotionDetailsComponent', () => {
  let promotionDetailsComponent,
    msgDetails,
    router,
    route,
    cmsService,
    promotionsService,
    pubSubService,
    dynamicComponentsService,
    rendererService,
    promotionData,
    changeDetectorRef;

  beforeEach(() => {
    promotionsService = {
      isUserLoggedIn: jasmine.createSpy('isUserLoggedIn')
    };
    dynamicComponentsService = {
      addComponent: jasmine.createSpy()
    };
    router = {
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    };
    route = {
      params: of({ typeName: 'typeName', promoKey: 'promoKey' })
    };
    pubSubService = {
      subscribe: jasmine.createSpy('subscribe').and.callFake((p1, p2, callback) => {
        callback();
      }),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      publish: jasmine.createSpy('publish'),
      API: pubSubApi
    };
    cmsService = {
      getSystemConfig: jasmine.createSpy('getSystemConfig').and.callFake((p1, p2) => {
        return of(['sysConfig']);
      }),
    };
    promotionData = {
      marketLevelFlag: 'marketLevelFlag',
      eventLevelFlag: 'eventLevelFlag',
      useDirectFileUrl: 'sport/football/matches/today',
      directFileUrl: 'sport/football/matches/today',
      overlayBetNowUrl: 'url',
      flagName: 'flags',
      iconId: 'icon/122'
    };
    promotionsService = {
      isUserLoggedIn: jasmine.createSpy('isUserLoggedIn'),
      disableOptInButton: jasmine.createSpy('disableOptInButton'),
      decorateLinkAndTrust: jasmine.createSpy('decorateLinkAndTrust'),
      enableOptInButton: jasmine.createSpy('enableOptInButton'),
      changeBtnLabel: jasmine.createSpy('changeBtnLabel'),
      sendGTM: jasmine.createSpy('sendGTM'),
      preparePromotions: jasmine.createSpy('preparePromotions').and.callFake((p1) => {
        return p1;
      }),
      storeId: jasmine.createSpy('storeId').and.callFake((p1) => {
        return of({fired: false});
      })
    };
    dynamicComponentsService = {
      addComponent: jasmine.createSpy('addComponent')
    };
    rendererService = {
      renderer: {
        listen: jasmine.createSpy('listen')
      }
    };
    changeDetectorRef = {
      detectChanges: jasmine.createSpy('detectChanges')
    };

    promotionDetailsComponent = new PromotionDetailsComponent(
      promotionsService,
      pubSubService,
      rendererService,
      {} as any,
      {} as any,
      cmsService,
      dynamicComponentsService,
      router,
      route,
      changeDetectorRef
    );

    promotionDetailsComponent['sysConfig'] = {
      OptInMessagging: {
        alreadyOptedInMessage: 'successMessage',
        errorMessage: 'ffs',
        successMessage: 'successMessage',
      }
    };
    promotionDetailsComponent['elementRef'] = {
      nativeElement: {
        querySelectorAll: () => {},
        querySelector: () => {},
      }
    };
  });

  describe('@infoPanelWarningMessage', () => {
    beforeEach(() => {
      promotionDetailsComponent.sysConfig = { OptInMessagging: { errorMessage: 'ffs' } };
      msgDetails = { type: 'warning', message: 'ffs', align: 'center' };
    });

    it('should run infoPanelWarningMessage function first time', () => {
      promotionDetailsComponent['infoPanelComponent'] = null;
      promotionDetailsComponent['optInButton'] = { parentNode: 'who cares' };
      const optInButton = promotionDetailsComponent['optInButton'];

      promotionDetailsComponent['infoPanelWarningMessage']();

      expect(dynamicComponentsService.addComponent)
        .toHaveBeenCalledWith(jasmine.any(Function), msgDetails, optInButton.parentNode, optInButton);
    });

    it('should run infoPanelWarningMessage function rest times with other params', () => {
      promotionDetailsComponent['infoPanelComponent'] = { instance: { showInfoPanel() {} } };
      const ifoPanelInstance = promotionDetailsComponent['infoPanelComponent'].instance;
      spyOn(ifoPanelInstance, 'showInfoPanel');

      promotionDetailsComponent['infoPanelWarningMessage']();

      expect(ifoPanelInstance.showInfoPanel)
        .toHaveBeenCalledWith(msgDetails.message, msgDetails.type);
    });
  });


  describe('init', () => {
    beforeEach(() => {
      promotionDetailsComponent.sysConfig = { OptInMessagging: { errorMessage: 'ffs' } };
      msgDetails = { type: 'warning', message: 'ffs', align: 'center' };
    });

    it('should call disableOptInButton', () => {
      pubSubService.subscribe = jasmine.createSpy('subscribe').and.callFake((p1, p2, callback) => {
        callback('status');
      });
      spyOn(promotionDetailsComponent, 'disableOptInButton');
      promotionDetailsComponent['infoPanelComponent'] = null;
      promotionDetailsComponent['optInButton'] = { parentNode: 'who cares' };
      promotionDetailsComponent['validPromotion'] = true;
      promotionDetailsComponent['init']();
      expect(promotionDetailsComponent['disableOptInButton']).toHaveBeenCalled();
    });

    it('should not call disableOptInButton', () => {
      pubSubService.subscribe = jasmine.createSpy('subscribe').and.callFake((p1, p2, callback) => {
        callback();
      });
      spyOn(promotionDetailsComponent, 'disableOptInButton');
      promotionDetailsComponent['infoPanelComponent'] = null;
      promotionDetailsComponent['optInButton'] = { parentNode: 'who cares' };
      promotionDetailsComponent['validPromotion'] = true;
      promotionDetailsComponent['init']();
      expect(promotionDetailsComponent['disableOptInButton']).not.toHaveBeenCalled();
    });

    it('should call enableOptInButton', () => {
      spyOn(promotionDetailsComponent, 'enableOptInButton');
      promotionDetailsComponent['infoPanelComponent'] = null;
      promotionDetailsComponent['optInButton'] = { parentNode: 'who cares' };
      promotionDetailsComponent['validPromotion'] = true;
      promotionDetailsComponent['init']();
      expect(promotionDetailsComponent['enableOptInButton']).toHaveBeenCalled();
    });

    it('should call initPromo', () => {
      spyOn(promotionDetailsComponent, 'initPromo');
      let status = true;
      promotionsService.isUserLoggedIn = jasmine.createSpy('isUserLoggedIn').and.callFake(() => {
        status = !status;
        return status;
      });
      promotionDetailsComponent['infoPanelComponent'] = null;
      promotionDetailsComponent['optInButton'] = { parentNode: 'who cares' };
      promotionDetailsComponent['lastLoginStatus'] = 'lastLoginStatus';
      promotionDetailsComponent['init']();

      expect(promotionDetailsComponent['initPromo']).toHaveBeenCalled();
    });

  });


  describe('OnInit', () => {

    it('should call onInit', () => {
      promotionDetailsComponent['elementRef'] = {
        nativeElement: {
          querySelector: jasmine.createSpy().and.returnValue('handle-opt-in'),
          querySelectorAll: jasmine.createSpy().and.returnValue('#promo-descr a')
        }
      };
      promotionDetailsComponent['rendererService'] = {
        renderer: {
          listen: jasmine.createSpy('listen').and.callFake((p1, p2, p3) => {
            return  {listener: true} ;
          })
        }
      };
      promotionsService.promotionData =  jasmine.createSpy().and.returnValue(Promise.resolve(
        promotionData
      ));
      spyOn(promotionDetailsComponent, 'showSpinner');
      promotionDetailsComponent.ngOnInit();

      expect(promotionDetailsComponent.sysConfig).toEqual(['sysConfig']);
    });

    it('should call onInit', () => {
      promotionsService.promotionData =  jasmine.createSpy().and.returnValue(throwError(null));
      spyOn(promotionDetailsComponent, 'showError');
      spyOn(promotionDetailsComponent, 'hideSpinner');
      promotionDetailsComponent.ngOnInit();

      expect(promotionDetailsComponent.showError).toHaveBeenCalled();
      expect(promotionDetailsComponent.hideSpinner).toHaveBeenCalled();
    });

  });

  describe('disable Option button', () => {

    it('should not have been called ', () => {
      promotionDetailsComponent['optInButton'] = false;
      promotionDetailsComponent.disableOptInButton();

      expect(promotionsService.disableOptInButton).not.toHaveBeenCalled();
    });

    it('should have been called ', () => {
      promotionDetailsComponent['optInButton'] = true;
      promotionDetailsComponent['optInButtonListeners'] = [true];
      promotionDetailsComponent.disableOptInButton();

      expect(promotionsService.disableOptInButton)
        .toHaveBeenCalledWith(promotionDetailsComponent['optInButton'], promotionDetailsComponent['optInButtonListeners']);
    });

  });
  describe('enabled Option button', () => {

    it('should not been enabled option button ', () => {
      promotionDetailsComponent['optInButton'] = false;
      promotionDetailsComponent['optInButtonHandler'] = false;
      promotionDetailsComponent.enableOptInButton();

      expect(promotionsService.enableOptInButton).not.toHaveBeenCalled();
    });
    it('should not been enabled option button ', () => {
      promotionDetailsComponent['optInButton'] = true;
      promotionDetailsComponent['optInButtonHandler'] = false;
      promotionDetailsComponent.enableOptInButton();

      expect(promotionsService.enableOptInButton).not.toHaveBeenCalled();
    });

    it('should have been option enabled ', () => {
      promotionDetailsComponent['optInButton'] = true;
      promotionDetailsComponent['optInButtonHandler'] = true;
      promotionDetailsComponent.enableOptInButton();

      const result = [];
      result.push(promotionsService.enableOptInButton
      (promotionDetailsComponent['optInButton'], promotionDetailsComponent['optInButtonHandler']));

      expect(promotionDetailsComponent.optInButtonListeners).toEqual(result);
    });

  });

  describe('#changeAccordionState', () => {
    it('should call changeAccordionState method', () => {
      expect(promotionDetailsComponent.isExpanded).toEqual([true, true]);

      promotionDetailsComponent.changeAccordionState(1, false);

      expect(promotionDetailsComponent.isExpanded).toEqual([true, false]);
    });
  });

  describe('promoNotFired', () => {

    it('should call enableOptInButton', () => {

      spyOn(promotionDetailsComponent, 'enableOptInButton');
      promotionDetailsComponent['optInButtonClicked'] = false;
      promotionDetailsComponent.promoNotFired();

      expect(promotionDetailsComponent.enableOptInButton).toHaveBeenCalled();
    });
    it('should call enableOptInButton', () => {

      spyOn(promotionDetailsComponent, 'startOptIn');
      promotionDetailsComponent['optInButtonClicked'] = true;
      promotionDetailsComponent.promoNotFired();

      expect(promotionDetailsComponent['optInButtonClicked']).toBe(false);
      expect(promotionDetailsComponent.startOptIn).toHaveBeenCalled();
    });

  });

  describe('promoFired', () => {

    it('should call enableOptInButton', () => {

      promotionDetailsComponent['sysConfig'] = {
        OptInMessagging: {
          alreadyOptedInMessage: 'successMessage'
        }
      };
      promotionDetailsComponent.optInButton = true;
      promotionDetailsComponent.promoFired();

      expect(promotionsService.changeBtnLabel).toHaveBeenCalledWith
      (promotionDetailsComponent.sysConfig.OptInMessagging.alreadyOptedInMessage, promotionDetailsComponent.optInButton );
    });
  });
  describe('initPromotionButton', () => {

    beforeEach(() => {
      promotionDetailsComponent['elementRef'] = {
        nativeElement: {
          querySelector: jasmine.createSpy().and.returnValue('handle-opt-in')
        }
      };

      promotionDetailsComponent.optInButton = true;
      promotionDetailsComponent['domToolsService'] = {
        removeClass: jasmine.createSpy('removeClass')
      };
      promotionDetailsComponent['rendererService'] = {
        renderer: {
          listen: jasmine.createSpy('listen').and.callFake((p1, p2, p3) => {
           return  {listener: true} ;
          })
        }
      };

      spyOn(window, 'clearTimeout');
    });

    it('should call domToolsService', fakeAsync(() => {
      promotionDetailsComponent['COMPILATION_DELAY'] = '1000';
      promotionDetailsComponent['timeoutInstance'] = '1000';
      promotionDetailsComponent.initPromotionButton();
      tick(1000);
      expect(promotionDetailsComponent['optInButton']).toEqual('handle-opt-in');
      expect(promotionDetailsComponent['domToolsService'].removeClass).toHaveBeenCalled();
    }));

    it('should not been called domToolsService', fakeAsync(() => {
      promotionDetailsComponent['elementRef'] = {
        nativeElement: {
          querySelector: jasmine.createSpy().and.returnValue(null)
        }
      };
      promotionDetailsComponent['COMPILATION_DELAY'] = '1000';
      promotionDetailsComponent.initPromotionButton();
      tick(1000);
      expect(promotionDetailsComponent['optInButton']).toEqual(null);
      expect(promotionDetailsComponent['domToolsService'].removeClass).not.toHaveBeenCalled();
    }));

    it('should add new eventListener', fakeAsync(() => {
      promotionDetailsComponent['COMPILATION_DELAY'] = '1000';
      promotionDetailsComponent.initPromotionButton();
      tick(1000);
      expect(promotionDetailsComponent['optInButtonListeners']).toEqual([{listener: true}]);
      expect(promotionDetailsComponent['eventListeners']).toEqual( [{listener: true}]);
    }));

    it('should call disableOptInButton', fakeAsync(() => {
      promotionDetailsComponent['COMPILATION_DELAY'] = '1000';
      promotionsService['isUserLoggedIn'] = jasmine.createSpy().and.callFake(() => {
        return true;
      });
      spyOn(promotionDetailsComponent, 'disableOptInButton');
      spyOn(promotionDetailsComponent, 'checkPromotionStatus');

      promotionDetailsComponent.initPromotionButton();
      tick(1000);

      expect(promotionDetailsComponent['disableOptInButton']).toHaveBeenCalled();
      expect(promotionDetailsComponent['checkPromotionStatus']).toHaveBeenCalled();
    }));
  });

  describe('sendGTM', () => {
    it('send should be call', () => {
      const clickEvent = {
        target: {
          dataset: {}
        }
      };
      promotionDetailsComponent['validPromotion'] = true;
      promotionDetailsComponent.sendGTM(clickEvent);

      expect(promotionsService['sendGTM']).toHaveBeenCalled();
    });
  });


  describe('initPromo', () => {
    beforeEach(() => {
      spyOn(promotionDetailsComponent, 'initPromotionButton');
    });

    it('should call initPromo', () => {
      promotionsService.preparePromotions = jasmine.createSpy('preparePromotions').and.callFake((p1) => {
        return [{
          requestId: 'requestId',
          safeDescription: 'safeDescription',
          safeHtmlMarkup: 'safeHtmlMarkup',
          description: 'description',
          htmlMarkup: 'htmlMarkup'
        }];
      });
      promotionDetailsComponent['promotion'] = true;
      promotionDetailsComponent.initPromo();

      expect(promotionDetailsComponent['initPromotionButton']).toHaveBeenCalled();
    });

    it('should not call initPromo', () => {
      promotionsService.preparePromotions = jasmine.createSpy('preparePromotions').and.callFake((p1) => []);
      promotionDetailsComponent['promotion'] = true;
      promotionDetailsComponent.initPromo();

      expect(promotionDetailsComponent['initPromotionButton']).not.toHaveBeenCalled();
    });
  });

  describe('optInButtonHandler', () => {

    it('disableOptInButton to have not been called', () => {
      const event = {
        preventDefault: () => {},
        stopPropagation: () => {},
      };
      spyOn(promotionDetailsComponent, 'disableOptInButton');
      spyOn(promotionDetailsComponent, 'startOptIn');

      promotionDetailsComponent['validPromotion'] = true;
      promotionDetailsComponent['infoPanelComponent'] = {
        instance: {
          hideInfoPanel: () => {}
        }
      };
      promotionDetailsComponent.optInButtonHandler(event);

      expect(pubSubService['publish']).toHaveBeenCalled();
      expect(promotionDetailsComponent['disableOptInButton']).not.toHaveBeenCalled();
      expect(promotionDetailsComponent['startOptIn']).not.toHaveBeenCalled();
    });

    it('disableOptInButton to have been called', () => {
      const event = {
        preventDefault: () => {},
        stopPropagation: () => {},
      };

      promotionsService['isUserLoggedIn'] = jasmine.createSpy().and.callFake(() => {
        return true;
      });

      spyOn(promotionDetailsComponent, 'disableOptInButton');
      spyOn(promotionDetailsComponent, 'startOptIn');

      promotionDetailsComponent['validPromotion'] = true;
      promotionDetailsComponent.optInButtonHandler(event);

      expect(pubSubService['publish']).not.toHaveBeenCalled();
      expect(promotionDetailsComponent['disableOptInButton']).toHaveBeenCalled();
      expect(promotionDetailsComponent['startOptIn']).toHaveBeenCalled();
    });
  });

  describe('checkPromotionStatus', () => {
    it('notFiredCallBack should be call', () => {

      promotionsService.checkStatus = jasmine.createSpy().and.callFake((p1) => {
        return of({fired: false});
      });

      promotionDetailsComponent.validPromotion =  {
        requestId: 123
      };
      const firedCallBack = spyOn(promotionDetailsComponent, 'infoPanelWarningMessage');
      const notFiredCallBack = spyOn(promotionDetailsComponent, 'enableOptInButton');
      promotionDetailsComponent.checkPromotionStatus(firedCallBack, notFiredCallBack);

      expect(notFiredCallBack).toHaveBeenCalled() ;
    });

    it('FiredCallBack should be call', () => {

      promotionsService.checkStatus = jasmine.createSpy().and.callFake((p1) => {
        return of({fired: true});
      });

      promotionDetailsComponent.validPromotion =  {
        requestId: 123
      };
      const firedCallBack = spyOn(promotionDetailsComponent, 'enableOptInButton');
      const notFiredCallBack = spyOn(promotionDetailsComponent, 'infoPanelWarningMessage');
      promotionDetailsComponent.checkPromotionStatus(firedCallBack, notFiredCallBack);

      expect(firedCallBack).toHaveBeenCalled() ;
    });

    it('should return error should', () => {
      promotionsService.checkStatus = jasmine.createSpy().and.returnValue(throwError(null));
      promotionDetailsComponent.validPromotion =  {
        requestId: 123
      };
      const firedCallBack = spyOn(promotionDetailsComponent, 'infoPanelWarningMessage');
      const notFiredCallBack = spyOn(promotionDetailsComponent, 'enableOptInButton');
      promotionDetailsComponent.checkPromotionStatus(firedCallBack, notFiredCallBack);

      expect(notFiredCallBack).toHaveBeenCalled() ;
    });
  });

  describe('startOptIn', () => {

    it('should be true', () => {

      spyOn(promotionDetailsComponent, 'infoPanelWarningMessage');
      spyOn(promotionDetailsComponent, 'enableOptInButton');
      promotionDetailsComponent['validPromotion'] = {
        requestId: 123
      };
      promotionDetailsComponent.startOptIn();

      expect(promotionDetailsComponent.infoPanelWarningMessage).toHaveBeenCalled();
      expect(promotionDetailsComponent.enableOptInButton).toHaveBeenCalled();
    });

    it('changeBtnLabel should be called', () => {
      promotionsService['storeId'] =  jasmine.createSpy('storeId').and.callFake((p1) => {
        return of({fired: true});
      });
        spyOn(promotionDetailsComponent, 'infoPanelWarningMessage');
        spyOn(promotionDetailsComponent, 'enableOptInButton');
      promotionDetailsComponent['validPromotion'] = {
        requestId: 123
      };
      promotionDetailsComponent.startOptIn();

        expect(promotionDetailsComponent.infoPanelWarningMessage).not.toHaveBeenCalled();
        expect(promotionDetailsComponent.enableOptInButton).not.toHaveBeenCalled();
    });

    it('should be false ', () => {

      spyOn(promotionDetailsComponent, 'infoPanelWarningMessage');
      spyOn(promotionDetailsComponent, 'enableOptInButton');
      promotionDetailsComponent['validPromotion'] = {
        requestId: 123
      };
      promotionDetailsComponent.startOptIn();

      expect(promotionDetailsComponent.infoPanelWarningMessage).toHaveBeenCalled();
      expect(promotionDetailsComponent.enableOptInButton).toHaveBeenCalled();
    });

    it('should return error ', () => {

      spyOn(promotionDetailsComponent, 'infoPanelWarningMessage');
      spyOn(promotionDetailsComponent, 'enableOptInButton');

      promotionsService.storeId = jasmine.createSpy().and.returnValue(throwError(null));
      promotionDetailsComponent['validPromotion'] = {
        requestId: 123
      };
      promotionDetailsComponent.startOptIn();

      expect(promotionDetailsComponent.infoPanelWarningMessage).toHaveBeenCalled();
      expect(promotionDetailsComponent.enableOptInButton).toHaveBeenCalled();
    });

  });


  describe('@checkRedirect', () => {
    it('should navigate using route if attr "routerlink" is exist', () => {
      const clickEvent = {
        target: {
          dataset: {
            routerlink: 'sport/football/matches/today'
          }
        },
        path: []
      };
      promotionDetailsComponent.checkRedirect(clickEvent);
      expect(router.navigateByUrl).toHaveBeenCalledWith('sport/football/matches/today');
    });

    it('should not navigate if attr "routerlink" is not exist', () => {
      const clickEvent = {
        target: {
          dataset: {}
        },
        path: []
      };
      promotionDetailsComponent.checkRedirect(clickEvent);
      expect(router.navigateByUrl).not.toHaveBeenCalled();
    });

    describe('ngOnDestroy', () => {
      it('ngOnDestroy, init', () => {
        spyOn(window, 'clearTimeout');

        promotionDetailsComponent['timeoutInstance'] = '100';
        promotionDetailsComponent['init']();
        promotionDetailsComponent.ngOnDestroy();
        expect(pubSubService.subscribe).toHaveBeenCalledTimes(4);
        expect(pubSubService.unsubscribe).toHaveBeenCalledTimes(1);
      });

      it('should remove all listeners', () => {
        const listener = jasmine.createSpy('listener');

        promotionDetailsComponent.eventListeners.push(listener);

        promotionDetailsComponent.ngOnDestroy();
        expect(listener).toHaveBeenCalled();
      });
    });
  });

  describe('@handleGtmTracking', () => {
    it('handleGtmTracking (sendGTM)', () => {
      const event = <any>{
        target: {
          nodeName: 'A'
        },
        path: [
          {
            id: 'q'
          },
          {
            id: 'terms-and-cond'
          }
        ]
      };
      promotionDetailsComponent['handleGtmTracking'](event);
      expect(promotionsService.sendGTM).toHaveBeenCalled();
    });

    it('handleGtmTracking (sendGTM)', () => {
      const event = <any>{
        target: {
          nodeName: 'A'
        },
        path: [
          {
            id: 'q'
          },
          {
            id: 'promo-descr'
          }
        ]
      };
      promotionDetailsComponent['handleGtmTracking'](event);
      expect(promotionsService.sendGTM).toHaveBeenCalled();
    });

    it('handleGtmTracking', () => {
      const event = <any>{
        target: {
          nodeName: 'A'
        },
        path: [
          {
            id: 'q'
          },
          {
            id: 'p'
          }
        ]
      };
      promotionDetailsComponent['handleGtmTracking'](event);
      expect(promotionsService.sendGTM).not.toHaveBeenCalled();
    });

    it('handleGtmTracking', () => {
      const event = <any>{
        target: {
          nodeName: 'B'
        },
        path: [
          {
            id: 'q'
          },
          {
            id: 'terms-and-cond'
          }
        ]
      };

      promotionDetailsComponent['handleGtmTracking'](event);
      expect(promotionsService.sendGTM).not.toHaveBeenCalled();
    });
  });
  describe('ngOnInit', () => {
    it('should call getSystemConfig with false param', () => {
      promotionDetailsComponent.ngOnInit();

      expect(cmsService.getSystemConfig).toHaveBeenCalled();
    });
  });
});
