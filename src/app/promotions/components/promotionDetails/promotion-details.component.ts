import { map, concatMap } from 'rxjs/operators';
import {
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef
} from '@angular/core';
import * as _ from 'underscore';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { PromotionsService } from '@promotions/services/promotions/promotions.service';
import { ISpPromotion } from '@promotions/models/sp-promotion.model';
import { DomToolsService } from '@coreModule/services/domTools/dom.tools.service';
import { NgInfoPanelComponent } from '@shared/components/infoPanel/ng-info-panel.component';
import { DynamicComponentsService } from '@core/services/dynamicComponents/dynamic-components.service';
import { IDynamicComponent } from '@core/services/dynamicComponents/dynamic-components.model';
import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { ISystemConfig } from '@core/services/cms/models';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { ICheckStatusResponse } from '@promotions/models/response.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { RendererService } from '@shared/services/renderer/renderer.service';

@Component({
  selector: 'promotion-details',
  templateUrl: './promotion-details.component.html',
  styleUrls: ['../../assets/styles/main.less'],
  encapsulation : ViewEncapsulation.None
})

export class PromotionDetailsComponent extends AbstractOutletComponent implements OnInit, OnDestroy {
  sysConfig: ISystemConfig;
  promotion: ISpPromotion;
  validPromotion: ISpPromotion;
  isExpanded: boolean[] = [true, true];

  private readonly COMPONENT_NAME: string = 'PromotionDetailsComponent';

  private timeoutInstance: any = null;
  private optInButton: HTMLElement = null;
  private optInButtonClicked: boolean = false;
  private infoPanelComponent: IDynamicComponent = null;
  private lastLoginStatus: boolean;
  private COMPILATION_DELAY: number = 300;
  private eventListeners: Function[] = [];
  private optInButtonListeners: Function[] = [];

  constructor(
    private promotionsService: PromotionsService,
    private pubSubService: PubSubService,
    private rendererService: RendererService,
    private elementRef: ElementRef,
    private domToolsService: DomToolsService,
    private cmsService: CmsService,
    private dynamicComponentsService: DynamicComponentsService,
    private router: Router,
    private route: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    super();

    this.optInButtonHandler = this.optInButtonHandler.bind(this);
    this.promoFired = this.promoFired.bind(this);
    this.promoNotFired = this.promoNotFired.bind(this);
    this.sendGTM = this.sendGTM.bind(this);
  }

  ngOnInit(): void {
    this.route.params.pipe(concatMap((params: Params) => {
      this.showSpinner();
      return this.cmsService.getSystemConfig().pipe(
         map((systemConfig: ISystemConfig) => {
           this.sysConfig = systemConfig;

           return params.promoKey;
         }));
    }),
    concatMap((promoKey: string) => {
      return this.promotionsService.promotionData(promoKey);
    }))
    .subscribe((promotionData: ISpPromotion) => {
      this.promotion = promotionData;
      this.init();
      this.hideSpinner();
    }, () => {
      this.showError();
      this.hideSpinner();
    });
  }

  ngOnDestroy(): void {
    this.timeoutInstance && clearTimeout(this.timeoutInstance);
    this.pubSubService.unsubscribe(this.COMPONENT_NAME);

    // remove all renderer.listen listeners
    _.each(this.eventListeners, removeEventListener => {
      removeEventListener();
    });
  }

  changeAccordionState(index: number, value: boolean): void {
    this.isExpanded[index] = value;
    this.changeDetectorRef.detectChanges();
  }

  @HostListener('click', ['$event'])
  checkRedirect(event: MouseEvent): void {
    const redirectUrl: string = (<HTMLElement>event.target).dataset.routerlink;

    this.handleGtmTracking(event);

    if (redirectUrl) {
      this.router.navigateByUrl(redirectUrl);
    }
  }

  private init(): void {
    this.lastLoginStatus = this.promotionsService.isUserLoggedIn();

    // Waits for login state change (like login process started and login process finished)
    this.pubSubService.subscribe(this.COMPONENT_NAME, this.pubSubService.API.LOGIN_PENDING, status => {
      if (status && this.validPromotion && this.optInButton) {
        this.disableOptInButton();
      }
    });

    // Waits for login failed
    this.pubSubService.subscribe(this.COMPONENT_NAME, this.pubSubService.API.SET_LOGOUT_STATUS, () => {
      if (this.validPromotion && this.optInButton) {
        this.enableOptInButton();
      }
    });

    /*
     * Waits for login process (have to wait until login and vip statuses will be refreshed to be able to
     * filter promotions corresponding to user's vip level) to be completed and make auto startOptIn or
     * manual by user's click
     */
    this.pubSubService.subscribe(this.COMPONENT_NAME, this.pubSubService.API.SUCCESSFUL_LOGIN, () => {
      this.lastLoginStatus = this.promotionsService.isUserLoggedIn();
      this.initPromo();
    });

    // Waits for logout - here is the main case - server unexpectedly log out User from App
    this.pubSubService.subscribe(this.COMPONENT_NAME, [this.pubSubService.API.SESSION_LOGOUT], () => {
      if (this.lastLoginStatus !== this.promotionsService.isUserLoggedIn()) {
        this.initPromo();
      }
    });

    this.initPromo();
  }

  private optInButtonHandler(e: MouseEvent): void {
    e.preventDefault();
    e.stopPropagation();

    this.infoPanelComponent && this.infoPanelComponent.instance.hideInfoPanel();
    this.optInButtonClicked = true;
    if (!this.promotionsService.isUserLoggedIn()) {
      this.pubSubService.publish(this.pubSubService.API.OPEN_LOGIN_DIALOG, { moduleName: 'optin' });
      return;
    }
    this.disableOptInButton();
    this.startOptIn();
  }

  private initPromo(): void {
    if (this.promotion) {
      const promotions = this.promotionsService.preparePromotions([this.promotion]);

      /*
       * Find link to mcasino in html source and add custom parameter 'deliveryPlatform' depending on
       * whether user using Wrapper or HTML5 app in browser
       */
      this.validPromotion = promotions.length && promotions[0];
      if (this.validPromotion) {
        this.validPromotion.safeDescription = this.promotionsService.decorateLinkAndTrust(this.validPromotion.description);
        this.validPromotion.safeHtmlMarkup = this.promotionsService.decorateLinkAndTrust(this.validPromotion.htmlMarkup);

        if (this.validPromotion.requestId) {
          this.initPromotionButton();
        }
      }
    }
  }

  /**
   * Enable button, add and remember listeners
   */
  private enableOptInButton(): void {
    if (!this.optInButton || !this.optInButtonHandler) { return; }

    this.optInButtonListeners.push(
      this.promotionsService.enableOptInButton(this.optInButton, this.optInButtonHandler)
    );
  }

  /**
   * Disable button, cancel added listeners
   */
  private disableOptInButton(): void {
    if (!this.optInButton) { return; }

    this.promotionsService.disableOptInButton(this.optInButton, this.optInButtonListeners);
  }

  /**
   * initializing Promotion Buttons GTM tracking functionality
   */
  private handleGtmTracking(event): void {
    const promoDescriptionId: string = 'promo-descr'; // all promo links
    const termsAndConditions: string = 'terms-and-cond'; // all T&C links
    const idPath: string = event.path.map(node => node.id).toString();

    if (!event.target || event.target.nodeName !== 'A') {
      return;
    }

    if (idPath.indexOf(promoDescriptionId) > -1 || idPath.indexOf(termsAndConditions) > -1) {
      this.sendGTM(event);
    }
  }

  private sendGTM(event: MouseEvent): void {
    this.promotionsService.sendGTM(this.validPromotion, event, true);
  }

  /**
   * initializing Promotion Button functionality
   */
  private initPromotionButton(): void {
    this.timeoutInstance && clearTimeout(this.timeoutInstance);

    this.timeoutInstance = setTimeout(() => {
      this.optInButton = this.elementRef.nativeElement.querySelector('.handle-opt-in');

      if (this.optInButton) {
        this.domToolsService.removeClass(
          this.optInButton,
          'hidden'
        );

        // add click listener for Opt in Button
        const listener = this.rendererService.renderer.listen(this.optInButton, 'click', this.optInButtonHandler);
        this.eventListeners.push(listener);
        this.optInButtonListeners.push(listener);

        if (this.promotionsService.isUserLoggedIn()) {
          this.disableOptInButton();
          this.checkPromotionStatus(this.promoFired, this.promoNotFired);
        }
      }
    }, this.COMPILATION_DELAY);
  }

  /**
   * checks Promotion Status and call needed callback
   * @param firedCallBack
   * @param notFiredCallBack
   */
  private checkPromotionStatus(firedCallBack: Function, notFiredCallBack: Function): void {
    this.promotionsService.checkStatus(this.validPromotion.requestId)
      .subscribe((response: ICheckStatusResponse) => {
        if (response.fired) {
          firedCallBack();
        } else {
          notFiredCallBack();
        }
      }, () => {
        console.warn('checkPromotionStatus failed');
        notFiredCallBack();
      });
  }

  /**
   * behavior for fired promotion
   */
  private promoFired(): void {
    this.promotionsService.changeBtnLabel(this.sysConfig.OptInMessagging.alreadyOptedInMessage, this.optInButton);
  }

  /**
   * behavior for not fired promotion
   */
  private promoNotFired(): void {
    if (this.optInButtonClicked) {
      this.optInButtonClicked = false;
      this.startOptIn();
    } else {
      this.enableOptInButton();
    }
  }

  /**
   * Start the Opt In functionality.
   */
  private startOptIn(): void {
    this.promotionsService.storeId(this.validPromotion.requestId)
      .subscribe(storeResponse => {
        if (storeResponse.fired) {
          this.promotionsService.changeBtnLabel(this.sysConfig.OptInMessagging.successMessage, this.optInButton);
        } else {
          this.infoPanelWarningMessage();
          this.enableOptInButton();
        }
      }, () => {
        this.infoPanelWarningMessage();
        this.enableOptInButton();
      });
  }

  /**
   * Adds/shows ng-info-panel component and shows Warning Message
   */
  private infoPanelWarningMessage(): void {
    const message = this.sysConfig.OptInMessagging.errorMessage,
      type = 'warning',
      align = 'center';

    if (!this.infoPanelComponent) {
      this.infoPanelComponent = this.dynamicComponentsService.addComponent(
        NgInfoPanelComponent,
        {type, message, align},
        this.optInButton.parentNode,
        this.optInButton
      );
    } else {
      this.infoPanelComponent.instance.showInfoPanel(message, type);
    }
  }
}
