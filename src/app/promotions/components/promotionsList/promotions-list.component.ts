import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import * as _ from 'underscore';

import { PromotionsService } from '@promotions/services/promotions/promotions.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { ISpPromotion } from '@promotions/models/sp-promotion.model';
import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { IPromotionsList, IPromotionSection } from '@core/services/cms/models';

@Component({
  selector: 'promotions-list',
  templateUrl: './promotions-list.component.html',
  styleUrls: ['../../assets/styles/main.less']
})
export class PromotionsListComponent extends AbstractOutletComponent implements OnInit, OnDestroy {
  @Input() promotions?: ISpPromotion[];
  @Input() isRetail?: boolean;
  @Input() skipGrouped?: boolean = false;

  validPromotions: ISpPromotion[];
  groupedPromotions: IPromotionsList;
  lastLoginStatus: boolean;
  sendGTM: Function;
  availableGroupedPromotions: boolean;

  private readonly componentName = 'PromotionsListComponent';

  constructor(
    private promotionsService: PromotionsService,
    private pubSubService: PubSubService,
  ) {
    super();
    this.lastLoginStatus = this.promotionsService.isUserLoggedIn();
    this.sendGTM = this.promotionsService.sendGTM;
  }

  ngOnInit(): void {
    this.showSpinner();
    if (this.promotions) {
      this.init();
    } else {
      this.getPromotionsRequest().subscribe((res: IPromotionsList) => {
        this.setPromotions(res);
        this.init();
      }, () => {
        this.showError();
      });
    }
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe(this.componentName);
  }

  trackPromotionBy(index: number, promotion: ISpPromotion): string {
    return promotion.id;
  }

  trackGroupBy(index: number, group: IPromotionSection): string {
    return `${group.name}_${index}`;
  }

  private init(): void {
    this.pubSubService.subscribe(this.componentName, this.pubSubService.API.SESSION_LOGIN, () => {
      this.getPromotionsRequest().subscribe((res: IPromotionsList) => {
        this.setPromotions(res);
        this.initPromotionsList();
      });
    });

    // Waits for logout - here is the main case - server unexpectedly log out User from App
    this.pubSubService.subscribe(this.componentName, [this.pubSubService.API.SESSION_LOGOUT], () => {
        this.initPromotionsList();
    });

    // start of receiving and setting promos
    this.initPromotionsList();
  }

  private initPromotionsList(): void {
    if (this.promotions) {
      const filteredByOfferIdPromotions = this.promotionsService.filterByOfferId(this.promotions);
      this.validPromotions = this.promotionsService.preparePromotions(filteredByOfferIdPromotions) as ISpPromotion[];
    }

    if (this.groupedPromotions) {
      this.availableGroupedPromotions = false;

      _.each(this.groupedPromotions.promotionsBySection, section => {
        const filteredByOfferIdPromotions = this.promotionsService.filterByOfferId(section.promotions as ISpPromotion[]);

        section.availablePromotions = this.promotionsService.preparePromotions(filteredByOfferIdPromotions);
        this.availableGroupedPromotions = this.availableGroupedPromotions ? this.availableGroupedPromotions
          : section.availablePromotions.length > 0;
      });
    }

    this.hideSpinner();
  }

  private setPromotions(data: IPromotionsList) {
    if (!this.skipGrouped) {
      if (data.promotionsBySection) {
        this.groupedPromotions = data;
      } else {
        this.promotions = data.promotions as ISpPromotion[];
      }
    }
  }

  private getPromotionsRequest(): Observable<IPromotionsList> {
    if (this.isRetail) {
      return this.promotionsService.promotionsRetailData();
    }

    return this.promotionsService.isGroupBySectionsEnabled().pipe(
      mergeMap((res: boolean) => {
        return res ? this.promotionsService.promotionsGroupedData() :
          this.promotionsService.promotionsDigitalData();
      })
    );
  }
}
