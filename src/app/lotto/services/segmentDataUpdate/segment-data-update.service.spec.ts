import { Subject } from 'rxjs';

import { SegmentDataUpdateService } from './segment-data-update.service';

describe('SegmentDataUpdateService', () => {
  let service: SegmentDataUpdateService;

  beforeEach(() => {
    service = new SegmentDataUpdateService();
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
    expect(service.dataSubject).toEqual(jasmine.any(Subject));
  });

  it('changes', () => {
    expect(service.changes).toEqual(jasmine.any(Subject));
  });
});
