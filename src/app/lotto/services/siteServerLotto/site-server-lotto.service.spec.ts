import {of as observableOf,  Observable } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';

import { SiteServerLottoService } from './site-server-lotto.service';
import environment from '@environment/oxygenEnvConfig';

describe('SiteServerLottoService', () => {
  let service: SiteServerLottoService;

  let http;
  let buildLotteriesService;
  let timeService;
  let simpleFiltersService;

  beforeEach(() => {
    http = {
      get: jasmine.createSpy().and.returnValue(observableOf(null))
    };

    buildLotteriesService = {
      build: jasmine.createSpy('build'),
      buildLottoResults: jasmine.createSpy('buildLottoResults')
    };

    timeService = {
      getTimeWithDelta: jasmine.createSpy('getTimeWithDelta'),
    };

    simpleFiltersService = {
      genFilters: jasmine.createSpy('genFilters')
    };

    service = new SiteServerLottoService(
      http,
      buildLotteriesService,
      timeService,
      simpleFiltersService
    );
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  it('getLotteries', () => {
    service['sendRequest'] = jasmine.createSpy().and.returnValue(observableOf(null));
    expect(service.getLotteries()).toEqual(jasmine.any(Observable));
    expect(service['sendRequest']).toHaveBeenCalledWith(
      `${environment.SITESERVER_LOTTERY_ENDPOINT}/LotteryToDraw?simpleFilter=lottery.hasOpenDraw&translationLang=en&responseFormat=json`
    );
  });

  it('should build lotteries',  fakeAsync(() => {
    const successHandler = jasmine.createSpy('successHandler');

    service['sendRequest'] = jasmine.createSpy().and.returnValue(observableOf({body: {}}));
    service.getLotteries().subscribe(successHandler);
    tick();

    expect(successHandler).toHaveBeenCalled();
    expect(buildLotteriesService.build).toHaveBeenCalledWith({});
  }));

  it('getLottoResults', () => {
    service['sendRequest'] = jasmine.createSpy().and.returnValue(observableOf(null));

    const result = service.getLottoResults({ lottoIds: ['1', '2', '3'], page: 1 });

    expect(simpleFiltersService.genFilters).toHaveBeenCalledWith(jasmine.any(Object));
    expect(timeService.getTimeWithDelta).toHaveBeenCalledWith(7);
    expect(timeService.getTimeWithDelta).toHaveBeenCalledWith(0);
    expect(service['sendRequest']).toHaveBeenCalledWith(
      jasmine.stringMatching(`${environment.SITESERVER_HISTORIC_ENDPOINT}/ResultsForLottery/1,2,3`)
    );
    expect(result).toEqual(jasmine.any(Observable));
  });

  it('should ', fakeAsync(() => {
    const successHandler = jasmine.createSpy('successHandler');

    service['sendRequest'] = jasmine.createSpy().and.returnValue(observableOf({body: {}}));
    service.getLottoResults({ lottoIds: ['1', '2', '3'], page: 1 }).subscribe(successHandler);
    tick();

    expect(successHandler).toHaveBeenCalled();
    expect(buildLotteriesService.buildLottoResults).toHaveBeenCalledWith({});
  }));

  it('sendRequest', () => {
    const url = 'http://path/to/api';
    const params = { x: 1, y: 2 };

    const result = service['sendRequest'](url, params);

    expect(http.get).toHaveBeenCalledWith(url, {
      observe: 'response', params
    });
    expect(result).toEqual(jasmine.any(Observable));
  });

  it('sendRequest', () => {
    const url = 'http://path/to/api';

    service['sendRequest'](url);

    expect(http.get).toHaveBeenCalledWith(url, {
      observe: 'response', params: {}
    });
  });
});
