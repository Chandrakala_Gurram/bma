import { of as observableOf,  Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import * as _ from 'underscore';

import { SiteServerLottoService } from '../siteServerLotto/site-server-lotto.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { SORT_CODES } from '@platform/lotto/services/mainLotto/main-lotto.constant';
import { ILotto, ILottoPrice, ILotteryMap, ILottonMenuItem, ILottoDraw } from '../../models/lotto.model';

@Injectable()
export class MainLottoService {

  lotteryData: ILotteryMap;
  activeLotto: string;

  constructor(
    private siteServerLottoService: SiteServerLottoService,
    private filterService: FiltersService
  ) {
    this.lotteryData = {};
  }

  getLotteryData(lotteryName: string): Observable<ILotto> {
    if (lotteryName && this.lotteryData[lotteryName]) {
      return observableOf(this.lotteryData[lotteryName]);
    } else if (this.lotteryData[this.activeLotto]) {
      return observableOf(this.lotteryData[this.activeLotto]);
    }

    return this.getLotteriesByLotto().pipe(map((lottoMap) => {
      return lottoMap[this.activeLotto];
    }));
  }

  getMenuItems(lotteriesByLotto: ILotto | ILotteryMap): ILottonMenuItem[] {
    const lottoArray = this.filterService.orderBy(_.toArray(lotteriesByLotto), ['shutAtTime', 'name']);

    return lottoArray.map(({ name, uri, sortCode }) => ({
      imageTitle: name,
      uri,
      svg: name,
      svgId: `${SORT_CODES.indexOf(sortCode) > -1 ? sortCode : '49-lotto'}`,
      inApp: true,
      targetUri: `/lotto/${uri}`,
      targetUriCopy: uri
    }));
  }

  getLotteriesByLotto(): Observable<ILotteryMap> {
    return this.siteServerLottoService.getLotteries().pipe(map((data) => {
      return this.arrangeByLotto(data);
    }));
  }

  getLottoType(lottoEntity, is7Ball) {
    return (!is7Ball && _.has(lottoEntity, 'normal')) ? 'normal' : 'boosterBall';
  }

  getShutAtTime(lotteryEntity: ILotto): string {
    return _.min(lotteryEntity.draw, (item: ILottoDraw) => {
      const currentTime = Date.now(),
        endTime = Date.parse(item.shutAtTime);
      if (endTime >= currentTime) {
        return Date.parse(item.shutAtTime);
      }

      return undefined;
    }).shutAtTime;
  }

  /**
   * Check if lottery is 7ball lottery
   *
   * @param lotteryName
   * @returns {boolean}
   */
  private is7BallLottery(lotteryName) {
    return new RegExp('7 ball', 'i').test(lotteryName);
  }

  private sortLotteryPrices(lotteryEntityPrices: ILottoPrice[]) {
    return _.sortBy(lotteryEntityPrices, 'numberPicks');
  }

  /**
   * return Lotteries packed in Lotto Entity
   *
   * @param lotteriesArray
   * @returns {{}}
   */
  private arrangeByLotto(lotteriesArray: ILotto[]): ILotteryMap {
    let lotteryName;
    let lottoType;
    let sortCode;
    let propName;
    const lottoMap = {};

    _.forEach(lotteriesArray, (lotteryEntity: ILotto) => {
      const shutAtTime = this.getShutAtTime(lotteryEntity);
      if (shutAtTime) {
        lotteryEntity.lotteryPrice = this.sortLotteryPrices(lotteryEntity.lotteryPrice);
        sortCode = this.filterSortCode(lotteryEntity.sort);
        lotteryName = lotteryEntity.description.replace(/\sLottery|\sLotto/g, '')
          .replace(/(\d\s(ball|Ball))/g, '');
        propName = this.filterLottoDesc(lotteryName);
        lottoType = !this.is7BallLottery(lotteryEntity.name) ? 'normal' : 'boosterBall';
        if (!_.has(lottoMap, propName)) {
          lottoMap[propName] = {
            limits: lotteryEntity.limits,
            active: false,
            name: lotteryName,
            sortCode: sortCode,
            description: lotteryEntity.description,
            country: lotteryEntity.country,
            uri: propName
          };
        }
        lottoMap[propName][lottoType] = lotteryEntity;
        lottoMap[propName][lottoType].shutAtTime = shutAtTime;
        if (!lottoMap[propName].shutAtTime ||
          Date.parse(lottoMap[propName].shutAtTime) > Date.parse(lottoMap[propName][lottoType].shutAtTime)) {
          lottoMap[propName].shutAtTime = lottoMap[propName][lottoType].shutAtTime;
        }
      }
    });
    this.activeLotto = _.min(_.toArray(lottoMap), (item: any) => Date.parse(item.shutAtTime)).uri;
    if (this.activeLotto) {
      lottoMap[this.activeLotto].active = true;
    }
    this.lotteryData = lottoMap;

    return lottoMap;
  }

  private filterSortCode(sortCode: string): string {
    const firstNumRegex = /(^\d.)/; // Match First Numbers: [0-9]
    const code = firstNumRegex.test(sortCode) ? sortCode.replace(/(\D)/g, '') :
      sortCode.replace(/(\d)/g, '');
    return `${code.toLowerCase()}-lotto`;
  }

  /**
   * filterLottoDesc (Example: |Lotto USA| ==> lotto-usa; |69s| ==> lotto-69s)
   * Filter lotto description (lottoData.description).
   */
  private filterLottoDesc(input: string): string {
    const symbolRegex = /[\|.,']/g; // Match Symbols: |.,',
    const spaceRegex = /\b(\s+)\b/g; // Match Spaces between words: ' '
    const firstNumRegex = /(^\d.)/; // Match First Numbers: [0-9]
    const text = input
        .replace(symbolRegex, '')
        .replace(spaceRegex, '-')
        .toLowerCase()
        .trim(); // |Lotto USA | ==> lotto-usa

    // add text 'lotto-' if first symbol is number in input
    return firstNumRegex.test(text) ? `lotto-${text}` : text;
  }
}
