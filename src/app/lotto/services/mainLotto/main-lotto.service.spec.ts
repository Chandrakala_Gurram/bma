import { of as observableOf,  Observable } from 'rxjs';
import { MainLottoService } from './main-lotto.service';
import { fakeAsync, tick } from '@angular/core/testing';

describe('MainLottoService', () => {
  let service: MainLottoService;

  let siteServerLottoService;
  let filterService;

  const lottoData = [
    {
      description: '49s Lotto',
      name: '49s 6 ball lotto',
      sort: '49S',
      country: 'UK',
      lotteryPrice: [
        { numberPicks: '3' },
        { numberPicks: '2' },
        { numberPicks: '1' }
        ],
      draw: [
        { shutAtTime: '2099-01-01' }
      ]
    },
    {
      description: '49s Lotto',
      name: '49s 7 ball lotto',
      sort: '49BS',
      country: 'UK',
      lotteryPrice: [
        { numberPicks: '2' },
        { numberPicks: '1' }
      ],
      draw: [
        { shutAtTime: '2099-01-01' }
      ]
    },
    {
      description: 'Italy Lotto',
      name: 'Italy 7 ball lotto',
      sort: 'IT7',
      country: 'Italy',
      lotteryPrice: [],
      draw: [
        { shutAtTime: '2010-01-01' }
      ]
    },
    {
      description: 'Italy Lotto',
      name: 'Italy 6 ball lotto',
      sort: 'IT6',
      country: 'Italy',
      lotteryPrice: [],
      draw: [
        { shutAtTime: '2099-01-01' }
      ]
    }
  ];

  const lottoResultData = {
    'lotto-49s': {
      active: true,
      boosterBall: {
        country: 'UK',
        description: '49s Lotto',
        draw: [{
          shutAtTime: '2099-01-01'
        }],
        lotteryPrice: [
          { numberPicks: '1' },
          { numberPicks: '2' }
        ],
        name: '49s 7 ball lotto',
        shutAtTime: '2099-01-01',
        sort: '49BS'
      },
      country: 'UK',
      description: '49s Lotto',
      limits: undefined,
      name: '49s',
      sortCode: '49-lotto',
      normal: {
        country: 'UK',
        description: '49s Lotto',
        draw: [{
          shutAtTime: '2099-01-01'
        }],
        lotteryPrice: [
          { numberPicks: '1' },
          { numberPicks: '2' },
          { numberPicks: '3' }
        ],
        name: '49s 6 ball lotto',
        shutAtTime: '2099-01-01',
        sort: '49S'
      },
      shutAtTime: '2099-01-01',
      uri: 'lotto-49s'
    },
    'italy': {
      active: false,
      country: 'Italy',
      description: 'Italy Lotto',
      sortCode: 'it-lotto',
      limits: undefined,
      name: 'Italy',
      normal: {
        country: 'Italy',
        description: 'Italy Lotto',
        draw: [{
          shutAtTime: '2099-01-01'
        }],
        lotteryPrice: [],
        name: 'Italy 6 ball lotto',
        shutAtTime: '2099-01-01',
        sort: 'IT6'
      },
      shutAtTime: '2099-01-01',
      uri: 'italy'
    }
  };

  beforeEach(() => {
    siteServerLottoService = {
      getLotteries: jasmine.createSpy().and.returnValue(observableOf(null))
    };

    filterService = {
      orderBy: jasmine.createSpy().and.returnValue([lottoResultData['lotto-49s'], lottoResultData['italy']])
    };

    service = new MainLottoService(
      siteServerLottoService,
      filterService
    );
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
    expect(service['lotteryData']).toEqual({});
  });

  describe('getLotteryData', () => {
    it('should return observable', () => {
      service['lotteryData']['a'] = {} as any;
      expect(service.getLotteryData('a')).toEqual(jasmine.any(Observable));

      service['activeLotto'] = 'a';
      expect(service.getLotteryData(null)).toEqual(jasmine.any(Observable));

      service['lotteryData'] = {};
      service['activeLotto'] = null;
      service.getLotteriesByLotto = jasmine.createSpy().and.returnValue(observableOf(null));
      expect(service['getLotteryData'](null)).toEqual(jasmine.any(Observable));
      expect(service.getLotteriesByLotto).toHaveBeenCalled();
    });

    it('should return active lotto from lotto map', fakeAsync(() => {
      const successHandler = jasmine.createSpy('successHandler');

      service['lotteryData'] = {};
      service.activeLotto = 'activeLotto';
      service.getLotteriesByLotto = jasmine.createSpy().and.returnValue(observableOf({activeLotto: {}}));

      service['getLotteryData']('').subscribe(successHandler);
      tick();

      expect(successHandler).toHaveBeenCalledWith({});
      expect(service.getLotteriesByLotto).toHaveBeenCalled();
    }));
  });

  describe('getLotteriesByLotto', () => {
    it('should return observable of lotteries', () => {
      expect(service.getLotteriesByLotto()).toEqual(jasmine.any(Observable));
      expect(siteServerLottoService.getLotteries).toHaveBeenCalled();
    });

    it('should ', fakeAsync(() => {
      const successHandler = jasmine.createSpy('successHandler');

      service['arrangeByLotto'] = jasmine.createSpy('arrangeByLotto');
      service.getLotteriesByLotto().subscribe(successHandler);
      tick();

      expect(successHandler).toHaveBeenCalled();
      expect(service['arrangeByLotto']).toHaveBeenCalled();
    }));
  });

  it('getLottoType', () => {
    expect(service.getLottoType({}, true)).toBe('boosterBall');
    expect(service.getLottoType({ id: 1 }, false)).toBe('boosterBall');
    expect(service.getLottoType({ normal: true }, false)).toBe('normal');
  });

  it('getShutAtTime', () => {
    let data = {
      draw: [
        { shutAtTime: '2099-09-30' },
        { shutAtTime: '2099-09-29' }
      ]
    };
    expect(service.getShutAtTime(data as any)).toBe('2099-09-29');

    data = {
      draw: [
        { shutAtTime: '2015-09-30' },
        { shutAtTime: '2016-09-29' }
      ]
    };
    expect(service.getShutAtTime(data as any)).toBeFalsy();
  });

  it('is7BallLottery', () => {
    expect(service['is7BallLottery']('Loto9')).toBeFalsy();
    expect(service['is7BallLottery']('7 BALL')).toBeTruthy();
    expect(service['is7BallLottery']('7 Ball')).toBeTruthy();
  });

  it('sortLotteryPrices', () => {
    const data = [
      { numberPicks: 'z' },
      { numberPicks: 'a' },
      { numberPicks: 'h' },
    ];
    const result = service['sortLotteryPrices'](data as any);
    expect(result).toEqual(jasmine.any(Array));
    expect(result[0].numberPicks).toBe('a');
    expect(result[1].numberPicks).toBe('h');
    expect(result[2].numberPicks).toBe('z');
  });

  describe('@arrangeByLotto', () => {
    it('should create lotto data', () => {
      const result = service['arrangeByLotto'](lottoData as any);
      expect(service.lotteryData).toEqual(lottoResultData as any);
      expect(result).toEqual(lottoResultData as any);
    });

    it('should not set active lotto', () => {
      const data = [
        {
          description: '49s Lotto',
          name: '49s 6 ball lotto',
          sort: '49S',
          country: 'UK',
          lotteryPrice: [
            { numberPicks: '3' },
            { numberPicks: '2' },
            { numberPicks: '1' }
          ],
          draw: [
            { shutAtTime: null }
          ]
        },
        {
          description: '49s Lotto',
          name: '49s 7 ball lotto',
          sort: '49BS',
          country: 'UK',
          lotteryPrice: [
            { numberPicks: '2' },
            { numberPicks: '1' }
          ],
          draw: [
            { shutAtTime: null }
          ]
        },
        {
          description: 'Italy Lotto',
          name: 'Italy 7 ball lotto',
          sort: 'IT7',
          country: 'Italy',
          lotteryPrice: [],
          draw: [
            { shutAtTime: null }
          ]
        },
        {
          description: 'Italy Lotto',
          name: 'Italy 6 ball lotto',
          sort: 'IT6',
          country: 'Italy',
          lotteryPrice: [],
          draw: [
            { shutAtTime: null }
          ]
        }
      ];
      const result = service['arrangeByLotto'](data as any);

      expect(service.lotteryData).toEqual({} as any);
      expect(result).toEqual({} as any);
    });
  });

  describe('@getMenuItems', () => {
    it('should create lotto menu items', () => {
      const result = service.getMenuItems(lottoResultData as any);
      expect(filterService.orderBy).toHaveBeenCalledWith(jasmine.any(Array), ['shutAtTime', 'name']);
      expect(result).toEqual([{
        imageTitle: '49s',
        inApp: true,
        svg: '49s',
        svgId: '49-lotto',
        targetUri: '/lotto/lotto-49s',
        targetUriCopy: 'lotto-49s',
        uri: 'lotto-49s'
      }, {
        imageTitle: 'Italy',
        inApp: true,
        svg: 'Italy',
        svgId: '49-lotto',
        targetUri: '/lotto/italy',
        targetUriCopy: 'italy',
        uri: 'italy'
      }]);
    });
  });
});
