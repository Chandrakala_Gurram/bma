import { Injectable } from '@angular/core';

import * as _ from 'underscore';
import environment from '@environment/oxygenEnvConfig';
import { ILottoResult } from '../../models/lotto-result.model';
import { ILotteriesConfig } from '../../models/lotteries-config.model';
import { ILotto } from '@app/lotto/models/lotto.model';

@Injectable()
export class BuildLotteriesService {

  LOTTERIES_CONFIG: ILotteriesConfig;

  constructor() {
    this.LOTTERIES_CONFIG = environment.LOTTERIES_CONFIG;
  }

  build(data): ILotto[] {
    return this.stripResponceFooter(data).filter(this.isNotExcluded.bind(this))
      .map(lotteryObject => {
        const lottery = lotteryObject.lottery;
        if (this.LOTTERIES_CONFIG[lottery.id]) {
          lottery.limits = _.has(this.LOTTERIES_CONFIG[lottery.id], 'limits')
            ? this.LOTTERIES_CONFIG[lottery.id].limits : false;
          return _.defaults(this.arrangeChildren(lotteryObject.lottery), { country: this.getCountry(lotteryObject.lottery) });
        }
      })
      .filter(lotteryEntity => _.has(lotteryEntity, 'draw'));
  }

  buildLottoResults(data): ILottoResult[] {
    return this.stripResponceFooter(data)
      .filter(this.isNotExcluded.bind(this))
      .map(lotteryObject => {
        lotteryObject.lottery.ballColor = this.LOTTERIES_CONFIG[lotteryObject.lottery.id].color;
        return this.arrangeChildren(lotteryObject.lottery);
      });
  }

  /**
   * Cutts responseFooter
   *
   * @param data
   * @returns {Array}
   */
  private stripResponceFooter(data) {
    const arr = data.SSResponse.children;
    arr.splice(-1, 1);
    return arr;
  }

  /**
   * Arranges lottery children to simpler view
   *
   * @param lotteryEntity
   * @returns {*}
   */
  private arrangeChildren(lotteryEntity) {
    let propKey;
    _.forEach(lotteryEntity.children, childEntity => {
      propKey = Object.keys(childEntity)[0];
      if (!_.has(lotteryEntity, propKey)) {
        lotteryEntity[propKey] = [];
      }
      lotteryEntity[propKey].push(childEntity[propKey]);
    });
    delete lotteryEntity.children;
    return lotteryEntity;
  }

  /**
   * Adds country to lottery
   *
   * @param lotteryEntity
   * @returns {*}
   */
  private getCountry(lotteryEntity) {
    return this.LOTTERIES_CONFIG[lotteryEntity.id].country;
  }

  /**
   * Checks if lottery is not excluded by config
   *
   * @param lotteryObject
   * @returns {boolean}
   */
  private isNotExcluded(lotteryObject) {
    const lotteryEntity = lotteryObject.lottery;
    return !_.has(this.LOTTERIES_CONFIG[lotteryEntity.id], 'excluded') ||
      (_.has(this.LOTTERIES_CONFIG[lotteryEntity.id], 'excluded') && !this.LOTTERIES_CONFIG[lotteryEntity.id].excluded);
  }

}
