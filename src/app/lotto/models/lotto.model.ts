export interface ILotteryMap {
  [key: string]: ILotto;
}

export interface ILotteryResultsMap {
  [key: string]: ILottoResultDraw;
}

export interface ILottoDraw {
  description: string;
  drawAtTime: string;
  drawDescriptionId: string;
  id: string;
  isActive: boolean;
  lotteryId: string;
  openAtTime: string;
  shutAtTime: string;
  sort: string;
  checked: boolean;
}

export interface ILottoPrice {
  id: string;
  lotteryId: string;
  numberCorrect: string;
  numberPicks: string;
  priceDen: number;
  priceNum: number;
}

export interface ILotto {
  country: string;
  description: string;
  draw: ILottoDraw[];
  hasOpenDraw: boolean;
  id: string;
  limits: number;
  lotteryPrice: ILottoPrice[];
  maxLines: string;
  maxNumber: string;
  maxPicks: number;
  minNumber: string;
  minPicks: string;
  name: string;
  siteChannels: string;
  sort: string;
  resultedDraw?: ILottoResultDraw[];
  normal: ILottoType;
  boosterBall: ILottoType;
  uri: string;
  shutAtTime: string;
}

export interface ILottoType {
  name: string;
}

export interface ILottonMenuItem {
  imageTitle: string;
  uri: string;
  svg: string;
  svgId: string;
  inApp: boolean;
  targetUri: string;
  targetUriCopy: string;
}

export interface ILottoResultDraw extends ILotto {
  sortDate: string;
  resultedDraw: any;
  ballColor: string;
  results: string;

  page?: number;
}

export interface ILottoPlaceBetObj {
  name: string;
  selectionName: string;
  selections: string;
  draws: ILottoDraw[];
  multiplier: number;
  amount: number;
  odds: ILottoPrice;
  currency: string;
}

export interface ILottoTab {
  title: string;
  name: string;
  hidden: boolean;
  id: number;
  url: string;
}
