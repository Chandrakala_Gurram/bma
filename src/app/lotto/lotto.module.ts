import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'angular-custom-modal';

import { SharedModule } from '@sharedModule/shared.module';

import { SimpleFiltersService } from '@ss/services/simple-filters.service';
import { LottoRoutingModule } from './lotto-routing.module';
import { LottoResultsComponent } from './components/lottoResults/lotto-results.component';
import { LottoMainComponent } from './components/lottoMain/lotto-main.component';
import { LottoReceiptComponent } from './components/lottoReceipt/lotto-receipt.component';
import { LottoResultsPageComponent } from './components/lottoResultsPage/lotto-results-page.component';
import { LottoResultsFilterPageComponent } from './components/lottoResulsFilterPage/lotto-results-filter-page.component';
import { LottoSegmentPageComponent } from '@lottoModule/components/lottoSegmentPage/lotto-segment-page.component';
import { NumberSelectorComponent } from './components/numberSelector/number-selector.component';
import { LottoNumberSelectorComponent } from './components/lottoNumberSelectorDialog/lotto-number-selector-dialog.component';
import { SiteServerLottoService } from './services/siteServerLotto/site-server-lotto.service';
import { BuildLotteriesService } from './services/buildLotteries/build-lotteries.service';
import { LottoReceiptService } from './services/lottoReceipt/lotto-receipt.service';
import { LottoBetService } from './services/lottoBet/lotto-bet.service';
import { LottoResultsService } from './services/lottoResults/lotto-results.service';
import { MainLottoService } from './services/mainLotto/main-lotto.service';
import { SegmentDataUpdateService } from './services/segmentDataUpdate/segment-data-update.service';
import { BannersModule } from '@banners/banners.module';

@NgModule({
  declarations: [
    LottoMainComponent,
    LottoResultsComponent,
    LottoReceiptComponent,
    LottoResultsPageComponent,
    LottoResultsFilterPageComponent,
    LottoSegmentPageComponent,
    NumberSelectorComponent,
    LottoNumberSelectorComponent
  ],
  imports: [
    ModalModule,
    SharedModule,
    FormsModule,
    LottoRoutingModule,
    BannersModule
  ],
  exports: [
    LottoMainComponent,
    LottoResultsComponent,
    LottoReceiptComponent,
    LottoResultsPageComponent,
    LottoResultsFilterPageComponent,
    LottoSegmentPageComponent,
    NumberSelectorComponent,
    LottoNumberSelectorComponent
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  entryComponents: [
    LottoMainComponent,
    LottoResultsComponent,
    LottoReceiptComponent,
    LottoResultsPageComponent,
    LottoResultsFilterPageComponent,
    LottoSegmentPageComponent,
    NumberSelectorComponent,
    LottoNumberSelectorComponent
  ],
  providers: [
    BuildLotteriesService,
    SiteServerLottoService,
    MainLottoService,
    LottoReceiptService,
    LottoBetService,
    LottoResultsService,
    SegmentDataUpdateService,
    SimpleFiltersService
  ],
})
export class LottoModule {}
