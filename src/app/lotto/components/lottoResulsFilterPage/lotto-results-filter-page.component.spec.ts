import { LottoResultsFilterPageComponent } from '@app/lotto/components/lottoResulsFilterPage/lotto-results-filter-page.component';
import { EMPTY, of } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';

describe('LottoResultsFilterPageComponent', () => {
  let component: LottoResultsFilterPageComponent;
  let location: any;
  let lottoResultsService: any;
  let route: any;

  beforeEach(() => {
    location = {
      path: () => 'some/path'
    };

    lottoResultsService = {
      getLottoResults: jasmine.createSpy('getLottoResults').and.returnValue(of({})),
      getLottoResultsById: jasmine.createSpy('getLottoResultsById'),
      loadMoreByDate: jasmine.createSpy('loadMoreByDate'),
      getLottoResultsByLotto: jasmine.createSpy('getLottoResultsByLotto'),
      getLottoResultsByTime: jasmine.createSpy('getLottoResultsByTime')
    };

    route = {
      params: of({a: 112})
    };

    component = new LottoResultsFilterPageComponent(
      location,
      lottoResultsService,
      route
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should return result', fakeAsync(() => {
      spyOn<any>(component, 'init');
      spyOn<any>(component, 'hideSpinner');
      spyOn<any>(component, 'showSpinner');

      lottoResultsService.getLottoResults.and.returnValue({a: 1});

      component.ngOnInit();

      tick();
      expect(component['showSpinner']).toHaveBeenCalled();
      expect(component['hideSpinner']).toHaveBeenCalled();
      expect(lottoResultsService.getLottoResults).toHaveBeenCalled();
    }));

    it('should subscribe to route params',  fakeAsync(() => {
      component['init'] = jasmine.createSpy('init');

      component.ngOnInit();
      tick(200);

      expect(component.lottoData).toEqual({});
      expect(component['init']).toHaveBeenCalled();
      expect(component.isResultsLoading).toBeFalsy();
      expect(component.state.loading).toBeFalsy();
    }));
  });

  describe('loadMoreByLotto', () => {
    it('should call getLottoResultsById', () => {
      lottoResultsService.getLottoResultsById.and.returnValue(EMPTY);
      component.loadMoreByLotto({page: 1, ids: 'sdfsd'} as any);

      expect(lottoResultsService.getLottoResultsById).toHaveBeenCalled();
    });

    it('should set loading to false', () => {
      lottoResultsService.getLottoResultsById.and.returnValue(of({}));
      component.loadMoreByLotto({page: 1, ids: 'sdfsd'} as any);

      expect(component.isResultsLoading).toBeFalsy();
    });
  });

  describe('loadMoreByDate', () => {
    it('should increment page number', () => {
      lottoResultsService.getLottoResults.and.returnValue(EMPTY);

      component['lottoData'] = [{page: 1}] as any;

      component.loadMoreByDate();
      expect(component['lottoData'][0].page as any).toEqual(2);
    });

    it('should update lotto results', () => {
      component.loadMoreByDate();

      expect(component.isResultsLoading).toBeFalsy();
      expect(lottoResultsService.getLottoResultsByTime).toHaveBeenCalled();
    });
  });

  it('lottoResultsArr', () => {
    this.lottoResults = {};

    component['lottoData'] = [{page: 1}] as any;

    expect(component.lottoResultsArr.length).not.toBeUndefined();
  });

  it('init', () => {
    spyOn<any>(component, 'setFilter');

    component['lottoData'] = [{page: 1}] as any;
    component['init']();

    expect(component['setFilter']).toHaveBeenCalled();
  });

  describe('setFilter', () => {
    it('should call getLottoResultsByLotto' , () => {
      component['setFilter']();

      expect(lottoResultsService.getLottoResultsByLotto).toHaveBeenCalled();
    });

    it('should call getLottoResultsByTime' , () => {
      component['location'] = {
        path: () => 'some/path/byTime'
      } as any;

      component['setFilter']();

      expect(lottoResultsService.getLottoResultsByTime).not.toHaveBeenCalled();
    });

    it('should set filter "byTime"',  () => {
      component['location'] = {
        path: () => 'some/path/set/byTime'
      } as any;

      component['setFilter']();

      expect(lottoResultsService.getLottoResultsByTime).toHaveBeenCalled();
      expect(component.filter).toEqual('byTime');
    });
  });
});
