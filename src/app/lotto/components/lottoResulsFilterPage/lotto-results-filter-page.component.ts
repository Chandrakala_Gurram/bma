import { concatMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import * as _ from 'underscore';

import { LottoResultsService } from '../../services/lottoResults/lotto-results.service';
import { ILotteryResultsMap, ILottoResultDraw } from '../../models/lotto.model';
import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';

@Component({
  selector: 'lotto-results-filter-page',
  templateUrl: './lotto-results-filter-page.component.html'
})
export class LottoResultsFilterPageComponent extends AbstractOutletComponent implements OnInit {
  lottoData: ILotteryResultsMap;
  filter: string;
  lottoResults: ILotteryResultsMap;
  isResultsLoading: boolean;

  constructor(
    private location: Location,
    private lottoResultsService: LottoResultsService,
    private route: ActivatedRoute
  ) {
    super()/* istanbul ignore next */;
  }

  ngOnInit(): void {
    this.isResultsLoading = true;

    this.route.params.pipe(
      concatMap(() => {
        this.showSpinner();
        return this.lottoResultsService.getLottoResults(this.isResultsLoading);
      }))
      .subscribe((lottoData: ILotteryResultsMap) => {
        this.lottoData = lottoData;
        this.init();
        this.isResultsLoading = false;
        this.hideSpinner();
      }, () => {
        this.showError();
      });
  }

  loadMoreByLotto(lotto: { ids: string; page: number; }): void {
    this.isResultsLoading = true;
    lotto.page += 1;
    this.lottoResultsService.getLottoResultsById({ lottoIds: lotto.ids.split(','), page: lotto.page })
      .subscribe(() => {
        this.isResultsLoading = false;
      });
  }

  loadMoreByDate(): void {
    this.isResultsLoading = true;
    (this.lottoResultsService.getLottoResults(true))
      .subscribe(() => {
        this.lottoResults = this.lottoResultsService.getLottoResultsByTime();
        this.isResultsLoading = false;
      });

    _.each(this.lottoData, lotto => {
      lotto.page += 1;
    });
  }

  get lottoResultsArr(): ILottoResultDraw[] {
    return _.toArray(this.lottoResults);
  }

  private init(): void {
    this.setFilter();
  }

  private setFilter(): void {
    if (this.location.path().split('/')[3] === 'byTime') {
      this.lottoResults = this.lottoResultsService.getLottoResultsByTime();
      this.filter = 'byTime';
    } else {
      this.lottoResults = this.lottoResultsService.getLottoResultsByLotto();
      this.filter = 'byLotto';
    }
  }
}
