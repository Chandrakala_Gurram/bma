import { of as observableOf, throwError } from 'rxjs';

import { LottoMainComponent } from './lotto-main.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('LottoMainComponent', () => {
  let component: LottoMainComponent;

  let lottoService;
  let userService;
  let cmsService;
  let changeDetector;
  let pubSubService;
  let userBalanceUpdCb;
  let messageUpdateCb;
  let menuUpdateCb;

  beforeEach(() => {
    lottoService = {
      getLotteriesByLotto: jasmine.createSpy().and.returnValue(observableOf(null)),
      getMenuItems: jasmine.createSpy()
    };

    changeDetector = {
      detectChanges: jasmine.createSpy()
    };

    userService = {
      currencySymbol: '$',
      status: true,
      currency: 'USD',
      oddsFormat: 'anc',
      sportBalance: 99
    };

    cmsService = {
      getItemSvg: jasmine.createSpy().and.returnValue(observableOf({}))
    };

    pubSubService = {
      publish: jasmine.createSpy('publish'),
      subscribe: jasmine.createSpy('subscribe').and.callFake((file, method, callback) => {
        if (method === 'USER_BALANCE_UPD') {
          userBalanceUpdCb = callback;
        } else if (method === 'MSG_UPDATE') {
          messageUpdateCb = callback;
        } else if (method === 'MENU_UPDATE') {
          menuUpdateCb = callback;
        }
      }),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: pubSubApi
    };

    component = new LottoMainComponent(
      lottoService,
      userService,
      cmsService,
      changeDetector,
      pubSubService
    );
  });

  it('constructor', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit',  () => {
    it('should get lotteries by lotto', () => {
      component['init'] = () => {};
      component.ngOnInit();
      expect(lottoService.getLotteriesByLotto).toHaveBeenCalled();
    });

    it('should show error', () => {
      component['init'] = jasmine.createSpy('init');
      lottoService.getLotteriesByLotto = jasmine.createSpy('getLotteriesByLotto').and.returnValue(throwError('error'));

      component.ngOnInit();

      expect(component.state.error).toBeTruthy();
      expect(component.state.loading).toBeFalsy();
      expect(component['init']).not.toHaveBeenCalled();
    });
  });

  it('ngOnDestroy', () => {
    component.ngOnDestroy();
    expect(pubSubService.unsubscribe).toHaveBeenCalledWith('lottoMainCtrl');
  });

  describe('init', () => {
    it('init', () => {
      component.lottoData = {};
      component['getSession'] = jasmine.createSpy('getSession');
      component['init']();
      menuUpdateCb('lotto-69');

      expect(cmsService.getItemSvg).toHaveBeenCalledWith('Lotto');
      expect(component['getSession']).toHaveBeenCalled();
      expect(pubSubService.subscribe).toHaveBeenCalledWith('lottoMainCtrl', 'USER_BALANCE_UPD', jasmine.any(Function));
      expect(lottoService.getMenuItems).toHaveBeenCalledWith(component.lottoData);
      expect(pubSubService.subscribe).toHaveBeenCalledWith('lottoMainCtrl', 'MSG_UPDATE', jasmine.any(Function));
      expect(pubSubService.subscribe).toHaveBeenCalledWith('lottoMainCtrl', 'MENU_UPDATE', jasmine.any(Function));
      expect(changeDetector.detectChanges).toHaveBeenCalled();
      expect(component.activeUrl).toBe('lotto-69');
    });

    it('should get session data',  () => {
      component['getSession'] = jasmine.createSpy('getSession');
      component['init']();
      userBalanceUpdCb();

      expect(component['getSession']).toHaveBeenCalled();
    });

    it('should set lotto message',  () => {
      const message = { msg: 'msg', type: 'type'};
      component['init']();
      messageUpdateCb(message);

      expect(component.lottoMessage).toEqual(message);
      expect(changeDetector.detectChanges).toHaveBeenCalled();
    });
  });

  it('getSession', () => {
    component['getSession']();
    expect(component.currencySymbol).toBe(userService.currencySymbol);
    expect(component.sessionStatus).toBe(userService.status);
    expect(component.currency).toBe(userService.currency);
    expect(component.oddsFormat).toBe(userService.oddsFormat);
    expect(component.userBalance).toBe(userService.sportBalance);
  });
});
