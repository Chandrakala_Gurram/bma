import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lotto-results',
  templateUrl: './lotto-results.component.html'
})
export class LottoResultsComponent implements OnInit {

  @Input() draw;
  @Input() filter;

  constructor() { }

  ngOnInit(): void { }

}
