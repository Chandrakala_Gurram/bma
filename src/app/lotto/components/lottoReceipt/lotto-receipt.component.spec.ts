import { LottoReceiptComponent } from './lotto-receipt.component';

describe('LottoReceiptComponent', () => {
  let router;
  let windowRefService;
  let fracToDecService;
  let userService;
  let lottoReceiptService;
  let component: LottoReceiptComponent;

  beforeEach(() => {
    router = {
      navigate: jasmine.createSpy('navigate')
    };
    windowRefService = {
      nativeWindow: {
        history: {
          back: jasmine.createSpy('back')
        }
      }
    };
    fracToDecService = {
      getDecimal: jasmine.createSpy('getDecimal')
    };
    userService = {};
    lottoReceiptService = {
      getReceipt: jasmine.createSpy('getReceipt')
    };

    component = new LottoReceiptComponent(
      router,
      windowRefService,
      fracToDecService,
      userService,
      lottoReceiptService
    );
  });

  describe('ngOnInit', () => {
    it('should redirect to lotto page', () => {
      component.ngOnInit();
      expect(router.navigate).toHaveBeenCalledWith(['/lotto']);
    });

    it('should get receipt details', () => {
      lottoReceiptService.receiptData = { name: '123', betOdds: {} };
      lottoReceiptService.getReceipt.and.returnValue(lottoReceiptService.receiptData);

      userService.oddsFormat = 'frac';
      component.ngOnInit();

      userService.oddsFormat = 'dec';
      component.ngOnInit();

      expect(lottoReceiptService.getReceipt).toHaveBeenCalledTimes(2);
      expect(fracToDecService.getDecimal).toHaveBeenCalledTimes(1);
    });
  });

  it('redirectToPrevPage', () => {
    component.redirectToPrevPage();
    expect(windowRefService.nativeWindow.history.back).toHaveBeenCalledTimes(1);
  });
});
