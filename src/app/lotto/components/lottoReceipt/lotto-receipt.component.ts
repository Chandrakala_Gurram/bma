import { Component, OnInit } from '@angular/core';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { Router } from '@angular/router';

import { FracToDecService } from '@core/services/fracToDec/frac-to-dec.service';
import { UserService } from '@core/services/user/user.service';
import { ILottoResult } from '../../models/lotto-result.model';
import { LottoReceiptService } from '@app/lotto/services/lottoReceipt/lotto-receipt.service';

@Component({
  selector: 'lotto-receipt',
  templateUrl: './lotto-receipt.component.html'
})
export class LottoReceiptComponent implements OnInit {

  receipt: ILottoResult;
  prices: string;
  potentialReturns: number;

  constructor(
    private router: Router,
    private windowRef: WindowRefService,
    private fracToDecService: FracToDecService,
    private user: UserService,
    private lottoReceiptService: LottoReceiptService,
  ) { }

  ngOnInit(): void {
    if (!this.canShowReceipt()) {
      this.router.navigate(['/lotto']);
    } else {
      this.receipt = this.lottoReceiptService.getReceipt();

      this.prices = this.user.oddsFormat === 'frac' ?
        `${this.receipt.betOdds.priceNum}/${this.receipt.betOdds.priceDen}`
        : <string>this.fracToDecService.getDecimal(this.receipt.betOdds.priceNum, this.receipt.betOdds.priceDen);
      this.potentialReturns =
        (this.receipt.betStake * this.receipt.betOdds.priceNum / this.receipt.betOdds.priceDen) +
        Number(this.receipt.betStake);
    }
  }

  redirectToPrevPage(): void {
    this.windowRef.nativeWindow.history.back();
  }

  private canShowReceipt(): boolean {
    const data = this.lottoReceiptService.receiptData;
    return data && Object.keys(data).length > 0;
  }
}
