import { LottoNumberSelectorComponent } from '@app/lotto/components/lottoNumberSelectorDialog/lotto-number-selector-dialog.component';
import { EMPTY, of } from 'rxjs';

describe('LottoNumberSelectorComponent', () => {
  let component: LottoNumberSelectorComponent;
  let deviceService: any;
  let segmentDataUpdateService: any;
  let filterService: any;
  let callbackFn: any;
  let windowRef;

  beforeEach(() => {
    deviceService = {};
    windowRef = {};

    segmentDataUpdateService = {
      changes: {
        subscribe: jasmine.createSpy('subscribe').and.callFake((callback) => {
          callbackFn = callback;
        })
      }
    };

    filterService = {getComplexTranslation: jasmine.createSpy('getComplexTranslation')};

    component = new LottoNumberSelectorComponent(
      deviceService,
      segmentDataUpdateService,
      filterService,
      windowRef
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    let data: any;

    beforeEach(() => {
      component.params = {};
      component.dialog = { close: {bind: () => {}}, onKeyDownHandler: {bind: () => {}} };
      data = of({ numbersSelected: 12, numbersData: 33 });
    });

    it('should not set params', () => {
      component['segmentDataUpdateService'] = {
        changes: EMPTY
      } as any;

      component.ngOnInit();

      expect(component.params.numbersSelected).not.toEqual(12);
    });

    it('should set params', () => {
      component['segmentDataUpdateService'] = {
        changes: data
      } as any;

      component.ngOnInit();

      expect(component.params.numbersSelected).toEqual(12);
    });

    it('should not set numbersSelected and numberData', () => {
      component.params = {};
      component.ngOnInit();
      callbackFn();

      expect(component.params).toEqual({});
    });
  });

  it('getDipTranlationsl', () => {
    component.getDipTranlations('12');

    expect(filterService.getComplexTranslation).toHaveBeenCalledWith('lotto.lucky', '%num', '12');
  });

  it('isSelected', () => {
    component.params = {numbersData: [{selected: true}]} as any;

    expect(component.isSelected).toBeTruthy();
  });
});
