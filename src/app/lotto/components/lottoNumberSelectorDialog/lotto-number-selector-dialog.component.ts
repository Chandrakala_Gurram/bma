import { Component, OnInit, ViewChild } from '@angular/core';

import { ILottoChangeEvent } from '@app/lotto/models/lotteries-config.model';
import { AbstractDialog } from '@shared/components/oxygenDialogs/abstract-dialog';
import { DeviceService } from '@core/services/device/device.service';
import { SegmentDataUpdateService } from '@app/lotto/services/segmentDataUpdate/segment-data-update.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

@Component({
  selector: 'lotto-number-selector-dialog',
  templateUrl: './lotto-number-selector-dialog.component.html',
  styleUrls: ['lotto-number-selector-dialog.component.less']
})
export class LottoNumberSelectorComponent extends AbstractDialog implements OnInit {

  @ViewChild('lottoNumberSelector') dialog;

  constructor(
    device: DeviceService,
    private segmentDataUpdateService: SegmentDataUpdateService,
    private filterService: FiltersService,
    windowRef: WindowRefService
  ) {
    super(device, windowRef);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.segmentDataUpdateService.changes.subscribe((data: ILottoChangeEvent) => {
      if (data && this.params) {
        this.params.numbersSelected = data.numbersSelected;
        this.params.numbersData = data.numbersData;
      }
    });
  }

  getDipTranlations(num: string): string {
    return this.filterService.getComplexTranslation('lotto.lucky', '%num', num);
  }

  get isSelected(): boolean {
    return this.params && this.params.numbersData.filter(num => num.selected).length > 0;
  }
}
