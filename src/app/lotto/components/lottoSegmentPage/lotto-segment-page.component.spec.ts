import { throwError, of } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';
import * as _ from 'underscore';
import { LottoSegmentPageComponent } from './lotto-segment-page.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';
import { NavigationStart, NavigationEnd } from '@angular/router';

describe('LottoSegmentPageComponent', () => {
  let component: LottoSegmentPageComponent;

  let filterService;
  let locale;
  let lottoService;
  let lottoBetService;
  let fracToDecService;
  let betPlacementErrorTracking;
  let storage;
  let location;
  let datePipe;
  let route;
  let router;
  let user;
  let segmentDataUpdateService;
  let routeChangeListener;
  let accountUpgradeLinkService;
  let windowRefService;
  let pubSubService;
  let changeDetectorRef;

  const currentLotto = <any>{
    normal: { lotteryPrice: []}, boosterBall: { lotteryPrice: []}, uri: 'test_uri', lotteryPrice: 'test_price'
  };

  beforeEach(fakeAsync(() => {
    filterService = {
      getComplexTranslation: jasmine.createSpy('getComplexTranslation'),
      setCurrency: jasmine.createSpy('setCurrency').and.returnValue('setCurrency'),
      filterLink: jasmine.createSpy('filterLink').and.returnValue('test'),
      orderBy: jasmine.createSpy('orderBy').and.returnValue([])
    };
    locale = {
      getString: jasmine.createSpy('getString').and.returnValue('test_string')
    };
    lottoService = {
      getShutAtTime: jasmine.createSpy('getShutAtTime').and.returnValue([]),
      getMenuItems: jasmine.createSpy('getMenuItems').and.returnValue([]),
      getLotteryData: jasmine.createSpy('getLotteryData').and.returnValue(of({})),
      getLotteriesByLotto: jasmine.createSpy('getLotteriesByLotto').and.returnValue(of(<any>[{ active: true, uri: 'test'}]))
    };
    lottoBetService = {
      openReceipt: jasmine.createSpy('getShutAtTime'),
      placeBet: jasmine.createSpy('placeBet').and.returnValue(of({}))
    };
    fracToDecService = {
      getDecimal: jasmine.createSpy('getDecimal')
    };
    betPlacementErrorTracking = {
      sendLotto: jasmine.createSpy('sendLotto')
    };
    storage = {
      get: jasmine.createSpy('get'),
      set: jasmine.createSpy('set'),
      remove: jasmine.createSpy('remove')
    };
    location = {
      path: jasmine.createSpy('path').and.returnValue('test')
    };
    datePipe = {
      transform: jasmine.createSpy('transform')
    };
    route = {
      params: of([])
    };
    user = {
      currencySymbol: 'GBP',
      oddsFormat: 'frac',
      sportBalance: 0,
      isInShopUser: jasmine.createSpy().and.returnValue(false)
    };
    segmentDataUpdateService = {
      changes: {
        next: jasmine.createSpy('next')
      }
    };
    routeChangeListener = {
      unsubscribe: jasmine.createSpy('unsubscribe')
    };
    router = {
      navigateByUrl: jasmine.createSpy('navigateByUrl'),
      navigate: jasmine.createSpy('navigate'),
      url: 'test_url',
      events: {
        subscribe: jasmine.createSpy('subscribe').and.returnValue(routeChangeListener)
      }
    };
    pubSubService = {
      publish: jasmine.createSpy('publish'),
      subscribe: jasmine.createSpy('subscribe'),
      API: pubSubApi
    };

    accountUpgradeLinkService = {
      inShopToMultiChannelLink: 'URL_TO_REDIRECT'
    };
    windowRefService = {
      nativeWindow: {
        location: {
          href: ''
        }
      }
    };
    changeDetectorRef = {
      detectChanges: jasmine.createSpy('detectChanges')
    };

    createComp();
  }));

  function createComp() {
    component = new LottoSegmentPageComponent(
      filterService,
      locale, lottoService, lottoBetService, fracToDecService, betPlacementErrorTracking,
      storage, location, datePipe, route, router, user, segmentDataUpdateService,
      pubSubService, accountUpgradeLinkService, windowRefService, changeDetectorRef
    );

    component.currentLotto = <any>{
      name: 'test_name',
      boosterBall: {
        name: 'test_name1'
      },
      normal: {
        name: 'test_name2'
      }
    };
    component.activeMenuItem = <any>{
      uri: 'test_uri'
    };
    component.lotteryData = <any>{
      name: 'test_name',
    };
    component.showDays = 123;
    component['routeChangeListener'] = routeChangeListener;
  }

  it('ngOnInit: no data', fakeAsync(() => {
    component.ngOnInit();
    tick();
    expect(router.navigate).toHaveBeenCalledWith(['/404']);
  }));

  it('ngOnInit: with data', fakeAsync(() => {
    lottoService.getLotteryData = jasmine.createSpy('getLotteryData').and.returnValue(of({ uri: 'test_uri'}));
    createComp();

    component['initLotto'] = jasmine.createSpy('initLotto');
    component.hideSpinner = jasmine.createSpy('hideSpinner');

    component.ngOnInit();
    tick();

    expect(component.lotteryData.uri).toEqual('test_uri');
    expect(component['initLotto']).toHaveBeenCalled();
    expect(component.hideSpinner).toHaveBeenCalled();
    expect(router.navigate).not.toHaveBeenCalled();
  }));

  it('ngOnInit: error', fakeAsync(() => {
    lottoService.getLotteryData = jasmine.createSpy('getLotteryData').and.returnValue(throwError([]));
    createComp();
    component.showError = jasmine.createSpy('showError');
    component.ngOnInit();

    tick();
    expect(component.showError).toHaveBeenCalled();
  }));

  it('ngOnDestroy', () => {
    component['routeChangeListener'] = <any>{
      unsubscribe: jasmine.createSpy()
    };
    component.timeInterval = setInterval(() => {}, 1000);
    component.ngOnDestroy();
    expect(component.timeInterval).toEqual(jasmine.any(Number));
    expect(component['routeChangeListener'].unsubscribe).toHaveBeenCalled();
  });

  it('getDipTranslations', () => {
    component.getDipTranslations(2);
    expect(filterService.getComplexTranslation).toHaveBeenCalledWith('lotto.lucky', '%num', '2');
  });

  it('setLotto', () => {
    component['setLottoTabs'] = jasmine.createSpy('setLottoTabs');
    component['getSelected'] = jasmine.createSpy('getSelected');
    component['initializeTimer'] = jasmine.createSpy('initializeTimer');
    spyOn(component, 'drawsShow');
    spyOn(component, 'updateDraws');
    spyOn(component, 'setBallNumbers');

    component.setLotto(currentLotto, true);

    expect(component.boosterBall).toEqual(true);
    expect(component.currentLotto).toEqual(_.extend(currentLotto, { shutAtTime: [] }));
    expect(component.currentLottery.lotteryPrice).toEqual([]);
    expect(component.activeMenuItem.uri).toEqual(currentLotto.uri);
    expect(lottoService.getShutAtTime).toHaveBeenCalledWith({ lotteryPrice: [] });
    expect(component.drawsShow).toHaveBeenCalledWith(component.showDays);
    expect(component.updateDraws).toHaveBeenCalled();
    expect(component.setBallNumbers).toHaveBeenCalled();
    expect(component['setLottoTabs']).toHaveBeenCalled();
    expect(component['getSelected']).toHaveBeenCalled();
    expect(component['initializeTimer']).toHaveBeenCalledWith(currentLotto.shutAtTime);
    expect(pubSubService.publish).toHaveBeenCalledWith('ballsUpdate');

    component.setLotto({ normal: currentLotto.normal, boosterBall: currentLotto.boosterBall }, null);
    expect(component.lotteryPrice).toEqual(component.currentLottery.lotteryPrice);
    expect(component.currentLottery).toEqual(currentLotto.normal);
    component.setLotto({ boosterBall: currentLotto.boosterBall }, null);
    expect(component.currentLottery).toEqual(currentLotto.boosterBall);
  });

  describe('drawsShow', () => {
    it('drawsShow', fakeAsync(() => {
      filterService.orderBy = jasmine.createSpy().and.returnValue([{ id: 1, shutAtTime: '10-10-2030' }]);
      storage.get = jasmine.createSpy('get').and.returnValue([{}]);
      createComp();
      component.currentLottery = <any>{
        draw: [{}, {}, {}],
        limits: 3
      };

      component.drawsShow(10000);
      expect(component.limitValue).toBe(3);
      expect(component.orderedDraws).toEqual(<any>[{
        id: 1,
        shutAtTime: '10-10-2030',
        checked: true
      }]);
      expect(filterService.orderBy).toHaveBeenCalledWith(component.currentLottery.draw, ['drawAtTime', 'description']);
      expect(storage.get).toHaveBeenCalledWith('test_nameDraw');
    }));

    it('should set ordered draws', function () {
      filterService.orderBy = jasmine.createSpy().and.returnValue([
        {id: 1, shutAtTime: '10-10-2030'}, {id: 2, shutAtTime: '10-10-2030'}, {id: 3, shutAtTime: '10-10-2019'}
        ]);
      storage.get = jasmine.createSpy('get').and.returnValue([
        {id: 1, shutAtTime: '10-10-2030'}, {id: 2, shutAtTime: '10-10-2030'}, {id: 3, shutAtTime: '10-10-2019'}
        ]);
      component.orderedDraws = [{id: 1}] as any;
      component.currentLottery = <any>{
        draw: [{}, {}, {}],
        limits: null
      };

      component.drawsShow(10000);

      expect(component.orderedDraws).toEqual([
        {id: 1, shutAtTime: '10-10-2030', checked: true}, {id: 2, shutAtTime: '10-10-2030'}, {id: 3, shutAtTime: '10-10-2019'}
        ] as any);
    });
  });

  describe('updateDraws: ', () => {
    it('checked', () => {
      component.orderedDraws = <any>[{ checked: true }, { checked: true }];
      component.updateDraws();

      expect(storage.set).toHaveBeenCalledWith('test_nameDraw', component.orderedDraws);
      expect(_.pluck(component.draws, 'checked').length).toBe(2);
      expect(component.drawMultiplier).toEqual(2);
    });

    it('unchecked', () => {
      component.orderedDraws = <any>[{}, {}];
      component.updateDraws();

      expect(_.pluck(component.draws, 'checked').length).toBe(0);
      expect(component.drawMultiplier).toEqual(1);
    });
  });

  describe('setBallNumbers', () => {
    it('setBallNumbers: ball selection not stored', () => {
      spyOn(component, 'disabledBalls');
      component['setSelectedBallNumbers'] = jasmine.createSpy('setSelectedBallNumbers');
      component.currentLottery = <any>{
        maxNumber: 1
      };
      component.setBallNumbers();

      expect(component.ballSelected).toEqual([]);
      expect(component.numbersData[0]).toEqual({
        disabled: false,
        selected: false,
        value: 1
      });
      expect(component.selected).not.toBeTruthy();
      expect(component.disabledBalls).not.toHaveBeenCalled();
      expect(storage.get).toHaveBeenCalledWith(component.lotteryData.name);
      expect(component['setSelectedBallNumbers']).toHaveBeenCalledWith([]);
    });

    it('setBallNumbers: ball selection stored', fakeAsync(() => {
      storage.get = jasmine.createSpy('get').and.returnValue([{}]);
      createComp();
      component['setSelectedBallNumbers'] = jasmine.createSpy('setSelectedBallNumbers');
      component.currentLottery = <any>{
        maxNumber: 1
      };
      spyOn(component, 'disabledBalls');
      tick();

      component.setBallNumbers();

      expect(component.ballSelected.length).toEqual(1);
      expect(component.numbersData[0]).toEqual({
        disabled: false,
        selected: false,
        value: 1
      });
      expect(component.selected).toBeTruthy();
      expect(component.disabledBalls).toHaveBeenCalled();
      expect(storage.get).toHaveBeenCalledWith(component.lotteryData.name);
      expect(component['setSelectedBallNumbers']).toHaveBeenCalledWith([{}]);
    }));
  });

  describe('disabledBalls', () => {
    it('should set disable some balls', () => {
      component.currentLottery = <any>{ maxPicks: 2 };
      component.numbersData = <any>[ { selected: true }, { selected: false }, { selected: true }];
      component.disabledBalls();

      expect(component.selected).toBe(2);
      expect(_.pluck(component.numbersData, 'disabled')).toEqual([false, true, false]);
    });

    it('should set disable all balls', () => {
      component.currentLottery = <any>{ maxPicks: 3 };
      component.numbersData = <any>[ { selected: true }, { selected: false }, { selected: true }];
      component.disabledBalls();

      expect(_.pluck(component.numbersData, 'disabled')).toEqual([false, false, false]);
    });
  });

  it('resetDraws', () => {
    component.orderedDraws = <any>[{ checked: true }, { checked: true }];
    component.resetDraws();

    expect(component.orderedDraws[0].checked).toBeFalsy();
    expect(storage.remove).toHaveBeenCalledWith('test_nameDraw');
  });

  describe('selectNumbers', () => {
    it(' while balls are enabled', () => {
      component.numbersData = <any>[{ selected: true, disabled: true }];
      spyOn(component, 'disabledBalls');

      component.selectNumbers({ index: 0 });
      expect(component.numbersData[0].selected).toBeFalsy();
      expect(component.numbersData[0].disabled).toBeFalsy();
      expect(component.disabledBalls).toHaveBeenCalled();

      component.numbersData = <any>[{ disabled: true }];

      component.selectNumbers({ index: 0 });
      expect(component.numbersData[0].disabled).toBeTruthy();
    });

    it(' selected undefiend', () => {
      component.numbersData = <any>[{ selected: false, disabled: false }];
      spyOn(component, 'disabledBalls');
      component.selected = undefined;

      component.selectNumbers({ index: 0 });
      expect(component.disabledBalls).toHaveBeenCalled();
    });

    it(' stress clicking: click before balls are disabled', () => {
      component.numbersData = <any>[{ selected: false, disabled: false }];
      component.currentLottery = {
        maxPicks: 5
      } as any;
      spyOn(component, 'disabledBalls');
      component.selected = 5;

      component.selectNumbers({ index: 0 });
      expect(component.disabledBalls).not.toHaveBeenCalled();
    });
  });

  it('resetSelected', () => {
    component.resetSelected(2);
    expect(component.selected).toBe(2);
  });

  it('doneSelected', () => {
    component.numbersData = <any>[{}, { selected: true }];

    component['getSelected'] = jasmine.createSpy();
    component['setSelectedBallNumbers'] = jasmine.createSpy();

    component.doneSelected();

    expect(component['getSelected']).toHaveBeenCalled();
    expect(component['setSelectedBallNumbers']).toHaveBeenCalledWith([component.numbersData[1]]);
    expect(changeDetectorRef.detectChanges).toHaveBeenCalled();
    expect(storage.set).toHaveBeenCalledWith(component.currentLotto.name, [component.numbersData[1]]);
  });

  it('luckyDip', () => {
    spyOn(component, 'disabledBalls');
    component.numbersData = <any>[{ value: '1' }, { value: '2' }, { value: '3' }];

    component.luckyDip(2);
    expect(_.compact(_.pluck(component.numbersData, 'selected')).length).toEqual(2);
    expect(component.ballSelected.length).toEqual(2);
    expect(_.keys(component.ballSelected[0])).toEqual(['value', 'selected']);
    expect(component.disabledBalls).toHaveBeenCalled();
  });

  it('resetNumbers', () => {
    component['getSelected'] = jasmine.createSpy();
    spyOn(component, 'setBallNumbers');

    component.resetNumbers();

    expect(component.ballSelected).toEqual([]);
    expect(component.selected).toBeFalsy();
    expect(storage.remove).toHaveBeenCalledWith(component.currentLotto.name);
    expect(component.setBallNumbers).toHaveBeenCalled();
    expect(component['getSelected']).toHaveBeenCalled();
    expect(segmentDataUpdateService.changes.next).toHaveBeenCalledWith({
      numbersSelected: component.numbersSelected,
      numbersData: component.numbersData,
      selected: component.selected
    });
  });

  it('setTotalStake', () => {
    component.accumulatorAmount = 10;
    component.setTotalStake();
    expect(component.totalStake === component.accumulatorAmount).toBeTruthy();
  });

  describe('confirmation', () => {
    let event;

    beforeEach(() => {
      spyOn(component as any, 'displayPopupForInShopUser').and.callThrough();
      event = { stopPropagation: jasmine.createSpy('stopPropagation') };
    });

    it('used logged in', () => {
      user.status = true;

      component.confirmation(event);

      expect(component['displayPopupForInShopUser']).toHaveBeenCalled();
      expect(event.stopPropagation).toHaveBeenCalled();
      expect(component.confirm).toBeTruthy();
    });

    it('used logged in (in shop)', () => {
      user.status = true;
      user.isInShopUser.and.returnValue(true);

      component.confirmation(event);

      expect(component['displayPopupForInShopUser']).toHaveBeenCalled();
      expect(event.stopPropagation).toHaveBeenCalled();
      expect(component.confirm).toBeFalsy();
      expect(windowRefService.nativeWindow.location.href).toBe(accountUpgradeLinkService.inShopToMultiChannelLink);
    });

    it('used logged out', () => {
      user.status = false;
      component.confirmation(event);
      expect(component.confirm).toBeFalsy();
      expect(pubSubService.publish).toHaveBeenCalledWith('OPEN_LOGIN_DIALOG', { moduleName: 'lottobet' });
    });
  });

  describe('placeLottoBet', () => {
    beforeEach(() => {
      spyOn(component, 'resetNumbers');
      spyOn(component, 'resetDraws');
      component['getBetObject'] = jasmine.createSpy('getBetObject').and.returnValue(<any>{ multiplier: 2 });
    });

    it('happy path', fakeAsync(() => {
      component.placeLottoBet();

      tick();

      expect(component['getBetObject']).toHaveBeenCalled();
      expect(component.lottoError).toEqual({ type: '', msg: '' });
      expect(lottoBetService.placeBet).toHaveBeenCalled();
      expect(component.resetNumbers).toHaveBeenCalled();
      expect(component.resetDraws).toHaveBeenCalled();
      expect(component.placeBetPending).toBeFalsy();
      expect(component.confirm).toBeFalsy();
      expect(lottoBetService.openReceipt).toHaveBeenCalled();
    }));

    it('happy path. should not call publish', fakeAsync(() => {
      lottoBetService.placeBet.and.returnValue(of({}));

      component.placeLottoBet();

      tick();

      expect(component['getBetObject']).toHaveBeenCalled();
      expect(component.lottoError).toEqual({ type: '', msg: '' });
      expect(lottoBetService.placeBet).toHaveBeenCalled();
      expect(component.resetNumbers).toHaveBeenCalled();
      expect(component.resetDraws).toHaveBeenCalled();
      expect(component.placeBetPending).toBeFalsy();
      expect(component.confirm).toBeFalsy();
      expect(lottoBetService.openReceipt).toHaveBeenCalled();
    }));

    it('error path: error.data exist', fakeAsync(() => {
      lottoBetService.placeBet = jasmine.createSpy('placeBet').and.returnValue(throwError({
        data: {},
        status: 'status',
        statusText: 'status_text'
      }));
      createComp();
      component['getBetObject'] = jasmine.createSpy('getBetObject').and.returnValue(<any>{ multiplier: 2 });

      component.placeLottoBet();

      tick();

      expect(lottoBetService.placeBet).toHaveBeenCalled();
      expect(component['getBetObject']).toHaveBeenCalled();
      expect(component.placeBetPending).toBeFalsy();
      expect(component.confirm).toBeFalsy();
      expect(component.lottoError).toEqual({ type: '', msg: '' });
      expect(betPlacementErrorTracking.sendLotto).toHaveBeenCalledWith('status', 'status_text');
    }));

    it('error path: error.data no funds', fakeAsync(() => {
      lottoBetService.placeBet = jasmine.createSpy('placeBet').and.returnValue(throwError({}));
      createComp();
      component.ballSelected = [{}] as any;
      component.accumulatorAmount = 10;
      component.setTotalStake();
      component['getBetObject'] = jasmine.createSpy('getBetObject').and.returnValue(<any>{ multiplier: 2 });

      component.placeLottoBet();

      tick();

      expect(lottoBetService.placeBet).toHaveBeenCalledWith({ multiplier: 2 });
      expect(locale.getString).toHaveBeenCalledWith('lotto.noFunds');
      expect(component['getBetObject']).toHaveBeenCalled();
      expect(component.placeBetPending).toBeFalsy();
      expect(component.confirm).toBeFalsy();
      expect(component.ballSelected.length).toEqual(1);
      expect(component.lottoError).toEqual({ type: 'error', msg: 'test_string' });
      expect(betPlacementErrorTracking.sendLotto).toHaveBeenCalledWith('NOFUNDS', 'TEST_STRING');
    }));

    it('error path: error.data not exist case #1', fakeAsync(() => {
      lottoBetService.placeBet = jasmine.createSpy('placeBet').and.returnValue(throwError({
        errorDesc: 'errorDesc',
        code: 'code'
      }));
      createComp();
      component['getBetObject'] = jasmine.createSpy('getBetObject').and.returnValue(<any>{ multiplier: 2 });

      component.placeLottoBet();

      tick();

      expect(component['getBetObject']).toHaveBeenCalled();
      expect(lottoBetService.placeBet).toHaveBeenCalled();
      expect(component.placeBetPending).toBeFalsy();
      expect(component.confirm).toBeFalsy();
      expect(locale.getString).toHaveBeenCalledWith('lotto.code');
      expect(component.lottoError).toEqual({ type: 'error', msg: 'test_string: errorDesc' });
      expect(betPlacementErrorTracking.sendLotto).toHaveBeenCalledWith('code', 'errorDesc');
    }));

    it('error path: error.data not exist case #2', fakeAsync(() => {
      lottoBetService.placeBet = jasmine.createSpy('placeBet').and.returnValue(throwError({
        code: 'code'
      }));
      createComp();
      component['getBetObject'] = jasmine.createSpy('getBetObject').and.returnValue(<any>{ multiplier: 2 });

      component.placeLottoBet();

      tick();

      expect(component['getBetObject']).toHaveBeenCalled();
      expect(lottoBetService.placeBet).toHaveBeenCalled();
      expect(component.placeBetPending).toBeFalsy();
      expect(component.confirm).toBeFalsy();
      expect(locale.getString).toHaveBeenCalledWith('lotto.code');
      expect(component.lottoError).toEqual({ type: 'error', msg: 'test_string' });
      expect(betPlacementErrorTracking.sendLotto).toHaveBeenCalledWith('code', undefined);
    }));

    it('error path: error.data not exist case #3', fakeAsync(() => {
      lottoBetService.placeBet = jasmine.createSpy('placeBet').and.returnValue(throwError({}));
      createComp();
      component['getBetObject'] = jasmine.createSpy('getBetObject').and.returnValue(<any>{ multiplier: 2 });

      component.placeLottoBet();

      tick();

      expect(component['getBetObject']).toHaveBeenCalled();
      expect(lottoBetService.placeBet).toHaveBeenCalled();
      expect(component.placeBetPending).toBeFalsy();
      expect(component.confirm).toBeFalsy();
      expect(component.lottoError).toEqual({ type: 'error', msg: 'test_string' });
      expect(locale.getString).toHaveBeenCalledWith('lotto.BET_ERROR');
      expect(betPlacementErrorTracking.sendLotto).toHaveBeenCalled();
    }));

    it('error path: response.betError exist', fakeAsync(() => {
      component['lottoBetService'].placeBet = jasmine.createSpy('placeBet').and.returnValue(of({
        betError: [ { err: 1 } ]
      }));
      component['handleError'] = jasmine.createSpy('handleError');

      component.placeLottoBet();
      expect(component['handleError']).toHaveBeenCalledWith({ multiplier: 2 }, { err: 1 });
    }));
  });

  it('totalStakeGetter: isNaN false', () => {
    component.totalStake = 10;
    component.drawMultiplier = 10;
    expect(component.totalStakeGetter).toEqual('setCurrency');
    expect(filterService.setCurrency).toHaveBeenCalledWith(100, user.currencySymbol);
  });

  it('totalStakeGetter: isNaN true', () => {
    component.totalStake = NaN;
    component.drawMultiplier = 10;
    expect(component.totalStakeGetter).toEqual('setCurrency');
    expect(filterService.setCurrency).toHaveBeenCalledWith(0, user.currencySymbol);
  });

  it('currencySymbol', () => {
    expect(component.currencySymbol).toEqual(user.currencySymbol);
  });

  describe('getBetObject', () => {
    beforeEach(() => {
      component.boosterBall = null;
      component.selectionName = 'ACC5';
      component.ballPicks = [1, 2, 3];
      component.draws = <any>[{}];
      component.drawMultiplier = 1;
      component.accumulatorAmount = 2;
      component.prices = <any>{};
    });

    it('without booster ball', () => {
      expect(component['getBetObject']()).toEqual({
        name: component.currentLotto.normal.name,
        selectionName: component.selectionName,
        selections: component.ballPicks.join('|'),
        draws: component.draws,
        multiplier: component.drawMultiplier,
        amount: component.accumulatorAmount,
        odds: component.prices,
        currency: user.currency
      });
    });

    it('with booster ball', () => {
      component.boosterBall = true;
      expect(component['getBetObject']().name).toEqual(component.currentLotto.boosterBall.name);
    });
  });

  it('setSelectedBallNumbers', () => {
    const data = <any>[{ value: 2 }, {}, { value: 10 }, { value: 5 }];
    const res = [{ value: 2, selected: true, disabled: false },
                 { value: '-', selected: false, disabled: false },
                 { value: 10, selected: true, disabled: false },
                 { value: 5, selected: true, disabled: false }];

    component.numbersSelected = [];
    component.currentLottery = <any>{ maxPicks: 4 };

    component['setSelectedBallNumbers'](data);
    expect(component.numbersSelected).toEqual(res);
  });

  it('getTimeLeft', () => {
    const d = new Date();
    const res = component['getTimeLeft'](d);

    expect(_.isNumber(res.total)).toBeTruthy();
    expect(_.isNumber(res.days)).toBeTruthy();
    expect(_.isNumber(res.hours)).toBeTruthy();
    expect(_.isNumber(res.minutes)).toBeTruthy();
  });

  describe('initializeTimer', () => {
    it('initializeTimer', () => {
      component['updateLotto'] = jasmine.createSpy('updateLotto');
      component['initializeTimer'](Date.now());

      expect(component.days).toEqual(0);
      expect(component.hours).toEqual(0);
      expect(component.minutes).toEqual(0);
      expect(component['updateLotto']).toHaveBeenCalled();
      expect(typeof component.timeInterval).toBe('number');
      expect(component['routeChangeListener']).toEqual(jasmine.anything());
      expect(router.events.subscribe).toHaveBeenCalledWith(jasmine.anything());
      clearInterval(component.timeInterval);
    });

    it('should not update lotto',  () => {
      router.events = of(new NavigationStart(1, ''));
      component['updateLotto'] = jasmine.createSpy('updateLotto');
      component['getTimeLeft'] = jasmine.createSpy('getTimeLeft').and.returnValue({total: 10, days: 10, hours: 10, minutes: 10});
      component['initializeTimer'](Date.now());

      expect(component['updateLotto']).not.toHaveBeenCalled();
    });

    it('should not update lotto and clear interval',  () => {
      router.events = of(new NavigationEnd(1, '', ''));
      component['updateLotto'] = jasmine.createSpy('updateLotto');
      component['getTimeLeft'] = jasmine.createSpy('getTimeLeft').and.returnValue({total: 10, days: 10, hours: 10, minutes: 10});
      component['initializeTimer'](Date.now());

      expect(component['updateLotto']).not.toHaveBeenCalled();
    });
  });

  it('getSelected', () => {
    component.numbersSelected = <any>[{ value: 1 }, { value: '-' }, { value: 3 }];
    component['setAccumulatorPrices'] = jasmine.createSpy('setAccumulatorPrices');

    component['getSelected']();

    expect(component.ballPicks).toEqual([1, 3]);
    expect(component.selectionName).toEqual('DBL');
    expect(component['setAccumulatorPrices']).toHaveBeenCalled();
  });

  it('setLottoTabs', () => {
    component['setLottoTabs']();

    expect(component.lottoTabs).toEqual([{
      title: 'test_string',
      name: 'Straight',
      hidden: false,
      id: 0,
      url: 'test_url'
    },
    { title: 'test_string',
      name: 'Combo',
      hidden: true,
      id: 1,
      url: 'test_url/combo'
    },
    { title: 'test_string',
      name: 'Results',
      hidden: true,
      id: 2,
      url: 'test_url/results'
    }]);
    expect(component.activeTab.name).toEqual('Straight');
    expect(pubSubService.publish).toHaveBeenCalledWith('MENU_UPDATE', component.activeMenuItem.uri);
  });

  describe('setAccumulatorPrices', () => {
    it('should not calc price if lotteryPrice empty', () => {
      component.lotteryPrice = [];
      component.accumulatorPrices = undefined;
      component['setAccumulatorPrices']();

      expect(component.accumulatorPrices).toBeUndefined();
    });

    it('should not calc if no maxPicks and no ballPicks', () => {
      component.lotteryPrice = [{}] as any;
      component.ballPicks = null;
      component.currentLottery = {} as any;
      component['setAccumulatorPrices']();

      expect(component.accumulatorPrices).toBeUndefined();
    });

    it('should not calc if no maxPicks and ballPicks empty', () => {
      component.lotteryPrice = [{}] as any;
      component.ballPicks = [];
      component.currentLottery = {} as any;
      component['setAccumulatorPrices']();

      expect(component.accumulatorPrices).toBeUndefined();
    });

    it('should not calc odds if correct lottery price not found', () => {
      component.lotteryPrice = [{
        numberPicks: '1', numberCorrect: '1', priceNum: '600', priceDen: '1'
      }] as any;
      component.ballPicks = [1, 2, 3];
      component['setAccumulatorPrices']();

      expect(component.accumulatorPrices).toBeUndefined();
    });

    it('should calc odds in frac format by ballPicks count', () => {
      component.lotteryPrice = [
        { numberPicks: '1', numberCorrect: '1', priceNum: '50', priceDen: '1' },
        { numberPicks: '2', numberCorrect: '2', priceNum: '700', priceDen: '1' },
        { numberPicks: '3', numberCorrect: '3', priceNum: '10000', priceDen: '1' }
      ] as any;
      component.ballPicks = [1, 2, 3];
      component['setAccumulatorPrices']();

      expect(component.accumulatorPrices).toBe('10000/1');
      expect(fracToDecService.getDecimal).not.toHaveBeenCalled();
    });

    it('should calc odds in dec format by currentLottery maxPicks', () => {
      fracToDecService.getDecimal.and.returnValue('100');
      component['user'] = {
        oddsFormat: 'dec'
      } as any;
      component.lotteryPrice = [
        { numberPicks: '1', numberCorrect: '1', priceNum: '50', priceDen: '1' },
        { numberPicks: '2', numberCorrect: '2', priceNum: '700', priceDen: '1' },
        { numberPicks: '3', numberCorrect: '3', priceNum: '10000', priceDen: '1' }
      ] as any;
      component.ballPicks = [];
      component.currentLottery = { maxPicks: '2' } as any;
      component['setAccumulatorPrices']();

      expect(component.accumulatorPrices).toBe('100');
      expect(fracToDecService.getDecimal).toHaveBeenCalledWith('700', '1');
    });
  });


  it('initLotto: lottery data old', fakeAsync(() => {
    component['updateLotto'] = jasmine.createSpy('updateLotto');
    spyOn(component, 'setLotto');
    const d = <any>new Date();
    d.setYear(1990);
    component.lotteryData = currentLotto;
    component.lotteryData.shutAtTime = d;
    component['initLotto']();
    tick();

    expect(component.activeMenuItem).toEqual({ uri: null });
    expect(component.isExpended).toBeTruthy();
    expect(component.boosterBall).toBeFalsy();
    expect(component.confirm).toBeFalsy();
    expect(component.orderedDraws).toEqual([]);
    expect(component.ballPicks).toEqual([]);
    expect(component.ballSelected).toEqual([]);
    expect(component.numbersSelected).toEqual({});
    expect(component.luckyDipArr).toEqual([3, 4, 5]);
    expect(component.showDays).toEqual(7);
    expect(lottoService.getMenuItems).toHaveBeenCalledWith(currentLotto);
    expect(component.menuItems).toEqual([]);

    expect(component['updateLotto']).toHaveBeenCalledWith(component.lotteryData);
    expect(component.setLotto).not.toHaveBeenCalled();
  }));

  it('initLotto: lottery data new', () => {
    component['updateLotto'] = jasmine.createSpy('updateLotto');
    spyOn(component, 'setLotto');

    const d = <any>new Date();
    d.setYear(3000);
    component.setLotto = jasmine.createSpy('setLotto');
    component.lotteryData = currentLotto;
    component.lotteryData.shutAtTime = d;
    component.boosterBall = true;
    component['initLotto']();

    expect(component.setLotto).toHaveBeenCalledWith(component.lotteryData, component.boosterBall);
    expect(component['updateLotto']).not.toHaveBeenCalled();
  });

  it('initLotto: no lotteryData', () => {
    component['updateLotto'] = jasmine.createSpy().and.returnValue(null);
    spyOn(component, 'setLotto');

    component.lotteryData = null;
    component['initLotto']();

    expect(component['updateLotto']).not.toHaveBeenCalled();
    expect(component.setLotto).not.toHaveBeenCalled();
  });

  describe('updateLotto', () => {
    it('updateLotto', fakeAsync(() => {
      spyOn(component, 'setLotto');

      component.boosterBall = true;
      component['updateLotto'](currentLotto);

      tick();

      expect(pubSubService.publish).toHaveBeenCalledWith('MSG_UPDATE', {
        type: 'normal',
        msg: currentLotto.name + locale.getString() + datePipe.transform()
      });
      expect(locale.getString).toHaveBeenCalledWith('lotto.finished');
      expect(datePipe.transform).toHaveBeenCalledWith(currentLotto.shutAtTime, 'HH:mm dd/MM/yyyy');
      expect(lottoService.getLotteriesByLotto).toHaveBeenCalled();
      expect(filterService.filterLink).toHaveBeenCalledWith('lotto/test');
      expect(pubSubService.publish).toHaveBeenCalledWith('MENU_UPDATE', []);
      expect(location.path).toHaveBeenCalled();
      expect(component.setLotto).toHaveBeenCalledWith({ active: true, uri: 'test' }, true);
    }));

    it('should navigate by url',  fakeAsync(() => {
      filterService.filterLink.and.returnValue('link');
      component['updateLotto'](currentLotto);
      tick();

      expect(router.navigateByUrl).toHaveBeenCalled();
    }));
  });

  it('#documentClick', () => {
    component.documentClick();

    expect(component.confirm).toBeFalsy();
  });
});
