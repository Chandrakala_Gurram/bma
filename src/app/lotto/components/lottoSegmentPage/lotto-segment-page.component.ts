
import { concatMap } from 'rxjs/operators';
import { Location, DatePipe } from '@angular/common';
import { Component, ChangeDetectorRef, OnInit, OnDestroy, HostListener } from '@angular/core';
import { ActivatedRoute, NavigationStart, Event as EventRouter, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import * as _ from 'underscore';
import { FiltersService } from '@core/services/filters/filters.service';
import { FracToDecService } from '@core/services/fracToDec/frac-to-dec.service';
import { MainLottoService } from '../../services/mainLotto/main-lotto.service';
import { LottoBetService } from '../../services/lottoBet/lotto-bet.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { StorageService } from '@core/services/storage/storage.service';
import { BetPlacementErrorTrackingService } from '@core/services/betPlacementErrorTracking/bet-placement-error-tracking';
import { AccountUpgradeLinkService } from '@app/vanillaInit/services/accountUpgradeLink/account-upgrade-link.service';
import { WindowRefService } from '@coreModule/services/windowRef/window-ref.service';

import {
  ILottonMenuItem,
  ILotteryMap,
  ILottoPlaceBetObj,
  ILottoDraw,
  ILotto,
  ILottoPrice,
  ILottoTab
} from '../../models/lotto.model';
import { UserService } from '@core/services/user/user.service';
import { SegmentDataUpdateService } from '@app/lotto/services/segmentDataUpdate/segment-data-update.service';
import { ILottoNumber, ILottoNumberMap } from '../../models/lotto-numbers.model';
import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import environment from '@environment/oxygenEnvConfig';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'lotto-segment-page',
  templateUrl: './lotto-segment-page.component.html',
  styleUrls: ['./lotto-segment-page.component.less']
})
export class LottoSegmentPageComponent extends AbstractOutletComponent implements OnInit, OnDestroy {
  lotteryData: ILotto;
  isExpended: boolean;
  boosterBall: boolean;
  confirm: boolean;
  orderedDraws: ILottoDraw[];
  ballPicks: number[];
  ballSelected: ILottoNumber[];
  numbersSelected: ILottoNumberMap;
  luckyDipArr: number[];
  showDays: number;
  activeMenuItem: { uri: string; };
  currentLotto: ILotto;
  currentLottery: ILotto;
  limitValue: number;
  draws: ILottoDraw[];
  drawMultiplier: number;
  lotteryPrice: ILottoPrice[];
  numbersData: ILottoNumber[];
  selected: boolean | number;
  lottoTabs: ILottoTab[];
  tab: string;
  activeTab: ILottoTab;
  menuItems: ILottonMenuItem[];
  selectionName: string;
  prices: ILottoPrice;
  accumulatorPrices: string;
  days: number;
  hours: number;
  minutes: number;
  timeInterval;
  totalStake: number;
  accumulatorAmount: number;
  lottoError: { type: string, msg: string } = { msg: '',  type: ''};
  placeBetPending: boolean;
  helpSupportUrl = environment.HELP_LOTTO_RULES;
  private routeChangeListener: Subscription;

  constructor(
    private filterService: FiltersService,
    private locale: LocaleService,
    private lottoService: MainLottoService,
    private lottoBetService: LottoBetService,
    private fracToDecService: FracToDecService,
    private betPlacementErrorTracking: BetPlacementErrorTrackingService,
    private storage: StorageService,
    private location: Location,
    private datePipe: DatePipe,
    private route: ActivatedRoute,
    private router: Router,
    protected user: UserService,
    private segmentDataUpdateService: SegmentDataUpdateService,
    private pubSubService: PubSubService,
    private accountUpgradeLinkService: AccountUpgradeLinkService,
    private windowRefService: WindowRefService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    super()/* istanbul ignore next */;
  }

  ngOnInit(): void {
    this.route.params.pipe(
      concatMap((params) => {
        return this.lottoService.getLotteryData(params.lottery);
      }))
      .subscribe((lottoData: ILotto) => {
        if (!lottoData || !lottoData.uri) {
          this.router.navigate(['/404']);
        } else {
          this.lotteryData = lottoData;
          this.initLotto();
          this.hideSpinner();
        }
      }, () => {
        this.showError();
      });
  }

  getDipTranslations(luckyNumber: number) {
    return this.filterService.getComplexTranslation('lotto.lucky', '%num', luckyNumber.toString());
  }

  ngOnDestroy(): void {
    clearInterval(this.timeInterval);
    this.routeChangeListener && this.routeChangeListener.unsubscribe();
  }

  setLotto(data, boosterBall): void {
    this.boosterBall = boosterBall;
    this.currentLotto = data;
    this.currentLottery = boosterBall || !data.normal ? data.boosterBall : data.normal;
    this.currentLotto.shutAtTime = this.lottoService.getShutAtTime(this.currentLottery);
    this.activeMenuItem.uri = this.currentLotto.uri;
    this.drawsShow(this.showDays);
    this.updateDraws();
    this.lotteryPrice = this.currentLottery.lotteryPrice;
    this.setBallNumbers();
    this.setLottoTabs();
    this.getSelected();
    this.initializeTimer(this.currentLotto.shutAtTime);
    this.pubSubService.publish('ballsUpdate');
  }

  drawsShow(days: number): void {
    const date = new Date();
    const lastDay = date.setDate(date.getDate() + days);
    const storageArray = this.storage.get(`${this.currentLotto.name}Draw`) as ILottoDraw[];
    const limitArray = _.each(this.filterService.orderBy(this.currentLottery.draw, ['drawAtTime', 'description']), (draw: ILottoDraw) => {
      draw.checked = false;
    });
    this.orderedDraws = [];

    _.each(limitArray, (draw: ILottoDraw) => {
      if (Date.parse(draw.shutAtTime) > Date.now() && Date.parse(draw.shutAtTime) < lastDay) {
        this.orderedDraws.push(draw);
      }
    });

    if (storageArray && storageArray[0].id === this.orderedDraws[0].id) {
      this.orderedDraws = storageArray;
    }

    this.limitValue = this.currentLottery.limits ? this.currentLottery.limits : this.orderedDraws.length;
    this.orderedDraws = this.orderedDraws.splice(0, this.limitValue);
    this.orderedDraws[0].checked = true;
  }

  updateDraws(): void {
    this.storage.set(`${this.currentLotto.name}Draw`, this.orderedDraws);
    this.draws = this.orderedDraws.filter((draw) => draw.checked);
    this.drawMultiplier = this.draws.length || 1;
  }

  setBallNumbers(): void {
    this.ballSelected = this.storage.get(this.lotteryData.name) || [];
    this.numbersData = [];

    // It creates data for balls from 1 to max ball number. (For example, [1,2,...,49])
    this.numbersData = _.chain(this.currentLottery.maxNumber)
      .range()
      .map(num => ({
        value: num + 1,
        selected: false,
        disabled: false
      }))
      .value();

    if (this.ballSelected.length) {
      this.selected = true;
      // It merges data from numbers data (numbersData) and selected balls data (ballSelected)
      this.numbersData = _.values(_.extend(
        _.indexBy(this.numbersData, 'value'), _.indexBy(this.ballSelected, 'value')
      ));
      this.disabledBalls();
    }

    this.setSelectedBallNumbers(this.ballSelected);
  }

  disabledBalls(): void {
    this.selected = _.where(this.numbersData, { selected: true }).length;
    _.each(this.numbersData, num => {
      num.disabled =
        this.selected === Number(this.currentLottery.maxPicks)
          ? num.selected === false
          : false;
    });
  }

  resetDraws(): void {
    _.each(this.orderedDraws, item => {
      item.checked = false;
    });
    this.storage.remove(`${this.currentLotto.name}Draw`);
  }

  selectNumbers(val): void {
    if (this.numbersData[val.index].selected) {
      _.each(this.numbersData, num => {
        num.disabled = false;
      });
    } else if (this.selected && +this.selected === this.currentLottery.maxPicks) {
      return;
    }

    this.numbersData[val.index].selected = !this.numbersData[val.index].selected;
    this.disabledBalls();
  }

  /**
   * Event handler on event emitted in number-selector.component.onBeforeClose
   * @param {number} num - selected value before dialog opened to restore
   */
  resetSelected(num: number): void {
    this.selected = num;
  }

  doneSelected(): void {
    const numbersDataSelected = _.where(this.numbersData, { selected: true });
    this.storage.set(this.currentLotto.name, numbersDataSelected);
    this.setSelectedBallNumbers(numbersDataSelected);
    this.getSelected();
    this.changeDetectorRef.detectChanges();
  }

  luckyDip(number): void {
    this.ballSelected = [];
    _.each(this.numbersData, num => {
      num.selected = false;
    });
    this.ballSelected = _.each(_.sample(this.numbersData, number), num => {
      num.selected = true;
    }) as ILottoNumber[];

    // It merges data from numbers data (numbersData) and selected balls data (ballSelected)
    this.numbersData = _.values(_.extend(
      _.indexBy(this.numbersData, 'value'), _.indexBy(this.ballSelected, 'value')
    ));
    this.disabledBalls();
  }

  resetNumbers(): void {
    this.ballSelected = [];
    this.storage.remove(this.currentLotto.name);
    this.setBallNumbers();
    this.selected = false;
    this.getSelected();

    this.segmentDataUpdateService.changes.next({
      numbersSelected: this.numbersSelected,
      numbersData: this.numbersData,
      selected: this.selected
    });
  }

  setTotalStake(): void {
    this.totalStake = this.accumulatorAmount;
  }

  confirmation(event: Event): void {
    if (this.user.status) {
      this.displayPopupForInShopUser(event);
    } else {
      this.confirm = false;
      this.pubSubService.publish(this.pubSubService.API.OPEN_LOGIN_DIALOG, { moduleName: 'lottobet' });
    }
  }

  @HostListener('document:click')
  documentClick(): void {
    this.confirm = false;
  }

  placeLottoBet(): void {
    const data = this.getBetObject();
    // clear previous message
    this.lottoError = {
      type: '',
      msg: ''
    };

    this.placeBetPending = true;
    this.lottoBetService.placeBet(data).subscribe(
      (response) => {
        this.placeBetPending = false;
        this.confirm = false;
        if (response.betError && response.betError[0]) {
          this.handleError(data, response.betError[0]);
        } else {
          this.resetNumbers();
          this.resetDraws();
          this.lottoBetService.openReceipt();
        }
      }, (error) => {
        this.handleError(data, error);
      });
  }

  protected displayPopupForInShopUser(event: Event): void {
    event.stopPropagation();
    this.confirm = false;
    if (this.user.isInShopUser()) {
      this.windowRefService.nativeWindow.location.href = this.accountUpgradeLinkService.inShopToMultiChannelLink;
      return;
    }

    this.confirm = true;
  }

  private handleError(data: ILottoPlaceBetObj, error): void {
    this.placeBetPending = false;
    this.confirm = false;
    let lottoBetErrorCode;
    let lottoBetErrorMessage;

    if (error.data) {
      lottoBetErrorCode = error.status;
      lottoBetErrorMessage = error.statusText;
    } else {
      let errorMsg;

      if (Number(this.user.sportBalance) < (this.totalStake * data.multiplier)) {
        errorMsg = this.locale.getString('lotto.noFunds');
        lottoBetErrorCode = 'NOFUNDS';
        lottoBetErrorMessage = errorMsg.toUpperCase();
      } else {
        if (error.errorDesc && error.code) {
          errorMsg = `${this.locale.getString(`lotto.${error.code}`)}: ${error.errorDesc}`;
        } else if (error.code) {
          errorMsg = this.locale.getString(`lotto.${error.code}`);
        } else {
          errorMsg = this.locale.getString(`lotto.BET_ERROR`);
        }

        lottoBetErrorCode = error.subErrorCode || error.code;
        lottoBetErrorMessage = error.errorDesc || error.subErrorCode;
      }

      this.lottoError = {
        type: 'error',
        msg: errorMsg
      };
    }

    // sending errors to betPlacementErrorTracking service
    this.betPlacementErrorTracking.sendLotto(lottoBetErrorCode, lottoBetErrorMessage);
  }

  get totalStakeGetter(): string {
    const totalStake = this.totalStake * this.drawMultiplier;
    return this.filterService.setCurrency(_.isNaN(totalStake) ? 0 : totalStake, this.user.currencySymbol);
  }

  get currencySymbol(): string {
    return this.user.currencySymbol;
  }

  private getBetObject(): ILottoPlaceBetObj {
    return {
      name: this.boosterBall || !this.currentLotto.normal
        ? this.currentLotto.boosterBall.name
        : this.currentLotto.normal.name, // lottery product
      selectionName: this.selectionName, // 'ACC5' bet selection type
      selections: this.ballPicks.join('|'), // '1|3|5' ball selections
      draws: this.draws, // object with draw data
      multiplier: this.drawMultiplier, // 1x 2x etc
      amount: this.accumulatorAmount, // e.g '2.00'
      odds: this.prices, // object with priceNum & priceDen
      currency: this.user.currency  // 'GBP'
    };
  }

  private setSelectedBallNumbers(data: ILottoNumberMap): void {
    _.each(_.range(this.currentLottery.maxPicks), (num, i) => {
      const number = data[num] && data[num].value;
      this.numbersSelected[i] = {
        value: number || '-',
        selected: _.isNumber(number),
        disabled: false
      };
    });
  }

  private getTimeLeft(endTime: Date): {
    total: number;
    days: number;
    hours: number;
    minutes: number;
  } {
    const time = endTime.getTime() - new Date().getTime();
    const minutes = Math.floor((time / 1000 / 60) % 60);
    const hours = Math.floor((time / (1000 * 60 * 60)) % 24);
    const days = Math.floor(time / (1000 * 60 * 60 * 24));

    return {
      total: time,
      days,
      hours,
      minutes
    };
  }

  private initializeTimer(time): void {
    const endTime = new Date(time);
    const updateClock = () => {
      const { total, days, hours, minutes } = this.getTimeLeft(endTime);
      const zeroify = value => value <= 0 ? 0 : value;

      this.days = zeroify(days);
      this.hours = zeroify(hours);
      this.minutes = zeroify(minutes);

      if (total <= 0) {
        clearInterval(this.timeInterval);
        this.updateLotto(this.currentLotto);
      }
    };

    clearInterval(this.timeInterval);

    updateClock();
    this.timeInterval = setInterval(updateClock, 1000);

    this.routeChangeListener = this.router.events.subscribe((event: EventRouter) => {
      if (event instanceof NavigationStart) {
        clearInterval(this.timeInterval);
      }
    });
  }

  private getSelected(): void {
    const sglArray = ['SGL', 'DBL', 'TBL', 'ACC4', 'ACC5'];
    this.ballPicks = _.without(_.map((this.numbersSelected as ILottoNumber[]), num => num.value), '-') as number[];
    this.selectionName = sglArray[this.ballPicks.length - 1];
    this.setAccumulatorPrices();
  }

  private setLottoTabs(): void {
    const tabUri =  this.router.url;
    this.lottoTabs = [
      { title: this.locale.getString('lotto.straight'),
        name: 'Straight',
        hidden: false,
        id: 0,
        url: tabUri
      },
      { title: this.locale.getString('lotto.combo'),
        name: 'Combo',
        hidden: true,
        id: 1,
        url: `${tabUri}/combo`
      },
      { title: this.locale.getString('lotto.results'),
        name: 'Results',
        hidden: true,
        id: 2,
        url: `${tabUri}/results`
      }
    ];

    this.activeTab = _.findWhere(this.lottoTabs, { url: tabUri });
    this.pubSubService.publish('MENU_UPDATE', this.activeMenuItem.uri);
  }

  /**
   * Set accumulator for prices
   */
  private setAccumulatorPrices(): void {
    if (this.lotteryPrice.length) {
      const ballsCount = this.ballPicks && this.ballPicks.length || +this.currentLottery.maxPicks;

      if (!ballsCount) {
        return;
      }

      this.prices = this.lotteryPrice.find((price: ILottoPrice) => {
        return ballsCount === +price.numberPicks && ballsCount === +price.numberCorrect;
      });

      if (this.prices) {
        this.accumulatorPrices = this.user.oddsFormat === 'frac'
          ? (`${this.prices.priceNum}/${this.prices.priceDen}`)
          : <string>this.fracToDecService.getDecimal(this.prices.priceNum, this.prices.priceDen);
      }
    }
  }

  /**
   * Initial for lotto segment page
   */
  private initLotto(): void {
    this.activeMenuItem = { uri: null };
    this.isExpended = true;
    this.boosterBall = false;
    this.confirm = false;
    this.orderedDraws = this.ballPicks = this.ballSelected = [];
    this.numbersSelected = {};
    this.luckyDipArr = [3, 4, 5]; // Lucky Dip options for the straight bet type
    this.showDays = 7;  // Days quantity (shows data for those days)
    this.menuItems = this.lottoService.getMenuItems(this.lotteryData);

    if (this.lotteryData && (Date.parse(this.lotteryData.shutAtTime) < Date.now())) {
      this.updateLotto(this.lotteryData);
    } else if (this.lotteryData) {
      this.setLotto(this.lotteryData, this.boosterBall);
    }
  }

  /**
   * Update current lotto
   * @param {Object} currentLotto
   */
  private updateLotto(currentLotto: ILotto): void {
    this.pubSubService.publish('MSG_UPDATE', {
      type: 'normal',
      msg: currentLotto.name + this.locale.getString('lotto.finished') +
      this.datePipe.transform(currentLotto.shutAtTime, 'HH:mm dd/MM/yyyy')
    });

    this.lottoService.getLotteriesByLotto().subscribe((data: ILotteryMap) => {
      const activeLotto: ILottonMenuItem = _.findWhere(_.toArray(data), { active: true });
      const uri = this.filterService.filterLink(`lotto/${activeLotto.uri}`);
      this.pubSubService.publish('MENU_UPDATE', this.lottoService.getMenuItems(data));
      if (this.location.path() !== uri) {
        setTimeout(() => this.router.navigateByUrl(uri));
      } else {
        this.setLotto(activeLotto, this.boosterBall);
      }
    });
  }
}
