import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { CmsService } from '@coreModule/services/cms/cms.service';

@Component({
  selector: 'lotto-results-page',
  templateUrl: './lotto-results-page.component.html'
})
export class LottoResultsPageComponent implements OnInit {

  viewByFilters: string[];
  switchers: any[];
  filter: string;
  svg: string;
  svgId: string;

  constructor(
    private location: Location,
    private cmsService: CmsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.viewByFilters = [
      'byLotto',
      'byTime'
    ];

    this.switchers = [{
      onClick: () => this.goToFilter(this.viewByFilters[0]),
      viewByFilters: this.viewByFilters[0],
      name: 'lotto.byLotto'
    }, {
      onClick: () => this.goToFilter(this.viewByFilters[1]),
      viewByFilters: this.viewByFilters[1],
      name: 'lotto.byTime'
    }];

    this.init();
  }

  private init(): void {
    this.cmsService.getItemSvg('Lotto')
      .subscribe((icon) => {
        this.svg = icon.svg;
        this.svgId = icon.svgId;
    });

    this.goToFilter(this.location.path().split('/')[3] || this.viewByFilters[0]);
  }

  private goToFilter(filter) {
    this.filter = filter;
    this.router.navigate(['/lotto', 'results', filter]);
  }
}
