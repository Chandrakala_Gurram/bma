import { LottoResultsPageComponent } from './lotto-results-page.component';
import { of as observableOf } from 'rxjs';

describe('#LottoResultsPageComponent', () => {
  let component, location, cmsService, router;

  beforeEach(() => {
    location = {
      path: jasmine.createSpy('location.path').and.returnValue('location/1/2/test')
    };
    cmsService = {
      getItemSvg: jasmine.createSpy('cmsService.getItemSvg').and.returnValue(observableOf({svg: 'svg', svgId: 'svgId'}))
    };
    router = {
      navigate: jasmine.createSpy('router.navigate')
    };

    component = new LottoResultsPageComponent(location, cmsService, router);
  });

  it('#ngOnInit', () => {
    component.ngOnInit();

    expect(component.viewByFilters).toEqual(['byLotto', 'byTime']);
    expect(component.switchers).toBeDefined();
    expect(component.switchers[0].viewByFilters).toBe('byLotto');
    expect(component.switchers[0].name).toBe('lotto.byLotto');
    expect(component.switchers[1].viewByFilters).toBe('byTime');
    expect(component.switchers[1].name).toBe('lotto.byTime');
    expect(cmsService.getItemSvg).toHaveBeenCalled();
    expect(component.svg).toBe('svg');
    expect(component.svgId).toBe('svgId');
    expect(component.filter).toBe('test');
    expect(router.navigate).toHaveBeenCalledWith(['/lotto', 'results', 'test']);

    component['goToFilter'] = jasmine.createSpy('goToFilter');
    component.switchers[0].onClick();
    component.switchers[1].onClick();
    expect(component['goToFilter']).toHaveBeenCalledTimes(2);
  });

  it('init should navigate with viewByFilters value', () => {
    component['goToFilter'] = jasmine.createSpy('goToFilter');
    location.path.and.returnValue('1/2');
    component.viewByFilters = ['filter'];
    component['init']();

    expect(component['goToFilter']).toHaveBeenCalledWith('filter');
  });
});
