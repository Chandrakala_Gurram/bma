import { Component, Input, Output, EventEmitter, ComponentFactoryResolver } from '@angular/core';
import * as _ from 'underscore';

import { ILottoNumber, ILottoNumberMap } from '../../models/lotto-numbers.model';
import { DialogService } from '@core/services/dialogService/dialog.service';
import { LottoNumberSelectorComponent } from '@lottoModule/components/lottoNumberSelectorDialog/lotto-number-selector-dialog.component';

@Component({
  selector: 'number-selector',
  templateUrl: './number-selector.component.html'
})
export class NumberSelectorComponent {

  @Input() numbersSelected: ILottoNumberMap;
  @Input() numbersData: ILottoNumber[];
  @Input() luckyDipArr: number[];
  @Input() selected: boolean;

  @Output() readonly selectNumbers = new EventEmitter();
  @Output() readonly luckyDip = new EventEmitter();
  @Output() readonly resetNumbers = new EventEmitter();
  @Output() readonly doneSelected = new EventEmitter();
  @Output() readonly resetSelected = new EventEmitter();

  private isDone: boolean;
  constructor(
    private dialogService: DialogService,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
  }

  numbersTrackBy(index: number, item): string {
    return `${index}${item.value}`;
  }

  numDialog(): void {
    const numberBackup: ILottoNumber[] = JSON.parse(JSON.stringify(this.numbersData));
    const selectedBackup: boolean | number = this.selected;

    this.dialogService.openDialog(
      DialogService.API.lottoNumberSelector,
      this.componentFactoryResolver.resolveComponentFactory(LottoNumberSelectorComponent),
      true,
      {
        numbersData: this.numbersData,
        luckyDipArr: this.luckyDipArr,
        selected: this.selected,
        selectNumbers: (value, index) => {
          this.selectNumbers.emit({ value, index });
        },
        luckyDip: (val) => {
          this.luckyDip.emit(val);
        },
        resetNumbers: () => {
          this.resetNumbers.emit();
        },
        doneSelected: () => {
          this.isDone = true;
          this.doneSelected.emit();
        },
        onBeforeClose: () => {
          if (!this.isDone) {
            this.clearNumberOnClose(numberBackup);
            this.resetSelected.emit(selectedBackup);
          }
          this.isDone = false;
        }
      }
    );
  }

  get numbers(): ILottoNumber[] {
    return _.toArray(this.numbersSelected);
  }

  private clearNumberOnClose(numberBackup: ILottoNumber[]): void {
    _.each(this.numbersData, (number, key) => {
      number.selected = numberBackup[key].selected;
      number.disabled = numberBackup[key].disabled;
      number.value = numberBackup[key].value;
    });
  }

}
