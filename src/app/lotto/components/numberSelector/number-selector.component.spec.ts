import { NumberSelectorComponent } from './number-selector.component';

describe('NumberSelectorComponent', () => {
  let component, dialogService, componentFactoryResolver, dialogObjClone;

  beforeEach(() => {
    dialogService = {
      openDialog: jasmine.createSpy('dialogService.openDialog').and.callFake(
        (numberSelector, compFactory, bool, dialogObj) => {
          dialogObjClone = dialogObj;
        }
      )
    };
    componentFactoryResolver = {
      resolveComponentFactory: jasmine.createSpy('componentFactoryResolver.resolveComponentFactory')
    };

    component = new NumberSelectorComponent(dialogService, componentFactoryResolver);
    component.luckyDip = { emit: jasmine.createSpy('luckyDip.emit') };
    component.selectNumbers = { emit: jasmine.createSpy('selectNumbers.emit') };
    component.resetNumbers = { emit: jasmine.createSpy('resetNumbers.emit') };
    component.doneSelected = { emit: jasmine.createSpy('doneSelected.emit') };
    component.resetSelected = { emit: jasmine.createSpy('resetSelected.emit') };

    component.luckyDipArr = [1, 2, 3];
    component.selected = true;
    component.numbersSelected = {0: {value: 'val', selected: true, disabled: false},  1: {value: 'val2', selected: true, disabled: false}};
    component.numbersData = [{value: 'val', selected: true, disabled: false}, {value: 'val2', selected: true, disabled: false}];
  });

  it('#ngOnInit', () => {
    const numberBackup = JSON.parse(JSON.stringify(component.numbersData));

    component.numDialog();

    expect(dialogService.openDialog).toHaveBeenCalled();
    expect(dialogObjClone.numbersData).toEqual(component.numbersData);
    expect(dialogObjClone.luckyDipArr).toEqual(component.luckyDipArr);
    expect(dialogObjClone.selected).toBeTruthy();

    dialogObjClone.selectNumbers(1, 1);
    expect(component.selectNumbers.emit).toHaveBeenCalled();

    dialogObjClone.luckyDip(2);
    expect(component.luckyDip.emit).toHaveBeenCalledWith(2);

    dialogObjClone.resetNumbers();
    expect(component.resetNumbers.emit).toHaveBeenCalled();

    dialogObjClone.doneSelected();
    expect(component.isDone).toBeTruthy();
    expect(component.resetNumbers.emit).toHaveBeenCalled();

    dialogObjClone.onBeforeClose();

    expect(component.numbersData).toEqual(numberBackup);
    expect(component.resetSelected.emit).not.toHaveBeenCalled();
    expect(component.isDone).toBeFalsy();
  });

  it('should clear numbers',  () => {
    component['clearNumberOnClose'] = jasmine.createSpy('clearNumberOnClose');
    component.isDone = false;
    component.numDialog();

    dialogObjClone.onBeforeClose();

    expect(component.resetSelected.emit).toHaveBeenCalled();
    expect(component['clearNumberOnClose']).toHaveBeenCalledWith([
      { value: 'val', selected: true, disabled: false },
      { value: 'val2', selected: true, disabled: false }
      ]);
  });

  it('#numbers', () => {
    expect(component.numbers).toEqual(component.numbersData);
  });

  it('#numbersTrackBy', () => {
    const result = component.numbersTrackBy(1, {value: 1});
    expect(result).toEqual('11');
  });

  it('clearNumberOnClose should update numbersData',  () => {
    const numberBackup = [
      {value: 'value', selected: false, disabled: true},
      {value: 'value2', selected: true, disabled: false}
    ];
    component['clearNumberOnClose'](numberBackup);

    expect(component.numbersData).toEqual(numberBackup);
  });
});
