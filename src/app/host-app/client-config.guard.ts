import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { ClientConfigService, ProductService } from '@vanilla/core/core';

@Injectable({
  providedIn: 'root'
})
export class ClientConfigGuard implements CanActivate {
  constructor(private clientConfigService: ClientConfigService, private productService: ProductService) {
  }

  async canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {
    if (!route.data || !route.data['product']) {
      throw new Error(`ClientConfigGuard requires 'product' property to be specified on routes 'data'.`);
    }

    const product = route.data['product'];

    await this.clientConfigService.load(this.productService.getMetadata(product).apiBaseUrl);

    return true;
  }
}
