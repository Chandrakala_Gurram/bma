import { ProductSwitchResolver } from './product-switch.resolver';

describe('ProductSwitchResolver', () => {
  let service: ProductSwitchResolver;
  let productServiceMock;
  let productActivatorServiceMock;
  let stylesServiceMock;
  let tagManagerServiceMock;
  let vanillaTrackingServiceMock;
  let route;
  let windowRefMock;

  beforeEach(() => {
    productServiceMock = {
      current: {
        name: 'host'
      }
    };
    productActivatorServiceMock = {
      activate: jasmine.createSpy().and.returnValue(Promise.resolve())
    };
    stylesServiceMock = {
      load: jasmine.createSpy().and.returnValue(Promise.resolve())
    };
    tagManagerServiceMock = {
      load: jasmine.createSpy().and.returnValue(Promise.resolve())
    };
    vanillaTrackingServiceMock = {
      triggerEvent: jasmine.createSpy().and.returnValue(Promise.resolve())
    };
    route = {
      root: {
        data: {
          product: 'portal'
        },
        children: []
      }
    };
    windowRefMock = {
      nativeWindow: {
        PreGTMLoad: {
          Execute: jasmine.createSpy()
        }
      }
    };
    service = new ProductSwitchResolver(productServiceMock,
                                        productActivatorServiceMock,
                                        stylesServiceMock,
                                        tagManagerServiceMock,
                                        vanillaTrackingServiceMock,
                                        windowRefMock);
  });

  it('calls resolve with route data as null and throws error', () => {
    route = {};
    service.resolve(route)
      .then()
      .catch((err) => expect(err.message)
        .toBe(`ProductSwitchResolver requires 'product' property to be specified on routes 'data'.`));
  });

  it('calls resolve with route data product as null and throws error', () => {
    route.data = {};
    service.resolve(route)
      .then()
      .catch((err) => expect(err.message)
        .toBe(`ProductSwitchResolver requires 'product' property to be specified on routes 'data'.`));
  });

  it('calls resolve, style service load called in prod mode, GTMLoad.Execute,tagmanager,tracking,productactivator service called', () => {
    service.resolve(route);
    const product = route.root.data['product'];
    if (process.env.NODE_ENV === 'production') {
      expect(stylesServiceMock.load).toHaveBeenCalledWith('portalStyles');
    }
    expect(windowRefMock.nativeWindow.PreGTMLoad.Execute).toHaveBeenCalled();
    expect(tagManagerServiceMock.load).toHaveBeenCalledWith('GoogleTagManagerRenderer');
    expect(vanillaTrackingServiceMock.triggerEvent).toHaveBeenCalledWith('Vanilla_Portal_App_Loaded');
    expect(productActivatorServiceMock.activate).toHaveBeenCalledWith(product);
  });

  it('calls resolve in dev env, styles service not called, GTMLoad.Execute,tagmanager,tracking,productactivator service called', () => {
    service.resolve(route);
    const product = route.root.data['product'];
    if (process.env.NODE_ENV === 'development') {
      expect(stylesServiceMock.load).not.toHaveBeenCalled();
    }
    expect(windowRefMock.nativeWindow.PreGTMLoad.Execute).toHaveBeenCalled();
    expect(tagManagerServiceMock.load).toHaveBeenCalledWith('GoogleTagManagerRenderer');
    expect(vanillaTrackingServiceMock.triggerEvent).toHaveBeenCalledWith('Vanilla_Portal_App_Loaded');
    expect(productActivatorServiceMock.activate).toHaveBeenCalledWith(product);
  });

  it('calls resolve with same current and requested product,styles,GTM.Execute,tagmanager,tracking,product activator not called', () => {
    productServiceMock.current.name = 'portal';
    service.resolve(route);
    expect(windowRefMock.nativeWindow.PreGTMLoad.Execute).not.toHaveBeenCalled();
    expect(stylesServiceMock.load).not.toHaveBeenCalled();
    expect(tagManagerServiceMock.load).not.toHaveBeenCalled();
    expect(vanillaTrackingServiceMock.triggerEvent).not.toHaveBeenCalled();
    expect(productActivatorServiceMock.activate).not.toHaveBeenCalled();
  });

  it('called with different requested product,not call styles,GTM.Execute,tagmanager,tracking service,calls product activator', () => {
    route.root.data['product'] = 'dummyProduct';
    const product = route.root.data['product'];
    service.resolve(route);
    expect(stylesServiceMock.load).not.toHaveBeenCalled();
    expect(windowRefMock.nativeWindow.PreGTMLoad.Execute).not.toHaveBeenCalled();
    expect(tagManagerServiceMock.load).not.toHaveBeenCalled();
    expect(vanillaTrackingServiceMock.triggerEvent).not.toHaveBeenCalled();
    expect(productActivatorServiceMock.activate).toHaveBeenCalledWith(product);
  });
});
