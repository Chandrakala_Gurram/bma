import { ClientConfigGuard } from './client-config.guard';
import { ActivatedRouteSnapshot } from '@angular/router';

describe('ClientConfigGuardTests', () => {
  let clientConfigGuard;

  let clientConfigService;
  let productService;

  const route = new ActivatedRouteSnapshot();

  beforeEach(() => {
    clientConfigService = {
      load: jasmine.createSpy()
    };
    productService = {
      getMetadata: jasmine.createSpy('getMetadata').and.returnValue({
        apiBaseUrl: 'http://someurl'
      })
    };

    clientConfigGuard = new ClientConfigGuard(clientConfigService, productService);
  });

  describe('canActivate()', () => {
    it('should throw error if current route does not have data', () => {
      clientConfigGuard.canActivate(route)
        .then()
        .catch((err) => expect(err.message)
          .toBe(`ClientConfigGuard requires 'product' property to be specified on routes 'data'.`));
    });

    it('should throw error if current route data does not have product', () => {
      route.data = {};

      clientConfigGuard.canActivate(route)
        .then()
        .catch((err) => expect(err.message)
          .toBe(`ClientConfigGuard requires 'product' property to be specified on routes 'data'.`));
    });

    it('should load appropriate client config', () => {
      route.data = { product: 'portal' };

      clientConfigGuard.canActivate(route);

      expect(clientConfigService.load).toHaveBeenCalledWith('http://someurl');
    });
  });
});
