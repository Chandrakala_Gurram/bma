import { NgModule } from '@angular/core';
import { PortalModule } from '@frontend/portal';

@NgModule({
  imports: [PortalModule]
})
export class PortalModuleLoader {
}
