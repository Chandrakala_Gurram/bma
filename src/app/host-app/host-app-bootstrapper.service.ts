import { Injectable } from '@angular/core';
import { OnAppInit, ProductActivatorService } from '@vanilla/core/core';

/**
 * NOTE: Dynacon HostApp.Products might look similar to this
 *
 * ```
 * {
 *       "portal": {
 *          "enabled": true,
 *           "apiBaseUrl": "http://${dynacon:environment}.portal.${dynacon:label}"
 *      },
 *       "host": {
 *           "enabled": true,
 *           "apiBaseUrl": ""
 *       }
 *   }
 * ```
 */
@Injectable()
export class HostAppBootstrapper implements OnAppInit {
  constructor(private productActivatorService: ProductActivatorService) {
  }

  async onAppInit(): Promise<void> {
    await this.productActivatorService.activate('host');
  }
}
