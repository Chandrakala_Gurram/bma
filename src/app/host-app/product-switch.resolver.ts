import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { ProductService, ProductActivatorService, StylesService, TagManagerService, TrackingService } from '@vanilla/core/core';
import { WindowRefService } from '@root/app/core/services/windowRef/window-ref.service';

@Injectable({
  providedIn: 'root'
})
export class ProductSwitchResolver implements Resolve<Promise<void>> {
  constructor(
    private productService: ProductService,
    private productActivatorService: ProductActivatorService,
    private stylesService: StylesService,
    private tagManagerService: TagManagerService,
    private vanillaTrackingService: TrackingService,
    private windowRef: WindowRefService
    ) { }

  async resolve(route: ActivatedRouteSnapshot): Promise<void> {
    const product = this.getProduct(route);

    if (!product) {
      throw new Error(`ProductSwitchResolver requires 'product' property to be specified on routes 'data'.`);
    }

    if (product !== this.productService.current.name) {
      if (product === 'portal') {
        if (process.env.NODE_ENV === 'production') {
          await this.stylesService.load('portalStyles');
        }
        // Call function if attached on window to handle fixes via sitecore.
        this.windowRef.nativeWindow.PreGTMLoad && this.windowRef.nativeWindow.PreGTMLoad.Execute
          && this.windowRef.nativeWindow.PreGTMLoad.Execute();
        // Load GTM Script
        this.tagManagerService.load('GoogleTagManagerRenderer');
        // Fire App Loaded Event for use as a trigger in GTM
        this.vanillaTrackingService.triggerEvent('Vanilla_Portal_App_Loaded');
      }
      await this.productActivatorService.activate(product);
    }
  }

  private getProduct(route: ActivatedRouteSnapshot) {
      let node = route.root;
      let result = null;
      while (node != null) {
          if (node.data && node.data['product']) {
              result = node.data['product'];
          }
          node = node.children && node.children[0];
      }
      return result;
  }

}
