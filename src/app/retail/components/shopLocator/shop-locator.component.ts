import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, NgZone, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import environment from '@environment/oxygenEnvConfig';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { DomToolsService } from '@coreModule/services/domTools/dom.tools.service';
import { RendererService } from '@shared/services/renderer/renderer.service';

@Component({
  selector: 'shop-locator',
  templateUrl: 'shop-locator.component.html'
})
export class ShopLocatorComponent implements AfterViewInit {
  readonly title: string = 'Shop Locator';
  readonly shopLocatorUrl: SafeResourceUrl;

  @ViewChild('shopLocator') private shopLocatorView: ElementRef;

  constructor(
    private rendererService: RendererService,
    private domSanitizer: DomSanitizer,
    private zone: NgZone,
    private changeDetector: ChangeDetectorRef,
    private windowRef: WindowRefService,
    private domTools: DomToolsService
  ) {
    this.shopLocatorUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(
      environment.SHOP_LOCATOR_ENDPOINT
    );
    setTimeout(() => {
      this.zone.run(() => {
        this.changeDetector.detectChanges();
      });
    });
  }

  ngAfterViewInit(): void {
    this.windowRef.nativeWindow.setTimeout(() => this.setShopLocatorHeight(), 500);
  }

  private setShopLocatorHeight(): void {
    this.rendererService.renderer.setStyle(
      this.shopLocatorView.nativeElement,
      'min-height',
      `${this.calcFreeHeight()}px`
    );
  }

  private calcFreeHeight(): number {
    const windowHeight: number = this.windowRef.nativeWindow.innerHeight,
      headerViewHeight: number = this.domTools.HeaderEl.offsetHeight,
      footerMenuHeight: number = this.domTools.FooterEl ? this.domTools.FooterEl.offsetHeight : 0,
      contentTop: number = this.shopLocatorView.nativeElement.offsetTop;

    return windowHeight - footerMenuHeight - headerViewHeight - contentTop;
  }
}
