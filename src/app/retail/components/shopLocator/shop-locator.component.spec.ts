import { ShopLocatorComponent } from './shop-locator.component';

describe('ShopLocatorComponent', () => {
  let component: ShopLocatorComponent;
  let rendererService;
  let domSanitizer;
  let zone;
  let changeDetector;
  let windowRef;
  let domTools;

  beforeEach(() => {
    rendererService = {
      renderer: {
        setStyle: jasmine.createSpy('setStyle')
      }
    };
    domSanitizer = {
      bypassSecurityTrustResourceUrl: jasmine.createSpy('bypassSecurityTrustResourceUrl')
    };
    zone = {
      run: jasmine.createSpy('run').and.callFake(cb => {
        cb();
      })
    };
    changeDetector = {
      detectChanges: jasmine.createSpy('detectChanges')
    };
    windowRef = {
      nativeWindow: {
        setTimeout: jasmine.createSpy('setTimeout').and.callFake(cb => {
          cb();
        }),
        innerHeight: 1000
      }
    };
    domTools = {
      HeaderEl: {
        offsetHeight: 40
      },
      FooterEl: {
        offsetHeight: 40
      }
    };

    component = new ShopLocatorComponent(
      rendererService,
      domSanitizer,
      zone,
      changeDetector,
      windowRef,
      domTools
    );
  });

  it('ngAfterViewInit', done => {
    component['setShopLocatorHeight'] = jasmine.createSpy('setShopLocatorHeight');
    component.ngAfterViewInit();
    expect(domSanitizer.bypassSecurityTrustResourceUrl).toHaveBeenCalled();
    expect(windowRef.nativeWindow.setTimeout).toHaveBeenCalled();
    expect(component['setShopLocatorHeight']).toHaveBeenCalled();
    done();
  });

  it('setShopLocatorHeight', () => {
    component['calcFreeHeight'] = jasmine.createSpy('calcFreeHeight').and.returnValue('500');
    component['shopLocatorView'] = {
      nativeElement: {}
    };
    component['setShopLocatorHeight']();
    expect(rendererService.renderer.setStyle).toHaveBeenCalledWith(
      jasmine.any(Object),
      'min-height',
      jasmine.any(String)
    );
    expect(component['calcFreeHeight']).toHaveBeenCalled();
  });

  describe('calcFreeHeight', () => {
    beforeEach(() => {
      component['shopLocatorView'] = { nativeElement: { offsetTop: 0 } };
    });

    it('should return height (with footer)', () => {
      expect(component['calcFreeHeight']()).toBe(920);
    });

    it('should return height (without footer)', () => {
      domTools.FooterEl = null;
      expect(component['calcFreeHeight']()).toBe(960);
    });
  });
});
