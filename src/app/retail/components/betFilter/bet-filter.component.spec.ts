
import { of as observableOf } from 'rxjs';
import { tick, fakeAsync } from '@angular/core/testing';
import { ChildActivationStart, ChildActivationEnd } from '@angular/router';

import { BetFilterComponent } from '@app/retail/components/betFilter/bet-filter.component';

describe('BetFilterComponent ', () => {
  let component: BetFilterComponent,
    windowRef,
    asyncLoad,
    betFilterParams,
    router,
    routingState,
    deviceService,
    commandService;

  beforeEach(() => {
    windowRef = {
      document: {
        dispatchEvent: jasmine.createSpy('dispatchEvent'),
        addEventListener: jasmine.createSpy('addEventListener'),
        removeEventListener: jasmine.createSpy('removeEventListener')
      }
    };
    asyncLoad = {
      loadJsFile: jasmine.createSpy('loadJsFile').and.returnValue(observableOf(null)),
      loadCssFile: jasmine.createSpy('loadCssFile').and.returnValue(observableOf(null))
    };
    betFilterParams = {
      chooseMode: jasmine.createSpy('chooseMode').and.returnValue(observableOf({}))
    };
    router = {
      events: observableOf([]),
      navigate: jasmine.createSpy('navigate'),
    };
    routingState = {
      getCurrentSegment: jasmine.createSpy('getCurrentSegment'),
      getPreviousSegment: jasmine.createSpy('getPreviousSegment')
    };
    deviceService = {};
    commandService = {
      executeAsync: jasmine.createSpy('executeAsync'),
      API: {
        ADD_TO_BETSLIP_BY_OUTCOME_IDS: '@betslipModule/betslip.module#BetslipModule:addToBetSlip'
      }
    };

    component = new BetFilterComponent(
      windowRef,
      asyncLoad,
      betFilterParams,
      router,
      routingState,
      deviceService,
      commandService,
    );
    component['tryBootstrapBetFilter']({ mode: 'online' });
  });

  describe('#tryBootstrapBetFilter', () => {
    it('and should use stickyElements true in NOT desktop mode', () => {
      component['deviceService'].isDesktop = false;
      component['tryBootstrapBetFilter']({ mode: 'online' });

      expect(windowRef.document.dispatchEvent)
        .toHaveBeenCalledWith(jasmine.objectContaining({ detail: { mode: 'online', stickyElements: true } }));
    });

    it('and should use stickyElements false in desktop mode', () => {
      component['deviceService'].isDesktop = true;
      component['tryBootstrapBetFilter']({ mode: 'online' });

      expect(windowRef.document.dispatchEvent)
        .toHaveBeenCalledWith(jasmine.objectContaining({ detail: { mode: 'online', stickyElements: false } }));
    });
  });

  describe('ngOnInit', () => {
    it('should navigate to "/" (canceled)', fakeAsync(() => {
      betFilterParams.chooseMode.and.returnValue(observableOf({ cancelled: true }));
      component.ngOnInit();
      tick();
      expect(betFilterParams.chooseMode).toHaveBeenCalledTimes(1);
      expect(router.navigate).toHaveBeenCalledWith(['/']);
    }));

    it('should navigate to "/" (cannot bootstrap params)', fakeAsync(() => {
      component.ngOnInit();
      tick();
      expect(betFilterParams.chooseMode).toHaveBeenCalledTimes(1);
      expect(router.navigate).toHaveBeenCalledWith(['/']);
    }));

    it('should not subscribe to choose mode', () => {
      betFilterParams.params = { mode: 'online' };
      component.ngOnInit();
      expect(betFilterParams.chooseMode).not.toHaveBeenCalled();
    });

    it('should not navigate to "/"', fakeAsync(() => {
      betFilterParams.chooseMode.and.returnValue(observableOf({ mode: 'online' }));
      component.ngOnInit();
      tick();
      expect(betFilterParams.chooseMode).toHaveBeenCalledTimes(1);
      expect(router.navigate).not.toHaveBeenCalled();
    }));

    it('should listen to "BF_ADD_TO_BETSLIP"', () => {
      component.ngOnInit();
      expect(windowRef.document.addEventListener).toHaveBeenCalledWith('BF_ADD_TO_BETSLIP', jasmine.any(Function));
    });
  });

  describe('ngOnInit (DESTROY_BET_FILTER)', () => {
    beforeEach(() => {
      windowRef.document.dispatchEvent.calls.reset();
    });

    it('should dispatch event', fakeAsync(() => {
      router.events = observableOf(new ChildActivationStart({} as any));
      routingState.getCurrentSegment.and.returnValue('');
      routingState.getPreviousSegment.and.returnValue('betFilter');

      component.ngOnInit();
      tick();

      expect(windowRef.document.dispatchEvent).toHaveBeenCalledWith(
        jasmine.objectContaining({ type: 'DESTROY_BET_FILTER' })
      );
    }));

    it('should not dispatch event (event instance mismatch)', fakeAsync(() => {
      router.events = observableOf(new ChildActivationEnd({} as any));
      component.ngOnInit();
      tick();
      expect(windowRef.document.dispatchEvent).not.toHaveBeenCalled();
    }));

    it('should not dispatch event (segments the same)', fakeAsync(() => {
      router.events = observableOf(new ChildActivationStart({} as any));
      routingState.getCurrentSegment.and.returnValue('');
      routingState.getPreviousSegment.and.returnValue('');
      component.ngOnInit();
      tick();
      expect(windowRef.document.dispatchEvent).not.toHaveBeenCalled();
    }));

    it('should not dispatch event (prev segment is not "betFilter")', fakeAsync(() => {
      router.events = observableOf(new ChildActivationStart({} as any));
      routingState.getCurrentSegment.and.returnValue('');
      routingState.getPreviousSegment.and.returnValue('home');
      component.ngOnInit();
      tick();
      expect(windowRef.document.dispatchEvent).not.toHaveBeenCalled();
    }));
  });

  describe('#ngOnDestroy', () => {
    it('should unsubscribe', () => {
      component['routeChangeStartHandler'] = { unsubscribe: jasmine.createSpy() } as any;
      component.ngOnDestroy();

      expect(windowRef.document.removeEventListener).toHaveBeenCalledWith('BF_ADD_TO_BETSLIP', jasmine.any(Function));
      expect(component['routeChangeStartHandler'].unsubscribe).toHaveBeenCalled();
    });
  });

  it('@handleAddToBetslip', () => {
    const event = { detail: { ids: ['123', '123'] } } as any;
    component['handleAddToBetslip'](event);

    expect(commandService.executeAsync).toHaveBeenCalledWith(commandService.API.ADD_TO_BETSLIP_BY_OUTCOME_IDS,
      [event.detail.ids, true, true, false]);
  });

});
