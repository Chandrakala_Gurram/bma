import { forkJoin as observableForkJoin, Subscription } from 'rxjs';
import { ChangeDetectorRef, Component, Input, OnInit, OnDestroy } from '@angular/core';
import { AsyncScriptLoaderService } from '@core/services/asyncScriptLoader/async-script-loader.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import environment from '@environment/oxygenEnvConfig';
import { UserService } from '@core/services/user/user.service';
import { IBetTrackerParams } from '@core/models/bet-tracker-params.model';
import { UpgradeAccountProviderService } from '@app/retail/services/upgradeAccountProvider/upgrade-account-provider.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { BET_TRACKER } from '@app/retail/constants/retail.constant';
import { IGetIdTokenInfoFromUserNameResponseModel } from '@retail/services/upgradeAccountProvider/upgrade-account-provider.model';

@Component({
  selector: 'bet-tracker',
  templateUrl: 'bet-tracker.component.html',
  styleUrls: ['bet-tracker.component.less']
})

export class BetTrackerComponent implements OnInit, OnDestroy {

  @Input() public mode?: string;
  isBetTrackerLoaded: boolean = false;
  readonly BET_HISTORY = 'BET_HISTORY';
  private upgradeAccountSubscribe: Subscription;
  private bootstrapBetSubscribe: Subscription;

  constructor(
    private asyncLoad: AsyncScriptLoaderService,
    private windowRef: WindowRefService,
    private upgradeAccountProviderService: UpgradeAccountProviderService,
    private userService: UserService,
    private pubSubService: PubSubService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.changeDetectorRef.detach();
    this.initBetTracker();
    this.pubSubService.subscribe('betTrackerComponent', this.pubSubService.API.SUCCESSFUL_LOGIN, () => {
      this.initBetTracker();
    });
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe('betTrackerComponent');
    this.upgradeAccountSubscribe && this.upgradeAccountSubscribe.unsubscribe();
    this.bootstrapBetSubscribe && this.bootstrapBetSubscribe.unsubscribe();
  }

  get userStatus(): boolean {
    return this.userService.status;
  }

  private initBetTracker(): void {
    if (this.userService.isMultiChannelUser() && !this.userService.cardNumber) {
      this.upgradeAccountSubscribe = this.upgradeAccountProviderService.getRequest<IGetIdTokenInfoFromUserNameResponseModel>(
        'getidtokeninfofromusername',
        {'username': this.userService.username},
        environment.APOLLO.UCMS_ROUTE
      ).subscribe(
        (resp: IGetIdTokenInfoFromUserNameResponseModel) => {
          if (resp.data.printedTokenCode) {
            this.userService.set({ cardNumber: resp.data.printedTokenCode});
          }
          this.bootstrapBetTracker();
        });
    } else {
      this.bootstrapBetTracker();
    }
  }

  /** initialize Bet Tracker with specific params
   */
  private bootstrapBetTracker() {
    this.bootstrapBetSubscribe = observableForkJoin([
      this.asyncLoad.loadJsFile(`${environment.BET_TRACKER_ENDPOINT}main.bundle.js`),
      this.asyncLoad.loadCssFile(`${environment.BET_TRACKER_ENDPOINT}main.css`)
    ]).subscribe(() => {
      this.isBetTrackerLoaded = true;
      this.changeDetectorRef.detectChanges();
      this.windowRef.document.dispatchEvent(
        new CustomEvent(BET_TRACKER.BOOTSTRAP_BET_TRACKER, { detail: {
            mode: this.mode,
            username: this.userService.username,
            cardNumber: this.userService.cardNumber
          } as IBetTrackerParams })
      );
    });
  }
}
