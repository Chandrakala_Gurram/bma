import { forkJoin as observableForkJoin, of, Subscription } from 'rxjs';

import { BetTrackerComponent } from './bet-tracker.component';
import environment from '@environment/oxygenEnvConfig';

describe('BetTrackerComponent', () => {
  let component: BetTrackerComponent;
  let asyncLoad;
  let windowRef;
  let upgradeAccountProviderService;
  let userService;
  let pubSubService;
  let changeDetectorRef;

  beforeEach(() => {
    asyncLoad = {
      loadJsFile: jasmine.createSpy().and.returnValue(of({})),
      loadCssFile: jasmine.createSpy().and.returnValue(of({}))
    };
    windowRef = {
      document: { dispatchEvent: jasmine.createSpy('dispatchEvent') }
    };
    upgradeAccountProviderService = {
      getRequest: jasmine.createSpy().and.returnValue(of({ data: { printedTokenCode: '12345678' } }))
    };
    userService = {
      isMultiChannelUser: () => true,
      cardNumber: '',
      username: 'userName',
      set: jasmine.createSpy(),
      status: true
    };
    pubSubService = {
      subscribe: jasmine.createSpy(),
      unsubscribe: jasmine.createSpy(),
      API: {
        SUCCESSFUL_LOGIN: 'SUCCESSFUL_LOGIN'
      }
    };
    changeDetectorRef = {
      detectChanges: jasmine.createSpy('detectChanges'),
      detach: jasmine.createSpy('detach'),
    };
  });

  beforeEach(() => {
    environment.APOLLO = { UCMS_ROUTE: 'ucms_api_index.php' } as any;
    component = new BetTrackerComponent(
      asyncLoad,
      windowRef,
      upgradeAccountProviderService,
      userService,
      pubSubService,
      changeDetectorRef
    );
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      component['initBetTracker'] = jasmine.createSpy('initBetTracker');
    });

    it('should call initBetTracker', () => {
      component.ngOnInit();

      expect(component['initBetTracker']).toHaveBeenCalled();
    });

    it('should subscribe to betTrackerComponent SUCCESSFUL_LOGIN', () => {
      pubSubService.subscribe.and.callFake((a, b, cb) => cb());

      component.ngOnInit();

      expect(pubSubService.subscribe).toHaveBeenCalledWith(
        'betTrackerComponent',
        pubSubService.API.SUCCESSFUL_LOGIN,
        jasmine.any(Function));
      expect(component['initBetTracker']).toHaveBeenCalled();
    });
  });

  describe('ngOnDestroy', () => {
    it('should unsubscribe from betTrackerComponent', () => {
      component['upgradeAccountSubscribe'] = new Subscription();
      component['bootstrapBetSubscribe'] = new Subscription();
      component['upgradeAccountSubscribe'].unsubscribe = jasmine.createSpy('unsubscribe');
      component['bootstrapBetSubscribe'].unsubscribe = jasmine.createSpy('unsubscribe');
      component.ngOnDestroy();

      expect(component['pubSubService'].unsubscribe).toHaveBeenCalled();
      expect(component['upgradeAccountSubscribe'].unsubscribe).toHaveBeenCalled();
      expect(component['bootstrapBetSubscribe'].unsubscribe).toHaveBeenCalled();
    });

    it('if subscription is not defined', () => {
      component['upgradeAccountSubscribe'] = null;
      component['bootstrapBetSubscribe'] = null;
      expect(() => {
        component.ngOnDestroy();
      }).not.toThrow();
    });
  });

  describe('userStatus', () => {
    it('should return user status', () => {
      const result = component.userStatus;

      expect(result).toBeTruthy();
    });
  });

  describe('#initBetTracker', () => {
    beforeEach(() => {
      component['bootstrapBetTracker'] = jasmine.createSpy();
    });

    it('should do call to APPOLO Service and run bootstrapBetTracker and set cardNumber and if user is MultiChannel and NOT have ' +
      'cardNumber', () => {
      component['initBetTracker']();

      expect(component['upgradeAccountProviderService'].getRequest).toHaveBeenCalledWith(
        'getidtokeninfofromusername',
        {'username': 'userName'},
        'ucms_api_index.php'
      );
      expect(component['userService'].set).toHaveBeenCalledWith({ cardNumber: '12345678'});
      expect(component['bootstrapBetTracker']).toHaveBeenCalledTimes(1);
    });

    it('should do call to APPOLO Service and run bootstrapBetTracker and if user is MultiChannel and NOT have cardNumber', () => {
      upgradeAccountProviderService.getRequest.and.returnValue(of({ data: { printedTokenCode: undefined } }));

      component['initBetTracker']();

      expect(component['upgradeAccountProviderService'].getRequest).toHaveBeenCalledWith(
        'getidtokeninfofromusername',
        {'username': 'userName'},
        'ucms_api_index.php'
      );
      expect(component['userService'].set).not.toHaveBeenCalledWith({ cardNumber: '12345678'});
      expect(component['bootstrapBetTracker']).toHaveBeenCalledTimes(1);
    });

    it('should run bootstrapBetTracker if user is MultiChannel and have cardNumber', () => {
      // @ts-ignore
      component['userService'] = { isMultiChannelUser: () => true, cardNumber: '123' };
      component['initBetTracker']();

      expect(component['bootstrapBetTracker']).toHaveBeenCalledTimes(1);
    });

    it('should run bootstrapBetTracker if user is NOT MultiChannel and have cardNumber', () => {
      // @ts-ignore
      component['userService'] = { isMultiChannelUser: () => false, cardNumber: '123' };
      component['initBetTracker']();

      expect(component['bootstrapBetTracker']).toHaveBeenCalledTimes(1);
    });

  });

  describe('bootstrapBetTracker', () => {
    it('should return user status', () => {
      const result = component.userStatus;

      expect(result).toBeTruthy();
    });
  });

  describe('bootstrapBetTracker', () => {
    it('should return user status', () => {
      component['bootstrapBetTracker']();

      observableForkJoin([
        component['asyncLoad'].loadJsFile(''),
        component['asyncLoad'].loadCssFile('')
      ]).subscribe(null, null, () => {
        expect(component['windowRef'].document.dispatchEvent).toHaveBeenCalled();
      });
    });
  });
});
