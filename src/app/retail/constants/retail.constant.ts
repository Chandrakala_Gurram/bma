export const BET_FILTER = {
  BOOTSTRAP_BET_FILTER: 'BOOTSTRAP_BET_FILTER',
  DESTROY_BET_FILTER: 'DESTROY_BET_FILTER',
  BF_ADD_TO_BETSLIP: 'BF_ADD_TO_BETSLIP'
};

export const UPGRADE_ACCOUNT_DIALOG = {
  inshopUpgrade: {
    dialogBody: 'inshop-upgrade-dialog-body'
  }
};

export const RETAIL_PAGE = {
  title: 'Connect',
  bannersPage: 'retail',
  trackingLocation: 'connect'
};

export const BET_FILTER_DIALOG = {
  modes: {
    online: 'online',
    inshop: 'inshop'
  }
};

export const BET_TRACKER = {
  BOOTSTRAP_BET_TRACKER: 'BOOTSTRAP_BET_TRACKER',
  DESTROY_BET_TRACKER: 'DESTROY_BET_TRACKER'
};

export const UPGRADE_ACCOUNT_MENU_ITEMS: string[] = [
  'deposit',
  'deposit/registered',
  'withdraw',
  'freebets',
];

// config constants
export type RETAIL_MENU_LOCATION = 'HUB' | 'RHM' | 'AZ';
export type UPGRADE_DIALOG_LOCATION = 'LOGIN' | 'HUB';

