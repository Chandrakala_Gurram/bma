import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BetFilterComponent } from '@platform/retail/components/betFilter/bet-filter.component';
import { ModalModule } from 'angular-custom-modal';

import { SharedModule } from '@sharedModule/shared.module';
import { RetailRoutingModule } from '@app/retail/retail-routing.module';
import { RetailPageComponent } from '@platform/retail/components/retailPage/retail-page.component';
import { ShopLocatorComponent } from '@app/retail/components/shopLocator/shop-locator.component';
import { BetFilterDialogComponent } from '@app/retail/components/betFilterDialog/bet-filter-dialog.component';
import { BetTrackerComponent } from '@app/retail/components/betTracker/bet-tracker.component';
import { DYNAMIC_BET_TRACKER } from '@app/dynamicLoader/dynamic-loader-manifest';
import { RetailRunService } from '@app/retail/services/retailRun/retail-run.service';
import { BannersModule } from '@banners/banners.module';

@NgModule({
  imports: [
    HttpClientModule,
    ReactiveFormsModule,
    ModalModule,
    SharedModule,
    RetailRoutingModule,
    BannersModule
  ],
  providers: [
    { provide: DYNAMIC_BET_TRACKER, useValue: BetTrackerComponent }
  ],
  declarations: [
    RetailPageComponent,
    ShopLocatorComponent,
    BetFilterComponent,
    BetTrackerComponent,
    BetFilterDialogComponent,
  ],
  entryComponents: [
    RetailPageComponent,
    ShopLocatorComponent,
    BetTrackerComponent,
    BetFilterComponent,
    BetFilterDialogComponent,
  ],
  exports: [
    BetFilterDialogComponent,
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class RetailModule {
  constructor(private retailRunService: RetailRunService) {
    this.retailRunService.run();
  }
}
