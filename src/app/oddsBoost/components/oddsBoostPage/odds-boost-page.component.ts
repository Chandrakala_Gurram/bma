import { Component, OnDestroy, OnInit } from '@angular/core';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { UserService } from '@core/services/user/user.service';
import { OddsBoostService } from '@oddsBoostModule/services/odds-boost.service';
import { IFreebetToken } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { IOddsBoostConfig } from '@core/services/cms/models';
import * as _ from 'underscore';
import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'odds-boost-page',
  templateUrl: './odds-boost-page.component.html',
  styleUrls: ['./odds-boost-page.component.less']
})
export class OddsBoostPageComponent extends AbstractOutletComponent implements OnInit, OnDestroy {
  isLoggedIn: boolean;
  availableBoosts: IFreebetToken[] = [];
  upcomingBoosts: IFreebetToken[] = [];
  config: IOddsBoostConfig;
  pageTitle: string;
  loggedInHeaderText: SafeHtml;
  loggedOutHeaderText: SafeHtml;
  termsAndConditionsText: SafeHtml;
  nextBoostDate: Date;

  private readonly title = 'oddsBoost';
  private oddsBoostSubscription: Subscription;

  constructor(
    private pubSubService: PubSubService,
    private userService: UserService,
    private oddsBoostService: OddsBoostService,
    private cmsService: CmsService,
    private locale: LocaleService,
    private domSanitizer: DomSanitizer
  ) {
    super()/* istanbul ignore next */;
    this.isLoggedIn = userService.status;
    this.pubSubService.subscribe(
        this.title,
        [this.pubSubService.API.SUCCESSFUL_LOGIN, this.pubSubService.API.SESSION_LOGOUT],
        this.sessionStatusChange.bind(this)
    );
    this.pubSubService.subscribe(this.title, 'STORE_FREEBETS', this.getOddsBoosts.bind(this));
    this.pageTitle = this.locale.getString('oddsboost.page.title');
  }

  ngOnInit(): void {
    this.showSpinner();
    this.getContent();
    if (this.isLoggedIn) {
      this.getOddsBoosts();
    } else {
      this.hideSpinner();
    }
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe(this.title);
    this.unsubscribeOddsBoost();
  }

  openLoginDialog(): void {
    this.pubSubService.publish(this.pubSubService.API.OPEN_LOGIN_DIALOG, { moduleName: 'oddsboost' });
  }

  private unsubscribeOddsBoost(): void {
    this.oddsBoostSubscription && this.oddsBoostSubscription.unsubscribe();
  }

  private getContent(): void {
    this.cmsService.getOddsBoost().subscribe((config: IOddsBoostConfig) => {
      this.config = config;
      this.loggedInHeaderText = this.domSanitizer.bypassSecurityTrustHtml(this.config.loggedInHeaderText);
      this.loggedOutHeaderText = this.domSanitizer.bypassSecurityTrustHtml(this.config.loggedOutHeaderText);
      this.termsAndConditionsText = this.domSanitizer.bypassSecurityTrustHtml(this.config.termsAndConditionsText);
    }, () => this.showError());
  }

  private sessionStatusChange(): void {
    this.isLoggedIn = this.userService.status;
    if (this.isLoggedIn) {
      this.getOddsBoosts();
    }
  }

  private getOddsBoosts(): void {
    this.unsubscribeOddsBoost();
    this.oddsBoostSubscription = this.oddsBoostService.getOddsBoostTokens().subscribe((data: IFreebetToken[]) => {
      const nowDate = new Date();
      const allOddsBoosts = _.partition(data, (oddBoost: IFreebetToken) => {
        oddBoost.freebetTokenExpiryDate = this.formatDate(oddBoost.freebetTokenExpiryDate);
        oddBoost.freebetTokenStartDate = this.formatDate(oddBoost.freebetTokenStartDate);

        return nowDate > new Date(oddBoost.freebetTokenStartDate);
      });

      this.availableBoosts = allOddsBoosts[0];
      this.upcomingBoosts = allOddsBoosts[1];
      this.hideSpinner();
      this.setUpcomingBoostTimer();
    }, () => this.showError());
  }

  private setUpcomingBoostTimer(): void {
    if (this.upcomingBoosts.length) {
      this.nextBoostDate = new Date(this.getNextUpcomingBoost(this.upcomingBoosts).freebetTokenStartDate);
    }
  }

  private getNextUpcomingBoost(oddsBoosts: IFreebetToken[]): IFreebetToken {
    let nextBoost = oddsBoosts[0];

    _.each(oddsBoosts, (oddsBoost: IFreebetToken) => {
      const oddsBoostStartDate = new Date(oddsBoost.freebetTokenStartDate);
      const nextBoostStartDate = new Date(nextBoost.freebetTokenStartDate);

      nextBoost = oddsBoostStartDate < nextBoostStartDate ? oddsBoost : nextBoost;
    });
    return nextBoost;
  }

  private formatDate(date: string): string {
    return date.replace(/-/g, '/');
  }
}
