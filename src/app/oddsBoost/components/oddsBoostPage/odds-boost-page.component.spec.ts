import { of as observableOf, throwError } from 'rxjs';
import { OddsBoostPageComponent } from './odds-boost-page.component';
import { fakeAsync, tick } from '@angular/core/testing';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('OddsBoostPageComponent', () => {
  const title = 'oddsBoost';

  let component: OddsBoostPageComponent;
  let userService;
  let oddsBoostService;
  let cmsService;
  let localeService;
  let sessionStatusCallback;
  let domSanitizer;
  let pubSubService;

  beforeEach(() => {
    pubSubService = {
      subscribe: jasmine.createSpy('subscribe').and.callFake((file, method, callback) => {
        if (method !== 'STORE_FREEBETS') {
          sessionStatusCallback = callback;
        }
      }),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      publish: jasmine.createSpy('publish'),
      API: pubSubApi
    };
    userService = {
      status: true
    };
    oddsBoostService = {
      getOddsBoostTokens: jasmine.createSpy('getOddsBoostTokens').and.returnValue(observableOf([{
        freebetTokenExpiryDate: new Date().toISOString(),
        freebetTokenStartDate: new Date().toISOString()
      }]))
    };
    cmsService = {
      getOddsBoost: jasmine.createSpy('getOddsBoost').and.returnValue(observableOf({
        termsAndConditionsText: 'terms',
        loggedInHeaderText: 'in',
        loggedOutHeaderText: 'out'
      }))
    };
    localeService = {
      getString: jasmine.createSpy().and.returnValue('Odds Boost')
    };
    domSanitizer = {
      bypassSecurityTrustHtml: jasmine.createSpy()
    };

    component = new OddsBoostPageComponent(
      pubSubService,
      userService,
      oddsBoostService,
      cmsService,
      localeService,
      domSanitizer
    );
    spyOn(component, 'showSpinner').and.callThrough();
    spyOn(component, 'hideSpinner').and.callThrough();
  });

  it('constructor', () => {
    expect(component).toBeDefined();
    expect(pubSubService.subscribe).toHaveBeenCalledWith(
        title, [pubSubService.API.SUCCESSFUL_LOGIN, pubSubService.API.SESSION_LOGOUT], jasmine.any(Function)
    );
  });

  it('should get oddsboost on init', () => {
    component.ngOnInit();
    expect(cmsService.getOddsBoost).toHaveBeenCalled();
    expect(oddsBoostService.getOddsBoostTokens).toHaveBeenCalled();
  });

  it('should not get oddsboost on init if not logged in', () => {
    userService.status = false;
    sessionStatusCallback();
    component.ngOnInit();
    expect(oddsBoostService.getOddsBoostTokens).not.toHaveBeenCalled();
    expect(component.hideSpinner).toHaveBeenCalled();
  });

  it('should show spinner on init and hide on get odds boost success', fakeAsync(() => {
    userService.status = true;
    sessionStatusCallback();
    component.ngOnInit();
    expect(component.showSpinner).toHaveBeenCalled();
    expect(oddsBoostService.getOddsBoostTokens).toHaveBeenCalled();
    tick();
    expect(component.hideSpinner).toHaveBeenCalled();
  }));

  it('should hide spinner when getOddBoostToken api fails', () => {
    component['oddsBoostService'].getOddsBoostTokens = jasmine.createSpy().and.returnValue(throwError(new Error('Fake error')));
    component.ngOnInit();
    expect(component.hideSpinner).toHaveBeenCalled();
  });

  it('should open login dialog', () => {
    component.openLoginDialog();
    expect(pubSubService.publish).toHaveBeenCalledWith(pubSubService.API.OPEN_LOGIN_DIALOG, jasmine.objectContaining(
      {
        moduleName: 'oddsboost'
      }
    ));
  });

  it('should subscribe login/logout events', () => {
    userService.status = false;
    sessionStatusCallback();
    expect(component.isLoggedIn).toBe(false);

    userService.status = true;
    sessionStatusCallback();
    expect(component.isLoggedIn).toBe(true);
  });

  it('setUpcomingBoostTimer', () => {
    component['getNextUpcomingBoost'] = jasmine.createSpy().and.returnValue({});
    component.upcomingBoosts = [{}] as any;
    component['setUpcomingBoostTimer']();
    expect(component['getNextUpcomingBoost']).toHaveBeenCalled();
  });

  it('setUpcomingBoostTimer no upcoming boosts', () => {
    component['getNextUpcomingBoost'] = jasmine.createSpy();
    component.upcomingBoosts = [] as any;
    component['setUpcomingBoostTimer']();
    expect(component['getNextUpcomingBoost']).not.toHaveBeenCalled();
  });

  describe('@ngOnDestroy', () => {
    beforeEach(() => {
      component.ngOnInit();
    });
    it('should unbind subscriptions if oddsBoostSubscription was defined', () => {
      spyOn<any>(component['oddsBoostSubscription'], 'unsubscribe');
      component.ngOnDestroy();
      expect(component['oddsBoostSubscription'].unsubscribe).toHaveBeenCalled();
      expect(pubSubService.unsubscribe).toHaveBeenCalled();
    });

    it('should not unsubscibe oddsBoostSubscription if not defined', () => {
      component['oddsBoostSubscription'] = null;
      component.ngOnDestroy();
      expect(component['oddsBoostSubscription']).toBeFalsy();
      expect(pubSubService.unsubscribe).toHaveBeenCalled();
    });
  });

  it('getContent shoud show error', () => {
    spyOn(component, 'showError');
    cmsService.getOddsBoost.and.returnValue(throwError(null));
    component['getContent']();
    expect(component.showError).toHaveBeenCalledTimes(1);
  });

  it('getNextUpcomingBoost', () => {
    const tokens: any = [
      { freebetTokenStartDate: '2020-04-08T13:20:00.000Z' },
      { freebetTokenStartDate: '2020-04-08T13:10:00.000Z' },
    ];
    expect(component['getNextUpcomingBoost'](tokens)).toEqual(tokens[1]);
  });
});
