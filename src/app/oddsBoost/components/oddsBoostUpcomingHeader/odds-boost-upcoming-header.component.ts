import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'odds-boost-upcoming-header',
  templateUrl: './odds-boost-upcoming-header.component.html',
  styleUrls: ['./odds-boost-upcoming-header.component.less']
})
export class OddsBoostUpcomingHeaderComponent implements OnChanges {
  @Input() countDownDate: Date;
  countDown: string;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.countDownDate.currentValue) {
      this.initCountdown(changes.countDownDate.currentValue);
    }
  }

  private initCountdown(date: Date): void {
    const countDownDate = date.getTime();
    const second = 1000;
    const minute = 1000 * 60;
    const hour = 1000 * 60 * 60;

    const timer = setInterval(() => {
      const now = new Date().getTime();
      const difference = countDownDate - now;

      if (difference > 0) {
        const hours = this.padNumber(Math.floor((difference % (hour * 24)) / hour));
        const minutes = this.padNumber(Math.floor((difference % hour) / minute));
        const seconds = this.padNumber(Math.floor((difference % minute) / second));

        this.countDown = `${hours}:${minutes}:${seconds}`;
      } else {
        clearInterval(timer);
        this.countDown = '00:00:00';
      }
    }, 1000);
  }

  private padNumber(number: number): string {
    return number < 10 ? `0${number}` : `${number}`;
  }
}
