import { fakeAsync, tick, discardPeriodicTasks } from '@angular/core/testing';
import { SimpleChange, SimpleChanges } from '@angular/core';
import { OddsBoostUpcomingHeaderComponent } from './odds-boost-upcoming-header.component';

describe('OddsBoostUpcomingHeaderComponent', () => {
  let component;

  beforeEach(() => {
    component = new OddsBoostUpcomingHeaderComponent();
  });

  it('should init countdown', fakeAsync(() => {
    const timerRegex = /^(?:2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]$/;
    const date: Date = new Date('10/10/2025');
    const changesObj: SimpleChanges = {
      countDownDate: new SimpleChange(date, date, false)
    };
    component.ngOnChanges(changesObj);
    tick(2000);
    expect(timerRegex.test(component.countDown)).toBe(true);
    discardPeriodicTasks();
  }));

  it('shoud not init countdown', () => {
    spyOn(component, 'initCountdown');
    component.ngOnChanges({ countDownDate: {} });
    expect(component.initCountdown).not.toHaveBeenCalled();
  });

  it('should stop countdown', fakeAsync(() => {
    const date: Date = new Date();
    date.setSeconds(date.getSeconds() + 1);
    const changesObj: SimpleChanges = {
      countDownDate: new SimpleChange(date, date, false)
    };
    component.ngOnChanges(changesObj);
    tick(3000);
    expect(component.countDown).toBe('00:00:00');
    discardPeriodicTasks();
  }));
});
