import { of as observableOf } from 'rxjs';
import { OddsBoostListComponent } from './odds-boost-list.component';
import { SimpleChange, SimpleChanges } from '@angular/core';
import { IFreebetToken } from '@app/bpp/services/bppProviders/bpp-providers.model';

describe('OddsBoostListComponent', () => {
  let component: OddsBoostListComponent;
  let freeBetsService;
  let oddsBoostService;
  let userService;
  let currencyPipe;

  beforeEach(() => {
    freeBetsService = {
      getOddsBoostsWithCategories: jasmine.createSpy('getOddsBoostsWithCategories').and.returnValue(observableOf(
        [{
          categoryId: '16',
          freebetTokenId: '10',
          freebetMaxStake: '5.00',
          freebetTokenDisplayText: 'asd|asd'
        }]
      ))
    };

    oddsBoostService = {
      sortPageTokens: jasmine.createSpy('sortPageTokens')
    };

    userService = {
      currencySymbol: '£'
    };
    currencyPipe = {
      transform: jasmine.createSpy('transform').and.returnValue('£50')
    };
    component = new OddsBoostListComponent(freeBetsService, oddsBoostService, userService, currencyPipe);
  });

  it('constructor', () => {
    expect(component).toBeDefined();
  });

  it('ngOnChanges', () => {
    const changesObj: SimpleChanges = {
      oddsBoosts: new SimpleChange(undefined, [{freebetTokenId: '126'}], false)
    };
    component.ngOnChanges(changesObj);
    expect(freeBetsService.getOddsBoostsWithCategories).toHaveBeenCalled();
    expect(currencyPipe.transform).toHaveBeenCalledTimes(1);
    expect(currencyPipe.transform).toHaveBeenCalledWith('5.00', userService.currencySymbol, 'code');
    expect(oddsBoostService.sortPageTokens).toHaveBeenCalledTimes(1);
  });

  it('ngOnChanges no tokens', () => {
    const changesObj: SimpleChanges = {
      oddsBoosts: new SimpleChange(undefined, null, false)
    };
    component.ngOnChanges(changesObj);
    expect(freeBetsService.getOddsBoostsWithCategories).not.toHaveBeenCalled();
  });

  it('ngOnChanges no category id', () => {
    freeBetsService.getOddsBoostsWithCategories.and.returnValue(observableOf([{
        freebetMaxStake: '50',
        freebetTokenId: '10',
        freebetTokenDisplayText: 'asd|asd'
      }]));

    const changesObj: SimpleChanges = {
      oddsBoosts: new SimpleChange(undefined, [{freebetTokenId: '126'}], false)
    };
    component.ngOnChanges(changesObj);
    expect(oddsBoostService.sortPageTokens).toHaveBeenCalledWith([{
      freebetMaxStake: '£50',
      freebetOfferDesc: 'asd',
      freebetOfferName: 'asd',
      freebetTokenDisplayText: 'asd|asd',
      freebetTokenId: '10'
    }]);
  });

  it('ngOnChanges should not call "transform"', () => {

    freeBetsService.getOddsBoostsWithCategories.and.returnValue(observableOf([{
        freebetMaxStake: '£50',
        freebetTokenDisplayText: 'asd|asd',
      }]));

    const changesObj: SimpleChanges = {
      oddsBoosts: new SimpleChange(undefined, [{freebetTokenId: '126'}], false)
    };

    component.ngOnChanges(changesObj);

    expect(currencyPipe.transform).not.toHaveBeenCalled();
  });

  it('ngOnChanges should call "transform"', () => {

    freeBetsService.getOddsBoostsWithCategories.and.returnValue(observableOf([{
        freebetMaxStake: 5,
        freebetTokenDisplayText: 'asd|asd',
      }]));

    const changesObj: SimpleChanges = {
      oddsBoosts: new SimpleChange(undefined, [{freebetTokenId: '126'}], false)
    };

    component.ngOnChanges(changesObj);

    expect(currencyPipe.transform).toHaveBeenCalledWith('5.00', '£', 'code');
  });

  it('should get objectKeys', () => {
    expect(component.objectKeys({a: 1, b: 2})).toEqual(['a', 'b']);
  });

  it('should trackByOddsBoosts', () => {
    expect(component.trackByOddsBoosts(2, <IFreebetToken>{freebetTokenId: '100001'})).toEqual('2100001');
  });

  it('should trackByCategory', () => {
    expect(component.trackByCategory(1, '32')).toEqual('1_32');
  });

  it('getOddsBoostsWithCategories, no data', () => {
    freeBetsService.getOddsBoostsWithCategories.and.returnValue(observableOf(undefined));

    const changesObj: SimpleChanges = {
      oddsBoosts: new SimpleChange(undefined, [{freebetTokenId: '126'}], false)
    };
    component.ngOnChanges(changesObj);

    expect(oddsBoostService.sortPageTokens).not.toHaveBeenCalled();
  });
});
