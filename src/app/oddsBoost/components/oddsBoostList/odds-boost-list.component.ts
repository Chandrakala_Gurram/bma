import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import * as _ from 'underscore';
import { FreeBetsService } from '@coreModule/services/freeBets/free-bets.service';
import { OddsBoostService } from '@oddsBoostModule/services/odds-boost.service';

import { CurrencyPipe } from '@angular/common';
import { IFreebetToken } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { map } from 'rxjs/operators';
import { UserService } from '@core/services/user/user.service';

@Component({
  selector: 'odds-boost-list',
  templateUrl: './odds-boost-list.component.html',
  styleUrls: ['./odds-boost-list.component.less']
})
export class OddsBoostListComponent implements OnChanges {
  @Input() oddsBoosts: IFreebetToken[];
  @Input() type: string;

  sortedBoosts: {[categoryId: string]: IFreebetToken[]} = {};

  constructor(
    private freeBetsService: FreeBetsService,
    private oddsBoostService: OddsBoostService,
    private userService: UserService,
    private currencyPipe: CurrencyPipe
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.oddsBoosts.currentValue) {
      this.freeBetsService
          .getOddsBoostsWithCategories(this.oddsBoosts)
          .pipe(map((data: IFreebetToken[] = []) => {
            const formattedTokens =  data.map((token: IFreebetToken) => {
              const displayArr: string[] = token.freebetTokenDisplayText.split('|');
              token.freebetOfferName = displayArr[0];
              token.freebetOfferDesc = displayArr[1];
              if ((typeof token.freebetMaxStake === 'string' && !token.freebetMaxStake.includes(this.userService.currencySymbol)) ||
                typeof token.freebetMaxStake === 'number') {
                token.freebetMaxStake = this.currencyPipe.transform(
                  Number(token.freebetMaxStake).toFixed(2),
                  this.userService.currencySymbol,
                  'code'
                );
              }
              return token;
            });
            const sortedBoosts = _.groupBy(formattedTokens, oddsBoost => oddsBoost.categoryId ? oddsBoost.categoryId : '0');
            _.each(sortedBoosts, (tokens: IFreebetToken[]) => {
              this.oddsBoostService.sortPageTokens(tokens);
            });
            return sortedBoosts;
          }))
          .subscribe((data: {[categoryId: string]: IFreebetToken[]}) => {
        this.sortedBoosts = data;
      });
    }
  }

  objectKeys(obj: Object): string[] {
    return _.keys(obj);
  }

  trackByOddsBoosts(index: number, oddsBoost: IFreebetToken): string {
    return `${index}${oddsBoost.freebetTokenId}`;
  }

  trackByCategory(index: number, category: string) {
    return `${index}_${category}`;
  }
}
