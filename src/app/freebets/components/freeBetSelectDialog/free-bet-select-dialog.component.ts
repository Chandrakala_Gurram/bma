import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

import { DeviceService } from '@core/services/device/device.service';
import { AbstractDialog } from '@shared/components/oxygenDialogs/abstract-dialog';
import { IFreeBet } from '@betslip/services/freeBet/free-bet.model';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'free-bet-select-dialog',
  templateUrl: 'free-bet-select-dialog.component.html',
  styleUrls: ['free-bet-select-dialog.component.less']
})
export class FreeBetSelectDialogComponent extends AbstractDialog {
  @ViewChild('dialog') dialog: any;

  freeBets: IFreeBet[];
  selected: IFreeBet;

  constructor(device: DeviceService, windowRef: WindowRefService, private changeDetectorRef: ChangeDetectorRef) {
    super(device, windowRef);
  }

  open(): void {
    super.open();
    this.selected = null;
    this.freeBets = this.params.freeBets.sort((a: IFreeBet, b: IFreeBet) => {
      const aFb = Date.parse(a.freebetTokenExpiryDate),
            bFb = Date.parse(b.freebetTokenExpiryDate);
      return aFb - bFb;
    });
    this.changeDetectorRef.markForCheck();
  }

  freeBetClick(freeBet: IFreeBet): void {
    this.selected = freeBet;
  }

  addFreeBet(): void {
    if (this.selected) {
      this.params.onSelect(this.selected);
    }
  }

  trackByIndex(index: number): number {
    return index;
  }
}
