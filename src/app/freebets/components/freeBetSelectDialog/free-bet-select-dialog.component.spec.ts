import { FreeBetSelectDialogComponent } from './free-bet-select-dialog.component';
import { IFreeBet } from '@betslip/services/freeBet/free-bet.model';

describe('FreeBetSelectDialogComponent', () => {
  let deviceService, windowRef, changeDetectorRef;
  let component: FreeBetSelectDialogComponent;

  beforeEach(() => {
    deviceService = {};
    windowRef = {
      document: {
        body: {
          classList: {
            add: jasmine.createSpy('add')
          }
        }
      }
    };
    changeDetectorRef = {
      markForCheck: jasmine.createSpy('markForCheck')
    };
    component = new FreeBetSelectDialogComponent(deviceService, windowRef, changeDetectorRef);
    component.dialog = {};
    component.params = {
      freeBets: [{
        id: 1,
        freebetTokenExpiryDate: 321
      }, {
        id: 2,
        freebetTokenExpiryDate: 123
      }, {
        id: 3,
        freebetTokenExpiryDate: 421
      }],
      onSelect: jasmine.createSpy('onSelect')
    };
  });

  it('open', () => {
    component.open();
    expect(component.selected).toBeNull();
    expect(component.freeBets).toBe(component.params.freeBets);
    // @ts-ignore
    expect(component.freeBets[0].id).toBe(2);
    // @ts-ignore
    expect(component.freeBets[1].id).toBe(1);
    expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
  });

  it('freeBetClick', () => {
    const freeBet: any = {};
    component.freeBetClick(freeBet);
    expect(component.selected).toBe(freeBet);
  });

  describe('addFreeBet', () => {
    it('should call onSelect callback', () => {
      component.selected = {} as IFreeBet;
      component.addFreeBet();

      expect(component.params.onSelect).toHaveBeenCalledWith(component.selected);
    });

    it('should not call onSelect callback', () => {
      component.addFreeBet();

      expect(component.params.onSelect).not.toHaveBeenCalled();
    });
  });

  it('trackByIndex', () => {
    const index = 5;
    const result = component.trackByIndex(index);
    expect(result).toBe(index);
  });
});
