import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Input } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';

import { StorageService } from '@core/services/storage/storage.service';
import { UserService } from '@core/services/user/user.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { DeviceService } from '@core/services/device/device.service';

@Component({
  selector: 'free-bet-tooltip',
  styleUrls: ['free-bet-tooltip.component.less'],
  templateUrl: 'free-bet-tooltip.component.html'
})
export class FreeBetTooltipComponent implements OnInit, OnDestroy {
  @ViewChild('tooltipBody') tooltipBody: ElementRef;

  @Input() set isQuickbet(val: boolean) {
    this.quickbet = val;
    this.setVisibility();
  }

  visible: boolean;
  private static cmpId: number = 1;
  private quickbet: boolean;
  private cmpName: string;
  private winClickSub: Subscription;

  constructor(
    private storageService: StorageService,
    private userService: UserService,
    private windowRefService: WindowRefService,
    private pubSubService: PubSubService,
    private deviceService: DeviceService
  ) {
    this.cmpName = `FreeBetTooltipComponent-${FreeBetTooltipComponent.cmpId++}`;
    this.windowClick = this.windowClick.bind(this);
  }

  ngOnInit(): void {
    this.setVisibility();

    this.pubSubService.subscribe(
      this.cmpName, this.pubSubService.API.BS_HIDE_FREEBET_TOOLTIP, () => this.hideTooltip()
    );

    this.pubSubService.subscribe(
      this.cmpName, this.pubSubService.API.DIGIT_KEYBOARD_SHOWN, () => this.scrollToTooltip()
    );

    this.pubSubService.subscribe(
      this.cmpName, this.pubSubService.API['show-slide-out-betslip-true'], () => this.scrollToTooltip()
    );

    this.scrollToTooltip();
  }

  ngOnDestroy(): void {
    this.winClickSub && !this.winClickSub.closed && this.winClickSub.unsubscribe();
    this.pubSubService.unsubscribe(this.cmpName);
  }

  private setVisibility(): void {
    this.winClickSub && !this.winClickSub.closed && this.winClickSub.unsubscribe();
    this.visible = !this.storageService.get(this.getStorageKey());
    if (this.visible) {
      this.winClickSub = fromEvent(
        this.windowRefService.nativeWindow as HTMLElement,
        this.deviceService.isIos ? 'touchstart' : 'click',
        { capture: true }
      ).subscribe(this.windowClick);
    }
  }

  private windowClick(event: MouseEvent): void {
    if (!this.visible) {
      return;
    }

    if (
      (event.target as HTMLElement).closest('betslip-container, .slide-out-betslip') ||
      this.quickbet
    ) {
      this.hideTooltip();
    }
  }

  private getStorageKey(): string {
    return `freeBetTooltipSeen${this.quickbet ? 'Quickbet' : 'Betslip'}-${this.userService.username}`;
  }

  private hideTooltip(): void {
    if (this.visible) {
      this.visible = false;
      this.storageService.set(this.getStorageKey(), true);
      this.winClickSub.unsubscribe();
    }
  }

  private scrollToTooltip(): void {
    if (!this.visible || this.quickbet) {
      return;
    }

    this.windowRefService.nativeWindow.setTimeout(() => {
      const wrap = this.windowRefService.document.querySelector('.bs-selections-wrapper');
      const body = this.tooltipBody && this.tooltipBody.nativeElement;

      if (!wrap || !body) {
        return;
      }

      const wrapBottom = wrap.getBoundingClientRect().bottom;
      const bodyBottom = body.getBoundingClientRect().bottom;

      if (bodyBottom > wrapBottom) {
        wrap.scrollTop += bodyBottom - wrapBottom + 5;
      }
    }, 100);
  }
}
