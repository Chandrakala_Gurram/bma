import { FreeBetTooltipComponent } from './free-bet-tooltip.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('FreeBetTooltipComponent', () => {
  let storageService;
  let userService;
  let windowRefService;
  let pubSubService;
  let deviceService;
  let component: FreeBetTooltipComponent;

  beforeEach(() => {
    storageService = {
      get: jasmine.createSpy('get').and.returnValue(false),
      set: jasmine.createSpy('set')
    };
    userService = {
      username: 'user1'
    };
    windowRefService = {
      nativeWindow: {
        addEventListener: jasmine.createSpy('addEventListener'),
        removeEventListener: jasmine.createSpy('removeEventListener'),
        setTimeout: jasmine.createSpy('setTimeout')
      },
      document: {
        querySelector: jasmine.createSpy('querySelector')
      }
    };
    pubSubService = {
      API: pubSubApi,
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy('unsubscribe')
    };
    deviceService = {};

    component = new FreeBetTooltipComponent(
      storageService,
      userService,
      windowRefService,
      pubSubService,
      deviceService
    );
    component.ngOnInit();
  });

  it('ngOnInit - tootlip visible', () => {
    pubSubService.subscribe.and.callFake((...args) => args[2]());
    component['hideTooltip'] = jasmine.createSpy('hideTooltip');
    component['scrollToTooltip'] = jasmine.createSpy('scrollToTooltip');

    component.ngOnInit();

    expect(storageService.get).toHaveBeenCalledWith(`freeBetTooltipSeenBetslip-${userService.username}`);
    expect(pubSubService.subscribe).toHaveBeenCalledWith(
      component['cmpName'], pubSubApi.BS_HIDE_FREEBET_TOOLTIP, jasmine.any(Function)
    );
    expect(pubSubService.subscribe).toHaveBeenCalledWith(
      component['cmpName'], pubSubApi.DIGIT_KEYBOARD_SHOWN, jasmine.any(Function)
    );
    expect(pubSubService.subscribe).toHaveBeenCalledWith(
      component['cmpName'], 'show-slide-out-betslip-true', jasmine.any(Function)
    );
    expect(component['hideTooltip']).toHaveBeenCalledTimes(1);
    expect(component['scrollToTooltip']).toHaveBeenCalledTimes(3);
  });

  it('ngOnInit - tootlip not visible', () => {
    pubSubService.subscribe.and.callFake((...args) => args[2]());
    component['hideTooltip'] = jasmine.createSpy('hideTooltip');
    component['scrollToTooltip'] = jasmine.createSpy('scrollToTooltip');

    storageService.get.and.returnValue(true);
    component.ngOnInit();

    expect(storageService.get).toHaveBeenCalledWith(`freeBetTooltipSeenBetslip-${userService.username}`);
    expect(pubSubService.subscribe).toHaveBeenCalledWith(
      component['cmpName'], pubSubApi.BS_HIDE_FREEBET_TOOLTIP, jasmine.any(Function)
    );
    expect(pubSubService.subscribe).toHaveBeenCalledWith(
      component['cmpName'], pubSubApi.DIGIT_KEYBOARD_SHOWN, jasmine.any(Function)
    );
    expect(pubSubService.subscribe).toHaveBeenCalledWith(
      component['cmpName'], 'show-slide-out-betslip-true', jasmine.any(Function)
    );
    expect(component['hideTooltip']).toHaveBeenCalledTimes(1);
    expect(component['scrollToTooltip']).toHaveBeenCalledTimes(3);
  });

  it('setVisibility called on init and on isQuicket setter', () => {
    component['setVisibility'] = jasmine.createSpy('setVisibility');

    component.ngOnInit();
    component.isQuickbet = true;
    expect(component['setVisibility']).toHaveBeenCalledTimes(2);
  });

  describe('windowClick', () => {
    it('tooltip not visible', () => {
      component.visible = false;
      component['windowClick']({} as any);
      expect(storageService.set).not.toHaveBeenCalled();
    });

    it('tooltip visible, click outside betslip', () => {
      component.visible = true;
      component['windowClick']({
        target: { closest: () => false }
      } as any);
      expect(storageService.set).not.toHaveBeenCalled();
    });

    it('tooltip visible, click inside betslip', () => {
      component.ngOnInit();
      component['windowClick']({
        target: { closest: () => true }
      } as any);
      expect(storageService.set).toHaveBeenCalledWith(`freeBetTooltipSeenBetslip-${userService.username}`, true);
    });

    it('tooltip visible, click inside quickbet', () => {
      component.ngOnInit();
      component['quickbet'] = true;
      component['windowClick']({
        target: { closest: () => false }
      } as any);
      expect(storageService.set).toHaveBeenCalledWith(`freeBetTooltipSeenQuickbet-${userService.username}`, true);
    });
  });

  describe('scrollToTooltip', () => {
    it('should not set timeout (quickbet)', () => {
      windowRefService.nativeWindow.setTimeout.calls.reset();
      component['quickbet'] = true;
      component['scrollToTooltip']();
      expect(windowRefService.nativeWindow.setTimeout).not.toHaveBeenCalled();
    });

    it('should not set timeout (not visible)', () => {
      windowRefService.nativeWindow.setTimeout.calls.reset();
      component['quickbet'] = false;
      component.visible = false;
      component['scrollToTooltip']();
      expect(windowRefService.nativeWindow.setTimeout).not.toHaveBeenCalled();
    });

    describe('timeout callback', () => {
      beforeEach(() => {
        component.visible = true;
        windowRefService.nativeWindow.setTimeout.and.callFake(cb => cb());
      });

      it('wrap and body are not defined', () => {
        windowRefService.document.querySelector.and.returnValue(null);
        component.tooltipBody = null;
        component['scrollToTooltip']();
        expect(windowRefService.document.querySelector).toHaveBeenCalledTimes(1);
      });

      it('body is not defined', () => {
        windowRefService.document.querySelector.and.returnValue({});
        component.tooltipBody = null;
        component['scrollToTooltip']();
        expect(windowRefService.document.querySelector).toHaveBeenCalledTimes(1);
      });

      it('update wrap scroll top', () => {
        let wrap;
        windowRefService.document.querySelector.and.callFake(() => wrap);

        wrap = {
          getBoundingClientRect: () => ({ bottom: 100 }), scrollTop: 0
        };
        component.tooltipBody = {
          nativeElement: {
            getBoundingClientRect: () => ({ bottom: 100 })
          }
        };
        component['scrollToTooltip']();
        expect(wrap.scrollTop).toBe(0);

        wrap = {
          getBoundingClientRect: () => ({ bottom: 100 }), scrollTop: 0
        };
        component.tooltipBody = {
          nativeElement: {
            getBoundingClientRect: () => ({ bottom: 200 })
          }
        };
        component['scrollToTooltip']();
        expect(wrap.scrollTop).toBe(105);
      });
    });
  });

  it('should subscribe for touch start event', () => {
    deviceService.isIos = true;
    component.ngOnInit();
    expect(windowRefService.nativeWindow.addEventListener).toHaveBeenCalledWith(
      'touchstart', jasmine.any(Function), { capture: true }
    );
  });

  it('should unsubscribe if visible true', () => {
    component['winClickSub'] = { unsubscribe: jasmine.createSpy('unsubscribe') } as any;
    component.visible = true;
    component['hideTooltip']();
    expect(component.visible).toBeFalsy();
    expect(storageService.set).toHaveBeenCalledWith(jasmine.any(String), true);
    expect(component['winClickSub'].unsubscribe).toHaveBeenCalledTimes(1);
  });

  it('should not unsubscribe if visible false', () => {
    component['winClickSub'] = { unsubscribe: jasmine.createSpy('unsubscribe') } as any;
    component.visible = false;
    component['hideTooltip']();
    expect(component.visible).toBeFalsy();
    expect(component['winClickSub'].unsubscribe).not.toHaveBeenCalled();
  });

  it('ngOnDestroy', () => {
    component['winClickSub'] = { unsubscribe: jasmine.createSpy('unsubscribe') } as any;
    component.ngOnDestroy();
    expect(component['winClickSub'].unsubscribe).toHaveBeenCalledTimes(1);
    expect(pubSubService.unsubscribe).toHaveBeenCalledWith(component['cmpName']);
  });
});
