import { throwError, of } from 'rxjs';
import { FreebetsComponent } from './freebets.component';
import { fakeAsync, tick } from '@angular/core/testing';

describe('FreebetsComponent', () => {
  let component: FreebetsComponent;
  let userService;
  let filtersService;
  let freeBetsService;
  let router;

  beforeEach(() => {
    userService = {
      status: false
    };
    filtersService = {
      setCurrency: jasmine.createSpy()
    };
    freeBetsService = {
      getFreeBets: jasmine.createSpy().and.returnValue(of(null))
    };
    router = {
      navigate: jasmine.createSpy()
    };

    component = new FreebetsComponent(userService, filtersService, freeBetsService, router);
  });

  it('constructor', () => {
    expect(component).toBeTruthy();
  });

  describe('#ngOnInit', () => {
    it('ngOnInit', () => {
      component.hideSpinner = jasmine.createSpy();
      component['addCurrencySymbol'] = jasmine.createSpy();
      component['getTotalFreeBetsBalance'] = jasmine.createSpy();
      component['getTotalBalance'] = jasmine.createSpy();
      component['extendFreebetsData'] = jasmine.createSpy();
      component.totalFreeBetsAmount = '10';

      component.ngOnInit();

      expect(component.hideSpinner).toHaveBeenCalled();
      expect(component['addCurrencySymbol']).toHaveBeenCalledWith(userService.sportBalance);
      expect(component['getTotalFreeBetsBalance']).toHaveBeenCalled();
      expect(component['addCurrencySymbol']).toHaveBeenCalledWith(component.totalFreeBetsAmount);
      expect(component['getTotalBalance']).toHaveBeenCalled();
      expect(component['extendFreebetsData']).toHaveBeenCalled();
    });

    it('ngOnInit (get freebets fail)', fakeAsync(() => {
      component.hideSpinner = jasmine.createSpy();
      freeBetsService.getFreeBets.and.returnValue(throwError(null));

      component.ngOnInit();
      tick();

      expect(component.hideSpinner).toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith(['/']);
    }));

    it('ngOnInit (get freebets fail) no user status', fakeAsync(() => {
      component.hideSpinner = jasmine.createSpy();
      userService.status = true;
      freeBetsService.getFreeBets.and.returnValue(throwError(null));

      component.ngOnInit();
      tick();

      expect(component.state.error).toBeTruthy();
      expect(component.hideSpinner).toHaveBeenCalled();
      expect(router.navigate).not.toHaveBeenCalled();
    }));
  });

  it('indexNumber', () => {
    expect(component.indexNumber(0)).toBe(0);
    expect(component.indexNumber(1)).toBe(1);
  });

  it('getTotalBalance', () => {
    component['userService'] = { sportBalance: '5' } as any;
    component.totalFreeBetsAmount = '5';
    component['addCurrencySymbol'] = jasmine.createSpy();

    component['getTotalBalance']();
    expect(component['addCurrencySymbol']).toHaveBeenCalledWith(10);
  });

  it('addCurrencySymbol', () => {
    component['userService'] = { currencySymbol: '$' } as any;
    component['addCurrencySymbol'](1);
    expect(filtersService.setCurrency).toHaveBeenCalledWith(1, component['userService'].currencySymbol);
  });

  it('extendFreebetsData', () => {
    component['addCurrencySymbol'] = jasmine.createSpy();
    component.freebets = [{
      freebetTokenId: 'id',
      freebetTokenValue: 'value'
    }] as any[];

    component['extendFreebetsData']();

    expect(component['addCurrencySymbol']).toHaveBeenCalledWith(
      component.freebets[0].freebetTokenValue
    );
    expect(component.freebets[0].redirectUrl).toBe('/freebets/id');
  });

  describe('getTotalFreeBetsBalance', () => {
    it('should handle freebets stored in component', () => {
      component.freebets = [
        { freebetTokenValue: '4' },
        { freebetTokenValue: '3.2567' }
      ] as any[];
      expect(component['getTotalFreeBetsBalance']()).toBe('7.26');
    });

    it('should handle case when no freebets stored in component', () => {
      component.freebets = undefined;
      expect(component['getTotalFreeBetsBalance']()).toBe('0.00');
    });
  });
});
