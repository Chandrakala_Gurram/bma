import { finalize } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import * as _ from 'underscore';
import { Router } from '@angular/router';

import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { UserService } from '@core/services/user/user.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { IFreebetToken } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { FreeBetsService } from '@coreModule/services/freeBets/free-bets.service';

@Component({
  selector: 'freebets',
  templateUrl: './freebets.component.html'
})
export class FreebetsComponent extends AbstractOutletComponent implements OnInit {
  freebets: IFreebetToken[];
  totalFreeBetsAmount: string;
  totalFreeBets: string;
  sportBalance: string;
  totalBalance: string;

  constructor(
    protected userService: UserService,
    protected filtersService: FiltersService,
    protected freeBetsService: FreeBetsService,
    protected router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.freeBetsService.getFreeBets().pipe(
      finalize(() => {
        this.hideSpinner();
      }))
      .subscribe((data: IFreebetToken[]) => {
        this.freebets = data || [];
        this.sportBalance = this.addCurrencySymbol(this.userService.sportBalance);
        this.totalFreeBetsAmount = this.getTotalFreeBetsBalance();
        this.totalFreeBets = this.addCurrencySymbol(this.totalFreeBetsAmount);
        this.totalBalance = this.getTotalBalance();

        this.extendFreebetsData();
      }, () => {
        if (!this.userService.status) {
          this.router.navigate(['/']);
        } else {
          this.showError();
        }
      });
  }

  /**
   * Track Events by index
   * @param {number} index
   * @returns {number}
   */
  indexNumber(index: number): number {
    return index;
  }

  /**
   * Returns total user's total balance combined with sport and free bets balances.
   * @return {string}
   */
  private getTotalBalance(): string {
    return this.addCurrencySymbol(Number(this.userService.sportBalance) + Number(this.totalFreeBetsAmount));
  }

  /**
   * Formats given value with currency filter.
   * @param {string|number} value
   * @return {string}
   * @private
   */
  private addCurrencySymbol(value: string | number): string {
    return this.filtersService.setCurrency(value, this.userService.currencySymbol);
  }

  /**
   * Extends free bets list with redirection url and formatted freebet value.
   * @param {Array} freebets The list of freebet objects.
   * @return {Array}
   * @private
   */
  private extendFreebetsData() {
    _.each(this.freebets, (freebet: IFreebetToken) => {
      freebet.redirectUrl = `/freebets/${freebet.freebetTokenId}`;
      freebet.amount = this.addCurrencySymbol(freebet.freebetTokenValue);
    });
  }

  /**
   * Calculates total free bets balance.
   * @param {Array} freeBets
   * @returns {string}
   * @private
   */
  private getTotalFreeBetsBalance(): string {
    const totalPrize = _.reduce(this.freebets || [], (sum, bet) => sum + parseFloat(bet.freebetTokenValue), 0);
    return totalPrize.toFixed(2);
  }
}
