import { Component, Input, Output, EventEmitter, ComponentFactoryResolver } from '@angular/core';

import { DialogService } from '@core/services/dialogService/dialog.service';
import { IFreeBet } from '@betslip/services/freeBet/free-bet.model';
import { FreeBetSelectDialogComponent } from '../freeBetSelectDialog/free-bet-select-dialog.component';

@Component({
  selector: 'free-bet-toggle',
  templateUrl: './free-bet-toggle.component.html',
  styleUrls: ['./free-bet-toggle.component.less']
})
export class FreeBetToggleComponent {
  @Input() freeBets: IFreeBet[];
  @Input() selected: IFreeBet;
  @Output() readonly selectedChange = new EventEmitter();

  constructor(
    private dialogService: DialogService,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
  }

  get dialogComponent() {
    return FreeBetSelectDialogComponent;
  }

  useFreeBet(): void {
    if (!this.freeBets || !this.freeBets.length) {
      return;
    }

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.dialogComponent);
    this.dialogService.openDialog(DialogService.API.selectFreeBetDialog, componentFactory, true, {
      dialogClass: 'free-bet-select-dialog',
      freeBets: this.freeBets,
      onSelect: (freeBet: IFreeBet) => {
        this.selectedChange.emit(freeBet);
        this.dialogService.closeDialog(DialogService.API.selectFreeBetDialog);
      }
    });
  }

  removeFreeBet(): void {
    this.selectedChange.emit(null);
  }
}
