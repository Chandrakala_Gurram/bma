import { fakeAsync, tick } from '@angular/core/testing';

import { FreeBetToggleComponent } from './free-bet-toggle.component';
import { dialogIdentifierDictionary } from '@core/constants/dialog-identifier-dictionary.constant';

describe('FreeBetToggleComponent', () => {
  let dialogService;
  let componentFactoryResolver;
  let component: FreeBetToggleComponent;
  let resolvedDialogComponent;

  beforeEach(() => {
    dialogService = {
      API: dialogIdentifierDictionary,
      openDialog: jasmine.createSpy('openDialog').and.callFake((p1, p2, p3, opt) => {
        opt.onSelect();
      }),
      closeDialog: jasmine.createSpy('closeDialog')
    };
    resolvedDialogComponent = {
      name: dialogService.API.selectFreeBetDialog
    };
    componentFactoryResolver = {
      resolveComponentFactory: jasmine.createSpy('resolveComponentFactory').and.returnValue(resolvedDialogComponent)
    };

    component = new FreeBetToggleComponent(
      dialogService,
      componentFactoryResolver
    );
  });

  describe('useFreebet', () => {
    it('no available free bets', () => {
      component.freeBets = null;
      component.useFreeBet();
      expect(componentFactoryResolver.resolveComponentFactory).not.toHaveBeenCalled();
    });

    it('open dialog', fakeAsync(() => {
      component.freeBets = [{}] as any[];
      component.useFreeBet();
      tick();
      expect(componentFactoryResolver.resolveComponentFactory).toHaveBeenCalledWith(jasmine.any(Function));
      expect(dialogService.openDialog).toHaveBeenCalledWith(
        'selectFreeBetDialog', resolvedDialogComponent, true, jasmine.any(Object)
      );
      expect(dialogService.closeDialog).toHaveBeenCalledWith('selectFreeBetDialog');
    }));
  });

  it('removeFreeBet', () => {
    component.selectedChange.emit = jasmine.createSpy('emit');
    component.removeFreeBet();
    expect(component.selectedChange.emit).toHaveBeenCalledWith(null);
  });
});
