import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ModalModule } from 'angular-custom-modal';

import { SharedModule } from '@sharedModule/shared.module';
import { FreebetsRoutingModule } from '@freebetsModule/freebets-routing.module';
import { FreebetsComponent } from '@freebetsModule/components/freebets/freebets.component';
import { FreebetDetailsComponent } from '@freebetsModule/components/freebetDetails/freebet-details.component';
import { FreeBetToggleComponent } from '@freebetsModule/components/freeBetToggle/free-bet-toggle.component';
import { FreeBetSelectDialogComponent } from '@freebetsModule/components/freeBetSelectDialog/free-bet-select-dialog.component';
import { FreeBetTooltipComponent } from '@freebetsModule/components/freeBetTooltip/free-bet-tooltip.component';
import { LpSpDropdownComponent } from '@freebetsModule/components/lpSpDropdown/lp-sp-dropdown.component';

@NgModule({
  imports: [
    SharedModule,
    ModalModule,
    FreebetsRoutingModule
  ],
  declarations: [
    FreebetsComponent,
    FreebetDetailsComponent,
    FreeBetToggleComponent,
    FreeBetSelectDialogComponent,
    FreeBetTooltipComponent,
    LpSpDropdownComponent
  ],
  exports: [],
  entryComponents: [
    FreebetsComponent,
    FreebetDetailsComponent,
    FreeBetToggleComponent,
    FreeBetSelectDialogComponent,
    FreeBetTooltipComponent,
    LpSpDropdownComponent
  ],
  providers: [],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class FreebetsModule {
  static entry = { FreeBetToggleComponent, FreeBetTooltipComponent, LpSpDropdownComponent };
}
