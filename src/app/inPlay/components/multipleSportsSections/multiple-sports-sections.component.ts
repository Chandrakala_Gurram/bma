import { map } from 'rxjs/operators';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { forkJoin as observableForkJoin, Observable } from 'rxjs';
import * as _ from 'underscore';
import { InplayMainService } from '@inplayModule/services/inplayMain/inplay-main.service';
import { ISwitcherConfig } from '@core/models/switcher-config.model';
import { ISportSegment } from '@app/inPlay/models/sport-segment.model';
import { ISportTracking } from '@app/inPlay/models/sport-tracking.model';
import { IInplayConnectionState } from '@app/inPlay/models/connection.model';
import { InplayConnectionService } from '@app/inPlay/services/inplayConnection/inplay-connection.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { StickyVirtualScrollerService } from '@root/app/shared/components/stickyVirtualScroller/sticky-virtual-scroller.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { GamingService } from '@core/services/sport/gaming.service';
import { inplayConfig } from '@app/inPlay/constants/config';
import { EVENTS } from '@core/constants/websocket-events.constant';
import { NewRelicService } from '@core/services/newRelic/new-relic.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'multiple-sports-sections',
  templateUrl: 'multiple-sports-sections.component.html',
  styleUrls: ['./multiple-sports-sections.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [StickyVirtualScrollerService]
})
export class MultipleSportsSectionsComponent implements OnInit, OnDestroy, OnChanges {
  @Input() eventsByGroups: any;
  @Input() viewByFilters: string[];
  @Input() showExpanded: boolean;
  @Input() expandedSportsCount: number;
  @Input() expandedLeaguesCount: number;
  @Input() ssError: boolean;
  @Input() isWatchLive: boolean = false;

  moduleName: string = 'sports-tab';
  isExpanded: boolean = false;
  switchers: ISwitcherConfig[];
  isDataReady: boolean = false;
  isVirtualScrollEnabled: boolean;
  contentReady: boolean = false;

  /**
   * Identifier of expand/collapse state.
   * @type {string}
   */
  expandedKey: string = 'expanded-sports-tab';

  constructor(
    private stickyVirtualScrollerService: StickyVirtualScrollerService,
    private inPlayMainService: InplayMainService,
    private inPlayConnectionService: InplayConnectionService,
    private routingHelperService: RoutingHelperService,
    private cmsService: CmsService,
    private newRelicService: NewRelicService,
    private pubsubService: PubSubService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
  }

  ngOnInit(): void {
    this.cmsService.getSystemConfig().subscribe(systemConfig => {
      this.isVirtualScrollEnabled = systemConfig && systemConfig.VirtualScrollConfig && systemConfig.VirtualScrollConfig.enabled;
      this.initAccordions();
    });

    this.pubsubService.subscribe(this.moduleName, `${inplayConfig.moduleName}.${EVENTS.SOCKET_RECONNECT_ERROR}`, () => {
      this.newRelicService.addPageAction('inplay=>UI_Message=>Unavailable=>wsError');
    });

    if (this.ssError) {
      this.newRelicService.addPageAction('inplay=>UI_Message=>Unavailable=>ssError');
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.eventsByGroups && !changes.eventsByGroups.isFirstChange() && !this.isDataReady) {
      this.initAccordions();
    }

    // Track UI Message Service is unavailable
    if (changes.ssError && changes.ssError.currentValue) {
      this.newRelicService.addPageAction('inplay=>UI_Message=>Unavailable=>ssError');
    }
  }

  ngOnDestroy(): void {
    this.pubsubService.unsubscribe(this.moduleName);
    this.stickyVirtualScrollerService.destroyEvents();
  }

  /**
   * Error on Web Sockets connection
   * @type {object}
   */
  get wsError(): IInplayConnectionState {
    return this.inPlayConnectionService.status;
  }

  get isErrorState(): boolean {
    return this.ssError || this.wsError.reconnectFailed;
  }

  goToSportCompetitionsPage(eventsBySports: ISportSegment): string {
    return this.routingHelperService.formSportCompetitionsUrl(eventsBySports.sportUri);
  }

  /**
   * Check if sport events is present and show no events section
   * @param {String} filter
   */
  showNoEventsSection(filter: string): boolean {
    if (!this.ssError && this.eventsByGroups[filter]) {
      return !this.eventsByGroups[filter].eventsIds.length;
    }
    return false;
  }

  /**
   * Toggle expand/collapse state of sport section.
   * @param {Object} sportSection
   */
  toggleSport(isExpanded: boolean, sportSection: ISportSegment, filter: string): void {
    const categoryId = parseInt(sportSection.categoryId, 10);
    sportSection[this.expandedKey] = isExpanded;
    this.changeDetectorRef.detectChanges();

    if (sportSection[this.expandedKey]) {
      this.getSportData(categoryId, filter, this.inPlayMainService.getSportName(sportSection)).subscribe(() => {
        // if use expand and collapse section, after getting data we will check section again
        // unsubscribe if section was collapsed
        if (sportSection[this.expandedKey] === false) {
          this.inPlayMainService.unsubscribeForSportCompetitionUpdates(sportSection);
          this.inPlayMainService.unsubscribeForEventsUpdates(sportSection);
        }

        this.pubsubService.publish(this.pubsubService.API.INPLAY_DATA_RELOADED);
        this.stickyVirtualScrollerService.updateScrollVisibility();
        this.changeDetectorRef.detectChanges();
      });
    } else {
      this.inPlayMainService.unsubscribeForSportCompetitionUpdates(sportSection);
      this.inPlayMainService.unsubscribeForEventsUpdates(sportSection);
      this.stickyVirtualScrollerService.updateScrollVisibility();
      this.changeDetectorRef.detectChanges();
    }
  }

  /**
   * Returns unique key for sport category in current filter view.
   * @param {number} options.categoryId
   * @param {string} options.topLevelType
   * @return {string}
   */
  getSportTrackingId(index: number, eventsBySports: ISportTracking): string {
    const categoryId = eventsBySports.categoryId;
    const topLevelType = eventsBySports.topLevelType;

    return `${categoryId}-${topLevelType}`;
  }

  isLiveNowFilter(filter: string): boolean {
    return filter === 'livenow';
  }

  isUpcomingFilter(filter: string): boolean {
    return filter === 'upcomingLiveStream' || filter === 'upcoming';
  }

  trackById(index: number, filter: string) {
    return `${index}_${filter}`;
  }

  reloadComponent(): void {
    this.ssError = false;
    this.ngOnDestroy();
    this.ngOnInit();
  }

  getNoEventsMessage(filter: string): string {
    return ['livenow', 'liveStream'].includes(filter) ? 'inplay.noLiveEventsFound' : 'inplay.noUpcomingEventsFound';
  }

  /**
   * is sport section expanded
   * @param {number} index
   * @private
   */
  private shouldSportBeExpanded(index): boolean {
    return index < this.expandedSportsCount;
  }

  /**
   * Sets view filter.
   * @param {string} newFilter
   * @private
   */
  private initAccordions(): void {
    if (this.eventsByGroups && this.eventsByGroups[this.viewByFilters[0]]) {
      _.each(this.eventsByGroups[this.viewByFilters[0]].eventsBySports, (sportSection: ISportSegment, index) => {
        const sportId = parseInt(sportSection.categoryId, 10);

        sportSection[this.expandedKey] = this.shouldSportBeExpanded(index);
        sportSection.initiallyExpanded = sportSection[this.expandedKey];

        if (sportSection[this.expandedKey]) {
          this.getSportData(sportId, this.viewByFilters[0], this.inPlayMainService.getSportName(sportSection)).subscribe(() => {},
            (error) => {
              console.warn(error);
            }, () => {
              this.handleEventsLoaded();
            });
        }
      });

      this.isDataReady = true;
    }
    this.handleEventsLoaded();
  }

  /**
   * Updates content ready state when child component data loaded
   */
  private handleEventsLoaded(): void {
    if (this.eventsByGroups && this.eventsByGroups[this.viewByFilters[0]]) {
      this.contentReady = (this.eventsByGroups[this.viewByFilters[0]].eventsBySports || [])
        .every(sportSection => sportSection.initiallyExpanded ? sportSection.eventsLoaded : true);
    } else {
      this.contentReady =  false;
    }
    this.changeDetectorRef.markForCheck();
  }

  /**
   * Apply single sport data
   * @param {ISportSegment} sportSection
   * @param {number} id
   * @private
   */
  private applySingleSportData(sportSection: ISportSegment, id: number, filter: string,
    sportInstance: GamingService | {}): void {
    const sportDataIsPresent = _.has(sportSection, 'eventsByTypeName') && sportSection.eventsByTypeName.length;
    const eventsByGroup = this.eventsByGroups[filter];
    const sportIndex = this.inPlayMainService.getLevelIndex(eventsByGroup.eventsBySports, 'categoryId', id);

    if (sportDataIsPresent) {
      const sportSectionToUpdate = eventsByGroup.eventsBySports[sportIndex];
      // data is present
      if (sportSectionToUpdate) {
        // Update ids list for in-play group: "livenow" or "upcoming"
        eventsByGroup.eventsIds = _.union(eventsByGroup.eventsIds, sportSection.eventsIds);
        this.inPlayMainService.extendSectionWithSportInstance(sportSectionToUpdate, sportInstance);
        sportSectionToUpdate.eventsIds = sportSection.eventsIds;
        sportSectionToUpdate.eventsByTypeName = sportSection.eventsByTypeName;
        sportSectionToUpdate.eventsLoaded = true;
      }
    } else {
      // no data were received
      eventsByGroup.eventsBySports.splice(sportIndex, 1);
    }
    this.changeDetectorRef.markForCheck();
  }

  /**
   * Get data by sport
   * @param {number} categoryId
   * @return {Observable<ISportSegment>}
   * @private
   */
  private getSportData(categoryId: number, filter: string, sportName: string): Observable<ISportSegment> {
    return observableForkJoin(
      this.inPlayMainService.getSportData({
        categoryId,
        isLiveNowType: this.isLiveNowFilter(filter),
        topLevelType: this.inPlayMainService.getTopLevelTypeParameter(filter)
      }, false, !this.isVirtualScrollEnabled),
      this.inPlayMainService.getSportConfigSafe(sportName)
    ).pipe(
      map((data: [ISportSegment, GamingService | {}]) => {
        const sportSegment = data[0],
          sportInstance = data[1];
        // error handling
        if (!sportSegment || _.isEmpty(sportSegment) || _.has(sportSegment, 'error')) {
          this.inPlayConnectionService.setConnectionErrorState(true);
          return;
        }
        this.applySingleSportData(sportSegment, categoryId, filter, sportInstance);

        return sportSegment;
      }));
  }
}
