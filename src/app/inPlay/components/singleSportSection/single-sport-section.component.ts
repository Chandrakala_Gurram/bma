import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as _ from 'underscore';

import { IReloadDataParams } from '@app/inPlay/models/reload-data-params.model';
import { IRequestParams } from '@app/inPlay/models/request.model';
import { ISportSegment } from '@app/inPlay/models/sport-segment.model';
import { ITypeSegment } from '@app/inPlay/models/type-segment.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { IScrollVisibility } from '@root/app/shared/components/stickyVirtualScroller/sticky-virtual-scroller.model';

import { InplayMainService } from '@inplayModule/services/inplayMain/inplay-main.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { CoreToolsService } from '@root/app/core/services/coreTools/core-tools.service';
import { StickyVirtualScrollerService } from '@root/app/shared/components/stickyVirtualScroller/sticky-virtual-scroller.service';
import { GamingService } from '@core/services/sport/gaming.service';

import { ICompetitionsConfig } from '@core/services/cms/models/system-config';
import environment from '@environment/oxygenEnvConfig';
import { ILazyComponentOutput } from '@app/shared/components/lazy-component/lazy-component.model';

@Component({
  selector: 'single-sport-section',
  templateUrl: 'single-sport-section.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./single-sport-section.component.less']
})
export class SingleSportSectionComponent implements OnInit, OnDestroy, OnChanges {
  @Input() filter: string;
  @Input() eventsBySports: ISportSegment;
  @Input() showExpanded: boolean;
  @Input() inner: any;
  @Input() expandedLeaguesCount: number;
  @Input() gtmModuleTitle?: string;
  @Input() virtualScroll: boolean;

  @Output() readonly reloadData: EventEmitter<IReloadDataParams> = new EventEmitter();
  @Output() readonly collapseSportEmitter: EventEmitter<boolean> = new EventEmitter();

  /**
   * Flags for control events during Competition data request process.
   * example "typeId":boolean
   * {
   *    '141':true
   * }
   * @type {object}
   */
  competitionRequestInProcessFlags = {};
  competitionsWithPages: ICompetitionsConfig;

  /**
   * IsExpanded Flags for competitions sections.
   * @type {object}
   * {
   *   "competitionId": boolean
   * }
   */
  expandedFlags = {};
  competitionsAvailability: { [key: string]: string } = {};
  isAllExpanded = false;
  isLoading: boolean = false;
  scrollSportUId: string;
  subscriptionFlags = {};
  sportName: string;
  selectedMarketName: string;
  categoryId: string;
  topLevelType: string;
  isMarketSelectorAvailable: boolean;
  sportInstance: GamingService;
  isMarketSwitcherConfigured: boolean = false;

  /**
   * Constant with sports data
   */
  private CATEGORIES_DATA: any = environment.CATEGORIES_DATA;
  private detectListener: number;
  private syncName: string;
  private marketSwitcherConfigSubscription: Subscription;

  constructor(
    private inPlayMainService: InplayMainService,
    private pubsubService: PubSubService,
    private windowRef: WindowRefService,
    private changeDetectorRef: ChangeDetectorRef,
    private routingHelperService: RoutingHelperService,
    private activatedRoute: ActivatedRoute,
    private cmsService: CmsService,
    private coreToolService: CoreToolsService,
    private stickyVirtualScrollerService: StickyVirtualScrollerService,
  ) {
    this.syncName = `inplay-single-sport_${this.coreToolService.uuid()}`;
    this.reloadSportData = this.reloadSportData.bind(this);
    this.changeDetectorRef.detach();
  }

  ngOnInit(): void {
    this.detectListener = this.windowRef.nativeWindow.setInterval(() => {
      this.changeDetectorRef.detectChanges();
    }, 100);
    this.categoryId = this.eventsBySports.categoryId;
    this.topLevelType = this.inPlayMainService.getTopLevelTypeParameter(this.filter);


    // Filter Enhanced Multiples
    if (this.eventsBySports.eventsByTypeName && this.eventsBySports.eventsByTypeName.length) {
      this.eventsBySports.eventsByTypeName = this.eventsBySports.eventsByTypeName.filter(competitionSection => {
        return competitionSection.typeName !== 'Enhanced Multiples';
      });
    }

    this.cmsService.getCompetitions(this.activatedRoute.snapshot.paramMap.get('sport') || '')
      .subscribe((cmsData: ICompetitionsConfig) => {
        this.competitionsWithPages = cmsData;
        this.setCompetitionPagesAvailability();
        this.changeDetectorRef.markForCheck();
      });

    this.sportName = this.inPlayMainService.getSportName(this.eventsBySports);

    this.marketSwitcherConfigSubscription = this.cmsService.getMarketSwitcherFlagValue(this.sportName)
    .subscribe((flag: boolean) => { this.isMarketSwitcherConfigured = flag; });

    this.inPlayMainService.getSportConfigSafe(this.sportName).subscribe((sportInstance: GamingService) => {
      this.sportInstance = sportInstance;
      this.extendSectionData();
      this.changeDetectorRef.markForCheck();
    });

    this.pubsubService.subscribe(this.syncName, `INPLAY_COMPETITION_REMOVED:${this.categoryId}:${this.topLevelType}`,
      (removedCompetition: ITypeSegment) => {
        this.subscriptionFlags[removedCompetition.typeId] = false;
        if (this.isMarketSelectorAvailable && !this.eventsBySports.eventsByTypeName.length && !this.isFirstMarketSelected()) {
          this.reloadSportData({
            useCache: false,
            additionalParams: {
              marketSelector: this.eventsBySports.marketSelectorOptions[0]
            }
          });
          this.changeDetectorRef.detectChanges();
        }

        if (this.inner) {
          // get first type section (after removed) to check if it is subscribed on updates
          const nextCompetitionSection = this.eventsBySports.eventsByTypeName && this.eventsBySports.eventsByTypeName[0];
          // subscribe if not subscribed
          if (nextCompetitionSection && !this.subscriptionFlags[nextCompetitionSection.typeId]) {
            this.loadCompetionSection(nextCompetitionSection, this.eventsBySports.eventsByTypeName)
              .subscribe(() => {
                this.expandedFlags[nextCompetitionSection.typeId] = true;
                nextCompetitionSection.isExpanded = true;
                this.changeDetectorRef.markForCheck();
              });
          }
        }

        this.calculateIsAllExpanded();
        this.changeDetectorRef.markForCheck();
      });

    this.pubsubService.subscribe(this.syncName, `INPLAY_COMPETITION_ADDED:${this.categoryId}:${this.topLevelType}`,
      (addedCompetition: ITypeSegment) => {
        if (this.inner) {
          if (addedCompetition.events.length === 0) {
            const sectionsArray = this.eventsBySports.eventsByTypeName;
            sectionsArray.splice(sectionsArray.indexOf(addedCompetition), 1);
          }

          this.setCompetitionPagesAvailability();

          if (!this.virtualScroll) {
            this.inPlayMainService.subscribeForUpdates(addedCompetition.events);
          }

          // Reset state of added accordion on SLP
          this.expandedFlags[addedCompetition.typeId] = this.inner;
        } else if (!this.inner && !this.virtualScroll) {
          if (this.expandedFlags[addedCompetition.typeId]) {
            this.inPlayMainService.subscribeForUpdates(addedCompetition.events);
          }
        }
        this.calculateIsAllExpanded();
        addedCompetition.isExpanded = this.expandedFlags[addedCompetition.typeId];
        this.changeDetectorRef.markForCheck();
      });

    this.scrollSportUId = this.coreToolService.uuid();
    this.processInitialData();
    this.checkIfMarketSelectorAvailable();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const eventsBySports = changes && changes.eventsBySports && changes.eventsBySports.currentValue;
    if (eventsBySports && Object.keys(eventsBySports).length) {
      this.processInitialData(eventsBySports);
      this.extendSectionData(true);
    }
    this.changeDetectorRef.markForCheck();
  }

  ngOnDestroy(): void {
    this.inPlayMainService.unsubscribeForSportCompetitionUpdates(this.eventsBySports);
    this.inPlayMainService.unsubscribeForEventsUpdates(this.eventsBySports);
    this.pubsubService.unsubscribe(this.syncName);
    this.eventsBySports.eventsByTypeName = [];
    this.windowRef.nativeWindow.clearInterval(this.detectListener);
    this.stickyVirtualScrollerService.stick(false, true);
    this.marketSwitcherConfigSubscription && this.marketSwitcherConfigSubscription.unsubscribe();
  }

  goToCompetition(competitionSection: ITypeSegment): string {
    const competitionPageUrl = this.routingHelperService.formCompetitionUrl({
      sport: this.activatedRoute.snapshot.paramMap.get('sport'),
      typeName: competitionSection.typeName,
      className: competitionSection.className
    });
    return competitionPageUrl;
  }

  trackByTypeId(index: number, item: any): string {
    return item.typeId;
  }

  trackByEventId(index: number, item: any): string {
    return item.id;
  }

  /**
   * Set Expanded flags after reloading data.
   * @param {{}} options - { useCache, additionalParams }
   */
  reloadSportData(options: IReloadDataParams): void {
    this.reloadData.emit(options);
  }

  handleOutput(event: ILazyComponentOutput) {
    if (event.output === 'reloadData') {
      this.reloadSportData(event.value);
    }
    if (event.output === 'selectedMarketName') {
      this.selectedMarketName = event.value;
    }
  }

  setCompetitionPagesAvailability(): void {
    if (!this.eventsBySports || !this.eventsBySports.eventsByTypeName || !this.competitionsWithPages) {
      return;
    }

    const popularCompetitions = ((this.competitionsWithPages.InitialClassIDs || '').split(',') || []),
      aZCompetitions = ((this.competitionsWithPages['A-ZClassIDs'] || '').split(',') || []),
      allAvailableCompetitions = popularCompetitions.concat(aZCompetitions),
      competitionsAvailability = {};

    this.eventsBySports.eventsByTypeName.forEach((competitionSection) => {
      competitionSection.classId = competitionSection.events && competitionSection.events[0].classId;
      if (!competitionSection.classId) {
        return;
      }
      competitionsAvailability[competitionSection.classId] = _.contains(allAvailableCompetitions,
        competitionSection.classId);
    });
    this.competitionsAvailability = competitionsAvailability;
  }

  /**
   * Toggle expand/collapse state of league section.
   * @param {object} competitionSection
   * @param {array} sectionsArray
   */
  toggleCompetitionSection(competitionSection: ITypeSegment, sectionsArray: ITypeSegment[]): void {
    const isExpanded = this.expandedFlags[competitionSection.typeId] = !this.expandedFlags[competitionSection.typeId];
    const isRequestInProcess = this.competitionRequestInProcessFlags[competitionSection.typeId];
    // if we are requesting data for specific competition, we will not subscribe/unsubscribe until request is done.
    if (isRequestInProcess) {
      return;
    }
    competitionSection.isExpanded = isExpanded;

    if (isExpanded) {
      this.loadCompetionSection(competitionSection, sectionsArray).subscribe(() => {
        // subscribe only if user not collapsed section during request
        if (this.expandedFlags[competitionSection.typeId]) {
          this.inPlayMainService.subscribeForUpdates(competitionSection.events);
          this.subscriptionFlags[competitionSection.typeId] = true;
        }
        this.changeDetectorRef.markForCheck();
      });
    } else {
      this.inPlayMainService.unsubscribeForEventsUpdates(competitionSection);
      this.subscriptionFlags[competitionSection.typeId] = false;
    }
  }

  loadCompetionSection(competitionSection: ITypeSegment, sectionsArray: ITypeSegment[], silent: boolean = true): Observable<ISportEvent[]> {
    this.isLoading = !silent;
    this.competitionRequestInProcessFlags[competitionSection.typeId] = true;
    this.changeDetectorRef.markForCheck();
    const requestParams: IRequestParams = {
      categoryId: this.categoryId,
      isLiveNowType: this.filter === 'livenow' || this.filter === 'liveStream',
      topLevelType: this.inPlayMainService.getTopLevelTypeParameter(this.filter),
      typeId: competitionSection.typeId,
      // flag to detect should we modify markets marketMeaningMinorCode
      // and dispSortName in inPlayDataService(see modifyMainMarkets methods comment)
      modifyMainMarkets: true
    };

    if (competitionSection.marketSelector) {
      requestParams.marketSelector = competitionSection.marketSelector;
    }

    return this.inPlayMainService._getCompetitionData(requestParams, this.eventsBySports.categoryCode)
      .pipe(map((competitionEvents: ISportEvent[]) => {
        this.competitionRequestInProcessFlags[competitionSection.typeId] = false;

        if (competitionEvents.length === 0) {
          sectionsArray.splice(sectionsArray.indexOf(competitionSection), 1);
        } else {
          competitionSection.events = competitionEvents;
          competitionSection.eventsIds = competitionEvents.map((event: ISportEvent) => event.id);
        }

        this.setCompetitionPagesAvailability();

        this.isLoading = false;
        this.changeDetectorRef.markForCheck();
        return competitionEvents;
      }));

  }

  prefetchNext(competitionSection: ITypeSegment) {
    if (this.isAllExpanded && competitionSection) {
      const isExpanded = this.expandedFlags[competitionSection.typeId];

      if (!isExpanded) {
        this.loadCompetionSection(competitionSection, this.eventsBySports.eventsByTypeName).subscribe(() => {
          this.expandedFlags[competitionSection.typeId] = true;
          competitionSection.isExpanded = true;
          this.changeDetectorRef.markForCheck();
        });
      }
      this.changeDetectorRef.markForCheck();
    }
  }

  /**
   * Check rules to show marketSelector
   * @returns {boolean}
   */
  isMarketSelectorVisible(): boolean {
    const isEventsPresent: boolean = this.eventsBySports.eventsByTypeName && !!this.eventsBySports.eventsByTypeName.length;
    return this.isMarketSelectorAvailable && isEventsPresent;
  }

  /**
   * Check if marketSelector is available
   * @returns {boolean}
   */
  checkIfMarketSelectorAvailable(): void {
    const sportCategoryId = parseInt(this.categoryId, 10);
    const isMarketSelectorAvailable: boolean = this.filter === 'livenow' &&
      this.eventsBySports &&
      this.eventsBySports.marketSelectorOptions &&
      this.CATEGORIES_DATA.tierOne.includes(sportCategoryId.toString()) &&
      !this.inner;
    this.isMarketSelectorAvailable = !!isMarketSelectorAvailable;
  }

  /**
   * Get competition title text
   * @param {object} competitionSectionData
   * @returns {string}
   */
  getSectionTitle(competitionSectionData: ITypeSegment): string {
    return competitionSectionData.typeName || '';
  }

  showMoreSport(competitionSections: ITypeSegment[]): void {
    if (!this.inner) {
      return;
    }
    if (this.virtualScroll) {
      const nextCompetitionSection = competitionSections[this.expandedLeaguesCount];
      this.loadCompetionSection(nextCompetitionSection, competitionSections)
        .subscribe(() => {
          this.expandedFlags[nextCompetitionSection.typeId] = true;
          nextCompetitionSection.isExpanded = true;
          this.changeDetectorRef.markForCheck();
        });
    } else {
      competitionSections.forEach((competitionSection, i) => {
        if (i > this.expandedLeaguesCount - 1) {
          this.loadCompetionSection(competitionSection, competitionSections)
            .subscribe(() => {
              this.inPlayMainService.subscribeForUpdates(competitionSection.events);
              this.expandedFlags[competitionSection.typeId] = this.subscriptionFlags[competitionSection.typeId] = true;
              competitionSection.isExpanded = true;
              this.changeDetectorRef.markForCheck();
            });
        }
      });
    }

    this.isAllExpanded = true;
  }

  handleLiveUpdatesSubscriptions(event: IScrollVisibility, competitionSection: ITypeSegment): void {
    if (event.visible) {
      if (event.reloadData) {
        this.loadCompetionSection(competitionSection, this.eventsBySports.eventsByTypeName, true).subscribe(events => {
          if (!this.subscriptionFlags[competitionSection.typeId]) {
            this.inPlayMainService.subscribeForUpdates(competitionSection.events);
            this.subscriptionFlags[competitionSection.typeId] = true;
          }
          this.changeDetectorRef.markForCheck();
        });
      } else {
        if (!this.subscriptionFlags[competitionSection.typeId]) {
          this.inPlayMainService.subscribeForUpdates(competitionSection.events);
          this.subscriptionFlags[competitionSection.typeId] = true;
        }
      }
    } else {
      this.inPlayMainService.unsubscribeForEventsUpdates(competitionSection, false);
      this.subscriptionFlags[competitionSection.typeId] = false;
    }
    this.changeDetectorRef.markForCheck();
  }

  /**
   * extends section data and sets availibility for Competition Pages
   * @param {Boolean} setAvailability
   */
  private extendSectionData(setAvailability: boolean = false): void {
    if (this.eventsBySports && this.sportInstance) {
      this.inPlayMainService.extendSectionWithSportInstance(this.eventsBySports, this.sportInstance);
      if (setAvailability) {
        this.setCompetitionPagesAvailability();
      }
    }
  }

  /**
   * Initial sections expanding/subscription on live updates
   */
  private processInitialData(data?: ISportSegment): void {
    const eventsBySports = data || this.eventsBySports;

    if (eventsBySports && eventsBySports.eventsByTypeName) {
      eventsBySports.eventsByTypeName.forEach((competitionSection: ITypeSegment, index: number) => {
        this.setExpandedFlag(competitionSection, index);
      });
    }
  }

  /**
   * set Expanded flag to manage loading data when section being expanded
   */
  private setExpandedFlag(competitionSection: ITypeSegment, index: number): void {
    const isExpanded = this.expandedLeaguesCount === undefined || index < this.expandedLeaguesCount;
    this.expandedFlags[competitionSection.typeId] = isExpanded;
    competitionSection.isExpanded = isExpanded;
  }

  private calculateIsAllExpanded(): void {
    const isCollapsedExists: boolean = _.some(this.expandedFlags, (isExpanded: boolean) => !isExpanded);
    this.isAllExpanded = !isCollapsedExists;
  }

  /**
   * Check if first market is selected in market selector
   */
  private isFirstMarketSelected(): boolean {
    return ['Main Market', 'Match Betting'].includes(this.selectedMarketName);
  }
}
