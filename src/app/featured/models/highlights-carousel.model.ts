import { ISportEvent } from '@core/models/sport-event.model';

export interface IHighlightsCarousel {
  _id: string;
  typeId: number;
  data: ISportEvent[];
  outcomeColumnsTitles: string[];
  svgId: string;
  title: string;
  sportId: number;
  inPlay: boolean;
}
