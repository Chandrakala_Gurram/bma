export interface IRpgConfig {
  gamesAmount?: number;
  seeMoreLink?: string;
  title?: string;
  categoryId?: string;
  bundleUrl?: string;
  loaderUrl?: string;
}

export interface IRpgApiConfig {
  GAMES_URL: string;
  FETCH_URL: string;
  XBC_LOADER: {
    data_bundle: string;
    src: string;
  };
}

export interface IRpgCarousel {
  maxNumOfGames: number;
  tilesPerSlide: number;
  onGameCountChange(gameConfigArrayToDisplay: any[]): void;
  onPlayClick(synthEvent: any, gameCode: string): void;
}

export interface IGamesResponse {
  baseUrl: string;
  games: {
    [key: string]: IGame
  };
}

export interface IGame {
  imageURL: string;
  name: string;
  gameCode: string;
}
