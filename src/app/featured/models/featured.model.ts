import { IOutputModule } from './output-module.model';

export interface IFeaturedModel {
  directiveName: string;
  modules: IOutputModule[];
  showTabOn: string;
  title: string;
  visible: boolean;
}
