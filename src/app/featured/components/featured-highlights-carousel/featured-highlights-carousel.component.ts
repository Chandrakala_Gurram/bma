import * as _ from 'underscore';
import { Component, OnInit, Input, OnDestroy, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { IHighlightsCarousel } from '@featured/models/highlights-carousel.model';
import { Router } from '@angular/router';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { DeviceService } from '@core/services/device/device.service';
import { CarouselService } from '@shared/directives/ng-carousel/carousel.service';
import { ISportEvent } from '@core/models/sport-event.model';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { RendererService } from '@shared/services/renderer/renderer.service';
import { DomToolsService } from '@coreModule/services/domTools/dom.tools.service';
import { Carousel } from '@shared/directives/ng-carousel/carousel.class';

@Component({
  selector: 'featured-highlight-carousel',
  templateUrl: './featured-highlights-carousel.component.html',
  styleUrls: ['featured-highlights-carousel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FeaturedHighlightsCarouselComponent implements OnInit, OnDestroy {
  @Input() highlightsCarousel: IHighlightsCarousel;
  @Input() sportName: string;
  @Input() outcomeColumnsTitles: string[];

  pageId: string = _.uniqueId('featuredHighlightsCarousel_');
  isCarouselInit: boolean;
  showCarouselButtons: boolean = false;

  private resizeListener: Function;

  constructor(
    private elementRef: ElementRef,
    private windowRef: WindowRefService,
    private router: Router,
    private domTools: DomToolsService,
    private carouselService: CarouselService,
    private renderer: RendererService,
    private routingHelperService: RoutingHelperService,
    private device: DeviceService
  ) {
  }

  ngOnInit(): void {
    if (this.highlightsCarousel.data.length > 1) {
      this.sortCards();
    }
    if (!this.device.isTouch()) {
      this.windowRef.nativeWindow.setTimeout(() => {
        this.initShowCarouselButtons();
      }, 0);
      this.resizeListener = this.renderer.renderer.listen(this.windowRef.nativeWindow, 'resize', () => {
        this.initShowCarouselButtons();
      });
    }
  }

  ngOnDestroy(): void {
    this.highlightsCarousel && this.carouselService.remove(this.highlightsCarousel._id);
    if (!this.device.isTouch()) {
      this.resizeListener && this.resizeListener();
    }
  }

  sortCards(): void {
    this.highlightsCarousel.data.sort((ev1: ISportEvent, ev2: ISportEvent): number => {
      const criteria1 = ev1.startTime === ev2.startTime ? 0 : (new Date(ev1.startTime)).getTime() - (new Date(ev2.startTime)).getTime();
      const criteria2 = ev1.displayOrder - ev2.displayOrder;
      const criteria3 = ev1.name === ev2.name ? 0 : ev1.name < ev2.name ? -1 : 1;
      return criteria1 || criteria2 || criteria3;
    });
  }

  trackByCard(i: number, element): string {
    return element.id;
  }

  /**
   * SEE ALL link
   * to competition page by typeID
   */
  competitionsNavigate(): void {
    const competitionPageUrl = this.routingHelperService.formCompetitionUrl({
      sport: this.highlightsCarousel.data[0].categoryCode.toLowerCase(),
      typeName: this.highlightsCarousel.data[0].typeName,
      className: this.highlightsCarousel.data[0].className
    });

    this.router.navigate([competitionPageUrl]);
  }

  nextSlide(): void {
    this.carousel.next();
  }

  prevSlide(): void {
    this.carousel.previous();
  }

  get showNext(): boolean {
    return this.carousel.currentSlide < this.carousel.slidesCount - 1;
  }

  get showPrev(): boolean {
    return this.carousel.currentSlide > 0;
  }

  public get isOneCard(): boolean {
    return this.highlightsCarousel.data && this.highlightsCarousel.data.length === 1;
  }

  public get isValidCarousel(): boolean {
    return !!(this.highlightsCarousel && this.highlightsCarousel.data && this.highlightsCarousel.data.length);
  }

  private get carousel(): Carousel {
    return this.highlightsCarousel ? this.carouselService.get(this.highlightsCarousel._id) : null;
  }

  private initShowCarouselButtons(): void {
    const carouselSlides = this.highlightsCarousel.data.length;
    const carouselOuterWidth = this.domTools.getWidth(this.elementRef.nativeElement.querySelector('.highlight-carousel'));
    const slideWidth = this.domTools.getWidth(this.elementRef.nativeElement.querySelector('.slide'));
    this.showCarouselButtons = carouselOuterWidth < (carouselSlides * slideWidth);
  }
}
