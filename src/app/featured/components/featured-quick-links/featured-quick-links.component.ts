import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import {
  IOFeaturedQuickLinksModel,
  IOquickLinkDataModel
} from '@featured/models/featured-quick-link.model';
import { Router } from '@angular/router';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { GtmService } from '@core/services/gtm/gtm.service';

@Component({
  selector: 'featured-quick-links',
  templateUrl: './featured-quick-links.component.html',
  styleUrls: ['featured-quick-links.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FeaturedQuickLinksComponent {
  @Input() quickLinks: IOFeaturedQuickLinksModel;
  @Input() sportName: string;

  constructor(
    private windowRef: WindowRefService,
    private router: Router,
    private gtmService: GtmService
  ) {}

  trackByLink(i: number, element: IOquickLinkDataModel): string {
    return `${i}_${element.id}`;
  }

  clickOnLink(event, item: IOquickLinkDataModel): void {
    const currentDomain = this.windowRef.nativeWindow.location.origin;

    event.preventDefault();

    this.gtmService.push('trackEvent', {
      event: 'trackEvent',
      eventCategory: 'quick links',
      eventAction: this.sportName || 'home',
      eventLabel: item.title
    });

    if (item.destination.indexOf(currentDomain) >= 0) {
      this.router.navigate([item.destination.replace(currentDomain, '')]);
    } else {
      this.windowRef.nativeWindow.location.href = item.destination;
    }
  }
}
