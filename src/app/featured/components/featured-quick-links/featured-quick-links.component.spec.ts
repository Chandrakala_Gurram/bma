import { FeaturedQuickLinksComponent } from '@featured/components/featured-quick-links/featured-quick-links.component';

describe('FeaturedQuickLinksComponent', () => {
  let component: FeaturedQuickLinksComponent;

  let windowRef;
  let gtmService;
  let router;
  let quickLinskData;
  let linkDestinationMock;
  let linkDestinationWithoutOrigin;
  let event;

  beforeEach(() => {
    windowRef = {
      nativeWindow: {
        location: {
          origin: 'https://bm-tst1.coral.co.uk',
          href: ''
        }
      }
    };

    router = {
      navigate: jasmine.createSpy()
    };

    gtmService = {
      push: jasmine.createSpy()
    };

    linkDestinationMock = 'https://bm-tst1.coral.co.uk/linkDestination';
    linkDestinationWithoutOrigin = '/linkDestination';

    quickLinskData = {
      data: [{
        destination: linkDestinationMock,
        displayOrder: -11,
        id: '5bf3c940c9e77c0001238a52',
        svg: '<symbol id="0169fbe3-ff05-3910-94bf-13a07fa2ba16"</symbol>',
        title: 'BBC'
      }]
    };

    event = {
      preventDefault: jasmine.createSpy()
    };

    component = new FeaturedQuickLinksComponent(windowRef, router, gtmService);
  });

  it('trackByLink() Compose unique string for each slide in collection', () => {
    const element = {
      id: 112,
      title: 'titleOFelement'
    } as any;

    expect(component.trackByLink(33, element)).toEqual('33_112');
  });

  it('clickOnLink() Check navigation by clicking on Quick link. Domain belongs to Coral', () => {
    component.clickOnLink(event, quickLinskData.data[0]);
    expect(gtmService.push).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith([linkDestinationWithoutOrigin]);
  });

  it('clickOnLink() Check navigation by clicking on Quick link. Domain does NOT belongs to Coral', () => {
    const changedLinkObj = quickLinskData.data[0];

    changedLinkObj.destination = 'https://wwww.nocoraldomain.co.uk/fakeUrl';
    component.clickOnLink(event, changedLinkObj);
    expect(component['windowRef'].nativeWindow.location.href).toEqual(changedLinkObj.destination);
  });

  it('should use OnPush strategy', () => {
    expect(FeaturedQuickLinksComponent['__annotations__'][0].changeDetection).toBe(0);
  });
});
