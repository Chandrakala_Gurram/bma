import { Component, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { RaceCardHomeComponent } from '@lazy-modules/lazyNextRacesTab/components/raceCardHome/race-card-home.component';

@Component({
  selector: 'featured-race-card-home',
  templateUrl: '../../../lazy-modules/lazyNextRacesTab/components/raceCardHome/race-card-home.component.html',
  styleUrls: ['../../../lazy-modules/lazyNextRacesTab/components/raceCardHome/race-card-home.component.less'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FeaturedRaceCardHomeComponent extends RaceCardHomeComponent {}
