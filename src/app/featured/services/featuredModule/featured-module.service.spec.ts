import { FeaturedModuleService } from './featured-module.service';
import environment from '@environment/oxygenEnvConfig';
import { actions } from '@core/services/newRelic/new-relic.constant';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('FeaturedModuleService', () => {
  let service: FeaturedModuleService;

  let cacheEventsService;
  let commentsService;
  let timeSyncService;
  let liveEventClockProviderService;
  let windowRefService;
  let newRelicService;
  let pubsubService;
  let ngZone;

  let pubSubTrigger: Function;

  beforeEach(() => {
    pubsubService = {
      subscribe: jasmine.createSpy('subscribe').and.callFake(
        (subscriberName, channel, channelFunction) => {
          pubSubTrigger = channelFunction;
        }
      ),
      publish: jasmine.createSpy('publish'),
      publishSync: jasmine.createSpy('publishSync'),
      API: pubSubApi
    };
    cacheEventsService = {
      store: jasmine.createSpy()
    };

    commentsService = {
      getCLockData: jasmine.createSpy()
    };

    timeSyncService = {
      getTimeDelta: jasmine.createSpy()
    };

    liveEventClockProviderService = {
      create: jasmine.createSpy()
    };

    windowRefService = {
      nativeWindow: {
        io: jasmine.createSpy().and.returnValue({
          on: jasmine.createSpy(),
          emit: jasmine.createSpy(),
          removeListener: jasmine.createSpy(),
          removeAllListeners: jasmine.createSpy(),
          io: { disconnect: jasmine.createSpy() }
        })
      }
    };

    newRelicService = {
      API: actions,
      addPageAction: jasmine.createSpy('addPageAction')
    };

    ngZone = {
      runOutsideAngular: jasmine.createSpy().and.callFake(fn => fn())
    };

    service = new FeaturedModuleService(
      cacheEventsService,
      commentsService,
      timeSyncService,
      liveEventClockProviderService,
      windowRefService,
      pubsubService,
      newRelicService,
      ngZone
    );
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
    expect(service['ioSettings']).toEqual(jasmine.any(Object));
    expect(service['moduleStates']).toEqual(jasmine.any(Map));
    expect(service['subscribedFeaturedTabModules']).toEqual(jasmine.any(Array));
    expect(service['connection']).toBe(null);

    service['currentSport'] = 0;
    pubSubTrigger();
    expect(ngZone.runOutsideAngular).toHaveBeenCalled();
  });

  it('get tabModuleStates', () => {
    expect(service.tabModuleStates).toBe(service['moduleStates']);
  });

  it('getSubscribedFeaturedTabModules', () => {
    expect(service.getSubscribedFeaturedTabModules()).toBe(service['subscribedFeaturedTabModules']);
  });

  it('addModuleToSubscribedFeaturedTabModules', () => {
    service.addModuleToSubscribedFeaturedTabModules('1');
    service.addModuleToSubscribedFeaturedTabModules('2');
    expect(service['subscribedFeaturedTabModules'].length).toBe(2);

    service.addModuleToSubscribedFeaturedTabModules('2');
    expect(service['subscribedFeaturedTabModules'].length).toBe(2);
  });

  it('clearSubscribedFeaturedTabModules', () => {
    service['subscribedFeaturedTabModules'] = ['1', '2'];
    service.clearSubscribedFeaturedTabModules();
    expect(service['subscribedFeaturedTabModules'].length).toBe(0);
  });

  it('reconnect', () => {
    service.disconnect = jasmine.createSpy();
    service.startConnection = jasmine.createSpy();
    service['currentSport'] = 1;
    service['currentPageType'] = 'sport';

    service.reconnect();
    expect(service.disconnect).toHaveBeenCalled();
    expect(service.startConnection).toHaveBeenCalledWith(1, 'sport');
  });

  it('emit', () => {
    const event = 'ON_ERROR';
    const data = ['l', 'c'];

    service.establishConnection(0, 'sport');
    service.emit(event, data);
    expect(service['connection'].emit).toHaveBeenCalledWith(event, data);
  });

  it('removeEventListener', () => {
    const event = 'ON_SELECTED';
    const handler = () => { };

    service.establishConnection(0, 'sport');
    service.removeEventListener(event, handler);
    expect(service['connection'].removeListener).toHaveBeenCalledWith(event, handler);
  });

  it('removeAllListeners', () => {
    const events = ['ON_OK', 'ON_FAIL'];

    service.establishConnection(0, 'sport');
    service.removeAllListeners(events);
    expect(service['connection'].removeAllListeners).toHaveBeenCalledWith(events);
  });

  it('#disconnect should terminate connection', () => {
    service.disconnect();
    service.establishConnection(0, 'sport');
    service.disconnect();
    expect(service['connection'].io.disconnect).toHaveBeenCalledTimes(1);
  });

  it('startConnection', () => {
    const sportIdMock = 0;
    service.establishConnection = jasmine.createSpy();
    service.addEventListener = jasmine.createSpy('addEventListener').and.callFake((event, cb) => {
      if (event === 'connect') {
        cb();
        expect(pubsubService.publishSync).toHaveBeenCalledWith(pubsubService.API.FEATURED_CONNECT_STATUS, true);
      } else {
        cb();
      }
    });


    service.startConnection(sportIdMock, 'sport');

    expect(service['currentSport']).toEqual(sportIdMock);
    expect(service['currentPageType']).toEqual('sport');
    expect(service.establishConnection).toHaveBeenCalledWith(sportIdMock, 'sport');
    expect(service.addEventListener).toHaveBeenCalledWith('connect', jasmine.any(Function));
    expect(service.addEventListener).toHaveBeenCalledWith('reconnect_failed', jasmine.any(Function));
    expect(service.addEventListener).toHaveBeenCalledWith('error', jasmine.any(Function));
    expect(service.addEventListener).toHaveBeenCalledWith('reconnect_attempt', jasmine.any(Function));
    expect(service.addEventListener).toHaveBeenCalledWith('reconnect', jasmine.any(Function));
    expect(service.addEventListener).toHaveBeenCalledWith('connect_error', jasmine.any(Function));
    expect(newRelicService.addPageAction).toHaveBeenCalledTimes(5);
  });

  it('startConnection eventhub', () => {
    const hubIndexdMock = 0;

    spyOn(service, 'establishConnection');
    spyOn(service, 'addEventListener');

    service.startConnection(hubIndexdMock, 'eventhub');

    expect(service['currentSport']).toEqual(hubIndexdMock);
    expect(service['currentPageType']).toEqual('eventhub');
  });

  it('should addEventListener for connection', () => {
    service.establishConnection(0, 'sport');
    service.addEventListener('testMessage', () => { });

    expect(service['connection'].on).toHaveBeenCalledWith('testMessage', jasmine.any(Function));
  });

  it('should handle Namespace MS error message', () => {
    service.errorsMessagesHandler('randomError');

    expect(pubsubService.publish).not.toHaveBeenCalled();

    service.errorsMessagesHandler('Invalid namespace');

    expect(pubsubService.publish).toHaveBeenCalled();
  });

  it('establishConnection', () => {
    const sportIdMock = 10;

    service.establishConnection(sportIdMock, 'sport');
    expect(windowRefService.nativeWindow.io).toHaveBeenCalledWith(
      `${environment.FEATURED_SPORTS}/${sportIdMock}`, service['ioSettings']
    );
    expect(service['connection']).toBeTruthy();
    expect(ngZone.runOutsideAngular).toHaveBeenCalled();
  });

  it('should establish WS connection for eventhub with EH namespace', () => {
    const hubIndexMock = 1;
    service.establishConnection(hubIndexMock, 'eventhub');

    expect(windowRefService.nativeWindow.io).toHaveBeenCalledWith(
      `${environment.FEATURED_SPORTS}/h${hubIndexMock}`, service['ioSettings']
    );
    expect(ngZone.runOutsideAngular).toHaveBeenCalled();
  });

  describe('#onError', () => {
    const onErrorCallback = jasmine.createSpy('onErrorCallback');
    const createMockData = (isBoosted) => {
      const ioOnHandler = (event, cb) => {
        cb(isBoosted);
      };
      windowRefService.nativeWindow.io.and.returnValue({
        on: jasmine.createSpy('on').and.callFake(ioOnHandler),
        io: { disconnect: jasmine.createSpy() }
      });

      service.startConnection(0, 'sport');
    };

    it('should not call callback function', () => {
      createMockData(true);

      service.onError(onErrorCallback);
      expect(service['connection'].on).toHaveBeenCalledWith('error', jasmine.any(Function));
      expect(service['connection'].on).toHaveBeenCalledWith('disconnect', jasmine.any(Function));
      expect(onErrorCallback).not.toHaveBeenCalled();
      expect(service['connection'].io.disconnect).not.toHaveBeenCalled();
    });

    it('should reconnect', () => {
      createMockData(false);

      service.onError(onErrorCallback);
      expect(onErrorCallback).toHaveBeenCalled();
      expect(service['connection'].io.disconnect).toHaveBeenCalledTimes(2);
    });
  });

  it('#cacheEvents should cache data', () => {
    const data: any = {};
    service.cacheEvents(data);
    expect(cacheEventsService.store).toHaveBeenCalledWith('ribbonEvents', data);
  });

  it('#cacheEvents should NOT cache data', () => {
    const data: any = {};
    service.cacheEvents(undefined);
    expect(cacheEventsService.store).not.toHaveBeenCalledWith('ribbonEvents', data);
  });

  it('addClock', () => {
    const events: any = [
      {},
      { comments: { latestPeriod: {} }, categoryCode: '' },
      { comments: { latestPeriod: {} }, categoryCode: '' }
    ];

    expect(service.addClock(events)).toBe(events);
    expect(commentsService.getCLockData).toHaveBeenCalledTimes(2);
    expect(liveEventClockProviderService.create).toHaveBeenCalledTimes(2);
    expect(timeSyncService.getTimeDelta).toHaveBeenCalled();
  });

  it('isConnected', () => {
    service['connection'] = null;
    expect(service['isConnected']()).toBeFalsy();

    service['connection'] = { connected: false };
    expect(service['isConnected']()).toBeFalsy();

    service['connection'] = { connected: true };
    expect(service['isConnected']()).toBeTruthy();
  });

  describe('#trackDataReceived', () => {
    it('should track featured structure changed', () => {
      service.trackDataReceived({
        events: ['12213', '12312312'],
        someData: 'someData'
      }, 'FEATURED_STRUCTURE_CHANGED');

      expect(newRelicService.addPageAction).toHaveBeenCalledWith('FEATURED_WS_STRUCTURE_RECEIVED', {
        message: 'FEATURED_STRUCTURE_CHANGED',
        payloadSize: 53
      });
    });
  });
});
