import { CommentsService } from '@core/services/comments/comments.service';
import { IFeaturedModel } from './../../models/featured.model';
import environment from '@environment/oxygenEnvConfig';
import { Injectable, NgZone } from '@angular/core';
import * as _ from 'underscore';
import { IIoSettings } from '../../models/io-settings.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { LiveEventClockProviderService } from '@shared/components/liveClock/live-event-clock-provider.service';
import { TimeSyncService } from '@core/services/timeSync/time-sync.service';
import { CacheEventsService } from '@core/services/cacheEvents/cache-events.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { NewRelicService } from '@root/app/core/services/newRelic/new-relic.service';
import { IConstant } from '@root/app/core/services/models/constant.model';

@Injectable({
  providedIn: 'root'
})
export class FeaturedModuleService {

  private readonly ioSettings: IIoSettings;
  private readonly moduleStates: Map<string, boolean>;
  private connection;
  private subscribedFeaturedTabModules: string[];
  private currentSport: number;
  private currentPageType: string;

  constructor(
    private cacheEventsService: CacheEventsService,
    private commentsService: CommentsService,
    private timeSyncService: TimeSyncService,
    private liveEventClockProviderService: LiveEventClockProviderService,
    private windowRef: WindowRefService,
    private pubSubService: PubSubService,
    private newRelicService: NewRelicService,
    private ngZone: NgZone
  ) {
    this.ioSettings = {
      transports: ['websocket'],
      upgrade: false,
      reconnectionDelay: 1000,
      forceNew: true,
      timeout: 10000,
      reconnectionAttempts: 15,
      query: ''
    };
    this.moduleStates = new Map();
    this.subscribedFeaturedTabModules = [];
    this.connection = null;

    this.pubSubService.subscribe('featuredModule', this.pubSubService.API.RELOAD_COMPONENTS, () => {
      if (!this.isConnected()) {
        this.reconnect();
      }
    });
  }

  get tabModuleStates(): Map<string, boolean> {
    return this.moduleStates;
  }

  /**
   * Get subscribed modules
   * @returns {Array}
   */
  getSubscribedFeaturedTabModules(): string[] {
    return this.subscribedFeaturedTabModules;
  }

  /**
   * Add module to subscribed modules
   * @param {String} id
   */
  addModuleToSubscribedFeaturedTabModules(id: string): void {
    if (this.subscribedFeaturedTabModules.indexOf(id) === -1) {
      this.subscribedFeaturedTabModules.push(id);
    }
  }

  /**
   * Clear subscribed modules
   */
  clearSubscribedFeaturedTabModules(): void {
    this.subscribedFeaturedTabModules = [];
  }

  /**
   * Reconnect
   */
  reconnect(): void {
    this.disconnect();
    this.startConnection(this.currentSport, this.currentPageType);
  }

  /**
   * Emit connection event
   * @param {String} event
   * @param {*} data
   */
  emit(event: string, data: string | string[]): void {
    this.connection.emit(event, data);
  }

  /**
   * Remove event listener from connection
   * @param {String} event
   * @param {Function} handler
   */
  removeEventListener(event: string, handler: Function): void {
    this.connection.removeListener(event, handler);
  }

  /**
   * Remove all connection listeners
   * @param events {Array}
   */
  removeAllListeners(events: string[]): void {
    this.connection.removeAllListeners(events);
  }

  /**
   * Disconnect
   */
  disconnect(): void {
    if (this.connection) {
      this.connection.io.disconnect();
    }
  }

  /**
   * Start MS connection
   * @param sportId - sportId or event hub index
   * @param pageType - 'sport' or 'eventhub'
   */
  startConnection(sportId: number, pageType: string): void {
    this.currentSport = sportId;
    this.currentPageType = pageType;
    this.establishConnection(sportId, pageType);
    this.addEventListener('connect', () => {
      this.pubSubService.publishSync(this.pubSubService.API.FEATURED_CONNECT_STATUS, true);
      this.newRelicService.addPageAction(this.newRelicService.API.FEATURED_WS_CONNECTION_SUCCESS);
    });
    this.addEventListener('reconnect_failed', () => {
      this.pubSubService.publishSync(this.pubSubService.API.FEATURED_CONNECT_STATUS, false);
      this.newRelicService.addPageAction(this.newRelicService.API.FEATURED_WS_RECONNECTION_FAILED);
    });
    this.addEventListener('reconnect_attempt', (attemp: number) => {
      this.newRelicService.addPageAction(this.newRelicService.API.FEATURED_WS_RECONNECTION_ATTEMP, {
        attemp
      });
    });
    this.addEventListener('reconnect', (attemp: number) => {
      this.newRelicService.addPageAction(this.newRelicService.API.FEATURED_WS_RECONNECTION_SUCCESS, {
        attemp
      });
    });
    this.addEventListener('connect_error', (error: IConstant) => {
      this.newRelicService.addPageAction(this.newRelicService.API.FEATURED_WS_CONNECTION_FAILED, {
        error
      });
    });

    this.addEventListener('error', (error) => this.errorsMessagesHandler(error));
  }

  /**
   * Establish Connection for featured, sportPage or Eventhub
   * @param sportId - sportId or event hub index
   * @param pageType - 'sport' or 'eventhub'
   */
  establishConnection(sportId: number, pageType: string): void {
    let nameSpace: string = sportId.toString();

    if (pageType === 'eventhub') {
      nameSpace = `h${sportId}`;
    }

    // namespace for sports pages. homepage is a sport with ID === 0
    // each sport has its own connection with different sportId
    const url = `${environment.FEATURED_SPORTS}/${nameSpace}`;
    this.ngZone.runOutsideAngular(() => {
      this.connection = this.windowRef.nativeWindow.io(url, this.ioSettings);
    });
  }

  /**
   * Add event listener to connection
   * @param {String} event
   * @param {Function} cb
   */
  addEventListener(event: string, cb: Function): void {
    this.connection.on(event, cb);
  }

  /**
   * Connection error handler
   * @param {Function} cb
   */
  onError(cb: Function): void {
    const connectionErrorHandler = isBooted => {
      if (isBooted) {
        return;
      }

      this.reconnect();
      cb();
    };

    this.connection.on('error', connectionErrorHandler);
    this.connection.on('disconnect', connectionErrorHandler);
  }

  /**
   * Handler for microservice error messages
   * @param error
   */
  errorsMessagesHandler(error: string): void {
    if (error === 'Invalid namespace') {
      this.pubSubService.publish(this.pubSubService.API.NAMESPACE_ERROR);
    }
  }

  /**
   * Cache events
   * @param {Object} data
   */
  cacheEvents(data: IFeaturedModel): void {
    if (!data) {
      return;
    }
    this.cacheEventsService.store('ribbonEvents', data);
  }

  /**
   * Add clock data to events
   * @param {Array} events
   * @returns {Array}
   */
  addClock(events: ISportEvent[]): ISportEvent[] {
    const serverTimeDelta = this.timeSyncService.getTimeDelta();

    events.forEach(event => {
      if (!event.comments || !event.comments.latestPeriod) {
        return;
      }

      // TODO move this initialisation to liveClock directive
      const clockInitialData = this.commentsService.getCLockData(_.extend(
        {
          startTime: event.comments.latestPeriod.startTime,
          responseCreationTime: event.responseCreationTime,
          sport: event.categoryCode.toLowerCase(),
          ev_id: event.comments.latestPeriod.eventId,
        },
        event.comments.latestPeriod
      ));

      // ToDo import after migration
      event.clock = this.liveEventClockProviderService.create(
        serverTimeDelta,
        clockInitialData
      );
    });

    return events;
  }

  trackDataReceived(data: IConstant, message: string): void {
    this.newRelicService.addPageAction(this.newRelicService.API.FEATURED_WS_STRUCTURE_RECEIVED, {
      message,
      payloadSize: JSON.stringify(data).length
    });
  }

  /**
   * Check if connected
   */
  private isConnected(): boolean {
    return this.connection && this.connection.connected;
  }
}
