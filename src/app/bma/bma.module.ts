import { FormsModule } from '@angular/forms';
import { APP_INITIALIZER, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ModalModule } from 'angular-custom-modal';
import { SharedModule } from '@sharedModule/shared.module';
import { FavouritesModule } from '@favouritesModule/favourites.module';

import { BmaInit } from '@bmaModule/services/bmaRunService/bma-init';
import { BmaRunService } from '@bmaModule/services/bmaRunService/bma-run.service';
import { ContactUsComponent } from '@bmaModule/components/contactUs/contact-us.component';
import { NotFoundComponent } from '@bmaModule/components/404/404.component';
import {
  RightColumnWidgetWrapperComponent
} from '@bmaModule/components/rightColumn/rightColumnWidgetWrapper/right-column-widget-wrapper.component';
import { RightColumnWidgetComponent } from '@bmaModule/components/rightColumn/rightColumnWidget/right-column-widget.component';
import {
  RightColumnWidgetItemComponent
} from '@bmaModule/components/rightColumn/rightColumnWidgetItem/right-column-widget-item.component';
import { WBetslipComponent } from '@bmaModule/components/rightColumn/wBetslip/w-betslip.component';
import { WMatchCentreComponent } from '@bmaModule/components/rightColumn/wMatchCentre/w-match-centre.component';
import { InplayHomeTabComponent } from '@bmaModule/components/inlayHomeTab/inplay-home-tab.component';
import { BannersModule } from '@banners/banners.module';
import { LocaleService } from '@coreModule/services/locale/locale.service';
import { StaticComponent } from '@bmaModule/components/static/static.component';
import { RetailOverlayComponent } from '@retailModule/components/retailOverlay/retail-overlay.component';
import * as criticalLangData from '@localeModule/translations/en-US/critical-lang-data';
import { LiveStreamWrapperComponent } from '@bma/components/liveStream/live-stream-wrapper.component';

// Vanilla overrides
import { BmaMainComponent } from '@bmaModule/components/bmaMain/bma-main.component';
import { HomeComponent } from '@bmaModule/components/home/home.component';
import { TutorialOverlayComponent } from '@bmaModule/components/tutorialOverlay/tutorial-overlay.component';

@NgModule({
  imports: [
    SharedModule,
    ModalModule,
    FormsModule,
    FavouritesModule,
    BannersModule
  ],
  providers: [
    BmaRunService,
    {
      provide: APP_INITIALIZER,
      useFactory: BmaInit,
      deps: [BmaRunService],
      multi: true
    },
  ],
  declarations: [
    ContactUsComponent,
    TutorialOverlayComponent,
    HomeComponent,
    NotFoundComponent,
    RightColumnWidgetWrapperComponent,
    RightColumnWidgetComponent,
    RightColumnWidgetItemComponent,
    WBetslipComponent,
    WMatchCentreComponent,
    InplayHomeTabComponent,
    StaticComponent,
    RetailOverlayComponent,
    BmaMainComponent,
    LiveStreamWrapperComponent,
  ],
  entryComponents: [
    ContactUsComponent,
    TutorialOverlayComponent,
    HomeComponent,
    NotFoundComponent,
    RightColumnWidgetWrapperComponent,
    RightColumnWidgetComponent,
    RightColumnWidgetItemComponent,
    WBetslipComponent,
    WMatchCentreComponent,
    InplayHomeTabComponent,
    StaticComponent,
    RetailOverlayComponent,
    LiveStreamWrapperComponent,
    BmaMainComponent,
  ],
  exports: [
    ContactUsComponent,
    TutorialOverlayComponent,
    NotFoundComponent,
    RightColumnWidgetWrapperComponent,
    RightColumnWidgetComponent,
    RightColumnWidgetItemComponent,
    WBetslipComponent,
    WMatchCentreComponent,
    InplayHomeTabComponent,
    StaticComponent,
    RetailOverlayComponent,
    LiveStreamWrapperComponent,
    BmaMainComponent,
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class BmaModule {
  constructor(private localeService: LocaleService) {
    this.localeService.setLangData(criticalLangData);
  }
}
