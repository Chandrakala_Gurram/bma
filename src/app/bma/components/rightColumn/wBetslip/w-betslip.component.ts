import { timer as observableTimer } from 'rxjs';

import { Component, ViewChild, OnInit, OnDestroy, ViewContainerRef, ComponentFactory, ComponentRef } from '@angular/core';
import { DynamicComponentsService } from '@core/services/dynamicComponents/dynamic-components.service';
import {
  DYNAMIC_BET_HISTORY_PAGE,
  DYNAMIC_CASH_OUT_PAGE,
  DYNAMIC_OPEN_BETS,
  DYNAMIC_IN_SHOP_BETS_PAGE
} from '@app/dynamicLoader/dynamic-loader-manifest';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { DeviceService } from '@core/services/device/device.service';
import { BetslipTabsService } from '@core/services/betslipTabs/betslip-tabs.service';
import { IBetslipTab } from '@core/services/betslipTabs/betslip-tab.model';
import { UserService } from '@core/services/user/user.service';


@Component({
  selector: 'w-betslip',
  templateUrl: 'w-betslip.component.html',
  styleUrls: ['./w-betslip.component.less']
})
export class WBetslipComponent implements OnInit, OnDestroy {
  title: string = 'wBeslip';
  betslipTabs: IBetslipTab[];
  sessionStateDefined: boolean = false;
  mobile: boolean;
  tabsMap: { [key: string]: string };
  MAIN_VIEWS = {
    MY_BETS: 'myBets',
    BETSLIP: 'betslip'
  };

  activeTab: IBetslipTab = { id: 1, name: '', title: '', url: '' };
  activeView: string = '';
  quickDepositIFrameOpened = false;
  errorMsg: string = '';
  @ViewChild('betHistoryPage', {read: ViewContainerRef}) set bhCont(container: ViewContainerRef) {
    if (!container) {
      return;
    }

    if (this.betHistoryPageFactory) {
      this.dynamicComponentsService.createDynamicComponent(
        this.betHistoryPageRef,
        container,
        this.betHistoryPageFactory,
        { isUsedFromWidget: true },
      );
    } else {
      this.dynamicComponentsService.getDynamicComponent(
        DYNAMIC_BET_HISTORY_PAGE,
        this.betHistoryPageRef,
        container,
        this.betHistoryPageFactory,
        { isUsedFromWidget: true },
      );
    }
  }

  @ViewChild('cashOutPage', {read: ViewContainerRef}) set coCont(container: ViewContainerRef) {
    if (!container) {
      return;
    }

    if (this.cashOutPageFactory) {
      this.dynamicComponentsService.createDynamicComponent(
        this.cashOutPageRef,
        container,
        this.cashOutPageFactory,
        { isUsedFromWidget: true },
      );
    } else {
      this.dynamicComponentsService.getDynamicComponent(
        DYNAMIC_CASH_OUT_PAGE,
        this.cashOutPageRef,
        container,
        this.cashOutPageFactory,
        { isUsedFromWidget: true },
      );
    }
  }

  @ViewChild('openBets', {read: ViewContainerRef}) set obCont(container: ViewContainerRef) {
    if (!container) {
      return;
    }

    if (this.openBetsFactory) {
      this.dynamicComponentsService.createDynamicComponent(
        this.openBetsRef,
        container,
        this.openBetsFactory,
        { isUsedFromWidget: true , area: 'open-bets-page'},
      );
    } else {
      this.dynamicComponentsService.getDynamicComponent(
        DYNAMIC_OPEN_BETS,
        this.openBetsRef,
        container,
        this.openBetsFactory,
        { isUsedFromWidget: true, area: 'open-bets-page' },
      );
    }
  }

  @ViewChild('inShopBetsPage', {read: ViewContainerRef}) set isbCont(container: ViewContainerRef) {
    if (!container) {
      return;
    }

    if (this.inShopBetsPageFactory) {
      this.dynamicComponentsService
        .createDynamicComponent(this.inShopBetsPageRef, container, this.inShopBetsPageFactory, { mode: 'BET_HISTORY' });
    } else {
      this.dynamicComponentsService
        .getDynamicComponent(DYNAMIC_IN_SHOP_BETS_PAGE, this.inShopBetsPageRef, container, this.inShopBetsPageFactory);
    }
  }

  private betHistoryPageFactory: ComponentFactory<any>;
  private betHistoryPageRef: ComponentRef<any>;
  private cashOutPageFactory: ComponentFactory<any>;
  private cashOutPageRef: ComponentRef<any>;
  private openBetsFactory: ComponentFactory<any>;
  private openBetsRef: ComponentRef<any>;
  private inShopBetsPageFactory: ComponentFactory<any>;
  private inShopBetsPageRef: ComponentRef<any>;
  private editMyAccaUnsavedInWidget: boolean;

  constructor(
    protected pubSubService: PubSubService,
    private localeService: LocaleService,
    private dynamicComponentsService: DynamicComponentsService,
    private deviceService: DeviceService,
    private betslipTabsService: BetslipTabsService,
    private userService: UserService
  ) {
    this.mobile = this.deviceService.isMobileOrigin && this.deviceService.isMobile;
    this.tabsMap = {
      cashout: this.localeService.getString('app.betslipTabs.cashout'),
      openbets: this.localeService.getString('app.betslipTabs.openbets'),
      bethistory: this.localeService.getString('app.betslipTabs.betHistory')
    };

    this.openBetslip = this.openBetslip.bind(this);
  }

  ngOnInit(): void {
    this.betslipTabsService.getTabsList().subscribe((tabs: IBetslipTab[]) => {
      this.betslipTabs = tabs;

      // select second tab (cashout / open bets)
      this.activeTab.id = tabs[1].id;
      if (!this.userService.status) {
        this.showError(tabs[1].name);
      }
    });

    this.pubSubService.subscribe(this.title, [this.pubSubService.API.SESSION_LOGOUT,
      this.pubSubService.API.HOME_BETSLIP], this.openBetslip);

    this.pubSubService.subscribe(this.title, this.pubSubService.API.LOAD_CASHOUT_BETS,
      () => this.selectMyBetsTab(
        this.betslipTabsService.createTab('cashout', 1)
      ));

    this.pubSubService.subscribe(this.title, 'LOAD_UNSETTLED_BETS',
       () => this.selectMyBetsTab(
        this.betslipTabsService.createTab('openbets', 2)
      ));

    this.pubSubService.subscribe(this.title, 'LOAD_BET_HISTORY',
      () => this.selectMyBetsTab(
        this.betslipTabsService.createTab('betHistory', 3)
      ));

    this.pubSubService.subscribe(this.title, 'EMA_UNSAVED_IN_WIDGET', (unsaved: boolean) => {
      this.editMyAccaUnsavedInWidget = unsaved;
    });

    // No need to render betslip container instantly. Get advantage of betslip lazy loading
    observableTimer(100).subscribe(this.openBetslip);

    this.pubSubService.subscribe(this.title, this.pubSubService.API.TOGGLE_QUICK_DEPOSIT_IFRAME, (isOpened: boolean) => {
      this.quickDepositIFrameOpened = isOpened;
    });
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe(this.title);
    this.betHistoryPageRef && this.betHistoryPageRef.destroy();
    this.cashOutPageRef && this.cashOutPageRef.destroy();
    this.openBetsRef && this.openBetsRef.destroy();
    this.inShopBetsPageRef && this.inShopBetsPageRef.destroy();
  }

  handleTabClick(data) {
    if (!this.editMyAccaUnsavedInWidget) {
      this.selectBetSlipTab(data.id, data.tab);
    } else {
      this.pubSubService.publish(this.pubSubService.API.EMA_OPEN_CANCEL_DIALOG);
    }
  }

  setActiveView(view: string): void {
    if (this.editMyAccaUnsavedInWidget) {
      this.pubSubService.publish(this.pubSubService.API.EMA_OPEN_CANCEL_DIALOG);
    } else {
      this.activeView = view;
    }
  }

  /**
   * Select betslip widget tab for tablet
   * @param {string} label
   * @param {IBetslipTab} tab
   */
  private selectBetSlipTab(label: string, tab: IBetslipTab): void {
    this.updateActiveTab(tab);
    this.pubSubService.publish(this.pubSubService.API.BETSLIP_LABEL, label);
  }

  /**
   * Select my bets widget tab for tablet
   * @param {IBetslipTab} tab
   */
  private selectMyBetsTab(tab: IBetslipTab): void {
    this.updateActiveTab(tab);
    this.setActiveView(this.MAIN_VIEWS.MY_BETS);
  }

  /**
   * update active tab id and name and show error if user is not logged in
   * @param {IBetslipTab} tab
   */
  private updateActiveTab(tab: IBetslipTab ): void {
    this.activeTab = { ...this.activeTab, id: tab.id, name: tab.name };

    if (!this.userService.status) {
      this.showError(tab.name);
    }
  }

  private openBetslip(): void {
    this.setActiveView(this.MAIN_VIEWS.BETSLIP);
  }

  /**
   * Show error message when user is logged out
   */
  private showError(activeTabName: string): void {
    const activeTabId: string = activeTabName.replace(/\s/g, '').toLowerCase();
    const page: string = this.localeService.getString(`app.betslipTabs.${activeTabId}`).toLowerCase();

    this.errorMsg = this.localeService.getString(
      'app.loginToSeePageMessage',
      activeTabId === 'cashout' ? { page: `${ page } bets` } : { page }
    );
  }
}
