import { of as observableOf } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';
import * as _ from 'underscore';

import { WBetslipComponent } from './w-betslip.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('WBetslipComponent', () => {
  let component: WBetslipComponent;

  let pubSubService;
  let localeService;
  let dynamicComponentsService;
  let deviceService;
  let betslipTabsService;
  let subscribeEvents;
  let userService;

  beforeEach(() => {
    subscribeEvents = {
      'LOAD_CASHOUT_BETS': null,
      'LOAD_UNSETTLED_BETS': null,
      'LOAD_BET_HISTORY': null,
      'EMA_UNSAVED_IN_WIDGET': null
    };
    pubSubService = {
      API: pubSubApi,
      publish: jasmine.createSpy('publish'),
      subscribe: jasmine.createSpy('publish').and.callFake((subscriber, method, cb) => {
        if (typeof method === 'string' && ['LOAD_CASHOUT_BETS', 'LOAD_UNSETTLED_BETS', 'LOAD_BET_HISTORY',
          'EMA_UNSAVED_IN_WIDGET'].includes(method)) {
          subscribeEvents[method] = cb;
        }
      }),
      unsubscribe: jasmine.createSpy('unsubscribe')
    };
    localeService = {
      getString: jasmine.createSpy('getString').and.returnValue('betslip')
    };
    dynamicComponentsService = {
      createDynamicComponent: jasmine.createSpy('dynamicComponentsService'),
      getDynamicComponent: jasmine.createSpy('getDynamicComponent')
    };
    deviceService = {
      isMobileOrigin: true,
      isMobile: true
    };
    betslipTabsService = {
      createTab: jasmine.createSpy('betslipTabsService'),
      getTabsList: jasmine.createSpy('getTabsList')
    };
    userService = {
      status: false
    };

    component = new WBetslipComponent(
      pubSubService,
      localeService,
      dynamicComponentsService,
      deviceService,
      betslipTabsService,
      userService
    );

    component['betHistoryPageRef'] = {
      destroy: jasmine.createSpy('destroy')
    } as any;
    component['cashOutPageRef'] = {
      destroy: jasmine.createSpy('destroy')
    } as any;
    component['openBetsRef'] = {
      destroy: jasmine.createSpy('destroy')
    } as any;
    component['inShopBetsPageRef'] = {
      destroy: jasmine.createSpy('destroy')
    } as any;

    component['betslipContainerView'] = {
      parentInjector: null
    } as any;

    component.sessionStateDefined = false;
    component.activeTab = { id: 0, name: '', title: '', url: '' };
  });

  it('constructor', () => {
    expect(component.mobile).toBeTruthy();
    expect(_.keys(component['tabsMap'])).toEqual(['cashout', 'openbets', 'bethistory']);
    expect(localeService.getString).toHaveBeenCalledTimes(3);
  });

  it('ngOnInit', fakeAsync(() => {
    component['openBetslip'] = jasmine.createSpy();
    component['selectMyBetsTab'] = jasmine.createSpy();
    component.title = 'title';
    betslipTabsService.createTab = jasmine.createSpy().and.callFake((tabName, id) => {
      return {
        title: tabName,
        name: tabName,
        url: '',
        id
      };
    });
    betslipTabsService.getTabsList.and.returnValue(observableOf([{ id: 0, name: 'Cashout' }, { id: 2, name: 'Open Bets' }]));
    pubSubService.subscribe = jasmine.createSpy('publish').and.callFake((subscriber, method, cb) => cb());
    component.ngOnInit();

    tick(100);
    expect(betslipTabsService.getTabsList).toHaveBeenCalled();
    expect(component.betslipTabs.length).toEqual(2);
    expect(component.errorMsg).toEqual('betslip');
    expect(pubSubService.subscribe).toHaveBeenCalledTimes(6);
    expect(pubSubService.subscribe).toHaveBeenCalledWith(
      component.title,
      jasmine.arrayContaining([pubSubService.API.SESSION_LOGOUT, pubSubService.API.HOME_BETSLIP]),
      jasmine.any(Function)
    );
    expect(pubSubService.subscribe).toHaveBeenCalledWith(component.title, 'LOAD_CASHOUT_BETS', jasmine.any(Function));
    expect(pubSubService.subscribe).toHaveBeenCalledWith(component.title, 'LOAD_UNSETTLED_BETS', jasmine.any(Function));
    expect(pubSubService.subscribe).toHaveBeenCalledWith(component.title, 'LOAD_BET_HISTORY', jasmine.any(Function));
    expect(pubSubService.subscribe).toHaveBeenCalledWith(component.title, 'EMA_UNSAVED_IN_WIDGET', jasmine.any(Function));
    expect(component['openBetslip']).toHaveBeenCalledTimes(2);
    expect(betslipTabsService.createTab).toHaveBeenCalledWith('cashout', 1);
    expect(betslipTabsService.createTab).toHaveBeenCalledWith('openbets', 2);
    expect(betslipTabsService.createTab).toHaveBeenCalledWith('betHistory', 3);
    expect(component['selectMyBetsTab']).toHaveBeenCalledWith(jasmine.objectContaining({
      title: 'cashout',
      name: 'cashout',
      url: '',
      id: 1
    }));
    expect(component['selectMyBetsTab']).toHaveBeenCalledWith(jasmine.objectContaining({
      title: 'openbets',
      name: 'openbets',
      url: '',
      id: 2
    }));
    expect(component['selectMyBetsTab']).toHaveBeenCalledWith(jasmine.objectContaining({
      title: 'betHistory',
      name: 'betHistory',
      url: '',
      id: 3
    }));
  }));

  it('ngOnInit: should not show error when user is logged in', fakeAsync(() => {
    userService.status = true;
    betslipTabsService.getTabsList.and.returnValue(observableOf([{ id: 0, name: 'Cashout' }, { id: 2, name: 'Open Bets' }]));
    component.ngOnInit();

    tick(100);
    expect(betslipTabsService.getTabsList).toHaveBeenCalled();
    expect(component.errorMsg).toEqual('');
  }));

  it('ngOnDestroy', () => {
    component.ngOnDestroy();

    expect(component['betHistoryPageRef'].destroy).toHaveBeenCalled();
    expect(component['cashOutPageRef'].destroy).toHaveBeenCalled();
    expect(component['openBetsRef'].destroy).toHaveBeenCalled();
    expect(component['inShopBetsPageRef'].destroy).toHaveBeenCalled();
  });

  describe('handleTabClick', () => {
    it('handleTabClick', () => {
      component['selectBetSlipTab'] = jasmine.createSpy();
      component.handleTabClick({ id: 0, tab: 'test' });
      expect(component['selectBetSlipTab']).toHaveBeenCalledWith(0, 'test');
    });

    it('handleTabClick (unseved acca in bs widget)', () => {
      component['editMyAccaUnsavedInWidget'] = true;
      component.handleTabClick({ id: 0, tab: 'test' });
      expect(pubSubService.publish).toHaveBeenCalledWith(pubSubService.API.EMA_OPEN_CANCEL_DIALOG);
    });
  });

  describe('setActiveView', () => {
    it('setActiveView', () => {
      component.setActiveView('betslip');
      expect(component.activeView).toEqual('betslip');
    });

    it('setActiveView', () => {
      component['editMyAccaUnsavedInWidget'] = true;
      component.setActiveView('mybets');
      expect(component.activeView).not.toEqual('mybets');
      expect(pubSubService.publish).toHaveBeenCalledWith(pubSubService.API.EMA_OPEN_CANCEL_DIALOG);
    });
  });

  it('LOAD_CASHOUT_BETS', () => {
    component.tabsMap = {cashout: 'cashout'};
    betslipTabsService.getTabsList.and.returnValue(observableOf([{ id: 0, name: 'Cashout' }, { id: 2, name: 'Open Bets' }]));
    component['selectMyBetsTab'] = jasmine.createSpy().and.callFake(() => {});

    component.ngOnInit();
    subscribeEvents['LOAD_CASHOUT_BETS']();
    expect(component['selectMyBetsTab']).toHaveBeenCalled();
    expect(betslipTabsService.createTab).toHaveBeenCalledWith('cashout', 1);
  });

  it('LOAD_UNSETTLED_BETS', () => {
    component['selectMyBetsTab'] = jasmine.createSpy().and.callFake(() => {});
    betslipTabsService.getTabsList.and.returnValue(observableOf([{ id: 0, name: 'Cashout' }, { id: 2, name: 'Open Bets' }]));

    component.ngOnInit();
    subscribeEvents['LOAD_UNSETTLED_BETS']();
    expect(component['selectMyBetsTab']).toHaveBeenCalled();
    expect(betslipTabsService.createTab).toHaveBeenCalledWith('openbets', 2);
  });

  it('LOAD_BET_HISTORY', () => {
    component.tabsMap = {bethistory: 'bethistory'};
    component['selectMyBetsTab'] = jasmine.createSpy().and.callFake(() => {});
    betslipTabsService.getTabsList.and.returnValue(observableOf([{ id: 0, name: 'Cashout' }, { id: 2, name: 'Open Bets' }]));

    component.ngOnInit();
    subscribeEvents['LOAD_BET_HISTORY']();
    expect(component['selectMyBetsTab']).toHaveBeenCalled();
    expect(betslipTabsService.createTab).toHaveBeenCalledWith('betHistory', 3);
  });

  it('EMA_UNSAVED_IN_WIDGET', () => {
    betslipTabsService.getTabsList.and.returnValue(observableOf([{ id: 0, name: 'Cashout' }, { id: 2, name: 'Open Bets' }]));
    component.ngOnInit();
    subscribeEvents['EMA_UNSAVED_IN_WIDGET'](true);
    expect(component['editMyAccaUnsavedInWidget']).toEqual(true);
  });

  describe('set bhCont', () => {
    it('should create dynamic component', () => {
      const container: any = {};
      component['betHistoryPageFactory'] = <any>{};
      component['bhCont'] = container;

      expect(dynamicComponentsService.createDynamicComponent).toHaveBeenCalled();
      expect(dynamicComponentsService.createDynamicComponent).toHaveBeenCalledWith(
        jasmine.anything(),
        container,
        jasmine.anything(),
        { isUsedFromWidget: true }
      );
    });

    it('should get dynamic component', () => {
      const container: any = {};
      const factory = null;
      component['betHistoryPageFactory'] = factory;
      component['bhCont'] = container;

      expect(dynamicComponentsService.getDynamicComponent).toHaveBeenCalled();
      expect(dynamicComponentsService.getDynamicComponent).toHaveBeenCalledWith(
        jasmine.anything(),
        jasmine.anything(),
        container,
        factory,
        { isUsedFromWidget: true }
      );
    });

    it('shoud not create or get dynamic component', () => {
      component['bhCont'] = null;
      expect(dynamicComponentsService.createDynamicComponent).not.toHaveBeenCalled();
      expect(dynamicComponentsService.getDynamicComponent).not.toHaveBeenCalled();
    });
  });

  describe('set coCont', () => {
    it('should create dynamic component', () => {
      const container: any = {};
      component['cashOutPageFactory'] = <any>{};
      component['coCont'] = container;

      expect(dynamicComponentsService.createDynamicComponent).toHaveBeenCalled();
      expect(dynamicComponentsService.createDynamicComponent).toHaveBeenCalledWith(
        jasmine.anything(),
        container,
        jasmine.anything(),
        { isUsedFromWidget: true }
      );
    });

    it('should get dynamic component', () => {
      const container: any = {};
      const factory = null;
      component['cashOutPageFactory'] = factory;
      component['coCont'] = container;

      expect(dynamicComponentsService.getDynamicComponent).toHaveBeenCalled();
      expect(dynamicComponentsService.getDynamicComponent).toHaveBeenCalledWith(
        jasmine.anything(),
        jasmine.anything(),
        container,
        factory,
        { isUsedFromWidget: true }
      );
    });

    it('shoud not create or get dynamic component', () => {
      component['coCont'] = null;
      expect(dynamicComponentsService.createDynamicComponent).not.toHaveBeenCalled();
      expect(dynamicComponentsService.getDynamicComponent).not.toHaveBeenCalled();
    });
  });

  describe('set obCont', () => {
    it('should create dynamic component', () => {
      const container: any = {};
      component['openBetsFactory'] = <any>{};
      component['obCont'] = container;

      expect(dynamicComponentsService.createDynamicComponent).toHaveBeenCalled();
      expect(dynamicComponentsService.createDynamicComponent).toHaveBeenCalledWith(
        jasmine.anything(),
        container,
        jasmine.anything(),
        { isUsedFromWidget: true, area: 'open-bets-page' }
      );
    });

    it('should get dynamic component', () => {
      const container: any = {};
      const factory = null;
      component['openBetsFactory'] = factory;
      component['obCont'] = container;

      expect(dynamicComponentsService.getDynamicComponent).toHaveBeenCalled();
      expect(dynamicComponentsService.getDynamicComponent).toHaveBeenCalledWith(
        jasmine.anything(),
        jasmine.anything(),
        container,
        factory,
        { isUsedFromWidget: true, area: 'open-bets-page' }
      );
    });

    it('shoud not create or get dynamic component', () => {
      component['obCont'] = null;
      expect(dynamicComponentsService.createDynamicComponent).not.toHaveBeenCalled();
      expect(dynamicComponentsService.getDynamicComponent).not.toHaveBeenCalled();
    });
  });

  describe('set isbCont', () => {
    it('shoud create dynamic component', () => {
      component['inShopBetsPageFactory'] = {} as any;
      component.isbCont = {} as any;
      expect(dynamicComponentsService.createDynamicComponent).toHaveBeenCalledTimes(1);
    });

    it('shoud get dynamic component', () => {
      component['inShopBetsPageFactory'] = null;
      component.isbCont = {} as any;
      expect(dynamicComponentsService.getDynamicComponent).toHaveBeenCalledTimes(1);
    });

    it('shoud not create or get dynamic component', () => {
      component.isbCont = null;
      expect(dynamicComponentsService.createDynamicComponent).not.toHaveBeenCalled();
      expect(dynamicComponentsService.getDynamicComponent).not.toHaveBeenCalled();
    });
  });

  describe('widget actions', () => {
    describe('select widget tabs', () => {
      let mockData;

      beforeEach(() => {
        mockData = { id: 0, name: 'betslip' } as any;
        component['updateActiveTab'] = jasmine.createSpy();
      });

      it('@selectBetSlipTab', () => {
        component['selectBetSlipTab']('betslip', mockData);
        expect(component['updateActiveTab']).toHaveBeenCalledWith(mockData);
        expect(pubSubService.publish).toHaveBeenCalledWith(pubSubService.API.BETSLIP_LABEL, 'betslip');
      });

      it('@selectMyBetsTab', () => {
        component['setActiveView'] = jasmine.createSpy();

        component['selectMyBetsTab'](mockData);
        expect(component['updateActiveTab']).toHaveBeenCalledWith(mockData);
        expect(component['setActiveView']).toHaveBeenCalledWith(component.MAIN_VIEWS.MY_BETS);
      });
    });

    it('@updateActiveTab', () => {
      const mockData = { id: 2, name: 'myBets' } as any;
      component.activeTab = {} as any;

      component['updateActiveTab'](mockData);
      expect(component.activeTab).toEqual(mockData);
      expect(component.errorMsg).toEqual('betslip');
    });

    it('@updateActiveTab: should not show error when user is logged in', () => {
      userService.status = true;
      const mockData = { id: 2, name: 'myBets' } as any;
      component.activeTab = {} as any;

      component['updateActiveTab'](mockData);
      expect(component.activeTab).toEqual(mockData);
      expect(component.errorMsg).toEqual('');
    });

    it('@openBetslip', () => {
      component['setActiveView'] = jasmine.createSpy();

      component['openBetslip']();
      expect(component['setActiveView']).toHaveBeenCalledWith(component.MAIN_VIEWS.BETSLIP);
    });
  });

  describe('@showError', () => {
    beforeEach(() => {
      localeService.getString = jasmine.createSpy('getString').and.callFake((token: string, args: { page: string }) => {
        if (token.indexOf('cashout') > -1 && token.indexOf('betslipTabs') > -1) {
          return 'cash out';
        } else if (args && args.page && args.page.indexOf('cashout')) {
          return args && args.page;
        } else {
          return 'betslip';
        }
      });
    });

    it('should show error message when user is logged out', () => {
      const activeTabName = 'Open Bets';
      component['showError'](activeTabName);

      expect(component.errorMsg).toEqual('betslip');
      expect(localeService.getString).toHaveBeenCalledWith('app.betslipTabs.openbets');
      expect(localeService.getString).toHaveBeenCalledWith('app.loginToSeePageMessage', { page: 'betslip' });
    });

    it('should show error message for cash out tab', () => {
      const activeTabName = 'Cash Out';
      component['showError'](activeTabName);

      expect(component.errorMsg).toEqual('cash out bets');
      expect(localeService.getString).toHaveBeenCalledWith('app.betslipTabs.cashout');
      expect(localeService.getString).toHaveBeenCalledWith('app.loginToSeePageMessage', { page: 'cash out bets' });
    });
  });
});
