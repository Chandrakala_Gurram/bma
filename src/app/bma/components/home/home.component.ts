import { Component, OnInit } from '@angular/core';
import { DynamicLoaderService } from '@app/dynamicLoader/dynamic-loader.service';

import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { IFeaturedModule } from '@featured/components/featured-tab/featured-module.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'home',
  templateUrl: 'home.component.html'
})

export class HomeComponent extends AbstractOutletComponent implements OnInit {
  showBanner: boolean;
  femData: any;
  ribbon: IFeaturedModule[];
  isEnhancedMultiplesEnabled: boolean = false;

  constructor(
    private cms: CmsService,
    protected dynamicComponentLoader: DynamicLoaderService
  ) {
    super()/* istanbul ignore next */;

    this.showBanner = true;
  }

  ngOnInit(): void {
    this.cms.getRibbonModule().subscribe((initialData: { getRibbonModule: IFeaturedModule[]; getMMOutcomesByEventType: any; }) => {
      // to avoid this the bottle neck and having 500 error b/c of e.g.
      // banners have not been loaded.
      this.femData = initialData.getMMOutcomesByEventType;
      this.ribbon = initialData.getRibbonModule;

      this.hideSpinner();
    }, () => {
      this.showError();
    });

    this.getIsEnhancedMultiplesEnabled().subscribe((isEnhancedMultiplesEnabled: boolean) => {
      this.isEnhancedMultiplesEnabled = isEnhancedMultiplesEnabled;
    });
  }

  private getIsEnhancedMultiplesEnabled(): Observable<boolean> {
    return this.cms.getToggleStatus('EnhancedMultiples');
  }
}
