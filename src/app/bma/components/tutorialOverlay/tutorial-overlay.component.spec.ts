import { TutorialOverlayComponent } from './tutorial-overlay.component';
import { commandApi } from '@core/services/communication/command/command-api.constant';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';
import { fakeAsync, flush } from '@angular/core/testing';

describe('#TutorialOverlayComponent', () => {
  let component: TutorialOverlayComponent;

  let windowRef,
    rendererService,
    elRef,
    storage,
    pubSubService,
    device,
    location,
    domTools,
    nativeBridgeService,
    dialogService,
    user,
    commandService,
    changeDetectorRef;

  const domElement = {};
  beforeEach(() => {
    windowRef = {
      document: {
        querySelector: jasmine.createSpy().and.returnValue(domElement)
      }
    };
    rendererService = { renderer: {
        addClass: jasmine.createSpy(),
        removeClass: jasmine.createSpy(),
        listen: jasmine.createSpy().and.returnValue(() => {
        })
      }
    };
    elRef = {
      nativeElement: {
        remove: jasmine.createSpy()
      }
    };
    storage = {
      set: jasmine.createSpy(),
      get: jasmine.createSpy()
    };
    device = {
      isMobile: true
    };
    location = {
      path: jasmine.createSpy().and.returnValue('/home/featured')
    };
    domTools = {
      closest: jasmine.createSpy()
    };
    nativeBridgeService = {
      onClosePopup: jasmine.createSpy(),
      onOpenPopup: jasmine.createSpy()
    };
    dialogService = {
      openedPopups: 'openedPopups'
    };
    pubSubService = {
      subscribe: jasmine.createSpy().and.callFake((command: string, command2: string, callback: Function) => {
        callback('mobile');
      }),
      unsubscribe: jasmine.createSpy(),
      API: pubSubApi
    };
    user = {
      isRouletteJourney: jasmine.createSpy().and.returnValue(false)
    };
    commandService = {
      API: commandApi,
      register: jasmine.createSpy('register').and.callFake((cmd, cb) => cb())
    };
    changeDetectorRef = {
      markForCheck: jasmine.createSpy('markForCheck')
    };

    component = new TutorialOverlayComponent(
      windowRef,
      rendererService,
      elRef,
      storage,
      pubSubService,
      device,
      location,
      domTools,
      nativeBridgeService,
      dialogService,
      user,
      commandService,
      changeDetectorRef
    );
  });

  describe('#ngOnInit', () => {
    it('should assign value to tutorialOpenedClassName', () => {
      component.ngOnInit();
      expect(component.tutorialOpenedClassName).toBe('tut-overlay-open');
      expect(windowRef.document.querySelector).toHaveBeenCalledWith('html');
      expect(windowRef.document.querySelector).toHaveBeenCalledWith('html body');
      expect(commandService.register).toHaveBeenCalledWith(commandApi.SHOW_TUTORIAL_OVERLAY, jasmine.any(Function));
      expect(user.isRouletteJourney).toHaveBeenCalled();
      expect(rendererService.renderer.addClass).toHaveBeenCalled();
    });

    it('should not toggle tutorial overlay if roulette journey active', () => {
      component['toggleTutorial'] = jasmine.createSpy('toggleTutorial');
      user.isRouletteJourney.and.returnValue(true);
      component.ngOnInit();
      expect(component['toggleTutorial']).not.toHaveBeenCalled();
    });

    it('should remove class if device is not mobile', () => {
      device.isMobile = false;
      component.ngOnInit();
      expect(rendererService.renderer.removeClass).toHaveBeenCalled();
    });
  });

  describe('#closeTutorial', () => {

    it('should run if elemBody is truthy', () => {
      component.elmBody = windowRef.document.querySelector('html body');
      component['tutorialClose'].next = jasmine.createSpy();
      component['tutorialClose'].complete = jasmine.createSpy();
      component.elmHtml = windowRef.document.querySelector('html');
      component.closeTutorial(false);
      expect(component.elmBody).toBeTruthy();
      expect(rendererService.renderer.removeClass).toHaveBeenCalledWith(component.elmBody, component.tutorialOpenedClassName);
      expect(rendererService.renderer.removeClass).toHaveBeenCalledWith(component.elmHtml, component.tutorialOpenedClassName);
      expect(elRef.nativeElement.remove).toHaveBeenCalled();
      expect(storage.set).toHaveBeenCalledWith('tutorial', true);
      expect(nativeBridgeService.onClosePopup).toHaveBeenCalledWith('tutorial_overlay', 'openedPopups');
      expect(component['tutorialClose'].next).toHaveBeenCalled();
      expect(component['tutorialClose'].complete).toHaveBeenCalled();
    });

    it('should not run if elemBody is falsy', () => {
      component.elmBody = null;
      component['tutorialClose'].next = jasmine.createSpy();
      component['tutorialClose'].complete = jasmine.createSpy();
      component.closeTutorial(false);
      expect(component.elmBody).toBeFalsy();
      expect(rendererService.renderer.removeClass).not.toHaveBeenCalledWith(component.elmBody, component.tutorialOpenedClassName);
      expect(elRef.nativeElement.remove).not.toHaveBeenCalled();
      expect(storage.set).not.toHaveBeenCalledWith('tutorial', true);
      expect(nativeBridgeService.onClosePopup).not.toHaveBeenCalledWith('tutorial_overlay', 'openedPopups');
      expect(component['tutorialClose'].next).not.toHaveBeenCalled();
      expect(component['tutorialClose'].complete).not.toHaveBeenCalled();
    });

    it('should not run onClosePopUp if shown is true', () => {
      component.elmBody = windowRef.document.querySelector('html body');
      component.closeTutorial(true);
      expect(nativeBridgeService.onClosePopup).not.toHaveBeenCalledWith('tutorial_overlay', 'openedPopups');
    });
  });

  describe('#toggleTutorial', () => {

    it('should assign value to userMenuElem', () => {
      component['toggleTutorial']();
      expect(component.userMenuElem).toBe(windowRef.document.querySelector('.header-user-menu'));
    });

    it('isHome = true and tutorialShown = false, should add class on mobile type', () => {
      component['topMenuClickHandler'] = jasmine.createSpy();
      const val = component['homePageFeaturedUrl'];
      location.path.and.returnValue(val);
      rendererService.renderer.listen.and.callFake((a, b, cb) => {
        cb({target: true});
      });
      component['toggleTutorial']();
      expect(component['topMenuClickHandler']).toHaveBeenCalledTimes(3);
      expect(nativeBridgeService.onOpenPopup).toHaveBeenCalledWith('tutorial_overlay');
      expect(pubSubService.subscribe)
        .toHaveBeenCalledWith('tutorialOverlay', pubSubService.API.DEVICE_VIEW_TYPE_CHANGED_NEW, jasmine.any(Function));
      expect(rendererService.renderer.addClass).toHaveBeenCalledWith(component.elmBody, 'offset-sm');
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
      expect(rendererService.renderer.addClass).toHaveBeenCalledWith(component.elmBody, component.tutorialOpenedClassName);
      expect(rendererService.renderer.addClass).toHaveBeenCalledWith(component.elmHtml, component.tutorialOpenedClassName);
    });

    it('should remove class if type is not mobile', () => {
      const val = component['homePageFeaturedUrl'];
      pubSubService.subscribe = jasmine.createSpy().and.callFake((command: string, command2: string, callback: Function) => {
        callback('xmobile');
      });
      location.path.and.returnValue(val);
      component['toggleTutorial']();
      expect(pubSubService.subscribe)
        .toHaveBeenCalledWith('tutorialOverlay', pubSubService.API.DEVICE_VIEW_TYPE_CHANGED_NEW, jasmine.any(Function));
      expect(rendererService.renderer.addClass).not.toHaveBeenCalledWith(component.elmBody, 'offset-sm');
      expect(rendererService.renderer.removeClass).toHaveBeenCalledWith(component.elmBody, 'offset-sm');
    });

    it('isHome = false and tutorialShown = true, should call closeTutorial', fakeAsync(() => {
      spyOn(component, 'closeTutorial');
      location.path.and.returnValue('test');
      storage.get.and.returnValue(true);
      component['toggleTutorial']();
      flush();
      expect(component['closeTutorial']).toHaveBeenCalled();
    }));

    it('isHome = false and tutorialShown = false, should not call closeTutorial', fakeAsync(() => {
      spyOn(component, 'closeTutorial');
      location.path.and.returnValue('test');
      storage.get.and.returnValue(false);
      component['toggleTutorial']();
      flush();
      expect(component['closeTutorial']).not.toHaveBeenCalled();
    }));
  });

  describe('#topMenuClickHandler', () => {

    it('should call getUserMenuElm', () => {
      const event = {target: {} as HTMLElement} as any;
      spyOn(component, 'closeTutorial');
      spyOn<any>(component, 'getUserMenuElm');
      component['topMenuClickHandler'](event);
      expect(component['getUserMenuElm']).toHaveBeenCalled();
    });

    it('should call closeTutorial when getUserMenuElm is truthy', fakeAsync(() => {
      const event = {target: {} as HTMLElement} as any;
      spyOn(component, 'closeTutorial');
      spyOn<any>(component, 'getUserMenuElm').and.returnValue(true);
      component['topMenuClickHandler'](event);
      flush();
      expect(component['closeTutorial']).toHaveBeenCalled();
    }));

    it('should not call closeTutorial when getUserMenuElm is falsy', fakeAsync(() => {
      const event = {target: {} as HTMLElement} as any;
      spyOn(component, 'closeTutorial');
      spyOn<any>(component, 'getUserMenuElm').and.returnValue(false);
      component['topMenuClickHandler'](event);
      flush();
      expect(component['closeTutorial']).not.toHaveBeenCalled();
    }));
  });

  it('getUserMenuElm', () => {
    const event = {target: {} as HTMLElement} as any;
    component.headerClassName = '.test-header-class-name';
    component['getUserMenuElm'](event);
    expect(JSON.stringify(domTools.closest.calls.mostRecent().args[0]))
      .toEqual('{}');
    expect(domTools.closest.calls.mostRecent().args[1])
      .toEqual(component.headerClassName);
  });

  it('ngOnDestroy', () => {
    component['userMenuClickListener'] = jasmine.createSpy('userMenuClickListener');
    component['userMenuTouchStartListener'] = jasmine.createSpy('userMenuClickListener');
    component['userMenuTouchEndListener'] = jasmine.createSpy('userMenuClickListener');

    component.ngOnDestroy();

    expect(pubSubService.unsubscribe).toHaveBeenCalledWith('tutorialOverlay');
    expect(component['userMenuClickListener']).toHaveBeenCalled();
    expect(component['userMenuTouchStartListener']).toHaveBeenCalled();
    expect(component['userMenuTouchEndListener']).toHaveBeenCalled();
  });
});
