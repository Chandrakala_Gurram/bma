import { Component, ElementRef, OnDestroy, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Subject } from 'rxjs';

import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { StorageService } from '@core/services/storage/storage.service';
import { DeviceService } from '@core/services/device/device.service';
import { Location } from '@angular/common';
import { DomToolsService } from '@coreModule/services/domTools/dom.tools.service';
import { NativeBridgeService } from '@app/core/services/nativeBridge/native-bridge.service';
import { DialogService } from '@app/core/services/dialogService/dialog.service';
import { PubSubService } from '@app/core/services/communication/pubsub/pubsub.service';
import { UserService } from '@core/services/user/user.service';
import { RendererService } from '@shared/services/renderer/renderer.service';
import { CommandService } from '@core/services/communication/command/command.service';

@Component({
  selector: 'tutorial-overlay',
  templateUrl: 'tutorial-overlay.component.html',
  styleUrls: ['tutorial-overlay.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TutorialOverlayComponent implements OnInit, OnDestroy {
  elmBody: Element;
  elmHtml: Element;
  userMenuElem: Element;
  tutorialVisible: boolean;
  tutorialOpenedClassName: string;
  headerClassName: string = '.navbar-wrapper-right';
  isMobile: boolean = this.device.isMobile;
  private userMenuClickListener: () => void;
  private userMenuTouchStartListener: () => void;
  private userMenuTouchEndListener: () => void;

  private homePageFeaturedUrl: string = '/home/featured';

  private tutorialClose = new Subject();

  constructor(
    private windowRef: WindowRefService,
    private rendererService: RendererService,
    private elRef: ElementRef,
    private storage: StorageService,
    private pubSubService: PubSubService,
    public device: DeviceService,
    private location: Location,
    private domTools: DomToolsService,
    private nativeBridgeService: NativeBridgeService,
    private dialogService: DialogService,
    private user: UserService,
    private commandService: CommandService,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.tutorialOpenedClassName = 'tut-overlay-open';
    this.elmHtml = this.windowRef.document.querySelector('html');
    this.elmBody = this.windowRef.document.querySelector('html body');

    this.commandService.register(this.commandService.API.SHOW_TUTORIAL_OVERLAY, () => {
      if (!this.user.isRouletteJourney()) {
        this.toggleTutorial();
      }
      return this.tutorialClose.toPromise();
    });

    this.device.isMobile ? this.rendererService.renderer.addClass(this.elmBody, 'offset-sm')
      : this.rendererService.renderer.removeClass(this.elmBody, 'offset-sm');
  }

  ngOnDestroy(): void {
    this.destroyEvents();
  }

  /**
   * Close tutorial overlay.
   */
  closeTutorial(shown: boolean = false): void {
    if (this.elmBody) {
      this.rendererService.renderer.removeClass(this.elmBody, this.tutorialOpenedClassName);
      this.rendererService.renderer.removeClass(this.elmHtml, this.tutorialOpenedClassName);
      this.destroyEvents();
      this.elRef.nativeElement.remove();
      this.storage.set('tutorial', true);
      this.tutorialClose.next();
      this.tutorialClose.complete();
      if (!shown) {
        this.nativeBridgeService.onClosePopup('tutorial_overlay', this.dialogService.openedPopups);
      }
    }
  }

  /**
   * Check visibility of tutorial overlay.
   */
  private toggleTutorial(): void {
    const location = this.location.path();
    const isHomeURL = location === '' || location === this.homePageFeaturedUrl;
    const tutorialShown = this.storage.get('tutorial');
    this.userMenuElem = this.windowRef.document.querySelector('.header-user-menu');

    if (isHomeURL && !tutorialShown) {
      this.nativeBridgeService.onOpenPopup('tutorial_overlay');

      // Subscribe to view type change event (after resize of orientation change).
      this.pubSubService.subscribe('tutorialOverlay', this.pubSubService.API.DEVICE_VIEW_TYPE_CHANGED_NEW, (type: string) => {
        type === 'mobile' ? this.rendererService.renderer.addClass(this.elmBody, 'offset-sm')
          : this.rendererService.renderer.removeClass(this.elmBody, 'offset-sm');
      });

      this.userMenuClickListener = this.rendererService.renderer.listen(this.windowRef.document, 'click',
        (event: CustomEvent) => this.topMenuClickHandler(event));
      this.userMenuTouchStartListener = this.rendererService.renderer.listen(this.windowRef.document, 'touchstart',
        (event: CustomEvent) => this.topMenuClickHandler(event));
      this.userMenuTouchEndListener = this.rendererService.renderer.listen(this.windowRef.document, 'touchend',
        (event: CustomEvent) => this.topMenuClickHandler(event));

      // Add className to body to disable scrolling.
      this.rendererService.renderer.addClass(this.elmBody, this.tutorialOpenedClassName);
      this.rendererService.renderer.addClass(this.elmHtml, this.tutorialOpenedClassName);

      this.tutorialVisible = true;
    } else if (tutorialShown && !isHomeURL) {
      setTimeout(() => this.closeTutorial(true));
    } else {
      this.tutorialClose.next();
      this.tutorialClose.complete();
    }

    this.changeDetectorRef.markForCheck();
  }

  private topMenuClickHandler(event: CustomEvent): void {
    if (this.getUserMenuElm(event)) {
      setTimeout(() => this.closeTutorial());
    }
  }

  private getUserMenuElm(event: CustomEvent): HTMLElement {
    return this.domTools.closest(event.target as HTMLElement, this.headerClassName);
  }

  /**
   * Unsync from Connect events and remove event listenters.
   */
  private destroyEvents(): void {
    this.userMenuClickListener && this.userMenuClickListener();
    this.userMenuTouchStartListener && this.userMenuTouchStartListener();
    this.userMenuTouchEndListener && this.userMenuTouchEndListener();
    this.pubSubService.unsubscribe('tutorialOverlay');
  }
}
