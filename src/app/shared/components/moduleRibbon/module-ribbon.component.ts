import { AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Event, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Location } from '@angular/common';
import * as _ from 'underscore';

import { UserService } from '@core/services/user/user.service';
import { ModuleRibbonService } from '@core/services/moduleRibbon/module-ribbon.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';

import { IModuleRibbonTab } from '@core/services/cms/models';
import { IConstant } from '@core/services/models/constant.model';
import { SessionService } from '@authModule/services/session/session.service';

@Component({
  selector: 'module-ribbon',
  templateUrl: 'module-ribbon.component.html'
})
export class ModuleRibbonComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() ribbon: IModuleRibbonTab[];
  @ViewChild('moduleRibbon') moduleRibbon: ElementRef;

  homeTabUrl: string;
  activeTab: IConstant;
  moduleList: Partial<IModuleRibbonTab>[];

  private routeListener: Subscription;
  private readonly title = 'ModuleRibbonComponent';
  protected privateMarketTabCreated: boolean;

  constructor(
    protected location: Location,
    protected ribbonService: ModuleRibbonService,
    protected user: UserService,
    protected pubSubService: PubSubService,
    protected router: Router,
    protected sessionService: SessionService,
  ) {}

  /**
   * Initialization function
   */
  ngOnInit(): void {
    this.moduleList = this.ribbonService.moduleList.length
      ? this.ribbonService.moduleList : this.ribbonService.filterTabs(this.ribbon);

    this.setActiveTab();

    this.sessionService.whenUserSession()
      .subscribe(() => this.addPrivateMarketTab(), err => console.error(err));

    this.pubSubService.subscribe(this.title, [this.pubSubService.API.SUCCESSFUL_LOGIN], () => {
      this.addPrivateMarketTab();
    });

    this.routeListener = this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.setActiveTab();
      }
    });
  }

  ngAfterViewInit() {
    if (performance.mark) { performance.mark('BMA:FMP'); }
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe(this.title);
    this.routeListener.unsubscribe();
  }

  protected addPrivateMarketTab(): void {
    if (this.user && this.user.status && !this.isOnPrivateMarketTab() && !this.privateMarketTabCreated) {
      this.privateMarketTabCreated = true;
      this.ribbonService.getPrivateMarketTab(_.clone(this.ribbon)).subscribe(result => {
        this.moduleList = [...result];
        this.setLocation();
      }, () => this.privateMarketTabCreated = false);
    }
  }

  /**
   * Set Location
   * @private
   */
  protected setLocation(): void {
    if (this.canRedirectToHomePage()) {
      this.router.navigate(['/']);
    } else if (this.canRedirectToPrivateMarketsTab()) {
      this.router.navigate(['home', 'private-markets']);
    }
  }

  protected isOnPrivateMarketTab(): boolean {
    return this.location.isCurrentPathEqualTo('/home/private-markets');
  }

  protected canRedirectToHomePage() {
    return !this.ribbonService.isPrivateMarketsTab() && this.isOnPrivateMarketTab();
  }

  protected isCurrentPathEmpty(): boolean {
    return this.router.url.split(/[?#]/)[0] === '/';
  }

  protected canRedirectToPrivateMarketsTab() {
    return this.isCurrentPathEmpty() && this.ribbonService.isPrivateMarketsTab();
  }

  /**
   * Set Active Tab
   * @param {string} currentUrl
   */
  private setActiveTab(currentUrl?: string): void {
    const url = currentUrl || this.location.path();
    const activeTab = _.find(this.moduleList, m => m.url === url);
    const currentId = activeTab ? activeTab.id : 'tab-featured';

    this.homeTabUrl = `${this.isCurrentPathEmpty() ? '/home/featured' : this.location.path()}`;
    this.activeTab = {
      id: currentId,
      url: activeTab ? activeTab.url : ''
    };
  }
}
