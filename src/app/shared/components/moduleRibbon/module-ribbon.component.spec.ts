import { ModuleRibbonComponent } from '@shared/components/moduleRibbon/module-ribbon.component';
import { fakeAsync, tick } from '@angular/core/testing';
import { of, throwError } from 'rxjs';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('ModuleRibbonComponent', () => {
  const title = 'ModuleRibbonComponent';

  let moduleRibbonComponent,
    location,
    ribbonService,
    user,
    pubSubService,
    router,
    sessionService;

  beforeEach(() => {
    location = {
      isCurrentPathEqualTo: jasmine.createSpy('isCurrentPathEqualTo'),
      path: jasmine.createSpy('path')
    };
    ribbonService = {
      moduleList: [{}],
      removeTab: jasmine.createSpy('removeTab'),
      isPrivateMarketsTab: jasmine.createSpy('isPrivateMarketsTab'),
      getPrivateMarketTab: jasmine.createSpy('getPrivateMarketTab').and.returnValue(of([]))
    };
    user = {};
    pubSubService = {
      API: pubSubApi,
      subscribe: jasmine.createSpy('subscribe').and.callFake((place, key, callback) => {
        callback();
      }),
      unsubscribe: jasmine.createSpy()
    };
    router = {
      events: {
        subscribe: jasmine.createSpy('subscribe').and.returnValue({
          unsubscribe: jasmine.createSpy()
        })
      },
      navigate: jasmine.createSpy('navigate'),
      url: '/'
    };
    sessionService = {
      whenUserSession: jasmine.createSpy('whenUserSession').and.returnValue(of({}))
    };
    moduleRibbonComponent = new ModuleRibbonComponent(
      location, ribbonService, user, pubSubService, router, sessionService
    );
    moduleRibbonComponent.moduleRibbon = null;
  });


  describe('@ngOnInit', () => {
    beforeEach(() => {
      sessionService.whenUserSession = jasmine.createSpy('sessionService.whenSession').and.returnValue(of({}));
    });

    it('ngOnInit success', fakeAsync(() => {
      moduleRibbonComponent.user = {
        status: true
      };
      moduleRibbonComponent.ngOnInit();
      tick();
      expect(pubSubService.subscribe)
        .toHaveBeenCalledWith(title, ['SUCCESSFUL_LOGIN'], jasmine.any(Function));
      expect(ribbonService.getPrivateMarketTab).toHaveBeenCalledTimes(1);
    }));

    it('ngOnInit - user status = false', fakeAsync(() => {
      moduleRibbonComponent.user = {};
      moduleRibbonComponent.ngOnInit();
      tick();

      expect(pubSubService.subscribe)
        .toHaveBeenCalledWith(title, ['SUCCESSFUL_LOGIN'], jasmine.any(Function));
      expect(ribbonService.getPrivateMarketTab).not.toHaveBeenCalled();
    }));

    it('ngOnInit error', () => {
      moduleRibbonComponent.user = {
        status: true
      };
      sessionService.whenUserSession = jasmine.createSpy('sessionService.whenSession').and.returnValue(throwError(null));
      moduleRibbonComponent.ngOnInit();
      expect(ribbonService.getPrivateMarketTab).toHaveBeenCalledTimes(1);
    });

    it('ngOnInit - redirect to private-markets page if "private markets" tab is available and user is' +
      'currently on Home Page', fakeAsync(() => {
      moduleRibbonComponent.user = { status: true };
      sessionService.whenUserSession = jasmine.createSpy('sessionService.whenSession').and.returnValue(of({}));

      moduleRibbonComponent.router.url = '/';
      ribbonService.isPrivateMarketsTab.and.returnValue(true);

      moduleRibbonComponent.ngOnInit();
      tick();

      expect(router.navigate).toHaveBeenCalledWith(['home', 'private-markets']);
    }));

    it('ngOnInit - do NOT redirect to home page if "private markets" tab is NOT available and user is' +
      'currently NOT on PRivate markets Tab', fakeAsync(() => {
      moduleRibbonComponent.user = { status: true };
      sessionService.whenUserSession = jasmine.createSpy('sessionService.whenSession').and.returnValue(of({}));

      location.isCurrentPathEqualTo.and.callFake(param => param === '/home/next-races');
      ribbonService.isPrivateMarketsTab.and.returnValue(false);

      moduleRibbonComponent.ngOnInit();
      tick();

      expect(router.navigate).not.toHaveBeenCalledWith(['/']);
    }));

    it('ngOnInit - do NOT redirect to private-markets page if user is currently NOT on Home page', fakeAsync(() => {
      moduleRibbonComponent.user = { status: true };
      sessionService.whenUserSession = jasmine.createSpy('sessionService.whenSession').and.returnValue(of({}));
      moduleRibbonComponent.router.url = '/home/private-markets';

      moduleRibbonComponent.ngOnInit();
      tick();

      expect(router.navigate).not.toHaveBeenCalledWith(['home', 'private-markets']);
    }));

    it('ngOnInit - do NOT redirect to private-markets page if "private markets" tab is NOT available and user is' +
      'currently on Home page', fakeAsync(() => {
      moduleRibbonComponent.user = { status: true };
      sessionService.whenUserSession = jasmine.createSpy('sessionService.whenSession').and.returnValue(of({}));
      location.isCurrentPathEqualTo.and.callFake(param => param === '/' ? true : undefined);
      ribbonService.isPrivateMarketsTab.and.returnValue(false);

      moduleRibbonComponent.ngOnInit();
      tick();

      expect(router.navigate).not.toHaveBeenCalledWith(['home', 'private-markets']);
    }));
  });

  describe('setActiveTab', () => {
    it('set constants by negative condition', () => {
      moduleRibbonComponent.location.isCurrentPathEqualTo = jasmine.createSpy('isCurrentPathEqualTo').and.returnValue(true);
      moduleRibbonComponent.activeTab = { id: 'test' };
      moduleRibbonComponent['setActiveTab']();
      expect(moduleRibbonComponent.homeTabUrl).toEqual('/home/featured');
    });

    it('set constants by positive condition', () => {
      moduleRibbonComponent.location.isCurrentPathEqualTo = jasmine.createSpy('isCurrentPathEqualTo').and.returnValue(false);
      moduleRibbonComponent.location.path = jasmine.createSpy('path').and.returnValue('test');
      moduleRibbonComponent.router.url = 'test';
      moduleRibbonComponent.activeTab = null;
      moduleRibbonComponent['setActiveTab']();
      expect(moduleRibbonComponent.homeTabUrl).toEqual('test');
    });
  });

  describe('isCurrentPathEmpty', () => {
    it('should check that current path is empty', () => {
      moduleRibbonComponent.router.url = '/';
      const result = moduleRibbonComponent['isCurrentPathEmpty']();
      expect(result).toBeTruthy();
    });

    it('should check that current path is not empty', () => {
      moduleRibbonComponent.router.url = '/test';
      const result = moduleRibbonComponent['isCurrentPathEmpty']();
      expect(result).toBeFalsy();
    });

    it('should current path with query params in url', () => {
      moduleRibbonComponent.router.url = '/?id=2';
      const result = moduleRibbonComponent['isCurrentPathEmpty']();
      expect(result).toBeTruthy();
    });
  });

  describe('addPrivateMarketTab', () => {
    beforeEach(() => {
      moduleRibbonComponent['setLocation'] = jasmine.createSpy('setLocation');
    });

    it('should check for user session', (done: DoneFn) => {
      moduleRibbonComponent.user = {
        status: true
      };
      moduleRibbonComponent['addPrivateMarketTab'](true);

      setTimeout(() => {
        expect(moduleRibbonComponent['setLocation']).toHaveBeenCalled();
        done();
      });
    });

    it('should not check for user session', (done: DoneFn) => {
      moduleRibbonComponent.user = {
        status: false
      };
      moduleRibbonComponent['addPrivateMarketTab']();

      setTimeout(() => {
        expect(sessionService.whenUserSession).not.toHaveBeenCalled();
        expect(ribbonService.getPrivateMarketTab).not.toHaveBeenCalled();
        expect(moduleRibbonComponent['setLocation']).not.toHaveBeenCalled();
        done();
      });
    });
  });

  describe('setLocation', () => {
    it('should navigate to home/private-markets page', () => {
      location.isCurrentPathEqualTo.and.callFake((path) => path === '/');
      ribbonService.isPrivateMarketsTab.and.returnValue(true);

      moduleRibbonComponent['setLocation']();

      expect(router.navigate).toHaveBeenCalledWith(['home', 'private-markets']);
    });

    it('should navigate to home page when it is not private market', () => {
      location.isCurrentPathEqualTo.and.returnValue(true);
      ribbonService.isPrivateMarketsTab.and.returnValue(false);

      moduleRibbonComponent['setLocation']();

      expect(location.isCurrentPathEqualTo).toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith(['/']);
    });

    afterEach(() => {
      expect(ribbonService.isPrivateMarketsTab).toHaveBeenCalled();
    });
  });

  it('ngOnDestroy should unsubscribe', () => {
    moduleRibbonComponent.routeListener = jasmine.createSpyObj(['unsubscribe']);

    moduleRibbonComponent.ngOnDestroy();

    expect(pubSubService.unsubscribe).toHaveBeenCalledWith(title);
    expect(moduleRibbonComponent.routeListener.unsubscribe).toHaveBeenCalled();
  });

  describe('addPrivateMarketTab', () => {
    it('should set privateMarketTabCreated to true, call ribbonService.getPrivateMarketTab, fill moduleList and call setLocation',
    fakeAsync(() => {
      moduleRibbonComponent['privateMarketTabCreated'] = false;
      moduleRibbonComponent['isOnPrivateMarketTab'] = jasmine.createSpy('isOnPrivateMarketTab').and.returnValue(false);
      moduleRibbonComponent['setLocation'] = jasmine.createSpy('setLocation');
      ribbonService.getPrivateMarketTab.and.returnValue(of([1]));
      moduleRibbonComponent.user = {
        status: true
      };

      moduleRibbonComponent['addPrivateMarketTab']();
      tick();

      expect(moduleRibbonComponent['privateMarketTabCreated']).toBe(true);
      expect(moduleRibbonComponent['setLocation']).toHaveBeenCalled();
      expect(moduleRibbonComponent.moduleList).toEqual([1]);
    }));

    it('should set privateMarketTabCreated to false when getPrivateMarketTab promise fails', fakeAsync(() => {
      moduleRibbonComponent['privateMarketTabCreated'] = false;
      moduleRibbonComponent['isOnPrivateMarketTab'] = jasmine.createSpy('isOnPrivateMarketTab').and.returnValue(false);
      moduleRibbonComponent['setLocation'] = jasmine.createSpy('setLocation');
      ribbonService.getPrivateMarketTab.and.returnValue(throwError(''));
      moduleRibbonComponent.user = {
        status: true
      };

      moduleRibbonComponent['addPrivateMarketTab']();
      tick();

      expect(moduleRibbonComponent['privateMarketTabCreated']).toBe(false);
      expect(moduleRibbonComponent['setLocation']).not.toHaveBeenCalled();
      expect(moduleRibbonComponent.moduleList).toEqual(undefined);
    }));

    it('should NOT go into if statement body if user is not defined', () => {
      moduleRibbonComponent.user = null;
      moduleRibbonComponent['isOnPrivateMarketTab'] = jasmine.createSpy('isOnPrivateMarketTab').and.returnValue(false);
      moduleRibbonComponent['privateMarketTabCreated'] = false;

      moduleRibbonComponent['addPrivateMarketTab']();

      expect(ribbonService.getPrivateMarketTab).not.toHaveBeenCalled();
    });

    it('should NOT go into if statement body if isOnPrivateMarketTab returns true', () => {
      moduleRibbonComponent.user = {
        status: true
      };
      moduleRibbonComponent['isOnPrivateMarketTab'] = jasmine.createSpy('isOnPrivateMarketTab').and.returnValue(true);
      moduleRibbonComponent['privateMarketTabCreated'] = false;

      moduleRibbonComponent['addPrivateMarketTab']();

      expect(ribbonService.getPrivateMarketTab).not.toHaveBeenCalled();
    });

    it('should NOT go into if statement body if privateMarketTabCreated is set to true', () => {
      moduleRibbonComponent.user = {
        status: true
      };
      moduleRibbonComponent['isOnPrivateMarketTab'] = jasmine.createSpy('isOnPrivateMarketTab').and.returnValue(false);
      moduleRibbonComponent['privateMarketTabCreated'] = true;

      moduleRibbonComponent['addPrivateMarketTab']();

      expect(ribbonService.getPrivateMarketTab).not.toHaveBeenCalled();
    });
  });
});
