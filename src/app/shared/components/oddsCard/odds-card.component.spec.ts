import { OddsCardComponent } from './odds-card.component';

describe('OddsCardComponent', () => {
  let component: OddsCardComponent;

  let eventFactory;
  let marketTypeService;
  let timeService;
  let filters;
  let routingHelper;
  let templateService;
  let router;
  let sportsConfigHelperServive;

  beforeEach(() => {
    router = {
      navigateByUrl: jasmine.createSpy('routerNavigate')
    };
    eventFactory = {
      isLiveStreamAvailable: jasmine.createSpy('isLiveStreamAvailable')
    };
    marketTypeService = {
      getDisplayMarketConfig: jasmine.createSpy('getDisplayMarketConfig').and.returnValue({
        primaryMarket: 'primaryMarket',
        handicapMarket: 'handicapMarket',
        hasPrimaryMarket: true,
        hasHandicapMarket: true,
        displayMarket: {
          id: '1',
          label: 'Market label'
        },
        displayMarketName: 'displayMarketName'
      })
    };
    timeService = {
      getLocalHourMin: jasmine.createSpy('getLocalHourMin'),
      getEventTime: jasmine.createSpy('getEventTime')
    };
    filters = {};
    routingHelper = {
      formEdpUrl: jasmine.createSpy().and.returnValue('sport/event/1234567')
    };
    templateService = {
      getTemplate: jasmine.createSpy('getTemplate').and.returnValue({
        name: 'Enhanced Multiples'
      }),
      isMultiplesEvent: jasmine.createSpy('isMultiplesEvent')
    };
    sportsConfigHelperServive = {};

    component = new OddsCardComponent(
      eventFactory,
      marketTypeService,
      timeService,
      filters,
      routingHelper,
      templateService,
      router,
      sportsConfigHelperServive
    );

    component.event = { isFinished: false, markets: [] } as any;
  });

  it('constructor', () => {
    expect(component).toBeTruthy();
  });

  describe('#ngOnInit', () => {
    it('should call ngOnInit with sport config', () => {
      component.sportConfig = {
        config: {
          path: '/football',
          request: {
            categoryId: '16'
          }
        }
      } as any;
      component.ngOnInit();

      expect(component.sportType).toEqual('/football');
    });

    it('should call getDisplayMarketConfig', () => {
      const result = {
        primaryMarket: 'primaryMarket',
        handicapMarket: 'handicapMarket',
        hasPrimaryMarket: true,
        hasHandicapMarket: true,
        displayMarket: {
          id: '1',
          label: 'Market label'
        },
        displayMarketName: 'displayMarketName'
      };
      component.sportConfig = {
        config: {
          path: '/football',
          request: {
            categoryId: '21',
            marketTemplateMarketNameIntersects: 'template name'
          }
        }
      } as any;
      component.ngOnInit();

      expect(marketTypeService.getDisplayMarketConfig).toHaveBeenCalledWith('template name', []);
      expect(component.displayMarketConfig).toEqual(result as any);
      expect(component.isOutrightsCard).toEqual(false);
      expect(component.selectedMarket).toEqual('displayMarketName');
      expect(component.selectedMarketObject).toEqual({
        id: '1',
        label: 'Market label'
      });
    });
  });

  describe('#goToEvent', () => {
    it('#goToEvent with justReturn - true', () => {
      const callGoToEventCallbackFn = spyOn(component, 'callGoToEventCallback');

      expect(component['goToEvent'](true, 'element $event')).toEqual('sport/event/1234567');
      expect(component['routingHelper'].formEdpUrl).toHaveBeenCalledWith({ isFinished: false, markets: [] });
      expect(callGoToEventCallbackFn).toHaveBeenCalledTimes(0);
      expect(component['router'].navigateByUrl).not.toHaveBeenCalledWith('sport/event/1234567');
    });

    it('#goToEvent with justReturn - false and isEnhancedMultiples - true', () => {
      const callGoToEventCallbackFn = spyOn(component, 'callGoToEventCallback');

      component.isEnhancedMultiples = true;
      expect(component['goToEvent'](false, 'element $event')).toEqual('sport/event/1234567');
      expect(component['routingHelper'].formEdpUrl).toHaveBeenCalledWith({ isFinished: false, markets: [] });
      expect(callGoToEventCallbackFn).toHaveBeenCalledTimes(0);
      expect(component['router'].navigateByUrl).not.toHaveBeenCalledWith('sport/event/1234567');
    });

    it('#goToEvent with justReturn - false and isEnhancedMultiples - false and isFinished true', () => {
      const callGoToEventCallbackFn = spyOn(component, 'callGoToEventCallback');

      component.event.isFinished = true;
      component.isEnhancedMultiples = false;
      expect(component['goToEvent'](false, 'element $event')).toEqual('sport/event/1234567');
      expect(component['routingHelper'].formEdpUrl).toHaveBeenCalledWith({ isFinished: true, markets: [] });
      expect(callGoToEventCallbackFn).toHaveBeenCalledTimes(0);
      expect(component['router'].navigateByUrl).not.toHaveBeenCalledWith('sport/event/1234567');
    });

    it('#goToEvent with justReturn - false and isEnhancedMultiples - false and isFinished false', () => {
      const callGoToEventCallbackFn = spyOn(component, 'callGoToEventCallback');

      component.isEnhancedMultiples = false;
      expect(component['goToEvent'](false, 'element $event')).toEqual('sport/event/1234567');
      expect(component['routingHelper'].formEdpUrl).toHaveBeenCalledWith({ isFinished: false, markets: [] });
      expect(callGoToEventCallbackFn).toHaveBeenCalledTimes(1);
      expect(component['router'].navigateByUrl).toHaveBeenCalledWith('sport/event/1234567');
    });
  });
  describe('#changeCardView', () => {
    it('should isOutrightsCard = false', () => {
      component.sportConfig = {
        config: {
          path: '/football',
          request: {
            categoryId: '21',
            marketTemplateMarketNameIntersects: 'template name'
          }
        }
      } as any;
      component.changeCardView({
        isDisplayed: false
      } as any);

      expect(component.isOutrightsCard).toEqual(false);
    });
  });

  it('should update mmarket display for football', () => {
    component.marketUndisplayed.emit = jasmine.createSpy('component.marketUndisplayed.emit');
    component.sportConfig = {
      config: {
        path: '/football',
        request: {
          categoryId: '16',
          marketTemplateMarketNameIntersects: 'template name'
        }
      }
    } as any;
    component.changeCardView({
      isDisplayed: false
    } as any);

    expect(component.marketUndisplayed.emit).toHaveBeenCalledWith({
      isDisplayed: false
    } as any);
  });
});
