import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'underscore';

import { IMarket } from '@core/models/market.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { FiltersService } from '@core/services/filters/filters.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';

import { TimeService } from '@core/services/time/time.service';
import environment from '@environment/oxygenEnvConfig';
import { EventService } from '@sb/services/event/event.service';
import { mediaDrillDownNames } from '@shared/constants/media-drill-down-names';
import { ICategoriesData } from '@shared/models/categories-data.model';
import { MarketTypeService } from '@shared/services/marketType/market-type.service';
import { TemplateService } from '@shared/services/template/template.service';
import { SportsConfigHelperService } from '@sb/services/sportsConfig/sport-config-helper.service';
import { ISportConfig } from '@core/services/cms/models';
import { IEventMarketConfig } from '@root/app/core/models/event-market-config.model';
import { handicapTemplateMarketName } from '@root/app/shared/constants/odds-card-constant';

@Component({
  selector: 'odds-card-component',
  templateUrl: 'odds-card.component.html'
})

export class OddsCardComponent implements OnInit, OnChanges {

  @Input() event: ISportEvent;
  @Input() showLocalTime?: string;
  @Input() eventType?: string;
  @Input() selectedMarket?: string;
  @Input() widget?: string;
  @Input() featured?: { isSelection: boolean };
  @Input() limitSelections?: boolean | number;
  @Input() isFilterByTemplateMarketName?: boolean;
  @Input() gtmModuleTitle?: string;
  @Input() sportConfig?: ISportConfig;
  @Input() isMarketSwitcherConfigured?: boolean;

  @Output() readonly goToEventCallback: EventEmitter<number> = new EventEmitter<number>();
  @Output() readonly marketUndisplayed: EventEmitter<IMarket> = new EventEmitter<IMarket>();

  eventTime: string;
  eventFirstName: string;
  eventSecondName: string;
  racingData: ICategoriesData;

  isEnhancedMultiplesCard: boolean;
  isSpecialCard: boolean;
  isOutrightsCard: boolean;
  isStream: boolean;
  eventName: string;
  nameOverride: string;
  selectedMarketObject: any;
  startTime: any;
  isRacing: boolean;
  localTime: string;
  sportType: string;
  eventStartedOrLive: boolean;
  isEnhancedMultiples: boolean;
  template: any;

  header2Columns: boolean;
  displayMarketConfig: IEventMarketConfig;

  private matchResultMarket: IMarket;

  private readonly MEDIA_DRILL_DOWN_NAMES: Array<string> = mediaDrillDownNames;
  private readonly FOOTBALL_ID: string = environment.CATEGORIES_DATA.footballId;

  constructor(
    private eventFactory: EventService,
    private marketTypeService: MarketTypeService,
    private timeService: TimeService,
    private filters: FiltersService,
    private routingHelper: RoutingHelperService,
    private templateService: TemplateService,
    private router: Router,
    private sportsConfigHelperServive: SportsConfigHelperService
  ) {
    this.racingData = environment.CATEGORIES_DATA.racing;
  }

  callGoToEventCallback() {
    this.goToEventCallback.emit();
  }

  /**
   * Initializes all variables (private and public)
   * @private
   */
  ngOnInit(): void {
    this.matchResultMarket = this.event.markets.length && _.find(this.event.markets, { name: 'Match Result' });
    this.template = this.templateService.getTemplate(this.event);
    this.isEnhancedMultiples = this.templateService.isMultiplesEvent(this.event);
    this.localTime = this.timeService.getLocalHourMin(this.event.startTime);
    this.eventStartedOrLive = (this.event.isStarted || this.event.eventIsLive);

    this.isEnhancedMultiplesCard = (this.template.name === 'Enhanced Multiples') &&
      !this.event.hideEvent && this.isEnhancedMultiples;
    this.isSpecialCard = this.eventType === 'specials' && !this.isEnhancedMultiples;
    this.isStream = this.isStreamAvailable();
    this.eventName = this.event.nameOverride || this.event.name;
    this.nameOverride = this.event.nameOverride;
    this.selectedMarketObject = this.getSelectedMarket();
    this.isOutrightsCard = this.template.name === 'Outrights' && !this.eventType;
    this.startTime = new Date(this.event.startTime);

    if (this.sportConfig) {
      this.sportType = this.sportConfig.config.path;
      this.updateDisplayMarket(this.sportConfig);
    } else {
      this.sportsConfigHelperServive.getSportPathByCategoryId(Number(this.event.categoryId))
        .subscribe((sportPath: string) => {
          this.sportType = sportPath;
        });
    }

    this.isRacing = !!(_.find(this.racingData, (sport: any) => Number(sport.id) === Number(this.event.categoryId)));

    // Stream event
    _.extend(this.event, this.eventFactory.isLiveStreamAvailable(this.event));

    // Event start time
    this.eventTime = this.timeService.getEventTime(`${this.startTime}`);

    if (this.eventName) {
      // Event Teams Name
      this.eventFirstName = this.filters.getTeamName(this.eventName, 0);
      this.eventSecondName = this.filters.getTeamName(this.eventName, 1);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.event || changes.selectedMarket) {
      this.watchHandler();
    }
  }

  /**
   * Checks if odds card is sport one
   * @param market
   * @returns {boolean}
   */
  isSportCard(market: any): boolean {
    return ((!!this.featured || this.isSelectedMarket(market))
      && !this.isEnhancedMultiplesCard
      && (this.template.name !== 'Outrights')
      && !this.isSpecialCard);
  }

  /**
   * Check if it Selected Market
   * @param {IMarket} market
   * @returns {boolean}
   */
  isSelectedMarket(market: IMarket): boolean {
    if (this.selectedMarket) {
      return this.selectedMarket.toLowerCase() === market.templateMarketName.toLowerCase()
        || this.selectedMarket.toLowerCase() === market.name.toLowerCase();
    }

    return true;
  }

  /**
   * Checks if event is live
   * @returns {boolean}
   */
  get isLive(): boolean {
    return this.eventStartedOrLive;
  }

  /**
   * Redirects to event details page
   * @param justReturn
   * @param event
   * @returns {*}
   */
  goToEvent(justReturn: boolean, event: any): string | boolean {
    const edpUrl: string = this.routingHelper.formEdpUrl(this.event);

    if (!justReturn && !this.isEnhancedMultiples && !this.event.isFinished) {
      this.callGoToEventCallback();
      this.router.navigateByUrl(edpUrl);
    }

    return edpUrl;
  }

  /**
   * CHecks if stream is available
   * @returns {boolean}
   * @private
   */
  isStreamAvailable(): boolean {
    return this.event.liveStreamAvailable && this.showStreamIcon();
  }

  changeCardView(changedMarket: IMarket): void {
    if (!changedMarket.isDisplayed) {
      if (this.sportConfig.config.request.categoryId !== this.FOOTBALL_ID) {
        this.event.markets = this.event.markets.filter(market => market.id !== changedMarket.id);
      }
      this.updateDisplayMarket(this.sportConfig);
      this.marketUndisplayed.emit(changedMarket);
    }
  }

  private updateDisplayMarket(sportConfig: ISportConfig): void {
    if ((!environment.CATEGORIES_DATA.categoryIds.includes(this.sportConfig.config.request.categoryId) || !this.isMarketSwitcherConfigured)
      && (sportConfig.config.request.categoryId !== this.FOOTBALL_ID)) {
      this.displayMarketConfig = this.marketTypeService.getDisplayMarketConfig(
        sportConfig.config.request.marketTemplateMarketNameIntersects,
        this.event.markets);
      this.isOutrightsCard = this.isOutrightsCard || this.displayMarketConfig.displayMarketName === handicapTemplateMarketName;
      this.selectedMarket = this.displayMarketConfig.displayMarketName;
      this.selectedMarketObject = this.displayMarketConfig.displayMarket;
    }
  }

  /**
   * Watcher for necessary variables
   */
  private watchHandler(): void {
    const market = this.getSelectedMarket();
    this.selectedMarketObject = market;
    this.header2Columns = market && !this.marketTypeService.isMatchResultType(market) &&
      !this.marketTypeService.isHomeDrawAwayType(market);
    this.isStream = this.isStreamAvailable();
  }

  /**
   * Gets selected market object
   * @returns {*}
   * @private
   */
  private getSelectedMarket(): IMarket {
    if (this.matchResultMarket && !this.selectedMarket) {
      this.selectedMarket = 'Match Result';
    }
    return this.event.markets[this.marketIndex(this.event.markets)];
  }

  /**
   * Get Market Index
   * @param markets
   * @returns {number}
   */
  private marketIndex(markets: IMarket[]): number {
    const index: number = _.findIndex(markets, market => this.isSelectedMarket(market));
    return (index === -1) ? 0 : index;
  }

  /**
   * Checks if in drilldownTagNames property of event are two or more providers
   * @returns {boolean}
   * @private
   */
  private showStreamIcon(): boolean {
    const eventDrilldownTagNames = this.event.drilldownTagNames ? this.event.drilldownTagNames.split(',') : [];
    return _.intersection(eventDrilldownTagNames, this.MEDIA_DRILL_DOWN_NAMES).length > 0;
  }
}
