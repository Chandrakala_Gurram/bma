import { of } from 'rxjs';

import { OddsCardEnhancedMultiplesComponent } from './odds-card-enhanced-multiples.component';

describe('OddsCardEnhancedMultiplesComponent', () => {
  let component: OddsCardEnhancedMultiplesComponent;
  let eventService;
  let marketTypeService;
  let templateService;
  let timeService;
  let filtersService;
  let routingHelperService;
  let router;
  let sportsConfigHelperService;

  beforeEach(() => {
    eventService = {
      isLiveStreamAvailable: jasmine.createSpy()
    };
    marketTypeService = {};
    templateService = {
      getTemplate: jasmine.createSpy(),
      isMultiplesEvent: jasmine.createSpy()
    };
    timeService = {
      getLocalHourMin: jasmine.createSpy(),
      getEventTime: jasmine.createSpy()
    };
    filtersService = {
      getTeamName: jasmine.createSpy()
    };
    routingHelperService = {};
    router = {};
    sportsConfigHelperService = {
      getSportPathByCategoryId: jasmine.createSpy('getSportPathByCategoryId').and.returnValue(of(''))
    };

    component = new OddsCardEnhancedMultiplesComponent(
      eventService,
      marketTypeService,
      templateService,
      timeService,
      filtersService,
      routingHelperService,
      router,
      sportsConfigHelperService
    );
  });

  it('ngOnInit', () => {
    templateService.getTemplate.and.returnValue({});
    component.event = { markets: [] } as any;
    component.racingData = {};
    component['isStreamAvailable'] = (() => {}) as any;
    component['getSelectedMarket'] = (() => {}) as any;

    component.ngOnInit();
    component.nameOverride = null;
    component.featured = component.eventName = null;
    expect(component.nameOverride).toBeFalsy();

    component.ngOnInit();
    component.nameOverride = 'test';
    expect(component.nameOverride).toBe('test');
  });

  it('trackById', () => {
    expect(
      component.trackById({ id: '1' } as any)
    ).toBe('1');
  });

  it('nameOfEvent', () => {
    component.nameOverride = 'abc';
    expect( component.nameOfEvent({} as any) ).toBe('abc');

    component. nameOverride = '';
    expect( component. nameOfEvent({ name: 'xyz' } as any) ).toBe('xyz');
  });

  it('filterOutcomes', () => {
    component.limitSelections = null;
    expect(
      component.filterOutcomes([{}, {}, {}] as any)
    ).toEqual([{}, {}, {}] as any);

    component.limitSelections = 1;
    expect(
      component.filterOutcomes([{}, {}, {}] as any)
    ).toEqual([{}] as any);
  });
});
