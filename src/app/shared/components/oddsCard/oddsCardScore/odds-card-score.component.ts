import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ITypedScoreData } from '@core/services/scoreParser/models/score-data.model';

@Component({
  selector: 'odds-card-score',
  styleUrls: ['./odds-card-score.component.less'],
  templateUrl: './odds-card-score.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OddsCardScoreComponent {
  @Input() score: string[][];
  @Input() showServingTeam?: boolean;
  @Input() servingTeams?: number[];
  @Input() isHeaderShown?: boolean;
  @Input() boxScore?: ITypedScoreData;

  trackByIndex(index: number): number {
    return index;
  }
}
