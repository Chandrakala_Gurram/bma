import { OddsCardSurfaceBetComponent } from './odds-card-surface-bet.component';
import { FracToDecService } from '@core/services/fracToDec/frac-to-dec.service';

describe('OddsCardSurfaceBetComponent', () => {
  let component: OddsCardSurfaceBetComponent,
    fracToDecService: Partial<FracToDecService>;

  beforeEach(() => {
    fracToDecService = {
      getFormattedValue: jasmine.createSpy('getFormattedValue').and.returnValue('3/7')
    };
    component = new OddsCardSurfaceBetComponent(fracToDecService as FracToDecService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#ngOnInit should not set old price', () => {
    component.surfaceBet = { oldPrice: null } as any;
    component.ngOnInit();
    expect(component.oldPrice).toEqual(undefined);

    component.surfaceBet = { oldPrice: {} } as any;
    component.ngOnInit();
    expect(component.oldPrice).toEqual(undefined);

    component.surfaceBet = { oldPrice: { priceNum: '3' } } as any;
    component.ngOnInit();
    expect(component.oldPrice).toEqual(undefined);

    component.surfaceBet = { oldPrice: { priceDen: '7' } } as any;
    component.ngOnInit();
    expect(component.oldPrice).toEqual(undefined);

    expect(fracToDecService.getFormattedValue).not.toHaveBeenCalled();
  });

  it('#ngOnInit should set old price', () => {
    component.surfaceBet = {
      oldPrice: {
        priceNum: '3',
        priceDen: '7'
      }
    } as any;

    component.ngOnInit();
    expect(fracToDecService.getFormattedValue).toHaveBeenCalledWith('3', '7');
    expect(component.oldPrice).toEqual('3/7');
  });

  it('#ngOnInit should set old price on Changes', () => {
    fracToDecService.getFormattedValue =  jasmine.createSpy('getFormattedValue').and.callFake((a, b) => (`${a}/${b}`));

    component.surfaceBet = {
      oldPrice: {
        priceNum: '3',
        priceDen: '7'
      }
    } as any;

    component.ngOnInit();
    expect(component.oldPrice).toEqual('3/7');

    component.surfaceBet = {
      oldPrice: {
        priceNum: '10',
        priceDen: '10'
      }
    } as any;

    component.ngOnChanges();

    expect(component.oldPrice).toEqual('10/10');
  });

  describe('#ngOnChanges', () => {
    it('component isButton should be true', () => {
      component.surfaceBet = { markets: [ { outcomes: [ {} ] }] } as any;
      component.ngOnChanges();
      expect(component.isButton).toBeTruthy();
    });

    it('component isButton should be true', () => {
      component.surfaceBet = { markets: [] } as any;
      component.ngOnChanges();
      expect(component.isButton).toBeFalsy();
    });
  });
});
