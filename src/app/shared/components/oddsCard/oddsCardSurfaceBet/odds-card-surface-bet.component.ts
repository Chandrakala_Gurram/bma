import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { FracToDecService } from '@core/services/fracToDec/frac-to-dec.service';
import { ISurfaceBetEvent } from '@shared/models/surface-bet-event.model';

@Component({
  selector: 'odds-card-surface-bet',
  templateUrl: './odds-card-surface-bet.component.html',
  styleUrls: ['./odds-card-surface-bet.component.less']
})
export class OddsCardSurfaceBetComponent implements OnInit, OnChanges {
  @Input() surfaceBet: ISurfaceBetEvent;

  public oldPrice: string;
  public isButton: boolean;

  constructor(protected fracToDecService: FracToDecService) {
  }

  ngOnInit(): void {
    this.setOldPrice();
  }

  ngOnChanges(): void {
    this.isButton = Boolean(this.surfaceBet.markets && this.surfaceBet.markets[0] &&
      this.surfaceBet.markets[0].outcomes && this.surfaceBet.markets[0].outcomes[0]);

    this.setOldPrice();
  }

  private setOldPrice(): void {
    if (this.surfaceBet.oldPrice && this.surfaceBet.oldPrice.priceNum && this.surfaceBet.oldPrice.priceDen) {
      this.oldPrice = <string>this.fracToDecService.getFormattedValue(
        this.surfaceBet.oldPrice.priceNum,
        this.surfaceBet.oldPrice.priceDen
      );
    }
  }
}
