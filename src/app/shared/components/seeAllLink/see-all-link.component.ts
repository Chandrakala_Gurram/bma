import { Input, Component, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'see-all-link',
  template: '<a [i18n]="\'sb.seeAll\'" class="see-all-link" [routerLink]="link"></a>',
  styleUrls: ['./see-all-link.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class SeeAllLinkComponent {
  @Input() link?: string;
}
