import * as _ from 'underscore';

import { Component, OnInit, OnDestroy, ElementRef, Input } from '@angular/core';
import { Router } from '@angular/router';

import { RacingGaService } from '@app/racing/services/racing-ga.service';
import { TemplateService } from '@shared/services/template/template.service';
import { CarouselService } from '../../directives/ng-carousel/carousel.service';
import { Carousel } from '../../directives/ng-carousel/carousel.class';
import { IMarket } from '@core/models/market.model';
import { RendererService } from '@shared/services/renderer/renderer.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { SbFiltersService } from '@sb/services/sbFilters/sb-filters.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { ISilkStyleModel } from '@core/services/raceOutcomeDetails/silk-style.model';
import { IOutcome } from '@core/models/outcome.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { RaceOutcomeDetailsService } from '@core/services/raceOutcomeDetails/race-outcome-details.service';
import { DomToolsService } from '@coreModule/services/domTools/dom.tools.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { CommandService } from '@core/services/communication/command/command.service';
import { VirtualSharedService } from '@shared/services/virtual/virtual-shared.service';
import { EventService } from '@root/app/sb/services/event/event.service';
import { DatePipe } from '@angular/common';
import { NextRacesHomeService } from '@lazy-modules/lazyNextRacesTab/components/nextRacesHome/next-races-home.service';

@Component({
  selector: 'race-card',
  templateUrl: './race-card.component.html',
  styleUrls: [
    './next-races.less',
    './next-races-carousel.less',
    './race-card.component.less'
  ],
  providers: [CarouselService]
})
export class RaceCardComponent implements OnInit, OnDestroy {
  @Input() raceData: ISportEvent[];
  @Input() viewFullRaceText: string;
  @Input() trackGaDesktop: boolean;
  @Input() trackGa: boolean;
  @Input() raceWidget: string;
  @Input() raceIndex: number | string;
  @Input() raceOrigin: string;
  @Input() raceMaxSelections: number;
  @Input() showTimer: boolean;
  @Input() showBriefHeader: boolean;
  @Input() fluid: boolean;
  @Input() gtmModuleTitle?: string;
  @Input() hideNonRunners?: boolean;
  @Input() hostContext?: string;

  disableScroll: boolean;
  showCarouselButtons: boolean;
  eventCategory: string;
  raceCarousel: string;
  isFitSize: boolean;
  terms: Array<string>;
  marketOutcomesMap: {[key: string]: IOutcome[]} = {};
  isVirtual: boolean = false;

  private resizeListener: Function;
  private raceMarkets: string[] = [];
  private isHR: boolean;

  constructor(
    private elementRef: ElementRef,
    private domTools: DomToolsService,
    public raceOutcomeDetails: RaceOutcomeDetailsService,
    private routingHelperService: RoutingHelperService,
    private windowRef: WindowRefService,
    private commandService: CommandService,
    private locale: LocaleService,
    private sbFiltersService: SbFiltersService,
    private filtersService: FiltersService,
    private renderer: RendererService,
    private carouselService: CarouselService,
    private pubSubService: PubSubService,
    private router: Router,
    private templateService: TemplateService,
    private virtualSharedService: VirtualSharedService,
    private datePipe: DatePipe,
    private nextRacesHomeService: NextRacesHomeService,
    private eventService: EventService
  ) {
    this.trackEvent = this.trackEvent.bind(this);
  }

  ngOnInit(): void {
    const raceWidget = this.raceWidget ? 'widget' : 'container';
    this.raceIndex = this.raceIndex || 0;
    this.raceOrigin = this.raceOrigin ? `?origin=${this.raceOrigin}` : '';
    this.isHR = this.raceData[0].categoryCode === 'HORSE_RACING';

    this.viewFullRaceText = this.locale.getString(this.viewFullRaceText || 'sb.viewFullRace');
    this.eventCategory = this.windowRef.nativeWindow.location.pathname === '/' ? 'home' : 'widget';

    this.raceCarousel = `race-carousel-${raceWidget}-${this.raceIndex}`;

    if (!this.isHR) {
      this.setTrapNumbers();
    }
    this.processOutcomes();

    // Generate Each Way terms for events
    this.generateEachWayTerms();

    // re-sort outcomes on price change event
    this.pubSubService.subscribe('RaceCardComponent', this.pubSubService.API.OUTCOME_UPDATED, (market: IMarket) => {
      if (this.raceMarkets.includes(market.id)) {
        this.processOutcomes(market);
      }
    });

    this.windowRef.nativeWindow.setTimeout(() => {
      this.initShowCarouselButtons();
    }, 0);

    this.resizeListener = this.renderer.renderer.listen(this.windowRef.nativeWindow, 'resize', () => {
      this.initShowCarouselButtons();
    });

    this.isVirtual = this.raceData && this.raceData.length && this.virtualSharedService.isVirtual(this.raceData[0].categoryId);
  }

  isStreamLabelShown(event: ISportEvent): boolean {
    const liveStreamAvailable: boolean = this.eventService.isLiveStreamAvailable(event).liveStreamAvailable;
    return liveStreamAvailable;
  }

  generateEachWayTerms(): void {
    this.terms = [];
    _.each(this.raceData, (event: ISportEvent) => {
      if (event.markets && event.markets[0] && this.showEchWayTerms(event.markets[0])) {
        this.terms.push(this.generateTerms(event.markets[0]));
      }
    });
  }

  ngOnDestroy(): void {
    this.resizeListener();
    this.pubSubService.unsubscribe('RaceCardComponent');
  }

  trackByEvents(i: number, event: ISportEvent): string {
    return `${event.id}_${event.name}_${event.categoryId}_${event.markets && event.markets.length}`;
  }

  trackByMarkets(i: number, market: IMarket): string {
    return `${market.id}_${market.name}_${market.marketStatusCode}`;
  }

  trackByOutcomes(i: number, outcome: IOutcome): string {
    return `${outcome.id}_${outcome.name}_${outcome.runnerNumber}`;
  }

  removeLineSymbol(name: string): string {
    return this.filtersService.removeLineSymbol(name);
  }

  getEventName(event: ISportEvent): string {
    if (this.isVirtual) {
      const correctTime = this.datePipe.transform(event.startTime, 'HH:mm');
      return event.name.replace(/\d\d\:\d\d/, correctTime);
    }
    const name = event.localTime ? `${event.localTime} ${event.typeName}` : event.name;
    return event.nameOverride || name;
  }

  formEdpUrl(eventEntity: ISportEvent): string {
    return `${this.routingHelperService.formEdpUrl(eventEntity)}${this.raceOrigin}`;
  }

  trackEvent(entity: ISportEvent): void {
    const isVirtual = this.virtualSharedService.isVirtual(entity.categoryId);
    if (this.trackGaDesktop) {
      const name = entity.localTime ? `${entity.localTime} ${entity.typeName}` : entity.name;
      const eventName = entity.nameOverride || name;

      this.commandService.executeAsync(this.commandService.API.RACING_GA_SERVICE).then((racingGaService: RacingGaService) => {
        racingGaService.sendGTM(`full race card - ${eventName}`, this.eventCategory);
      });
    }

    if (this.trackGa && !this.trackGaDesktop) {
      this.commandService.executeAsync(this.commandService.API.RACING_GA_SERVICE).then((racingGaService: RacingGaService) => {
        racingGaService.trackNextRace(entity.categoryName);
      });
    }

    const link = isVirtual ? this.virtualSharedService.formVirtualEventUrl(entity) : this.formEdpUrl(entity);
    this.router.navigateByUrl(link);
  }

  showEchWayTerms(market: IMarket): boolean {
    return !!(market && market.eachWayPlaces && market.eachWayFactorDen && market.eachWayFactorNum);
  }

  /**
   * Generate string with Each way terms
   *
   * @param {object} market
   */
  generateTerms(market: IMarket): string {
    return this.templateService.genTerms(market, 'sb.newOddsAPlacesExtended');
  }

  isGenericSilk(eventEntity: ISportEvent, outcomeEntity: IOutcome): boolean {
    return this.raceOutcomeDetails.isGenericSilk(eventEntity, outcomeEntity);
  }

  isGreyhoundSilk(eventEntity: ISportEvent, outcomeEntity: IOutcome): boolean {
    return this.raceOutcomeDetails.isGreyhoundSilk(eventEntity, outcomeEntity);
  }

  isNumberNeeded(eventEntity: ISportEvent, outcomeEntity: IOutcome): boolean {
    return this.raceOutcomeDetails.isNumberNeeded(eventEntity, outcomeEntity);
  }

  getRunnerNumber(outcomeEntity: IOutcome): string {
    return outcomeEntity.runnerNumber || outcomeEntity.racingFormOutcome && outcomeEntity.racingFormOutcome.draw;
  }

  getSilkStyle(raceData: ISportEvent[], outcomeEntity: IOutcome): ISilkStyleModel {
    return this.raceOutcomeDetails.getSilkStyle(raceData, outcomeEntity);
  }

  nextSlide(): void {
    this.carousel.next();
    if (this.trackGaDesktop) {
      this.commandService.executeAsync(this.commandService.API.RACING_GA_SERVICE).then((racingGaService: RacingGaService) => {
        racingGaService.sendGTM('navigate right', this.eventCategory);
      });
    }
  }

  prevSlide(): void {
    this.carousel.previous();
    if (this.trackGaDesktop) {
      this.commandService.executeAsync(this.commandService.API.RACING_GA_SERVICE).then((racingGaService: RacingGaService) => {
        racingGaService.sendGTM('navigate left', this.eventCategory);
      });
    }
  }

  getGoing(going: string): string {
    return this.nextRacesHomeService.getGoing(going);
  }

  getDistance(distance: string): string {
    return this.nextRacesHomeService.getDistance(distance);
  }

  get showNext(): boolean {
    return this.carousel && this.carousel.currentSlide < this.carousel.slidesCount - 1;
  }

  get showPrev(): boolean {
    return this.carousel && this.carousel.currentSlide > 0;
  }

  /**
   * Sort and filter outcomes of market(s)
   * providing updatedMarket parameter will process only appropriate market, otherwise - all existing
   *
   * @param updatedMarket
   */
  private processOutcomes(updatedMarket?: IMarket): void {
    this.raceData.forEach((event: ISportEvent) => {
      event.markets.forEach((market: IMarket) => {
        if (updatedMarket) {
          if (updatedMarket.id === market.id) {

            _.each(market.outcomes, (outcome: IOutcome) => {
              const updatedOutcomeObj = _.find(updatedMarket.outcomes, (updatedOutcome: IOutcome) => updatedOutcome.id === outcome.id);
              outcome.prices = updatedOutcomeObj && updatedOutcomeObj.prices || outcome.prices;
            });

            this.marketOutcomesMap[market.id] = this.applyFilters(market);
            return true;
          }
        } else {
          this.raceMarkets.push(market.id);
          this.marketOutcomesMap[market.id] = this.applyFilters(market);
        }
      });
    });
  }

  private applyFilters(market: IMarket): IOutcome[] {
    const orderedOutcomes = this.sbFiltersService.orderOutcomeEntities(
      market.outcomes,
      market.isLpAvailable,
      true,
      true,
      this.hideNonRunners,
      false,
      !this.isHR
    );
    return this.raceMaxSelections ? orderedOutcomes.slice(0, this.raceMaxSelections) : orderedOutcomes;
  }

  private initShowCarouselButtons(): void {
    const carouselSlides = this.raceData.length;
    const carouselOuterWidth = this.domTools.getWidth(this.elementRef.nativeElement.querySelector('.race-card-carousel'));
    const slideWidth = this.domTools.getWidth(this.elementRef.nativeElement.querySelector('.slide'));
    const allSlidesWidth = carouselSlides * slideWidth;
    const minSlidesOffset = 20; // Arrows appear when there is minimum 2 slides on carousel. One slide has 10px of margin.
    const isFitSize = carouselOuterWidth >= allSlidesWidth + minSlidesOffset;

    this.disableScroll = this.isFitSize = isFitSize;
    this.showCarouselButtons = !(carouselOuterWidth >= allSlidesWidth);
  }

  private get carousel(): Carousel {
    return this.carouselService.get(this.raceCarousel);
  }

  private setTrapNumbers(): void {
    this.raceData.forEach((event: ISportEvent) => {
      event.markets.forEach((market: IMarket) => {
        market.outcomes.forEach((outcome: IOutcome) => {
          if (outcome.runnerNumber) {
            const index = outcome.name.search(/(\(RES\))/);
            outcome.trapNumber = index !== -1 ? outcome.displayOrder : Number(outcome.runnerNumber);
          }
        });
      });
    });
  }
}
