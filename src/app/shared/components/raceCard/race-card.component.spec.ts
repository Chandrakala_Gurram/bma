import { RaceCardComponent } from './race-card.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('#RaceCardComponent', () => {
  let component: RaceCardComponent;
  let raceOutcomeDetails;
  let routingHelperService;
  let localeService;
  let domTools;
  let windowRef;
  let elementRef;
  let renderer;
  let commandService;
  let carouselService;
  let sbFiltersService;
  let filtersService;
  let pubSubService;
  let router;
  let templateService;
  let outcomeUpdateCb;
  let virtualSharedService;
  let datePipe;
  let nextRacesHomeService;
  let eventService;

  const mockString = 'More';

  beforeEach(() => {
    localeService = {
      getString: jasmine.createSpy('getString').and.returnValue(mockString)
    };
    pubSubService = {
      subscribe: jasmine.createSpy('subscribe').and.callFake((file, method, cb) => {
        if (method === 'OUTCOME_UPDATED') {
          outcomeUpdateCb = cb;
        }
      }),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: pubSubApi
    };
    windowRef = {
      nativeWindow: {
        setTimeout: jasmine.createSpy('setTimeout'),
        location: {
          pathname: 'pathname'
        }
      }
    };
    elementRef = {
      nativeElement: {
        querySelector: jasmine.createSpy('querySelector')
      }
    };
    renderer = {
      renderer: {
        listen: jasmine.createSpy('listen').and.returnValue(() => {})
      }
    };
    commandService = {
      executeAsync: jasmine.createSpy('executeAsync')
    };
    templateService = {
      genTerms: jasmine.createSpy().and.returnValue('test')
    };
    carouselService = {
      get: jasmine.createSpy('get')
    };
    domTools = {
      getWidth: jasmine.createSpy('getWidth')
    };
    filtersService = {
      removeLineSymbol: jasmine.createSpy('removeLineSymbol')
    };
    routingHelperService = {
      formEdpUrl: jasmine.createSpy('formEdpUrl')
    };
    raceOutcomeDetails = {
      isGenericSilk: jasmine.createSpy('isGenericSilk'),
      isGreyhoundSilk: jasmine.createSpy('isGreyhoundSilk'),
      isNumberNeeded: jasmine.createSpy('isNumberNeeded'),
      getSilkStyle: jasmine.createSpy('getSilkStyle')
    };
    sbFiltersService = {
      orderOutcomeEntities: jasmine.createSpy('orderOutcomeEntities').and.returnValue([{
        id: 'outcome1'
      },
        {
          id: 'outcome2'
        }])
    };
    router = {
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    };

    virtualSharedService = {
      isVirtual: jasmine.createSpy('isVirtual'),
      formVirtualEventUrl: jasmine.createSpy('formVirtualEventUrl')
    };

    datePipe = {
      transform: () => '20:15'
    } as any;

    nextRacesHomeService = {
      getGoing: jasmine.createSpy('getGoing'),
      getDistance: jasmine.createSpy('getDistance')
    };
    eventService = {
      isLiveStreamAvailable: jasmine.createSpy('isLiveStreamAvailable').and.returnValue({
        liveStreamAvailable: true
      })
    };

    component = new RaceCardComponent(elementRef, domTools, raceOutcomeDetails, routingHelperService, windowRef, commandService,
      localeService, sbFiltersService, filtersService, renderer, carouselService, pubSubService, router, templateService,
      virtualSharedService, datePipe, nextRacesHomeService, eventService);

    component['resizeListener'] = () => {};
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  it('#isStreamLabelShown', () => {
    const result = component.isStreamLabelShown(({ name: 'event' } as any));

    expect(eventService.isLiveStreamAvailable).toHaveBeenCalledWith({ name: 'event' });
    expect(result).toEqual(true);
  });

  it('#ngOnInit', () => {
    sbFiltersService.orderOutcomeEntities = jasmine.createSpy('sbFiltersService').and.callFake(x => x);
    component.raceMaxSelections = 3;
    component.raceData = [ {
      id: 1,
      markets: [
        {
          id: 1,
          outcomes: [
            {
              id: 1
            },
            {
              id: 2
            },
            {
              id: 3
            },
            {
              id: 4
            },
          ]
        }
      ]
    }, { id : 2, markets: [] } ] as any;

    windowRef.nativeWindow.setTimeout.and.callFake(cb => cb && cb());
    component.ngOnInit();

    expect(component.raceIndex).toEqual(0);
    expect(component.raceOrigin).toEqual('');
    expect(component.viewFullRaceText).toEqual('More');
    expect(component.eventCategory).toEqual('widget');
    expect(localeService.getString).toHaveBeenCalledWith('sb.viewFullRace');
    expect(pubSubService.subscribe).toHaveBeenCalledWith('RaceCardComponent', 'OUTCOME_UPDATED', jasmine.any(Function));
    expect(windowRef.nativeWindow.setTimeout).toHaveBeenCalled();
    expect(domTools.getWidth).toHaveBeenCalledTimes(2);
    expect(component.disableScroll).toBeDefined();
    expect(component.isFitSize).toBeDefined();
    expect(component.showCarouselButtons).toBeDefined();
    expect(component.marketOutcomesMap[1]).toEqual([
      {
        id: 1
      },
      {
        id: 2
      },
      {
        id: 3
      }
    ] as any);
    expect(component.raceData[0].markets[0].outcomes.length).toEqual(4);
  });

  it('should processOutcomes on OUTCOME_UPDATED', () => {
    const martet = { id: '12' };
    component['processOutcomes'] = jasmine.createSpy();
    component.raceData = [ {id: 1, markets: [{outcomes: []} ]}, { id : 2, markets: [{outcomes: []} ] } ] as any;

    component.ngOnInit();
    component['raceMarkets'] = ['12', '14'];
    outcomeUpdateCb(martet);

    expect(component['processOutcomes']).toHaveBeenCalledTimes(2);
    expect(component['processOutcomes']).toHaveBeenCalledWith(martet);
  });

  it('should processOutcomes on OUTCOME_UPDATED and set new prices', () => {
    const market = { id: '8', outcomes: [{ id: '901', prices: [{ id: '72' }] }] };
    component.raceData = [
      {
        id: '8',
        markets: [{ id: '8', outcomes: [{ id: '901', prices: [{ id: '18' }] }] }]
      },
      {
        id: '2', markets: [{ outcomes: [] }]
      }
    ] as any;
    component.ngOnInit();
    component['raceMarkets'] = ['12', '14', '8'];
    outcomeUpdateCb(market);

    expect(component.raceData).toEqual([
      {
        id: '8',
        markets: [{ id: '8', outcomes: [{ id: '901', prices: [{ id: '72' }] }] }]
      },
      {
        id: '2', markets: [{ outcomes: [] }]
      }
    ] as any);
  });

  it('should processOutcomes on OUTCOME_UPDATED and do not update prices if no match', () => {
    const market = { id: '8', outcomes: [{ id: '91', prices: [{ id: '72' }] }] };
    component.raceData = [
      {
        id: '8',
        markets: [{ id: '8', outcomes: [{ id: '901', prices: [{ id: '18' }] }] }]
      }
    ] as any;
    component.ngOnInit();
    component['raceMarkets'] = ['12', '14', '8'];
    outcomeUpdateCb(market);

    expect(component.raceData).toEqual([
      {
        id: '8',
        markets: [{ id: '8', outcomes: [{ id: '901', prices: [{ id: '18' }] }] }]
      }
    ] as any);
  });

  describe('trackEvent', () => {
    it('should NOT track Virtual Event', () => {
      virtualSharedService.isVirtual.and.returnValue(false);
      component.trackEvent({} as any);

      expect(virtualSharedService.formVirtualEventUrl).not.toHaveBeenCalled();
    });

    it('should track Virtual Event', () => {
      virtualSharedService.isVirtual.and.returnValue(true);
      component.trackEvent({} as any);

      expect(virtualSharedService.formVirtualEventUrl).toHaveBeenCalledWith({});
    });
  });

  it('should generate each way terms', () => {
    const market = {
      eachWayFactorNum: '1',
      eachWayFactorDen: '2',
      eachWayPlaces: '1,2,3',
      outcomes: []
    } as any;
    component.raceData = [
      {
        markets: [market]
      }
    ] as any;

    spyOn(component, 'generateEachWayTerms').and.callThrough();
    component.ngOnInit();
    expect(component.generateEachWayTerms).toHaveBeenCalled();
    expect(component.terms[0]).toEqual('test');
  });

  it('should generate Each Way terms for events(no events)', () => {
    spyOn(component, 'generateTerms');

    component.raceData = <any>[
      {
        categoryCode: 'HORSE_RACING',
        markets: [
          { outcomes: []}
        ]
      }
    ];
    component.ngOnInit();

    expect(component.generateTerms).not.toHaveBeenCalled();
  });

  it('should generate Each Way terms for events(no markets)', () => {
    spyOn(component, 'generateTerms');

    component.raceData = <any>[
      {
        categoryCode: 'HORSE_RACING',
        markets: [
          { outcomes: []}
        ]
      }
    ];
    component.ngOnInit();

    expect(component.generateTerms).not.toHaveBeenCalled();
  });

  it('should trackByEvents', () => {
    const event1 = { name: 'test', categoryId: '2', id: 11, markets: [] } as any;
    expect(component.trackByEvents(1, event1)).toBe('11_test_2_0');

    const event2 = { name: 'test', categoryId: '2', id: 11 } as any;
    expect(component.trackByEvents(1, event2)).toBe('11_test_2_undefined');
  });

  it('should trackByMarkets', () => {
    expect(component.trackByMarkets(1, <any>{ name: 'test', marketStatusCode: '2', id: 11 })).toBe('11_test_2');
  });

  it('should trackByOutcomes', () => {
    expect(component.trackByOutcomes(1, <any>{ name: 'test', runnerNumber: '2', id: 11 })).toBe('11_test_2');
  });


  it('should test generation of template for terms', () => {
    const market = {
      eachWayFactorNum: '1',
      eachWayFactorDen: '2',
      eachWayPlaces: '1,2,3'
    };
    component.generateTerms(<any>market);
    expect(templateService.genTerms).toHaveBeenCalledWith(market, 'sb.newOddsAPlacesExtended');
  });

  describe('#applyFilters', () => {
    it('should filter outcomes of market', () => {
      component.hideNonRunners = false;
      const result = component['applyFilters'](({
        outcomes: [],
        isLpAvailable: 'Yes',
      } as any));
      expect(sbFiltersService.orderOutcomeEntities).toHaveBeenCalledWith([], 'Yes', true, true, false, false, true);
      expect(result).toEqual(([{
        id: 'outcome1'
      },
        {
          id: 'outcome2'
        }] as any));
    });

    it('should filter outcomes of marke and splice for max selections on card', () => {
      component.raceMaxSelections = 1;
      component.hideNonRunners = false;
      const result = component['applyFilters'](({
        outcomes: [],
        isLpAvailable: 'Yes',
      } as any));
      expect(sbFiltersService.orderOutcomeEntities).toHaveBeenCalledWith([], 'Yes', true, true, false, false, true);
      expect(result).toEqual(([{
        id: 'outcome1'
      }] as any));
    });
  });

  describe('#showNext', () => {
    it('should get showNext when carousel is defined', () => {
      carouselService.get.and.returnValue({
        currentSlide: 3,
        slidesCount: 5
      });

      expect(component.showNext).toEqual(true);
    });

    it('should get showNext when carousel not defined', () => {
      carouselService.get.and.returnValue(null);

      expect(component.showNext).toEqual(null);
    });
  });

  describe('#showPrev', () => {
    it('should get showPrev when carousel is defined', () => {
      carouselService.get.and.returnValue({
        currentSlide: 3
      });

      expect(component.showPrev).toEqual(true);
    });

    it('should get showPrev when carousel not defined', () => {
      carouselService.get.and.returnValue(null);

      expect(component.showPrev).toEqual(null);
    });
  });

  describe('@getEventName', () => {
    it('should return correct virtual name', () => {
      component.isVirtual = true;
      const event = { id: 1, categoryId: 39, name: '18:15 Laddies Leap\'s Lane' } as any;
      expect(component.getEventName(event)).toBe('20:15 Laddies Leap\'s Lane');
    });
    it('should return correct name if NOT Virtual Event', () => {
      component.isVirtual = false;
      const result = component.getEventName(({
        localTime: '2',
        typeName: 'London',
        name: 'London2',
        nameOverride: 'London1'
      }) as any);

      expect(result).toEqual('London1');
    });
  });

  it('#getRunnerNumber return runner number', () => {
    expect(component.getRunnerNumber({} as any)).toBe(undefined);
    expect(component.getRunnerNumber({runnerNumber: '3'} as any)).toBe('3');
    expect(component.getRunnerNumber({racingFormOutcome: {}} as any)).toBe(undefined);
    expect(component.getRunnerNumber({racingFormOutcome: { draw: '6' }} as any)).toBe('6');
  });

  it('#getGoing', () => {
    component.getGoing('G');

    expect(nextRacesHomeService.getGoing).toHaveBeenCalledWith('G');
  });

  it('#getDistance', () => {
    component.getDistance('Distance');

    expect(nextRacesHomeService.getDistance).toHaveBeenCalledWith('Distance');
  });
  it('setTrapNumbers', () => {
    component.raceData = [
      {
        id: 1,
        markets: [
          {
            outcomes: [
              {
                name: 'Test',
                runnerNumber: 2
              }
            ]
          }
        ]
      },
      {
        id : 2,
        markets: [
          {
            outcomes: [
              {
                name: 'Test (RES)',
                runnerNumber: 1,
                displayOrder: 1
              }
            ]
          }
        ]
      }
    ] as any;

    component['setTrapNumbers']();

    expect(component.raceData).toEqual(<any>[
      {
        id: 1,
        markets: [
          {
            outcomes: [
              {
                name: 'Test',
                runnerNumber: 2,
                trapNumber: 2
              }
            ]
          }
        ]
      },
      {
        id : 2,
        markets: [
          {
            outcomes: [
              {
                name: 'Test (RES)',
                displayOrder: 1,
                runnerNumber: 1,
                trapNumber: 1
              }
            ]
          }
        ]
      }
    ]);
  });

  it('setTrapNumbers', () => {
    component.raceData = [
      {
        id: 1,
        markets: [
          {
            outcomes: [
              {
                name: 'Test',
              }
            ]
          }
        ]
      },
      {
        id : 2,
        markets: [
          {
            outcomes: [
              {
                name: 'Test (RES)'
              }
            ]
          }
        ]
      }
    ] as any;

    component['setTrapNumbers']();

    expect(component.raceData).toEqual(<any>[
      {
        id: 1,
        markets: [
          {
            outcomes: [
              {
                name: 'Test',
              }
            ]
          }
        ]
      },
      {
        id : 2,
        markets: [
          {
            outcomes: [
              {
                name: 'Test (RES)'
              }
            ]
          }
        ]
      }
    ]);
  });
});
