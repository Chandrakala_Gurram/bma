import {
  InformationDialogComponent
} from '@sharedModule/components/informationDialog/information-dialog.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('InformationDialogComponent', () => {
  let component: InformationDialogComponent;
  let clickEventMock;
  let deviceServiceStub;
  let rendererService;
  let windowRefStub;
  let pubSubStub;
  let rountingUtilsServiceStub;

  beforeEach(() => {
    clickEventMock = {
      target: {
        attributes: {
          href: {
            value: 'testHref'
          }
        }
      },
      preventDefault: jasmine.createSpy()
    };

    deviceServiceStub = {
      isWrapper: false,
      isIos: false,
      visible: false,
      visibleAnimate: false
    };

    rendererService = {
      renderer: {
        listen: jasmine.createSpy().and.returnValue(() => {})
      }
    };

    windowRefStub = {
      setTimeout: jasmine.createSpy('setTimeout').and.callFake(cb => cb()),
      document: {
        querySelector: jasmine.createSpy().and.returnValue({
          querySelector: jasmine.createSpy()
        })
      }
    };
    pubSubStub = {
      subscribe: jasmine.createSpy(),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: pubSubApi
    };

    rountingUtilsServiceStub = {
      openUrl: jasmine.createSpy()
    };

    component = new InformationDialogComponent(deviceServiceStub, rendererService, windowRefStub,
      pubSubStub, rountingUtilsServiceStub);

    component.dialog = {
      changeDetectorRef: {
        detectChanges: jasmine.createSpy('detectChanges')
      }
    };
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  it('should test init function', () => {
    component.ngAfterViewInit();
    component['redirectListener'] = () => {};
    component['checkLink'](clickEventMock);
    expect(windowRefStub.document.querySelector).toHaveBeenCalledWith('.modal-body');
    expect(rountingUtilsServiceStub.openUrl).toHaveBeenCalledWith('testHref', true);
  });

  it('should test init function when new modal opened inside', () => {
    spyOn(component as any, 'init').and.callThrough();

    pubSubStub.subscribe.and.callFake((name, event, callback) => {
       callback();

       expect(component.dialog.changeDetectorRef.detectChanges).toHaveBeenCalled();
       expect(component['init']).toHaveBeenCalled();
    });

    component.ngAfterViewInit();

    expect(pubSubStub.subscribe).toHaveBeenCalledWith('InformationDialogComponent',
      pubSubStub.API.NEW_DIALOG_OPENED, jasmine.any(Function));
  });

  describe('@handleBtnClick', () => {
    beforeEach(() => {
      spyOn(component, 'closeDialog');
    });

    it('should call provided handler', () => {
      const btnStub = {handler: jasmine.createSpy('handler')};
      component.handleBtnClick(btnStub);

      expect(component.closeDialog).not.toHaveBeenCalled();
      expect(btnStub.handler).toHaveBeenCalled();
    });

    it('should close dialog if no handler provided', () => {
      component.handleBtnClick({});

      expect(component.closeDialog).toHaveBeenCalled();
    });
  });
});
