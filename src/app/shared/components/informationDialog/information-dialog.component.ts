import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import * as _ from 'underscore';

import { AbstractDialog } from '../oxygenDialogs/abstract-dialog';
import { DeviceService } from '@core/services/device/device.service';
import { IDialogButton } from '@core/services/dialogService/dialog-params.model';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { NavigationService } from '@core/services/navigation/navigation.service';
import { RendererService } from '@shared/services/renderer/renderer.service';

@Component({
  selector: 'information-dialog',
  templateUrl: './information-dialog.component.html',
})
export class InformationDialogComponent extends AbstractDialog implements AfterViewInit, OnDestroy {

  @ViewChild('informationDialog') dialog;
  params: any = {};
  redirectListener;

  constructor(deviceService: DeviceService,
              private rendererService: RendererService,
              protected windowRef: WindowRefService,
              private pubSubService: PubSubService,
              private navigationService: NavigationService) {
    super(deviceService, windowRef);
    this.checkLink = this.checkLink.bind(this);
  }

  ngAfterViewInit(): void {
    this.init();

    this.pubSubService.subscribe('InformationDialogComponent', this.pubSubService.API.NEW_DIALOG_OPENED, () => {
      this.dialog.changeDetectorRef.detectChanges();
      this.init();
    });
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe('InformationDialogComponent');
  }

  handleBtnClick(button) {
    if (_.isFunction(button.handler)) {
      button.handler();
    } else {
      this.closeDialog();
    }
  }

  trackByFn(index: number, item: IDialogButton): string {
    return `${index}_${item.caption}`;
  }

  private init(): void {
    const textBlock = this.windowRef.document.querySelector('.modal-body');
    const link = textBlock.querySelector('a');
    if (link) {
      this.redirectListener = this.rendererService.renderer.listen(link, 'click', this.checkLink);
    }
  }

  private checkLink(e): void {
    e.preventDefault();
    const href = e.target.attributes && e.target.attributes.href && e.target.attributes.href.value;
    if (href) {
      this.navigationService.openUrl(href, true);
    }
  }
}
