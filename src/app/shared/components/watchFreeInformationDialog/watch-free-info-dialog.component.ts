import { Component, ViewChild } from '@angular/core';
import { AbstractDialog } from '@shared/components/oxygenDialogs/abstract-dialog';
import { DeviceService } from '@core/services/device/device.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

@Component({
  selector: 'watch-free-info-dialog',
  templateUrl: 'watch-free-info-dialog.component.html'
})
export class WatchFreeInfoDialogComponent extends AbstractDialog {

  @ViewChild('watchFreeInfoDialog') dialog;

  constructor(
    device: DeviceService, windowRef: WindowRefService
  ) {
    super(device, windowRef);
  }
}
