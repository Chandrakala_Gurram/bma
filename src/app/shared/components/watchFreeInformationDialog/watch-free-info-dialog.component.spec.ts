import { WatchFreeInfoDialogComponent } from '@shared/components/watchFreeInformationDialog/watch-free-info-dialog.component';

describe('WatchFreeInfoDialogComponent', () => {
  let component: WatchFreeInfoDialogComponent;
  let device, windowRef;
  beforeEach(() => {
    device = {};
    windowRef = {};
    component = new WatchFreeInfoDialogComponent(device, windowRef);
  });
  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });
});
