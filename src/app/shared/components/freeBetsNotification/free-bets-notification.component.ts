import { Component, Input, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FreeBetsService } from '@coreModule/services/freeBets/free-bets.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { AbstractNotification } from '@shared/components/oxygenNotification/abstract-notification';
import { DeviceService } from '@core/services/device/device.service';

@Component({
  selector: 'free-bets-notification',
  templateUrl: 'free-bets-notification.component.html',
  styleUrls: ['free-bets-notification.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FreeBetsNotificationComponent extends AbstractNotification implements OnInit {
  @Input() isBetslip: boolean;
  isTablet: boolean;

  constructor(
    private freeBetsService: FreeBetsService,
    private pubsubService: PubSubService,
    private device: DeviceService
  ) {
    super();
    this.isTablet = this.device.isTablet;
  }

  ngOnInit(): void {
    this.message = this.isBetslip ? this.freeBetsService.getFreeBetAvailableMessage() : this.freeBetsService.getFreeBetExpiryMessage();
  }

  closeNotification(): void {
    this.close();

    if (!this.isBetslip) {
      this.freeBetsService.hideExpiringMessageForFreeBet();
      this.pubsubService.publish(this.pubsubService.API.NOTIFICATION_HIDE);
    }
  }
}
