import { async } from '@angular/core/testing';
import { FreeBetsNotificationComponent } from '@shared/components/freeBetsNotification/free-bets-notification.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('FreeBetsNotificationComponent', () => {
  let component: FreeBetsNotificationComponent;
  let freeBetsService;
  let pubSubService;
  let device;

  beforeEach(async(() => {
    freeBetsService = {
      getFreeBetExpiryMessage: jasmine.createSpy('getFreeBetExpiryMessage').and.returnValue('ExpiryMessage'),
      getFreeBetAvailableMessage: jasmine.createSpy('getFreeBetAvailableMessage').and.returnValue('You have a free bet available !'),
      hideExpiringMessageForFreeBet: jasmine.createSpy('hideExpiringMessageForFreeBet')
    };
    pubSubService = {
      API: pubSubApi,
      publish: jasmine.createSpy('publish')
    };
    device = {
      isTablet: true
    };
  }));

  beforeEach(() => {
    component = new FreeBetsNotificationComponent(freeBetsService, pubSubService, device);
  });

  it('constructor should define component and detect if device is tablet', () => {
    expect(component).toBeDefined();
    expect(component.isTablet).toEqual(device.isTablet);
  });

  describe('ngOnInit', () => {
    it('should show expiry message', () => {
      component.ngOnInit();
      expect(component.message).toEqual('ExpiryMessage');
    });

    it('should show free bet availability message', () => {
      component.isBetslip = true;
      component.ngOnInit();
      expect(component.message).toEqual('You have a free bet available !');
    });
  });

  describe('closeNotification', () => {
    beforeEach(() => {
      spyOn(component.hide, 'emit');
    });

    it('should close notification', () => {
      component.closeNotification();
      expect(component.hide.emit).toHaveBeenCalledTimes(1);
      expect(freeBetsService.hideExpiringMessageForFreeBet).toHaveBeenCalledTimes(1);
      expect(pubSubService.publish).toHaveBeenCalledWith(pubSubService.API.NOTIFICATION_HIDE);
    });

    it('should close notification and DO NOT publish NOTIFICATION_HIDE', () => {
      component.isBetslip = true;
      component.closeNotification();
      expect(component.hide.emit).toHaveBeenCalledTimes(1);
    });
  });
});
