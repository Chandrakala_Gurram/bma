import { LoadingOverlayComponent } from '@shared/components/loadingOverlay/loading-overlay.component';

describe('LoadingOverlayComponent', () => {
  let pubsub;
  let changeDetectorRef;
  let component;
  beforeEach(() => {
    pubsub = {};
    changeDetectorRef = {
      detectChanges: jasmine.createSpy('detectChanges')
    };

    component = new LoadingOverlayComponent(
      pubsub,
      changeDetectorRef
    );
  });

  it('should', () => {
    component.toggleVisibleState({
      overlay: true,
      spinner: true
    });

    expect(changeDetectorRef.detectChanges).toHaveBeenCalled();
  });
});
