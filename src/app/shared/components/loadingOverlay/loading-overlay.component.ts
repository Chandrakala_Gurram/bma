import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import * as _ from 'underscore';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { IVisibleState } from '@shared/components/loadingOverlay/loading-overlay.model';

@Component({
  selector: 'loading-overlay',
  templateUrl: 'loading-overlay.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadingOverlayComponent implements OnInit, OnDestroy {
  overlayVisible: boolean = false;
  spinnerVisible: boolean = false;
  readonly NAME: string = 'LoadingOverlay';

  constructor(
    private pubsub: PubSubService,
    private changeDetectorRef: ChangeDetectorRef,
  ) {
    this.toggleVisibleState = this.toggleVisibleState.bind(this);
  }

  ngOnInit(): void {
    this.pubsub.subscribe(this.NAME, this.pubsub.API.TOGGLE_LOADING_OVERLAY, this.toggleVisibleState);
  }

  ngOnDestroy(): void {
    this.pubsub.unsubscribe(this.NAME);
  }

  /**
   * Toggles visible state of overlay and spinner.
   * @param {boolean=} options.overlay
   * @param {boolean=} options.spinner
   * @private
   */
  private toggleVisibleState(option: IVisibleState) {
    this.overlayVisible = _.isUndefined(option.overlay) ? this.overlayVisible : option.overlay;
    this.spinnerVisible = _.isUndefined(option.spinner) ? this.spinnerVisible : option.spinner;
    this.changeDetectorRef.detectChanges();
  }
}

