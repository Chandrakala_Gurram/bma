import { of } from 'rxjs';

import { SeoStaticBlockComponent } from '@shared/components/seoStaticBlock/seo-static-block.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('SeoStaticBlockComponent', () => {
  let component: SeoStaticBlockComponent,
    pubSub,
    domSanitizer,
    seoDataService,
    rendererService,
    elementRef,
    domToolsService,
    windowRef,
    changeDetectorRef;

  beforeEach(() => {
    domSanitizer = {
      sanitize: () => 'safeString',
      bypassSecurityTrustHtml: jasmine.createSpy('bypassSecurityTrustHtml').and.returnValue('sanitized')
    };
    pubSub = {
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      publishSync: jasmine.createSpy('publishSync'),
      API: pubSubApi
    };
    seoDataService = {
      getPage: jasmine.createSpy('getPage').and.returnValue(of({}))
    };
    rendererService = {
      renderer: {
        listen: jasmine.createSpy('listen').and.returnValue(jasmine.any(Function))
      }
    };
    elementRef = {
      nativeElement : {
        querySelector: jasmine.createSpy('querySelector').and.returnValue({}),
        querySelectorAll: jasmine.createSpy('querySelectorAll').and.returnValue([{
          querySelectorAll: jasmine.createSpy('querySelectorAll').and.returnValue([{}])
        }])
      }
    };
    domToolsService = {
      toggleClass: jasmine.createSpy('toggleClass'),
      toggleVisibility: jasmine.createSpy('toggleVisibility')
    };
    windowRef = {
      nativeWindow: {
        setTimeout: jasmine.createSpy('setTimeout').and.callFake(fn => fn()),
        clearTimeout: jasmine.createSpy('clearTimeout')
      }
    };
    changeDetectorRef = {
      markForCheck: jasmine.createSpy('markForCheck')
    };

    component = new SeoStaticBlockComponent(
      domSanitizer,
      pubSub,
      seoDataService,
      rendererService,
      elementRef,
      domToolsService,
      windowRef,
      changeDetectorRef
    );
  });

  it('ngOnInit', () => {
    component['updateDOM'] = jasmine.createSpy('updateDOM').and.callThrough();
    component['addListeners'] = jasmine.createSpy('addListeners').and.callThrough();
    component.ngOnInit();

    expect(seoDataService.getPage).toHaveBeenCalled();
    expect(pubSub.subscribe).toHaveBeenCalledWith(
      'seoStaticBlockComponent',
      pubSub.API.SEO_DATA_UPDATED,
      component['updateDOM']
    );
    expect(component['addListeners']).toHaveBeenCalled();
  });

  it('ngOnDestroy', () => {
    component.ngOnDestroy();

    expect(pubSub.unsubscribe).toHaveBeenCalledWith('seoStaticBlockComponent');
    expect(windowRef.nativeWindow.clearTimeout).toHaveBeenCalled();
  });

  describe('@updateDOM', () => {
    let data;

    beforeEach(() => {
      data = {
        staticBlock: 'staticBlock'
      };
      component['addListeners'] = jasmine.createSpy('addListeners').and.callThrough();
    });

    it('should add static block content', () => {
      component['updateDOM'](data);

      expect(domSanitizer.bypassSecurityTrustHtml).toHaveBeenCalledWith(data.staticBlock);
    });

    it('should not add static block content', () => {
      data.staticBlock = '';

      component['updateDOM'](data);

      expect(domSanitizer.bypassSecurityTrustHtml).not.toHaveBeenCalled();
    });

    afterEach(() => {
      expect(windowRef.nativeWindow.clearTimeout).toHaveBeenCalled();
      expect(component['addListeners']).toHaveBeenCalled();
      expect(changeDetectorRef.markForCheck).toHaveBeenCalledTimes(1);
    });
  });

  describe('@addListeners', () => {
    it('should add listener to handle expand/collapse panel', () => {
      rendererService.renderer.listen.and.callFake((a, b, cb) => {
        cb();
      });
      component['addListeners']();

      expect(windowRef.nativeWindow.setTimeout).toHaveBeenCalled();
      expect(elementRef.nativeElement.querySelectorAll).toHaveBeenCalledWith('.page-container');
      expect(rendererService.renderer.listen).toHaveBeenCalled();
      expect(domToolsService.toggleClass).toHaveBeenCalled();
      expect(domToolsService.toggleVisibility).toHaveBeenCalled();
    });
  });
});
