import { EventEmitter, Output } from '@angular/core';

export class AbstractNotification {
  @Output() readonly hide: EventEmitter<boolean> = new EventEmitter();
  message: string;

  protected close(): void {
    this.hide.emit();
  }
}
