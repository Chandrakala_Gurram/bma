import { Component, OnDestroy, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';
import { Subscription, Observable, from, of } from 'rxjs';
import { switchMap, mergeMap } from 'rxjs/operators';
import * as _ from 'underscore';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { UserService } from '@core/services/user/user.service';
import { CasinoLinkService } from '@core/services/casinoLink/casino-link.service';
import { DeviceService } from '@core/services/device/device.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { GtmService } from '@core/services/gtm/gtm.service';
import { ServingService } from '@core/services/serving/serving.service';
import { BetslipTabsService } from '@core/services/betslipTabs/betslip-tabs.service';
import { NavigationService } from '@core/services/navigation/navigation.service';
import { CommandService } from '@core/services/communication/command/command.service';

import { IFooterMenu } from '@core/services/cms/models/menu/footer-menu.model';
import { ISystemConfig } from '@core/services/cms/models';
import { IOpenBetsCount } from '@app/bpp/services/bppProviders/bpp-providers.model';

@Component({
  selector: 'footer-menu',
  templateUrl: 'footer-menu.component.html',
  styleUrls: ['footer-menu.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class FooterMenuComponent implements OnInit, OnDestroy {
  // Config object for currently selected betslip tab.
  betSlipActiveTab: { name: string } = { name: '' };
  // The list of all footer menu links.
  // The list of filtered footer menu links.
  footerLinks: IFooterMenu[];
  routeChangeSuccessHandler: Subscription;
  unsubscribeBetsCounter: Subscription;

  animate: boolean = false;
  animateOpenBetsCounter: number = 0;
  moreThanTwenty: boolean = false;
  openBetsCounter: number = 0;

  private readonly title = 'footerMenu';

  constructor(
    private cmsService: CmsService,
    private userService: UserService,
    private casinoLinkService: CasinoLinkService,
    private pubSubService: PubSubService,
    private betslipTabsService: BetslipTabsService,
    private deviceService: DeviceService,
    private gtmService: GtmService,
    private servingService: ServingService,
    private router: Router,
    private navigationService: NavigationService,
    private commandService: CommandService,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.getLinks();
    this.pubSubService.subscribe(
        this.title, this.pubSubService.API.SESSION_LOGOUT, () => {
          this.getLinks();
      }
    );
    this.pubSubService.publishSync(this.pubSubService.API.FOOTER_MENU_READY);

    this.pubSubService.subscribe(
        this.title, this.pubSubService.API.SESSION_LOGIN, () => {
          this.getLinks();
          this.handleMyBetsCount(); // load myBets count information after login
      }
    );
    this.pubSubService.subscribe(this.title, this.pubSubService.API.DEVICE_VIEW_TYPE_CHANGED_NEW, () => this.updateLinksState());

    /**
     * Clears active state for bottom button which has redirect to betSlip if location changed,
     *   because focus on betslip is missed.
     */
    this.routeChangeSuccessHandler = this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        if (this.betSlipActiveTab.name) {
          this.betSlipActiveTab.name = '';
        }

        this.updateLinksState();
      }
    });

    if (this.userService.status) {
      this.handleMyBetsCount(); // load myBets count information after login
    }
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe(this.title);

    if (this.routeChangeSuccessHandler) {
      this.routeChangeSuccessHandler.unsubscribe();
    }

    this.commandService.executeAsync(this.commandService.API.UNSUBSCRIBE_OPEN_BETS_COUNT);
  }

  /**
   * ngFor trackBy function
   * @param {number} index
   * @return {number}
   */
  trackByIndex(index: number): number {
    return index;
  }

  /**
   * Performs navigation to specific place in betSlip widget
   *
   * @param {MouseEvent} $event
   * @param {Object} link
   */
  customRedirect($event: MouseEvent, link: IFooterMenu): void {
    $event.preventDefault();

    if (link.redirectUrl) {
      this.navigationService.openUrl(link.redirectUrl, true);
    }

    this.navigationTracking(link.linkTitle);
    // Send cookies if link is external TODO seems no longer required
    this.servingService.sendExternalCookies(link.relUri);
    // get link to redirect in betSlip
    this.betSlipActiveTab.name = this.betslipTabsService.redirectToBetSlipTab(link.linkTitle);
    // Update state of bottom links.
    this.updateLinksState();
  }

  /**
   * Checks if given link is currently active based on current location and selected tab in
   *   betslip widget.
   * @param {Object} link
   * @return {boolean}
   */
  private isActiveLink(link: IFooterMenu): boolean {
    if (this.betSlipActiveTab.name) {
      return this.betSlipActiveTab.name === link.linkTitle;
    }

    return this.servingService.getClass(link.targetUri);
  }

  /**
   * Updates redirection url and "active" class for the list of links.
   * @param {Array=} links
   */
  private updateLinksState(links: IFooterMenu[] = this.footerLinks): void {
    const pagesWithLinks = ['My Bets', 'Cash Out'],
      isMobile = this.deviceService.isMobile || this.deviceService.isTablet;

    _.each(links, (link: IFooterMenu) => {
      link.redirectUrl = !isMobile && _.contains(pagesWithLinks, link.linkTitle) ? '' : link.targetUri;
      link.isActive = this.isActiveLink(link);
    });

    this.cd.markForCheck();
  }

  /**
   * Tracking - Navigation of footer menu
   * @param {string} linkTitle
   */
  private navigationTracking(linkTitle: string): void {
    this.gtmService.push('trackEvent', {
      eventCategory: 'navigation',
      eventAction: 'footer',
      eventLabel: linkTitle
    });
  }

  /**
   * Retrieves footer menu links from CMS.
   * @private
   */
  private getLinks(): void {
    this.cmsService.getFooterMenu().subscribe((data: IFooterMenu[]) => {
      // #remove-hash code above should be removed after CMS remove #.
      _.each(data, (link: IFooterMenu) => {
        link.targetUri = link.targetUri.replace('#/', '/');
      });
      const status = !this.userService.status ? 'loggedIn' : 'loggedOut';
      const footerLinks = _.difference(data, _.where(data, { showItemFor: status }));

      // Find link to mcasino in Menu elements and add custom parameter 'deliveryPlatform' depending on
      // whether user using Wrapper or HTML5 app in browser
      this.footerLinks = _.map(footerLinks, link => {
        link.targetUri = this.casinoLinkService.uriDecoration(link.targetUri);
        return link;
      });
      this.cd.markForCheck();
      this.updateLinksState(this.footerLinks);
    });
  }

  private handleMyBetsCount(): void {
    this.cmsService.getSystemConfig()
      .pipe(mergeMap((config: ISystemConfig): Observable<IOpenBetsCount | {}> => {
        if (config.BetsCounter && config.BetsCounter.enabled) {
          return this.getOpenBetsCount();
        } else {
          return of({});
        }
      }))
      .subscribe((val: IOpenBetsCount) => {
        this.animateOpenBetsCount(val);
      }
    );
  }

  private getOpenBetsCount(): Observable<IOpenBetsCount> {
    const observableFromPromise = from(this.commandService.executeAsync(this.commandService.API.GET_OPEN_BETS_COUNT));

    return observableFromPromise.pipe(switchMap((value: Observable<IOpenBetsCount>) => value));
  }

  private animateOpenBetsCount(value: IOpenBetsCount): void {
    if (value && (this.animateOpenBetsCounter !== value.count || value.count === 20)) {
      this.animate = false;
      this.cd.markForCheck();
      requestAnimationFrame(() => {
        this.moreThanTwenty = value.moreThanTwenty;
        this.animate = true;
        this.animateOpenBetsCounter = value.count;
        this.openBetsCounter = value.moreThanTwenty ? 20 : value.count || 0;
        this.cd.markForCheck();
      });
    }
  }
}
