import * as _ from 'underscore';
import { Injectable } from '@angular/core';
import { LocaleService } from '@core/services/locale/locale.service';
import environment from '@environment/oxygenEnvConfig';
import { oddsCardConstant, handicapTemplateMarketName } from '@sharedModule/constants/odds-card-constant';
import { OUTRIGHTS_CONFIG } from '@core/constants/outrights-config.constant';
import { IOutrightsConfig } from '@shared/models/outrights-config.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { IMarket } from '@core/models/market.model';
import { CoreToolsService } from '@core/services/coreTools/core-tools.service';
import { ISportConfig } from '@core/services/cms/models';
import { MarketTypeService } from '@root/app/shared/services/marketType/market-type.service';
import { IEventMarketConfig } from '@root/app/core/models/event-market-config.model';

@Injectable()
export class OddsCardHeaderService {

  private readonly marketTypes: Array<string> = oddsCardConstant.MARKET_TYPES;
  private readonly OUTRIGHTS_CONFIG: IOutrightsConfig = OUTRIGHTS_CONFIG;
  private readonly racingData: any = environment.CATEGORIES_DATA.racing;

  constructor(
    private locale: LocaleService,
    private coreTools: CoreToolsService,
    private marketTypeService: MarketTypeService
  ) {}

  /**
   * Check if all events in section are special
   * For such events we don't show fixture header
   */
  isSpecialSection(eventEntities: ISportEvent[], sportConfig: ISportConfig): boolean {
    return _.every(eventEntities, event => {
      return this.isSpecialEvent(event, sportConfig);
    });
  }

  /**
   * Get event sport name
   */
  getSportName(event: ISportEvent): string {
    return event.categoryName.toLowerCase().replace(/\s|\/|\|/g, '');
  }

  /**
   * Check if event is special
   * For such events we don't show fixture header
   */
  isSpecialEvent(event: ISportEvent, sportConfig: ISportConfig): boolean {
    if (!sportConfig) {
      return;
    }
    let sortCodeList; // Checks if event - OutRight.

    if (this.isOutrightSport(event.categoryCode, sportConfig)) {
      sortCodeList = this.OUTRIGHTS_CONFIG.outrightsSportSortCode;
    } else {
      sortCodeList = this.OUTRIGHTS_CONFIG.sportSortCode;
    }

    // Checks if event - Enhance Multiples.
    const isEnhanceMultiples =
    _.contains(sportConfig.specialsTypeIds, Number(event.typeId));

    const isOutright = sortCodeList.indexOf(event.eventSortCode) !== -1;
    let inHandicapMathResult: boolean;
    // The below method returns always the first match of market not based on the selected market
    // this.marketTypeService.getDisplayMarketConfig(sportMarketNames, marketNames)
    if (!environment.CATEGORIES_DATA.categoryIds.includes(event.categoryId)) {
      const displyMarketConfig: IEventMarketConfig =
        this.marketTypeService.getDisplayMarketConfig(sportConfig.config.request.marketTemplateMarketNameIntersects, event.markets);
      inHandicapMathResult = displyMarketConfig && displyMarketConfig.displayMarketName === handicapTemplateMarketName;
    }

    // check if event special (Enhance Multiples or OutRight).
    return isEnhanceMultiples || isOutright || inHandicapMathResult;
  }

  /**
   * Check templateMarketName
   */
  isHomeDrawAwayMarketType(templateMarketName: string): boolean {
    return _.contains(this.marketTypes, templateMarketName);
  }

  isRacing(sportId: string): boolean {
    return _.some(this.racingData, (elem: { id: string; }) => elem.id === sportId);
  }

  /**
   * Get locale from string
   *
   * @param {string} arr
   * @returns {Array}
   */
  getLocale(arr: string): Array<string> | string {
    return arr ? _.map(arr.split(','), item => {
      return +item ? item : this.locale.getString(`sb.${item}`);
    }) : '';
  }

  extractMarketNameFromEvents(events: ISportEvent[], isFilterByTemplateMarketName?: boolean): string[] {
    const marketNames = _.reduce(events, (accumulator, event) => {
      const eventMarketNames = (event.markets || []).map(market => {
        if (isFilterByTemplateMarketName) {
          return market.templateMarketName;
        }

        return market.name;
      });

      accumulator.push(...eventMarketNames);

      return accumulator;
    }, []);
    return marketNames;
  }

  /**
   * Check for showing/hiding component (should contain minimum one market)
   */
  showComponent(marketNames: string[], selectedMarket?: string): boolean {
    if (marketNames.length === 0) {
      return false;
    }

    if (!selectedMarket) {
      return true;
    }

    return _.contains(marketNames, selectedMarket);
  }

  /**
   * Get market by template market name
   */
  getMarketByTemplateMarketName(eventMarkets: IMarket[], selectedMarket: string, isFilterByTemplateMarketName: boolean): IMarket {
    let market;

    if (isFilterByTemplateMarketName && selectedMarket) {
      market = _.find(eventMarkets, eventMarket => eventMarket.templateMarketName.toLowerCase() === selectedMarket.toLowerCase());
    } else {
      market = eventMarkets[0];
    }
    return market;
  }

  /**
   * Get odds card header by view type
   */
  getHeaderByMarketName(isFootballMarket: boolean, market: IMarket): string | void {
    let header;
    const templateMarketName = market && market.templateMarketName.toLowerCase();
    if (isFootballMarket && this.isHomeDrawAwayMarketType(market.templateMarketName)) {
      header = 'homeDrawAwayType';
    }

    if (isFootballMarket && templateMarketName === 'to win to nil') {
      header = 'homeAwayType';
    }

    if (isFootballMarket && templateMarketName === 'score goal in both halves') {
      header = 'yesNoType';
    }
    return header;
  }

  /**
   * Get odds card header by view type
   */
  getHeaderByViewType(viewType: string, outcomesCount: number, sportName: string): string | void {
    let header;

    if (viewType === 'columns-2-3' ||
      viewType === 'columns-3' ||
      outcomesCount === 3
    ) {
      header = sportName === 'golf' ? 'oneThreeType' : 'homeDrawAwayType';
    }
    return header;
  }

  /**
   * Checks if any event shown has comments with scores
   * @param events
   * @returns {boolean}
   */
  isEventsHaveScores(events: ISportEvent[]): boolean {
    return events.some((event: ISportEvent) => this.isEventHasScore(event));
  }

  /**
   * Sort events so those with comments and scores would be displayed first
   * @param events
   */
  sortEventsByScores(events: ISportEvent[]): void {
    events.sort((a: ISportEvent, b: ISportEvent) => {
      const aHasScores = this.isEventHasScore(a);
      const bHasScores = this.isEventHasScore(b);
      if (aHasScores && !bHasScores) {
        return -1;
      }
      if (!aHasScores && bHasScores) {
        return 1;
      }
      return 0;
    });
  }

  /**
   * Checks if single event has scores
   * @param event ISportEvent
   * @returns {boolean}
   */
  private isEventHasScore(event: ISportEvent): boolean {
    return this.coreTools.hasOwnDeepProperty(event, 'comments.teams.home.score') ||
      this.coreTools.hasOwnDeepProperty(event, 'comments.teams.player_1');
  }

  /**
   * Checks if sport is outright
   */
  private isOutrightSport(categoryCode: string, sportConfig: ISportConfig): boolean {
    return _.indexOf(this.OUTRIGHTS_CONFIG.outrightsSports, categoryCode) !== -1 || sportConfig.isOutrightSport;
  }
}
