import { OddsCardHeaderService } from './odds-card-header.service';

describe('OddsCardHeaderService', () => {
  let service: OddsCardHeaderService;

  let locale, coreTools, marketTypeService: any;

  beforeEach(() => {
    locale = {
      getString: jasmine.createSpy()
    };
    coreTools = {
      hasOwnDeepProperty: jasmine.createSpy('hasOwnDeepProperty'),
    };
    marketTypeService = {
      getDisplayMarketConfig: jasmine.createSpy('getDisplayMarketConfig').and.returnValue({})
    };
    service = new OddsCardHeaderService(
      locale,
      coreTools,
      marketTypeService
    );
  });

  it('isSpecialSection', () => {
    const res: boolean = service.isSpecialSection(<any>[
      {
        categoryCode: 16,
        categoryName: 'football',
        typeId: '8',
        eventSortCode: 'TNMT'
      }
    ], <any>{
      football: [{
        specialsTypeIds: ['8']
      }],
      config: {
        request: {
          marketTemplateMarketNameIntersects: ''
        }
      }
    });

    expect(res).toBeTruthy();
  });

  it('isSpecialSection: outright', () => {
    const res: boolean = service.isSpecialSection(<any>[
      {
        categoryCode: 16,
        categoryName: 'football',
        typeId: '8',
        eventSortCode: 'TNMT'
      }
    ], <any>{
      football: [{
        specialsTypeIds: ['7']
      }],
      config: {
        request: {
          marketTemplateMarketNameIntersects: ''
        }
      }
    });

    expect(res).toBeTruthy();
  });

  it('isHomeDrawAwayMarketType: true', () => {
    expect(service.isHomeDrawAwayMarketType('Match Betting')).toBeTruthy();
  });

  it('isHomeDrawAwayMarketType: false', () => {
    expect(service.isHomeDrawAwayMarketType('Unkown')).toBeFalsy();
  });

  it('isRacing: true', () => {
    expect(service.isRacing('21')).toBeTruthy();
  });

  it('isRacing: false', () => {
    expect(service.isRacing('16')).toBeFalsy();
  });

  it('getLocale', () => {
    service.getLocale('locale,locale');
    expect(locale.getString).toHaveBeenCalledTimes(2);
    expect(locale.getString).toHaveBeenCalledWith('sb.locale');
  });

  it('getLocale', () => {
    expect(service.getLocale('')).toEqual('');
  });

  it('showComponent', () => {
    const res: boolean = service.showComponent(<any>[
      'selectedMarket'
    ], 'selectedMarket');
    expect(res).toBeTruthy();
  });

  it('showComponent', () => {
    const res: boolean = service.showComponent(<any>[{
      markets: []
    }], 'selectedMarket');

    expect(res).toBeFalsy();
  });

  describe('@getHeaderByMarketName', () => {
    it('it should set homeAwayType header', () => {
      const market = {
        templateMarketName: 'To Win to Nil'
      } as any;
      const result = service.getHeaderByMarketName(true, market);
      expect(result).toEqual('homeAwayType');
    });

    it('it should set homeDrawAwayType header', () => {
      const market = {
        templateMarketName: 'Match Betting'
      } as any;
      const result = service.getHeaderByMarketName(true, market);
      expect(result).toEqual('homeDrawAwayType');
    });

    it('it should set yesNoType header', () => {
      const market = {
        templateMarketName: 'Score Goal in Both Halves'
      } as any;
      const result = service.getHeaderByMarketName(true, market);
      expect(result).toEqual('yesNoType');
    });

    it('it should not set header', () => {
      const market = {
        templateMarketName: 'Score Goal in Both Halves'
      } as any;
      const result = service.getHeaderByMarketName(true, market);
      expect(result).toEqual('yesNoType');
    });
  });

  describe('isEventsHaveScores', () => {
    beforeEach(() => {
      coreTools.hasOwnDeepProperty = jasmine.createSpy('hasOwnDeepProperty').and.callFake((obj, path) => {
        if (path === 'comments.teams.home.score' && obj.comments.teams.home.score) {
          return true;
        }
      });
    });

    it('should return true if event comments has player_1', () => {
      coreTools.hasOwnDeepProperty = jasmine.createSpy('hasOwnDeepProperty').and.callFake((obj, path) => {
        if (path === 'comments.teams.player_1' && obj.comments.teams.player_1) {
          return true;
        }
      });
      const event = {
        comments: {
          teams: {
            player_1: { }
          }
        }
      };
      expect(service.isEventsHaveScores([event] as any)).toBeTruthy();
    });

    it('should return true if event comments have scores', () => {
      const event = {
        comments: {
          teams: {
            home: {
              score: '0'
            }
          }
        }
      };
      expect(service.isEventsHaveScores([event] as any)).toBeTruthy();
    });

    it('should not return true if event comments have no scores', () => {
      const event = {
        comments: {
          teams: {
            home: {}
          }
        }
      };
      expect(service.isEventsHaveScores([event] as any)).toBeFalsy();
    });
  });

  describe('sortEventsByScores', () => {
    let eventA, eventB, events;

    beforeEach(() => {
      coreTools.hasOwnDeepProperty = jasmine.createSpy('hasOwnDeepProperty').and.callFake((obj, path) => {
        if (path === 'comments.teams.home.score' && obj.comments.home.score) {
          return true;
        }
      });
      eventA = { comments: { home: { } } };
      eventB = { comments: { home: { } } };
      events = [eventA, eventB];
    });

    describe('should correctly sort events when', () => {
      it('both events have no scores', () => {
        service.sortEventsByScores(events);
        expect(events).toEqual([eventA, eventB]);
      });

      it('A has scores and B has no scores', () => {
        eventA.comments.home.score = '0';
        service.sortEventsByScores(events);
        expect(events).toEqual([eventA, eventB]);
      });

      it('A has no scores and B has scores', () => {
        eventB.comments.home.score = '0';
        service.sortEventsByScores(events);
        expect(events).toEqual([eventB, eventA]);
      });

      it('both events have scores', () => {
        eventA.comments.home.score = '0';
        eventB.comments.home.score = '0';
        service.sortEventsByScores(events);
        expect(events).toEqual([eventA, eventB]);
      });
    });
  });

  describe('isSpecialEvent', () => {
    it('should return false (no sport config)', () => {
      expect(service.isSpecialEvent({} as any, null)).toBeFalsy();
    });

    it('should return true (outright sport)', () => {
      const event: any = { categoryCode: 'MOTOR_CARS', eventSortCode: 'TNMT'  };
      const sportConfig: any = {
      config: {
        request: {
          marketTemplateMarketNameIntersects: ''
        }
      }};
      expect(service.isSpecialEvent(event, sportConfig)).toBeTruthy();
    });

    it('should return true (enhance multiples)', () => {
      const event: any = { typeId: 1 };
      const sportConfig: any = { specialsTypeIds: [1],
      config: {
        request: {
          marketTemplateMarketNameIntersects: ''
        }
      } };
      expect(service.isSpecialEvent(event, sportConfig)).toBeTruthy();
    });

    it('should return false (not special event)', () => {
      const event: any = { typeId: 1 };
      const sportConfig: any = { specialsTypeIds: [2],
      config: {
        request: {
          marketTemplateMarketNameIntersects: ''
        }
      } };
      expect(service.isSpecialEvent(event, sportConfig)).toBeFalsy();
    });
    it('should return false (not special event)', () => {
      const event: any = { typeId: 1, categoryId: '16' };
      const sportConfig: any = {
        specialsTypeIds: [2],
        config: {
          request: {
            marketTemplateMarketNameIntersects: ''
          }
        }
      };
      expect(service.isSpecialEvent(event, sportConfig)).toBeFalsy();
    });
  });

  describe('#extractMarketNameFromEvents', () => {
    it('should return market names', () => {
      const events = [{
        categoryCode: 16,
        categoryName: 'football',
        typeId: '8',
        markets: [{
          templateMarketName: 'templateMarketName'
        }],
      }];
      const result = service.extractMarketNameFromEvents(events as any, true);

      expect(result).toEqual(['templateMarketName'] as any);
    });
  });
});
