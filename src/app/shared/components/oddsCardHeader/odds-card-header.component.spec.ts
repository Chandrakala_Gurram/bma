import { of } from 'rxjs';

import { OddsCardHeaderComponent } from '@shared/components/oddsCardHeader/odds-card-header.component';

describe('OddsCardHeaderComponent', () => {
  let component: OddsCardHeaderComponent,
    marketTypeService,
    templateService,
    oddsCardHeaderService,
    pubSubService,
    scoreParserService,
    sportsConfigService,
    sportsConfigHelperService,
    coreToolsService: any;

  beforeEach(() => {
    marketTypeService = {
      isYesNoType: jasmine.createSpy('isYesNoType').and.returnValue(true),
      isOverUnderType: jasmine.createSpy('isOverUnderType').and.returnValue(true),
      isHomeDrawAwayType: jasmine.createSpy('isHomeDrawAwayType'),
      someEventsAreMatchResultType: jasmine.createSpy('someEventsAreMatchResultType').and.returnValue(false),
      isOneTieTwoType: jasmine.createSpy('isOneTieTwoType').and.returnValue(false),
      getDisplayMarketConfig: jasmine.createSpy('getDisplayMarketConfig').and.returnValue({
        displayMarketName: 'market name'
      })
    };
    templateService = {
      getMarketViewType: jasmine.createSpy('getMarketViewType').and.returnValue('viewType')
    };
    oddsCardHeaderService = {
      getSportName: jasmine.createSpy('getSportName').and.returnValue('some'),
      isRacing: jasmine.createSpy('isRacing').and.returnValue(true),
      getLocale: jasmine.createSpy('getLocale'),
      isSpecialSection: jasmine.createSpy('isSpecialSection').and.returnValue(false),
      isEventsHaveScores: jasmine.createSpy('isEventsHaveScores'),
      sortEventsByScores: jasmine.createSpy('sortEventsByScores'),
      extendSportConfig: jasmine.createSpy('extendSportConfig'),
      showComponent: jasmine.createSpy('showComponent').and.returnValue(true),
      getMarketByTemplateMarketName: jasmine.createSpy('getMarketByTemplateMarketName').and.returnValue({ outcomes: [{}] }),
      getHeaderByMarketName: jasmine.createSpy('getHeaderByMarketName').and.returnValue('some'),
      getHeaderByViewType: jasmine.createSpy('getHeaderByViewType'),
      extractMarketNameFromEvents: jasmine.createSpy('extractMarketNameFromEvents')
    };
    pubSubService = {
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: {
        EVENT_SCORES_UPDATE: 'EVENT_SCORES_UPDATE',
        DELETE_EVENT_FROM_CACHE: 'DELETE_EVENT_FROM_CACHE'
      },
    };
    coreToolsService = {
      uuid: jasmine.createSpy('uuid').and.returnValue('randomUuid'),
    };
    scoreParserService = {
      getScoreHeaders: jasmine.createSpy('getScoreHEaders'),
    };
    sportsConfigService = {
      getSport: jasmine.createSpy('getSport').and.returnValue(of({
        sportConfig: {}
      }))
    };
    sportsConfigHelperService = {
      getSportConfigName: jasmine.createSpy('getSportConfigName')
    };

    component = new OddsCardHeaderComponent(
      marketTypeService,
      templateService,
      oddsCardHeaderService,
      pubSubService,
      coreToolsService,
      scoreParserService,
      sportsConfigService,
      sportsConfigHelperService
    );

    component.sportConfig = {
      config: {
        request: {
          marketTemplateMarketNameIntersects: 'marketTemplateMarketNameIntersects'
        }
      }
    };
  });

  describe('#initHeader', () => {
    beforeEach(() => {
      component.events = [{}] as any;
    });

    it('no events', () => {
      component.events = [];
      component['oddsCardHeader'] = '1';

      component['initHeader']();

      expect(component['oddsCardHeader']).toBeUndefined();
    });

    it('no sport data in sportsConfig', () => {
      component['sportsConfig'] = {} as any;

      component['initHeader']();

      expect(component['oddsCardHeader']).toBeUndefined();
      expect(component.showOddsCardHeader).toBe(false);
      expect(marketTypeService.getDisplayMarketConfig).toHaveBeenCalled();
    });

    it('should not call getDisplayMarketConfig', () => {
      component.sportConfig = {
        isFootball: true,
        config: {
          request: {
            categoryId: '16'
          }
        }
      } as any;

      component['initHeader']();

      expect(component['oddsCardHeader']).toBeUndefined();
      expect(component.showOddsCardHeader).toBe(false);
      expect(marketTypeService.getDisplayMarketConfig).not.toHaveBeenCalled();
    });

    it('selectedMarket not equal handicap', () => {
      component['initHeader']();

      expect(component['oddsCardHeader']).toBeUndefined();
      expect(component['showOddsCardHeader']).toEqual(false);
    });

    it('sportConfig is null', () => {
      oddsCardHeaderService.isRacing = jasmine.createSpy().and.returnValue(false);
      component.sportConfig = null as any;

      component['initHeader']();

      expect(component['oddsCardHeader']).toBeUndefined();
      expect(component['showOddsCardHeader']).toEqual(false);
    });

    it('isRacing return false', () => {
      oddsCardHeaderService.isRacing = jasmine.createSpy('isRacing').and.returnValue(false);
      oddsCardHeaderService.isSpecialSection = jasmine.createSpy('isSpecialSection').and.returnValue(true);

      component['initHeader']();

      expect(component['oddsCardHeader']).toBeUndefined();
      expect(component['showOddsCardHeader']).toEqual(false);
    });

    describe('exist sportConfig and isSpecialEvent = false', () => {
      beforeEach(() => {
        oddsCardHeaderService.isRacing = jasmine.createSpy('isRacing').and.returnValue(false);
        component['getOddsCardHeader'] = jasmine.createSpy('getOddsCardHeader').and.callThrough();
        component['setOddCardHeaderType'] = jasmine.createSpy('setOddCardHeaderType').and.callThrough();
        component['setHeaderContent'] = jasmine.createSpy('setHeaderContent').and.callThrough();
        component['showScoreHeaders'] = jasmine.createSpy('showScoreHeaders').and.callThrough();
      });

      it('exist isMultiTemplateSport', () => {
        component['initHeader']();
      });

      it('selectedMarket !== handicapTemplateMarketName', () => {
        component.sportConfig.config.request = {
          marketTemplateMarketNameIntersects: 'marketTemplateMarketNameIntersects'
        } as any;

        component['initHeader']();
      });

      it('sportName !== football', () => {
        component.sportConfig.config.oddsCardHeaderType = null;

        component['initHeader']();
      });

      it('sportName == football', () => {
        component.sportConfig.config.oddsCardHeaderType = {};
        component['sportName'] = 'football';

        component['initHeader']();
      });

      afterEach(() => {
        expect(component['setOddCardHeaderType']).toHaveBeenCalled();
        expect(component['showScoreHeaders']).toHaveBeenCalled();
        expect(component['setOddCardHeaderType']).toHaveBeenCalledTimes(2);
        expect(component['setHeaderContent']).toHaveBeenCalled();
        expect(component['getOddsCardHeader']).toHaveBeenCalledTimes(1);
      });
    });

    describe('set oddsCardHeader', () => {
      beforeEach(() => {
        component.sportConfig.config.oddsCardHeaderType = {
          outcomesTemplateType1: 'outcomesTemplateType1'
        };
        oddsCardHeaderService.isRacing = jasmine.createSpy('isRacing').and.returnValue(false);

        component['setHeaderContent'] = jasmine.createSpy('setHeaderContent').and.callThrough();
        component['showScoreHeaders'] = jasmine.createSpy('showScoreHeaders').and.callThrough();
        component['setOddCardHeaderType'] = jasmine.createSpy('setOddCardHeaderType').and.callThrough();

      });
      it('if oddsCardHeaderType is obj', () => {
        component['initHeader']();

        expect(component['oddsCardHeader']).toEqual('outcomesTemplateType1');
      });

      it('if oddsCardHeaderType is string', () => {
        component.sportConfig.config.oddsCardHeaderType = 'outcomesTemplateType';

        component['initHeader']();

        expect(component['oddsCardHeader']).toEqual('outcomesTemplateType');
      });

      afterEach(() => {
        expect(component['setHeaderContent']).toHaveBeenCalled();
        expect(component['showScoreHeaders']).toHaveBeenCalled();
        expect(component['setOddCardHeaderType']).toHaveBeenCalled();
      });
    });

    it('is not special', () => {
      component['sportsConfig'] = {
        'some': {}
      } as any;

      component['initHeader']();

      expect(component['oddsCardHeader']).toBeUndefined();
      expect(component.showOddsCardHeader).toBe(false);
    });

    it('isMultiTemplateSport true/false && no oddsCardHeaderType', () => {
      component['sportConfig'] = {
        config: {
          isMultiTemplateSport: true,
          request: {
            marketTemplateMarketNameIntersects: 'marketTemplateMarketNameIntersects'
          }
        }
      } as any;
      (oddsCardHeaderService.isRacing as jasmine.Spy).and.returnValue(true);
      component['getOddsCardHeader'] = jasmine.createSpy().and.returnValue('header text');
      component['setOddCardHeaderType'] = jasmine.createSpy();
      component['setHeaderContent'] = jasmine.createSpy();

      component['initHeader']();
      expect(component['oddsCardHeader']).toBe(undefined);
      expect(component['showOddsCardHeader']).toBe(false);
      expect(component['undisplayedMarket']).toBe(null);
    });
  });

  describe('showScoreHeaders', () => {
    it('it should not set scoreHeaders if isScoreHeader=false', () => {
      component.isScoreHeader = false;
      component.showScoreHeaders('16');

      expect(component.scoreHeaders).toBe(null);
    });

    it('it should set scoreHeaders if isScoreHeader=true', () => {
      component.events = [];
      component.isScoreHeader = true;
      oddsCardHeaderService.isEventsHaveScores = jasmine.createSpy('isEventsHaveScores').and.returnValue(true);
      scoreParserService.getScoreHeaders = jasmine.createSpy('getScoreHeaders').and.returnValue(['S', 'G', 'P']);
      component.showScoreHeaders('16');

      expect(component.scoreHeaders).toEqual(['S', 'G', 'P']);
      expect(oddsCardHeaderService.sortEventsByScores).toHaveBeenCalledWith([]);
    });

    it('it should not set scoreHeaders if isScoreHeader=false', () => {
      component.events = [];
      component.isScoreHeader = true;
      oddsCardHeaderService.isEventsHaveScores = jasmine.createSpy('isEventsHaveScores').and.returnValue(true);
      scoreParserService.getScoreHeaders = jasmine.createSpy('getScoreHeaders').and.returnValue(null);

      component.showScoreHeaders('16');

      expect(component.scoreHeaders).toEqual(null);
      expect(oddsCardHeaderService.sortEventsByScores).not.toHaveBeenCalled();
    });
  });


  describe('#setHeaderContent', () => {
    it('it should set yesNoType header', () => {
      component['getFirstNotSpecialMarket'] = jasmine.createSpy().and.returnValue({});
      component.events = [{ markets: [] }] as any;
      component['oddsCardHeader'] = 'yesNoType';

      component['setHeaderContent']();

      expect(oddsCardHeaderService.getLocale).toHaveBeenCalledWith('over,under');
    });

    it('it should set homeDrawAwayType header', () => {
      component['getFirstNotSpecialMarket'] = jasmine.createSpy().and.returnValue({});
      component.events = [] as any;
      component['oddsCardHeader'] = 'homeDrawAwayType';

      component['setHeaderContent']();

      expect(oddsCardHeaderService.getLocale).toHaveBeenCalledWith('home,draw,away');
    });

    it('it should set homeAwayType header', () => {
      component['getFirstNotSpecialMarket'] = jasmine.createSpy().and.returnValue({});
      component.events = [] as any;
      component['oddsCardHeader'] = 'homeAwayType';

      component['setHeaderContent']();

      expect(oddsCardHeaderService.getLocale).toHaveBeenCalledWith('home,away');
    });

    it('should set showOddsCardHeader header', () => {
      component['availableOddsHeader'] = true;
      component['oddsCardHeader'] = 'oddsCardHeader';
      component.events = [] as any;

      component['setHeaderContent']();

      expect(component.showOddsCardHeader).toEqual(true);
    });

    it('should not set showOddsCardHeader header', () => {
      component['availableOddsHeader'] = true;
      component['hasOutcomeStatusTrue'] = true;
      component.events = [] as any;

      component['setHeaderContent']();

      expect(component.showOddsCardHeader).toEqual(false);
    });

    it('should not set showOddsCardHeader header', () => {
      component['availableOddsHeader'] = true;
      component.events = [] as any;

      component['setHeaderContent']();

      expect(component.showOddsCardHeader).toEqual(false);
    });

    it('should set OverUnderType header', () => {
      marketTypeService.isOverUnderType = jasmine.createSpy('isOverUnderType').and.returnValue(true);
      component['getFirstNotSpecialMarket'] = jasmine.createSpy().and.returnValue({});
      component.events = [{}] as any;
      component['oddsCardHeader'] = 'homeDrawAwayType';

      component['setHeaderContent']();

      expect(oddsCardHeaderService.getLocale).toHaveBeenCalledWith('over,under');
    });

    it('should set over/under header', () => {
      component['getFirstNotSpecialMarket'] = jasmine.createSpy().and.returnValue(null);
      component.events = [{}] as any;
      component['oddsCardHeader'] = 'homeDrawAwayType';
      component['getFirstNotSpecialMarket'] = jasmine.createSpy('getFirstNotSpecialMarket').and.returnValue(true);

      component['setHeaderContent']();

      expect(oddsCardHeaderService.getLocale).toHaveBeenCalledWith('over,under');
    });
  });

  describe('#getFirstNotSpecialMarket', () => {
    it('should call getFirstNotSpecialMarket', () => {
      oddsCardHeaderService.isSpecialEvent = jasmine.createSpy('isSpecialEvent').and.returnValue(true);
      component.sportConfig = {} as any;
      component['getFirstNotSpecialMarket']([{
        id: '123',
        markets: []
      }] as any);

      expect(oddsCardHeaderService.isSpecialEvent).toHaveBeenCalledWith({
        id: '123',
        markets: []
      }, {});
    });

   it('should call getFirstNotSpecialMarket with selectedMarket not equal handicap', () => {
      component.selectedMarket = 'Match Result';
      oddsCardHeaderService.isSpecialEvent = jasmine.createSpy('isSpecialEvent').and.returnValue(false);
      component.sportConfig = {} as any;
      component['getFirstNotSpecialMarket']([{
        id: '123',
        markets: [{}]
      }] as any);

      expect(oddsCardHeaderService.isSpecialEvent).toHaveBeenCalledWith({
        id: '123',
        markets: [{}]
      }, {});
    });

    it('should get filtered market to set Card Header', () => {
      oddsCardHeaderService.isSpecialEvent = jasmine.createSpy('isSpecialEvent').and.returnValue(false);
      const events = [{
        id: '123',
        markets: [{
          templateMarketName: 'Match Result'
        },
        {
          templateMarketName: ''
        }]
      }] as any;
      component.selectedMarket = 'Match Result';
      component['isFilterByTemplateMarketName'] = true;

      const result = component['getFirstNotSpecialMarket'](events);

      expect(result).toEqual({
        templateMarketName: 'Match Result'
      } as any);
    });
  });

  describe('#getOddsCardHeader', () => {
    it('should call getOddsCardHeader method for special event', () => {
      oddsCardHeaderService.isSpecialEvent = jasmine.createSpy('isSpecialEvent').and.returnValue(true);
      component.sportConfig = {} as any;
      component['getOddsCardHeader']([{
        id: '12345'
      }] as any);

      expect(oddsCardHeaderService.isSpecialEvent).toHaveBeenCalledWith({
        id: '12345'
      }, {});
    });

    it('should call getOddsCardHeader method for non special event', () => {
      oddsCardHeaderService.isSpecialEvent = jasmine.createSpy('isSpecialEvent').and.returnValue(false);
      component.sportConfig = {} as any;
      component['getOddsCardHeader']([{
        id: '12345',
        markets: [{
          name: 'Goals'
        }]
      }] as any);

      expect(oddsCardHeaderService.isSpecialEvent).toHaveBeenCalledWith({
        id: '12345',
        markets: [{
          name: 'Goals'
        }]
      }, {});
      expect(templateService.getMarketViewType).not.toHaveBeenCalled();
      expect(oddsCardHeaderService.getHeaderByViewType).not.toHaveBeenCalled();
    });

    it('should call getMarketViewType method for market', () => {
      const events = [{
        id: '12345',
        markets: [{
          name: 'Goals'
        }]
      }] as any;
      const sportName = 'football';
      component['selectedMarket'] = 'selectedMarket';

      oddsCardHeaderService.isSpecialEvent = jasmine.createSpy('isSpecialEvent').and.returnValue(false);
      oddsCardHeaderService.getHeaderByMarketName = jasmine.createSpy('getHeaderByMarketName').and.returnValue('');
      component.sportConfig = {} as any;
      component['getOddsCardHeader'](events, sportName);

      expect(oddsCardHeaderService.isSpecialEvent).toHaveBeenCalledWith({
        id: '12345',
        markets: [{
          name: 'Goals'
        }]
      }, {});

      expect(templateService.getMarketViewType).toHaveBeenCalled();
      expect(oddsCardHeaderService.getHeaderByViewType).toHaveBeenCalled();
    });

    it('should not call getMarketViewType method for market', () => {
      const events = [{
        id: '12345',
        markets: [{
          name: 'Goals'
        }]
      }] as any;
      const sportName = 'football';
      component['selectedMarket'] = 'selectedMarket';

      oddsCardHeaderService.isSpecialEvent = jasmine.createSpy('isSpecialEvent').and.returnValue(false);
      oddsCardHeaderService.getHeaderByMarketName = jasmine.createSpy('getHeaderByMarketName').and.returnValue('');
      oddsCardHeaderService.getMarketByTemplateMarketName = jasmine.createSpy('getMarketByTemplateMarketName').and.returnValue(null);
      component.sportConfig = {} as any;
      component['getOddsCardHeader'](events, sportName);

      expect(oddsCardHeaderService.isSpecialEvent).toHaveBeenCalledWith({
        id: '12345',
        markets: [{
          name: 'Goals'
        }]
      }, {});

      expect(oddsCardHeaderService.getHeaderByMarketName).not.toHaveBeenCalled();
      expect(templateService.getMarketViewType).not.toHaveBeenCalled();
      expect(oddsCardHeaderService.getHeaderByViewType).not.toHaveBeenCalled();
    });
  });

  describe('ngOnInit', () => {
    it('should set uuid', () => {
      component.events = [{}] as any;
      component.ngOnInit();

      expect(coreToolsService.uuid).toHaveBeenCalled();
      expect(component['uniqueId']).toBe('randomUuid');
    });

    it('should hasOutcomeStatusTrue to equal false', () => {
      component.events = undefined as any;
      component.ngOnInit();

      expect(component.events).toEqual([]);
      expect(component['hasOutcomeStatusTrue']).toEqual(false);
    });

    it('should hasOutcomeStatusTrue to equal true', () => {
      component.events = [{ outcomeStatus: true }] as any;
      component.ngOnInit();

      expect(component['hasOutcomeStatusTrue']).toEqual(true);
    });

    it('should subscribe to score updates and call initHeader on update if event id matches', () => {
      component.events = [{ id: 1}] as any;
      component['initHeader'] = jasmine.createSpy('initHeader');
      pubSubService.subscribe = jasmine.createSpy('subscribe').and.callFake((subId, updateType, updateHandler) => {
        const update = { event: { id: 1 } };

        updateHandler(update);
      });

      component.ngOnInit();

      expect(pubSubService.subscribe).toHaveBeenCalledWith('oddsCardHeader_randomUuid', 'EVENT_SCORES_UPDATE', jasmine.any(Function));
      expect(pubSubService.subscribe).toHaveBeenCalledWith('oddsCardHeader_randomUuid', 'DELETE_EVENT_FROM_CACHE', jasmine.any(Function));
      expect(component['initHeader']).toHaveBeenCalledTimes(2);
    });

    it('should subscribe to score updates and not call initHeader on update if event id does not match', () => {
      component.events = [{ id: 2 }] as any;
      component['initHeader'] = jasmine.createSpy('initHeader').and.callThrough();
      pubSubService.subscribe = jasmine.createSpy('subscribe').and.callFake((subId, updateType, updateHandler) => {
        const update = { event: { id: 1 } };

        updateHandler(update);
      });

      component.ngOnInit();

      expect(pubSubService.subscribe).toHaveBeenCalledWith('oddsCardHeader_randomUuid', 'EVENT_SCORES_UPDATE', jasmine.any(Function));
      expect(pubSubService.subscribe).toHaveBeenCalledWith('oddsCardHeader_randomUuid', 'DELETE_EVENT_FROM_CACHE', jasmine.any(Function));
      expect(component['cachedEventsIds']).toEqual([2]);
    });

    it('should get sport config', () => {
      component.events = [{ id: 2 }] as any;
      sportsConfigHelperService.getSportConfigName.and.returnValue('some');
      component.sportConfig = null;
      component['initHeader'] = jasmine.createSpy('initHeader');

      component.ngOnInit();

      expect(sportsConfigService.getSport).toHaveBeenCalledWith('some');
      expect(component.sportConfig).toBeDefined();
      expect(component['initHeader']).toHaveBeenCalled();
    });

    it('should not get sport config', () => {
      component.events = [{ id: 2 }] as any;
      sportsConfigHelperService.getSportConfigName.and.returnValue('some');
      sportsConfigService.getSport.and.returnValue(of(null));
      component.sportConfig = null;
      component['initHeader'] = jasmine.createSpy('initHeader');

      component.ngOnInit();

      expect(sportsConfigService.getSport).toHaveBeenCalledWith('some');
      expect(component.sportConfig).toBeNull();
      expect(component['initHeader']).toHaveBeenCalled();
    });
  });

  describe('#ngOnChanges', () => {
    beforeEach(() => {
      component['initHeader'] = jasmine.createSpy('initHeader').and.callThrough();
    });

    it('should have cachedEventsIds', () => {
      const changes = {
        events: {
          currentValue: 'value',
          previousValue: 'value1'
        }
      };
      component.events = [{ id: 2}] as any;

      component.ngOnChanges(changes as any);

      expect(component['cachedEventsIds']).toEqual([2]);
      expect(component['initHeader']).toHaveBeenCalled();
    });

    it('should call initHeader', () => {
      const changes = {
        events: {
          currentValue: 'value',
          previousValue: 'value'
        },
        undisplayedMarket: {
          currentValue: {
            isDisplayed: false
          }
        }
      };
      component.events = [{ id: 2}] as any;

      component.ngOnChanges(changes as any);

      expect(component['initHeader']).toHaveBeenCalled();
    });

    it('should call initHeader', () => {
      const changes = {
        selectedMarket: 'selectedMarket'
      };
      component.events = [{ id: 2}] as any;

      component.ngOnChanges(changes as any);

      expect(component['initHeader']).toHaveBeenCalled();
    });
  });

  it('should unsubscribe from pubSub', () => {
    component['uniqueId'] = '123';
    component['sportsConfigSubscription'] = { unsubscribe: jasmine.createSpy('unsubscribe') } as any;

    component.ngOnDestroy();

    expect(pubSubService.unsubscribe).toHaveBeenCalledWith('oddsCardHeader_123');
    expect(component['sportsConfigSubscription'].unsubscribe).toHaveBeenCalled();
  });

  it('@setOddCardHeaderType', () => {
    const events = [{}] as any,
      oddsCardHeader = 'oddsCardHeader';

    component['setOddCardHeaderType'](events, oddsCardHeader);

    expect(events[0].oddsCardHeaderType).toEqual(oddsCardHeader);
  });
});
