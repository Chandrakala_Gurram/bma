import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IGroupedSportEvent } from '@core/models/sport-event.model';

@Component({
  selector: 'breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.less']
})
export class BreadcrumbsComponent {
  @Input() items: { name: string, targetUri: string }[];
  @Output() readonly navigationMenu: EventEmitter<void> = new EventEmitter();
  @Input() isExpanded: boolean;
  @Input() menuItems: IGroupedSportEvent[];

  constructor() { }

  trackByBreadcrumb(breadCrumb): string {
    return breadCrumb.name;
  }

  lastItemClick(): void {
    if (this.menuItems && this.menuItems.length) {
      this.navigationMenu.emit();
    } else {
      return;
    }
  }
}
