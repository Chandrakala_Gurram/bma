import { Component, EventEmitter, Input, OnDestroy, OnInit, OnChanges, Output } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'underscore';
import { ISwitcherConfig } from '@core/models/switcher-config.model';
import { ITab } from '@shared/components/tabsPanel/tabs-panel.model';
import { LocaleService } from '@core/services/locale/locale.service';
import { GtmTrackingService } from '@core/services/gtmTracking/gtm-tracking.service';
import { NavigationService } from '@coreModule/services/navigation/navigation.service';

@Component({
  selector: 'switchers',
  templateUrl: 'switchers.component.html'
})

export class SwitchersComponent implements OnInit, OnDestroy, OnChanges {
  @Output() readonly switchAction?: EventEmitter<{ id: string; tab: ITab }> = new EventEmitter();

  @Input() switchIdPropertyName?: string;
  @Input() type: string;
  @Input() switchers: ISwitcherConfig[] | ITab[];
  @Input() filter: any;
  @Input() activeTab: ITab;
  @Input() noPaddings: boolean;
  @Input() detectGTMLocation?: string;
  @Input() preventRouteChange?: boolean;

  readonly maxTabsForEqualColumns: number = 4;

  constructor(
    private locale: LocaleService,
    public router: Router,
    private gtmTrackingService: GtmTrackingService,
    private navigationService: NavigationService
  ) { }

  getTabName(tab: ISwitcherConfig | ITab) {
    const translationName = (tab as ITab).title || tab.label || (tab as ISwitcherConfig).name;
    return translationName && translationName.match(/\./) ? this.locale.getString(translationName)
      : translationName;
  }

  ngOnInit() {
    this.type = this.type || 'regular';
    this.gtmTrackTabName();
  }

  ngOnChanges() {
    this.gtmTrackTabName();
  }

  ngOnDestroy() {
    if (this.detectGTMLocation) {
      this.gtmTrackingService.clearLocation(this.detectGTMLocation);
    }
  }

  trackByLabel(index: number, value: ISwitcherConfig| ITab): string {
    return value.label;
  }

  /**
   * Set Active Class
   * @param {ITab} tab
   * @returns {boolean}
   */
  isActive(tab: ISwitcherConfig | ITab, switcherIndex?: number): boolean {
    if (this.filter || this.filter === 0) {
      return this.filter === (tab as ISwitcherConfig).viewByFilters || +this.filter === +switcherIndex;
    }
    return this.activeTab ? (tab as ITab).id === this.activeTab.id : (tab as ITab).selected;
  }

  /**
   * Tab Function
   * @param {ITab} tab
   * @param {MouseEvent} $event
   */
  clickFunction(tab: ISwitcherConfig | ITab, $event: MouseEvent): void {
    $event.preventDefault();
    this.detectActiveTab(tab);

    if ((tab as ISwitcherConfig).onClick) {
      (tab as ISwitcherConfig).onClick((tab as ISwitcherConfig).viewByFilters);
      return;
    }

    if ((tab as ITab).url && !this.preventRouteChange) {
      this.navigationService.openUrl((tab as ITab).url, true, true);
    }

    if (this.switchAction.observers.length) {
      this.switchAction.emit({ id: tab[this.switchIdPropertyName], tab: (tab as ITab) });
    }
  }

  isAutoSizable() {
    const displayedSwitchersCount = _.filter(this.switchers, (switcher: ISwitcherConfig | ITab) => !(switcher as ITab).hidden).length;
    return displayedSwitchersCount > this.maxTabsForEqualColumns;
  }

  detectActiveTab(tab: ISwitcherConfig | ITab): void {
    if (this.detectGTMLocation && tab) {
      this.gtmTrackingService.setLocation(this.getTabName(tab), this.detectGTMLocation);
    }
  }

  private gtmTrackTabName(): void {
    let activeTab;

    if (!this.detectGTMLocation) {
      return;
    }

    if (this.type === 'links') {
      const filter = this.filter || 0;
      activeTab = this.switchers[filter] || null;
    } else {
      activeTab = _.find(this.switchers, (tab, index) => this.isActive(tab, Number(index)));
    }

    this.detectActiveTab(activeTab);
  }
}
