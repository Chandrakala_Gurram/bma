import { Component, Input, OnChanges } from '@angular/core';
import * as _ from 'underscore';
import { ISurfaceBetModule } from '@shared/models/surface-bet-module.model';
import { ISurfaceBetEvent } from '@shared/models/surface-bet-event.model';

@Component({
  selector: 'surface-bets-carousel',
  templateUrl: './surface-bets-carousel.component.html',
  styleUrls: ['./surface-bets-carousel.component.less']
})
export class SurfaceBetsCarouselComponent implements OnChanges {
  @Input() module: ISurfaceBetModule;

  public slides: number = 0;

  public carouselName: string = _.uniqueId('surface_bet_carousel');

  public trackByCard(index: number, item: ISurfaceBetEvent): string {
    return `${index}_${item.id}`;
  }

  public get isOneCard(): boolean {
    return this.module && this.module.data && this.module.data.length === 1;
  }

  ngOnChanges(): void {
    this.slides = this.module.data.filter(slide => slide.markets && slide.markets.length).length;
  }
}
