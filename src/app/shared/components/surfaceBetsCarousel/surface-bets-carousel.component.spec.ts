import { SurfaceBetsCarouselComponent } from './surface-bets-carousel.component';

describe('SurfaceBetsCarouselComponent', () => {
  let component: SurfaceBetsCarouselComponent;

  beforeEach(() => {
    component = new SurfaceBetsCarouselComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#trackById should return unique id', () => {
    expect(component.trackByCard(1, { id: 31 } as any)).toEqual('1_31');
  });

  it('#isOneCard should return boolean', () => {
    component.module = undefined;
    expect(component['isOneCard']).toBeFalsy();

    component.module = { data: [] } as any;
    expect(component['isOneCard']).toBeFalsy();

    component.module = { data: [{ id: 1 }] } as any;
    expect(component['isOneCard']).toBeTruthy();

    component.module = { data: [{ id: 1 }, { id: 2 }] } as any;
    expect(component['isOneCard']).toBeFalsy();
  });

  describe('ngOnChanges', () => {
    it('should be equal 3', () => {
      component.module = {
        data: [
          { markets: [ 1 ] },
          { markets: [ 1 ] },
          { markets: [ 1 ] },
        ]
      } as any;
      component.ngOnChanges();
      expect(component.slides).toEqual(3);
    });

    it('should be equal 2', () => {
      component.module = {
        data: [
          { markets: [ 1 ] },
          { markets: [] },
          { markets: [ 1 ] },
        ]
      } as any;
      component.ngOnChanges();
      expect(component.slides).toEqual(2);
    });
  });
});
