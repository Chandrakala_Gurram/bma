import {
  ChangeDetectionStrategy,
  Component, Input
} from '@angular/core';

@Component({
  selector: 'cashout-label',
  templateUrl: 'cashout-label.component.html',
  styleUrls: ['cashout-label.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CashoutLabelComponent {
  @Input() mode: string;

  get isBigMode() {
    return !this.mode || this.mode === 'big';
  }
}
