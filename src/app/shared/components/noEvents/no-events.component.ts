import { Component, Input } from '@angular/core';

@Component({
  selector: 'no-events',
  template: '<span [i18n]="message" data-crlat="noEventsFound"></span>',
  styleUrls: ['./no-events.component.less']
})
export class NoEventsComponent {
  @Input() message: string = 'app.noEventsFound';
}
