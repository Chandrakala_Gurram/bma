import { RacingPanelComponent } from '@shared/components/racingPanel/racing-panel.component';

describe('RacingPanelComponent', () => {
  let component: RacingPanelComponent;
  let localeService, router, routingHelperService;

  beforeEach(() => {
    routingHelperService = {
      formEdpUrl: jasmine.createSpy('formEdpUrl'),
      formResultedEdpUrl: jasmine.createSpy('formResultedEdpUrl')
    };
    localeService = {
      getString: jasmine.createSpy()
    };
    router = {
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    };
    component = new RacingPanelComponent(localeService, router, routingHelperService);
  });

  describe('#ngOnInit', () => {
    it('should set titleText => title = ""', () => {
      component.title = '';
      component.ngOnInit();
      expect(component.titleText).toBe('');
    });

    it('should set titleText => title = "Greyville"', () => {
      component.title = 'Greyville';
      component.ngOnInit();
      expect(component.titleText).toBe('Greyville');
    });

    it('should set titleText => title = "Greyville (UK)"', () => {
      component.title = 'Greyville (UK)';
      component.ngOnInit();
      expect(component.titleText).toBe('Greyville <b>(UK)</b>');
    });

    it('should set titleText => title = "racing.event"', () => {
      component.title = 'racing.event';
      component.ngOnInit();
      expect(localeService.getString).toHaveBeenCalled();
    });
  });

  describe('#trackById', () => {
    it('should trackById if id is exist', () => {
      expect(component.trackById(1, { id: '234234' } as any)).toBe('1234234');
    });

    it('should trackById if id is not exist', () => {
      expect(component.trackById(1, {} as any)).toBe('1');
    });
  });

  describe('#goToEvent', () => {
    const event = {
      preventDefault: jasmine.createSpy()
    } as any;

    it('should navigate to to event page', () => {
      spyOn(component, 'formEdpUrl');
      component.goToEvent({} as any, event);
      expect(component.formEdpUrl).toHaveBeenCalledWith({});
      expect(router.navigateByUrl).toHaveBeenCalled();
      expect(event.preventDefault).toHaveBeenCalled();
    });

    it('should emit event to Func is it is exist', () => {
      Object.defineProperty(component.clickFunction, 'observers', { value: [''] });
      spyOn(component.clickFunction, 'emit');
      component.goToEvent({} as any, event);

      expect(component.clickFunction.emit).toHaveBeenCalled();
      expect(router.navigateByUrl).not.toHaveBeenCalled();
      expect(event.preventDefault).toHaveBeenCalled();
    });

  });

  describe('#formEdpUrl', () => {
    it('tote flow', () => {
      component.isTote = true;
      const url = component.formEdpUrl({
        id: 1
      } as any);
      expect(url).toEqual('/tote/event/1');
      expect(routingHelperService.formResultedEdpUrl).not.toHaveBeenCalled();
    });
    it('not tote flow', () => {
      component.formEdpUrl({
        id: 1
      } as any);
      expect(routingHelperService.formResultedEdpUrl).toHaveBeenCalledWith({ id: 1 }, '');
    });

    it('not tote flow with origin', () => {
      component.origin = 'origin';
      component.formEdpUrl({
        id: 1
      } as any);
      expect(routingHelperService.formResultedEdpUrl).toHaveBeenCalledWith({ id: 1 }, '?origin=origin');
    });
  });
});
