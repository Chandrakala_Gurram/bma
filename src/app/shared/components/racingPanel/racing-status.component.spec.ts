import { RacingStatusComponent } from '@shared/components/racingPanel/racing-status.component';

describe('RacingStatusComponent', () => {
  let component: RacingStatusComponent;
  let localeService;

  beforeEach(() => {
    localeService = {
      getString: jasmine.createSpy()
    };
    component = new RacingStatusComponent(localeService);
    component.event = {} as any;
  });

  describe('#ngOnInit', () => {
    it('should set "default" status', () => {
      component.ngOnInit();
      expect(component.status.title).toBe('');
      expect(component.status.class).toBe('');
    });

    it('should set "resulted" status', () => {
      component.event.isResulted = true;
      component.ngOnInit();
      expect(localeService.getString).toHaveBeenCalledWith('racing.result');
      expect(component.status.class).toBe('resulted');
    });

    it('should set "race-off" status', () => {
      component.event.isResulted = false;
      component.event.isLiveNowEvent = false;
      component.event.isStarted = true;
      component.ngOnInit();
      expect(localeService.getString).toHaveBeenCalledWith('racing.raceOff');
      expect(component.status.class).toBe('race-off');
    });

    it('should set "live" status', () => {
      component.event.isResulted = false;
      component.event.isLiveNowEvent = true;
      component.event.isStarted = true;
      component.ngOnInit();
      expect(localeService.getString).toHaveBeenCalledWith('racing.liveNow');
      expect(component.status.class).toBe('live');
    });

    it('should set "race-off" status when event.categoryName === Horse Racing', () => {
      component.event.categoryId = '21';
      component.event.isResulted = false;
      component.event.isStarted = true;
      component.ngOnInit();
      expect(localeService.getString).toHaveBeenCalledWith('racing.raceOff');
      expect(component.status.class).toBe('race-off');
    });
  });
});
