import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { ISportEvent } from '@core/models/sport-event.model';
import { LocaleService } from '@core/services/locale/locale.service';
import { Router } from '@angular/router';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';

@Component({
  selector: 'racing-panel',
  templateUrl: 'racing-panel.component.html',
  styleUrls: ['./racing-panel.component.less']
})
export class RacingPanelComponent implements OnInit {
  @Input() events: ISportEvent[];
  @Input() eventId?: string;
  @Input() title?: string;
  @Input() isShowName?: boolean = true;
  @Input() origin: string = '';
  @Input() isTote: boolean = false;
  @Output() readonly clickFunction?: EventEmitter<{}> = new EventEmitter();

  titleText: string;

  constructor(
    private localeService: LocaleService,
    private router: Router,
    private routingHelperService: RoutingHelperService
  ) { }

  ngOnInit(): void {
    this.titleText = this.title ? this.getTitle(this.title) : '';
  }

  trackById(index: number, event: ISportEvent): string {
    return event.id ? `${index}${event.id}` : index.toString();
  }

  goToEvent(event: ISportEvent, $event: Event): void {
    if (this.clickFunction.observers.length) {
      this.clickFunction.emit(event);
    } else {
      const link = this.formEdpUrl(event);
      this.router.navigateByUrl(link);
    }
    $event.preventDefault();
  }

  formEdpUrl(event: ISportEvent): string {
    let eventUrl = '';
    if (this.isTote) {
      eventUrl += `/tote/event/${event.id}`;
    } else {
      eventUrl += this.routingHelperService.formResultedEdpUrl(event, this.origin ? `?origin=${this.origin}` : '');
    }
    return eventUrl;
  }

  private getTitle(title: string): string {
    const regex = /(\(.*\))/g;
    const match = title.match(regex);
    if (title.match(/\./)) {
      return this.localeService.getString(title);
    } else if (match) {
      return `${title.replace(regex, '')}<b>${match[0]}</b>`;
    }
    return title;
  }
}

