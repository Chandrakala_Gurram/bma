import { Component, Input, OnInit } from '@angular/core';
import { ISportEvent } from '@core/models/sport-event.model';
import { LocaleService } from '@core/services/locale/locale.service';
import environment from '@environment/oxygenEnvConfig';

@Component({
  selector: 'racing-status',
  templateUrl: 'racing-status.component.html',
  styleUrls: ['./racing-status.component.less']
})
export class RacingStatusComponent implements OnInit {
  @Input() event: ISportEvent;

  status: { title: string; class: string} = {
    title: '',
    class: ''
  };
  readonly HORSE_RACING_CATEGORY_ID: string = environment.HORSE_RACING_CATEGORY_ID;
  constructor(private localeService: LocaleService) {}

  ngOnInit(): void {
    if (this.event.isResulted) {
      this.status.title = this.localeService.getString('racing.result');
      this.status.class = 'resulted';
    } else {
      if (this.event.categoryId === this.HORSE_RACING_CATEGORY_ID && this.event.isStarted) {
        this.status.title = this.localeService.getString('racing.raceOff');
        this.status.class = 'race-off';
      } else {
        if (this.event.isStarted && !this.event.isLiveNowEvent) {
          this.status.title = this.localeService.getString('racing.raceOff');
          this.status.class = 'race-off';
        } else if (this.event.isStarted && this.event.isLiveNowEvent) {
          this.status.title = this.localeService.getString('racing.liveNow');
          this.status.class = 'live';
        }
      }
    }
  }
}
