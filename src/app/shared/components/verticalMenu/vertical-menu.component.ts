import { Component, Input, Output, EventEmitter, ViewChild, ViewContainerRef, OnInit, OnDestroy } from '@angular/core';
import * as _ from 'underscore';
import { NavigationService } from '@core/services/navigation/navigation.service';

import { IOddsBoostConfig, IVerticalMenu } from '@app/core/services/cms/models';
import { IMenuActionResult } from '@app/core/services/cms/models/menu/menu-action.model';
import { DYNAMIC_ODDS_BOOST_NOTIFICATION } from '@app/dynamicLoader/dynamic-loader-manifest';
import { DynamicLoaderService } from '@app/dynamicLoader/dynamic-loader.service';
import { ILazyComponent } from '@app/dynamicLoader/dynamic-loader.model';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { ODDS_BOOST_URL } from '@oddsBoost/constants/odds-boost.constant';

@Component({
  selector: 'vertical-menu',
  templateUrl: 'vertical-menu.component.html'
})

export class VerticalMenuComponent implements OnInit, OnDestroy {
  @Input() menuItems: IVerticalMenu[];
  @Input() header: string;
  @Input() showHeader: boolean = false;
  @Input() showDescription: boolean = false;
  @Input() disableDefaultNavigation: boolean = false;

  @Output() readonly itemClickFn: EventEmitter<IVerticalMenu> = new EventEmitter<IVerticalMenu>();

  @ViewChild('oddsBoostCountNotification', {read: ViewContainerRef}) set boostHead(container: ViewContainerRef) {
    if (!container) {
      return;
    }

    this.oddsBoostCountNotificationComponent.viewContainer = container;
    this.dynamicComponentLoader.getDynamicComponent(DYNAMIC_ODDS_BOOST_NOTIFICATION, this.oddsBoostCountNotificationComponent);
  }

  private oddsBoostEnabled: boolean;
  private oddsBoostCountNotificationComponent: ILazyComponent = {} as ILazyComponent;

  constructor(
    private navigationService: NavigationService,
    private dynamicComponentLoader: DynamicLoaderService,
    private cmsService: CmsService,
  ) { }

  ngOnInit(): void {
    this.cmsService.getOddsBoost().subscribe((config: IOddsBoostConfig) => {
      this.oddsBoostEnabled = config && config.enabled;
    });
  }

  ngOnDestroy(): void {
    this.dynamicComponentLoader.destroyDynamicComponent(this.oddsBoostCountNotificationComponent);
  }

  trackByIndex(index: number): number {
    return index;
  }

  /**
   * Menu item click handler
   *
   * @param  {IVerticalMenu} menuItem
   * @returns void
   */
  /**
   * Menu item click handler
   *
   * @param  {IVerticalMenu} menuItem
   * @returns void
   */
  menuItemClick(menuItem: IVerticalMenu): void {
    if (menuItem.action && _.isFunction(menuItem.action)) {
      menuItem.action().subscribe((actionResult: IMenuActionResult) => {
        if (actionResult && !actionResult.cancelled) {
          this.doMenuRouting({ ...menuItem, targetUri: actionResult.redirectUri || menuItem.targetUri });
        }
      });
    } else {
      this.doMenuRouting(menuItem);
    }
  }

  showOddsBoostCount(targetUri: string): boolean {
    return this.oddsBoostEnabled && targetUri.indexOf(ODDS_BOOST_URL) > -1;
  }

  private doMenuRouting(menuItem: IVerticalMenu): void {
    if (this.disableDefaultNavigation) {
      this.itemClickFn.emit(menuItem);
    } else if (menuItem.type !== 'button') {
      this.navigationService.openUrl(menuItem.targetUri, menuItem.inApp);
    }
  }
}
