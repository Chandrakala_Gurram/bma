import { fakeAsync, tick } from '@angular/core/testing';
import { DigitKeyboardComponent } from './digit-keyboard.component';

describe('DigitKeyboardComponent', () => {
  let component: DigitKeyboardComponent;
  let pubSubService;
  let coreToolsService;
  let userService;
  let changeDetectorRef;

  beforeEach(() => {
    changeDetectorRef = {
      detectChanges: jasmine.createSpy('detectChanges')
    };

    coreToolsService = {
      uuid: jasmine.createSpy().and.returnValue('xxxx-xxx-xxxx')
    };
    pubSubService = {
      subscribe: jasmine.createSpy(),
      unsubscribe: jasmine.createSpy(),
      publishSync: jasmine.createSpy(),
      API: {
        DIGIT_KEYBOARD_DEC_DOT_PRESSED: 'DIGIT_KEYBOARD_DEC_DOT_PRESSED',
        DIGIT_KEYBOARD_SHOWN: 'DIGIT_KEYBOARD_SHOWN',
        DIGIT_KEYBOARD_HIDDEN: 'DIGIT_KEYBOARD_HIDDEN',
        DIGIT_KEYBOARD_KEY_PRESSED: 'DIGIT_KEYBOARD_KEY_PRESSED'
      }
    };
    userService = {
      status: false
    };
    component = new DigitKeyboardComponent(
      pubSubService,
      coreToolsService,
      userService,
      changeDetectorRef
    );
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
    expect(component.isDecimalButtonEnabled).toEqual(true);
    expect(component.isDecimalPointPressed).toEqual(false);
    expect(component.isKeyboardShown).toEqual(false);
    expect(component.isQuickDepositButtonsShown).toEqual(false);
    expect(component.quickDepositButtons).toEqual([]);
  });

  describe('#ngOnInit', () => {
    it('should call ngOnInit', () => {
      component['setLabels'] = jasmine.createSpy();
      component.ngOnInit();

      expect(pubSubService.subscribe).toHaveBeenCalledTimes(3);
      expect(component['setLabels']).toHaveBeenCalled();
    });

    it('should set hideKeyboardFlag based on input', () => {
      component.hideKeyboardFlag = false;
      component.ngOnInit();

      expect(component.hideKeyboardFlag).toBeFalsy();
    });

    it('should set hideKeyboardFlag with default value', () => {
      component.ngOnInit();

      expect(component.hideKeyboardFlag).toBeTruthy();
    });
  });

  describe('#ngOnChanges', () => {
    it('should call ngOnChanges', () => {
      component['setLabels'] = jasmine.createSpy();
      component.currency = 'Kr';
      component.ngOnChanges();

      expect(component['setLabels']).toHaveBeenCalled();
    });
  });

  describe('#setLabels', () => {
    it('setLabels with KR', () => {
      component['getLabelData'] = jasmine.createSpy();

      component.currency = 'Kr';
      component['setLabels']();

      expect(component['getLabelData']).toHaveBeenCalledWith([50, 100, 500, 1000] );
    });
  });

  describe('#ngOnDestroy', () => {
    it('should call ngOnDestroy', () => {
      component.ngOnDestroy();

      expect(pubSubService.unsubscribe).toHaveBeenCalledWith('DigitKeyboardComponent-xxxx-xxx-xxxx');
    });
  });

  describe('#onButtonClickHandler', () => {
    it('should call onButtonClickHandler', () => {
      const event = {
        target: {
          dataset: {
            value: '1'
          }
        },
        stopPropagation: jasmine.createSpy('stopPropagation')
      };
      component.onButtonClickHandler((event as any));

      expect(pubSubService.publishSync).toHaveBeenCalledWith('DIGIT_KEYBOARD_KEY_PRESSED', '1');
    });
  });

  describe('#onButtonClickHandler', () => {
    it('should call onButtonClickHandler and isDecimalPointPressed', () => {
      component.isDecimalPointPressed = false;
      component.isDecimalButtonEnabled = true;

      const event = {
        target: {
          dataset: {
            value: '.'
          }
        },
        stopPropagation: jasmine.createSpy('stopPropagation')
      };
      component.onButtonClickHandler((event as any));
      expect(component.isDecimalPointPressed).toEqual(true);

      expect(pubSubService.publishSync).toHaveBeenCalledWith('DIGIT_KEYBOARD_KEY_PRESSED', '.');
    });
  });

  describe('#onButtonClickHandler', () => {
    it('should call onButtonClickHandler and return if isDecimalButtonEnabled is true', () => {
      component.isDecimalButtonEnabled = false;

      const event = {
        target: {
          dataset: {
            value: '.'
          }
        },
        stopPropagation: jasmine.createSpy('stopPropagation')
      };
      component.onButtonClickHandler((event as any));

      expect(pubSubService.publishSync).not.toHaveBeenCalled();
    });
  });

  describe('#getLabelData', () => {
    it('should getLabelData', () => {
      const values = [1, 3, 5, 7];
      expect(component['getLabelData'](values)[2]).toEqual(jasmine.objectContaining({
        label: '+5',
        value: 'qb-5'
      }));
    });

    it('should getLabelData (loggedIn)', () => {
      component['userService'] = <any>{ status: true };
      component['currency'] = '$';

      const values = [1, 3, 5, 7];
      expect(component['getLabelData'](values)[2]).toEqual(jasmine.objectContaining({
        label: '+$5',
        value: 'qb-5'
      }));
    });
  });

  describe('hideKeyboard', () => {
    let notifyEmitter;
    const componentId = '123';

    beforeEach(() => {
      notifyEmitter = jasmine.createSpy('notifyEmitter');

      component['componentId'] = componentId;
      component.keyboardHidden.subscribe(notifyEmitter);
    });

    it('should not handle close if keyboard was not shown', fakeAsync(() => {
      component['isKeyboardShown'] = false;
      component['hideKeyboard'](componentId);
      tick();

      expect(notifyEmitter).not.toHaveBeenCalled();
    }));

    it('should not handle close if passed id is not equal as component\'s id', fakeAsync(() => {
      component['isKeyboardShown'] = true;
      component['hideKeyboard']('12');
      tick();

      expect(notifyEmitter).not.toHaveBeenCalled();
    }));

    it('should handle close if passed id is equal as component\'s id and keyboard was shown', fakeAsync(() => {
      component['isDecimalButtonEnabled'] = false;
      component['isDecimalPointPressed'] = true;
      component['isKeyboardShown'] = true;
      component['hideKeyboard'](componentId);
      tick();

      expect(notifyEmitter).toHaveBeenCalled();
      expect(component['isDecimalButtonEnabled']).toBeTruthy();
      expect(component['isDecimalPointPressed']).toBeFalsy();
      expect(component['isKeyboardShown']).toBeFalsy();
    }));
  });

  describe('showKeyboard', () => {
    let notifyEmitter;
    const componentId = '123';

    beforeEach(() => {
      notifyEmitter = jasmine.createSpy('notifyEmitter');

      component['componentId'] = componentId;
      component.keyboardShown.subscribe(notifyEmitter);
    });

    it('should not handle if passed id is not equal as component\'s id', fakeAsync(() => {
      component['showKeyboard'](false, false, '12');
      tick();

      expect(notifyEmitter).not.toHaveBeenCalled();
    }));

    it('should handle if passed id is equal as component\'s id', fakeAsync(() => {
      const decimalButton = false;
      const quickDepositButton = false;

      component['isDecimalButtonEnabled'] = true;
      component['isQuickDepositButtonsShown'] = true;
      component['isDecimalPointPressed'] = true;
      component['isKeyboardShown'] = false;
      component['showKeyboard'](decimalButton, quickDepositButton, componentId);
      tick();

      expect(notifyEmitter).toHaveBeenCalled();
      expect(component['isDecimalButtonEnabled']).toBeFalsy();
      expect(component['isQuickDepositButtonsShown']).toBeFalsy();
      expect(component['isKeyboardShown']).toBeTruthy();
      expect(component['isDecimalPointPressed']).toBeFalsy();
    }));
  });

  describe('#updateDecimalPointState', () => {
    it('updateDecimalPointState', () => {
      component.isDecimalPointPressed = undefined;
      component['updateDecimalPointState']('', '');
      expect(component.isDecimalPointPressed).toEqual(undefined);
    });

    it('updateDecimalPointState true', () => {
      component.isDecimalPointPressed = undefined;
      component['updateDecimalPointState']('1', '.');
      expect(component.isDecimalPointPressed).toEqual(true);
    });
  });
});
