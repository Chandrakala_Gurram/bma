import {
  Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy,
  OnInit, Output, QueryList, ViewChild, ViewChildren, ChangeDetectorRef
} from '@angular/core';
import * as _ from 'underscore';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { CoreToolsService } from '@root/app/core/services/coreTools/core-tools.service';
import { UserService } from '@core/services/user/user.service';

interface ILabelledValue {
  label: string;
  value: string;
}

@Component({
  selector: 'digit-keyboard',
  templateUrl: 'digit-keyboard.component.html',
  styleUrls: ['digit-keyboard.component.less']
})
export class DigitKeyboardComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild('dkKeyEnter') enterKey: ElementRef<HTMLElement>;
  @ViewChildren('dkKey') keyboardKeys: QueryList<ElementRef<HTMLElement>>;
  @ViewChildren('qsKey') quickStakeKeys: QueryList<ElementRef<HTMLElement>>;

  @Input() currency?: string;
  @Input() componentId: string;
  @Input() hideKeyboardFlag?: boolean;

  @Output() readonly isNotBackspace: EventEmitter<boolean> = new EventEmitter();
  @Output() readonly keyboardShown: EventEmitter<void> = new EventEmitter();
  @Output() readonly keyboardHidden: EventEmitter<void> = new EventEmitter();

  isDecimalButtonEnabled: boolean = true;
  isDecimalPointPressed: boolean = false;
  isKeyboardShown: boolean = false;
  isQuickDepositButtonsShown: boolean = false;
  quickDepositButtons: ILabelledValue[] = [];

  private uuid = `DigitKeyboardComponent-${this.coreToolsService.uuid()}`;

  constructor(
    private pubSubService: PubSubService,
    private coreToolsService: CoreToolsService,
    private userService: UserService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.setLabels();
    this.pubSubService.subscribe(this.uuid,
      this.pubSubService.API.DIGIT_KEYBOARD_DEC_DOT_PRESSED, this.updateDecimalPointState.bind(this));
    this.pubSubService.subscribe(this.uuid,
      this.pubSubService.API.DIGIT_KEYBOARD_SHOWN, this.showKeyboard.bind(this));
    this.pubSubService.subscribe(this.uuid,
      this.pubSubService.API.DIGIT_KEYBOARD_HIDDEN, this.hideKeyboard.bind(this));
    this.hideKeyboardFlag = typeof this.hideKeyboardFlag === 'boolean' ? this.hideKeyboardFlag : true;
  }

  ngOnChanges(): void {
    this.setLabels();
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe(this.uuid);
  }

  /**
   * Handles key press
   * @param {MouseEvent} $event
   */
  onButtonClickHandler($event: MouseEvent): void {
    const value = (<HTMLElement>$event.target).dataset.value;

    if (value === '.') {
      if (!this.isDecimalButtonEnabled) { return; }
      this.isDecimalPointPressed = true;
    }

    this.pubSubService.publishSync(this.pubSubService.API.DIGIT_KEYBOARD_KEY_PRESSED, value);
    $event.stopPropagation();
  }

  private getLabelData(values: number[]): ILabelledValue[] {
    return _.map(values, (value: number): ILabelledValue => ({ label: this.getLabelWithCurrency(value), value: `qb-${value}` }));
  }

  private getLabelWithCurrency(value: number): string {
    return this.userService.status ? `+${this.currency}${value}` : `+${value}`;
  }

  private hideKeyboard(componentId: string): void {
    if (this.isKeyboardShown && (this.componentId === componentId)) {
      this.isDecimalButtonEnabled = true;
      this.isDecimalPointPressed = false;
      this.isKeyboardShown = false;
      this.keyboardHidden.emit();
    }
  }

  private setLabels(): void {
    this.quickDepositButtons = this.getLabelData(this.currency === 'Kr' ? [50, 100, 500, 1000] : [5, 10, 50, 100]);
  }

  private showKeyboard(decimalButton: boolean, quickDepositButton: boolean, componentId: string): void {
    if (this.componentId === componentId) {
      this.isDecimalButtonEnabled = decimalButton;
      this.isDecimalPointPressed = false;
      this.isKeyboardShown = true;
      this.isQuickDepositButtonsShown = quickDepositButton;
      this.keyboardShown.emit();
      this.changeDetectorRef.detectChanges();
    }
  }

  /**
   * Identify if 'dot' button should be disabled
   * @params {array} params
   */
  private updateDecimalPointState(newValue: string, value: string): void {
    if (!newValue && !value) {
      return;
    }
    this.isDecimalPointPressed = (newValue && newValue.indexOf('.') !== -1) || (value === '.');
  }
}
