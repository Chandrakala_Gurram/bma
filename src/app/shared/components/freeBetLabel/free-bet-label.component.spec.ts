import { FreeBetLabelComponent } from './free-bet-label.component';

describe('#FreeBetLabelComponent', () => {
  let component;
  let filtersService;

  beforeEach(() => {
    filtersService = {
      setCurrency: v => `$${v}`
    };

    component = new FreeBetLabelComponent(
      filtersService
    );
  });

  it('should set value variable on selected set', () => {
    component.selected = '5.00';
    expect(component.value).toEqual('$5.00');
  });

  it('should get "value" variable value (lol) on selected get', () => {
    component.value = '$3.00';
    expect(component.selected).toEqual('$3.00');
  });
});
