import { Component, Input } from '@angular/core';
import { FiltersService } from '@core/services/filters/filters.service';

@Component({
  selector: 'free-bet-label',
  templateUrl: './free-bet-label.component.html',
  styleUrls: ['./free-bet-label.less']
})
export class FreeBetLabelComponent {
  value: string;

  @Input() plus: boolean;
  @Input() margin: boolean;
  @Input()
  set selected(val: number | string) {
    this.value = this.filtersService.setCurrency(val);
  }
  get selected() {
    return this.value;
  }

  constructor(private filtersService: FiltersService) {}
}
