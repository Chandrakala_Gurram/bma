import { ListCardComponent } from '@shared/components/listCard/list-card.component';

describe('ListCardComponent', () => {
  let component: ListCardComponent;
  let router;

  const event = {
    preventDefault: jasmine.createSpy()
  } as any;

  beforeEach(() => {
    router = {
      navigateByUrl: jasmine.createSpy()
    };
    component = new ListCardComponent(router);
    component.link = '/horse-racing/featured';
  });

  describe('#gotToPage', () => {

    it('should navigate to page', () => {
      component.gotToPage(event);

      expect(router.navigateByUrl).toHaveBeenCalledWith(component.link);
      expect(event.preventDefault).toHaveBeenCalled();
    });

    it('should emit event to Func is it is exist', () => {
      Object.defineProperty(component.clickFunction, 'observers', { value: [''] });
      spyOn(component.clickFunction, 'emit');
      component.gotToPage(event);

      expect(component.clickFunction.emit).toHaveBeenCalled();
      expect(router.navigateByUrl).not.toHaveBeenCalled();
      expect(event.preventDefault).toHaveBeenCalled();
    });
  });
});
