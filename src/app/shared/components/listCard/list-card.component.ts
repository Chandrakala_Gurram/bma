import {
  Component,
  HostListener,
  Output,
  ViewEncapsulation,
  EventEmitter,
  Input
} from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'list-card',
  templateUrl: 'list-card.component.html',
  styleUrls: ['./list-card.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class ListCardComponent {
  @Input() item: { name: string };
  @Input() link: string;
  @Input() date?: string;
  @Input() title?: string;
  @Output() readonly clickFunction?: EventEmitter<{}> = new EventEmitter();

  constructor(private router: Router) {}

  @HostListener('click', ['$event'])
  gotToPage($event: Event): void {
    if (this.clickFunction.observers.length) {
      this.clickFunction.emit(event);
    } else {
      this.router.navigateByUrl(this.link);
    }
    $event.preventDefault();
  }
}
