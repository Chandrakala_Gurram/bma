import { Component, ViewChild } from '@angular/core';
import { AbstractDialog } from '../oxygenDialogs/abstract-dialog';
import { DeviceService } from '@core/services/device/device.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

@Component({
  selector: 'connection-lost-dialog',
  templateUrl: 'connection-lost-dialog.component.html',
  styleUrls: ['connection-lost-dialog.component.less']
})
export class ConnectionLostDialogComponent extends AbstractDialog {

  @ViewChild('connectionLostDialog') dialog;

  constructor(
    device: DeviceService, windowRef: WindowRefService
  ) {
    super(device, windowRef);
  }
}
