import { Component, Input, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { IVerticalMenu } from '@core/services/cms/models';
import { ITrackEvent } from '@core/services/gtm/models';
import { GtmService } from '@core/services/gtm/gtm.service';
import { UserService } from '@core/services/user/user.service';
import { RetailMenuService } from '@retailModule/services/retailMenu/retail-menu.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'retail-menu',
  templateUrl: 'retail-menu.component.html'
})

export class RetailMenuComponent implements OnInit, OnDestroy {
  @Input() showHeader: boolean = false;
  @Input() showDescription: boolean = false;
  @Input() showCardNumber: boolean = false;
  @Input() trackingLocation: string;

  @Output() readonly itemClick: EventEmitter<IVerticalMenu> = new EventEmitter<IVerticalMenu>();

  public menuItems: IVerticalMenu[] = [];
  public cardMenuItem: Partial<IVerticalMenu>;
  showTopBorder: boolean = false;


  private trackEventData: ITrackEvent = {
    event: 'trackEvent',
    eventCategory: 'navigation',
    eventAction: null,
    eventLabel: null
  };

  private subscription: Subscription;

  constructor(
    private gtmService: GtmService,
    private userService: UserService,
    private retailMenuService: RetailMenuService
  ) { }

  ngOnInit(): void {
    this.subscription = this.retailMenuService.retailMenuItems$.subscribe(data => {
      this.menuItems = data;
      this.showTopBorder = !this.menuItems.length;
      if (this.showCardNumber && !!this.userService.cardNumber && this.userService.isInShopUser()) {
        this.cardMenuItem = {
          title: this.userService.cardNumber,
          svgId: '#retail-card',
        };
      } else {
        this.cardMenuItem = null;
      }
    });
    this.trackEventData.eventAction = this.trackingLocation;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  /** Track menu item clicks in GA.
   * @param  {IVerticalMenu} menuItem
   * @returns void
   */
  trackNavigation(menuItem: IVerticalMenu): void {
    this.trackEventData.eventLabel = menuItem.linkTitle;
    this.gtmService.push('trackEvent', this.trackEventData);
    this.itemClick.emit(menuItem);
  }
}
