import { ViewChangerIconComponent } from '@shared/components/viewChangerIcon/view-changer-icon.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('ViewChangerIconComponent', () => {
  let component: ViewChangerIconComponent,
    pubsub;

  beforeEach(() => {
    pubsub = {
      publishSync: jasmine.createSpy('publishSync'),
      API: pubSubApi
    };

    component = new ViewChangerIconComponent(pubsub);
  });

  it('constructor', () => {
    expect(component['names']).toEqual({
      card: 'list view',
      list: 'card view'
    });
  });

  describe('ngOnInit', () => {
    it('should set name from given viewType', () => {
      component.viewType = 'card';

      component.ngOnInit();

      expect(component.name).toEqual('list view');
    });

    it('should set name from deafult value', () => {
      component.ngOnInit();

      expect(component.name).toEqual('card view');
    });
  });

  describe('viewChange', () => {
    let event;

    beforeEach(() => {
      component.id = 'ViewChangerIconComponent';

      event = {
        stopPropagation: jasmine.createSpy('stopPropagation')
      } as any;
    });

    it('should change view from list to card', () => {
      component.ngOnInit();
      component.viewChange(event);

      expect(component.viewType).toEqual('card');
      expect(component.name).toEqual('list view');
      expect(pubsub.publishSync).toHaveBeenCalledWith(pubsub.API.VIEW_CHANGE, [{
        type: 'card',
        id: 'ViewChangerIconComponent'
      }]);
    });

    it('should change view from card to list', () => {
      component.viewType = 'card';
      component.name = 'list view';

      component.viewChange(event);

      expect(component.viewType).toEqual('list');
      expect(component.name).toEqual('card view');
      expect(pubsub.publishSync).toHaveBeenCalledWith(pubsub.API.VIEW_CHANGE, [{
        type: 'list',
        id: 'ViewChangerIconComponent'
      }]);
    });
  });

  describe('getIconName', () => {
    it('should get name for card view', () => {
      component.ngOnInit();

      expect(component.getIconName).toEqual('#icon-card-view');
    });

    it('should get name for list view', () => {
      component.name = 'list view';

      expect(component.getIconName).toEqual('#icon-list-view');
    });
  });
});
