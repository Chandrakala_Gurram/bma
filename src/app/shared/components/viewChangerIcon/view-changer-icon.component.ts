import { Component, Input, OnInit } from '@angular/core';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'view-changer-icon',
  templateUrl: 'view-changer-icon.component.html'
})
export class ViewChangerIconComponent implements OnInit {

  @Input() id: string;
  @Input() viewType: string;

  name: string;

  private names: { [key: string]: string } = {
    card: 'list view',
    list: 'card view'
  };

  constructor(private pubsub: PubSubService) {}

  ngOnInit() {
    this.name = this.names[this.viewType] || this.names.list;
  }

  /**
   * On click yourcall icon action
   * @param event
   */
  viewChange(event): void {
    event.stopPropagation();

    this.viewType = this.viewType === 'card' ? 'list' : 'card';
    this.name = this.name === this.names.list ? this.names.card : this.names.list;
    this.pubsub.publishSync(this.pubsub.API.VIEW_CHANGE, [{
      type: this.viewType,
      id: this.id
    }]);
  }

  /**
   * Get icon svg icon(card/list view icon)
   * @returns {string}
   */
  get getIconName(): string {
    return `#icon-${this.name === this.names.list ? 'card' : 'list'}-view`;
  }
}

