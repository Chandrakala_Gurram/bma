import { FreeBetsDialogComponent } from './free-bets-dialog.component';

describe('FreeBetsDialogComponent', () => {
  let component: FreeBetsDialogComponent;
  let device;
  let windowRef;

  beforeEach(() => {
    device = {};
    windowRef = {};
    component = new FreeBetsDialogComponent(
      device, windowRef
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('trackByIndex', () => {
    const result = component.trackByIndex(1);

    expect(result).toEqual(1);
  });
});
