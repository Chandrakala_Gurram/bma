import { Component, ViewChild, ChangeDetectionStrategy } from '@angular/core';

import { AbstractDialog } from '../oxygenDialogs/abstract-dialog';
import { DeviceService } from '@core/services/device/device.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

@Component({
  selector: 'free-bet-dialog',
  templateUrl: 'free-bets-dialog.component.html',
  styleUrls: ['./free-bets-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FreeBetsDialogComponent extends AbstractDialog {

  @ViewChild('freeBetDialog') dialog;

  constructor(
    device: DeviceService, windowRef: WindowRefService
  ) {
    super(device, windowRef);
  }

  /**
   * ngFor trackBy function
   * @param {number} index
   * @return {number}
   */
  trackByIndex(index: number): number {
    return index;
  }
}
