import { Injectable } from '@angular/core';

import { SPORT_MATCHES, SPORT_COMPETITIONS, SPORT_INPLAY } from '@platform/shared/components/marketSelector/market-selector.constant';

import { IMarketSelectorConfig } from '@shared/components/marketSelector/market-selector.model';

@Injectable()
export class MarketSelectorConfigService {
  sportInplay: IMarketSelectorConfig[];
  sportMatches: IMarketSelectorConfig[];
  sportCompetition: IMarketSelectorConfig[];

  private SPORT_MATCHES = SPORT_MATCHES;
  private SPORT_COMPETITIONS = SPORT_COMPETITIONS;
  private SPORT_INPLAY = SPORT_INPLAY;
  constructor() {
    this.sportMatches = this.SPORT_MATCHES;
    this.sportCompetition = this.SPORT_COMPETITIONS;
    this.sportInplay = this.SPORT_INPLAY;
  }
}
