import { InplayMarketSelectorComponent } from '@shared/components/marketSelector/inplayMarketSelector/inplay-market-selector.component';

describe('InplayMarketSelectorComponent', () => {
  let component: InplayMarketSelectorComponent;
  let marketSelectorConfigService;
  let marketSelectorTrackingService;

  beforeEach(() => {
    marketSelectorConfigService = {
      selectorType: [{
        SPORT_ID: 234,
        MARKETS_NAMES: {
          1: '1',
          2: '2',
          3: '3',
          4: '4'
        }
      }]
    };
    marketSelectorTrackingService = {
      pushToGTM: jasmine.createSpy('pushToGTM')
    };

    component = new InplayMarketSelectorComponent(
      marketSelectorConfigService,
      marketSelectorTrackingService
    );
    component.selectorType = 'selectorType';
    component.marketSelectorOptions = ['1', '2', '3', '4'];
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  describe('#ngOnChanges', () => {
    it('check of market filter and marketFilterText', () => {
      component.SELECTOR_DATA = marketSelectorConfigService.selectorType[0];
      component.selectedMarketName.emit = jasmine.createSpy();
      component.selectOptions = [{ name: '1', text: '1' }, { name: '2', text: '2' }, { name: '3', text: '3' }, { name: '4', text: '4' }];
      const changes = {
        marketSelectorOptions: {
          currentValue: {},
          previousValue: {},
          firstChange: true
        },
        eventDataSection: {
          currentValue: { marketSelector: '2' },
          previousValue: { marketSelector: '1' },
          firstChange: true
        }
      };
      component.ngOnChanges(changes as any);

      expect(component.marketFilter).toEqual('2');
      expect(component.marketFilterText).toEqual('2');
      expect(component.selectedMarketName.emit).toHaveBeenCalledWith('2');
    });
  });

  it('ngOnInit with options', () => {
    component.selectedMarketName.emit = jasmine.createSpy();
    component.sportId = 234;
    component.ngOnInit();
    expect(component.selectedMarketName.emit).toHaveBeenCalledWith('1');
    expect(component.marketFilter).toEqual('1');
    expect(component.marketFilterText).toEqual('1');
  });

  it('ngOnInit without options', () => {
    component.marketSelectorOptions = [];
    component.ngOnInit();

    expect(component.marketFilter).toBeUndefined();
    expect(component.marketFilterText).toEqual('');
  });

  it('selector data should be null', () => {
    component.selectorType = 'footballTest';
    component['createOptionsList'] = jasmine.createSpy('createOptionsList');
    component.ngOnInit();
    expect(component.SELECTOR_DATA).toBe(null);
  });
  describe('filterMarkets', () => {
    it('should emit market name', () => {
      component.SELECTOR_DATA = marketSelectorConfigService.selectorType[0];
      component.selectedMarketName.emit = jasmine.createSpy();
      component.filterMarkets('someMarket');
      expect(component.selectedMarketName.emit).toHaveBeenCalledWith('someMarket');
    });
  });
  if (this.marketFilter && this.marketSelectorOptions.indexOf(this.marketFilter) === -1) {
    this.marketFilter = this.marketSelectorOptions[0];
    this.filterMarkets(this.marketFilter);
    this.selectedMarketName.emit(this.marketFilter);
    return;
  }

  describe('initSelectorOptions', () => {
    it('should emit market name', () => {
      component.SELECTOR_DATA = marketSelectorConfigService.selectorType[0];
      component.selectedMarketName.emit = jasmine.createSpy();
      component.marketFilter = 'someMarketFilter';
      component.marketSelectorOptions = ['someDifferentMarketFilter'];
      component['initSelectorOptions']();
      expect(component.selectedMarketName.emit).toHaveBeenCalledWith('someDifferentMarketFilter');
    });
  });
});
