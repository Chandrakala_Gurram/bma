import {
  Component,
  EventEmitter,
  Input, OnChanges,
  OnInit,
  Output, SimpleChanges
} from '@angular/core';

import { MarketSelectorConfigService } from '@sharedModule/components/marketSelector/market-selector-config.service';
import { MarketSelectorTrackingService } from '@sharedModule/components/marketSelector/market-selector-tracking.service';

import {
  IMarketSelectorConfig,
  IMarketSelectorOption,
  IReloadData
} from '../market-selector.model';
import { ITypeSegment } from '@app/inPlay/models/type-segment.model';

@Component({
  selector: 'inplay-market-selector',
  templateUrl: 'inplay-market-selector.component.html'
})
export class InplayMarketSelectorComponent implements OnInit, OnChanges {
  @Input() marketSelectorOptions: string[];
  @Input() eventDataSection: ITypeSegment;
  @Input() selectorType: string;
  @Input() sportId: number;

  @Output() readonly reloadData: EventEmitter<IReloadData> = new EventEmitter();
  @Output() readonly selectedMarketName: EventEmitter<string> = new EventEmitter();

  SELECTOR_DATA: IMarketSelectorConfig;

  marketFilter: string;
  marketFilterText: string;
  selectOptions: IMarketSelectorOption[];

  constructor(
    private marketSelectorConfigService: MarketSelectorConfigService,
    private marketSelectorTrackingService: MarketSelectorTrackingService
  ) { }

  /**
   *  Add listeners and initialize market selector
   * @private
   */
  ngOnInit(): void {
    this.SELECTOR_DATA = this.selectorData();
    this.initSelectorOptions();

    if (this.eventDataSection &&
      this.eventDataSection.marketSelector && this.marketSelectorOptions) {
      this.filterMarkets(this.marketSelectorOptions[0]);
    }

    this.marketFilter = this.marketSelectorOptions[0];

    // show option text in wrapped market selector in separate block
    this.marketFilterText = this.setSelectedOptionText(this.selectOptions, this.marketFilter);
    this.selectedMarketName.emit(this.marketFilter);
  }

  ngOnChanges(changes: SimpleChanges): void  {
    const isChanges = changes.eventDataSection.currentValue && changes.eventDataSection.previousValue !== undefined &&
      (changes.eventDataSection.currentValue.marketSelector !== changes.eventDataSection.previousValue.marketSelector);
    const isMarketChanged = changes.eventDataSection.currentValue && changes.eventDataSection.previousValue !== undefined &&
      (changes.eventDataSection.currentValue.marketSelector !== changes.eventDataSection.previousValue.marketSelector);
    if (isMarketChanged) {
      this.marketFilter = changes.eventDataSection.currentValue.marketSelector;
      this.selectedMarketName.emit(this.marketFilter);
      // show option text in wrapped market selector in separate block
      this.marketFilterText = this.setSelectedOptionText(this.selectOptions, this.marketFilter);
    }
    if (isChanges) {
      this.initSelectorOptions();
    }
  }

  trackByIndex(index: number): string {
    return index.toString();
  }

  setSelectedOptionText(selectOptions: IMarketSelectorOption[], marketFilter: string): string {
    const selectedOption = selectOptions &&
      selectOptions.filter((option: IMarketSelectorOption) => option.name === marketFilter);
    return (selectedOption && selectedOption[0] && selectedOption[0].text) || '';
  }

  /**
   * Apply new filter and reload sports data from server.
   * @param {string} templateMarketName
   */
  filterMarkets(templateMarketName: string): void {
    this.marketFilter = templateMarketName;
    const trackingMarketName = this.SELECTOR_DATA.MARKETS_NAMES[templateMarketName];
    this.reloadData.emit({
      useCache: false,
      additionalParams : {
        marketSelector: templateMarketName
      }
    });
    this.selectedMarketName.emit(this.marketFilter);
    this.marketSelectorTrackingService.pushToGTM(trackingMarketName, this.sportId);
  }

  /**
   * init/reinit options data to build select element
   * @private
   */
  private initSelectorOptions(): void {
    const sortedMarketNamesArray = this.marketSelectorOptions;

    if (this.selectOptions && !sortedMarketNamesArray) {
      return;
    }

    if (this.marketFilter && this.marketSelectorOptions.indexOf(this.marketFilter) === -1) {
      this.marketFilter = this.marketSelectorOptions[0];
      this.filterMarkets(this.marketFilter);
      this.selectedMarketName.emit(this.marketFilter);
      return;
    }

    this.selectOptions = this.createOptionsList(sortedMarketNamesArray);
  }

  /**
   * Create data for Selector Options
   * @returns {Array}
   * @param {Array} marketNamesArray
   */
  private createOptionsList(marketNamesArray: string[]): IMarketSelectorOption[] {
    return marketNamesArray.map(elem => {
      return {
        name: elem,
        text: this.SELECTOR_DATA.MARKETS_NAMES[elem]
      };
    });
  }
  /**
   * @returns IMarketSelectorConfig
   */
  private selectorData(): IMarketSelectorConfig {
    return this.marketSelectorConfigService[this.selectorType] &&
      this.marketSelectorConfigService[this.selectorType].length > 0 ?
      this.marketSelectorConfigService[this.selectorType].filter(item => item.SPORT_ID === this.sportId)[0] : null;
  }
}
