import { Injectable } from '@angular/core';
import { Router, Event, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { RoutingState } from '@sharedModule/services/routingState/routing-state.service';

@Injectable()
export class MarketSelectorStorageService {
  marketSelectorSport: string = 'football';
  selectorStoredData: { [key: string]: { selectedOption: string } } = {};
  removeRouteChangeListener: Subscription;

  private isListenForClean: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private routingState: RoutingState
  ) {
  }

  /**
   * Save selected option
   * @param sportName
   * @param optionValue
   */
  storeSelectedOption(sportName: string, optionValue: string): void {
    this.selectorStoredData[sportName] = {
      selectedOption: optionValue
    };

    this.addCleanUpListener();
  }

  /**
   * restore data to set active option
   * @param sportName
   * @returns {String}
   */
  restoreSelectedOption(sportName: string): string {
    return (this.selectorStoredData[sportName] && this.selectorStoredData[sportName].selectedOption) || '';
  }

  /**
   * URL validation for cleaning data.
   */
  private isValidFootballLocation(): boolean {
    return this.routingState.getRouteParam('sport', this.route.snapshot) === 'football' &&
      (this.routingState.getRouteParam('tab', this.route.snapshot) === 'today' ||
        this.routingState.getRouteParam('tab', this.route.snapshot) === 'tomorrow' ||
        this.routingState.getRouteParam('tab', this.route.snapshot) === 'future');
  }

  /**
   * Clean up stored data if user navigates to different page
   */
  private cleanUpStoredData(): void {
    delete this.selectorStoredData[this.marketSelectorSport];
  }

  /**
   * Check page for cleaning up stored data
   */
  private checkRouteForCleanUpData(): void {
    if (!this.isValidFootballLocation()) {
      this.cleanUpStoredData();
      this.removeCleanUpListener();
    }
  }

  /**
   * Start listen user navigation
   */
  private addCleanUpListener(): void {
    if (!this.isListenForClean) {
      this.isListenForClean = true;
      this.removeRouteChangeListener = this.router.events.subscribe((event: Event) => {
        if (event instanceof NavigationEnd) {
          this.checkRouteForCleanUpData();
        }
      });
    }
  }

  /**
   * remove navigation listener after deleting stored data
   */
  private removeCleanUpListener(): void {
    if (this.isListenForClean) {
      this.isListenForClean = false;
      this.removeRouteChangeListener.unsubscribe();
    }
  }
}
