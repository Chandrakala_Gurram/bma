import { MatchesMarketSelectorComponent } from './matches-market-selector.component';
import { ICouponMarketSelector } from '../market-selector.model';
import { IMarket } from '@core/models/market.model';

describe('MatchesMarketSelectorComponent', () => {
  let component: MatchesMarketSelectorComponent;
  let filtersService;
  let marketSelectorConfigService;
  let pubSubService;
  let marketSelectorTrackingService;
  let marketSelectorStorageService;

  const typeSegment = [{
    typeId: 971,
    events: [{
      id: 5112681,
      markets: [{
        templateMarketName: 'Match Result',
        outcomes: []
      }, {
        templateMarketName: 'To Qualify',
        outcomes: []
      }]
    }, {
      id: 3567838,
      markets: [{
        templateMarketName: 'Match Result',
        outcomes: []
      }]
    }]
  }] as any;

  const defaultSelectorData = [{
    SPORT_ID: 16,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result',
      'To Qualify',
      'Total Goals Over/Under 2.5',
      'Both Teams to Score',
      'To Win and Both Teams to Score',
      'Draw No Bet',
      'First-Half Result'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Both Teams to Score': 'Both Teams to Score',
      'Total Goals Over/Under 2.5': 'Total Goals Over/Under 2.5',
      'Draw No Bet': 'Draw No Bet',
      'To Win and Both Teams to Score': 'To Win & Both Teams to Score',
      'First-Half Result': '1st Half Result',
      'To Qualify': 'To Qualify'
    }
  }];

  beforeEach(() => {
    filtersService = {
      getTeamName: jasmine.createSpy().and.returnValue('dynamo')
    };
    marketSelectorConfigService = {
      footballCoupons: defaultSelectorData,
      footballInplay: defaultSelectorData,
      modifyTemplateName: jasmine.createSpy(),
      getMarketOptions: jasmine.createSpy()
    };
    pubSubService = {
      API: {
        DELETE_EVENT_FROM_CACHE: 'DELETE_EVENT_FROM_CACHE',
        DELETE_MARKET_FROM_CACHE: 'DELETE_MARKET_FROM_CACHE'
      },
      subscribe: jasmine.createSpy('subscribe').and.callFake((domain, channel, fn) => fn && fn()),
      unsubscribe: jasmine.createSpy('unsubscribe')
    };
    marketSelectorTrackingService = {
      pushToGTM: jasmine.createSpy('pushToGTM')
    };
    marketSelectorStorageService = {
      storeSelectedOption: jasmine.createSpy(),
      restoreSelectedOption: jasmine.createSpy()
    };

    component = new MatchesMarketSelectorComponent(
      pubSubService,
      marketSelectorConfigService,
      marketSelectorTrackingService,
      marketSelectorStorageService,
      filtersService
    );

    component.selectorType = 'footballCoupons';
    component.multipleEventsDataSections = typeSegment;
  });

  describe('@ngOnInit', () => {
    it('should load selector for Football Coupons', () => {
      component.marketOptions = [{
        title: 'Match Result',
        templateMarketName: 'Match Betting',
        header: ['Home', 'Draw', 'Away']
      }, {
        title: 'Both Teams to Score',
        templateMarketName: 'Both Teams to Score',
        header: ['Yes', 'No']
      }];
      component.selectorType = 'footballCoupons';
      component.sportId = '16';
      component.ngOnInit();
      expect(pubSubService.subscribe).toHaveBeenCalledWith(
        'MarketSelector', ['DELETE_EVENT_FROM_CACHE', 'DELETE_MARKET_FROM_CACHE'], jasmine.any(Function));
      expect(component.selectOptions).toEqual(component.marketOptions);
    });

    it('should call pubsub and connect callbacks', () => {
      component['initData'] = jasmine.createSpy('initData');

      component.ngOnInit();

      expect(component['initData']).toHaveBeenCalledTimes(2);
    });

    it('should load selector for Inplay', () => {
      component.selectorType = 'footballInplay';
      component.sportId = '16';
      component.ngOnInit();
      const selectOptions = [{
        templateMarketName: 'Match Result',
        title: 'Match Result'
      },
      {
        templateMarketName: 'To Qualify',
        title: 'To Qualify'
      }];
      expect(pubSubService.subscribe).toHaveBeenCalledWith(
        'MarketSelector', ['DELETE_EVENT_FROM_CACHE', 'DELETE_MARKET_FROM_CACHE'], jasmine.any(Function));
      expect(component.selectOptions).toEqual(selectOptions);
    });

    it('selector data should be null', () => {
      component.selectorType = 'footballTest';
      component['initData'] = jasmine.createSpy('initData');
      component.ngOnInit();
      expect(component.SELECTOR_DATA).toBe(null);
    });
  });

  describe('@ngOnDestroy', () => {
    it('should unsubscribe from Events', () => {
      component.ngOnDestroy();
      expect(pubSubService.unsubscribe).toHaveBeenCalled();
    });
  });

  it('should create a component', () => {
    expect(component).toBeTruthy();
  });

  it('trackById should return a string', () => {
    const mockLeg = { id: '1', title: 'test' } as ICouponMarketSelector;
    const result = component.trackById(1, mockLeg);

    expect(result).toBe('11');
  });

  describe('#modifyMarket', () => {
    it('it should change templateMarketName if rawHandicapValue is exist in templateMarketName and sport is football', () => {
      component.sportName = 'football';
      const market = {
        templateMarketName: 'Over/Under Total Goals',
        rawHandicapValue: '1.5'
      } as IMarket;
      const resultMarket = {
        rawHandicapValue: '1.5',
        templateMarketName: 'Over/Under Total Goals 1.5'
      } as IMarket;
      const event = { categoryName: 'football' } as any;

      expect(component['modifyMarket'](market, event)).toEqual(resultMarket);
    });

    it('it should change templateMarketName if rawHandicapValue is exist in templateMarketName and sport is not football', () => {
      component.sportName = 'football';
      const market = {
        templateMarketName: 'Over/Under Total Goals',
        rawHandicapValue: '1.5'
      } as IMarket;
      const resultMarket = {
        rawHandicapValue: '1.5',
        templateMarketName: 'Over/Under Total Goals'
      } as IMarket;
      const event = { categoryName: 'cricket' } as any;

      expect(component['modifyMarket'](market, event)).toEqual(resultMarket);
    });
  });

  describe('filterMarkets', () => {
    it('filterMarkets should filter data by market', () => {
      spyOn(component.hideEnhancedSection, 'emit');
      component.sportName = 'Football';
      component.sportId = '111';
      component.selectOptions = [{
        title: 'Match Result',
        templateMarketName: 'Match Betting',
        header: ['Home', 'Draw', 'Away']
      }, {
        title: 'Both Teams to Score',
        templateMarketName: 'Both Teams to Score',
        header: ['Yes', 'No']
      }];

      component.selectorType = 'footballCoupons';
      component.SELECTOR_DATA = marketSelectorConfigService.footballCoupons[0];
      component.filterMarkets('Match Betting,Both Teams to Score');

      expect(marketSelectorStorageService.storeSelectedOption).toHaveBeenCalledWith('Football', 'Match Betting');
      expect(marketSelectorTrackingService.pushToGTM).toHaveBeenCalledWith('Both Teams to Score', '111');
      expect(component.hideEnhancedSection.emit).toHaveBeenCalled();
      expect(component.marketFilterText).toEqual('Match Result');
    });


    it(`should work with array of strings`, () => {
      const filters = ['Match Betting', 'Both Teams to Score'];
      component.selectOptions = [];
      spyOn(component.filterChange, 'emit');
      component.SELECTOR_DATA = marketSelectorConfigService.footballCoupons[0];
      component.filterMarkets(filters);

      expect(component.filterChange.emit).toHaveBeenCalledWith(filters[0]);
      expect(component.marketFilterText).toEqual('');
    });

    it(`should work with array of strings with only one item`, () => {
      const filters = ['Match Result'];
      component.selectOptions = [];
      component.sportId = '111';

      spyOn(component.filterChange, 'emit');
      component.SELECTOR_DATA = marketSelectorConfigService.footballCoupons[0];
      component.filterMarkets(filters);

      expect(component.filterChange.emit).toHaveBeenCalledWith(filters[0]);
      expect(component['marketSelectorTrackingService'].pushToGTM).toHaveBeenCalledWith(filters[0], component.sportId);
      expect(component.marketFilterText).toEqual('');
    });
  });
});
