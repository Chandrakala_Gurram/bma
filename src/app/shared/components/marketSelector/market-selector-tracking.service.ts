import { Injectable } from '@angular/core';

import { GtmService } from '@core/services/gtm/gtm.service';

@Injectable()
export class MarketSelectorTrackingService {

  constructor(
    private gtmService: GtmService
  ) {}

  /**
   * Send market selector data to GA
   * @param {string} marketName
   * @param {string} eventCategory
   * @param {number} sportId
   */
  pushToGTM(marketName, sportId) {
    const data = {
      eventCategory: 'market selector',
      eventAction: 'change market',
      eventLabel: marketName,
      categoryID: sportId
    };

    this.gtmService.push('trackEvent', data);
  }
}
