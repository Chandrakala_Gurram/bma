import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'angular-custom-modal';

// TODO move these components to @shared dir
import { RacingOutcomeCardComponent } from '@racing/components/racingOutcomeCard/racing-outcome-card.component';
import { NextRacesModuleComponent } from '@racing/components/nextRaces/next-races.component';
import { ListCardComponent } from '@sharedModule/components/listCard/list-card.component';
import { WatchLabelComponent } from '@sharedModule/components/watchLabel/watch-label.component';
import { LiveLabelComponent } from '@sharedModule/components/liveLabel/live-label.component';
import { NewLabelComponent } from '@sharedModule/components/newLabel/new-label.component';
import { TimeFormSelectionSummaryComponent } from '@racing/components/timeformSummary/time-form-selection-summary.component';
import { SeeAllLinkComponent } from '@sharedModule/components/seeAllLink/see-all-link.component';
import { RacingGridComponent } from '@sharedModule/components/racingGrid/racing-grid.component';
import { QuickbetPanelWrapperComponent } from '@sharedModule/components/quickbetPanelWrapper/quickbet-panel-wrapper.component';
import { SvgListComponent } from '@sharedModule/components/svgList/svg-list.component';
import {
  FavouritesAddAllButtonComponent
} from '@sharedModule/components/favourites/components/addAllButton/favourites-add-all-button.component';
import { FavouritesAddButtonComponent } from '@sharedModule/components/favourites/components/addButton/favourites-add-button.component';
import { FavouriteIconComponent } from '@shared/components/favourites/components/favourite-icon/favourite-icon.component';
import { FavouritesCounterComponent } from '@sharedModule/components/favourites/components/favouritesCounter/favourites-counter.component';
import { RaceTimerComponent } from '@sharedModule/components/raceTimer/race-timer.component';
import { RaceCardComponent } from '@sharedModule/components/raceCard/race-card.component';
import { FreeBetsDialogComponent } from '@sharedModule/components/freeBetsDialog/free-bets-dialog.component';
import { FreeBetsNotificationComponent } from '@sharedModule/components/freeBetsNotification/free-bets-notification.component';
import { FreeBetLabelComponent } from '@sharedModule/components/freeBetLabel/free-bet-label.component';
import { ToggleSwitchComponent } from '@sharedModule/components/toggleSwitch/toggle-switch.component';
import { ViewChangerIconComponent } from '@sharedModule/components/viewChangerIcon/view-changer-icon.component';
import { AzSportsPageComponent } from '@sharedModule/components/azSportPage/az-sports-page.component';
import { AccaNotificationComponent } from '@sharedModule/components/accaNotification/acca-notification.component';
import { AccordionComponent } from '@sharedModule/components/accordion/accordion.component';
import { AccordionService } from '@sharedModule/components/accordion/accordion.service';
import { ModuleDisabledComponent } from '@sharedModule/components/moduleDisabled/module-disabled.component';
import { CustomSelectComponent } from '@sharedModule/components/customSelect/custom-select.component';
import { HistoricPricesComponent } from '@sharedModule/components/historicPrices/historic-prices.component';
import { LiveClockComponent } from '@sharedModule/components/liveClock/live-clock.component';
import { LiveEventClockProviderService } from '@sharedModule/components/liveClock/live-event-clock-provider.service';
import { PriceOddsButtonAnimationService } from '@sharedModule/components/priceOddsButton/price-odds-button.animation.service';
import { PriceOddsButtonService } from '@sharedModule/components/priceOddsButton/price-odds-button.service';
import { PriceOddsButtonComponent } from '@sharedModule/components/priceOddsButton/price-odds-button.component';
import { PriceOddsButtonOnPushComponent } from '@shared/components/priceOddsButtonOnPush/price-odds-button-onpush.component';
import { PriceOddsValueDirective } from '@sharedModule/components/priceOddsButton/price-odds-value.directive';
import { PriceOddsDisabledDirective } from '@sharedModule/components/priceOddsButton/price-odds-disabled.directive';
import { PriceOddsClassDirective } from '@sharedModule/components/priceOddsButton/price-odds-class.directive';
import { OddsCardHeaderComponent } from '@sharedModule/components/oddsCardHeader/odds-card-header.component';
import { OddsCardHeaderService } from '@sharedModule/components/oddsCardHeader/odds-card-header.service';
import { StaticBlockComponent } from '@sharedModule/components/staticBlock/static-block.component';
import { SwitchersComponent } from '@sharedModule/components/switchers/switchers.component';
import { DatePickerComponent } from '@sharedModule/components/datePicker/date-picker.component';
import { LocaleDirective } from '@sharedModule/directives/locale.directive';
import { RaceListComponent } from '@sharedModule/components/raceList/race-list.component';
import { RaceGridComponent } from '@sharedModule/components/raceGrid/race-grid';
import { RequestErrorComponent } from '@sharedModule/components/requestError/request-error.component';
import { ShowMoreComponentComponent } from '@sharedModule/components/showMore/show-more.component';
import { ExpandPanelComponent } from '@sharedModule/components/expandPanel/expand-panel.component';
import { LoadingOverlayComponent } from '@sharedModule/components/loadingOverlay/loading-overlay.component';
import { ModuleRibbonComponent } from '@sharedModule/components/moduleRibbon/module-ribbon.component';
import { OxygenDialogComponent } from '@sharedModule/components/oxygenDialogs/oxygen-dialog.component';
import { ConnectionLostDialogComponent } from '@sharedModule/components/connectionLostDialog/connection-lost-dialog.component';
import { SessionLogoutDialogComponent } from '@sharedModule/components/sessionLogoutDialog/session-logout-dialog.component';
import { BppErrorDialogComponent } from '@sharedModule/components/bppErrorDialog/bpp-error-dialog.component';
import { InformationDialogComponent } from '@sharedModule/components/informationDialog/information-dialog.component';
import { RetailMenuComponent } from '@sharedModule/components/retailMenu/retail-menu.component';
import { MarketTypeService } from '@sharedModule/services/marketType/market-type.service';
import { TemplateService } from '@sharedModule/services/template/template.service';
import { CarouselService } from '@sharedModule/directives/ng-carousel/carousel.service';
import { RoutingState } from '@sharedModule/services/routingState/routing-state.service';

import {
  OddsCardEnhancedMultiplesComponent
} from '@sharedModule/components/oddsCard/oddsCardEnhancedMultiples/odds-card-enhanced-multiples.component';
import {
  OddsCardHighlightCarouselComponent
} from '@sharedModule/components/oddsCard/oddsCardHightlightCarousel/odds-card-highlight-carousel.component';
import { OddsCardSurfaceBetComponent } from '@sharedModule/components/oddsCard/oddsCardSurfaceBet/odds-card-surface-bet.component';
import { OddsCardSpecialsComponent } from '@sharedModule/components/oddsCard/oddsCardSpecials/odds-card-specials.component';
import { OddsCardSportComponent } from '@sharedModule/components/oddsCard/oddsCardSport/odds-card-sport.component';
import { OddsCardComponent } from '@sharedModule/components/oddsCard/odds-card.component';
import { NgCarouselDirective } from '@sharedModule/directives/ng-carousel/carousel.directive';
import { PatternRestrictDirective } from '@sharedModule/directives/pattern-restrict.directive';
import { ScrollableDirective } from '@sharedModule/directives/scrollable.directive';
import { ScrollableRacingDirective } from '@sharedModule/directives/scrollable-racing.directive';
import { BackButtonDirective } from '@sharedModule/directives/back-button.directive';
import { ClickLinkDirective } from '@sharedModule/directives/click-link.directive';
import { TriggerDirective } from '@sharedModule/directives/trigger.directive';
import { DisableDraggingDirective } from '@sharedModule/directives/disable-dragging.directive';
import { LazyRenderDirective } from '@sharedModule/directives/lazy-render.directive';
import { LastMadeBetDirective } from '@sharedModule/directives/last-made-bet.directive';
import { HomeScreenComponent } from '@sharedModule/components/homeScreen/home-screen.component';
import { OverscrollFixDirective } from '@sharedModule/directives/overscroll-fix';
import { NgInfoPanelComponent } from '@sharedModule/components/infoPanel/ng-info-panel.component';
import { InputValueDirective } from '@sharedModule/directives/input-value.directive';
import { ScrollFixDirective } from '@sharedModule/directives/scroll-fix.directive';
import { LiveServIframeComponent } from '@sharedModule/components/liveServ/live-serv-iframe.component';
import { EqualColumnDirective } from '@sharedModule/directives/equal-column.directive';
import { MaintenanceComponent } from '@sharedModule/components/maintenance/maintenance.component';
import { ShowAllButtonComponent } from '@sharedModule/components/showAllButton/show-all-button.component';
import { TopBarComponent } from '@sharedModule/components/topBar/top-bar.component';
import { VerticalMenuComponent } from '@sharedModule/components/verticalMenu/vertical-menu.component';
import {
  TabsPanelComponent
} from '@sharedModule/components/tabsPanel/tabs-panel.component';
import {
  OxygenDialogContainerDirective
} from '@sharedModule/directives/oxygen-dialog-container.directive';
// import {
//   InplayMarketSelectorComponent
// } from '@sharedModule/components/marketSelector/inplayMarketSelector/inplay-market-selector.component';
// import {
//   MatchesMarketSelectorComponent
// } from '@sharedModule/components/marketSelector/matchesMarketSelector/matches-market-selector.component';
import {
  MarketSelectorStorageService
} from '@sharedModule/components/marketSelector/matchesMarketSelector/market-selector-storage.service';
import { MarketSelectorTrackingService } from '@sharedModule/components/marketSelector/market-selector-tracking.service';
import { MarketSelectorConfigService } from '@sharedModule/components/marketSelector/market-selector-config.service';
import { DigitKeyboardComponent } from '@sharedModule/components/digitKeyboard/digit-keyboard.component';
import { DigitKeyboardInputDirective } from '@sharedModule/components/digitKeyboard/digit-keyboard-input.directive';
import { ScoreDigitComponent } from '@sharedModule/components/scoreDigit/score-digit.component';
import { SeoStaticBlockComponent } from '@sharedModule/components/seoStaticBlock/seo-static-block.component';
import { FooterMenuComponent } from '@sharedModule/components/footerMenu/footer-menu.component';
import { RacingStatusComponent } from '@sharedModule/components/racingPanel/racing-status.component';
import { RacingPanelComponent } from '@sharedModule/components/racingPanel/racing-panel.component';

import { BetslipCounterComponent } from '@sharedModule/components/betslipCounter/betslip-counter.component';
import { AbstractOutletComponent } from '@sharedModule/components/abstractOutlet/abstract-outlet.component';
import { OutletStatusComponent } from '@sharedModule/components/outletStatus/outlet-status.component';
import { SidebarComponent } from '@sharedModule/components/sidebar/sidebar.component';
import { WatchFreeInfoDialogComponent } from '@sharedModule/components/watchFreeInformationDialog/watch-free-info-dialog.component';
import { OddsCardResultComponent } from '@sharedModule/components/oddsCardResult/odds-card-result.component';

import { PromotionsComponent } from '@app/promotions/components/promotion/promotions.component';
import { PromotionDialogComponent } from '@app/promotions/components/promotionDialog/promotion-dialog.component';
import { PromotionOverlayDialogComponent } from '@promotions/components/promotionOverlayDialog/promotion-overlay-dialog.component';
import { PromotionIconComponent } from '@app/promotions/components/promotionIcon/promotion-icon.component';
import { PromoLabelsComponent } from '@app/promotions/components/promoLabels/promo-labels.component';
import { PromotionsListComponent } from '@app/promotions/components/promotionsList/promotions-list.component';

import { OffersSectionComponent } from '@bmaModule/components/offerSection/offer-section.component';
import { VisPreMatchWidgetComponent } from '@sbModule/components/visPreMatchWidget/vis-pre-match-widget.component';
import { VisualizationContainerComponent } from '@sbModule/components/visualizationContainer/visualization-container.component';
import { VisIframeDimensionsDirective } from '@sbModule/directives/vis-iframe-dimensions.directive';
import { InplayScoreComponent } from '@sharedModule/components/inplayScore/inplay-score.component';
import { ToggleButtonsComponent } from '@sharedModule/components/toggleButtons/toggle-buttons.component';
import { LinkHrefDirective } from '@sharedModule/directives/link-href.directive';
import { CashoutLabelComponent } from '@sharedModule/components/cashoutLabel/cashout-label.component';
import { YourCallLabelComponent } from '@sharedModule/components/yourCallLabel/your-call-label.component';
import { NgCarouselExtendedDirective } from '@sharedModule/directives/ng-carousel-extended/carousel.directive';
import { OddsBoostInfoDialogComponent } from '@sharedModule/components/oddsBoostInfoDialog/odds-boost-info-dialog.component';
import { BybLabelComponent } from '@sharedModule/components/bybLabel/byb-label.component';
import { LazyComponent } from '@sharedModule/components/lazy-component/lazy-component.component';
import { BreadcrumbsComponent } from '@sharedModule/components/breadcrumbs/breadcrumbs.component';

import { TooltipComponent } from '@sharedModule/components/tooltip/tooltip.component';
import { TooltipDirective } from '@sharedModule/directives/tooltip.directive';

import { SvgTeamKitComponent } from '@sharedModule/components/svgTeamKit/svg-team-kit.component';
import { ScoreMarketBaseService } from '@sharedModule/services/scoreMarketBase/score-market-base.service';
import { DropDownMenuComponent } from '@sharedModule/components/dropDownMenu/drop-down-menu.component';
import { NoEventsComponent } from '@shared/components/noEvents/no-events.component';
import { SurfaceBetsCarouselComponent } from '@sharedModule/components/surfaceBetsCarousel/surface-bets-carousel.component';
import { DrawerComponent } from '@sharedModule/components/drawer/drawer.component';
import { OxygenNotificationComponent } from '@sharedModule/components/oxygenNotification/oxygen-notification.component';
import { StickyVirtualScrollerComponent } from '@sharedModule/components/stickyVirtualScroller/sticky-virtual-scroller.component';
import { SharedPipesModule } from '@shared/pipes/shared-pipes.module';
import { SpinnerComponent } from '@sharedModule/components/spinner/spinner.component';
import { CarouselMenuComponent } from '@sharedModule/components/carouselMenu/carousel-menu.component';
import { FooterSectionComponent } from '@sharedModule/components/footerSection/footer-section.component';
import { BetslipHeaderIconComponent } from '@sharedModule/components/betslipHeaderIcon/betslip-header-icon.component';
import { MyBetsButtonComponent } from '@shared/components/myBetsButton/my-bets-button.component';
import { LoadingScreenComponent } from '@shared/components/loadingScreen/loading-screen.component';
import { BogLabelComponent } from '@shared/components/bogLabel/bog-label.component';
import { VirtualSilkComponent } from '@shared/components/virtualSilk/virtual-silk.component';
import { OddsCardScoreComponent } from '@shared/components/oddsCard/oddsCardScore/odds-card-score.component';
import { RaceSilkComponent } from '@shared/components/raceSilk/race-silk.component';
import { UkOrIreSilkComponent } from '@shared/components/raceSilk/ukOrIreSilk/uk-or-ire-silk.component';
import { GhSilkComponent } from '@shared/components/ghSilk/gh-silk.component';
import { ShowCurrencyDirective } from '@shared/directives/show-currency/show-currency.directive';

@NgModule({
  imports: [
    SharedPipesModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    ModalModule,
    FormsModule
  ],
  declarations: [
    GhSilkComponent,
    NextRacesModuleComponent,
    SeeAllLinkComponent,
    SpinnerComponent,
    AccaNotificationComponent,
    AccordionComponent,
    HistoricPricesComponent,
    PriceOddsButtonComponent,
    PriceOddsButtonOnPushComponent,
    OddsCardHeaderComponent,
    CustomSelectComponent,
    SwitchersComponent,
    WatchLabelComponent,
    CarouselMenuComponent,
    OffersSectionComponent,
    LiveClockComponent,
    VisPreMatchWidgetComponent,
    VisualizationContainerComponent,
    VisIframeDimensionsDirective,
    DatePickerComponent,
    StaticBlockComponent,
    AzSportsPageComponent,
    ShowMoreComponentComponent,
    ShowAllButtonComponent,
    ModuleRibbonComponent,
    TopBarComponent,
    RaceListComponent,
    RaceGridComponent,
    VerticalMenuComponent,
    ExpandPanelComponent,
    RequestErrorComponent,
    TabsPanelComponent,
    ToggleSwitchComponent,
    ViewChangerIconComponent,
    NgInfoPanelComponent,
    LoadingOverlayComponent,
    LiveServIframeComponent,
    OxygenDialogComponent,
    OxygenNotificationComponent,
    FreeBetsDialogComponent,
    FreeBetsNotificationComponent,
    FreeBetLabelComponent,
    SessionLogoutDialogComponent,
    ConnectionLostDialogComponent,
    InformationDialogComponent,
    BppErrorDialogComponent,
    RetailMenuComponent,
    ModuleDisabledComponent,
    OddsCardEnhancedMultiplesComponent,
    OddsCardHighlightCarouselComponent,
    SurfaceBetsCarouselComponent,
    OddsCardSurfaceBetComponent,
    OddsCardSpecialsComponent,
    OddsCardSportComponent,
    OddsCardComponent,
    RacingGridComponent,
    RaceCardComponent,
    RaceTimerComponent,
    // InplayMarketSelectorComponent,
    DropDownMenuComponent,
    // MatchesMarketSelectorComponent,
    SvgListComponent,
    SvgTeamKitComponent,
    WatchFreeInfoDialogComponent,
    FavouritesCounterComponent,
    FavouritesAddAllButtonComponent,
    FavouritesAddButtonComponent,
    QuickbetPanelWrapperComponent,
    TimeFormSelectionSummaryComponent,
    NoEventsComponent,
    LocaleDirective,
    PriceOddsValueDirective,
    PriceOddsClassDirective,
    PriceOddsDisabledDirective,
    ScrollableDirective,
    ScrollableRacingDirective,
    BackButtonDirective,
    ClickLinkDirective,
    DisableDraggingDirective,
    LazyRenderDirective,
    LastMadeBetDirective,
    PatternRestrictDirective,
    HomeScreenComponent,
    ScrollFixDirective,
    OverscrollFixDirective,
    TriggerDirective,
    InputValueDirective,
    EqualColumnDirective,
    OxygenDialogContainerDirective,
    DigitKeyboardComponent,
    DigitKeyboardInputDirective,
    NgCarouselDirective,
    NgCarouselExtendedDirective,
    ScoreDigitComponent,
    MaintenanceComponent,
    SeoStaticBlockComponent,
    FooterMenuComponent,
    BetslipCounterComponent,
    AbstractOutletComponent,
    OddsBoostInfoDialogComponent,
    OutletStatusComponent,
    PromotionDialogComponent,
    PromotionOverlayDialogComponent,
    PromotionIconComponent,
    PromoLabelsComponent,
    PromotionsListComponent,
    PromotionsComponent,
    SidebarComponent,
    OddsCardResultComponent,
    InplayScoreComponent,
    RacingOutcomeCardComponent,
    OddsCardResultComponent,
    ToggleButtonsComponent,
    CashoutLabelComponent,
    YourCallLabelComponent,
    BybLabelComponent,
    ToggleButtonsComponent,
    LinkHrefDirective,
    LazyComponent,
    BreadcrumbsComponent,
    TooltipComponent,
    TooltipDirective,
    StickyVirtualScrollerComponent,
    DrawerComponent,
    RacingStatusComponent,
    RacingPanelComponent,
    ListCardComponent,
    LiveLabelComponent,
    WatchLabelComponent,
    BogLabelComponent,
    NewLabelComponent,
    FooterSectionComponent,
    BetslipHeaderIconComponent,
    VirtualSilkComponent,
    MyBetsButtonComponent,
    LoadingScreenComponent,
    OddsCardScoreComponent,
    FavouriteIconComponent,
    RaceSilkComponent,
    UkOrIreSilkComponent,
    ShowCurrencyDirective,
    RacingOutcomeCardComponent
  ],
  entryComponents: [
    GhSilkComponent,
    SvgTeamKitComponent,
    NextRacesModuleComponent,
    SeeAllLinkComponent,
    SpinnerComponent,
    AccaNotificationComponent,
    HistoricPricesComponent,
    CustomSelectComponent,
    PriceOddsButtonComponent,
    PriceOddsButtonOnPushComponent,
    OddsCardHeaderComponent,
    AccordionComponent,
    SwitchersComponent,
    WatchLabelComponent,
    DatePickerComponent,
    CarouselMenuComponent,
    LiveClockComponent,
    StaticBlockComponent,
    AzSportsPageComponent,
    VisPreMatchWidgetComponent,
    VisualizationContainerComponent,
    ModuleRibbonComponent,
    TabsPanelComponent,
    TopBarComponent,
    OddsBoostInfoDialogComponent,
    ModuleDisabledComponent,
    ShowMoreComponentComponent,
    ShowAllButtonComponent,
    HomeScreenComponent,
    RaceListComponent,
    RaceGridComponent,
    VerticalMenuComponent,
    ExpandPanelComponent,
    RequestErrorComponent,
    ToggleSwitchComponent,
    ViewChangerIconComponent,
    NgInfoPanelComponent,
    LoadingOverlayComponent,
    LiveServIframeComponent,
    RaceListComponent,
    FreeBetsDialogComponent,
    FreeBetsNotificationComponent,
    FreeBetLabelComponent,
    SessionLogoutDialogComponent,
    ConnectionLostDialogComponent,
    InformationDialogComponent,
    OffersSectionComponent,
    BppErrorDialogComponent,
    RetailMenuComponent,
    OddsCardComponent,
    DigitKeyboardComponent,
    RacingGridComponent,
    RaceCardComponent,
    MaintenanceComponent,
    RaceTimerComponent,
    NoEventsComponent,
    // InplayMarketSelectorComponent,
    DropDownMenuComponent,
    // MatchesMarketSelectorComponent,
    ScoreDigitComponent,
    SeoStaticBlockComponent,
    OxygenDialogComponent,
    OxygenNotificationComponent,
    BetslipCounterComponent,
    AbstractOutletComponent,
    OutletStatusComponent,
    PromotionDialogComponent,
    PromotionOverlayDialogComponent,
    SvgListComponent,
    WatchFreeInfoDialogComponent,
    SidebarComponent,
    PromotionsListComponent,
    PromotionIconComponent,
    PromoLabelsComponent,
    PromotionsComponent,
    FavouritesCounterComponent,
    FavouritesAddAllButtonComponent,
    FavouritesAddButtonComponent,
    OddsCardResultComponent,
    InplayScoreComponent,
    OddsCardResultComponent,
    RacingOutcomeCardComponent,
    TimeFormSelectionSummaryComponent,
    ToggleButtonsComponent,
    CashoutLabelComponent,
    YourCallLabelComponent,
    BybLabelComponent,
    QuickbetPanelWrapperComponent,
    BreadcrumbsComponent,
    TooltipComponent,
    DrawerComponent,
    RacingStatusComponent,
    RacingPanelComponent,
    ListCardComponent,
    LiveLabelComponent,
    WatchLabelComponent,
    BogLabelComponent,
    NewLabelComponent,
    FooterSectionComponent,
    BetslipHeaderIconComponent,
    VirtualSilkComponent,
    MyBetsButtonComponent,
    LoadingScreenComponent,
    RaceSilkComponent,
    UkOrIreSilkComponent,
    RacingOutcomeCardComponent
  ],
  exports: [
    GhSilkComponent,
    SharedPipesModule,
    CommonModule,
    RouterModule,
    NextRacesModuleComponent,
    SeeAllLinkComponent,
    SpinnerComponent,
    AccaNotificationComponent,
    HistoricPricesComponent,
    CustomSelectComponent,
    PriceOddsButtonComponent,
    PriceOddsButtonOnPushComponent,
    OddsCardHeaderComponent,
    AccordionComponent,
    SwitchersComponent,
    WatchLabelComponent,
    CarouselMenuComponent,
    LiveClockComponent,
    StaticBlockComponent,
    AzSportsPageComponent,
    TabsPanelComponent,
    TopBarComponent,
    ModuleRibbonComponent,
    ShowMoreComponentComponent,
    ShowAllButtonComponent,
    RaceListComponent,
    RaceGridComponent,
    ExpandPanelComponent,
    VerticalMenuComponent,
    RequestErrorComponent,
    ToggleSwitchComponent,
    ModuleDisabledComponent,
    ViewChangerIconComponent,
    NgInfoPanelComponent,
    LoadingOverlayComponent,
    LiveServIframeComponent,
    OxygenDialogComponent,
    OxygenNotificationComponent,
    MaintenanceComponent,
    OffersSectionComponent,
    FreeBetsDialogComponent,
    FreeBetsNotificationComponent,
    FreeBetLabelComponent,
    SessionLogoutDialogComponent,
    ConnectionLostDialogComponent,
    VisPreMatchWidgetComponent,
    VisualizationContainerComponent,
    VisIframeDimensionsDirective,
    InformationDialogComponent,
    BppErrorDialogComponent,
    OddsCardComponent,
    OddsCardHighlightCarouselComponent,
    SurfaceBetsCarouselComponent,
    OddsCardSurfaceBetComponent,
    RetailMenuComponent,
    RacingGridComponent,
    NoEventsComponent,
    RaceCardComponent,
    RaceTimerComponent,
    // InplayMarketSelectorComponent,
    DropDownMenuComponent,
    // MatchesMarketSelectorComponent,
    SeoStaticBlockComponent,
    SvgListComponent,
    FavouritesCounterComponent,
    FavouritesAddAllButtonComponent,
    FavouritesAddButtonComponent,
    DatePickerComponent,
    WatchFreeInfoDialogComponent,
    QuickbetPanelWrapperComponent,
    OddsBoostInfoDialogComponent,

    LocaleDirective,
    ScrollableDirective,
    ScrollableRacingDirective,
    LazyRenderDirective,
    BackButtonDirective,
    HomeScreenComponent,
    LastMadeBetDirective,
    PatternRestrictDirective,
    OverscrollFixDirective,
    TriggerDirective,
    ClickLinkDirective,
    DisableDraggingDirective,
    LinkHrefDirective,
    InputValueDirective,
    EqualColumnDirective,
    DigitKeyboardComponent,
    DigitKeyboardInputDirective,
    NgCarouselDirective,
    NgCarouselExtendedDirective,
    FooterMenuComponent,
    BetslipCounterComponent,
    ScoreDigitComponent,
    AbstractOutletComponent,
    OutletStatusComponent,
    PromotionDialogComponent,
    PromotionOverlayDialogComponent,
    PromotionIconComponent,
    PromoLabelsComponent,
    PromotionsListComponent,
    PromotionsComponent,
    SidebarComponent,
    OddsCardResultComponent,
    InplayScoreComponent,
    OddsCardResultComponent,
    TimeFormSelectionSummaryComponent,
    ToggleButtonsComponent,
    SvgTeamKitComponent,
    ToggleButtonsComponent,
    CashoutLabelComponent,
    YourCallLabelComponent,
    BybLabelComponent,
    LazyComponent,
    BreadcrumbsComponent,
    TooltipComponent,
    TooltipDirective,
    StickyVirtualScrollerComponent,
    DrawerComponent,
    RacingStatusComponent,
    RacingPanelComponent,
    ListCardComponent,
    LiveLabelComponent,
    WatchLabelComponent,
    BogLabelComponent,
    NewLabelComponent,
    FooterSectionComponent,
    BetslipHeaderIconComponent,
    VirtualSilkComponent,
    MyBetsButtonComponent,
    LoadingScreenComponent,
    MyBetsButtonComponent,
    OddsCardScoreComponent,
    FavouriteIconComponent,
    RaceSilkComponent,
    UkOrIreSilkComponent,
    ShowCurrencyDirective,
    RacingOutcomeCardComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule {
  static forRoot() {
    return {
      ngModule: SharedModule,
      providers: [
        RoutingState,
        AccordionService,
        PriceOddsButtonAnimationService,
        PriceOddsButtonService,
        OddsCardHeaderService,
        LiveEventClockProviderService,
        MarketTypeService,
        TemplateService,
        CarouselService,
        MarketSelectorStorageService,
        MarketSelectorTrackingService,
        MarketSelectorConfigService,
        ScoreMarketBaseService
      ]
    };
  }
}
