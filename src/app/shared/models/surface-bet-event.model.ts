import { ISportEvent } from '@core/models/sport-event.model';
import { IPrice } from '@core/models/price.model';

export interface ISurfaceBetEvent extends ISportEvent {
  svg: string;
  svgId: string;
  oldPrice: IPrice;
  title: string;
  content: string;
}
