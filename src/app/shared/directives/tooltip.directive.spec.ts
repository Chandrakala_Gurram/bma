import { TooltipDirective } from './tooltip.directive';

describe('TooltipDirective', () => {
  let directive: TooltipDirective,
   element,
   rendererService,
   resolver,
   vcr,
   locale;

  beforeEach(() => {
    element = {
      nativeElement : { contains : jasmine.createSpy('element.contains').and.returnValue(false)},
    };

    rendererService = {
      renderer: {
        createText: jasmine.createSpy('renderer.createText')
      }
    };

    resolver = {
      resolveComponentFactory: jasmine.createSpy('resolver.resolveComponentFactory')
    };

    vcr = {
      createComponent: jasmine.createSpy('vcr.createComponent')
    };

    locale = {
      getString: jasmine.createSpy('locale.getString')
    };

    directive = new TooltipDirective(
      element,
      rendererService,
      resolver,
      vcr,
      locale
    );
  });

  it('ngOnInit should call display tooltip', () => {
    directive.showTooltip = true;
    spyOn(directive, 'displayTooltip');

    directive.ngOnInit();
    expect(directive.displayTooltip).toHaveBeenCalled();
  });
  it('ngOnInit should not call display tooltip', () => {
    directive.showTooltip = false;
    spyOn(directive, 'displayTooltip');

    directive.ngOnInit();
    expect(directive.displayTooltip).not.toHaveBeenCalled();
  });

  it('click should hide tooltip as tooltip exists', () => {
    directive['componentRef'] = {location : 'test'} as any;
    spyOn(directive, 'destroy');

    directive.click();
    expect(directive.destroy).toHaveBeenCalled();
  });

  it('click should hide tooltip as tooltip showOnce is true', () => {
    directive['showOnce'] = true;
    spyOn(directive, 'destroy');

    directive.click();
    expect(directive.destroy).toHaveBeenCalled();
  });

  it('click should show tooltip', () => {
    spyOn(directive, 'displayTooltip');

    directive.click();
    expect(directive.displayTooltip).toHaveBeenCalled();
  });

  it('clickOutside', () => {
    spyOn(directive, 'destroy');
    const event = {
      target: null
    } as any;

    directive.clickOutside(event);
    expect((directive as any).element.nativeElement.contains).toHaveBeenCalledWith(null);
    expect(directive.destroy).toHaveBeenCalled();
  });

  it('clickOutside', () => {
    element.nativeElement.contains.and.returnValue(true);
    spyOn(directive, 'destroy');
    const event = { target: { tagName: 'target '} } as any;
    directive.clickOutside(event);
    expect((directive as any).element.nativeElement.contains).toHaveBeenCalledWith({ tagName: 'target '});
    expect(directive.destroy).not.toHaveBeenCalled();
  });

  it('displayTooltip', () => {
    directive.displayTooltip();

    expect(resolver.resolveComponentFactory).toHaveBeenCalled();
    expect(vcr.createComponent).toHaveBeenCalled();
  });

  it('generateNgContent', () => {
    directive.tooltip = 'test tooltip';
    directive.generateNgContent();

    expect(locale.getString).toHaveBeenCalledWith('app.tooltip.test tooltip');
    expect(rendererService.renderer.createText).toHaveBeenCalled();
  });

  it('destroy', () => {
    directive['componentRef'] = {location : 'test', destroy : jasmine.createSpy()} as any;
    directive.destroy();

    expect(directive['componentRef']).toBeNull();
  });

  it('ngOnDestroy', () => {
    spyOn(directive, 'destroy');
    directive.ngOnDestroy();

    expect(directive.destroy).toHaveBeenCalled();
  });
});
