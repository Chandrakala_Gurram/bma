import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[oxygen-dialog-container]',
})
export class OxygenDialogContainerDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
