import { Directive, HostListener } from '@angular/core';
import { BackButtonService } from '@coreModule/services/backButton/back-button.service';

@Directive({
  selector: '[back-button]'
})
export class BackButtonDirective {

  constructor(
    private backButtonService: BackButtonService
  ) {}

  @HostListener('click') redirect() {
    this.backButtonService.redirectToPreviousPage();
  }
}
