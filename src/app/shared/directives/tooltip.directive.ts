import {
  Directive,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  TemplateRef,
  ComponentRef,
  ElementRef,
  ReflectiveInjector,
  ComponentFactoryResolver,
  ViewContainerRef,
  HostListener
} from '@angular/core';

import { TooltipComponent } from '@sharedModule/components/tooltip/tooltip.component';

import { LocaleService } from '@core/services/locale/locale.service';
import { RendererService } from '@shared/services/renderer/renderer.service';

@Directive({
  selector: '[tooltip]'
})
export class TooltipDirective implements OnInit, OnDestroy {
  @Input('tooltip') tooltip: string | TemplateRef<any> | ComponentRef<any>;
  @Input() showTooltip: boolean;
  @Input() showOnce?: boolean;

  @Output() readonly toggleTooltip = new EventEmitter<any>();

  private componentRef: ComponentRef<TooltipComponent>;

  constructor( private element: ElementRef,
               private rendererService: RendererService,
               private resolver: ComponentFactoryResolver,
               private vcr: ViewContainerRef,
               private locale: LocaleService) {
  }

  ngOnInit(): void {
    if (this.showTooltip) {
      this.displayTooltip();
    }
  }

  @HostListener('click')
  click(): void {
    if (this.componentRef || this.showOnce) {
      this.destroy();
    } else {
      this.displayTooltip();
    }

  }
  @HostListener('document:click', ['$event'])
  @HostListener('document:touchend', ['$event'])
  clickOutside(event: MouseEvent): void {
    if (!this.element.nativeElement.contains(event.target)) {
      this.destroy();
    }
  }

  displayTooltip(): void {
    const factory = this.resolver.resolveComponentFactory(TooltipComponent);
    const injector = ReflectiveInjector.resolveAndCreate([
      {
        provide: 'tooltipConfig',
        useValue: {
          host: this.element.nativeElement
        }
      }
    ]);
    this.componentRef = this.vcr.createComponent(factory, 0, injector, this.generateNgContent());
  }

  generateNgContent(): any[][] {
    if ( typeof this.tooltip === 'string' ) {
      const text = this.locale.getString(`app.tooltip.${this.tooltip}`);
      const element = this.rendererService.renderer.createText(text);
      return [ [ element ] ];
    }
  }

  destroy(): void {
    this.componentRef && this.componentRef.destroy();
    this.componentRef = null;
  }

  ngOnDestroy(): void {
    this.destroy();
  }
}
