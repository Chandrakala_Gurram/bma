export const oddsCardConstant = {
  MARKET_TYPES: [
    'Match Result and Both Teams To Score',
    'Total Goals Over/Under 1.5',
    'Total Goals Over/Under 2.5',
    'Total Goals Over/Under 3.5',
    'Total Goals Over/Under',
    'Draw No Bet',
    'Both Teams to Score',
    'Next Team to Score',
    'Extra-Time Result',
    'Match Betting',
    'To Qualify'
  ]
};

export const uiScoreHeaders = {
  'tennis': ['S', 'G', 'P'],
  'badminton': ['G', 'P'],
  'volleyball': ['S', 'P']
};

export const handicapTemplateMarketName = 'Handicap Match Result';
export const toQualifyMarketName = 'To Qualify';
