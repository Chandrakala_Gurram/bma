import { ReplacePipe } from '@shared/pipes/replace/replace.pipe';

describe('ReplacePipe', () => {
  let pipe;
  let value;
  let replacement;

  beforeEach(() => {
    pipe = new ReplacePipe();
    value = 'Some test $$text';
    replacement = '@';
  });

  it('should transform replace unwanted combinations in pipes', () => {
    const result = pipe.transform(value, replacement);

    expect(result).toBe('Some test @text');
  });

  it('should transform replace unwanted combinations in pipes', () => {
    const result = pipe.transform(value);

    expect(result).toBe('Some test text');
  });

  it('should return empty string #1', () => {
    expect(pipe.transform()).toBe('');
  });

  it('should return empty string #2', () => {
    const result = pipe.transform('', replacement);
    expect(result).toBe('');
  });

  it('should return empty string #3', () => {
    const result = pipe.transform();
    expect(result).toBe('');
  });

  it('should return empty string #4', () => {
    const result = pipe.transform('$$$', '', /\${3}/g);
    expect(result).toBe('');
  });
});
