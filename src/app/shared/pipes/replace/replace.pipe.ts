import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replace'
})
export class ReplacePipe implements PipeTransform {
  transform(value: string = '', replacement: string = '', pattern = /\${2}/g): any {
    return value.replace(pattern, replacement);
  }
}
