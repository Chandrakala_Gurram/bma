import { BetslipBannerComponent } from './betslip-banner.component';
import { BetslipBanner } from '@betslip/components/betslipBanner/betslip-banner';

describe('BetslipBannerComponent', () => {
  let component: BetslipBannerComponent,
    betSlipBannerService,
    router,
    windowRef;

  beforeEach(() => {
    betSlipBannerService = {
      sendGTMBetReceiptBannerClick: jasmine.createSpy(),
      sendGTMBetReceiptBannerView: jasmine.createSpy(),
      info: {}
    };

    router = {
      navigate: jasmine.createSpy('navigate')
    };

    windowRef = {
      nativeWindow: {
        open: jasmine.createSpy('nativeWindow.open')
      }
    };

    component = new BetslipBannerComponent(betSlipBannerService, router, windowRef);
  });

  it('should create BetslipBannerComponent instance', () => {
    expect(component).toBeTruthy();
    expect(component.info).toEqual({} as BetslipBanner);
  });

  it('trackClickInGtm should call betSlipBannerService.sendGTMBetReceiptBannerClick', () => {
    component.trackClickInGtm();
    expect(component['betSlipBannerService'].sendGTMBetReceiptBannerClick).toHaveBeenCalledTimes(1);
  });

  it('trackClickInGtm should call betSlipBannerService.sendGTMBetReceiptBannerClick', () => {
    component.trackViewInGtm();
    expect(component['betSlipBannerService'].sendGTMBetReceiptBannerView).toHaveBeenCalledTimes(1);
  });

  describe('performNavigate', () => {
    it('should not navigate if no link provided', () => {
      component.performNavigate();
      expect(router.navigate).not.toHaveBeenCalled();
      expect(windowRef.nativeWindow.open).not.toHaveBeenCalled();

      component.info = { banner: {} } as any;

      component.performNavigate();
      expect(router.navigate).not.toHaveBeenCalled();
      expect(windowRef.nativeWindow.open).not.toHaveBeenCalled();

      component.info = { banner: { link: undefined } } as any;

      component.performNavigate();
      expect(router.navigate).not.toHaveBeenCalled();
      expect(windowRef.nativeWindow.open).not.toHaveBeenCalled();
    });

    it('should navigate in app if link is not external', () => {
      component.info = { banner: { link: 'events' } } as any;

      component.performNavigate();
      expect(router.navigate).toHaveBeenCalledWith(['events']);
      expect(windowRef.nativeWindow.open).not.toHaveBeenCalled();
    });

    it('should open external link in new window', () => {
      component.info = { banner: { link: 'https://some-link.com' } } as any;

      component.performNavigate();
      expect(router.navigate).not.toHaveBeenCalled();
      expect(windowRef.nativeWindow.open).toHaveBeenCalledWith('https://some-link.com', '_blank');
    });

    it('should open external link via specified target', () => {
      component.info = { banner: { link: 'http://some-link.com', target: '_self' } } as any;

      component.performNavigate();
      expect(router.navigate).not.toHaveBeenCalled();
      expect(windowRef.nativeWindow.open).toHaveBeenCalledWith('http://some-link.com', '_self');
    });
  });
});
