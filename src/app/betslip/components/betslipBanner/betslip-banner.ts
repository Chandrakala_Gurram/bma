import { DeviceService } from '@core/services/device/device.service';
import { IOffer } from '@core/models/aem-banners-section.model';

export class BetslipBanner {

  isBannerAvailable: boolean;
  banner: IOffer;
  isMobile: boolean;
  constructor(
    public deviceService: DeviceService
  ) {
    this.isBannerAvailable = false;
    this.banner = null;
    this.isMobile = this.deviceService.isMobile;
  }
}
