import { BetSlipBannerService } from './betslip-banner.service';

describe('BetSlipBannerService', () => {
  let service: BetSlipBannerService,
    userService,
    gtmService,
    deviceService;

  beforeEach(() => {
    userService = {
      vipLevel: 'vip'
    };
    gtmService = {
      push: jasmine.createSpy()
    };
    deviceService = {};

    service = new BetSlipBannerService(userService, gtmService, deviceService);
    service.isBetSlipOpened = 0;
    service.info = {
      isBannerAvailable: false,
      banner: {
        title: 'name'
      }
    } as any;
  });

  it('should set BetSlip as opened', () => {
    service.setBetSlipOpened(2);
    expect(service.isBetSlipOpened).toEqual(2);
  });

  it('should set info property: banner', () => {
    const banner = {
      useDirectFileUrl: '/',
      directFileUrl: '/direct'
    } as any;
    service.setBanner(banner);
    expect(service.info.banner).toEqual(banner);
  });

  it('should set is banner is available', () => {
    service.setIsBannerAvailable(true);
    expect(service.info.isBannerAvailable).toBe(true);
  });

  it('should send GTM bet receipt banner click', () => {
    service.sendGTMBetReceiptBannerClick();
    expect(service['gtmService'].push).toHaveBeenCalledWith('trackEvent', {
      eventCategory: 'banner',
      eventAction: 'click',
      eventLabel: 'name',
      location: 'Bet Receipt',
      vipLevel: userService.vipLevel,
      position: '1'
    });
  });

  it('should send GTM bet receipt banner view', () => {
    service.sendGTMBetReceiptBannerView();
    expect(service['gtmService'].push).toHaveBeenCalledWith('trackEvent', {
      eventCategory: 'banner',
      eventAction: 'view',
      eventLabel: 'name',
      location: 'Bet Receipt',
      vipLevel: userService.vipLevel,
      position: '1',
      personalised: true
    });
  });
});
