import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { BetSlipBannerService } from './betslip-banner.service';
import { BetslipBanner } from './betslip-banner';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

@Component({
  selector: 'betslip-banner',
  templateUrl: 'betslip-banner.component.html',
  styleUrls: ['./betslip-banner.component.less']
})
export class BetslipBannerComponent {

  info: BetslipBanner;
  constructor(
    private betSlipBannerService: BetSlipBannerService,
    private router: Router,
    private windowRef: WindowRefService
  ) {
    this.info = betSlipBannerService.info;
  }

  trackViewInGtm() {
    this.betSlipBannerService.sendGTMBetReceiptBannerView();
  }

  trackClickInGtm(): void {
    this.betSlipBannerService.sendGTMBetReceiptBannerClick();
  }

  performNavigate(): void {
    const link = this.info && this.info.banner && this.info.banner.link,
      target = this.info && this.info.banner && this.info.banner.target;

    if (!link) {
      return;
    }

    if (link.startsWith('http://') || link.startsWith('https://')) {
      this.windowRef.nativeWindow.open(link, target || '_blank');
    } else {
      this.router.navigate([link]);
    }
  }
}
