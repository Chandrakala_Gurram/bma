import { Injectable } from '@angular/core';
import { BetslipApiModule } from '@betslipModule/betslip-api.module';

import { UserService } from '@core/services/user/user.service';
import { GtmService } from '@core/services/gtm/gtm.service';
import { DeviceService } from '@core/services/device/device.service';
import { BetslipBanner } from './betslip-banner';
import { IOffer } from '@core/models/aem-banners-section.model';

@Injectable({ providedIn: BetslipApiModule })
export class BetSlipBannerService {

  info: BetslipBanner;
  isBetSlipOpened: number;
  constructor(
    private userService: UserService,
    private gtmService: GtmService,
    private deviceService: DeviceService
  ) {
    this.info = new BetslipBanner(this.deviceService);
  }

  /**
   * get length of betSlipSingle : length;
   * @param {number} count
   */
  setBetSlipOpened(count: number): void {
    this.isBetSlipOpened = count;
  }

  /**
   * set info property: banner;
   * @param {object} banner
   */
  setBanner(banner: IOffer): void {
    this.info.banner = banner;
  }

  /**
   * set info property: isBannerAvailable
   * @param {boolean} isBannerAvailable
   */
  setIsBannerAvailable(isBannerAvailable: boolean): void {
    this.info.isBannerAvailable = isBannerAvailable;
  }

  sendGTMBetReceiptBannerClick(): void {
    this.gtmService.push('trackEvent', {
      eventCategory: 'banner',
      eventAction: 'click',
      eventLabel: this.info.banner.title,
      location: 'Bet Receipt',
      vipLevel: this.userService.vipLevel,
      position: '1'
    });
  }

  sendGTMBetReceiptBannerView(): void {
    this.gtmService.push('trackEvent', {
      eventCategory: 'banner',
      eventAction: 'view',
      eventLabel: this.info.banner.title,
      location: 'Bet Receipt',
      vipLevel: this.userService.vipLevel,
      position: '1',
      personalised: true
    });
  }

}
