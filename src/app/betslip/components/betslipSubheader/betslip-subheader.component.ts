import { Component, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';

import { InfoDialogService } from '@coreModule/services/infoDialogService/info-dialog.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { BetslipService } from '@betslipModule/services/betslip/betslip.service';
import { GtmService } from '@core/services/gtm/gtm.service';

@Component({
  selector: 'betslip-subheader',
  templateUrl: './betslip-subheader.component.html',
  styleUrls: ['./betslip-subheader.component.less']
})
export class BetslipSubheaderComponent implements OnInit, OnDestroy {
  @Output() readonly clear = new EventEmitter();

  count: number;

  constructor(
    private infoDialogService: InfoDialogService,
    private localeService: LocaleService,
    private pubSubService: PubSubService,
    private betSlipService: BetslipService,
    private gtmService: GtmService
  ) {

  }

  ngOnInit(): void {
    this.count = this.betSlipService.count();

    this.pubSubService.subscribe(
      'BetslipSubheaderComponent',
      this.pubSubService.API.BETSLIP_COUNTER_UPDATE,
      (count: number) => this.count = count
    );
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe('BetslipSubheaderComponent');
  }

  showConfirm(): void {
    this.infoDialogService.openInfoDialog(
      this.localeService.getString('bs.clearBetslipTitle'),
      this.localeService.getString('bs.confirmClearOfBetSlip'),
      'bs-clear-dialog', undefined, undefined,
      [{
        cssClass: 'btn-style4',
        caption: this.localeService.getString('bs.clearBetslipCancel'),
      }, {
        caption: this.localeService.getString('bs.clearBetslipContinue'),
        cssClass: 'btn-style2',
        handler: () => {
          this.betSlipService.closeNativeBetslipAndWaitAnimation(() => {
            this.clear.emit();
            this.infoDialogService.closePopUp();

            this.gtmService.push('trackEvent', {
              eventAction: 'trackEvent',
              eventCategory: 'betslip',
              eventLabel: 'clear betslip click'
            });
          });
        }
      }]
    );
  }
}
