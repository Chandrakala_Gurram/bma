import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'empty-betslip',
  templateUrl: './empty-betslip.component.html',
  styleUrls: ['./empty-betslip.component.less']
})
export class EmptyBetslipComponent {
  @Input() showButton: boolean;

  constructor(
    private router: Router,
    private pubSubService: PubSubService
  ) {}

  goToHomePage(): void {
    this.router.navigate(['/']);
    this.pubSubService.publish(this.pubSubService.API['show-slide-out-betslip'], false);
  }
}
