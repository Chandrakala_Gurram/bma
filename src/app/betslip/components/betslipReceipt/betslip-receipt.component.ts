import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import * as _ from 'underscore';
import { UserService } from '@core/services/user/user.service';
import { BETSLIP_VALUES } from '@betslip/constants/bet-slip.constant';
import { BetReceiptService } from '@betslip/services/betReceipt/bet-receipt.service';
import { DeviceService } from '@core/services/device/device.service';
import { SessionService } from '@authModule/services/session/session.service';
import { IBetReceiptEntity } from '@betslip/services/betReceipt/bet-receipt.model';
import { StorageService } from '@core/services/storage/storage.service';
import { BetSlipBannerService } from '@betslip/components/betslipBanner/betslip-banner.service';
import { IBetDetail } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { BetInfoDialogService } from '@betslip/services/betInfoDialog/bet-info-dialog.service';
import { ISportEvent } from '@core/models/sport-event.model';
import { GtmService } from '@core/services/gtm/gtm.service';
import { CommandService } from '@core/services/communication/command/command.service';
import { GtmTrackingService } from '@core/services/gtmTracking/gtm-tracking.service';
import { BodyScrollLockService } from '@betslip/services/bodyScrollLock/betslip-body-scroll-lock.service';
import { NativeBridgeService } from '@core/services/nativeBridge/native-bridge.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { BannersService } from '@core/services/aemBanners/banners.service';
import { AT_JS_LOADING_TIMEOUT, IOffer } from '@core/models/aem-banners-section.model';
import { OverAskService } from '@betslip/services/overAsk/over-ask.service';
import { LocaleService } from '@core/services/locale/locale.service';
import Utils, { DEFAULT_OPTIONS } from '@app/core/services/aemBanners/utils';
import environment from '@environment/oxygenEnvConfig';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'betslip-receipt',
  templateUrl: 'betslip-receipt.component.html',
  styleUrls: ['betslip-receipt.component.less']
})
export class BetslipReceiptComponent implements OnInit, OnDestroy {

  @Input() winAlertsEnabled: boolean;

  @Output() readonly winAlertsToggleChanged = new EventEmitter<{ receipt: IBetDetail, value: boolean }>();

  reusePending: boolean = false;
  currencySymbol: string;
  isAndroidBrowser: boolean;
  isOldIos: boolean;
  isDesktop: boolean;
  isTablet: boolean;
  message: { type?: string; msg?: string; };
  loadComplete: boolean = false;
  loadFailed: boolean;

  totalStake: number;
  totalEstimatedReturns: number;

  freeBetsStake: number;
  allReceipts: IBetReceiptEntity;
  activeReceipts: IBetReceiptEntity;
  allEvents: ISportEvent[];
  isAllBetsDeclined: boolean;
  isFootballAvailable: boolean = false;
  banner: IOffer;
  betDate: string;
  receiptsCounter: number;
  winAlertsActive: boolean;
  private brand: string = '';

  // we need this variable to control the flow of placed bets.
  // If user comes back to Betslip and previous bet(s) was(were) placed,
  // we don't care about them and can clean Betslip at all.
  // Otherwise they will be added to Betslip again and could be displayed by mistake
  private isBettingDone: boolean = false;

  get overask(): OverAskService {
    return this.overAskService;
  }

  constructor(
    protected user: UserService,
    protected betReceiptService: BetReceiptService,
    protected sessionService: SessionService,
    protected storageService: StorageService,
    protected betSlipBannerService: BetSlipBannerService,
    protected betInfoDialogService: BetInfoDialogService,
    protected aemBannersService: BannersService,
    protected location: Location,
    protected gtmService: GtmService,
    protected router: Router,
    protected comandService: CommandService,
    protected device: DeviceService,
    protected gtmTrackingService: GtmTrackingService,
    protected bodyScrollLockService: BodyScrollLockService,
    protected nativeBridge: NativeBridgeService,
    protected window: WindowRefService,
    protected overAskService: OverAskService,
    protected localeService: LocaleService,
    protected pubSubService: PubSubService
  ) {
    this.brand = Utils.resolveBrandOrDefault(environment.brand);
    this.message = this.betReceiptService.message;
    this.isAndroidBrowser = device.browserName === BETSLIP_VALUES.ANDROID_NATIVE;
    this.isOldIos = device.isIos && Number(device.osVersion) <= BETSLIP_VALUES.OLD_IOS;
    this.isDesktop = device.isDesktop;
    this.isTablet = this.device.isTablet;
    this.currencySymbol = this.user.currencySymbol;
  }

  ngOnInit(): void {
    this.bodyScrollLockService.enableBodyScroll();
    this.winAlertsActive = this.storageService.get('winAlertsEnabled');

    this.sessionService.whenProxySession().then(() => {
      this.betReceiptService.getBetReceipts().subscribe((dataInit: IBetReceiptEntity[]) => {
        this.core(dataInit);
        this.loadComplete = true;
      }, () => {
        this.loadComplete = true;
        this.loadFailed = true;
      });
    });
  }

  ngOnDestroy(): void {
    this.overAskService.isInFinal && this.overAskService.setStateAndClearInStorage(this.overAskService.states.off);
    this.isBettingDone && this.pubSubService.publish(this.pubSubService.API.OVERASK_CLEAN_BETSLIP, false);
    this.betReceiptService.clearMessage();
  }

  isExternalUrl(url: string): boolean {
    const extRex = /^(http:\/\/|https:\/\/)/;
    return extRex.test(url);
  }

  goToFavourites(): void {
    this.router.navigate(['/favourites']);
  }

  done(): void {
    this.isBettingDone = true;
    this.betReceiptService.done();
    this.storageService.remove('vsm-betmanager-coralvirtuals-en-selections');
    this.storageService.remove('vsbr-selection-map');
    this.storageService.remove('lastMadeBetSport');
    this.storageService.remove('lastMadeBet');
  }

  reuse(): void {
    this.reusePending = true;
    this.betReceiptService.reuse().then(() => { // TODO: 2 Observable
      this.betSlipBannerService.setIsBannerAvailable(false);
      this.reusePending = false;
    });
  }

  core(dataReceipts: IBetReceiptEntity[]): void {
    this.allReceipts = dataReceipts[0];
    this.activeReceipts = dataReceipts[1];

    if (!this.allReceipts) {
      this.router.navigate(['/']);
      return;
    }

    // double check received data - OA may think bet is accepted (isConfirmed: "Y") though bet was not placed
    this.isAllBetsDeclined = this.overAskService.isAllBetsDeclined ||
      !(this.activeReceipts.singles.length || this.activeReceipts.multiples.length);

    if (this.isAllBetsDeclined) {
      this.allEvents = [];

      if (this.message && this.message.msg === this.localeService.getString('bs.depositAndPlacebetSuccessMessage')) {
        this.message.msg = undefined;
      }

      return;
    }

    const confirmedBetsLength: number = this.activeReceipts.singles.length + this.activeReceipts.multiples.length;
    this.pubSubService.publish(this.pubSubService.API.BETS_COUNTER_PLACEBET, confirmedBetsLength);

    this.freeBetsStake = Number(this.betReceiptService.freeBetStake);
    this.totalStake = this.freeBetsStake && this.freeBetsStake > 0 ?
                      Number(this.betReceiptService.totalStake) - this.freeBetsStake :
                      Number(this.betReceiptService.totalStake);
    this.totalEstimatedReturns = Number(this.betReceiptService.totalEstimatedReturns);
    this.betDate = this.getBetDate();
    this.allEvents = this.betReceiptService.getActiveFootballEvents(this.activeReceipts);
    this.isFootballAvailable = this.allEvents.length > 0;
    this.receiptsCounter = this.getReceiptCounter();

    const allSportsEvents: ISportEvent[] = this.betReceiptService.getActiveSportsEvents(this.activeReceipts);

    let device = DEFAULT_OPTIONS.device;
    if (!this.isDesktop) {
      device = 'mobile';
    }

    if (this.winAlertsActive) {
      const bets = this.getAllBets(this.allReceipts);
      bets.forEach((bet: IBetDetail) => this.toggleWinAlerts({ receipt: bet, state: true }));
    }

    this.aemBannersService.fetchBetslipOffersFromAEM({
      atJsLoadingTimeout: AT_JS_LOADING_TIMEOUT,
      brand: this.brand,
      device: device
    }, {
      page: 'betslip',
      categoryId: allSportsEvents.map(e => e.categoryId).filter(n => n).join(','),
      marketId: allSportsEvents.map(e => e.marketId).filter(n => n).join(','),
      typeId: allSportsEvents.map(e => e.typeId).filter(n => n).join(','),
      eventId: allSportsEvents.map(e => e.id).filter(n => n).join(','),
      selectionId: allSportsEvents.map(e => e.selectionId).filter(n => n).join(',')
    }).subscribe((targetBannerResponse: { offers: IOffer[] }) => {
      if (targetBannerResponse && targetBannerResponse.offers && targetBannerResponse.offers.length) {
        const bannerToShow: IOffer = targetBannerResponse.offers[0];
        this.betSlipBannerService.setIsBannerAvailable(!!this.betSlipBannerService.isBetSlipOpened);
        this.banner = bannerToShow;
        this.betSlipBannerService.setBanner(this.banner);
      }
    }, () => {
      console.log('Fetching betslip banners failed');
    });
    this.comandService.executeAsync(this.comandService.API.GET_LIVE_STREAM_STATUS, undefined, false).then(streamData => {
      const GTMObject = {
        eventCategory: 'betslip',
        eventAction: 'place bet',
        eventLabel: 'success',
        location: this.window.nativeWindow.location.pathname,
      };

      Object.assign(GTMObject, this.betReceiptService.getGtmObject(this.activeReceipts, this.totalStake));

      if (GTMObject['ecommerce'] && GTMObject['ecommerce'].purchase
        && Array.isArray(GTMObject['ecommerce'].purchase.products)) {
        GTMObject['ecommerce'].purchase.products.forEach((product) => {
          Object.assign(product, {
            dimension87: streamData && streamData.streamActive ? 1 : 0,
            dimension88: streamData && streamData.streamID || null
          });
        });
      }
      this.gtmService.push('trackEvent', GTMObject);
    });
  }

  trackByIndex(index: number): number {
    return index;
  }

  isSingles(): boolean {
    return this.allReceipts.singles && this.allReceipts.singles.length > 0;
  }

  isMultiples(): boolean {
    return this.allReceipts.multiples && this.allReceipts.multiples.length > 0;
  }

  openSelectionMultiplesDialog(index: number): void {
    const betReceipt = this.getBetReceiptById(index, true);
    this.betInfoDialogService.multiple(betReceipt.betType, Number(betReceipt.numLines));
  }

  getBetReceiptById(index: number, isMultiple: boolean): IBetDetail {
    return isMultiple ? this.allReceipts.multiples[index] : this.allReceipts.singles[index];
  }

  getReceiptNumbers(receipts: IBetReceiptEntity): string[] {
    const bets = this.getAllBets(receipts);
    return _.pluck(bets, 'receipt');
  }

  getAllBets(receipts: IBetReceiptEntity): IBetDetail[] {
    const singles = this.isSingles() ? receipts.singles : [];
    const multiples = this.isMultiples() ? receipts.multiples : [];
    return singles.concat(multiples);
  }

  hasBoostedBets(receipts: IBetReceiptEntity): boolean {
    return _.some(this.getAllBets(receipts), (bet: IBetDetail) => bet.oddsBoosted);
  }

  toggleWinAlerts(event: { receipt: IBetDetail, state: boolean }): void {
    if (this.window.nativeWindow.NativeBridge.pushNotificationsEnabled) {
      if (!this.user.winAlertsToggled) {
        this.user.set({ winAlertsToggled: true });
      }
      this.winAlertsToggleChanged.emit({ receipt: event.receipt, value: event.state });
    }
  }

  private getBetDate(): string {
    const bet: IBetDetail = this.getBetReceiptById(0, this.isMultiples());
    return bet.date;
  }

  private getReceiptCounter(): number {
    return this.allReceipts.singles.length + this.allReceipts.multiples.length;
  }
}
