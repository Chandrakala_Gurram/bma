import { fakeAsync, tick } from '@angular/core/testing';
import { of as observableOf, throwError as observableThrowError, throwError } from 'rxjs';

import { BetslipReceiptComponent } from '@betslip/components/betslipReceipt/betslip-receipt.component';
import { IBetReceiptEntity } from '@betslip/services/betReceipt/bet-receipt.model';
import { IBetDetail } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { DEFAULT_OPTIONS } from '@core/services/aemBanners/utils';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('BetslipReceiptComponent', () => {
  let component: BetslipReceiptComponent;
  let user,
    betReceiptService,
    sessionService,
    storageService,
    betSlipBannerService,
    betInfoDialogService,
    locationStub,
    gtmService,
    router,
    commandService,
    device,
    gtmTrackingService,
    bodyScrollLockService,
    nativeBridgeService,
    bannersService,
    windowRefService,
    overAskService,
    localeService,
    pubSubService;

  const betslipMock: IBetDetail = {
    betId: '574707',
    betType: 'SGL',
    betTypeName: 'Single',
    betTermsChange: [],
    bonus: '',
    callId: '',
    cashoutStatus: '',
    cashoutValue: '0.90',
    currency: 'GBP',
    date: '2018-12-27 07:24:19',
    eventMarket: 'Win or Each Way',
    eventName: '08:10 Steepledowns',
    ipaddr: '10.80.62.7',
    leg: [],
    legType: 'W',
    name: 'EMILY MOLLIE',
    numLegs: '1',
    numLines: '1',
    numLinesLose: '0',
    numLinesVoid: '0',
    numLinesWin: '0',
    numSelns: '1',
    odds: {
      frac: '5/2',
      dec: '3.50',
    },
    oddsBoosted: false,
    paid: 'Y',
    placedBy: '',
    potentialPayout: '3.50',
    receipt: 'O/0208872/0000115',
    refund: '0.00',
    settleInfo: '',
    settled: 'N',
    settledAt: '',
    source: 'M',
    stake: '1.00',
    stakePerLine: '1.00',
    startTime: '2018-12-27 08:10:00',
    asyncAcceptStatus: 'A',
    status: 'A',
    tax: '0.00',
    taxRate: '0.00',
    taxType: 'S',
    stakeValue: 1.00,
    tokenValue: '0.00',
    uniqueId: '625520283007346341076352773621',
    userId: '',
    winnings: '0.00',
  };
  let sportEvents = [
    {
      id: '123',
    },
    {
      id: '125',
    }
  ];

  const aemMockBanners = [{
    brand: 'coral',
    imgUrl: 'https://banners-cms-assets.coral.co.uk/is/image/1',
    altText: 'Banner 20',
    title: 'Banner 20',
    link: 'https://bet-hl.coral.co.uk/#/betslip/add/665596740',
    target: '_self',
    tcText: '<p><u>HTML T&Cs PLATINUM</u>. 18+. ' +
    'Terms and Conditions Apply. Max bet £10. Please click here to view more. ' +
    '18+. Terms and Conditions Apply. Max bet £10. Please click here to view more. ' +
    '18+. Terms and Conditions Apply. Max bet £10. 18+. 18+.<br>\r\n</p>\r\n',
    tcLink: 'https://bet-hl.coral.co.uk/#/promotions/bet5_get20',
    position: 1,
    lazy: false,
    imgClass: ''
  }];

  const receiptEventsMock = {
    multiples: [
      {
        betTypeName: 'Double',
        betType: 'SGL',
        receipt: 'O/11',
        numLines: '1',
        stake: '5.00',
        numLegs: '1',
        odds: {
          frac: '5/23',
          dec: '3.2',
        },
        potentialPayout: '5.00',
        leg: [
          {
            part: [{ event: { id: 1, }, marketId: '123123' }],
            odds: {
              frac: '5/2',
              dec: '3.50',
            }
          }
        ]
      }
    ],
    singles: [
      {
        betId: '123',
        betType: 'type',
        receipt: 'O/22',
        betTypeName: 'Single',
        stake: '5.00',
        odds: {
          frac: '5/2',
          dec: '3.50',
        },
        eventMarket: 'Match Result',
        numLegs: '1',
        potentialPayout: '5.00',
        leg: [
          {
            part: [{ event: { id: 1, }, marketId: '123123' }],
            odds: {
              frac: '5/2',
              dec: '3.50',
            }
          }
        ]
      }
    ]
  } as IBetReceiptEntity;

  const sportEventMock = {
    page: 'betslip',
    categoryId: '6',
    typeId: '5',
    id: '6702260',
    selectionId: '7',
    marketId: '9'
  };

  beforeEach(() => {
    user = {
      oddsFormat: 'frac',
      receiptViewsCounter: 0,
      winAlertsToggled: false,
      set: jasmine.createSpy()
    };
    betReceiptService = {
      getActiveSportsEvents: jasmine.createSpy('getActiveSportsEvents').and.returnValue([]),
      getActiveFootballEvents: jasmine.createSpy('getActiveFootballEvents').and.returnValue([]),
      getGtmObject: jasmine.createSpy().and.returnValue({}),
      reuse: jasmine.createSpy('reuse'),
      done: jasmine.createSpy('done'),
      clearMessage: jasmine.createSpy('clearMessage'),
      getBetReceipts: jasmine.createSpy('getBetReceipts').and.returnValue(observableOf({}))
    };
    sessionService = {
      whenProxySession: jasmine.createSpy('whenProxySession').and.returnValue(Promise.resolve({}))
    };
    storageService = {
      remove: jasmine.createSpy('remove'),
      get: jasmine.createSpy('get')
    };
    betSlipBannerService = {
      setIsBannerAvailable: jasmine.createSpy(),
      setBanner: jasmine.createSpy()
    };
    betInfoDialogService = {
      multiple: jasmine.createSpy()
    };
    locationStub = {
      path: () => ''
    };
    gtmService = {
      push: jasmine.createSpy()
    };
    router = {
      navigate: jasmine.createSpy()
    };
    commandService = {
      executeAsync: () => Promise.resolve({
        streamActive: true,
        streamID: '111'
      }),
      API: {
        GET_LIVE_STREAM_STATUS: ''
      }
    };
    device = {};
    gtmTrackingService = {};
    bodyScrollLockService = {
      enableBodyScroll: jasmine.createSpy('enableBodyScroll')
    };
    nativeBridgeService = {};

    windowRefService = {
      nativeWindow: {
        NativeBridge : { pushNotificationsEnabled: true },
        location: {
          pathname: 'testPath'
        }
      }
    } as any;

    bannersService = {
      fetchBetslipOffersFromAEM: jasmine.createSpy('fetchBetslipOffersFromAEM').and.returnValue(
        observableOf({offers: aemMockBanners})
      )
    };

    overAskService = {
      setStateAndClearInStorage: jasmine.createSpy('setStateAndClearInStorage'),
      states: {
        off: 'off'
      }
    };

    localeService = {
      getString: jasmine.createSpy('getString')
    };
    pubSubService = {
      publish: jasmine.createSpy('publish'),
      API: pubSubApi
    };
  });

  function createComponent() {
    component = new BetslipReceiptComponent(user, betReceiptService, sessionService, storageService, betSlipBannerService,
      betInfoDialogService, bannersService, locationStub, gtmService, router, commandService, device, gtmTrackingService,
      bodyScrollLockService, nativeBridgeService, windowRefService, overAskService, localeService, pubSubService);
  }

  afterEach(() => {
    component = null;
  });

  it('should create component instance', () => {
    createComponent();
    expect(component).toBeTruthy();
  });

  it('ngInit', fakeAsync(() => {
    betReceiptService.getActiveFootballEvents.and.returnValue([]);
    betReceiptService.getActiveSportsEvents.and.returnValue(sportEvents);
    createComponent();
    component.allReceipts = receiptEventsMock;
    tick();
    expect(component.allReceipts).toEqual(receiptEventsMock);
    tick();
  }));

  it('ngInit no data - Observable error', fakeAsync(() => {
    betReceiptService.getActiveSportsEvents.and.returnValue(sportEvents);
    betReceiptService.getBetReceipts.and.returnValue(throwError('error'));
    createComponent();
    component.ngOnInit();
    expect(component.loadComplete).toEqual(false);
    tick();
    expect(component.loadComplete).toBeTruthy();
    expect(component.loadFailed).toBeTruthy();
  }));

  it('ngInit no data - Observable success', fakeAsync(() => {
    betReceiptService.getBetReceipts.and.returnValue(observableOf(receiptEventsMock));
    createComponent();
    spyOn(component, 'core');
    component.ngOnInit();
    tick();
    expect(component.loadComplete).toEqual(true);
    expect(component.core).toHaveBeenCalledWith(receiptEventsMock);
  }));

  it('ngInit should start data initialization', fakeAsync(() => {
    betReceiptService.getBetReceipts.and.returnValue(observableOf(receiptEventsMock));
    createComponent();
    spyOn(component, 'core');
    component.ngOnInit();
    tick();

    expect(component.loadComplete).toEqual(true);
    expect(sessionService.whenProxySession).toHaveBeenCalled();
    expect(betReceiptService.getBetReceipts).toHaveBeenCalled();
  }));

  it('should return true if external URL', () => {
    createComponent();
    expect(component.isExternalUrl('https://example.com')).toBeTruthy();
  });

  it('should redirect to favourites', () => {
    createComponent();
    component.goToFavourites();
    expect(router.navigate).toHaveBeenCalledWith(['/favourites']);
  });

  it('overask, deposit overask declined', fakeAsync(() => {
    betReceiptService.getBetReceipts.and.returnValue(observableOf([receiptEventsMock, receiptEventsMock]));
    overAskService.isAllBetsDeclined = true;
    localeService.getString = jasmine.createSpy('getString').and.returnValue('bs.depositAndPlacebetSuccessMessage');
    createComponent();
    component.message = {
      msg: 'bs.depositAndPlacebetSuccessMessage'
    };
    component.ngOnInit();
    tick();
    expect(component.message.msg).toBeUndefined();
  }));

  describe('ngOnDestroy', () => {

    beforeEach(() => {
      createComponent();
    });

    it('should not finish overask if not in progress', () => {
      overAskService.isInFinal = false;
      component.ngOnDestroy();

      expect(overAskService.setStateAndClearInStorage).not.toHaveBeenCalled();
    });

    it('should set state and clear in storage ', () => {
      overAskService.isInFinal = true;
      component.ngOnDestroy();

      expect(overAskService.setStateAndClearInStorage).toHaveBeenCalledWith(overAskService.states.off);
    });

    it('should call pubSubService.publish with "OVERASK_CLEAN_BETSLIP" and "false" params', () => {
      component['isBettingDone'] = true;

      component.ngOnDestroy();
      expect(pubSubService.publish).toHaveBeenCalledWith('OVERASK_CLEAN_BETSLIP', false);
    });

    it('should not call pubSubService.publish when isBettingDone set to false', () => {
      component.ngOnDestroy();
      expect(pubSubService.publish).not.toHaveBeenCalled();
    });

    it('should clear bet receipt messages after component destroy', () => {
      component.ngOnDestroy();
      expect(betReceiptService.clearMessage).toHaveBeenCalled();
    });
  });

  it('done', () => {
    createComponent();
    component.done();
    expect(component['isBettingDone']).toEqual(true);
    expect(betReceiptService.done).toHaveBeenCalled();
    expect(storageService.remove).toHaveBeenCalledWith('vsm-betmanager-coralvirtuals-en-selections');
    expect(storageService.remove).toHaveBeenCalledWith('vsbr-selection-map');
    expect(storageService.remove).toHaveBeenCalledWith('lastMadeBetSport');
    expect(storageService.remove).toHaveBeenCalledWith('lastMadeBet');
  });

  it('should reuse', fakeAsync(() => {
    betReceiptService.reuse.and.returnValue(Promise.resolve());
    createComponent();
    component.reuse();
    expect(component.reusePending).toBeTruthy();
    tick();
    expect(betSlipBannerService.setIsBannerAvailable).toHaveBeenCalledWith(false);
    expect(component.reusePending).toBeFalsy();
  }));

  it('should navigate to home page if there are no data', () => {
    createComponent();
    component.core(false as any);
    expect(router.navigate).toHaveBeenCalledWith(['/']);
  });

  describe('should call for banners with proper parameters', () => {
    it('coral, not desktop - mobile device', fakeAsync(() => {
      betReceiptService.getActiveFootballEvents.and.returnValue([sportEventMock]);
      betReceiptService.getActiveSportsEvents.and.returnValue([sportEventMock]);
      device = {
        isDesktop: false
      };
      createComponent();
      component.core([receiptEventsMock, receiptEventsMock]);
      tick();
      expect(bannersService.fetchBetslipOffersFromAEM).toHaveBeenCalledWith(
        {atJsLoadingTimeout: 2000, brand: 'coral', device: 'mobile'},
        { page: 'betslip',
          categoryId: '6',
          marketId: '9',
          typeId: '5',
          eventId: '6702260',
          selectionId: '7'
        });
    }));

    it('coral, default device', fakeAsync(() => {
      betReceiptService.getActiveFootballEvents.and.returnValue([sportEventMock]);
      betReceiptService.getActiveSportsEvents.and.returnValue([sportEventMock]);
      device = {
        isDesktop: true
      };
      createComponent();
      component.core([receiptEventsMock, receiptEventsMock]);
      tick();
      expect(bannersService.fetchBetslipOffersFromAEM).toHaveBeenCalledWith(
        {atJsLoadingTimeout: 2000, brand: 'coral', device: DEFAULT_OPTIONS.device},
        { page: 'betslip',
          categoryId: '6',
          marketId: '9',
          typeId: '5',
          eventId: '6702260',
          selectionId: '7'
        });
    }));
  });

  describe('core', () => {

    beforeEach(() => {
      betReceiptService.getActiveSportsEvents.and.returnValue(sportEvents);
      betReceiptService.getActiveFootballEvents.and.returnValue([sportEventMock]);
    });

    describe('isAllBetsDeclined doublecheck', () => {

      it('should not treat all bets as declined - OA accepted, has active multiples', () => {
        overAskService.isAllBetsDeclined = false;
        createComponent();
        component.message = <any>{
          msg: 'test'
        };
        component.core([
          {
            multiples: [{}],
            singles: [{}]
          } as IBetReceiptEntity,
          {
            multiples: [{}],
            singles: []
          } as IBetReceiptEntity
        ]);

        expect(component.isAllBetsDeclined).toBe(false);
        expect(pubSubService.publish).toHaveBeenCalledWith('BETS_COUNTER_PLACEBET', 1);
      });

      it('should not treat all bets as declined - OA accepted, has active singles', () => {
        overAskService.isAllBetsDeclined = false;
        createComponent();
        component.message = <any>{
          msg: 'test'
        };
        component.core([
          {
            multiples: [{}],
            singles: [{}]
          } as IBetReceiptEntity,
          {
            multiples: [],
            singles: [{}]
          } as IBetReceiptEntity
        ]);

        expect(component.isAllBetsDeclined).toBe(false);
        expect(pubSubService.publish).toHaveBeenCalledWith('BETS_COUNTER_PLACEBET', 1);
      });

      it('should treat all bets as declined - OA accepted, but no active bets (topup)', () => {
        overAskService.isAllBetsDeclined = false;
        createComponent();
        component.message = <any>{
          msg: 'test'
        };
        component.core([
          {
            multiples: [{}],
            singles: [{}]
          } as IBetReceiptEntity,
          {
            multiples: [],
            singles: []
          } as IBetReceiptEntity
        ]);

        expect(component.totalStake).toBe(undefined);
        expect(component.totalEstimatedReturns).toBe(undefined);
        expect(component.betDate).toBe(undefined);
        expect(localeService.getString).toHaveBeenCalledWith('bs.depositAndPlacebetSuccessMessage');
        expect(component.isAllBetsDeclined).toBe(true);
      });

      it('should not calculate if all declined', () => {
        overAskService.isAllBetsDeclined = true;
        createComponent();
        component.message = <any>{
          msg: 'test'
        };
        component.core([receiptEventsMock, receiptEventsMock]);

        expect(component.totalStake).toBe(undefined);
        expect(component.totalEstimatedReturns).toBe(undefined);
        expect(component.betDate).toBe(undefined);
        expect(localeService.getString).toHaveBeenCalledWith('bs.depositAndPlacebetSuccessMessage');
      });
    });

    it('should set first banner coming from AEM Target', fakeAsync(() => {
      createComponent();
      component.core([receiptEventsMock, receiptEventsMock]);
      tick();
      expect(betSlipBannerService.setBanner).toHaveBeenCalledWith(aemMockBanners[0]);
    }));

    it('should toggleWinAlerts on init', fakeAsync(() => {
      createComponent();
      component.toggleWinAlerts = <any>jasmine.createSpy('toggleWinAlerts');
      component.winAlertsActive = true;
      component.core([receiptEventsMock, receiptEventsMock]);
      tick();
      expect(component.toggleWinAlerts).toHaveBeenCalledTimes(2);
    }));

    it('should test success placebet GTM event not on mobile version', fakeAsync(() => {
      const receipts = {
        singles: [{ }],
        multiples: [{ }]
      } as IBetReceiptEntity;
      createComponent();
      spyOn(component, 'getReceiptNumbers').and.returnValue('test numbers');
      component.core([receipts, receipts]);
      tick();
      expect(gtmService.push).toHaveBeenCalledWith('trackEvent', {
        eventCategory: 'betslip',
        eventAction: 'place bet',
        eventLabel: 'success',
        location: windowRefService.nativeWindow.location.pathname
      });
    }));

    it ('should test success placebet GTM event on mobile version', fakeAsync(() => {
      const receipts = {
        singles: [{ }],
        multiples: []
      } as IBetReceiptEntity;
      createComponent();
      spyOn(component, 'getReceiptNumbers').and.returnValue('test numbers');
      component.core([receipts, receipts]);
      tick();
      expect(gtmService.push).toHaveBeenCalledWith('trackEvent', {
        eventCategory: 'betslip',
        eventAction: 'place bet',
        eventLabel: 'success',
        location: windowRefService.nativeWindow.location.pathname
      });
    }));

    it('should not set banner coming from AEM Target if there is an empty response', fakeAsync(() => {
      bannersService.fetchBetslipOffersFromAEM.and.returnValue(observableOf(null));

      createComponent();
      component.core([receiptEventsMock, receiptEventsMock]);

      tick();
      expect(betSlipBannerService.setBanner).not.toHaveBeenCalled();
    }));

    it('should not set banner coming from AEM Target if there is offers is empty', fakeAsync(() => {
      bannersService.fetchBetslipOffersFromAEM.and.returnValue(observableOf({ offers: null }));

      createComponent();
      component.core([receiptEventsMock, receiptEventsMock]);

      tick();
      expect(betSlipBannerService.setBanner).not.toHaveBeenCalled();
    }));

    it('should not set banner coming from AEM Target if there is offers length is 0', fakeAsync(() => {
      bannersService.fetchBetslipOffersFromAEM.and.returnValue(observableOf({ offers: [] }));

      createComponent();
      component.core([receiptEventsMock, receiptEventsMock]);

      tick();
      expect(betSlipBannerService.setBanner).not.toHaveBeenCalled();
    }));

    it('should add stream dimentions', fakeAsync(() => {
      (betReceiptService.getGtmObject as jasmine.Spy).and.returnValue({
        ecommerce: {
          purchase: {
            products: [
              {}
            ]
          }
        }
      });
      createComponent();
      component.core([receiptEventsMock, receiptEventsMock]);
      tick();
      expect(gtmService.push).toHaveBeenCalledWith('trackEvent', {
        eventCategory: 'betslip',
        eventAction: 'place bet',
        eventLabel: 'success',
        location: windowRefService.nativeWindow.location.pathname,
        ecommerce: {
          purchase: {
            products: [{
              dimension87: 1,
              dimension88: '111'
            }]
          }
        }
      } as any);
    }));

    it('should add stream dimentions if no stream and no id', fakeAsync(() => {
      commandService.executeAsync = jasmine.createSpy('').and.returnValue(Promise.resolve({}));
      (betReceiptService.getGtmObject as jasmine.Spy).and.returnValue({
        ecommerce: {
          purchase: {
            products: [
              {}
            ]
          }
        }
      });
      createComponent();
      component.core([receiptEventsMock, receiptEventsMock]);
      tick();
      expect(gtmService.push).toHaveBeenCalledWith('trackEvent', {
        eventCategory: 'betslip',
        eventAction: 'place bet',
        eventLabel: 'success',
        location: windowRefService.nativeWindow.location.pathname,
        ecommerce: {
          purchase: {
            products: [{
              dimension87: 0,
              dimension88: null
            }]
          }
        }
      } as any);
    }));

    it('should not add stream dimentions when no data', fakeAsync(() => {
      (betReceiptService.getGtmObject as jasmine.Spy).and.returnValue({
        ecommerce: {
          purchase: {
            products: {}
          }
        }
      });
      createComponent();
      component.core([receiptEventsMock, receiptEventsMock]);
      tick();
      expect(gtmService.push).toHaveBeenCalledWith('trackEvent', {
        eventCategory: 'betslip',
        eventAction: 'place bet',
        eventLabel: 'success',
        location: windowRefService.nativeWindow.location.pathname,
        ecommerce: {
          purchase: {
            products: {}
          }
        }
      } as any);
    }));

    it('should not add stream dimentions when no purchase', fakeAsync(() => {
      (betReceiptService.getGtmObject as jasmine.Spy).and.returnValue({
        ecommerce: {}
      });
      createComponent();
      component.core([receiptEventsMock, receiptEventsMock]);
      tick();
      expect(gtmService.push).toHaveBeenCalledWith('trackEvent', {
        eventCategory: 'betslip',
        eventAction: 'place bet',
        eventLabel: 'success',
        location: windowRefService.nativeWindow.location.pathname,
        ecommerce: {}
      } as any);
    }));

    it('should handle error on fetchBetslipOffersFromAEM failure', fakeAsync(() => {
      betReceiptService.getActiveFootballEvents.and.returnValue([]);
      betReceiptService.getActiveSportsEvents.and.returnValue([]);
      bannersService.fetchBetslipOffersFromAEM.and.returnValue(observableThrowError('someError'));

      createComponent();
      component.core([receiptEventsMock, receiptEventsMock]);
      tick();
      expect(betSlipBannerService.setBanner).not.toHaveBeenCalled();
    }));
  });

  it('track by index', () => {
    createComponent();
    expect(component.trackByIndex(1)).toBe(1);
  });

  it('is Single?', () => {
    createComponent();
    component.allReceipts = {} as IBetReceiptEntity;
    component.allReceipts.singles = receiptEventsMock.singles;
    component.allReceipts.multiples = receiptEventsMock.multiples;
    expect(component.isSingles()).toBe(true);
  });

  it('is Multiple?', () => {
    createComponent();
    component.allReceipts = {} as IBetReceiptEntity;
    component.allReceipts.singles = receiptEventsMock.singles;
    component.allReceipts.multiples = receiptEventsMock.multiples;
    expect(component.isMultiples()).toBe(true);
  });

  it('is Multiple? should return false', () => {
    createComponent();
    component.allReceipts = {} as IBetReceiptEntity;
    component.allReceipts.singles = receiptEventsMock.singles;
    component.allReceipts.multiples = [];
    expect(component.isMultiples()).toBeFalsy();
  });

  it('openSelectionMultiplesDialog', () => {
    createComponent();
    component.allReceipts = {} as IBetReceiptEntity;
    component.allReceipts.multiples = receiptEventsMock.multiples;
    spyOn(component, 'getBetReceiptById').and.returnValue(betslipMock);
    component.openSelectionMultiplesDialog(0);
    expect(betInfoDialogService.multiple).toHaveBeenCalledWith(component.allReceipts.multiples[0].betType,
      Number(component.allReceipts.multiples[0].numLines));
  });

  it('getBetReceiptById should return singles', () => {
    createComponent();
    component.allReceipts = {} as IBetReceiptEntity;
    component.allReceipts.singles = receiptEventsMock.singles;
    expect(component.getBetReceiptById(0, false)).toBe(component.allReceipts.singles[0]);
  });

  it('getBetReceiptById should return multiples', () => {
    createComponent();
    component.allReceipts = {} as IBetReceiptEntity;
    component.allReceipts.multiples = receiptEventsMock.multiples;
    expect(component.getBetReceiptById(0, true)).toBe(component.allReceipts.multiples[0]);
  });

  it('isFootballAvailable should return false', () => {
    createComponent();
    sportEvents = [];
    betReceiptService.getActiveFootballEvents.and.returnValue(sportEvents);
    expect(component.isFootballAvailable).toBe(false);
  });

  it('getReceiptNumbers', () => {
    createComponent();
    component.allReceipts = {} as IBetReceiptEntity;
    component.allReceipts.singles = receiptEventsMock.singles;
    component.allReceipts.multiples = receiptEventsMock.multiples;
    expect(component.getReceiptNumbers(receiptEventsMock as IBetReceiptEntity)).toEqual(['O/22', 'O/11']);
  });

  it('getReceiptNumbers should return empty array', () => {
    createComponent();
    component.allReceipts = {} as IBetReceiptEntity;
    component.allReceipts.singles = [];
    component.allReceipts.multiples = [];
    expect(component.getReceiptNumbers(receiptEventsMock as IBetReceiptEntity)).toEqual([]);
  });

  it('should unlock scroll on bet receipt', () => {
    createComponent();
    spyOn<any>(component, 'core');
    component.ngOnInit();

    expect(bodyScrollLockService.enableBodyScroll).toHaveBeenCalled();
  });

  it('toggleWinAlerts should emit an action', () => {
    const receipt = {
      uniqueId: '123',
      betId: '111'
    } as IBetDetail;

    createComponent();
    spyOn(component.winAlertsToggleChanged, 'emit');

    component.toggleWinAlerts({ receipt, state: true });

    expect(component.winAlertsToggleChanged.emit).toHaveBeenCalled();
    expect(user.set).toHaveBeenCalledWith({winAlertsToggled : true});
  });

  it('toggleWinAlerts should not call user set ', () => {
    const receipt = {
      uniqueId: '123',
      betId: '111'
    } as IBetDetail;

    createComponent();
    component['user'] = {
      winAlertsToggled : true
    } as any;

    spyOn(component.winAlertsToggleChanged, 'emit');
    component.toggleWinAlerts({ receipt, state: true });

    expect(user.set).not.toHaveBeenCalled();
    expect(component.winAlertsToggleChanged.emit).toHaveBeenCalled();
  });

  it('toggleWinAlerts should not emit an action', () => {
    const receipt = {
      uniqueId: '123',
      betId: '111'
    } as IBetDetail;
    windowRefService.nativeWindow.NativeBridge.pushNotificationsEnabled = false;
    createComponent();
    spyOn(component.winAlertsToggleChanged, 'emit');


    component.toggleWinAlerts({ receipt, state: true });

    expect(user.set).not.toHaveBeenCalled();
  });

  describe('receipt list', () => {
    const singleBet = { receipt: '123' };
    const multipleBet = { receipt: '456' };
    const receipts = {
      singles: [singleBet],
      multiples: [multipleBet]
    } as IBetReceiptEntity;

    beforeEach(() => {
      createComponent();
      component.allReceipts = receipts;
    });

    it('should get receipt numbers', () => {
      expect(component.getReceiptNumbers(receipts)).toEqual(['123', '456']);
    });

    it('should get all bets', () => {
      expect(component.getAllBets(receipts)).toEqual([singleBet, multipleBet] as IBetDetail[]);
    });

    it('should ckeck for boosted bets (has not boosted bets)', () => {
      expect(component.hasBoostedBets(receipts)).toEqual(false);
    });

    it('should ckeck for boosted bets (has boosted bets)', () => {
      receipts.singles[0].oddsBoosted = true;
      expect(component.hasBoostedBets(receipts)).toEqual(true);
    });
  });

  describe('#getReceiptCounter', () => {
    it('should count receipts', () => {
      createComponent();
      const singleBet = { receipt: '123' };
      const multipleBet = { receipt: '456' };
      const receipts = {
        singles: [singleBet],
        multiples: [multipleBet]
      } as IBetReceiptEntity;
      component.allReceipts = receipts;
      expect(component['getReceiptCounter']()).toEqual(2);
    });
  });

  it('should return overask service', () => {
    createComponent();
    expect(component.overask).toEqual(overAskService);
  });
});
