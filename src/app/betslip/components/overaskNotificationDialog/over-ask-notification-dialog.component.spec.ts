import { OverAskNotificationDialogComponent } from './over-ask-notification-dialog.component';

describe('OverAskNotificationDialogComponent', () => {
  let device;
  let windowRef;

  let component: OverAskNotificationDialogComponent;

  beforeEach(() => {
    device = {};
    windowRef = {};
    component = new OverAskNotificationDialogComponent(device, windowRef);
  });

  it('ngOnInit', () => {
    component.params = {
      p1: 'p1'
    };

    component.dialog = {
      close: () => {},
      onKeyDownHandler: () => {}
    };
    component.ngOnInit();
    expect(component.params.closeByEsc).toBeUndefined();
  });

});
