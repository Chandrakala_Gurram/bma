import { BetslipMultiplesReceiptComponent } from './betslip-multiples-receipt.component';
import { cloneDeep } from 'lodash-es';

describe('BetslipMultiplesReceiptComponent', () => {
  let component;
  let nativeBridge;
  let userService;
  let betReceiptService;
  let storageService;

  const excludedDrillDownTagNames = 'DrillDownTagNames or Empty string';

  const multiplesReceipts = [{
    leg: [{ part: [{ isFootball: true, description: 'unnamed favourite' }] }, { part: [{ isFootball: true }] }]
  }, {
    leg: [{ part: [{ isFootball: false, description: 'Unnamed 2nd Favourite' }] }, { part: [{ isFootball: false }] }]
  }, {
    leg: [{ part: [{ isFootball: true }] }, { part: [{ isFootball: false }] }]
  }];

  const receipts = [{
    isFavouriteAvailable: true,
    stakeValue: 5,
    leg: [
      {
        excludedDrillDownTagNames: excludedDrillDownTagNames,
        part: [{ isFootball: true, description: 'unnamed favourite' }]
      },
      {
        part: [{ isFootball: true }]
      }
     ]
  }, {
    isFavouriteAvailable: false,
    stakeValue: 5,
    leg: [
      {
        excludedDrillDownTagNames: excludedDrillDownTagNames,
        part: [{ isFootball: false, description: 'Unnamed 2nd Favourite' }]
      },
      {
        part: [{ isFootball: false }]
      }
     ]
  }, {
    isFavouriteAvailable: true,
    stakeValue: 5,
    leg: [
      { part: [{ isFootball: true }] },
      { part: [{ isFootball: false }] }
     ]
  }];

  beforeEach(() => {
    nativeBridge = {};
    userService = {
      oddsFormat: 'frac',
      currencySymbol: '$',
      receiptViewsCounter: 5,
      winAlertsToggled: false,
      username: 'test'
    };

    betReceiptService = {
      hasStakeMulti: () => { },
      getStakeMulti: () => { },
      getStakeTotal: () => { },
      setToggleSwitchId: () => { },
      getEWTerms: () => { },
      getLinesPerStake: () => { },
      getReceiptOdds: () => { },
      getFormattedPrice: jasmine.createSpy('getFormattedPrice').and.returnValue('0.5/1'),
      getExcludedDrillDownTagNames: jasmine.createSpy('getExcludedDrillDownTagNames').and.returnValue(excludedDrillDownTagNames)
    };

    storageService = {
      get: jasmine.createSpy('get').and.returnValue(undefined)
    };

    component = new BetslipMultiplesReceiptComponent(
      nativeBridge,
      userService,
      betReceiptService,
      storageService
    );
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
    expect(component.currencySymbol).toEqual('$');
    expect(component.hasStakeMulti).toEqual(jasmine.any(Function));
    expect(component.getStakeMulti).toEqual(jasmine.any(Function));
    expect(component.getStakeTotal).toEqual(jasmine.any(Function));
    expect(component.getEWTerms).toEqual(jasmine.any(Function));
    expect(component.getLinesPerStake).toEqual(jasmine.any(Function));
    expect(component.setToggleSwitchId).toEqual(jasmine.any(Function));
    expect(component.getOdds).toEqual(jasmine.any(Function));
  });

  describe('#trackByIndex', () => {
    it('should call trackByIndex and track by index and leg #', () => {
      expect(component.trackByIndex(2, { legNo: 20 } as any)).toEqual('2_20');
    });
  });

   describe('ngOnInit', () => {
     it('should call ngOnInit and modify multiples receipt with stakeValue', () => {
       component.getStakeMulti = () => 5;
       component.multiReceipts = cloneDeep(multiplesReceipts);
       component.ngOnInit();

       expect(component.multiReceipts).toEqual(receipts);
    });
  });

  describe('#showWinAlertsTooltip', () => {
    it('should call showWinAlertsTooltip when receiptViewsCounter less or equal MAX_VIEWS_COUNT', () => {
      expect(component.showWinAlertsTooltip()).toEqual(true);
      expect(storageService.get).toHaveBeenCalled();
    });

    it('should call showWinAlertsTooltip when receiptViewsCounter less or equal MAX_VIEWS_COUNT', () => {
      storageService.get = jasmine.createSpy().and.returnValue({'receiptViewsCounter-test': 2});
      expect(component.showWinAlertsTooltip()).toEqual(false);
      expect(storageService.get).toHaveBeenCalled();
    });
  });

  describe('#toggleWinAlerts', () => {
    it('should call toggleWinAlerts method and emit winAlertsToggleChanged', () => {
      component.winAlertsToggleChanged.emit = jasmine.createSpy('winAlertsToggleChanged.emit');

      component.toggleWinAlerts({} as any, true);

      expect(component.winAlertsToggleChanged.emit).toHaveBeenCalledWith({
        receipt: {},
        state: true
      });
    });
  });

  describe('#oddsACCA', () => {
    it('should call oddsACCA method and retrun frac potentialPayout', () => {
      const result = component.oddsACCA({
        potentialPayout: '1/2'
      } as any);

      expect(betReceiptService.getFormattedPrice).toHaveBeenCalledWith({
        potentialPayout: '1/2'
      });
      expect(result).toEqual('0.5/1');
    });

    it('should call oddsACCA method and retrun dec potentialPayout', () => {
      userService.oddsFormat = 'dec';
      betReceiptService.getFormattedPrice.and.returnValue('1.5');
      const result = component.oddsACCA({
        potentialPayout: '1.5'
      } as any);

      expect(result).toEqual('1.5');
    });

    it('should check show oddsACCA condition', () => {
      const receipt = {
        numLines: '1',
        isFCTC: false,
        potentialPayout: '1.5'
      } as any;
      expect(component.showOddsAcca(receipt)).toBeTruthy();
      receipt.isFCTC = true;
      expect(component.showOddsAcca(receipt)).toBeFalsy();
      receipt.numLines = '3';
      expect(component.showOddsAcca(receipt)).toBeFalsy();
      component.oddsACCA = () => 'test';
      expect(component.showOddsAcca(receipt)).toBeFalsy();
    });

    it('should show SP price if all of legs has SP prices', () => {
      const receipt = {
        numLines: '1',
        isFCTC: false,
        leg: [{ odds: { dec: 'SP', frac: 'SP' } }, { odds: { dec: 'SP', frac: 'SP' }}]} as any;
      expect(component.oddsACCA(receipt)).toEqual('SP');
    });

    it('should call oddsACCA method dec', () => {
      betReceiptService.getFormattedPrice.and.returnValue('0.01');
      userService.oddsFormat = 'dec';
      const result = component.oddsACCA({
        potentialPayout: '0.01'
      } as any);

      expect(result).toEqual('0.01');
    });

    it('should call oddsACCA method frac', () => {
      userService.oddsFormat = 'frac';
      const result = component.oddsACCA({
        potentialPayout: '1.5'
      } as any);

      expect(result).toEqual('0.5/1');
    });

    it('should call oddsACCA method sp price', () => {
      const result = component.oddsACCA({
        potentialPayout: 'NOT_AVAILABLE'
      } as any);

      expect(result).toEqual('N/A');
    });

    it('should call oddsACCA method overask', () => {
      const result = component.oddsACCA({
        potentialPayout: 'N/A'
      } as any);

      expect(result).toEqual('N/A');
    });
  });

  describe('set StakeValue, Favourites availability and excluded promo-icons', () => {
    it('should setReceiptsAdditionalData', () => {
      component['setReceiptsAdditionalData'](multiplesReceipts as any);

      expect(receipts).toEqual(receipts as any );
    });
  });

});
