import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { NativeBridgeService } from '@root/app/core/services/nativeBridge/native-bridge.service';
import { BetReceiptService } from '../../services/betReceipt/bet-receipt.service';
import { UserService } from '@core/services/user/user.service';
import { IBetDetail, IBetDetailLeg, IBetDetailLegPart } from '@root/app/bpp/services/bppProviders/bpp-providers.model';
import { IWinAlert } from '../../models/betslip-win-alert.model';
import { BetDetailUtils } from '@app/bpp/services/bppProviders/bet-detail.utils';
import { StorageService } from '@core/services/storage/storage.service';

@Component({
  selector: 'betslip-multiples-receipt',
  templateUrl: 'betslip-multiples-receipt.component.html',
  styleUrls: ['../../assets/styles/modules/receipt.less', './betslip-multiples-receipt.component.less']
})
export class BetslipMultiplesReceiptComponent implements OnInit {
  @Input() multiReceipts: IBetDetail[];
  @Input() winAlertsEnabled: boolean;
  @Input() winAlertsActive: boolean;

  @Output() readonly winAlertsToggleChanged = new EventEmitter<IWinAlert>();

  configFavourites: { fromWhere: string; } = { fromWhere: 'bsreceipt' };
  currencySymbol: string;
  hasStakeMulti: (receipt: IBetDetail) => boolean;
  getStakeMulti: (receipt: IBetDetail) => number;
  getStakeTotal: (receipt: IBetDetail) => number;
  setToggleSwitchId: (receipt: IBetDetail) => string;
  getEWTerms: (legPart: IBetDetailLegPart) => string;
  getLinesPerStake: (receipt: IBetDetail) => string;
  getOdds: (receipt: IBetDetail) => string;

  isStakeCanceled: (stake: IBetDetail) => boolean = BetDetailUtils.isCanceled;

  constructor(
    protected nativeBridge: NativeBridgeService,
    protected user: UserService,
    protected betReceiptService: BetReceiptService,
    protected storageService: StorageService,
  ) {
    this.hasStakeMulti = betReceiptService.hasStakeMulti;
    this.getStakeMulti = betReceiptService.getStakeMulti;
    this.getStakeTotal = betReceiptService.getStakeTotal;
    this.setToggleSwitchId = betReceiptService.setToggleSwitchId;
    this.getOdds = betReceiptService.getReceiptOdds;
    this.getEWTerms = betReceiptService.getEWTerms;
    this.getLinesPerStake = betReceiptService.getLinesPerStake;
    this.currencySymbol = this.user.currencySymbol;
  }

  ngOnInit(): void {
    this.setReceiptsAdditionalData(this.multiReceipts);
  }

  trackByIndex(index: number, receipt: IBetDetailLeg): string {
    return `${index}_${receipt.legNo}`;
  }

  showWinAlertsTooltip(): boolean {
    const MAX_VIEWS_COUNT: number = 1;
    const tooltipData = this.storageService.get('tooltipsSeen') || {};
    return (tooltipData[`receiptViewsCounter-${this.user.username}`] || null) <= MAX_VIEWS_COUNT && !this.user.winAlertsToggled;
  }

  toggleWinAlerts(receipt: IBetDetail, event: boolean): void {
    this.winAlertsToggleChanged.emit({ receipt, state: event });
  }

  oddsACCA(receipt: IBetDetail): string {
    if (this.showSPOdds(receipt)) {
      return 'SP';
    }
    if (['NOT_AVAILABLE', 'N/A'].includes(receipt.potentialPayout.toString())) {
      return 'N/A';
    }
    return this.betReceiptService.getFormattedPrice(receipt);
  }

  /**
   * Check whether all of leg combination prices are 'SP' prices
   * @returns {boolean}
   */
  showSPOdds(receipt: IBetDetail): boolean {
    return receipt.leg && receipt.leg.map(leg => {
      return leg.odds && leg.odds.dec === 'SP' && leg.odds.frac === 'SP' ;
    }).reduce((previous, current) => previous && current);
  }

  /**
   * Check if receipt is for x1 multiplicator combination bet and not Forecast/Tricast bet
   * @returns {boolean}
   */
  showOddsAcca(receipt: IBetDetail): boolean {
    return receipt.numLines === '1' && !receipt.isFCTC && this.oddsACCA(receipt) !== '';
  }

  /**
   * set StakeValue, Favourites availability and excluded promo-icons
   * @param receipts
   */
  private setReceiptsAdditionalData(receipts: IBetDetail[]): void {
    receipts.forEach((receipt: IBetDetail) => {
      receipt.stakeValue = this.getStakeMulti(receipt);
      receipt.isFavouriteAvailable = false;

      receipt.leg.forEach((leg: IBetDetailLeg) => {
        // setFavouriteAvailability
        if (leg.part[0] && leg.part[0].isFootball) {
          receipt.isFavouriteAvailable = true;
        }

        // exclude extra place promo-icon from race unnamed favourites
        if (leg.part[0] && leg.part[0].description) {
          leg.excludedDrillDownTagNames = this.betReceiptService.getExcludedDrillDownTagNames(leg.part[0].description);
        }
      });
    });
  }
}
