import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { FiltersService } from '@root/app/core/services/filters/filters.service';
import { NativeBridgeService } from '@root/app/core/services/nativeBridge/native-bridge.service';
import { BetReceiptService } from '../../services/betReceipt/bet-receipt.service';
import { UserService } from '@core/services/user/user.service';
import { IBetDetail, IBetDetailLegPart } from '@root/app/bpp/services/bppProviders/bpp-providers.model';
import { IWinAlert } from '../../models/betslip-win-alert.model';
import { LocaleService } from '@root/app/core/services/locale/locale.service';
import { StorageService } from '@core/services/storage/storage.service';

import { BetDetailUtils } from '@app/bpp/services/bppProviders/bet-detail.utils';

@Component({
  selector: 'betslip-singles-receipt',
  templateUrl: 'betslip-singles-receipt.component.html',
  styleUrls: ['../../assets/styles/modules/receipt.less']
})
export class BetslipSinglesReceiptComponent implements OnInit {
  @Input() singleReceipts: IBetDetail[];
  @Input() winAlertsEnabled: boolean;
  @Input() winAlertsActive: boolean;

  @Output() readonly winAlertsToggleChanged = new EventEmitter<IWinAlert>();

  configFavourites: { fromWhere: string; } = { fromWhere: 'bsreceipt' };
  currencySymbol: string;

  filterPlayerName: (name: string) => string;
  filterAddScore: (marketName: string, outcomeName: string) => string;
  hasStake: (receipt: IBetDetail) => boolean;
  getStake: (receipt: IBetDetail) => number;
  getOdds: (receipt: IBetDetail) => string;
  setToggleSwitchId: (receipt: IBetDetail) => string;
  getEWTerms: (legPart: IBetDetailLegPart) => string;
  getLinesPerStake: (receipt: IBetDetail) => string;

  isStakeCanceled: (stake: IBetDetail) => boolean = BetDetailUtils.isCanceled;
  isBogEnabled: boolean = false;

  constructor(
    protected nativeBridge: NativeBridgeService,
    protected user: UserService,
    protected betReceiptService: BetReceiptService,
    protected filtersService: FiltersService,
    protected localeService: LocaleService,
    protected storageService: StorageService,
  ) {
    this.filterPlayerName = filtersService.filterPlayerName;
    this.filterAddScore = filtersService.filterAddScore;
    this.hasStake = betReceiptService.hasStake;
    this.getStake = betReceiptService.getStake;
    this.getOdds = betReceiptService.getReceiptOdds;
    this.setToggleSwitchId = betReceiptService.setToggleSwitchId;
    this.getEWTerms = betReceiptService.getEWTerms;
    this.getLinesPerStake = betReceiptService.getLinesPerStake;
    this.currencySymbol = this.user.currencySymbol;
  }

  ngOnInit() {
    this.setStakeValue(this.singleReceipts);
  }

  trackByIndex(index: number, receipt: IBetDetail): string {
    return `${index}_${receipt.betId}`;
  }

  showWinAlertsTooltip(): boolean {
    const MAX_VIEWS_COUNT: number = 1;
    const tooltipData = this.storageService.get('tooltipsSeen') || {};
    return (tooltipData[`receiptViewsCounter-${this.user.username}`] || null) <= MAX_VIEWS_COUNT && !this.user.winAlertsToggled;
  }

  toggleWinAlerts(receipt: IBetDetail, event: boolean): void {
    this.winAlertsToggleChanged.emit({ receipt, state: event });
  }

  /**
   * Build lines title e.g. '1 line at £2.00 per line'
   */
  buildLinesTitle(receipt: IBetDetail): string {
    if (!receipt.isFCTC) { return; }
    const { numLines, stakePerLine } = receipt;
    const langKey: string = +numLines > 1 ? 'bs.linesPerStake' : 'bs.linePerStake';
    return this.localeService.getString(langKey, {
      lines: numLines,
      stake: stakePerLine,
      currency: this.user.currencySymbol
    });
  }

  private setStakeValue(receipts: IBetDetail[]): void {
    receipts.forEach((receipt: IBetDetail) => {
      receipt.stakeValue = parseFloat(receipt.stake) - parseFloat(receipt.tokenValue);
      receipt.excludedDrillDownTagNames = this.betReceiptService.getExcludedDrillDownTagNames(receipt.name);
    });
  }
}
