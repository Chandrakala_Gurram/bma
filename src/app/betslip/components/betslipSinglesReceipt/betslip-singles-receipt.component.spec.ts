import { BetslipSinglesReceiptComponent } from './betslip-singles-receipt.component';

describe('BetslipSinglesReceiptComponent', () => {
  let component;
  let nativeBridge;
  let userService;
  let betReceiptService;
  let filtersService;
  let localeService;
  let storageService;

  beforeEach(() => {
    nativeBridge = {};

    userService = {
      currencySymbol: '$',
      receiptViewsCounter: 5,
      username: 'test'
    };

    betReceiptService = {
      hasStake: () => { },
      getStake: () => { },
      getReceiptOdds: () => { },
      getLinesPerStake: () => { },
      getEWTerms: () => { },
      setToggleSwitchId: () => { },
      isBogFromPriceType: () => { },
      getExcludedDrillDownTagNames: jasmine.createSpy('getExcludedDrillDownTagNames').and.returnValue('ExcludedDrillDownTagNames')
    };

    filtersService = {
      filterPlayerName: () => { },
      filterAddScore: () => { }
    };

    localeService = {
      getString: jasmine.createSpy()
    };

    storageService = {
      get: jasmine.createSpy('storageService.get').and.returnValue(undefined)
    };

    component = new BetslipSinglesReceiptComponent(
      nativeBridge,
      userService,
      betReceiptService,
      filtersService,
      localeService,
      storageService
    );

    component.isBogFromPriceType = jasmine.createSpy('isBogFromPriceType');
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
    expect(component.currencySymbol).toEqual('$');
    expect(component.filterPlayerName).toEqual(jasmine.any(Function));
    expect(component.filterAddScore).toEqual(jasmine.any(Function));
    expect(component.getEWTerms).toEqual(jasmine.any(Function));
    expect(component.getLinesPerStake).toEqual(jasmine.any(Function));
    expect(component.hasStake).toEqual(jasmine.any(Function));
    expect(component.getStake).toEqual(jasmine.any(Function));
    expect(component.getOdds).toEqual(jasmine.any(Function));
    expect(component.setToggleSwitchId).toEqual(jasmine.any(Function));
  });

  it('#ngOnInit should modify singles receipt with stakeValue and exclude extra place promo for unnamed favourites', () => {
    component.singleReceipts = [
        { name: 'unnamed favourite', stake: 5, tokenValue: 1 },
        { name: 'Unnamed 2nd Favourite', stake: 3, tokenValue: 2 }
      ];
    component.ngOnInit();

    expect(component.singleReceipts).toEqual([
        { excludedDrillDownTagNames: 'ExcludedDrillDownTagNames', name: 'unnamed favourite', stake: 5, tokenValue: 1, stakeValue: 4 },
        { excludedDrillDownTagNames: 'ExcludedDrillDownTagNames', name: 'Unnamed 2nd Favourite', stake: 3, tokenValue: 2, stakeValue: 1 }
      ]);
  });

  describe('#trackByIndex', () => {
    it('should call trackByIndex and track by index and betId #', () => {
      expect(component.trackByIndex(2, { betId: '23423423' } as any)).toEqual('2_23423423');
    });
  });

  describe('#showWinAlertsTooltip', () => {
    it('should call showWinAlertsTooltip when receiptViewsCounter less or equal MAX_VIEWS_COUNT', () => {
      expect(component.showWinAlertsTooltip()).toEqual(true);
      expect(storageService.get).toHaveBeenCalled();
    });
    it('should call showWinAlertsTooltip when receiptViewsCounter less or equal MAX_VIEWS_COUNT', () => {
      storageService.get = jasmine.createSpy('').and.returnValue({'receiptViewsCounter-test': 2});
      expect(component.showWinAlertsTooltip()).toEqual(false);
      expect(storageService.get).toHaveBeenCalled();
    });
  });

  describe('#toggleWinAlerts', () => {
    it('should call toggleWinAlerts method and emit winAlertsToggleChanged', () => {
      component.winAlertsToggleChanged.emit = jasmine.createSpy('winAlertsToggleChanged.emit');

      component.toggleWinAlerts({} as any, true);

      expect(component.winAlertsToggleChanged.emit).toHaveBeenCalledWith({
        receipt: {},
        state: true
      });
    });
  });

  describe('#buildLinesTitle', () => {
    let receipt;
    beforeEach(() => {
      receipt = {
        isFCTC: true,
        numLines: '5',
        stakePerLine: 2
      } as any;
    });
    it('forcast receipt case for 5 lines', () => {
      component.buildLinesTitle(receipt);
      expect(localeService.getString).toHaveBeenCalledWith('bs.linesPerStake', {
        lines: receipt.numLines,
        stake: receipt.stakePerLine,
        currency: '$'
      });
    });
    it('forcast receipt case for 1 line', () => {
      receipt.numLines = '1';
      component.buildLinesTitle(receipt);
      expect(localeService.getString).toHaveBeenCalledWith('bs.linePerStake', {
        lines: receipt.numLines,
        stake: receipt.stakePerLine,
        currency: '$'
      });
    });
    it('not a forcast receipt case', () => {
      receipt.isFCTC = false;
      expect(component.buildLinesTitle(receipt)).toBeUndefined();
      component.buildLinesTitle(receipt);
      expect(localeService.getString).not.toHaveBeenCalled();
    });
  });
});
