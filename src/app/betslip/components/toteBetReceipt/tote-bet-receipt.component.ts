import { Component } from '@angular/core';
import * as _ from 'underscore';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { ToteBetslipService } from '@app/betslip/services/toteBetslip/tote-betslip.service';
import { DeviceService } from '@core/services/device/device.service';
import { IToteBet, IToteBetDetails } from '@app/betslip/services/toteBetslip/tote-betslip.model';
import { StorageService } from '@core/services/storage/storage.service';
import { ToteBetReceiptService } from '@app/betslip/services/toteBetReceipt/tote-bet-receipt.service';
import { IPoolBetDetail } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { UserService } from '@core/services/user/user.service';

@Component({
  selector: 'tote-bet-receipt',
  templateUrl: './tote-bet-receipt.component.html'
})
export class ToteBetReceiptComponent {
  toteBetDetails: IToteBetDetails;
  loadComplete: boolean;
  loadFailed: boolean;
  receipts: IPoolBetDetail[];
  totalStake: string;
  userCurrencySymbol: string;
  poolCurrencyCode: string;
  betDate: string;

  private toteBet: IToteBet;

  constructor(
    private pubSubService: PubSubService,
    private deviceService: DeviceService,
    private toteBetslip: ToteBetslipService,
    private storageService: StorageService,
    private toteBetReceiptService: ToteBetReceiptService,
    private userService: UserService
  ) {
    this.toteBet = this.getToteBet();
    this.toteBetDetails = this.toteBet.toteBetDetails;
    this.poolCurrencyCode = this.toteBet.poolCurrencyCode;
    this.userCurrencySymbol = this.userService.currencySymbol;
    this.getToteBetReceipt();
  }

  /**
   * Close bet slip
   */
  done(): void {
    this.pubSubService.publish(this.pubSubService.API.HOME_BETSLIP);
    if (this.deviceService.isMobile) {
      // sync to `show-${scope.sideClass}` in sidebar.js
      this.pubSubService.publish(this.pubSubService.API['show-slide-out-betslip'], false);
    }
  }

  /**
   * Add selections one more time to betslip
   */
  reuse(): void {
    this.toteBetslip.addToteBet(this.toteBet);
    this.pubSubService.publish(this.pubSubService.API.REUSE_TOTEBET);
    this.pubSubService.publish(this.pubSubService.API.HOME_BETSLIP);
  }

  private getToteBetReceipt(): void {
    this.toteBetReceiptService.getToteBetReceipt().subscribe((data: IPoolBetDetail[]) => {
      const totePoolBetDetail: IPoolBetDetail = _.first(data);
      this.receipts = data;
      this.betDate = totePoolBetDetail.date;
      this.totalStake = totePoolBetDetail && totePoolBetDetail.stake;
      // Hide spinner
      this.loadComplete = true;
      this.toteBetslip.removeToteBet(false, true);
    }, () => {
      this.loadComplete = true;
      this.loadFailed = true;
    });
  }

  /**
   * Gets tote bet
   * @return {object} - tote Bet
   */
  private getToteBet(): IToteBet | null {
    return this.storageService.get('toteBet') || null;
  }
}
