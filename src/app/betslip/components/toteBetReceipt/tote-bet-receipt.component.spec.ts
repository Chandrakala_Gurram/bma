import { of as observableOf, throwError } from 'rxjs';

import { ToteBetReceiptComponent } from './tote-bet-receipt.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('ToteBetReceiptComponent', () => {
  let component: ToteBetReceiptComponent;
  let userService;
  let toteBetslipService;
  let deviceService;
  let pubSubService;
  let storageService;
  let toteBetReceiptService;
  let toteBet;

  beforeEach(() => {
    userService = {
      currencySymbol: '$'
    };
    toteBetslipService = {
      addToteBet: jasmine.createSpy(),
      removeToteBet: jasmine.createSpy()
    };
    deviceService = {
      deviceService: true,
      isMobile: true
    };
    pubSubService = {
      publish: jasmine.createSpy(),
      API: pubSubApi
    };
    toteBet = {
      poolCurrencyCode: 'GBP'
    };
    storageService = {
      get: () => {
        return toteBet;
      }
    };
    toteBetReceiptService = {
      getToteBetReceipt: () => observableOf([{
        date: '123'
      }])
    };

    component = new ToteBetReceiptComponent(pubSubService, deviceService, toteBetslipService,
      storageService, toteBetReceiptService, userService);
  });

  it('should create and init component: success', () => {
    expect(component).toBeTruthy();
    expect(component.poolCurrencyCode).toEqual('GBP');
    expect(component.userCurrencySymbol).toEqual('$');
  });

  it('should create and init component: failed', () => {
    component['toteBetReceiptService'].getToteBetReceipt = jasmine.createSpy().and.returnValue(throwError(
      'Failed to fetch'
    ));
    component['getToteBetReceipt']();
    expect(component.loadComplete).toBeTruthy();
    expect(component.loadFailed).toBeTruthy();
  });

  describe('done', () => {
    it('done', () => {
      component.done();
      expect(pubSubService.publish).toHaveBeenCalledTimes(2);
      expect(pubSubService.publish).toHaveBeenCalledWith(pubSubApi.HOME_BETSLIP);
      expect(pubSubService.publish).toHaveBeenCalledWith('show-slide-out-betslip', false);
    });

    it('done (no mobile)', () => {
      component['deviceService']['isMobile'] = false;
      component.done();
      expect(pubSubService.publish).not.toHaveBeenCalledWith('show-slide-out-betslip', false);
    });
  });

  it('reuse', () => {
    component.reuse();

    expect(toteBetslipService.addToteBet).toHaveBeenCalledTimes(1);
    expect(toteBetslipService.addToteBet).toHaveBeenCalledWith({ poolCurrencyCode: 'GBP' });

    expect(pubSubService.publish).toHaveBeenCalledTimes(2);
    expect(pubSubService.publish).toHaveBeenCalledWith(pubSubApi.REUSE_TOTEBET);
    expect(pubSubService.publish).toHaveBeenCalledWith(pubSubApi.HOME_BETSLIP);
  });

  it('should getToteBet', () => {
    component['storageService']['get'] = jasmine.createSpy('get').and.returnValue(undefined);
    expect(component['getToteBet']()).toEqual(null);
  });
});
