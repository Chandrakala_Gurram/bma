import { Component, Input, OnInit } from '@angular/core';

import { ToteBetslipService } from '../../services/toteBetslip/tote-betslip.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { IToteBetDetails } from '@betslip/services/toteBetslip/tote-betslip.model';
import { IBetDetail } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { CoreToolsService } from '@core/services/coreTools/core-tools.service';
import { UserService } from '@core/services/user/user.service';

@Component({
  selector: 'tote-bet-receipt-item',
  templateUrl: './tote-bet-receipt-item.component.html',
  styleUrls: ['../../assets/styles/modules/receipt.less']
})
export class ToteBetReceiptItemComponent implements OnInit {
  @Input() receipts: IBetDetail[];
  @Input() toteBet: IToteBetDetails;
  @Input() poolCurrencyCode: string;

  unitStakeSuffix: string;
  toteBetSlip: ToteBetslipService;
  poolCurrencySymbol: string;
  userCurrencySymbol: string;
  linesInfo: string;
  title: string;

  constructor(
    private toteBetslipService: ToteBetslipService,
    private localeService: LocaleService,
    private coreToolsService: CoreToolsService,
    private userService: UserService
  ) {
    this.toteBetSlip = this.toteBetslipService;
    this.userCurrencySymbol = this.userService.currencySymbol;
  }

  ngOnInit(): void {
    this.poolCurrencySymbol = this.coreToolsService.getCurrencySymbolFromISO(this.poolCurrencyCode);
    this.toteBet.isPotBet = !!this.toteBet.orderedLegs;
    this.linesInfo = this.buildLinesTitle();
    this.title =  this.toteBet.eventTitle;
    if (this.toteBet.isPotBet) {
      this.title = this.linesInfo;
    }
  }

  private buildLinesTitle(): string {
    const { numLines, stakePerLine } = this.receipts[0];
    const langKey: string = +numLines > 1 ? 'bs.linesPerStake' : 'bs.linePerStake';
    return this.localeService.getString(langKey, {
      lines: numLines,
      stake: stakePerLine,
      currency: this.poolCurrencySymbol
    });
  }
}
