import { ToteBetReceiptItemComponent } from './tote-bet-receipt-item.component';

describe('ToteBetReceiptItemComponent', () => {
  let component: ToteBetReceiptItemComponent;
  let coreToolsService;
  let userService;
  let toteBetslipService;
  let localeService;
  let receipt;

  const toteBet = {
    orderedLegs: [],
    betName: 'Placepot',
    numberOfLines: 5,
    poolName: 'test',
    stakeRestrictions: {
      maxStakePerLine: '1',
      maxTotalStake: '1',
      minStakePerLine: '1',
      minTotalStake: '1',
      stakeIncrementFactor: '1'
    }
  };

  beforeEach(() => {
    coreToolsService = {
      getCurrencySymbolFromISO: jasmine.createSpy()
    };
    userService = {
      currencySymbol: '$'
    };
    toteBetslipService = {};
    localeService = {
      getString: jasmine.createSpy('getString')
    };

    component = new ToteBetReceiptItemComponent(toteBetslipService, localeService, coreToolsService, userService);
    component.poolCurrencyCode = 'GBP';
    component.toteBet = toteBet;

    component['localeService'].getString = jasmine.createSpy();
    receipt = {
      isFCTC: true,
      numLines: '5',
      stakePerLine: 2
    } as any;
  });

  it('should create and init component', () => {
    expect(component).toBeTruthy();
    expect(component.userCurrencySymbol).toEqual('$');
  });

  describe('ngOnInit', () => {
    it('should init non pot bet', () => {
      component.receipts = [receipt];
      component.toteBet.orderedLegs = undefined;
      component.ngOnInit();
      expect(coreToolsService.getCurrencySymbolFromISO).toHaveBeenCalledWith('GBP');
      expect(component.toteBet.isPotBet).toBeFalsy();
    });
    it('should init pot bet', () => {
      component.receipts = [receipt];
      component.toteBet.orderedLegs = [];
      component.ngOnInit();
      expect(coreToolsService.getCurrencySymbolFromISO).toHaveBeenCalledWith('GBP');
      expect(component.toteBet.isPotBet).toBeTruthy();
    });
  });

  describe('#buildLinesTitle', () => {
    it('forcast receipt case for 5 lines', () => {
      component.receipts = [receipt];
      component['buildLinesTitle']();
      expect(component['localeService'].getString).toHaveBeenCalledWith('bs.linesPerStake', {
        lines: receipt.numLines,
        stake: receipt.stakePerLine,
        currency: component.poolCurrencySymbol
      });
    });
    it('forcast receipt case for 1 line', () => {
      receipt.numLines = '1';
      component.receipts = [receipt];
      component['buildLinesTitle']();
      expect(component['localeService'].getString).toHaveBeenCalledWith('bs.linePerStake', {
        lines: receipt.numLines,
        stake: receipt.stakePerLine,
        currency: component.poolCurrencySymbol
      });
    });
  });
});
