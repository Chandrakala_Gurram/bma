import { BetslipTotalWrapperComponent } from '@betslip/components/betslipTotalWrapper/betslip-total-wrapper.component';

describe('BetslipTotalWrapperComponent', () => {
  let component;

  beforeEach(() => {
    component = new BetslipTotalWrapperComponent();
  });

  describe('isTotalStakeShown', () => {
    it('should be true if there is both free bet and stake', () => {
      component.totalFreeBetsStake = '$1';
      component.totalStake = '$1';

      expect(component.isTotalStakeShown).toBeTruthy();
    });

    it('should be true if there is no free bet', () => {
      component.totalFreeBetsStake = null;
      component.totalStake = '$1';

      expect(component.isTotalStakeShown).toBeTruthy();
    });

    it('should be false when there are no neither free bet nor stake', () => {
      component.totalFreeBetsStake = null;
      component.totalStake = null;

      expect(component.isTotalStakeShown).toBeFalsy();
    });

    it('should be false if there is no stake', () => {
      component.totalFreeBetsStake = '$1';
      component.totalStake = null;

      expect(component.isTotalStakeShown).toBeFalsy();
    });
  });
});
