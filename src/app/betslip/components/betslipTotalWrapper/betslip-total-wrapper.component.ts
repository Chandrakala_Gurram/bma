import { Component, Input } from '@angular/core';

@Component({
  selector: 'betslip-total-wrapper',
  templateUrl: './betslip-total-wrapper.component.html',
  styleUrls: ['./betslip-total-wrapper.component.less']
})
export class BetslipTotalWrapperComponent {
  @Input() totalStake: string;
  @Input() totalReturns: string;
  @Input() totalFreeBetsStake: string;

  get isTotalStakeShown(): boolean {
    if (this.totalStake && this.totalStake.length) {
      const totalStakeParsed = parseFloat(this.totalStake.slice(1, this.totalStake.length));
      return !this.totalFreeBetsStake || totalStakeParsed > 0;
    }
  }
}
