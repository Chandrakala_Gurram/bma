import { Component, OnDestroy, OnInit } from '@angular/core';
import { LocaleService } from '@core/services/locale/locale.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { ISystemConfig } from '@core/services/cms/models';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { SessionService } from '@authModule/services/session/session.service';
import { NativeBridgeService } from '@core/services/nativeBridge/native-bridge.service';
import { DeviceService } from '@core/services/device/device.service';
import { OverAskService } from '@betslip/services/overAsk/over-ask.service';
import { IBetDetail } from '@root/app/bpp/services/bppProviders/bpp-providers.model';
import { StorageService } from '@core/services/storage/storage.service';

@Component({
  selector: 'betslip-container',
  templateUrl: 'betslip-container.component.html'
})
export class BetslipContainerComponent implements OnInit, OnDestroy {

  sessionStateDefined: boolean = false;
  tag: string = 'wBetslip';
  sysConfig: ISystemConfig;
  winAlertsBets: string[] = [];
  winAlertsReceiptId: string;

  private bsMode: string;
  private modes: { [key: string]: string; };

  constructor(
    locale: LocaleService,
    private cmsService: CmsService,
    private pubSubService: PubSubService,
    private sessionService: SessionService,
    private nativeBridge: NativeBridgeService,
    protected device: DeviceService,
    private overAskService: OverAskService,
    private storageService: StorageService
  ) {
    this.modes = {
      betslip: locale.getString('app.betslipTabs.betslip'),
      cashout: locale.getString('app.betslipTabs.cashout'),
      openbets: locale.getString('app.betslipTabs.openbets'),
      betReceipt: locale.getString('app.betslipTabs.betReceipt'),
      toteBetReceipt: locale.getString('app.betslipTabs.toteBetReceipt'),
      betHistory: locale.getString('app.betslipTabs.betHistory')
    };
  }

  get MODES() {
    return this.modes;
  }

  get mode() {
    return this.bsMode;
  }

  set mode(value) {
    this.bsMode = value;
    this.overAskService.bsMode = value;
  }

  selectBetSlipTab(name: string, preventSystemUpdate: boolean = false): void {
    const updateSystemConf = name === this.MODES.betslip && !preventSystemUpdate;

    if (this.mode === 'Bet Receipt' && this.winAlertsBets.length) {
      this.nativeBridge.onActivateWinAlerts(this.winAlertsReceiptId, this.winAlertsBets);
      this.winAlertsBets = [];
      this.winAlertsReceiptId = null;
    }

    this.mode = name;
    if (updateSystemConf) {
      this.cmsService.triggerSystemConfigUpdate();
    }
    this.pubSubService.publish(this.pubSubService.API.BETSLIP_LABEL, name);
  }


  ngOnInit(): void {
    this.selectBetSlipTab(this.MODES.betslip);

    this.pubSubService.subscribe(this.tag, [this.pubSubService.API.SUCCESSFUL_LOGIN], placeBet => {
      if (placeBet !== 'betslip') {
        this.selectBetSlipTab(this.mode || this.MODES.betslip);
      }
    });

    this.sessionService.whenSession().then(() => {
      this.sessionStateDefined = true;
      this.selectBetSlipTab(this.mode || this.MODES.betslip);
    }).catch(error => error && console.warn(error));

    this.pubSubService.subscribe(this.tag, this.pubSubService.API.SESSION_LOGOUT, () => {
      if (this.sessionStateDefined) {
        this.selectBetSlipTab(this.MODES.betslip, true);
      }
    });

    this.pubSubService.subscribe(this.tag, this.pubSubService.API.HOME_BETSLIP,
      name => this.selectBetSlipTab(name || this.MODES.betslip));

    this.cmsService.getSystemConfig().subscribe((config) => this.sysConfig = config);

    this.pubSubService.subscribe(this.tag, this.pubSubService.API.BETSLIP_UPDATED, () => {
      if (this.mode !== this.MODES.betslip) {
        this.selectBetSlipTab(this.MODES.betslip);
      }
    });
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe(this.tag);
  }

  setWinAlertsBets(event: {receipt: IBetDetail, value: boolean}): void {
    const receipt = event.receipt;

    if (event.value) {
      if (!this.winAlertsReceiptId) { this.winAlertsReceiptId = receipt.uniqueId; }
      this.winAlertsBets.push(receipt.receipt);
      this.storageService.set('winAlertsEnabled', true);
    } else {
      this.winAlertsBets.splice(this.winAlertsBets.indexOf(receipt.receipt), 1);

      if (!this.winAlertsBets.length) {
        this.storageService.set('winAlertsEnabled', false);
      }
    }
  }
}
