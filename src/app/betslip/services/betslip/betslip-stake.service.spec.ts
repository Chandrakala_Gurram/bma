import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';
import { BetslipStakeService } from './betslip-stake.service';
import { CoreToolsService } from '@core/services/coreTools/core-tools.service';

describe('BetslipStakeService', () => {
  let overAskService;
  let bsFiltersService;
  const coreToolsService = new CoreToolsService();
  let pubSubService;
  let storageService;
  let service: BetslipStakeService;

  beforeEach(() => {
    overAskService = {};
    bsFiltersService = {
      filterStakeValue: jasmine.createSpy('filterStakeValue')
    };
    pubSubService = {
      API: pubSubApi,
      subscribe: jasmine.createSpy('subscribe')
    };
    storageService = {
      get: jasmine.createSpy('get')
    };

    spyOn(coreToolsService, 'roundTo').and.callThrough();
    spyOn(coreToolsService, 'roundDown').and.callThrough();

    createService();
  });

  function createService() {
    service = new BetslipStakeService(
      overAskService,
      bsFiltersService,
      coreToolsService,
      pubSubService,
      storageService
    );
  }

  it('getStake', () => {
    const bets: any[] = [{
      selectedFreeBet: {}, disabled: true, stake: {}
    }, {
      selectedFreeBet: {}, stake: { amount: 1 }
    }, {
      selectedFreeBet: {}, stake: { perLine: 1 }
    }, {
      stake: {}
    }];

    service.getStake(bets);

    expect(coreToolsService.roundDown).toHaveBeenCalledTimes(1);
    expect(coreToolsService.roundDown).toHaveBeenCalledTimes(1);
  });

  it('getStake: should return sum of all(active and suspended) stakes', () => {
    const bets: any[] = [{
      selectedFreeBet: {}, disabled: true, stake: { amount: 2 }
    }, {
      selectedFreeBet: {}, stake: { amount: 1 }
    }, {
      selectedFreeBet: {}, stake: { perLine: 1 }
    }, {
      stake: {}
    }];

    service.getStake(bets, true);

    expect(coreToolsService.roundDown).toHaveBeenCalledTimes(1);
  });

  describe('getTotalEstReturns', () => {
    it('N/A', () => {
      expect(
        service.getTotalEstReturns([{
          isSP: true, stake: { perLine: 1 }
        }] as any, false)
      ).toBeNull();

      expect(
        service.getTotalEstReturns([{
          isSP: true, stake: { perLine: 0 }, selectedFreeBet: {}
        }] as any, false)
      ).toBeNull();

      expect(
        service.getTotalEstReturns([{
          type: 'SGL', stake: { perLine: 0 }, selectedFreeBet: {}
        }] as any, false)
      ).toBeNull();

      expect(
        service.getTotalEstReturns([{
          type: 'DBL', stake: { perLine: 0 }, selectedFreeBet: {},
          Bet: {}
        }] as any, false)
      ).toBeNull();
    });

    it('amount', () => {
      bsFiltersService.filterStakeValue.and.returnValue(10);
      expect(
        service.getTotalEstReturns([{
          type: 'SGL', stake: { perLine: 1 },
          price: { priceType: 'LP', priceNum: 1, priceDen: 2 },
          Bet: {}
        }, {
          type: 'SGL', stake: { perLine: 0 },
          price: { priceType: 'LP', priceNum: 1, priceDen: 2 },
          Bet: {}, selectedFreeBet: {}
        }] as any, false)
      ).toBe(30);
    });

    it('amount correctly truncated to lowest value', () => {
      bsFiltersService.filterStakeValue.and.returnValue(1);
      expect(
        service.getTotalEstReturns([{
          type: 'SGL', stake: { perLine: 1 },
          price: { priceType: 'LP', priceNum: 14, priceDen: 9 },
          Bet: {}
        }] as any, false)
      ).toBe(2.55); // truncate 2.5555555555555554 to 2.55
    });

    it('tote in betslip', () => {
      bsFiltersService.filterStakeValue.and.returnValue(10);
      expect(
        service.getTotalEstReturns([{ disabled: true }] as any, true)
      ).toBeNull();
    });
  });

  describe('calculateEstReturns', () => {
    it('N/A', () => {
      expect(
        service.calculateEstReturns({
          price: { priceType: 'SP' }
        } as any)
      ).toBe('N/A');
    });

    it('calculate returns', () => {
      service.calculateEstReturns({
        price: { priceType: 'LP' }, selectedFreeBet: { value: 1 },
        Bet: { isEachWay: true }, stake: {}
      } as any);

      service.calculateEstReturns({
        price: { priceType: 'LP' },
        Bet: {}, stake: {}
      } as any);

      expect(bsFiltersService.filterStakeValue).toHaveBeenCalledTimes(2);
    });

    it('should include freebets if no overask', () => {
      service['overAskService']['hasTraderMadeDecision'] = false;
      service['overAskService']['isInProcess'] = false;
      bsFiltersService.filterStakeValue.and.returnValue(5);

      expect(
        service.calculateEstReturns({
          type: 'SGL', stake: { perLine: 5 },
          price: { priceType: 'LP', priceNum: 1, priceDen: 2 },
          Bet: { isEachWay: false },
          selectedFreeBet: { value: 1 }
        } as any)
      ).toBe(8);
    });

    it('should return potentialPayout as is if overask', () => {
      const bet = {
        potentialPayout: '100.100',
        isTraderOffered: true,
        price: {
          priceType: 'LP'
        }
      } as any;

      expect(service.calculateEstReturns(bet)).toEqual(100.1);
    });
  });

  describe('getOddsBoostEnabled', () => {
    it('enabled in localstorage', () => {
      storageService.get.and.returnValue(true);
      service['getOddsBoostEnabled']();

      expect(service.oddsBoostEnabled).toEqual(true);
    });

    it('disabled in localstorage', () => {
      storageService.get.and.returnValue(null);
      service['getOddsBoostEnabled']();

      expect(service.oddsBoostEnabled).toEqual(false);
    });

    afterEach(() => {
      expect(storageService.get).toHaveBeenCalledWith('oddsBoostActive');
      expect(pubSubService.subscribe).toHaveBeenCalledWith('BetslipStakeService', pubSubApi.ODDS_BOOST_CHANGE, jasmine.any(Function));
    });
  });

  it('getSelectedBets', () => {
    expect(
      service['getSelectedBets']().length
    ).toBe(0);

    expect(
      service['getSelectedBets']([{}] as any).length
    ).toBe(1);

    overAskService.isInProcess = true;
    overAskService.isOnTradersReview = false;
    expect(
      service['getSelectedBets']([{}, { isSelected: true }] as any).length
    ).toBe(1);
  });

  describe('getPrices', () => {
    it('no odds boost', () => {
      const bet: any = { price: {} };
      expect(service['getPrices'](bet)).toBe(bet.price);
    });

    it('odds boost enabled and available', () => {
      service.oddsBoostEnabled = true;
      const bet: any = {
        Bet: {
          oddsBoost: { enhancedOddsPriceNum: 1, enhancedOddsPriceDen: 2 }
        }
      };
      expect(service['getPrices'](bet)).toEqual({ priceNum: 1, priceDen: 2 });
    });

    it('should left bet price even if boosted when bet is offered by overask', () => {
      service.oddsBoostEnabled = true;
      const bet: any = {
        isTraderOffered: true,
        price: {priceNum: 500, priceDen: 1 },
        Bet: {
          oddsBoost: { enhancedOddsPriceNum: 1, enhancedOddsPriceDen: 2 }
        }
      };
      expect(service['getPrices'](bet)).toBe(bet.price);
    });
  });

  describe('calculateEstReturnsMultiples', () => {
    it('should return potential payout', () => {
      bsFiltersService.filterStakeValue.and.returnValue(1);
      const bet: any = {
        stake: { lines: 1 },
        Bet: {}, potentialPayout: 2
      };
      expect(service.calculateEstReturnsMultiples(bet)).toBe(bet.potentialPayout);
    });

    it('should return potential payout with truncated value', () => {
      bsFiltersService.filterStakeValue.and.returnValue(1);
      const bet: any = {
        stake: { lines: 1 },
        Bet: {}, potentialPayout: 2.5555555555555554
      };
      expect(service.calculateEstReturnsMultiples(bet)).toBe(2.55);
    });

    it('should return N/A if no payout', () => {
      expect(
        service.calculateEstReturnsMultiples({
          stake: { lines: 1 },
          Bet: {}
        } as any)
      ).toBe('N/A');
    });

    it('should return N/A if SP price', () => {
      expect(
        service.calculateEstReturnsMultiples({
          isSP: true, stake: { lines: 1 },
          Bet: {}
        } as any)
      ).toBe('N/A');
    });

    it('should return N/A if boosted e/w bet', () => {
      service.oddsBoostEnabled = true;
      expect(
        service.calculateEstReturnsMultiples({
          isEachWay: true, stake: { lines: 1 },
          Bet: { oddsBoost: {} }
        } as any)
      ).toBe('N/A');
    });

    it('should calculate est returns for eachway if stakeMultiplier !== 1', () => {
      bsFiltersService.filterStakeValue = jasmine.createSpy().and.returnValue(0);
      createService();

      const bet = {
        stake: {
          perLine: 1,
          lines: 1,
          freeBetAmount: 3
        },
        selectedFreeBet: {
          value: '3'
        },
        potentialPayout: 20,
        stakeMultiplier: 2,
        Bet: {
          isEachWay: true,
          payout: [{ potential: 20, legType: 'P' }]
        },
        outcomes: [{
          eachWayFactorNum: 1,
          eachWayFactorDen: 3,
          price: {
            priceNum: 3,
            priceDen: 1
          }
        }]
      } as any;

      expect(service.calculateEstReturnsMultiples(bet)).toEqual(117);

      bet.outcomes = null;

      expect(service.calculateEstReturnsMultiples(bet)).toEqual(117);

      bet.outcomes = [{
        eachWayFactorNum: 1,
        eachWayFactorDen: 3,
        price: {
          priceNum: 3,
          priceDen: 1
        }
      }, {
        eachWayFactorNum: 1,
        eachWayFactorDen: 3,
        price: {
          priceNum: 3,
          priceDen: 1
        }
      }] as any;

        expect(service.calculateEstReturnsMultiples(bet)).toEqual(117);
    });

    it('should calculate est returns for eachway if stakeMultiplier === 1', () => {
      bsFiltersService.filterStakeValue = jasmine.createSpy().and.returnValue(0);
      createService();

      const bet = {
        stake: {
          perLine: 1,
          lines: 1,
          freeBetAmount: 3
        },
        selectedFreeBet: {
          value: '3'
        },
        potentialPayout: 20,
        stakeMultiplier: 1,
        Bet: {
          isEachWay: true,
          payout: [{ potential: 20, legType: 'P' }]
        },
        outcomes: [{
          eachWayFactorNum: 1,
          eachWayFactorDen: 3,
          price: {
            priceNum: 3,
            priceDen: 1
          }
        }, {
          eachWayFactorNum: 1,
          eachWayFactorDen: 3,
          price: {
            priceNum: 3,
            priceDen: 1
          }
        }]
      } as any;

      expect(service.calculateEstReturnsMultiples(bet)).toEqual(69);
    });

    it('should calculate return N/A for each way bet with SP price', () => {
      bsFiltersService.filterStakeValue = jasmine.createSpy().and.returnValue(0);
      createService();

      const bet = {
        stake: {
          perLine: 1,
          lines: 1,
          freeBetAmount: 3
        },
        selectedFreeBet: {
          value: '3'
        },
        potentialPayout: 20,
        stakeMultiplier: 1,
        isSP: true,
        Bet: {
          isEachWay: true,
          payout: [{ potential: 20, legType: 'P' }]
        },
        outcomes: [{
          eachWayFactorNum: 1,
          eachWayFactorDen: 3,
          price: {
            priceNum: 3,
            priceDen: 1
          }
        }]
      } as any;

      expect(service.calculateEstReturnsMultiples(bet)).toEqual('N/A');
    });

    it('should return N/A if potentialPayout is 0 and Bet data is empty', () => {
      const bet = {
        stake: {
          lines: 1,
          freeBetAmount: 0
        },
        potentialPayout: 0,
        Bet: {}
      } as any;

      expect(service.calculateEstReturnsMultiples(bet)).toEqual('N/A');
    });

    it('should return N/A if potentialPayout is not 0 but Bet data is empty', () => {
      bsFiltersService.filterStakeValue = jasmine.createSpy().and.returnValue(0);
      createService();

      const bet = {
        stake: {
          lines: 1,
          freeBetAmount: 0
        },
        potentialPayout: 10,
        isSP: true,
        Bet: {}
      } as any;

      expect(service.calculateEstReturnsMultiples(bet)).toEqual('N/A');

    });

    describe('for overask (bet.isTraderOffered)', () => {
      it('should calculate est returns as potentialPayout if oddsBoostEnabled', () => {
        service.oddsBoostEnabled = true;

        const bet = {
          isTraderOffered: true,
          stake: {
            perLine: 1,
            lines: 1,
            freeBetAmount: 0
          },
          selectedFreeBet: {
            value: '3'
          },
          potentialPayout: '20',
          Bet: {}
        } as any;

        expect(service.calculateEstReturnsMultiples(bet)).toEqual(20);
      });

      it('should returns potentialPayout even if boosted', () => {
        service.oddsBoostEnabled = true;

        const bet = {
          isTraderOffered: true,
          stake: {
            perLine: 1,
            lines: 1,
            freeBetAmount: 0
          },
          potentialPayout: '20',
          Bet: {
            oddsBoost: {
              enhancedOddsPrice: '40'
            }
          }
        } as any;

        expect(service.calculateEstReturnsMultiples(bet)).toEqual(20);
      });

      it('should return payout from trader offer if not SP', () => {
        const bet = {
          isTraderOffered: true,
          stake: { lines: 1 },
          Bet: {},
          potentialPayout: 1,
          selectedFreeBet: { value: 1 }
        } as any;

        expect(service.calculateEstReturnsMultiples(bet as any)).toBe(1);
      });

      it('should return N/A for SP price', () => {
        const bet = {
          isSP: true,
          isTraderOffered: true,
          stake: { lines: 1 },
          Bet: {},
          potentialPayout: 1,
          selectedFreeBet: { value: 1 }
        } as any;

        bsFiltersService.filterStakeValue.and.returnValue(1);
        expect(service.calculateEstReturnsMultiples(bet as any)).toBe('N/A');
      });

      it('should return N/A if no payout', () => {
        const bet = {
          isSP: false,
          isTraderOffered: true,
          stake: { lines: 1 },
          Bet: {},
          selectedFreeBet: { value: 1 }
        } as any;

        expect(service.calculateEstReturnsMultiples(bet as any)).toBe('N/A');
      });
    });
  });

  describe('getTotalStake', () => {
    it('getTotalStake', () => {
      service['getSelectedBets'] = () => [
        { stake: {} },
        { stake: { amount: '10', disabled: true }, selectedFreeBet: {}, disabled: true},
        { stake: { amount: '10', lines: 2, perLine: 2, freeBetAmount: 5.27 }, selectedFreeBet: { value: '10.555' }}] as any;

      expect(service.getTotalStake([])).toEqual('14.54');
    });

    it('getTotalStake: should return sum of all(active and suspended) stakes', () => {
      service['getSelectedBets'] = () => [
        { stake: {} },
        { stake: { amount: '10', lines: 1, perLine: 1 , disabled: true }, disabled: true},
        { stake: { amount: '10', lines: 2, perLine: 2, freeBetAmount: 5.27 }, selectedFreeBet: { value: '10.555' }}] as any;

      expect(service.getTotalStake([], true)).toEqual('15.54');
    });

    it('getTotalStake: should return sum of all(active and suspended) stakes with freebets', () => {
      service['getSelectedBets'] = () => [
        { stake: {} },
        { stake: { amount: '10', lines: 1, perLine: 1 , disabled: true, freeBetAmount: 2 },
          selectedFreeBet: { value: '2.00' }, disabled: true},
        { stake: { amount: '10', lines: 2, perLine: 2, freeBetAmount: 5.27 },
          selectedFreeBet: { value: '10.555' }, disabled: true}] as any;

      expect(service.getTotalStake([], true)).toEqual('17.54');
    });

    it('getTotalStake: should return totalStake with freeBets for isEachWay and ForeCast market', () => {
      service['getSelectedBets'] = () => [
        { stake: { amount: 0.2, lines: 2, perLine: '0.1', freeBetAmount: 0.5 },
          selectedFreeBet: { value: '1.00' }, Bet: { isEachWay: true }},
        { stake: { amount: 0.6, lines: 6, perLine: '0.1', freeBetAmount: 0.16 },
          selectedFreeBet: { value: '1.00' }, Bet: { isEachWay: false }}] as any;

      expect(service.getTotalStake([], true)).toEqual('2.76');
    });

    it('should not include freebets if overask offer page', () => {
      service['overAskService']['userHasChoice'] = true;

      const bets = [{
        stake: { amount: '10', lines: 1, perLine: 10, disabled: true, freeBetAmount: 2 },
        selectedFreeBet: { },
        Bet: {},
        isSelected: true
      }] as any;

      expect(service['getTotalStake'](bets, true)).toEqual('10.00');
    });
    it('should include freebets if overask not in process', () => {
      service['overAskService']['hasTraderMadeDecision'] = false;
      service['overAskService']['isInProcess'] = false;

      const bets = [{
        stake: { amount: '10', lines: 1, perLine: 10, disabled: true, freeBetAmount: 2 },
        selectedFreeBet: { },
        Bet: {},
        isSelected: true
      }] as any;

      expect(service['getTotalStake'](bets, true)).toEqual('12.00');
    });
  });

  describe('getFreeBetStake', () => {
    it('should return sum of active free bets', () => {
      service['getSelectedBets'] = () => [
        { stake: {} },
        { stake: { amount: '10', disabled: true }, selectedFreeBet: {}, disabled: true},
        { stake: { amount: '10', lines: 2, perLine: 2 }, selectedFreeBet: { value: '10.555' }}] as any;

      expect(service.getFreeBetStake([])).toEqual('10.55');
    });

    it('should return sum of all(active and suspended) free bets', () => {
      service['getSelectedBets'] = () => [
        { stake: {} },
        { stake: { amount: '10', lines: 1, perLine: 1 , disabled: true }, disabled: true},
        { stake: { amount: '10', lines: 2, perLine: 2 }, selectedFreeBet: { value: '10.555' }}] as any;

      expect(service.getFreeBetStake([], true)).toEqual('10.55');
    });

    it('should return sum of all(active and suspended) free bets(2)', () => {
      service['getSelectedBets'] = () => [
        { stake: {} },
        { stake: { amount: '10', lines: 1, perLine: 1 , disabled: true }, selectedFreeBet: { value: '2.00' }, disabled: true},
        { stake: { amount: '10', lines: 2, freeBetAmount: 2 }, selectedFreeBet: { value: '10.00' }, disabled: true}] as any;

      expect(service.getFreeBetStake([], true)).toEqual('6.00');
    });

    it('should retrun sum of tokenValue for not disabled bets if overask', () => {
      overAskService.userHasChoice = true;
      service['getSelectedBets'] = () => [
        { stake: {} },
        { tokenValue: 5, disabled: true },
        { tokenValue: 10 }
      ] as any;

      expect(service.getFreeBetStake([], true)).toEqual('10.00');
    });
  });
});
