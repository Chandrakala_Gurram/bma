import { Injectable } from '@angular/core';
import { BetslipApiModule } from '@betslipModule/betslip-api.module';
import * as _ from 'underscore';

import environment from '@environment/oxygenEnvConfig';
import { BET_TYPES } from '@betslip/constants/bet-slip.constant';

import { LocaleService } from '@core/services/locale/locale.service';
import { InfoDialogService } from '@coreModule/services/infoDialogService/info-dialog.service';

@Injectable({ providedIn: BetslipApiModule })
export class BetInfoDialogService {
  private racingCategories: { [sport: string]: { id: string } };

  constructor(
    private localeService: LocaleService,
    private infoDialogService: InfoDialogService,
  ) {
    this.racingCategories = environment.CATEGORIES_DATA.racing;
  }

  /**
   * Indicates whether it is a racing
   * @param {string} sportId
   * @return {boolean}
   */
  isRacing(sportId: string): boolean {
    return _.some(this.racingCategories, sport => sport.id === sportId);
  }

  /**
   * Open dialog with multiples info
   * @param {string} betType
   * @param {number} stakeMultiplier
   */
  multiple(betType: string, stakeMultiplier: number): void {
    this.infoDialogService.openInfoDialog(
      this.getDialogTitle(betType),
      this.getMultiplesInfo(betType, stakeMultiplier),
      'bs-selection-info-dialog',
      undefined,
      undefined,
      [
        {
          caption: this.localeService.getString('bs.ok'),
          cssClass: 'btn-style2',
          handler: () => {
            this.infoDialogService.closePopUp();
          }
        }
      ]
    );
  }

  /**
   * returns multiple info
   * @param {string} betTypeName
   * @param {number} stakeMultiplier
   * @return {string}
   */
  private getMultiplesInfo(betTypeName: string, stakeMultiplier: number): string {
    const generalBetType = this.getGeneralBetType(betTypeName);
    const text = this.localeService.getString(`bs.${generalBetType}_dialog_info`);
    return text === 'KEY_NOT_FOUND' ? this.localeService.getString('bs.betsNumber', [stakeMultiplier]) :
      this.localeService.getString(`bs.${generalBetType}_dialog_info`);
  }

  private getGeneralBetType(betType: string): string {
    if (betType.indexOf(BET_TYPES.accumulatorBet) !== -1) {
      return `${BET_TYPES.accumulatorBet}_common`;
    } else if (betType.indexOf(BET_TYPES.singleAboutBet) !== -1) {
      return `${BET_TYPES.singleAboutBet}_common`;
    } else if (betType.indexOf(BET_TYPES.doubleAboutBet) !== -1) {
      return `${BET_TYPES.doubleAboutBet}_common`;
    } else {
      return betType;
    }
  }

  private getDialogTitle(betType: string): string {
    return this.localeService.getString(`bs.${this.getGeneralBetType(betType)}`);
  }
}
