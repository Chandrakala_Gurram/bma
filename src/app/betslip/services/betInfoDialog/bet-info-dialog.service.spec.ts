import { fakeAsync, tick } from '@angular/core/testing';

import { BetInfoDialogService } from './bet-info-dialog.service';
import { LocaleService } from '@core/services/locale/locale.service';
import environment from '@environment/oxygenEnvConfig';

describe('BetInfoDialogService', () => {

  let service: BetInfoDialogService;
  let localeService: any = LocaleService;
  let infoDialogService;
  let buttons;
  let env;

  beforeEach(() => {
    env = <any>environment;

    localeService = {
      getString: jasmine.createSpy('getString').and.callFake(value => value)
    } as any;

    infoDialogService = {
      openInfoDialog: jasmine.createSpy('openInfoDialog').and.callFake((title, txt, cssClass, id, onClose, btns) => {
        buttons = btns;
      }),
      closePopUp: jasmine.createSpy('closePopUp')
    };

    service = new BetInfoDialogService(
      localeService,
      infoDialogService
    );
  });

  it('should check if it is sport category is racing', () => {
    const horseRacingCategory = env.CATEGORIES_DATA.racing.horseracing;
    const greyhoundCategory = env.CATEGORIES_DATA.racing.greyhound;
    const footballCategory = env.CATEGORIES_DATA.footballId;

    expect(service.isRacing(horseRacingCategory.id)).toBeTruthy();
    expect(service.isRacing(greyhoundCategory.id)).toBeTruthy();
    expect(service.isRacing(footballCategory.id)).toBeFalsy();
  });

  it('should show multiples pop-up', fakeAsync(() => {
    service.multiple('ACC4', 4);
    tick();

    expect(infoDialogService.openInfoDialog).toHaveBeenCalledWith(
      'bs.AC_common',
      'bs.AC_common_dialog_info',
      'bs-selection-info-dialog',
      undefined,
      undefined,
      [
        jasmine.objectContaining({
          caption: 'bs.ok',
          cssClass: 'btn-style2'
        })
      ]
    );
  }));

  it('should close multiples pop-up on button click', fakeAsync(() => {
    service.multiple('ACC4', 4);
    tick();
    expect(infoDialogService.openInfoDialog).toHaveBeenCalled();
    buttons[0].handler();
  }));

  describe('getMultiplesInfo', () => {
    it('should return info for accumulator bet', () => {
      service['getMultiplesInfo']('ACC4', 4);
      expect(localeService.getString).toHaveBeenCalledWith('bs.AC_common_dialog_info');
    });

    it('should return info for single bet about', () => {
      service['getMultiplesInfo']('SS3', 4);
      expect(localeService.getString).toHaveBeenCalledWith('bs.SS_common_dialog_info');
    });

    it('should return info for double bet about', () => {
      service['getMultiplesInfo']('DS3', 4);
      expect(localeService.getString).toHaveBeenCalledWith('bs.DS_common_dialog_info');
    });

    it('should return info for main bets', () => {
      service['getMultiplesInfo']('DBL', 4);
      expect(localeService.getString).toHaveBeenCalledWith(`bs.DBL_dialog_info`);
    });

    it('should return number of bets', () => {
      localeService.getString.and.returnValue('KEY_NOT_FOUND');
      service['getMultiplesInfo']('TST', 4);
      expect(localeService.getString).toHaveBeenCalledWith('bs.betsNumber', [4]);
    });
  });
});
