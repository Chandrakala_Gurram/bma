import { ITokenPossibleBet } from '@app/bpp/services/bppProviders/bpp-providers.model';

export interface IFreebetObj {
  freebet: IFreeBet;
}

export interface IFreeBet {
  id?: string;
  name?: string;
  value?: number;
  expireAt?: Date;
  doc?: Function;
  [ key: string ]: any;
  type: string;
  possibleBets?: ITokenPossibleBet[];
}
