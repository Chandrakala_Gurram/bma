import { FreeBet } from './free-bet';

describe('FreeBet', () => {
  it('should create freebet', () => {
    const params = {
      id: '1',
      name: 'FB',
      value: 10,
      expireAt: new Date(2019, 0),
      type: 'HR'
    };

    const userService: any = {
      currencySymbol: '$'
    };

    const freeBet = new FreeBet(params, userService);

    expect(freeBet.id).toBe(params.id);
    expect(freeBet.name).toEqual(`${userService.currencySymbol}${params.value} ${params.name}`);
    expect(freeBet.value).toBe(params.value);
    expect(freeBet.expireAt).toBe(params.expireAt);
    expect(freeBet.cleanName).toBe(params.name);
    expect(freeBet.type).toBe(params.type);
    expect(freeBet.doc()).toEqual({
      freebet: { id: params.id }
    } as any);
  });

  it('should create freebet with possible bet', () => {
    const freeBet = new FreeBet({
      possibleBets: [{ name: 'Football' }]
    } as any, {} as any);
    expect(freeBet['possibleBet']).toEqual(' (Football)');
  });
});
