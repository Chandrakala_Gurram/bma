import { IBetSelection } from '@betslip/services/betSelection/bet-selection.model';
import { IRacingEvent } from '@core/models/racing-event.model';
import { IOutcome } from '@core/models/outcome.model';
import { IPoolBet } from '@app/bpp/services/bppProviders/bpp-providers.model';

export interface IToteBet extends IBetSelection {
  channelIds: string[];
  events: IRacingEvent[];
  isTote: boolean;
  outcomes: IOutcome[];
  poolBet: IPoolBet;
  toteBetDetails: IToteBetDetails;
  poolCurrencyCode: string;
}

export interface IToteLeg {
  eventTitle: string;
  name: string;
  outcomes: IOutcome[];
}

export interface IToteBetDetails {
  poolName: string;
  stakeRestrictions: IStakeRestrictions;
  betName?: string;
  correctedDay?: string;
  eventTitle?: string;
  isPotBet?: boolean;
  numberOfLines?: number;
  orderedLegs?: IToteLeg[];
  orderedOutcomes?: IOutcome[];
  showOrderPosition?: boolean;
}

export interface IStakeRestrictions {
  maxStakePerLine: string;
  maxTotalStake: string;
  minStakePerLine: string;
  minTotalStake: string;
  stakeIncrementFactor: string;
}
