const events = [{
  categoryCode: 'BASKETBALL',
  categoryId: '6',
  classId: 51,
  typeId: 136,
  id: '111',
  selectionId: 1,
  marketId: 1,
  name: 'EventName1'
}, {
  categoryCode: 'FOOTBALL',
  categoryId: '16',
  classId: 97,
  typeId: 435,
  id: '222',
  selectionId: 1,
  marketId: 1,
  name: 'EventName2'
}];

const bets = [{
  betId: '381522',
  stake: '1.00',
  numLegs: '1',
  numLines: '1',
  stakePerLine: '1.00',
  betType: 'SGL',
  potentialPayout: '1.50',
  status: 'A',
  leg: [{
    part: [{
      outcome: '449905491',
      priceNum: '1',
      priceDen: '2',
      handicap: '',
      eventId: '6702230'
    }]
  }]
}, {
  betId: '381523',
  stake: '1.00',
  numLegs: '1',
  numLines: '1',
  stakePerLine: '1.00',
  betType: 'SGL',
  potentialPayout: '3.00',
  status: 'A',
  leg: [{
    legSort: 'SF',
    part: [{
      outcome: '449905612',
      priceNum: '2',
      priceDen: '1',
      handicap: '',
      eventId: '6702260'
    }]
  }]
}, {
  betId: '381524',
  stake: '1.00',
  numLegs: '2',
  numLines: '1',
  stakePerLine: '1.00',
  betType: 'DBL',
  potentialPayout: '4.50',
  status: 'A',
  leg: [{
    part: [{
      outcome: '449905491',
      priceNum: '1',
      priceDen: '2',
      handicap: '',
      eventId: '6702230'
    }]
  }, {
    part: [{
      outcome: '449905612',
      priceNum: '2',
      priceDen: '1',
      handicap: '',
      eventId: '6702260'
    }]
  }]
}];

export const betReceiptsMock = {
  receiptEventsMock: {
    multiples: [
      {
        betTypeName: 'Double',
        receipt: 'O/11',
        stake: '5.00',
        numLegs: '1',
        numLines: '1',
        potentialPayout: '5.00',
        leg: [
          { part: [ { event: events[1], isFootball: true, outcome: 1, marketId: 1, eventMarketDesc: 'Test Market Name' } ] },
          { part: [ { event: events[0], outcome: 1, marketId: 1, eventMarketDesc: 'Test Market Name' } ] }
        ]
      }
    ],
    singles: [
      {
        betId: '123',
        betType: 'type',
        receipt: 'O/22',
        betTypeName: 'Single',
        stake: '5.00',
        eventMarket: 'Match Result',
        numLegs: '1',
        numLines: '1',
        potentialPayout: '5.00',
        leg: [
          { part: [ { event: events[0], outcome: 1, marketId: 1 } ] }
        ],
        oddsBoosted: true
      },
      {
        betId: '456',
        betType: 'type',
        receipt: 'O/22',
        betTypeName: 'Single',
        stake: '10.00',
        eventMarket: 'Match Result',
        numLegs: '1',
        numLines: '1',
        potentialPayout: '20.00',
        leg: [
          { part: [ { event: events[1], outcome: 1, marketId: 1 } ] }
        ],
        isFootball: true
      }
    ]
  },
  eventsInReceipt: [...events, events[1], events[1]],
  ids: [ 381480, 381481],
  receiptData: {
    response: {
      respTransGetBetDetail: {
        bet: bets
      }
    }
  },
  eventIds: [6702230, 6702260, 6702230, 6702260],
  events: [
    { id: 6702260, name: 'Crystal Palace v West Brom', categoryId: '16', categoryCode: 'FOOTBALL' },
    { id: 6702230, name: 'Chelsea v Hull', categoryId: '16', categoryCode: 'FOOTBALL' }
  ],
};
