import { from as observableFrom, Observable, of as observableOf } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { BetslipApiModule } from '@betslipModule/betslip-api.module';
import { IBetSelection } from '@betslip/services/betSelection/bet-selection.model';
import { BetslipStorageService } from '@betslip/services/betslip/betslip-storage.service';
import * as _ from 'underscore';
import { StorageService } from '@core/services/storage/storage.service';
import { BppService } from '@app/bpp/services/bpp/bpp.service';
import { SiteServerService } from '@core/services/siteServer/site-server.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { FracToDecService } from '@core/services/fracToDec/frac-to-dec.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { DeviceService } from '@core/services/device/device.service';
import { CoreToolsService } from '@core/services/coreTools/core-tools.service';
import environment from '@environment/oxygenEnvConfig';
import { OUTRIGHTS_CONFIG } from '@core/constants/outrights-config.constant';
import { ISportEvent } from '@core/models/sport-event.model';
import { IBetReceiptEntity } from './bet-receipt.model';
import { IMarket } from '@core/models/market.model';
import { IOutcome } from '@core/models/outcome.model';
import {
  IBetDetail,
  IBetDetailLeg,
  IBetDetailLegPart,
  IBetOdds,
  IBetTermsChange,
  IClaimedOffer,
  IRequestTransGetBetDetail,
  IResponseTransGetBetDetail
} from '@app/bpp/services/bppProviders/bpp-providers.model';
import { IGtmOrigin } from '@core/services/gtmTracking/models/gtm-origin.model';
import { AddToBetslipByOutcomeIdService } from '@betslip/services/addToBetslip/add-to-betslip-by-outcome-id.service';
import { GtmTrackingService } from '@core/services/gtmTracking/gtm-tracking.service';
import { BetslipDataService } from '@betslip/services/betslip/betslip-data.service';
import { UserService } from '@core/services/user/user.service';
import { LocaleService } from '@root/app/core/services/locale/locale.service';
import { TemplateService } from '@root/app/shared/services/template/template.service';
import { TimeService } from '@core/services/time/time.service';
import { BetDetailUtils } from '@app/bpp/services/bppProviders/bet-detail.utils';
import { SportsConfigService } from '@sb/services/sportsConfig/sports-config.service';
import { ISportInstance, ISportConfig } from '@core/services/cms/models';
import { UNNAMED_FAVOURITES } from '@core/services/raceOutcomeDetails/race-outcome.constant';

interface IMessage {
  type?: string;
  msg?: string;
}

@Injectable({ providedIn: BetslipApiModule })
export class BetReceiptService {
  ids: string[];
  message: IMessage = {
    type: undefined,
    msg: undefined
  };

  private receipts: IBetDetail[];
  private env: any = environment;
  private footballConfig: ISportConfig;
  private isBetCanceled: (stake: IBetDetail) => boolean = BetDetailUtils.isCanceled;

  constructor(
    private storageService: StorageService,
    private bppService: BppService,
    private siteServerService: SiteServerService,
    private pubSubService: PubSubService,
    private fracToDecService: FracToDecService,
    private filtersService: FiltersService,
    private deviceService: DeviceService,
    private betslipStorageService: BetslipStorageService,
    private addToBetslipService: AddToBetslipByOutcomeIdService,
    private coreToolsService: CoreToolsService,
    private gtmTrackingService: GtmTrackingService,
    private betslipDataService: BetslipDataService,
    private user: UserService,
    private localeService: LocaleService,
    private templateService: TemplateService,
    private timeService: TimeService,
    private sportsConfigService: SportsConfigService
  ) {
    this.getEWTerms = this.getEWTerms.bind(this);
    this.getLinesPerStake = this.getLinesPerStake.bind(this);

    this.sportsConfigService.getSport('football').subscribe((footballConfig: ISportInstance) => {
      this.footballConfig = footballConfig.sportConfig;
    });
  }

  get freeBetStake(): string {
    return _.reduceRight(this.receipts, (sum: number, betReceipt: IBetDetail) => {
      return sum + Number(betReceipt.tokenValue);
    }, 0).toFixed(2);
  }

  get totalStake(): string {
    return this.getTotalStake();
  }

  get totalEstimatedReturns(): string {
    return this.getTotalReturns();
  }

  /**
   * Gets bets receipts.
   * @return {Promise}
   */
  getBetReceipts(): Observable<IBetReceiptEntity[] | void> {
    return this.ids
      ? this.bppService.send('getBetDetail',
        <IRequestTransGetBetDetail>{
          betId: this.ids,
          returnPartialCashoutDetails: 'Y'
        }).pipe(
        // extract receipt bets only
        map((receiptData: IResponseTransGetBetDetail): IBetDetail[] => receiptData.response.respTransGetBetDetail.bet),
        switchMap((receiptBets: IBetDetail[]) => {
          const filters = {
            includeUndisplayed: true,
            outcomesIds: this.getEventIds(receiptBets)
          };
          // get events for receipts
          return observableFrom(this.siteServerService.getEventsByOutcomeIds(filters, false)).pipe(
            map((events: ISportEvent[]) => {
              this.mergeEventsWithReceipts(events, receiptBets);
              this.markFootballReceipts(receiptBets);
              this.markBoostedReceipts(receiptBets);
              this.extendWithAccaInsuranceData(receiptBets);
              this.markForeCastTricastReceipts(receiptBets);
              this.updateVirtualEventNames(receiptBets);
              this.markBogReceipts(receiptBets);

              return this.divideOnSinglesAndMultiples(receiptBets);
            }));
        }))
      : observableOf(null);
  }

  getGtmObject(receipt: IBetReceiptEntity, totalStake: number) {
    const products = this.parseBetsFromReceipt(receipt);
    return {
      ecommerce: {
        purchase: {
          actionField: {
            id: `${products.map(i => i.id).sort()[0]}:${products.length}`,
            revenue: totalStake
          },
          products
        }
      }
    };
  }

  /**
   * Reuse outcome and build the same betslip.
   * @params {number[]} outcomesIdsList
   * @return {Promise<boolean | void>} - promise
   */
  reuse(outcomesIdsList?: string[]): Promise<boolean | void> {
    const outcomesIds = outcomesIdsList || this.getOutcomeIds();
    let selections$: Observable<boolean | void>;

    if (outcomesIds.length > 0) {
      selections$ = observableFrom(this.siteServerService.getEventsByOutcomeIds({ outcomesIds })).pipe(
        map(this.sortByOutcomeIds(outcomesIds)),
        switchMap(() => {
          return this.addToBetslipService.reuseSelections(outcomesIds, this.receipts);
        }),
        map(() => {
          this.gtmTrackingService.restoreGtmTracking(outcomesIds);
          return outcomesIds;
        }),
        map(() => {
          this.pubSubService.publish(this.pubSubService.API.HOME_BETSLIP);
          return outcomesIds;
        }),
        catchError(err => {
          console.warn('Error while getEventsByOutcomeIds (BetReceiptService.getEventsByOutcomeIds)', err);
          this.pubSubService.publish(this.pubSubService.API.HOME_BETSLIP);
          return observableOf(null);
        }));
    } else {
      this.pubSubService.publish(this.pubSubService.API.HOME_BETSLIP);
      selections$ = observableOf(true);
    }
    this.pubSubService.publish(this.pubSubService.API.REUSE_OUTCOME);
    this.message = { type: undefined, msg: undefined };
    return selections$.toPromise();
  }

  /**
   * Action on receipt 'Done' btn
   * - On done suspended outcomes saved in BetSlip
   * - Redirect to home page (close BetSlip)
   */
  done(): void {
    if (this.deviceService.isMobile) {
      // sync to `show-${this.sideClass}` in sidebar.component
      this.pubSubService.publish(this.pubSubService.API['show-slide-out-betslip'], false);
    } else {
      this.pubSubService.publish(this.pubSubService.API.HOME_BETSLIP);
    }

    const outcomesIds = this.getSuspendedOutcomeIds();

    if (outcomesIds.length > 0) {
      observableFrom(this.siteServerService.getEventsByOutcomeIds({ outcomesIds })).pipe(
        map(this.sortByOutcomeIds(outcomesIds)),
        switchMap(() => this.addToBetslipService.addToBetSlip(outcomesIds.join(','), true, true, false)),
        catchError(err => {
          console.error('Error while getEventsByOutcomeIds (BetReceiptService.getEventsByOutcomeIds)', err);
          return observableOf(null);
        }));
    }

    this.clearMessage();
  }

  /**
   * Add all football events to favourites from receipts.
   * @return array of football events
   */
  getActiveFootballEvents(receiptsEntity: IBetReceiptEntity): ISportEvent[] {
    const arr: ISportEvent[] = [];
    // adding all football events from singles to array

    _.each(receiptsEntity.singles, (receipt: IBetDetail): void => {
      receipt.isFootball && arr.push(receipt.leg[0].part[0].event);
    });
    // adding all football events from multuples to array
    _.each(receiptsEntity.multiples, (receipt: IBetDetail): void => {
      _.each(receipt.leg, (leg: IBetDetailLeg): void => {
        leg.part[0].isFootball && arr.push(leg.part[0].event);
      });
    });
    // return all events from singles and multiples without duplicates
    return _.uniq(arr);
  }

  /**
   * Add all events to favourites from receipts.
   * @return array of football events
   */
  getActiveSportsEvents(receiptsEntity: IBetReceiptEntity): ISportEvent[] {
    const arr: ISportEvent[] = [];

    _.each(receiptsEntity.singles, (receipt: IBetDetail): void => {
      const event = this.coreToolsService.deepClone(receipt.leg[0].part[0].event);
      event.selectionId = receipt.leg[0].part[0].outcome;
      event.marketId = receipt.leg[0].part[0].marketId;
      arr.push(event);
    });
    _.each(receiptsEntity.multiples, (receipt: IBetDetail): void => {
      _.each(receipt.leg, (leg: IBetDetailLeg): void => {
        const event = this.coreToolsService.deepClone(receipt.leg[0].part[0].event);
        event.selectionId = leg.part[0].outcome;
        event.marketId = leg.part[0].marketId;
        arr.push(event);
      });
    });
    // return all events from singles and multiples without duplicates
    return _.uniq(arr);
  }

  hasStake(receipt: IBetDetail): boolean {
    return Number(receipt.tokenValue) > 0 && Number(receipt.stake) - Number(receipt.tokenValue) > 0;
  }

  getStake(receipt: IBetDetail): number {
    return Number(receipt.stake) - Number(receipt.tokenValue);
  }

  getReceiptOdds(item: IBetDetail): string {
    return item.odds[this.user.oddsFormat];
  }

  setToggleSwitchId(receipt: IBetDetail): string {
    return `toggle-switch-betslip-${receipt.betId}`;
  }

  hasStakeMulti(receipt: IBetDetail): boolean {
    return Number(receipt.tokenValue) > 0 && Number(receipt.stakePerLine) * Number(receipt.numLines) - Number(receipt.tokenValue) > 0;
  }

  getStakeMulti(receipt: IBetDetail): number {
    return Number(receipt.stakePerLine) * Number(receipt.numLines) - Number(receipt.tokenValue);
  }

  getStakeTotal(receipt: IBetDetail): number {
    return Number(receipt.stakePerLine) * Number(receipt.numLines);
  }

  getEWTerms(legPart: IBetDetailLegPart): string {
    return this.localeService.getString('bs.oddsAPlaces', {
      num: legPart.eachWayNum,
      den: legPart.eachWayDen,
      arr: this.templateService.genEachWayPlaces(legPart, true)
    });
  }

  getLinesPerStake(receipt: IBetDetail): string {
    return this.localeService.getString('bs.linesPerStake', {
      lines: receipt.numLines,
      currency: this.user.currencySymbol,
      stake: Number(receipt.stakePerLine).toFixed(2)
    });
  }

  /**
   * Make price format according to User settings
   * @returns {String}
   */
  getFormattedPrice(receipt: IBetDetail): string {
    const potentialPayout = this.getMultiplePotentialPayout(receipt);

    return this.convertPotentialPayout(potentialPayout);
  }

  /**
   * Clear message
   */
  clearMessage(): void {
    this.message = { type: undefined, msg: undefined };
  }

  /**
   * Exclude extra place promo label for race unnamed favourites
   * @param selectionName
   */
  getExcludedDrillDownTagNames(selectionName: string): string {
    if (selectionName && UNNAMED_FAVOURITES.indexOf(selectionName.toLowerCase()) >= 0) {
      return 'MKTFLAG_EPR, EVFLAG_EPR';
    }

    return '';
  }

  private convertPotentialPayout(potentialPayout: string | number): string {
    if (this.user.oddsFormat === 'frac') {
      return this.fracToDecService.decToFrac(potentialPayout, true);
    } else {
      return Number(potentialPayout).toFixed(2);
    }
  }

  /**
   * Calculate potentialPayout for ACCA and Double,
   * it's made by multiplying all related singles dec prices
   * @param betslipStake {object}
   * @return potentialPayout {number}
   */
  private getMultiplePotentialPayout(receipt: IBetDetail): number {
    const newSinglesPrices = [];
    receipt.leg.forEach((leg: IBetDetailLeg) => {
      newSinglesPrices.push(Number(leg.odds.frac.split('/')[0]) / Number(leg.odds.frac.split('/')[1]) + 1);
    });

    return newSinglesPrices.reduce((prev: number, current: number) => prev * current);
  }

  private extendWithAccaInsuranceData(receiptBets: IBetDetail[]): void {
    const accaInsuranceBetsMap = {};

    _.each(this.betslipDataService.placedBets.bets, (placedBet: any) => {
      if (_.has(placedBet, 'claimedOffers') && this.isQualifiedAccaInsurance(placedBet.claimedOffers)) {
        accaInsuranceBetsMap[String(placedBet.id)] = placedBet;
      }
    });

    _.each(receiptBets, (bet: IBetDetail) => {
      const insuranceBet = accaInsuranceBetsMap[bet.betId];
      if (insuranceBet) {
        _.extend(bet, { claimedOffers: insuranceBet.claimedOffers });
      }
    });
  }

  private isQualifiedAccaInsurance(offers: IClaimedOffer[]): boolean {
    return _.some(offers, (offer: IClaimedOffer) => offer.status === 'qualified' && offer.offerCategory === 'Acca Insurance');
  }

  private parseBetsFromReceipt(receipt: IBetReceiptEntity) {
    const singles = receipt.singles && receipt.singles.length ? this.buildGtmObject(receipt.singles) : [];

    const multiples = receipt.multiples && receipt.multiples.length ? this.buildGtmObject(receipt.multiples, true) : [];

    return singles.concat(multiples);
  }

  private buildGtmObject(bets: IBetDetail[], isMultiple?: boolean) {
    const multText: string = 'multiple';
    let isSameCategory: boolean;
    let isSameType: boolean;
    let isSameMarket: boolean;
    return _.map(bets, (bet: IBetDetail) => {
      const outcomeIds = this.getOutcomeIds([bet]);

      let betOrigin;
      let odds;

      if (isMultiple) {
        isSameCategory = this.isSameCategory(bet);
        isSameType = this.isSameType(bet);
        isSameMarket = this.isSameMarket(bet);
      }

      if (isSameCategory || isSameType || isSameMarket) {
        betOrigin = this.compareAndSetBetOrigin(outcomeIds, multText);
        isSameMarket && (bet.eventMarket = this.getEventMarket(bet.leg[0], bet.numLines));
      } else {
        betOrigin = !isMultiple ? this.gtmTrackingService.getBetOrigin(outcomeIds[0]) : { location: multText, module: multText };
      }

      if (isMultiple || !bet.odds) {
        if (bet.stake && bet.potentialPayout && !isNaN(+bet.potentialPayout)) {
          odds = +(Number(bet.potentialPayout) / Number(bet.stake)).toFixed(2);
        } else {
          odds = 'SP';
        }
      } else {
        odds = bet.odds.dec === 'SP' ? 'SP' : +bet.odds.dec;
      }

      return this.getBetGtmObject({
        bet, isMultiple, multText, outcomeIds, betOrigin, odds, isSameCategory, isSameType, isSameMarket
      });
    });
  }

  private getBetGtmObject({
    bet, isMultiple, multText, outcomeIds, betOrigin, odds, isSameCategory, isSameType, isSameMarket
  }): {[key: string]: any} {
    const isStarted = _.some(bet.leg, (leg: IBetDetailLeg) => _.some(leg.part, (part: IBetDetailLegPart) =>
        part.event && part.event.isStarted));

    const isBuildYourBet = this.env.BYB_CONFIG && this.env.BYB_CONFIG.HR_YC_EVENT_TYPE_ID === Number(bet.leg[0].part[0].event.typeId);

    return {
      name: isMultiple ? bet.betTypeName : String(bet.leg[0].part[0].event.name),
      id: bet.receipt,
      price: Number(bet.stake || 0),
      dimension60: isMultiple ? multText : String(bet.leg[0].part[0].event.id),
      dimension61: outcomeIds.join(','),
      dimension62: isStarted ? 1 : 0,
      dimension63: isBuildYourBet ? 1 : 0,
      dimension64: isMultiple ? multText : betOrigin.location,
      dimension65: isMultiple ? multText : betOrigin.module,
      dimension66: Number(bet.numLines),
      dimension67: odds,
      dimension86: bet.oddsBoosted ? 1 : 0,
      metric1: Number(bet.tokenValue || 0),
      category: isMultiple && !isSameCategory ? multText : bet.leg[0].part[0].event.categoryId,
      variant: isMultiple && !isSameType ? multText : bet.leg[0].part[0].event.typeId,
      brand: isMultiple && !isSameMarket ? multText : bet.eventMarket,
      quantity: 1
    };
  }

  private isSameCategory(bet: IBetDetail): boolean {
    const categoryId = bet.leg[0].part[0].event.categoryId;

    return bet.leg.every(legInstance => {
      return categoryId === legInstance.part[0].event.categoryId;
    });
  }

  private isSameType(bet: IBetDetail): boolean {
    const typeId = bet.leg[0].part[0].event.typeId;

    return bet.leg.every(legInstance => {
      return typeId === legInstance.part[0].event.typeId;
    });
  }

  private isSameMarket(bet: IBetDetail): boolean {
    const eventMarketDesc = bet.leg[0].part[0].eventMarketDesc;

    return bet.leg.every(legInstance => {
      return eventMarketDesc === legInstance.part[0].eventMarketDesc;
    });
  }

  private compareAndSetBetOrigin(outcomeIds: string[], multText: string): IGtmOrigin {
    const originsArr = [];
    outcomeIds.forEach(id => {
      originsArr.push(this.gtmTrackingService.getBetOrigin(id));
    });
    const locationName = originsArr[0].location;
    const moduleName = originsArr[0].module;
    const isOneLocation = originsArr.every(betOrigin => {
      return locationName === betOrigin.location;
    });
    const isOneModule = originsArr.every(betOrigin => {
      return moduleName === betOrigin.module;
    });

    return {
      location: isOneLocation ? locationName : multText,
      module: isOneModule ? moduleName : multText
    };
  }

  /**
   * Calculate total stake
   * @param {Array} [betReceipts]
   * @returns {String}
   */
  private getTotalStake(betReceipts: IBetDetail[] = this.receipts): string {
    return _.reduceRight(betReceipts, (sum: number, betReceipt: IBetDetail): number => {
      return sum + (Number(betReceipt.stakePerLine) * Number(betReceipt.numLines));
    }, 0).toFixed(2);
  }

  /**
   * Calculate total estimated returns
   * @param {Array} [betReceipts]
   * @returns {String}
   */
  private getTotalReturns(betReceipts: IBetDetail[] = this.receipts): string {
    const isAnyNA = _.some(betReceipts, (betReceipt: IBetDetail): boolean => {
      return _.isNaN(Number(betReceipt.potentialPayout));
    });

    if (isAnyNA) {
      return 'N/A';
    }

    const result = _.reduceRight(betReceipts, (sum: number, betReceipt: IBetDetail): number => {
      return sum + Number(betReceipt.potentialPayout);
    }, 0);

    return result > 0 ? result.toFixed(2) : 'N/A';
  }

  private getEventIds(receiptBets: IBetDetail[]): string[] {
    const arr = [];
    _.each(receiptBets, (bet: IBetDetail): void => {
      _.each(bet.leg, (leg: IBetDetailLeg): void => {
        _.each(leg.part, (part: IBetDetailLegPart): void => {
          arr.push(part.outcome);
        });
      });
    });
    return arr;
  }

  /**
   * Extend betreceipt with full event data
   * @return object of event
   */
  private mergeEventsWithReceipts(events: ISportEvent[], receiptBets: IBetDetail[]): void {
    _.each(receiptBets, (receipt: IBetDetail): void => {
      // extending event from single receipt with all corresponding event data
      if (receipt.betType === 'SGL') {
        _.each(events, (event: ISportEvent): void => {
          if (Number(receipt.leg[0].part[0].eventId) === Number(event.id)) {
            receipt.leg[0].part[0].event = event;
          }
        });
        // extending events from multiple receipt with all corresponding events data
      } else {
        _.each(events, (event: ISportEvent): void => {
          _.each(receipt.leg, (leg: IBetDetailLeg): void => {
            _.each(leg.part, (part: IBetDetailLegPart): void => {
              if (Number(part.eventId) === Number(event.id)) {
                part.event = event;
              }
            });
          });
        });
      }
    });
  }

  /**
   * Check for football events from receipts.
   * @return Boolean (true for football).
   */
  private markFootballReceipts(receiptsEntity: IBetDetail[]): void {
    _.each(receiptsEntity, (receipt: IBetDetail): void => {
      // check for football events from singles
      if (receipt.betType === 'SGL') {
        if (receipt.leg[0].part[0].event.categoryId === this.footballConfig.config.request.categoryId &&
          !this.isSpecialEvent(receipt.leg[0].part[0].event)) {
          receipt.isFootball = true;
        }
        // check for football events from multiples
      } else {
        _.each(receipt.leg, (leg: IBetDetailLeg): void => {
          if (leg.part[0].event.categoryId === this.footballConfig.config.request.categoryId
            && !this.isSpecialEvent(leg.part[0].event)) {
            leg.part[0].isFootball = true;
          }
        });
      }
    });
  }

  private markBoostedReceipts(receiptsEntity: IBetDetail[]): void {
    _.each(receiptsEntity, (receipt: IBetDetail): void => {
      receipt.oddsBoosted = _.some(receipt.betTermsChange, (terms: IBetTermsChange) => terms.reasonCode === 'ODDS_BOOST');
    });
  }

  /**
   * Check for specials events (outrights and enchanced multiples)
   */
  private isSpecialEvent(event: ISportEvent): boolean {
    // Checks if event - Enhance Multiples.
    const isEnhanceMultiples = _.contains(this.footballConfig.specialsTypeIds, Number(event.typeId)),
      // Checks if event - OutRight.
      sortCodeList = _.indexOf(OUTRIGHTS_CONFIG.outrightsSports, event.categoryCode) !== -1
        ? OUTRIGHTS_CONFIG.outrightsSportSortCode : OUTRIGHTS_CONFIG.sportSortCode,
      isOutRight = sortCodeList.indexOf(event.eventSortCode) !== -1;

    // check if event special (Enhance Multiples or OutRight).
    return isEnhanceMultiples || isOutRight;
  }

  /**
   * Puts outcomes in order by they ids, same as in outcomesIds.
   *
   * @param outcomesIds
   * @returns {Function}
   */
  private sortByOutcomeIds(outcomesIds: string[]): (data: ISportEvent[]) => ISportEvent[] {
    return function (data: ISportEvent[]): ISportEvent[] {
      _.each(data, (event: ISportEvent): void => {
        _.each(event.markets, (market: IMarket) => {
          market.outcomes.sort((a: IOutcome, b: IOutcome): number => {
            const aIndex = outcomesIds.indexOf(a.id),
              bIndex = outcomesIds.indexOf(b.id);

            if (aIndex !== -1 && bIndex !== -1) {
              if (aIndex < bIndex) {
                return -1;
              } else if (aIndex > bIndex) {
                return 1;
              }
            }

            return 0;
          });
        });
      });

      return data;
    };
  }

  /**
   * Split bet receipts into 2 array and prepare data
   *  keeps only placed bets to work with (within this service),
   *  passes 2 entities with placed and all (including canceled) bets further
   *
   * @param  {Array} betsReceipts - array with bet receipts
   * @return {Object[]} - array of objects with singles and multiples
   */
  private divideOnSinglesAndMultiples(betsReceipts: IBetDetail[]): IBetReceiptEntity[] {
    betsReceipts = this.setOutcomeNames(betsReceipts);

    const
      allReceipts: IBetReceiptEntity = {
        singles: this.filterReceipts('filter', betsReceipts),
        multiples: this.filterReceipts('reject', betsReceipts)
      },
      activeReceipts: IBetReceiptEntity = {
        singles: allReceipts.singles.filter((bet: IBetDetail) => !this.isBetCanceled(bet)),
        multiples: allReceipts.multiples.filter((bet: IBetDetail) => !this.isBetCanceled(bet))
      };

    this.receipts = [].concat(activeReceipts.singles).concat(activeReceipts.multiples);

    return [allReceipts, activeReceipts];
  }

  /**
   * Add handicap values to names if available
   * @prams {array} receipts
   * @return {array} receipts
   */
  private setOutcomeNames(receiptsEntity: IBetDetail[]): IBetDetail[] {
    _.each(receiptsEntity, (receipt: IBetDetail): void => {
      _.each(receipt.leg, (leg: IBetDetailLeg): void => {
        _.each(leg.part, (part: IBetDetailLegPart): void => {
          if (part.handicap.length) {
            part.description = part.description + this.filtersService.makeHandicapValue(part.handicap);
            receipt.name = receipt.name + this.filtersService.makeHandicapValue(part.handicap);
          }
        });
      });
    });
    return receiptsEntity;
  }

  /**
   * Gets outcome ids which is already in betslip.
   * @return {[type]} [description]
   */
  private getOutcomesInBetSlip(): string[] {
    const betslip: IBetSelection[] = this.betslipStorageService.restore();

    return _.reduceRight(betslip, (ids: string[], outcome: IBetSelection): string[] => {
      return ids.concat(outcome.outcomesIds);
    }, []);
  }

  /**
   * Get suspended outcome ids
   */
  private getSuspendedOutcomeIds(): string[] {
    return this.storageService.get('betSuspendedSelections') || [];
  }

  /**
   * Gets outcome ids from bet receipts, betslip, suspended ids
   * @return {Array} - array with outcome ids.
   * @return {boolean} - falg to get outcomesIds only from source.
   */
  private getOutcomeIds(source?: IBetDetail[], sourceOnly: boolean = false): string[] {
    let outcomeIds = [];
    const receipts = source || this.receipts;

    _.each(receipts, (betReceipt: IBetDetail): void => {
      _.each(betReceipt.leg, (leg: IBetDetailLeg): void => {
        _.each(leg.part, (part: IBetDetailLegPart): void => {
          outcomeIds.push(part.outcome);
        });
      });
    });

    if (sourceOnly) {
      return _.uniq(outcomeIds);
    }

    outcomeIds = _.uniq(_.difference(outcomeIds, this.getOutcomesInBetSlip));
    return _.union(outcomeIds, this.getSuspendedOutcomeIds());
  }

  private getNameOfEvent(leg: IBetDetailLeg): string {
    return _.reduce(leg.part, (names: string[], part: IBetDetailLegPart): string[] => {
      names.push(part.description);
      return names;
    }, []).join(', ');
  }

  private getEventDesc(leg: IBetDetailLeg): string {
    return _.uniq(_.reduce(leg.part, (names: string[], part: IBetDetailLegPart): string[] => {
      names.push(part.eventDesc);
      return names;
    }, [])).join(', ');
  }

  private getEventMarket(leg: IBetDetailLeg, lines: string): string {
    const complexBet = this.addLegSortName(leg, lines);

    if (complexBet) {
      return complexBet;
    }

    return _.uniq(_.reduce(leg.part, (names: string[], part: IBetDetailLegPart): string[] => {
      names.push(part.eventMarketDesc);
      return names;
    }, [])).join(', ');
  }

  private addLegSortName(leg: IBetDetailLeg, lines: string): string {
    return {
      SF: 'Forecast',
      RF: 'Reverse Forecast 2',
      CF: `Combination Forecast ${lines}`,
      TC: 'Tricast',
      CT: `Combination Tricast ${lines}`
    }[leg.legSort];
  }

  /**
   * Odds converter
   * @param part
   * @returns {string}
   */
  private getOdds(part): IBetOdds {
    let frac = '',
      dec = '';

    if (part) {
      const isLP = part.priceNum && part.priceNum.length > 0;

      if (isLP) {
        frac = `${part.priceNum}/${part.priceDen}`;
        dec = this.fracToDecService.getDecimal(part.priceNum, part.priceDen).toString();
      } else {
        frac = 'SP';
        dec = 'SP';
      }
    }

    return { frac, dec };
  }

  /**
   * Gets clean bet.
   * @param  {Array} betsReceipts - response from service
   * @return {Array}              - array with bet receipts.
   */
  private getCleanBet(betsReceipts: IBetDetail[]): IBetDetail[] {
    return betsReceipts.reduce((array: IBetDetail[], rec: IBetDetail): IBetDetail[] => {
      if (rec.leg.length === 1) {
        const part = rec.leg[0].part[0];

        rec.name = this.getNameOfEvent(rec.leg[0]);
        rec.eventName = this.getEventDesc(rec.leg[0]);
        rec.eventMarket = this.getEventMarket(rec.leg[0], rec.numLines);
        rec.odds = this.getOdds(part);

        rec.startTime = part.startTime;
      } else {
        _.each(rec.leg, (item: IBetDetailLeg) => {
          item.odds = this.getOdds(item.part[0]);
        });
      }

      if (Number(rec.potentialPayout) === 0) {
        rec.potentialPayout = 'N/A';
      }

      array.push(rec);
      return array;
    }, []);
  }

  /**
   * Filter receipts on multiples and singles
   * @param  {String} method       - method which have to be used for filtering should be reject or filter
   * @param  {Array} betsReceipts  - array with bet receipts
   * @return {Array}               - singles or multiples bet receipts only
   */
  private filterReceipts(method: string, betsReceipts: IBetDetail[]): IBetDetail[] {
    return _[method](this.getCleanBet(betsReceipts), (receipt: IBetDetail): boolean => {
      return receipt.betType === 'SGL';
    });
  }

  /**
   * Best odds are guarantied when priceTypeCodes include GP (for Ladbrokes brand) or G (for Coral) and it is not a Greyhounds event
   * @param {IBetDetail[]} receiptBets
   */
  private markBogReceipts(receiptBets: IBetDetail[]): void {
    _.each(receiptBets, (betReceipt: IBetDetail): void => {
      _.each(betReceipt.leg, (leg: IBetDetailLeg): void => {
        _.each(leg.part, (part: IBetDetailLegPart): void => {
          part.isBog = (part && part.priceType && (part.priceType.includes('G') || part.priceType.includes('GP')))
            && !this.filtersService.isGreyhoundsEvent(part.eventCategoryId);
        });
      });
    });
  }

  private markForeCastTricastReceipts(receiptBets: IBetDetail[]): void {
    _.each(receiptBets, (receipt: IBetDetail) => {
      receipt.isFCTC = /^(SF|RF|CF|TC|CT)$/.test(receipt.leg[0].legSort);
    });
  }

  private updateVirtualEventNames(receiptBets: IBetDetail[]): void {
    _.each(receiptBets, (receipt: IBetDetail) => {
      _.each(receipt.leg, (leg: IBetDetailLeg) => {
        if (leg.part[0].event.sportId === environment.CATEGORIES_DATA.virtuals[0].id) {
          const cleanEventName = this.filtersService.clearEventName(leg.part[0].eventDesc);
          leg.part[0].eventDesc = `${this.timeService.formatByPattern(new Date(leg.part[0].event.startTime), 'HH:mm')} ${cleanEventName}`;
        }
      });
    });
  }
}
