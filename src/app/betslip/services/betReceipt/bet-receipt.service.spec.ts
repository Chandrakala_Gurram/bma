import { of as observableOf } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';

import { BetReceiptService } from './bet-receipt.service';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';
import { betReceiptsMock } from '@betslip/services/betReceipt/bet-receipt.service.mock';

describe('BetReceiptService', () => {
  let service;
  let storageService;
  let bppService;
  let siteServerService;
  let pubSubService;
  let fracToDecService;
  let filtersService;
  let deviceService;
  let betslipStorageService;
  let addToBetslipService;
  let coreToolsService;
  let gtmTrackingService;
  let mockServiceData;
  let betslipDataService;
  let userService;
  let localeService;
  let templateService;
  let timeService;
  let sportsConfigService;
  const mockUuid = '123';

  beforeEach(() => {
    storageService = jasmine.createSpyObj('storageService', ['get']);
    bppService = jasmine.createSpyObj('bppService', ['send']);
    siteServerService = {
      getEventsByOutcomeIds: jasmine.createSpy('getEventsByOutcomeIds').and.returnValue(Promise.resolve([])),
      getEvent: jasmine.createSpy('getEvent').and.returnValue(observableOf({}))
    };
    pubSubService = jasmine.createSpyObj('pubSubService', ['publish']);
    fracToDecService = jasmine.createSpyObj('fracToDecService', ['getDecimal', 'decToFrac']);
    filtersService = {
      clearEventName: jasmine.createSpy('clearEventName').and.returnValue('clearName'),
      makeHandicapValue: jasmine.createSpy('makeHandicapValue')
    };
    deviceService = { isMobile: false };
    betslipStorageService = jasmine.createSpyObj('betslipStorageService', ['restore']);
    addToBetslipService = jasmine.createSpyObj('addToBetslipService', ['addToBetSlip', 'reuseSelections']);
    coreToolsService = jasmine.createSpyObj('coreToolsService', ['uuid', 'deepClone']);

    pubSubService.API = pubSubApi;
    mockServiceData = JSON.parse(JSON.stringify(betReceiptsMock));
    fracToDecService.getDecimal.and.callFake((priceNum, priceDen) => (1 + (priceNum / priceDen)).toFixed(2));
    coreToolsService.uuid.and.returnValue(mockUuid);
    coreToolsService.deepClone.and.callFake(d => JSON.parse(JSON.stringify(d)));
    gtmTrackingService = {
      getBetOrigin: jasmine.createSpy().and.returnValue({
        location: 'testLocation',
        module: 'testModule'
      }),
      restoreGtmTracking: jasmine.createSpy()
    };
    betslipDataService = {
      placedBets: {
        bets: [
          {
            id: 10,
            claimedOffers: [{ status: 'qualified', offerCategory: 'test' }]
          },
          {
            id: 12
          },
          {
            id: 14,
            claimedOffers: [{ status: 'qualified', offerCategory: 'Acca Insurance' }]
          }
        ]
      }
    };
    userService = {
      oddsFormat: 'frac',
      currencySymbol: '$'
    };

    localeService = {
      getString: jasmine.createSpy('getString')
    };

    templateService = {
      genEachWayPlaces: jasmine.createSpy('genEachWayPlaces').and.returnValue('1-2-3-4')
    };

    timeService = {
      formatByPattern: jasmine.createSpy('formatByPattern').and.returnValue('12:35')
    };

    sportsConfigService = {
      getSport: jasmine.createSpy('getSport').and.returnValue(observableOf({
        sportConfig: {
          config: {
            request: {
              categoryId: '16'
            }
          }
        }
      }))
    };

    spyOn(console, 'warn');

    service = new BetReceiptService(
      storageService,
      bppService,
      siteServerService,
      pubSubService,
      fracToDecService,
      filtersService,
      deviceService,
      betslipStorageService,
      addToBetslipService,
      coreToolsService,
      gtmTrackingService,
      betslipDataService,
      userService,
      localeService,
      templateService,
      timeService,
      sportsConfigService
    );
  });

  it('should return valid object with eventsInReceipt after getActiveSportsEvents call', () => {
    const receiptEventsMock = Object.assign({}, mockServiceData.receiptEventsMock);

    const actualEvents = service.getActiveSportsEvents(receiptEventsMock);

    expect(actualEvents.length).toEqual(mockServiceData.eventsInReceipt.length);
    actualEvents.forEach(el => expect(mockServiceData.eventsInReceipt).toContain(jasmine.objectContaining(el)));
  });

  describe('getBetReceipts method', () => {
    beforeEach(() => {
      const receiptData = Object.assign({}, mockServiceData.receiptData);
      const events = Object.assign({}, mockServiceData.events);

      bppService.send.and.returnValue(observableOf(receiptData));
      siteServerService.getEvent.and.returnValue(Promise.resolve(events));
    });

    it('should not call getBetDetail', () => {
      service.getBetReceipts();
      expect(bppService.send).not.toHaveBeenCalled();
    });

    it('should test if getBetDetail and getEventsByOutcomeIds are called', fakeAsync(() => {
      spyOn(service, 'markFootballReceipts');
      spyOn(service, 'updateVirtualEventNames');
      const filters = {
        includeUndisplayed: true,
        outcomesIds: ['449905491', '449905612', '449905491', '449905612']
      };
      service.ids = mockServiceData.ids;
      service.getBetReceipts().subscribe(() => {
        expect(bppService.send).toHaveBeenCalledWith('getBetDetail', {
          betId: mockServiceData.ids,
          returnPartialCashoutDetails: 'Y'
        });
        expect(siteServerService.getEventsByOutcomeIds).toHaveBeenCalledWith(filters,  false);
      });

      tick();
    }));

    it('should test if mergeEventsWithReceipts and markFootballReceipts are called', fakeAsync(() => {
      spyOn(service, 'markFootballReceipts');
      spyOn(service, 'updateVirtualEventNames');
      spyOn(service, 'mergeEventsWithReceipts');
      const bets = mockServiceData.receiptData.response.respTransGetBetDetail.bet;
      service.ids = mockServiceData.ids;

      service.getBetReceipts().subscribe(() => {
        expect(service.markFootballReceipts).toHaveBeenCalledWith(bets);
        expect(service.mergeEventsWithReceipts).toHaveBeenCalledWith([], bets);
      });
      tick();
    }));

    it('should return observable of IBetReceiptEntity[]', fakeAsync(() => {
      spyOn(service, 'markFootballReceipts');
      spyOn(service, 'updateVirtualEventNames');
      service.ids = mockServiceData.ids;

      service.getBetReceipts().subscribe((result) => {
        expect(result.length).toEqual(2);
        expect(result[0].singles.length).toEqual(2);
        expect(result[0].multiples.length).toEqual(1);
      });
      tick();
    }));
  });

  describe('getGtmObject method', () => {
    it('', () => {
      expect(service.getGtmObject(mockServiceData.receiptEventsMock, '5')).toEqual(
        {
          ecommerce: {
            purchase: {
              actionField: {
                id: 'O/11:3',
                revenue: '5'
              },
              products: [{
                name: 'EventName1',
                id: 'O/22',
                price: 5,
                category: '6',
                variant: 136,
                brand: 'Match Result',
                dimension60: '111',
                dimension61: '1',
                dimension62: 0,
                dimension63: 0,
                dimension64: 'testLocation',
                dimension65: 'testModule',
                dimension66: 1,
                dimension67: 1,
                dimension86: 1,
                metric1: 0,
                quantity: 1
              }, {
                name: 'EventName2',
                id: 'O/22',
                price: 10,
                category: '16',
                variant: 435,
                brand: 'Match Result',
                dimension60: '222',
                dimension61: '1',
                dimension62: 0,
                dimension63: 0,
                dimension64: 'testLocation',
                dimension65: 'testModule',
                dimension66: 1,
                dimension67: 2,
                dimension86: 0,
                quantity: 1,
                metric1: 0,
              }, {
                name: 'Double',
                id: 'O/11',
                price: 5,
                category: 'multiple',
                variant: 'multiple',
                brand: 'Test Market Name',
                dimension60: 'multiple',
                dimension61: '1',
                dimension62: 0,
                dimension63: 0,
                dimension64: 'multiple',
                dimension65: 'multiple',
                dimension66: 1,
                dimension67: 1,
                dimension86: 0,
                metric1: 0,
                quantity: 1
              }]
            },
          }
        });
    });
  });

  describe('reuse', () => {
    it('reuse (outcomes availalbe)', fakeAsync(() => {
      const outcomesIds = ['1'];
      service['getOutcomeIds'] = () => outcomesIds;
      service.sortByOutcomeIds = jasmine.createSpy().and.returnValue(() => null);
      siteServerService.getEventsByOutcomeIds.and.returnValue(Promise.resolve(true));
      addToBetslipService.reuseSelections.and.returnValue(observableOf(null));

      expect(service.reuse()).toEqual(jasmine.any(Promise));

      expect(siteServerService.getEventsByOutcomeIds).toHaveBeenCalledWith(
        { outcomesIds }
      );
      tick();
      expect(service.sortByOutcomeIds).toHaveBeenCalledWith(outcomesIds);
      expect(addToBetslipService.reuseSelections).toHaveBeenCalledTimes(1);
      expect(gtmTrackingService.restoreGtmTracking).toHaveBeenCalledWith(outcomesIds);
      expect(pubSubService.publish).toHaveBeenCalledWith(pubSubApi.HOME_BETSLIP);
      expect(pubSubService.publish).toHaveBeenCalledWith('REUSE_OUTCOME');
      expect(service.message).toEqual({ type: undefined, msg: undefined });
    }));

    it('reuse (no outcomes)', () => {
      service['getOutcomeIds'] = () => [];
      expect(service.reuse()).toEqual(jasmine.any(Promise));
      expect(pubSubService.publish).toHaveBeenCalledWith(pubSubApi.HOME_BETSLIP);
      expect(pubSubService.publish).toHaveBeenCalledWith('REUSE_OUTCOME');
      expect(service.message).toEqual({ type: undefined, msg: undefined });
    });

    it('reuse (warn)', () => {
      const outcomesIdsList = ['1'];

      siteServerService.getEventsByOutcomeIds.and.returnValue(Promise.reject('error'));

      service.reuse(<any>outcomesIdsList).then(() => {
        expect(console.warn).toHaveBeenCalledWith(
          'Error while getEventsByOutcomeIds (BetReceiptService.getEventsByOutcomeIds)', 'error'
        );
      });
    });
  });

  describe('buildGtmObject', () => {
    let bet;

    beforeEach(() => {
      bet = {
        betTypeName: 'Bet Type Name',
        eventMarket: 'Event Market',
        receipt: '0000/12345678',
        stake: '15',
        potentialPayout: '30',
        odds: {
          dec: '1.20'
        },
        numLegs: '0',
        numLines: '0',
        tokenValue: '15',
        leg: [{
          part: [{
            event: {
              id: 111,
              name: 'EventName',
              categoryId: '555',
              typeId: '666',
              isStarted: true
            },
            eventMarketDesc: 'Test Market'
          }]
        }]
      };
      spyOn<any>(service, 'getOutcomeIds').and.returnValue(['55']);
    });

    it('should get bet origin for multiple', () => {
      service['buildGtmObject']([bet], true);
      expect(gtmTrackingService.getBetOrigin).toHaveBeenCalled();
    });

    it('should get bet origin for single', () => {
      service['buildGtmObject']([bet], false);
      expect(gtmTrackingService.getBetOrigin).toHaveBeenCalledWith('55');
    });

    it('should build GTM object for started event with number odds and price and freebet', () => {
      const result = service['buildGtmObject']([bet], false);

      expect(result).toEqual([{
        name: 'EventName',
        id: bet.receipt,
        price: 15,
        category: '555',
        variant: '666',
        brand: bet.eventMarket,
        dimension60: '111',
        dimension61: '55',
        dimension62: 1,
        dimension63: 0,
        dimension64: 'testLocation',
        dimension65: 'testModule',
        dimension66: 0,
        dimension67: 1.2,
        dimension86: 0,
        metric1: 15,
        quantity: 1
      }]);
    });

    it('should build GTM object for not started event with SP odds and without price and freebet', () => {
      bet.stake = null;
      bet.odds.dec = 'SP';
      bet.tokenValue = null;
      bet.leg[0].part[0].event.isStarted = false;

      const result = service['buildGtmObject']([bet], false);

      expect(result).toEqual([{
        name: 'EventName',
        id: bet.receipt,
        price: 0,
        category: '555',
        variant: '666',
        brand: bet.eventMarket,
        dimension60: '111',
        dimension61: '55',
        dimension62: 0,
        dimension63: 0,
        dimension64: 'testLocation',
        dimension65: 'testModule',
        dimension66: 0,
        dimension67: 'SP',
        dimension86: 0,
        metric1: 0,
        quantity: 1
      }]);
    });

    it('should build GTM object for multiple bets', () => {
      const result = service['buildGtmObject']([bet], true);
      expect(result).toEqual([{
        name: bet.betTypeName,
        id: bet.receipt,
        price: 15,
        dimension60: 'multiple',
        dimension61: '55',
        dimension62: 1,
        dimension63: 0,
        dimension64: 'multiple',
        dimension65: 'multiple',
        dimension66: 0,
        dimension67: 2,
        dimension86: 0,
        metric1: 15,
        category: '555',
        variant: '666',
        brand: 'Test Market',
        quantity: 1
      }]);
    });

    it('should build GTM object for multiple bets for SP odds', () => {
      let result;

      bet.potentialPayout = 'NOT_AVAILABLE';
      result = service['buildGtmObject']([bet], true);
      expect(result[0].dimension67).toEqual('SP');

      delete bet.potentialPayout;
      result = service['buildGtmObject']([bet], true);
      expect(result[0].dimension67).toEqual('SP');
    });
  });

  it('should extendWithAccaInsuranceData', () => {
    const receiptBets = [
      { betId: '10' },
      { betId: '12' },
      { betId: '13' },
      { betId: '14' }
    ];
    service['extendWithAccaInsuranceData'](receiptBets);
    expect(receiptBets[0].hasOwnProperty('claimedOffers')).toEqual(false);
    expect(receiptBets[1].hasOwnProperty('claimedOffers')).toEqual(false);
    expect(receiptBets[2].hasOwnProperty('claimedOffers')).toEqual(false);
    expect(receiptBets[3].hasOwnProperty('claimedOffers')).toEqual(true);
  });

  it('should get freeBetStake', () => {
    service['receipts'] = <any>[{ tokenValue: '2' }];
    expect(service.freeBetStake).toEqual('2.00');
  });

  it('should get totalStake', () => {
    service['receipts'] = <any>[{ stakePerLine: '2', numLines: '2' }];
    expect(service.totalStake).toEqual('4.00');
  });

  it('should get totalEstimatedReturns', () => {
    service['receipts'] = <any>[{ stakePerLine: '2', numLines: '2' }];
    expect(service.totalEstimatedReturns).toEqual('N/A');
  });

  describe('done', () => {
    it('should trigger publish if desktop', () => {
      deviceService.isMobile = false;
      service.done();

      expect(pubSubService.publish).toHaveBeenCalledWith(pubSubService.API.HOME_BETSLIP);
    });

    it('should clear message', () => {
      service.message.msg = 'Error';

      service.done();

      expect(service.message).toEqual({ type: undefined, msg: undefined });
    });

    it('should trigger done (mobile)', () => {
      deviceService.isMobile = true;
      service.done();
      expect(pubSubService.publish).toHaveBeenCalledWith('show-slide-out-betslip', false);
    });

    it('should trigger done (outcomesIds)', fakeAsync(() => {
      storageService.get.and.returnValue(['1', '2']);
      siteServerService.getEventsByOutcomeIds.and.returnValue(Promise.resolve([]));

      service.done();
      expect(siteServerService.getEventsByOutcomeIds).toHaveBeenCalledWith(jasmine.objectContaining({
        outcomesIds: ['1', '2']
      }));
    }));
  });

  it('should getActiveFootballEvents', () => {
    const receiptsEntity = {
      singles: [
        {
          isFootball: true,
          leg: [{ part: [{ event: {} }] }]
        },
        {
          isFootball: false
        }
      ],
      multiples: [
        {
          leg: [
            { part: [{ event: {}, isFootball: true }] },
            { part: [{ isFootball: false }] }
          ]
        }
      ]
    };
    expect(service.getActiveFootballEvents(<any>receiptsEntity).length).toEqual(2);
  });

  it('should parseBetsFromReceipt (no data)', () => {
    const receipt = {};
    expect(service['parseBetsFromReceipt'](<any>receipt)).toEqual([]);
  });

  it('should getTotalReturns', () => {
    const betReceipts = [{ potentialPayout: 4 }];
    expect(service['getTotalReturns'](<any>betReceipts)).toEqual('4.00');
  });

  describe('markFootballReceipts', () => {
    it('should markFootballReceipts (SGL)', () => {
      const receiptsEntity = [{ betType: 'SGL', isFootball: false, leg: [{ part: [{ event: {} }] }] }];
      service['markFootballReceipts'](<any>receiptsEntity);
      expect(receiptsEntity[0].isFootball).toEqual(false);
    });

    it('should markFootballReceipts', () => {
      const receiptsEntity = [{ betType: 'DBL', leg: [{ part: [{ event: {}, isFootball: false }] }] }];
      service['markFootballReceipts'](<any>receiptsEntity);
      expect(receiptsEntity[0].leg[0].part[0].isFootball).toEqual(false);
    });
  });

  it('should markBoostedReceipts', () => {
    const receiptsEntity = [{
      oddsBoosted: false,
      betTermsChange: [{ reasonCode: 'ODDS_BOOST' }, { reasonCode: 'PRICE_BOOST' }]
    }];
    service['markBoostedReceipts'](<any>receiptsEntity);
    expect(receiptsEntity[0].oddsBoosted).toEqual(true);
  });

  it('should sortByOutcomeIds', () => {
    const outcomesIds = ['1', '2', '3', '4'];
    const data = [
      {
        markets: [{
          outcomes: [
            { id: '2' },
            { id: '1' },
            { id: '3' },
            { id: '4' },
            { id: '5' }]
        }]
      }
    ];
    expect(service['sortByOutcomeIds'](<any>outcomesIds)(data)[0].markets[0].outcomes[0].id).toEqual('1');
    expect(service['sortByOutcomeIds'](<any>outcomesIds)(data)[0].markets[0].outcomes[2].id).toEqual('3');
  });

  it('should setOutcomeNames', () => {
    const receiptsEntity = [{
      name: 'receipt',
      leg: [
        {
          part: [
            { handicap: [] },
            { handicap: [{}], description: 'test' }
          ]
        }
      ]
    }];
    filtersService.makeHandicapValue.and.returnValue(1);
    service['setOutcomeNames'](<any>receiptsEntity);
    expect(receiptsEntity[0].leg[0].part[1].description).toEqual('test1');
    expect(receiptsEntity[0].name).toEqual('receipt1');
  });

  it('should getOutcomesInBetSlip', () => {
    betslipStorageService.restore.and.returnValue([{ outcomesIds: ['1', '2'] }, { outcomesIds: ['3', '4'] }]);
    expect(service['getOutcomesInBetSlip']()).toEqual(['3', '4', '1', '2']);
  });

  it('should getOutcomeIds', () => {
    const outcome = 4;
    const source = [
      {
        leg: [
          {
            part: [
              { outcome: outcome },
              { outcome: outcome }
            ]
          }
        ]
      }
    ];
    expect(service['getOutcomeIds'](<any>source, true)).toEqual([outcome]);
  });

  describe('getOutcomeIds', () => {
    it('should getOutcomeIds (!LP)', () => {
      const part = {
        priceNum: ''
      };
      expect(service['getOdds'](<any>part)).toEqual(jasmine.objectContaining({
        frac: 'SP',
        dec: 'SP'
      }));
    });

    it('should getOutcomeIds (!part)', () => {
      expect(service['getOdds']()).toEqual(jasmine.objectContaining({
        frac: '',
        dec: ''
      }));
    });
  });

  it('should getCleanBet', () => {
    const betsReceipts = [
      {
        potentialPayout: 0,
        leg: []
      }
    ];
    expect(service['getCleanBet'](<any>betsReceipts)[0].potentialPayout).toEqual('N/A');
  });

  describe('#getStakeMulti', () => {
    it('should call getStakeMulti method', () => {
      expect(service.getStakeMulti({
        stakePerLine: '2',
        numLines: '3',
        tokenValue: '3'
      } as any)).toEqual(3);
    });
  });

  describe('#hasStake', () => {
    it('should call hasStake method when token value more than 0 and stake less than token', () => {
      expect(service.hasStake({
        stake: '2',
        tokenValue: '3'
      } as any)).toEqual(false);
    });

    it('should call hasStake method when token value more than 0', () => {
      expect(service.hasStake({
        stake: '5',
        tokenValue: '3'
      } as any)).toEqual(true);
    });

    it('should call hasStake method when token value is 0', () => {
      expect(service.hasStake({
        stake: '2',
        tokenValue: '0'
      } as any)).toEqual(false);
    });
  });

  describe('#getStake', () => {
    it('should call getStake method', () => {
      expect(service.getStake({
        stake: '6',
        tokenValue: '3'
      } as any)).toEqual(3);
    });
  });

  describe('#getReceiptOdds', () => {
    it('should call getReceiptOdds method', () => {
      expect(service.getReceiptOdds({
        odds: {
          frac: '1/2'
        }
      } as any)).toEqual('1/2');
    });
  });

  describe('#setToggleSwitchId', () => {
    it('should call setToggleSwitchId method', () => {
      expect(service.setToggleSwitchId({
        betId: '22341241241241'
      } as any)).toEqual('toggle-switch-betslip-22341241241241');
    });
  });

  describe('#getFormattedPrice', () => {
    it('getFormattedPrice frac', () => {
      userService.oddsFormat = 'frac';
      fracToDecService.decToFrac.and.returnValue('15.61');
      const result = service.getFormattedPrice({
        leg: [
          {
            odds: {
              frac: '4/9'
            }
          }, {
            odds: {
              frac: '18/5'
            }
          }, {
            odds: {
              frac: '6/4'
            }
          }
        ]
      } as any);

      expect(fracToDecService.decToFrac).toHaveBeenCalledWith(16.61111111111111, true);
      expect(result).toEqual('15.61');
    });

    it('getFormattedPrice frac', () => {
      userService.oddsFormat = 'dec';
      const result = service.getFormattedPrice({
        leg: [
          {
            odds: {
              frac: '4/9'
            }
          }, {
            odds: {
              frac: '18/5'
            }
          }, {
            odds: {
              frac: '6/4'
            }
          }
        ]
      } as any);

      expect(fracToDecService.decToFrac).not.toHaveBeenCalled();
      expect(result).toEqual('16.61');
    });
  });

  describe('#hasStakeMulti', () => {
    it('should call hasStakeMulti method when token value is more than 0', () => {
      expect(service.hasStakeMulti({
        stakePerLine: '2',
        numLines: '3',
        tokenValue: '3'
      } as any)).toEqual(true);
    });

    it('should call hasStakeMulti method when token value is more than 0 and numLines less than token', () => {
      expect(service.hasStakeMulti({
        stakePerLine: '2',
        numLines: '1',
        tokenValue: '3'
      } as any)).toEqual(false);
    });

    it('should call hasStakeMulti method when token value is 0', () => {
      expect(service.hasStakeMulti({
        stakePerLine: '2',
        numLines: '3',
        tokenValue: '0'
      } as any)).toEqual(false);
    });
  });

  describe('#getStakeTotal', () => {
    it('should call getStakeTotal method', () => {
      expect(service.getStakeTotal({
        stakePerLine: '2',
        numLines: '3'
      } as any)).toEqual(6);
    });
  });

  describe('#getEWTerms', () => {
    it('should call getEWTerms method', () => {
      const legPart = {
        eachWayNum: '1',
        eachWayDen: '2'
      } as any;
      service.getEWTerms(legPart);

      expect(localeService.getString).toHaveBeenCalledWith('bs.oddsAPlaces', {
        num: '1',
        den: '2',
        arr: '1-2-3-4'
      });
      expect(templateService.genEachWayPlaces).toHaveBeenCalledWith(legPart, true);
    });
  });

  describe('#getLinesPerStake', () => {
    it('should call getLinesPerStake method', () => {
      const receipt = {
        numLines: '1',
        stakePerLine: '2'
      } as any;
      service.getLinesPerStake(receipt);

      expect(localeService.getString).toHaveBeenCalledWith('bs.linesPerStake', {
        lines: '1',
        currency: '$',
        stake: '2.00'
      });
    });
  });

  describe('#updateVirtualEventNames', () => {
    it('updateVirtualEventNames', () => {
      const receipts = [
        {
          leg: [
            {
              part: [
                {
                  eventDesc: 'test',
                  event: {
                    sportId: '10'
                  }
                }
              ]
            },
            {
              part: [
                {
                  eventDesc: 'test',
                  event: {
                    sportId: '39'
                  }
                }
              ]
            }
          ]
        }
      ];
      service['updateVirtualEventNames'](receipts);
      expect(receipts[0].leg[0].part[0].eventDesc).toEqual('test');
      expect(receipts[0].leg[1].part[0].eventDesc).toEqual('12:35 clearName');
    });
  });

  describe('divideOnSinglesAndMultiples', () => {

    beforeEach(() => {
      service['getCleanBet'] = jasmine.createSpy('getCleanBet').and.callFake((data) => data);
      service['setOutcomeNames'] = jasmine.createSpy('setOutcomeNames').and.callFake((data) => data);
      service['sendEgBetslipTransactions'] = jasmine.createSpy('sendEgBetslipTransactions');
      spyOn(service, 'isBetCanceled').and.callThrough();
    });

    it('should divide single and multiples and active and non active', () => {
      const data = [
        { betId: 1, betType: 'SGL', status: 'A' },
        { betId: 2, betType: 'SGL', status: 'X' },
        { betId: 11, betType: 'DBL', status: 'A' },
        { betId: 12, betType: 'DBL', status: 'X' },
        { betId: 13, betType: 'DBL', status: 'A', asyncAcceptStatus: 'T' }
      ] as any;

      const result = service['divideOnSinglesAndMultiples'](data);

      expect(service['setOutcomeNames']).toHaveBeenCalledWith(data);
      expect(service.isBetCanceled).toHaveBeenCalled();
      expect(service['receipts']).toEqual([data[0], data[2]]);
      expect(result).toEqual([{
        singles: [data[0], data[1]],
        multiples: [data[2], data[3], data[4]]
      }, {
        singles: [data[0]],
        multiples: [data[2]]
      }]);
    });
  });

  describe('getTotalReturns', () => {
    it('should return potentialPayout as is', () => {
      const betReceipts = [{ potentialPayout: '100' }];
      expect(service['getTotalReturns'](betReceipts)).toEqual('100.00');
    });

    it('should return 0', () => {
      const betReceipts = [{ potentialPayout: '100' }];
      expect(service['getTotalReturns'](betReceipts)).toEqual('100.00');
    });

    it('should return potentialPayout - tokenValue', () => {
      const betReceipts = [{ potentialPayout: '100', tokenValue: '50' }];
      expect(service['getTotalReturns'](betReceipts)).toEqual('100.00');
    });

    it('should return N/A if no potentialPayout', () => {
      const betReceipts = [{ tokenValue: '50' }];
      expect(service['getTotalReturns'](betReceipts)).toEqual('N/A');
    });

    it('should return N/A if potentialPayout is N/A', () => {
      const betReceipts = [{ tokenValue: '50', potentialPayout: 'N/A' }];
      expect(service['getTotalReturns'](betReceipts)).toEqual('N/A');
    });
  });

  it('@clearMessage: should clear message', () => {
    service.message.msg = 'Error msg';

    service.clearMessage();
    expect(service.message).toEqual({ type: undefined, msg: undefined });
  });

  describe('Test isBogFromPriceType()', () => {
    beforeEach(() => {
      service['filtersService']['isGreyhoundsEvent'] = jasmine.createSpy().and.returnValue(false);
    });

    it('should call isBogFromPriceType() and check if priceType: "G" ', () => {
      const receipts: any =  [
        {
          isFootball: true,
          leg: [{ part: [{ event: {}, priceType: 'G', eventCategoryId: '1' }] }]
        },
        {
          isFootball: true,
          leg: [{ part: [{ event: {}, priceType: 'SP,LP', eventCategoryId: '1' }] }]
        }];

      service['markBogReceipts'](receipts);

      expect(receipts[0].leg[0].part[0].isBog).toBe(true);
      expect(receipts[1].leg[0].part[0].isBog).toBe(false);
    });

    it('should call isBogFromPriceType() and check if priceType: "GP"', () => {
      const receipts: any =  [
        {
          isFootball: true,
          leg: [{ part: [{ event: {}, priceType: 'SL,SP', eventCategoryId: '1' }] }]
        },
        {
          leg: [
            { part: [{ event: {}, isFootball: true, priceType: 'GP,F', eventCategoryId: '1' }] },
            { part: [{ isFootball: false }] }
          ]
        }];

      service['markBogReceipts'](receipts);

      expect(receipts[0].leg[0].part[0].isBog).toBeFalsy(false);
      expect(receipts[1].leg[0].part[0].isBog).toBe(true);
      expect(receipts[1].leg[1].part[0].isBog).toBeFalsy(false);
    });

    it('should call isBogFromPriceType() and check priceType', () => {
      const receipts: any =  [
        {
          isFootball: true,
          leg: [{ part: [{ event: {}, priceType: 'SL,LP', eventCategoryId: '1' }] }]
        },
        {
          leg: [
            { part: [{ event: {}, isFootball: true, priceType: 'LP,S', eventCategoryId: '1' }] },
            { part: [{ isFootball: false }] }
          ]
        }];

      service['markBogReceipts'](receipts);

      expect(receipts[0].leg[0].part[0].isBog).toBeFalsy(false);
      expect(receipts[1].leg[0].part[0].isBog).toBeFalsy(false);
      expect(receipts[1].leg[1].part[0].isBog).toBeFalsy(false);
    });

    it('should call isBogFromPriceType(), check priceType and eventCategoryId', () => {
      const receipts: any = [
        {
          isFootball: true,
          leg: [{ part: [{ event: {}, priceType: 'G,GP', eventCategoryId: '19' }] }]
        },
        {
          leg: [
            { part: [{ event: {}, isFootball: true, priceType: 'G,GP', eventCategoryId: '19' }] },
            { part: [{ isFootball: false }] }
          ]
        }];
      service['filtersService']['isGreyhoundsEvent'] = jasmine.createSpy().and.returnValue(true);

      service['markBogReceipts'](receipts);

      expect(receipts[0].leg[0].part[0].isBog).toBeFalsy(false);
      expect(receipts[1].leg[0].part[0].isBog).toBeFalsy(false);
      expect(receipts[1].leg[1].part[0].isBog).toBeFalsy(false);
    });

    it ('should return excluded DrillDownTagNames for unnamed favourites', () => {
      const result1 = service.getExcludedDrillDownTagNames('radom name');

      expect(result1).toEqual('');

      const result2 = service.getExcludedDrillDownTagNames('unnamed favourite');
      expect(result2).toEqual('MKTFLAG_EPR, EVFLAG_EPR');

      const result3 = service.getExcludedDrillDownTagNames('Unnamed 2nd FaVouRiTe');
      expect(result3).toEqual('MKTFLAG_EPR, EVFLAG_EPR');
    });
  });
});
