import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from '@sharedModule/shared.module';
import { LazySportModule } from '@sbModule/sport/sport.module';
import { OlympicsRunService } from '@olympicsModule/services/olympics-run.service';
import { OlympicsPageComponent } from '@olympicsModule/components/olympicsPage/olympics-page.component';
import { BannersModule } from '@banners/banners.module';
import { OlympicsRoutingModule } from './olympics-route.module';

@NgModule({
  imports: [
    SharedModule,
    LazySportModule,
    OlympicsRoutingModule,
    BannersModule
  ],
  declarations: [
    OlympicsPageComponent
  ],
  exports: [
    OlympicsPageComponent
  ],
  entryComponents: [
    OlympicsPageComponent
  ],
  providers: [
    OlympicsRunService
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class OlympicsModule {
  constructor(private olympicsRunService: OlympicsRunService) {
    this.olympicsRunService.run();
  }
}
