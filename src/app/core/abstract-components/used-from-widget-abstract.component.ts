import { Input } from '@angular/core';


/**
 * The property should be set if the component is used from betslip widget on main page
 * Use it if you need to show/hide some content on the widget
 */
export abstract class UsedFromWidgetAbstractComponent {
  @Input() isUsedFromWidget = false;
}
