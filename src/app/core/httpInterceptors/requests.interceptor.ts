import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { Injectable, Injector } from '@angular/core';
import { NewRelicService } from '@core/services/newRelic/new-relic.service';
import { Observable, throwError } from 'rxjs';

@Injectable()
export class RequestsInterceptor implements HttpInterceptor {

  retries = 0;

  constructor(private injector: Injector) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const errorLogger = this.injector.get(NewRelicService);

    return next
      .handle(request)
      .pipe(
        retry(this.retries),
        catchError((error: HttpErrorResponse) => {
          errorLogger.noticeError(error);
          return throwError(error);
        })
      );
  }
}
