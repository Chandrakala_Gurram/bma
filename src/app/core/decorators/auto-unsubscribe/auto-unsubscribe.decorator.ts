/**
 * AutoUnsubscribe
 * @param blackListedSubscriptions
 * @constructor
 */
export function AutoUnsubscribe(blackListedSubscriptions = []) {
  return function (constructor) {
    const ngOnDestroy = constructor.prototype.ngOnDestroy;

    constructor.prototype.ngOnDestroy = function () {
      for (const prop of Object.keys(this)) {
        const property = this[prop];

        if (!blackListedSubscriptions.includes(prop)) {
          if (property && (typeof property.unsubscribe === 'function')) {
            property.unsubscribe();
          }
        }
      }

      ngOnDestroy && typeof ngOnDestroy === 'function' && ngOnDestroy.apply(this, arguments);
    };
  };
}
