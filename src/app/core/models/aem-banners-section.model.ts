export interface IParams {
  page?: string;
  brand?: string;
  channel?: string;
  locale?: string;
  userType?: string;
  imsLevel?: any;
  device?:  string;
  maxOffers?: number;
  atJsLoadingTimeout?: number;
}

export interface IBannersEndpointParams {
  page: string;
  brand: string;
  channel: string;
  locale: string;
  userType: string;
  imsLevel: any;
}

export interface IAemConfig {
  server: string;
  at_property: string;
  betslip_at_property: string;
  offer_modules_at_property?: string;
}

export interface IAEMCarousel {
  settings: any;
  _options: {
    brand?: string;
    locale?: string;
    server?: string;
    channel?: string;
    userType?: string;
    maxOffers?: number;
  };
}

export interface IOfferFromServer {
  id?: string;
  imgUrl: string;
  offerTitle: string;
  webUrl?: string;
  appUrl?: string;
  roxanneWebUrl?: string;
  roxanneAppUrl?: string;
  webTarget?: string;
  appTarget?: string;
  webTandC: string;
  webTandCLink?: string;
  mobTandCLink?: string;
  personalised?: boolean;
}

export interface IOfferGroupsFromServer {
  target?: IOfferFromServer[];
  library?: IOfferFromServer[];
  pinned?: {
    first?: any;
    last?: any;
  };
  rg?: IOfferFromServer;
}

export interface IOfferReport {
  providers?: any;
  offers: IOffer[];
}

export interface ICMSBannerConfig {
  enabled: boolean;
  timePerSlide: number;
  maxOffers: number;
}

export interface IOffer {
  name?: any;
  active?: boolean;
  id?: string;
  brand?: string;
  imgUrl?: string;
  altText?: string;
  title?: string;
  link?: string;
  target?: string;
  tcText?: string;
  tcLink?: string;
  position?: number;
  lazy?: boolean;
  imgClass?: string;
  personalised?: boolean;
  imageLoaded?: boolean;
}

export interface IAEMTargetBetslipParameters {
  page?: string;
  categoryId?: string;
  typeId?: string;
  eventId?: string;
  marketId?: string;
  selectionId?: string;
}

export const AT_JS_LOADING_TIMEOUT: number = 2000;
