import { IAggregation } from '@core/models/aggregation.model';
import { ISportEvent } from '@core/models/sport-event.model';

export interface ISportEventEntity {
  event: ISportEvent;
  aggregation?: IAggregation;
  coupon?: any;
  title?: any;
  id?: any;
}
