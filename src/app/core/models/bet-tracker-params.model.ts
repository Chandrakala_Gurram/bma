export interface IBetTrackerParams {
  mode: string;
  username: string;
  cardNumber: string;
}
