import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { UserService } from '@core/services/user/user.service';

@Injectable()
export class LoggedOutGuard implements CanActivate {
  constructor(
    private user: UserService,
    private router: Router
  ) { }

  canActivate(): boolean {
    if ((!this.user.status || this.user.isSignUpPending) && !this.user.loginPending) {
      return true;
    }

    this.router.navigate(['/']);
    return false;
  }
}
