import { CanDeactivateGuard } from './can-deactivate-guard.service';

describe('CanDeactivateGuard', () => {
  let service;
  const componentMock = {
    onChangeRoute: jasmine.createSpy('component.onChangeRoute')
  } as any;

  beforeEach(() => {
    service = new CanDeactivateGuard();
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  it('#CanDeactivate returns false', () => {
    componentMock.canChangeRoute = jasmine.createSpy('component.onChangeRoute').and.returnValue(false);
    const result = service.canDeactivate(componentMock);

    expect(componentMock.canChangeRoute).toHaveBeenCalled();
    expect(result).toBeFalsy();
  });

  it('#CanDeactivate returns true', () => {
    componentMock.canChangeRoute = jasmine.createSpy('component.onChangeRoute').and.returnValue(true);
    const result = service.canDeactivate(componentMock);

    expect(componentMock.canChangeRoute).toHaveBeenCalled();
    expect(componentMock.onChangeRoute).toHaveBeenCalled();
    expect(result).toBeTruthy();
  });
});
