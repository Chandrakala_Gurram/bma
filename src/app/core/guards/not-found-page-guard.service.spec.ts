import { NotFoundPageGuard } from '@core/guards/not-found-page-guard.service';

describe('ForbidManualNavigationGuard', () => {
  let service: NotFoundPageGuard;
  let navigationService;
  let route;
  let state;

  beforeEach(() => {
    navigationService = jasmine.createSpyObj('navigationService', ['handleHomeRedirect']);
    service = new NotFoundPageGuard(navigationService);
    route = {url: [{
        path: 'test'
      }, {
        path: 'url'
      }]
    };
    state = {
      url: '/test/url'
    };
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  describe('@canActivate', () => {
    it('should return false and trigger navigation service', () => {
      const result = service.canActivate(route, state);

      expect(result).toEqual(false);
      expect(navigationService.handleHomeRedirect).toHaveBeenCalledWith('general', '/test/url');
    });
  });
});
