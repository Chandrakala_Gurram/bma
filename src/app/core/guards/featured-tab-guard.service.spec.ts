import { FeaturedTabGuard } from './featured-tab-guard.service';

describe('FeaturedTabGuard', () => {
  let service;
  let router;
  let userService;
  let moduleRibbonService;
  const privateMarketsUrl = '/page';

  beforeEach(() => {
    router = {
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    };
    userService = {
      status: true
    };
    moduleRibbonService = {
      privateMarketsUrl: privateMarketsUrl,
      isPrivateMarketsTab: jasmine.createSpy('isPrivateMarketsTab').and.returnValue(true)
    };

    service = new FeaturedTabGuard(userService, router, moduleRibbonService);
  });

  describe('canActivate', () => {
    it('should return true if user is not logged in', () => {
      userService.status = false;

      expect(service.canActivate()).toEqual(true);
    });

    it('should return true if user is logged in and there are no private markets', () => {
      userService.status = true;
      moduleRibbonService.isPrivateMarketsTab.and.returnValue(false);

      expect(service.canActivate()).toEqual(true);
    });

    it('should return false and redirect to private markets page ' +
      'if user is logged in and there are private markets', () => {
      userService.status = true;
      moduleRibbonService.isPrivateMarketsTab.and.returnValue(true);

      const result = service.canActivate();

      expect(router.navigateByUrl).toHaveBeenCalledWith(privateMarketsUrl);
      expect(result).toEqual(false);
    });
  });
});
