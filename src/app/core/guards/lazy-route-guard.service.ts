import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LazyRouteGuard implements CanActivate {
  /**
   * Guard for Lazy Loading Modules which defined in routing.module
   * Just keeps redirect to home page
   */
  constructor(
    private router: Router
  ) {}

  canActivate(): boolean {
    this.router.navigate(['/']);
    return false;
  }
}
