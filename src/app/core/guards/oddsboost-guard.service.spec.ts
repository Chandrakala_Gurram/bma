
import {of as observableOf } from 'rxjs';
import { OddsBoostGuard } from './oddsboost-guard.service';

describe('OddsBoostGuard', () => {
  let service;
  let router;
  let cmsService;
  let config;
  beforeEach(() => {
    config = { enabled: true };
    router = {
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    };
    cmsService = {
      getOddsBoost: jasmine.createSpy('getOddsBoost').and.returnValue(observableOf(config))
    };

    service = new OddsBoostGuard(router, cmsService);
  });

  it('constrictor', () => {
    expect(service).toBeDefined(true);
  });

  describe('canActivate', () => {
    it('should allow activate route if oddsBoost enabled', () => {
      service.canActivate().subscribe(canActivate => {
        expect(canActivate).toEqual(true);
      });
    });

    it('should not allow activate route if oddsBoost disabled', () => {
      config.enabled = false;
      service.canActivate().subscribe(canActivate => {
        expect(canActivate).toEqual(false);
      });
      expect(router.navigateByUrl).toHaveBeenCalledWith('/');
    });
  });
});
