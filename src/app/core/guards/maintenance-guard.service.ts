import { EMPTY, Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { CanDeactivate, Resolve, Router } from '@angular/router';

import { MaintenanceComponent } from '@shared/components/maintenance/maintenance.component';
import { MaintenanceService } from '@core/services/maintenance/maintenance.service';
import { IMaintenancePage } from '@core/services/cms/models';

@Injectable()
export class MaintenanceGuard implements Resolve<IMaintenancePage>, CanDeactivate<MaintenanceComponent> {
  constructor(
    private maintenanceService: MaintenanceService,
    private router: Router
  ) { }

  /**
   * Check if maintenance page is enabled in order to show it,
   *  else break navigation and go to home page.
   *
   * @returns {Observable}
   */
  resolve(): Observable<IMaintenancePage> {
    return this.maintenanceService.getMaintenanceIfActive().pipe(switchMap((activePage: IMaintenancePage) => {
      if (!activePage) {
        this.router.navigate(['/']);
        return EMPTY;
      }

      return of(activePage);
    }));
  }

  /**
   * Prevent internal navigation if maintenance page is still enabled
   *
   * @returns {Observable}
   */
  canDeactivate(): Observable<boolean> {
    return this.maintenanceService.getActiveMaintenancePage().pipe(map((activePage: IMaintenancePage) => {
      return !activePage;
    }));
  }
}
