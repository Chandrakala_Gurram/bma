import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { Observable } from 'rxjs';
import { IRetailConfig } from '@app/core/services/cms/models/system-config';

@Injectable()
export class RetailFeatureGuard implements CanActivate {
  constructor(
    private router: Router,
    private cmsService: CmsService
  ) { }

  canActivate(route): Observable<boolean> {
    return this.cmsService.getSystemConfig().pipe(map(config => {
      const retailFeatureKey: keyof IRetailConfig = route.routeConfig.data.feature;
      // TODO: rename to retail after changes in cms.
      if (retailFeatureKey && config.Connect && config.Connect[retailFeatureKey]) {
        return true;
      }

      this.router.navigate(['/']);
      return false;
    }));
  }
}
