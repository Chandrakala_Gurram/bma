import { LazyRouteGuard } from '@core/guards/lazy-route-guard.service';

describe('LazyRouteGuard', () => {
  let service: LazyRouteGuard;
  let router;

  beforeEach(() => {
    router = {
      navigate: jasmine.createSpy()
    };

    service = new LazyRouteGuard(
      router
    );
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  it('canActivate', () => {
    expect(service.canActivate()).toBeFalsy();
    expect(router.navigate).toHaveBeenCalledWith(['/']);
  });
});
