import { PrivateMarketsGuard } from './private-markets-guard.service';

describe('PrivateMarketsGuard', () => {
  let service;
  let router;
  let moduleRibbonService;

  beforeEach(() => {
    router = {
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    };
    moduleRibbonService = {
      isPrivateMarketsTab: jasmine.createSpy('isPrivateMarketsTab').and.returnValue(true)
    };

    service = new PrivateMarketsGuard(router, moduleRibbonService);
  });

  describe('canActivate', () => {
    it('should return true if there are private markets', () => {
      moduleRibbonService.isPrivateMarketsTab.and.returnValue(true);

      expect(service.canActivate()).toEqual(true);
    });

    it('should return false and redirect to home page if' +
      'there are no private markets', () => {
      moduleRibbonService.isPrivateMarketsTab.and.returnValue(false);
      const result = service.canActivate();

      expect(router.navigateByUrl).toHaveBeenCalledWith('/');
      expect(result).toEqual(false);
    });
  });
});
