import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { NavigationService } from '@core/services/navigation/navigation.service';

@Injectable({
  providedIn: 'root'
})
export class NotFoundPageGuard implements CanActivate {
  constructor(
    private navigationService: NavigationService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    this.navigationService.handleHomeRedirect('general', state.url);
    return false;
  }
}
