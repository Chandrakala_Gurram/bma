import { of as observableOf } from 'rxjs';
import { CashOutTabGuard } from '@core/guards/cashout-tab-guard.service';

describe('CashOutTabGuard', () => {
  let service;
  let router;
  let cmsService;
  let config;

  beforeEach(() => {
    config = { CashOut: { isCashOutTabEnabled: true } };
    router = {
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    };
    cmsService = {
      getSystemConfig: jasmine.createSpy('getSystemConfig').and.returnValue(observableOf(config))
    };

    service = new CashOutTabGuard(router, cmsService);
  });

  it('constrictor', () => {
    expect(service).toBeDefined(true);
  });

  describe('canActivate', () => {
    it('should allow activate route if CashOutTab enabled', () => {
      service.canActivate().subscribe(canActivate => {
        expect(canActivate).toEqual(true);
      });
    });

    it('should not allow activate route if CashOutTab disabled', () => {
      config.CashOut.isCashOutTabEnabled = false;
      service.canActivate().subscribe(canActivate => {
        expect(canActivate).toEqual(false);
      });
      expect(router.navigateByUrl).toHaveBeenCalledWith('/');
    });
  });
});
