
import {of as observableOf,  Observable } from 'rxjs';
import { RetailFeatureGuard } from './retail-feature-guard.service';

describe('RetailFeatureGuard', () => {
  let service: RetailFeatureGuard;
  let router;
  let cmsService;

  beforeEach(() => {
    router = jasmine.createSpyObj('Router', ['navigate']);
    cmsService = jasmine.createSpyObj('CmsService', ['getSystemConfig']);

    service = new RetailFeatureGuard(
      router,
      cmsService
    );
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  describe('canActivate', () => {
    it('returns true', done => {
      const route = {
        routeConfig: {
          data: { segment: 'bet-tracker', feature: 'shopBetTracker' }
        }
      };

      cmsService.getSystemConfig.and.returnValue(
        observableOf({ // TODO: rename to retail after changes in cms.
          Connect: {
            shopBetTracker: true
          }
        })
      );

      const result = service.canActivate(route);
      result.subscribe(data => {
        expect(data).toBeTruthy();
        expect(router.navigate).not.toHaveBeenCalledWith(['/']);
        done();
      });

      expect(cmsService.getSystemConfig).toHaveBeenCalled();
      expect(result).toEqual(jasmine.any(Observable));
    });

    it('returns false', done => {
      const route = {
        routeConfig: {
          data: { segment: 'bet-tracker', feature: 'shopBetTracker' }
        }
      };

      cmsService.getSystemConfig.and.returnValue(
        observableOf({ // TODO: rename to retail after changes in cms.
          Connect: {
            shopBetTracker: false
          }
        })
      );

      const result = service.canActivate(route);
      result.subscribe(data => {
        expect(data).toBeFalsy();
        expect(router.navigate).toHaveBeenCalledWith(['/']);
        done();
      });

      expect(cmsService.getSystemConfig).toHaveBeenCalled();
      expect(result).toEqual(jasmine.any(Observable));
    });
  });
});
