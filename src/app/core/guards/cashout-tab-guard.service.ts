import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { Observable } from 'rxjs';

@Injectable()
export class CashOutTabGuard implements CanActivate {
  constructor(
    private router: Router,
    private cmsService: CmsService
  ) { }

  canActivate(): Observable<boolean> {
    return this.cmsService.getSystemConfig().pipe(map((data) => {
      if (data.CashOut && !data.CashOut.isCashOutTabEnabled) {
        this.router.navigateByUrl('/');
        return false;
      }
      return true;
    }));
  }
}
