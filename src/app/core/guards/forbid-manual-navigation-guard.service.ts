import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

@Injectable({
  providedIn: 'root'
})
export class ForbidManualNavigationGuard implements CanActivate {
  constructor(
    private windowRefService: WindowRefService,
    private router: Router
  ) {}

  canActivate(): boolean {
    if (this.windowRefService.document.referrer === '') {
      this.router.navigate(['/']);
      return false;
    }

    return true;
  }
}
