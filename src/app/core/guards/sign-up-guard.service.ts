import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { NavigationService as VanillaNavigationService } from '@labelhost/core';

@Injectable({
  providedIn: 'root'
})
export class SignUpRouteGuard implements CanActivate {
  /**
   * Guard for /signup to open Vanilla registration
   */
  constructor(
    private vanillaNavigationService: VanillaNavigationService
  ) { }

  canActivate(): boolean {
    this.vanillaNavigationService.goToRegistration();

    return false;
  }
}
