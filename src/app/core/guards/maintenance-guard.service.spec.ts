import { of as observableOf } from 'rxjs';

import { MaintenanceGuard } from '@core/guards/maintenance-guard.service';

describe('MaintenanceGuard', () => {
  let service: MaintenanceGuard;

  let maintenanceService;
  let router;
  let successHandler;

  beforeEach(() => {
    maintenanceService = {
      getMaintenanceIfActive: jasmine.createSpy('getMaintenanceIfActive').and.returnValue(observableOf({})),
      getActiveMaintenancePage: jasmine.createSpy('getActiveMaintenancePage').and.returnValue(observableOf(null))
    };
    router = jasmine.createSpyObj(['navigate']);

    successHandler = jasmine.createSpy('successHandler');

    service = new MaintenanceGuard(maintenanceService, router);
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  describe('resolve', () => {

    it('should ask service for maintenance page', () => {
      service.resolve().subscribe();

      expect(maintenanceService.getMaintenanceIfActive).toHaveBeenCalled();
    });

    it('should resolve with page data', () => {
      service.resolve().subscribe(successHandler);

      expect(successHandler).toHaveBeenCalledWith({});
      expect(router.navigate).not.toHaveBeenCalled();
    });

    it('should prevent navigation', () => {
      maintenanceService.getMaintenanceIfActive.and.returnValue(observableOf(null));

      service.resolve().subscribe(successHandler);

      expect(successHandler).not.toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith(['/']);
    });
  });

  describe('canDeactivate', () => {

    it('should ask service for maintenance page', () => {
      service.canDeactivate().subscribe();

      expect(maintenanceService.getActiveMaintenancePage).toHaveBeenCalled();
    });

    it('should allow navigation', () => {
      service.canDeactivate().subscribe(successHandler);

      expect(successHandler).toHaveBeenCalledWith(true);
    });

    it('should not allow navigation if maintenence is active', () => {
      maintenanceService.getActiveMaintenancePage.and.returnValue(observableOf({}));

      service.canDeactivate().subscribe(successHandler);

      expect(successHandler).toHaveBeenCalledWith(false);
    });
  });
});
