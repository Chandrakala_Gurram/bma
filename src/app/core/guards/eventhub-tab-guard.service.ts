import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import * as _ from 'underscore';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { IModuleRibbonTab } from '@core/services/cms/models';
import { IInitialData } from '@featured/components/featured-tab/initial-data.model';
import { CmsService } from '@coreModule/services/cms/cms.service';

@Injectable()
export class EventhubTabGuard implements CanActivate {
  constructor(
    private router: Router,
    private cms: CmsService
  ) { }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const hubIndex = route.params.hubIndex;

    if (hubIndex) {
      // as status of tab could be changed because of sceduling
      // we need to get ribbon data to check availability of Tab
      return this.cms.getRibbonModule()
        .pipe(map((data: IInitialData) => {
          if (!this.isHubActive(hubIndex, data.getRibbonModule)) {
            // hub is not active, redirect to first tab
            const firstModule = data.getRibbonModule[0];
            const firstModuleUrl = firstModule.url.substring(1);

            this.router.navigate([firstModuleUrl]);
            return false;
          }

          // Hub is active
          return true;
        }));
    }

    return of(false);
  }

  isHubActive(hubIndex, moduleList) {
    return _.find(moduleList, (ribbonItem: Partial<IModuleRibbonTab>) => {
      return ribbonItem.hubIndex === parseInt(hubIndex, 10);
    });
  }
}
