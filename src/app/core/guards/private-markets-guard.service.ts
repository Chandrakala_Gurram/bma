import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { ModuleRibbonService } from '@core/services/moduleRibbon/module-ribbon.service';

@Injectable()
export class PrivateMarketsGuard implements CanActivate {
  constructor(
    private router: Router,
    private ribbonService: ModuleRibbonService
  ) { }

  canActivate(): boolean {
    if (this.ribbonService.isPrivateMarketsTab()) {
      return true;
    }

    this.router.navigateByUrl('/');
    return false;
  }
}
