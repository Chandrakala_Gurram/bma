import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import environment from '@environment/oxygenEnvConfig';
import { GermanSupportService } from '@app/core/services/germanSupport/german-support.service';
import { LocaleService } from '@core/services/locale/locale.service';

@Injectable()
export class GermanSupportSportTabGuard implements CanActivate {

  private jackpotMessageBody: string = this.localeService.getString('bma.countryRestriction.jackpotMessageBody');

  constructor(
    private localeService: LocaleService,
    private germanSupportService: GermanSupportService,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (environment.brand === 'bma') { return true; } // skip guard for coral brand

    if (this.germanSupportService.isGermanUser() &&
      state.url.includes('football') &&
      state.url.includes('jackpot')) {
        this.router.navigate(['/']);
        this.germanSupportService.showDialog(this.jackpotMessageBody);
        return false;
    } else {
      return true;
    }
  }
}
