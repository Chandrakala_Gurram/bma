import { SignUpRouteGuard } from '@core/guards/sign-up-guard.service';

describe('SignUpRouteGuard', () => {
  let service: SignUpRouteGuard;
  let navigationService;

  beforeEach(() => {
    navigationService = {
      goToRegistration: jasmine.createSpy('goToRegistration')
    };

    service = new SignUpRouteGuard(
      navigationService
    );
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  it('canActivate', () => {
    expect(service.canActivate()).toBeFalsy();
    expect(navigationService.goToRegistration).toHaveBeenCalledWith();
  });
});
