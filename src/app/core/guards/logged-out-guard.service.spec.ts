import { LoggedOutGuard } from './logged-out-guard.service';

describe('LoggedOutGuard', () => {
  let service: LoggedOutGuard;
  let userService;
  let router;

  beforeEach(() => {
    userService = {};
    router = {
      navigate: jasmine.createSpy()
    };

    service = new LoggedOutGuard(
      userService,
      router
    );
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  it('canActivate', () => {
    userService.status = false;
    userService.loginPending = false;
    expect(service.canActivate()).toBeTruthy();

    userService.status = true;
    userService.loginPending = true;
    expect(service.canActivate()).toBeFalsy();
    expect(router.navigate).toHaveBeenCalledWith(['/']);
  });
});
