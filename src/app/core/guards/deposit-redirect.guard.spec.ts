import { DepositRedirectGuard } from './deposit-redirect.guard';

describe('DepositRedirectGuard', () => {
  let navigationService;
  let guard: DepositRedirectGuard;

  beforeEach(() => {
    navigationService = {
      goToCashierDeposit: jasmine.createSpy()
    };
    guard = new DepositRedirectGuard(navigationService);
  });

  it('canActivate', () => {
    const result = guard.canActivate();
    expect(navigationService.goToCashierDeposit).toHaveBeenCalledWith({});
    expect(result).toBeFalsy();
  });

});
