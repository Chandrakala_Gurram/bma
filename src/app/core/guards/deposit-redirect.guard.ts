import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { NavigationService as VanillaNavigationService } from '@labelhost/core/features';

@Injectable({
  providedIn: 'root'
})
export class DepositRedirectGuard implements CanActivate {

  constructor(
    private vanillaNavigationService: VanillaNavigationService
  ) { }

  canActivate(): boolean {
    this.vanillaNavigationService.goToCashierDeposit({});
    return false;
  }
}
