
import { map } from 'rxjs/operators';
import { IOddsBoostConfig } from '@core/services/cms/models/odds-boost-config.model';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { Observable } from 'rxjs';

@Injectable()
export class OddsBoostGuard implements CanActivate {
  constructor(
    private router: Router,
    private cmsService: CmsService
  ) { }

  canActivate(): Observable<boolean> {
    return this.cmsService.getOddsBoost().pipe(map((config: IOddsBoostConfig) => {
      if (config.enabled) {
        return true;
      }

      this.router.navigateByUrl('/');
      return false;
    }));
  }
}
