import { of as observableOf } from 'rxjs';
import { EventhubTabGuard } from '@core/guards/eventhub-tab-guard.service';

describe('EventhubTabGuard', () => {
  let service;
  let router;
  let route;
  let cmsService;
  let ribbonDataMock;

  beforeEach(() => {
    router = {
      navigateByUrl: jasmine.createSpy('navigateByUrl'),
      navigate: jasmine.createSpy('navigate')
    };
    route = {
      params: {}
    };

    ribbonDataMock = [
      {
        directiveName: 'Featured',
        id: 'tab-featured',
        showTabOn: 'both',
        title: 'Featured',
        url: '/home/featured',
        visible: true
      },
      {
        directiveName: 'EventHub',
        id: 'tab-eventhub-4',
        showTabOn: 'both',
        displayFrom: '2019-02-18T13:12:01Z',
        displayTo: '2019-02-18T15:12:01Z',
        title: 'hub 4',
        hubIndex: 4,
        url: '/home/eventhub/4',
        visible: true
      }
    ];

    cmsService = {
      getRibbonModule: jasmine.createSpy().and.returnValue(observableOf({
        getRibbonModule: ribbonDataMock
      }))
    };

    service = new EventhubTabGuard(
      router,
      cmsService
    );
  });

  describe('canActivate', () => {
    it('should return false for eventhub route, when no tab available', () => {
      route = {
        params: {
          hubIndex: '1'  // tab with 1 is absent, we load route for hub index 1
        }
      };

      service.canActivate(route)
        .subscribe((result: boolean) => {
          expect(result).toEqual(false);
        });
    });

    it('should return false for eventhub route, when evnthub index', () => {
      route = {
        params: {}
      };

      service.canActivate(route)
        .subscribe((result: boolean) => {
          expect(result).toEqual(false);
        });
    });

    it('should return true for eventhub route, when tab is available', () => {
      route = {
        params: {
          hubIndex: '4'  // tab with 4 is present, we load route for hub index 4
        }
      };

      service.canActivate(route)
        .subscribe((result: boolean) => {
          expect(result).toEqual(true);
        });
    });
  });
});
