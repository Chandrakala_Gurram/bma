import { ForbidManualNavigationGuard } from '@core/guards/forbid-manual-navigation-guard.service';

describe('ForbidManualNavigationGuard', () => {
  let service: ForbidManualNavigationGuard;
  let router;
  let windowRefService;

  beforeEach(() => {
    router = jasmine.createSpyObj('routerSpy', ['navigate']);
    windowRefService = { document: { referrer: '' } };
    service = new ForbidManualNavigationGuard(windowRefService, router);
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  describe('@canActivate', () => {
    it('should return true when user is redirected to the page', () => {
      windowRefService.document.referrer = 'referrer';
      expect(service.canActivate()).toEqual(true);
    });

    it('should return false when user manually navigates to the page', () => {
      expect(service.canActivate()).toEqual(false);
      expect(router.navigate).toHaveBeenCalledWith(['/']);
    });
  });
});
