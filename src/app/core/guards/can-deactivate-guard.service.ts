import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';

import { IComponentCanDeactivate } from '@root/app/core/models/component-can-deactivate.model';

/**
 * Description
 * Guard deciding if a route can be deactivated.
 * If it returns true, navigation will continue
 * Otherwise, onChangeRoute()  is called and navigation will be cancelled
 */

@Injectable({
  providedIn: 'root'
})
export class CanDeactivateGuard implements CanDeactivate<IComponentCanDeactivate > {
  canDeactivate(component: IComponentCanDeactivate): boolean {
    if (component.canChangeRoute()) { return true; }

    component.onChangeRoute();
    return false;
  }
}
