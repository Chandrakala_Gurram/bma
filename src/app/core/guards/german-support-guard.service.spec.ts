import { GermanSupportGuard } from './german-support-guard.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import environment from '@environment/oxygenEnvConfig';
import { fakeAsync, tick } from '@angular/core/testing';

describe('GermanSupportGuard', () => {
  let service: GermanSupportGuard;
  let router;
  let localeService;
  let germanSupportService;
  const route = {} as ActivatedRouteSnapshot;
  const stateArr = {
    football: {
      url: 'sports/football'
    },
    lotto: {
      url: 'lotto'
    },
    jackpot: {
      url: 'sports/football/jackpot'
    },
    racing: {
      url: 'racing'
    }
  };

  beforeEach(() => {
    germanSupportService = jasmine.createSpyObj('GermanSupportService', ['isGermanUser', 'showDialog']);
    router = {
      navigate: jasmine.createSpy('navigate').and.returnValue(Promise.resolve())
    };
    localeService = {
      getString: jasmine.createSpy()
    };

    service = new GermanSupportGuard(
      localeService,
      germanSupportService,
      router
    );
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  describe('coral brand', () => {

    it('should skip guard if bma', () => {
      const state = stateArr.racing as RouterStateSnapshot;

      expect(environment.brand).toBe('bma');
      expect(service.canActivate(route, state)).toBeTruthy();
      expect(germanSupportService.isGermanUser).not.toHaveBeenCalled();
    });
  });

  describe('ladbrokes brand', () => {

    beforeAll(() => {
      environment.brand = 'not-bma';
    });
    afterAll(() => {
      environment.brand = 'bma';
    });

    it('canActivate for german user on racing route', fakeAsync(() => {
      germanSupportService.isGermanUser.and.returnValue(true);
      const state = stateArr.racing as RouterStateSnapshot;
      expect(service['isLotto']).toBeFalsy();
      expect(service.canActivate(route, state)).toBeFalsy();
      tick();
      expect(router.navigate).toHaveBeenCalledWith(['/']);
      expect(germanSupportService.showDialog).toHaveBeenCalled();
    }));

    it('canActivate for german user on lotto route', fakeAsync(() => {
      germanSupportService.isGermanUser.and.returnValue(true);
      const state = stateArr.lotto as RouterStateSnapshot;
      expect(service.canActivate(route, state)).toBeFalsy();
      tick();
      expect(router.navigate).toHaveBeenCalledWith(['/']);
      expect(germanSupportService.showDialog).toHaveBeenCalled();
    }));

    it('canActivate for user except german', () => {
      germanSupportService.isGermanUser.and.returnValue(false);
      const state = stateArr.football as RouterStateSnapshot;
      expect(service.canActivate(route, state)).toBeTruthy();
    });

    it('isRestrictedUrl should return true', () => {
      const state = stateArr.lotto as RouterStateSnapshot;
      expect(service['isRestrictedUrl'](state)).toBeTruthy();
    });

    it('isRestrictedUrl should return false', () => {
      const state = stateArr.football as RouterStateSnapshot;
      expect(service['isRestrictedUrl'](state)).toBeFalsy();
    });
  });
});
