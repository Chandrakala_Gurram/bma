import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import environment from '@environment/oxygenEnvConfig';
import { GermanSupportSportTabGuard } from './german-support-sport-tab-guard.service';

describe('GermanSupportSportTabGuard', () => {
  let service: GermanSupportSportTabGuard;
  let router;
  let localeService;
  let germanSupportService;
  const route = {} as ActivatedRouteSnapshot;
  const stateArr = {
    football: {
      url: 'sports/football'
    },
    lotto: {
      url: 'lotto'
    },
    jackpot: {
      url: 'sports/football/jackpot'
    },
    racing: {
      url: 'racing'
    }
  };

  beforeEach(() => {
    germanSupportService = jasmine.createSpyObj('GermanSupportService', ['isGermanUser', 'showDialog']);
    router = {
      navigate: jasmine.createSpy('navigate')
    };
    localeService = {
      getString: jasmine.createSpy()
    };

    service = new GermanSupportSportTabGuard(
      localeService,
      germanSupportService,
      router
    );
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  describe('coral brand', () => {

    it('should skip guard if bma', () => {
      const state = stateArr.racing as RouterStateSnapshot;

      expect(environment.brand).toBe('bma');
      expect(service.canActivate(route, state)).toBeTruthy();
      expect(germanSupportService.isGermanUser).not.toHaveBeenCalled();
    });
  });

  describe('ladbrokes brand', () => {

    beforeAll(() => {
      environment.brand = 'not-bma';
    });
    afterAll(() => {
      environment.brand = 'bma';
    });

    it('canActivate for german user on jackpot route', () => {
      germanSupportService.isGermanUser.and.returnValue(true);
      const state = stateArr.jackpot as RouterStateSnapshot;
      expect(service.canActivate(route, state)).toBeFalsy();
      expect(router.navigate).toHaveBeenCalledWith(['/']);
      expect(germanSupportService.showDialog).toHaveBeenCalled();
    });

    it('canActivate for user except german', () => {
      germanSupportService.isGermanUser.and.returnValue(false);
      const state = stateArr.football as RouterStateSnapshot;
      expect(service.canActivate(route, state)).toBeTruthy();
    });
  });
});
