import { LoggedInGuard } from './logged-in-guard.service';

describe('LoggedInGuard', () => {
  let service: LoggedInGuard;
  let userService;
  let router;

  beforeEach(() => {
    userService = {};
    router = {
      navigate: jasmine.createSpy()
    };

    service = new LoggedInGuard(
      userService,
      router
    );
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  it('canActivate', () => {
    userService.status = true;
    expect(service.canActivate()).toBeTruthy();

    userService.status = false;
    expect(service.canActivate()).toBeFalsy();
    expect(router.navigate).toHaveBeenCalledWith(['/']);
  });
});
