import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import environment from '@environment/oxygenEnvConfig';
import { GermanSupportService } from '@app/core/services/germanSupport/german-support.service';
import { LocaleService } from '@core/services/locale/locale.service';

@Injectable()
export class GermanSupportGuard implements CanActivate {

  private isLotto: boolean;
  private lottoMessageBody: string = this.localeService.getString('bma.countryRestriction.lottoMessageBody');
  private racingMessageBody: string = this.localeService.getString('bma.countryRestriction.racingMessageBody');
  private restrictedUrls: Array<string> = ['racing', 'next-races', 'lotto']; // restricted urls for german users

  constructor(
    private localeService: LocaleService,
    private germanSupportService: GermanSupportService,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (environment.brand === 'bma') { return true; } // skip guard for coral brand

    if (this.germanSupportService.isGermanUser() && this.isRestrictedUrl(state)) {
      this.isLotto = state.url.includes('lotto');
      this.router.navigate(['/']).then(() => {
        setTimeout(() => this.germanSupportService.showDialog(this.isLotto ? this.lottoMessageBody : this.racingMessageBody), 0);
      });
      return false;
    } else {
      return true;
    }
  }

  private isRestrictedUrl(state: RouterStateSnapshot): boolean {
    const result = this.restrictedUrls.filter(url => state.url.includes(url));
    return result.length > 0;
  }
}
