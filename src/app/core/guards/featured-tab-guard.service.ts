import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { UserService } from '@core/services/user/user.service';
import { ModuleRibbonService } from '@core/services/moduleRibbon/module-ribbon.service';

@Injectable()
export class FeaturedTabGuard implements CanActivate {
  constructor(
    private user: UserService,
    private router: Router,
    private ribbonService: ModuleRibbonService
  ) { }

  canActivate(): boolean {
    if (this.user.status && this.ribbonService.isPrivateMarketsTab()) {
      this.router.navigateByUrl(this.ribbonService.privateMarketsUrl);
      return false;
    }

    return true;
  }
}
