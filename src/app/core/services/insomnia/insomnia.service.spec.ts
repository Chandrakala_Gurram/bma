import { fakeAsync, discardPeriodicTasks, tick } from '@angular/core/testing';

import { InsomniaService } from './insomnia.service';

describe('InsomniaService', () => {
  let service: InsomniaService;
  let pubSubService;
  let storageService;
  let reloadService;

  beforeEach(() => {
    pubSubService = {
      publish: jasmine.createSpy('publish')
    };
    storageService = {
      setCookie: jasmine.createSpy('setCookie')
    };
    reloadService = {
      reload: jasmine.createSpy('reload')
    };

    service = new InsomniaService(
      pubSubService,
      storageService,
      reloadService
    );
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  it('init', () => {
    service['addWorkerListeners'] = jasmine.createSpy();
    service['checkWorkerSync'] = jasmine.createSpy();

    service.init();

    expect(service.worker).toEqual(jasmine.any(Worker));
    expect(service['addWorkerListeners']).toHaveBeenCalled();
    expect(service['checkWorkerSync']).toHaveBeenCalled();
  });

  it('setTimeoutAction (worker supported)', () => {
    service.workerSupport = true;
    service.worker = {
      postMessage: jasmine.createSpy()
    } as any;

    const eventData: any = {};
    const interval = 1000;
    service.setTimeoutAction(eventData, interval);

    expect(service.worker.postMessage).toHaveBeenCalledWith({
      eventData, interval, type: 'timeout'
    });
  });

  it('setTimeoutAction (worker not supported)', () => {
    service.workerSupport = false;
    service['workerEmu'] = jasmine.createSpy();

    const eventData: any = {};
    const interval = 1000;
    service.setTimeoutAction(eventData, interval);

    expect(service['workerEmu']).toHaveBeenCalledWith({
      eventData, interval, type: 'timeout'
    });
  });

  it('findTimeEvent', () => {
    expect(service['findTimeEvent']('event1')).toBeFalsy();
    expect(service['findTimeEvent']('event2')).toBeFalsy();

    service['timeTimeoutEvents']['event1'] = {};
    expect(service['findTimeEvent']('event1')).toBeTruthy();

    service['timeIntervalEvents']['event2'] = {};
    expect(service['findTimeEvent']('event2')).toBeTruthy();
  });

  it('workerEmu (clear)', () => {
    service['timeTimeoutEvents'] = { event1: true };
    service['timeIntervalEvents'] = { event2: true };

    const data: any = {
      clearTimeouts: true, clearIntervals: true
    };

    service['workerEmu'](data);

    expect(service['timeTimeoutEvents']).toEqual({});
    expect(service['timeIntervalEvents']).toEqual({});
  });

  it('workerEmu (timeout)', fakeAsync(() => {
    const data: any = {
      type: 'timeout',
      eventData: { eventName: 'event1' },
      interval: 1
    };

    service['findTimeEvent'] = jasmine.createSpy().and.returnValue(true);
    service['timeTimeoutEvents'][data.eventData.eventName] = { timeout: 123 };

    service['workerEmu'](data);
    tick(1);

    expect(service['findTimeEvent']).toHaveBeenCalledWith(data.eventData.eventName);
    expect(pubSubService.publish).toHaveBeenCalledWith('INSOMNIA', [data.eventData]);
    expect(service['timeTimeoutEvents'][data.eventData.eventName]).toEqual({
      timeout: jasmine.any(Number),
      eventData: data.eventData
    });
  }));

  it('workerEmu (interval)', fakeAsync(() => {
    const data: any = {
      type: 'interval',
      eventData: { eventName: 'event1' },
      interval: 1
    };

    service['findTimeEvent'] = jasmine.createSpy().and.returnValue(false);
    service['timeIntervalEvents'][data.eventData.eventName] = { timeout: 123 };

    service['workerEmu'](data);
    tick(1);

    expect(service['findTimeEvent']).toHaveBeenCalledWith(data.eventData.eventName);
    expect(pubSubService.publish).toHaveBeenCalledWith('INSOMNIA', [data.eventData]);
    expect(service['timeIntervalEvents'][data.eventData.eventName]).toEqual({
      interval: jasmine.any(Number),
      eventData: data.eventData
    });
    discardPeriodicTasks();
  }));

  it('reloadApp', () => {
    spyOn(sessionStorage, 'setItem');

    storageService.isSupported = true;
    service['reloadApp']();
    expect(reloadService.reload).toHaveBeenCalled();
    expect(sessionStorage.setItem).toHaveBeenCalledWith('show-alternative-screen', 'true');

    storageService.isSupported = false;
    service['reloadApp']();
    expect(storageService.setCookie).toHaveBeenCalledWith(
      'show-alternative-screen', 'true', null, 1
    );
    expect(reloadService.reload).toHaveBeenCalled();
  });

  it('addWorkerListeners', () => {
    service.worker = {} as any;
    service['addWorkerListeners']();

    expect(service.worker.onmessage).toEqual(jasmine.any(Function));
    expect(service.worker.onerror).toEqual(jasmine.any(Function));
  });

  it('addWorkerListeners (check worker availability)', () => {
    service.worker = {} as any;
    service['addWorkerListeners']();

    service.worker.onmessage({
      data: { checkWorkerAvailability: true }
    } as any);

    expect(service.workerSupport).toBeTruthy();
  });

  it('addWorkerListeners (check worker sync)', () => {
    service.worker = {} as any;
    service['reloadApp'] = jasmine.createSpy();
    service['addWorkerListeners']();

    service.worker.onmessage({
      data: { checkWorkerSync: true }
    } as any);

    expect(service['reloadApp']).toHaveBeenCalled();
  });

  it('addWorkerListeners (insomnia)', () => {
    service.worker = {} as any;
    service['reloadApp'] = jasmine.createSpy();
    service['addWorkerListeners']();

    service.worker.onmessage({
      data: {}
    } as any);

    expect(pubSubService.publish).toHaveBeenCalledWith('INSOMNIA', [{}]);
  });

  it('addWorkerListeners should instantly return', () => {
    service.worker = {} as any;
    service['isTriggeredMap'].set(1, true);
    service['addWorkerListeners']();

    service.worker.onmessage({
      data: {
        classId: 1
      }
    } as any);

    expect(pubSubService.publish).not.toHaveBeenCalled();
  });

  it('addWorkerListeners should properly set isTriggeredMap', fakeAsync(() => {
    service.worker = {} as any;
    service['addWorkerListeners']();
    service.worker.onmessage({
      data: {
        classId: 1
      }
    } as any);
    expect(service['isTriggeredMap'].get(1)).toBe(true);
    tick(10000);
    expect(service['isTriggeredMap'].get(1)).toBe(false);
  }));

  it('checkWorkerSync', () => {
    service.worker = {
      postMessage: jasmine.createSpy()
    } as any;

    service['checkWorkerSync']();

    expect(service.worker.postMessage).toHaveBeenCalledWith({
      checkWorkerAvailability: true
    });
    expect(service.worker.postMessage).toHaveBeenCalledWith({
      checkWorkerSync: true, interval: 5000, type: 'interval'
    });
  });
});
