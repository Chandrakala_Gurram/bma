/**
 * Use for lazy loading components
 * Path to module ('./src/lazy-libs/lazy.module') also should be added to the angular.json file
 * Documentation: https://confluence.egalacoral.com/pages/viewpage.action?pageId=101687110
 *
 * "options": {
 *  ...
 *  "lazyModules": [
 *    ...
 *    './src/lazy-libs/lazy.module'
 *    ...
 *  ]
 *  ...
 * }
 */

import {
  ComponentRef, Injectable, Injector, NgModuleFactory, NgModuleFactoryLoader, NgModuleRef, ViewContainerRef
} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LazyComponentFactory {

  constructor(
    private loader: NgModuleFactoryLoader,
    private injector: Injector
  ) { }

  /**
   * Create lazy component by given module path:
   *  load module the component belongs to,
   *  create instance of that module,
   *  create component [by name] and put it to given outlet
   *
   * @param lazyModulePath
   * @param lazyComponentOutlet
   * @param entryComponentName - optional (skip it if module holds single component)
   */
  createLazyComponent(lazyModulePath: string, lazyComponentOutlet: ViewContainerRef, entryComponentName?: string)
    : Promise<ComponentRef<any>> {

    if (!lazyComponentOutlet) {
      return Promise.reject('Outlet for component is not defined!');
    }

    return this.createLazyModule(lazyModulePath, lazyComponentOutlet.parentInjector)
      .then(({moduleFactory, moduleRef}) => {
        let entryComponent = (<any>moduleFactory.moduleType).entry;

        if (entryComponentName && entryComponent && entryComponent.hasOwnProperty(entryComponentName)) {
          entryComponent = entryComponent[entryComponentName];
        }

        if (!entryComponent) {
          throw new Error(`Entry component not found in module "${lazyModulePath}"`);
        }

        const componentFactory = moduleRef.componentFactoryResolver.resolveComponentFactory(entryComponent);

        lazyComponentOutlet.clear();

        return lazyComponentOutlet.createComponent(componentFactory);
      }).catch(error => {
        return Promise.reject(error);
      });
  }

  /**
   * Create instance of loaded module
   *
   * @param lazyModulePath
   * @param parentInjector
   */
  createLazyModule(lazyModulePath: string, parentInjector: Injector)
    : Promise<{moduleFactory: NgModuleFactory<any>, moduleRef: NgModuleRef<any>}> {
    return this.loadLazyModule(lazyModulePath)
      .then((moduleFactory: NgModuleFactory<any>) => {
        return Promise.resolve({
          moduleFactory,
          moduleRef: moduleFactory.create(parentInjector || this.injector)
        });
      }).catch(error => {
        return Promise.reject(error);
      });
  }

  /**
   * Load given module
   *
   * @param lazyModulePath
   */
  loadLazyModule(lazyModulePath: string): Promise<NgModuleFactory<any>> {
    if (!lazyModulePath) {
      return Promise.reject('Path for module is not defined!');
    }

    return this.loader.load(lazyModulePath).catch(error => {
      return Promise.reject(error);
    });
  }
}
