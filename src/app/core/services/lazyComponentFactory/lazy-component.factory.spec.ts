import { fakeAsync, tick } from '@angular/core/testing';

import { LazyComponentFactory } from '@core/services/lazyComponentFactory/lazy-component.factory';

describe('LazyComponentFactory -', () => {
  let service: LazyComponentFactory;

  let
    loader,
    injector,
    componentFactoryResolver,
    moduleFactory;

  beforeEach(() => {
    componentFactoryResolver = jasmine.createSpyObj('componentFactoryResolver', ['resolveComponentFactory']);

    moduleFactory = {
      moduleType: {entry: {lazyComponent: 'foo'}},
      create: jasmine.createSpy('create').and.returnValue({componentFactoryResolver})
    };

    loader = {
      load: jasmine.createSpy('load').and.returnValue(Promise.resolve(moduleFactory))
    };

    injector = {};

    service = new LazyComponentFactory(loader, injector);
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  describe('@createLazyComponent', () => {
    let newComponent, lazyComponentOutlet;

    beforeEach(() => {
      newComponent = {foo: ''};

      lazyComponentOutlet = {
        parentInjector: {foo: ''},
        clear: jasmine.createSpy('clear'),
        createComponent: jasmine.createSpy('createComponent').and.returnValue(newComponent)
      };

      spyOn(service, 'createLazyModule').and.returnValue(Promise.resolve({moduleFactory, moduleRef: {componentFactoryResolver}}));
    });

    it('should return Promise', () => {
      expect(service.createLazyComponent('', null)).toEqual(jasmine.any(Promise));
    });

    it('should prevent execute if outlet not defined', fakeAsync(() => {
      service.createLazyComponent('foo', null).catch((err) => {
        expect(err).toEqual(jasmine.any(String));
        expect(err.includes('component')).toBeTruthy();
        expect(err.includes('not defined')).toBeTruthy();
      });

      tick();

      expect(service.createLazyModule).not.toHaveBeenCalled();
    }));

    it('should delegate creating lazy module to method', () => {
      service.createLazyComponent('/foo', lazyComponentOutlet);

      expect(service.createLazyModule).toHaveBeenCalledWith('/foo', lazyComponentOutlet.parentInjector);
    });

    it('should resolve component factory and create component', fakeAsync(() => {
      service.createLazyComponent('/foo', lazyComponentOutlet);
      tick();

      expect(componentFactoryResolver.resolveComponentFactory).toHaveBeenCalledWith(moduleFactory.moduleType.entry);
      expect(lazyComponentOutlet.clear).toHaveBeenCalled();
      expect(lazyComponentOutlet.createComponent).toHaveBeenCalled();
    }));
  });

  describe('@createLazyModule', () => {
    let parentInjector;

    beforeEach(() => {
      parentInjector = {foo: ''} as any;
      spyOn(service, 'loadLazyModule').and.returnValue(Promise.resolve(moduleFactory));
    });

    it('should return Promise', () => {
      expect(service.createLazyModule('',  parentInjector)).toEqual(jasmine.any(Promise));
    });

    it('should delegate loading module to method', () => {
      service.createLazyModule('/foo',  parentInjector);

      expect(service.loadLazyModule).toHaveBeenCalledWith('/foo');
    });

    it('should create module with parent injector', fakeAsync(() => {
      service.createLazyModule('/foo',  parentInjector);
      tick();

      expect(moduleFactory.create).toHaveBeenCalledWith(parentInjector);
    }));

    it('should create module with own injector', fakeAsync(() => {
      service.createLazyModule('/foo',  null);
      tick();

      expect(moduleFactory.create).toHaveBeenCalledWith(injector);
    }));
  });

  describe('@loadLazyModule', () => {
    it('should return Promise', () => {
      expect(service.loadLazyModule('')).toEqual(jasmine.any(Promise));
    });

    it('should prevent execute if path not defined', fakeAsync(() => {
      expect(service.loadLazyModule('').catch((err) => {
        expect(err).toEqual(jasmine.any(String));
        expect(err.includes('module')).toBeTruthy();
        expect(err.includes('not defined')).toBeTruthy();
      }));

      tick();

      expect(loader.load).not.toHaveBeenCalled();
    }));

    it('should load module via angular factory', fakeAsync(() => {
      service.loadLazyModule('/foo');
      tick();

      expect(loader.load).toHaveBeenCalledWith('/foo');
    }));
  });
});
