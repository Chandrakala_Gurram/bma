import { fakeAsync, tick } from '@angular/core/testing';
import { RacingResultsService } from '@core/services/sport/racing-results.service';
import { LoadByPortionsService } from '@ss/services/load-by-portions.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { SiteServerRequestHelperService } from '@core/services/siteServerRequestHelper/site-server-request-helper.service';
import { IRacingEvent } from '@core/models/racing-event.model';
import { IRacingResultedEventResponse } from '@core/models/racing-result-response.model';
import { IRacingMarket } from '@core/models/racing-market.model';
import { HorseracingService } from '@core/services/racing/horseracing/horseracing.service';
import { GreyhoundService } from '@core/services/racing/greyhound/greyhound.service';
import { RoutingState } from '@shared/services/routingState/routing-state.service';
import { SiteServerService } from '@core/services/siteServer/site-server.service';
import { TemplateService } from '@shared/services/template/template.service';
import {
  expectedResultedOutcomes,
  expectedResultedWEWMarket,
  virtualRacingResultMock
} from '@core/services/sport/racing-results.service.mock';

describe('RacingResultsService', () => {
  let service: RacingResultsService;

  let loadByPortionsService: Partial<LoadByPortionsService>,
    localeService: Partial<LocaleService>,
    siteServerRequestHelperService: Partial<SiteServerRequestHelperService>,
    siteServer: Partial<SiteServerService>,
    horseRacingService: Partial<HorseracingService>,
    greyhoundService: Partial<GreyhoundService>,
    routingState: Partial<RoutingState>,
    templateService: Partial<TemplateService>;

  let racingResult: IRacingResultedEventResponse,
    event: IRacingEvent;

  beforeEach(() => {
    event = {
      id: '12200197',
      markets: [{
        id: 1,
        hasResults: true,
        templateMarketName: 'top 3 first'
      } as any, {
        id: 2,
        templateMarketName: 'win or each way'
      } as any, {
        id: 3,
        hasResults: true,
        templateMarketName: 'win or each way',
        outcomes: [{
          id: 1,
          nonRunner: true
        }, {
          id: 2,
          name: 'lorem'
        }, {
          id: 3,
          name: 'ipsum'
        }, {
          id: 5,
          name: 'dolor'
        }, {
          id: 4,
          name: 'ameno',
          nonRunner: true
        }, {
          id: 7,
          name: 'avizo'
        }]
      } as any],
      sortedMarkets: [
        {
          id: 3,
          priceTypeCodes: 'SP,',
          ncastTypeCodes: 'Lorem',
          hasResults: true,
          outcomes: [{
            id: 91,
            results: {
              position: '1',
              resultCode: 'P',
              priceDec: '2.3'
            },
          }, {
            id: 1,
            nonRunner: true
          }, {
            id: 3,
            results: {}
          }]
        },
        {
          id: 5,
          outcomes: [{
            id: 141,
            results: {
              position: '2'
            },
          }]
        }
      ],
      resultedWEWMarket: {
        hasResults: true,
        hasPositions: true,
        outcomes: [
          {
            name: 'N/R test',
            results : {
              resultCode : 'V',
              position: 1
            }
          }
        ],
        nonRunners : []
      }
    } as any;

    racingResult = [
      {
        resultedEvent: {
          children: [{
            resultedMarket: {
              id: 3,
              children: [{
                resultedOutcome: {
                  id: 2,
                  children: [{
                    resultedPrice: {
                      priceTypeCode: 'SP',
                      priceNum: 4,
                      priceDec: '1.2'
                    }
                  },
                    {
                      resultedPrice: {
                        priceTypeCode: 'LP',
                        priceNum: 3
                      }
                    }],
                  position: 3,
                  resultCode: 'P'
                }
              }, {
                resultedOutcome: {
                  id: 3,
                  children: [{
                    resultedPrice: {
                      priceTypeCode: 'SP',
                      priceNum: 4
                    }
                  }],
                  resultCode: 'L'
                }
              }, {
                resultedOutcome: {
                  id: 1,
                  children: [{
                    resultedPrice: {
                      priceTypeCode: 'SP',
                      priceNum: 1,
                      priceDec: 3.4
                    }
                  }],
                  resultCode: 'W',
                  position: 1
                }
              }, {
                resultedOutcome: {
                  id: 4,
                  children: [{
                    resultedPrice: {
                      priceTypeCode: 'SP',
                      priceNum: 2.3
                    }
                  }],
                  resultCode: 'P',
                  position: 3
                }
              }, {
                resultedOutcome: {
                  id: 7,
                  children: [{
                    resultedPrice: {
                      priceTypeCode: 'SP'
                    }
                  }],
                  resultCode: 'V'
                }
              }]
            }
          }]
        }
      }
    ] as any;

    localeService = {
      getString: jasmine.createSpy().and.returnValue('Lorem')
    };
    siteServerRequestHelperService = {
      getRacingResultsForEvent: jasmine.createSpy().and.returnValue(Promise.resolve([racingResult]))
    };
    siteServer = {
      loadResultsOfEvent: jasmine.createSpy().and.returnValue(Promise.resolve(racingResult))
    };
    horseRacingService = {
      getConfig: jasmine.createSpy().and.returnValue({ request: 'Lorem' }),
      addFavouriteLabelToOutcomesWithResults: jasmine.createSpy('addFavouriteLabelToOutcomesWithResults')
    };
    greyhoundService = {
      getConfig: jasmine.createSpy().and.returnValue({ request: 'Lorem' }),
      addFavouriteLabelToOutcomesWithResults: jasmine.createSpy('addFavouriteLabelToOutcomesWithResults')
    };
    routingState = {
      getCurrentSegment: jasmine.createSpy().and.returnValue('horseracing')
    };
    loadByPortionsService = {
      get: (func: Function, ...args) => {
        return func();
      }
    } as any;
    templateService = {
      genTerms: jasmine.createSpy().and.returnValue('Each Way: 1/5 Odds - Places 1-2-3')
    } as any;

    service = new RacingResultsService(
      loadByPortionsService as LoadByPortionsService,
      localeService as LocaleService,
      siteServerRequestHelperService as SiteServerRequestHelperService,
      siteServer as SiteServerService,
      horseRacingService as HorseracingService,
      greyhoundService as GreyhoundService,
      routingState as RoutingState,
      templateService as TemplateService
    );
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  describe('makeVoidMarket', () => {
    it('should create', () => {
      const market: any =  {
        cashoutAvail: 'cashoutAvail',
        correctPriceTypeCode: 'correctPriceTypeCode',
        dispSortName: 'dispSortName',
        eachWayFactorNum: 1,
        eachWayFactorDen: 1,
        eachWayPlaces: 'eachWayPlaces',
        outcomes: [
          {
            runnerNumber: 11,
            nonRunner: false,
            name: 'eachWayPlaces'
          }, {
            runnerNumber: 21,
            nonRunner: false,
            name: 'eachWayPlaces'
          }, {
            runnerNumber: 31,
            nonRunner: false,
            name: 'eachWayPlaces'
          }
        ]
      };
      service['makeVoidMarket'](market);
      expect(service['makeVoidMarket']).toBeTruthy();
    });
  });

  describe('#getRacingResults should concatenate all data', () => {
    it('#getRacingResults should attach results to event', fakeAsync(() => {
      service['mapResultsToOutcomes'] = jasmine.createSpy();
      service['getRacingRulesAndDividends'] = jasmine.createSpy();
      service['sortOutcomesByPositions'] = jasmine.createSpy().and.callFake(data => data);

      service.getRacingResults(event);
      tick();
      expect(siteServer.loadResultsOfEvent).toHaveBeenCalled();
      expect(service['mapResultsToOutcomes']).toHaveBeenCalledWith(racingResult, event);
      expect(service['racingService'].addFavouriteLabelToOutcomesWithResults).toHaveBeenCalled();
      expect(event.sortedMarkets[0].outcomes.length).toEqual(3);
      expect(service['sortOutcomesByPositions']).toHaveBeenCalledWith(event.resultedWEWMarket);
      expect(service['getRacingRulesAndDividends']).toHaveBeenCalled();
    }));

    it('#getRacingResults case with resultMarket.priceTypeCodes', fakeAsync(() => {
      service['mapResultsToOutcomes'] = jasmine.createSpy();
      service['getRacingRulesAndDividends'] = jasmine.createSpy();
      service['sortOutcomesByPositions'] = jasmine.createSpy().and.callFake(data => data);

      event.sortedMarkets[0].priceTypeCodes = 'LP,';
      event.resultedWEWMarket.hasResults = false;
      service.getRacingResults(event);
      tick();
      expect(service['racingService'].addFavouriteLabelToOutcomesWithResults).not.toHaveBeenCalled();
      expect(service['sortOutcomesByPositions']).toHaveBeenCalled();
      expect(service['getRacingRulesAndDividends']).toHaveBeenCalled();
    }));

    it('#getRacingResults case with no market hasResults', fakeAsync(() => {
      event.sortedMarkets[0].hasResults = undefined;
      event.resultedWEWMarket.hasPositions = false;
      service['mapResultsToOutcomes'] = jasmine.createSpy();
      service['getRacingRulesAndDividends'] = jasmine.createSpy();
      service['sortOutcomesByPositions'] = jasmine.createSpy().and.callFake(data => data);
      service['makeVoidMarket'] = jasmine.createSpy();

      service.getRacingResults(event);
      tick();
      expect(service['racingService'].addFavouriteLabelToOutcomesWithResults).not.toHaveBeenCalled();
      expect(service['sortOutcomesByPositions']).not.toHaveBeenCalled();
      expect(service['getRacingRulesAndDividends']).not.toHaveBeenCalled();
      expect(service['makeVoidMarket']).toHaveBeenCalled();
      expect(event.voidResult).toBeTruthy();
    }));

    describe('#mapResultsToOutcomes cases with rasing exceptions', () => {
      it('response = []', () => {
        expect((): void => service['mapResultsToOutcomes']([] as any, event)).toThrow();
      });

      it('response with children = []', () => {
        expect((): void => service['mapResultsToOutcomes']([{
          resultedEvent: {
            children: []
          }
        }] as any, event)).toThrow();
      });

      it('response with no resulted market', () => {
        expect((): void => service['mapResultsToOutcomes']([{
          resultedEvent: {
            children: [{
              resultedMarket: {
                children: []
              }
            }]
          }
        }] as any, event)).toThrow();
      });
    });
  });

  it('#getRacingRulesAndDividends should get rules and dividend to event', fakeAsync(() => {
    service['mapRulesDividendsToOutcomes'] = jasmine.createSpy();
    service['filterDividends'] = jasmine.createSpy();
    service['filterRulesDeduction'] = jasmine.createSpy();
    const resultedMarket = event.markets[0];

    service.getRacingRulesAndDividends(event, resultedMarket, false);
    tick();

    expect(siteServerRequestHelperService.getRacingResultsForEvent).toHaveBeenCalled();
    expect(service['mapRulesDividendsToOutcomes']).toHaveBeenCalledWith([racingResult], resultedMarket, false);
    expect(service['filterDividends']).toHaveBeenCalledWith(resultedMarket);
    expect(service['filterRulesDeduction']).toHaveBeenCalledWith(resultedMarket);
  }));

  describe('racingService', () => {
    it('should return HorseracingService instance', () => {
      expect(service['racingService']).toEqual(horseRacingService as any);
    });

    it('should return GreyhoundService instance', () => {
      routingState.getCurrentSegment = jasmine.createSpy().and.returnValue('greyhound');

      expect(service['racingService']).toEqual(greyhoundService as any);
    });
  });

  describe('removeVoidRunners', () => {
    let market;

    beforeEach(() => {
      market = {
        outcomes: [{
          results: {
            resultCode: 'V',
            priceDec: 10
          }
        }]
      } as any;
    });

    it('should remove outcomes with resultCode Void (=nonRunners)', () => {
      service['removeVoidRunners'](market);
      expect(market.outcomes).toEqual([]);
    });

    it('should extract outcomes with no void resultCode', () => {
      market.outcomes[0].results.resultCode = '';
      service['removeVoidRunners'](market);
      expect(market.outcomes).toEqual(market.outcomes);
    });
  });

  it('#mapResultsToOutcomes should map results to event', () => {
    service['formMarketInfo'] = jasmine.createSpy();
    service['mapResultsToOutcomes'](virtualRacingResultMock as any, event);

    expect(event.markets[2].outcomes).toEqual(expectedResultedOutcomes);
    expect(service['formMarketInfo']).toHaveBeenCalled();
  });

  it('#mapResultsToOutcomes should map results to virtual event', () => {
    service['formMarketInfo'] = jasmine.createSpy();
    event.isVirtual = true;
    service['mapResultsToOutcomes'](virtualRacingResultMock as any, event);

    expect(event.resultedWEWMarket).toEqual(expectedResultedWEWMarket);
    expect(service['formMarketInfo']).toHaveBeenCalled();
  });

  describe('#formMarketInfo add some properties to resulted market', () => {
    it('resultedMarket has all needed data', () => {
      const evt = {
        resultedWEWMarket: {
          id: 1,
          cashoutAvail: 'Y',
          viewType: 'handicaps',
          drilldownTagNames: 'MKTFLAG_BBAL'
        },
        sortedMarkets: []
      } as any;

      service['formMarketInfo'](evt);
      expect(evt.resultedWEWMarket.terms).toBe('Each Way: 1/5 Odds - Places 1-2-3');
      expect(evt.resultedWEWMarket.cashoutAvail).toBe('Y');
      expect(evt.resultedWEWMarket.viewType).toBe('handicaps');
      expect(evt.resultedWEWMarket.drilldownTagNames).toBe('MKTFLAG_BBAL');
    });

    it('resultedMarket has no needed data but sortedMarket[0] as wew has', () => {
      const evt = {
        resultedWEWMarket: {
          id: 1,
        },
        sortedMarkets: [{
          id: 1,
          cashoutAvail: 'Y',
          viewType: 'handicaps',
          drilldownTagNames: 'MKTFLAG_BBAL'
        }]
      } as any;

      service['formMarketInfo'](evt);
      expect(evt.resultedWEWMarket.terms).toBe('Each Way: 1/5 Odds - Places 1-2-3');
      expect(evt.resultedWEWMarket.cashoutAvail).toBe('Y');
      expect(evt.resultedWEWMarket.viewType).toBe('handicaps');
      expect(evt.resultedWEWMarket.drilldownTagNames).toBe('MKTFLAG_BBAL');
    });

    it('resultedMarket has no needed data but sortedMarket[0] is not wew', () => {
      const evt = {
        resultedWEWMarket: {
          id: 1,
        },
        sortedMarkets: [{
          id: 2,
          cashoutAvail: 'Y',
          viewType: 'handicaps',
          drilldownTagNames: 'MKTFLAG_BBAL'
        }]
      } as any;

      service['formMarketInfo'](evt);
      expect(evt.resultedWEWMarket.terms).toBe('Each Way: 1/5 Odds - Places 1-2-3');
      expect(evt.resultedWEWMarket.cashoutAvail).toBe('');
      expect(evt.resultedWEWMarket.viewType).toBe('');
      expect(evt.resultedWEWMarket.drilldownTagNames).toBe('');
    });
  });

  it('#mapRulesDividendsToOutcomes should map results', () => {
    // 1 case
    const response = [{
      racingResult: {
        children: [{
          ncastDividend: {
            type: 'FC',
            marketId: 3,
            dividend: '10',
            runnerNumbers: '2,6,'
          }
        }, {
          rule4Deduction: {
            marketId: 3
          }
        }, undefined]
      }
    }] as any;

    service['mapRulesDividendsToOutcomes'](response, event.sortedMarkets[0], true);
    expect(event.sortedMarkets[0].dividends).toEqual([{
            type: 'FC',
            name: 'Lorem',
            value: '10',
            runnerNumbers: '2,6,'
          }] as any);
    expect(event.sortedMarkets[0].rulesFourDeduction).toEqual([{
            marketId: 3
          }] as any);

  // 2 case
    const market = {
        id: 3,
        outcomes: [{}]
    } as any;
    service['mapRulesDividendsToOutcomes'](response, market, false);
    expect(market.dividends).toEqual([] as any);
    expect(market.rulesFourDeduction).toEqual(null);
  });

  it('#sortOutcomesByPositions should sort list', () => {
    const market = {
      outcomes : [{
        name: '2nd',
        results: {
          position: '2',
        },
      }, {
        name: '1st',
        results: {
          position: '1'
        },
      }]
    } as any;

    service['sortOutcomesByPositions'](market);
    expect(market.outcomes[0].name).toBe('1st');
  });

  it('#filterDividends (incl. #isNCastEnabled) should filter dividends', () => {
    // 1 case
    const market: IRacingMarket = {
      ncastTypeCodes: 'CT,TC,SF,CF,RF',
      dividends: [
        { type: 'FC', runnerNumbers: '3,4,' },
        { type: 'TC', runnerNumbers: '8,9,6,' },
        { type: 'FC', runnerNumbers: '7,8,' },
        { type: 'TC', runnerNumbers: '3,4,7,' }
      ],
      outcomes: [
        { runnerNumber: '3' },
        { runnerNumber: '4' },
        { runnerNumber: '7' }
      ]
    } as any;
    let resDiv = [
      { type: 'FC', runnerNumbers: '3-4' },
      { type: 'TC', runnerNumbers: '3-4-7' }
    ];
    service['filterDividends'](market);
    expect(market.dividends).toEqual(resDiv as any);

    // 2 case
    market.ncastTypeCodes = 'CT,TC';
    market.dividends = [
      { type: 'FC', runnerNumbers: '3,4,' },
      { type: 'TC', runnerNumbers: '3,4,7,' }
    ] as any;
    service['filterDividends'](market);
    resDiv = [
      { type: 'TC', runnerNumbers: '3-4-7' }
    ];
    expect(market.dividends).toEqual(resDiv as any);

    // 3 case
    market.ncastTypeCodes = 'SF,CF,RF';
    market.dividends = [
      { type: 'FC', runnerNumbers: '3,4,' },
      { type: 'TC', runnerNumbers: '3,4,7,' }
    ] as any;
    service['filterDividends'](market);
    resDiv = [
      { type: 'FC', runnerNumbers: '3-4' }
    ];
    expect(market.dividends).toEqual(resDiv as any);

    // 4 case
    market.ncastTypeCodes = undefined;
    market.dividends = [] as any;
    service['filterDividends'](market);
    expect(market.dividends).toEqual([]);
  });

  it('#isNCastEnabled should return false for unknown nCastCode param', () => {
    expect(service['isNCastEnabled']({} as any, 'RT')).toBe(false);
  });

  it('#filterRulesDeduction should filter rules for deduction', () => {
    let market: IRacingMarket = {
      rulesFourDeduction: [
        { id: 1, toDate: '2019-02-01T14:53:08.756Z' },
        { id: 2, toDate: '2019-02-03T14:53:08.756Z' },
        { id: 3, toDate: '2019-02-06T14:53:08.756Z' },
        { id: 4, toDate: '2019-02-02T14:53:08.756Z' },
        { id: 5, toDate: '2019-02-07T14:53:08.756Z' }
      ]
    } as any;
    service['filterRulesDeduction'](market);
    expect(market).toEqual({ rulesFourDeduction: [{ id: 5, toDate: '2019-02-07T14:53:08.756Z' }] } as any);

    market = {} as any;
    service['filterRulesDeduction'](market);
    expect(market).toEqual({} as any);
  });
});
