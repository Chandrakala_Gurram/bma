
import {of as observableOf } from 'rxjs';
import { UkToteEventsLinkingService } from './uktote-events-linking.service';
describe('UkToteEventsLinkingService', () => {
  let service: UkToteEventsLinkingService;

  let siteServerService;
  beforeEach(() => {
    siteServerService = {
      getEvent: jasmine.createSpy().and.returnValue(observableOf({}))
    };

    service = new UkToteEventsLinkingService(siteServerService);
  });

  describe('extendToteEvents:', () => {
    it('isScoop6Pool false', () => {
      service.extendToteEvents(<any>[{
        externalKeys: {
          OBEvLinkNonTote: 123456
        }
      }], false, {
        extendEvent: () => {},
        extendMarket: () => {},
        extendOutcome: () => {}
      });
      expect(siteServerService.getEvent).toHaveBeenCalledTimes(1);
      expect(siteServerService.getEvent).toHaveBeenCalledWith(
        '123456', { racingFormOutcome: true }, false
      );
    });

    it('isScoop6Pool true', () => {
      service.extendToteEvents(<any>[{
        id: 123456
      }], true, {
        extendEvent: () => {},
        extendMarket: () => {},
        extendOutcome: () => {}
      });
      expect(siteServerService.getEvent).toHaveBeenCalledTimes(1);
      expect(siteServerService.getEvent).toHaveBeenCalledWith(
        '~ext-OBEvLinkScoop6:event:123456',
        { racingFormOutcome: true, externalKeysEvent: true },
        false
      );
    });
  });

  it('extendToteEventInfo', () => {
    const extendEvent = jasmine.createSpy();
    const extendMarket = jasmine.createSpy();
    const extendOutcome = jasmine.createSpy();
    service.extendToteEventInfo(<any>{
      markets: [{
        outcomes: [{
          name: 'Hamilton'
        }]
      }]
    }, <any>{
      markets: [{
        outcomes: [{
          name: 'Hamilton'
        }]
      }]
    }, {
      extendEvent: extendEvent,
      extendMarket: extendMarket,
      extendOutcome: extendOutcome
    });

    expect(extendEvent).toHaveBeenCalledTimes(1);
    expect(extendMarket).toHaveBeenCalledTimes(1);
    expect(extendOutcome).toHaveBeenCalledTimes(1);
  });

  describe('compareOutcomes', () => {
    it('should return true if outcomes names match', () => {
      const outcome = {
          name: 'Diamond Express'
        },
        linkedOutocme = {
          name: 'Diamond Express'
        };
      expect(service['compareOutcomes'](<any>outcome, <any>linkedOutocme)).toBeTruthy();
    });

    it('should return true if outcomes names match (when name of' +
      ' one outcome is in camel case like in International TOTE)', () => {
      const outcome = {
          name: 'Diamond Express'
        },
        linkedOutocme = {
          name: 'DIAMOND EXPRESS'
        };
      expect(service['compareOutcomes'](<any>outcome, <any>linkedOutocme)).toBeTruthy();
    });

    it('should return false if outcomes names match', () => {
      const outcome = {
          name: 'Diamond Express'
        },
        linkedOutocme = {
          name: 'Duba Plains'
        };
      expect(service['compareOutcomes'](<any>outcome, <any>linkedOutocme)).toBeFalsy();
    });

    it('should return true if only main outcome become N/R', () => {
      const outcome = {
          name: 'Diamond Express N/R'
        },
        linkedOutocme = {
          name: 'Diamond Express'
        };
      expect(service['compareOutcomes'](<any>outcome, <any>linkedOutocme)).toBeTruthy();
    });

    it('should return true if only linked outcome become N/R', () => {
      const outcome = {
          name: 'Diamond Express'
        },
        linkedOutocme = {
          name: 'Diamond Express N/R'
        };
      expect(service['compareOutcomes'](<any>outcome, <any>linkedOutocme)).toBeTruthy();
    });

    it('should return true if only linked outcome become N/R ' +
      '(first of outcomes is in camel case like in International TOTE)', () => {
      const outcome = {
          name: 'DIAMOND EXPRESS'
        },
        linkedOutocme = {
          name: 'Diamond Express N/R'
        };
      expect(service['compareOutcomes'](<any>outcome, <any>linkedOutocme)).toBeTruthy();
    });

    it('should return true if only linked outcome become N/R ' +
      '(second of outcomes is in in camel case like in International TOTE)', () => {
      const outcome = {
          name: 'Diamond Express'
        },
        linkedOutocme = {
          name: 'DIAMOND EXPRESS N/R'
        };
      expect(service['compareOutcomes'](<any>outcome, <any>linkedOutocme)).toBeTruthy();
    });
  });

  describe('loadEventsByScoop6EventIds', () => {
    it('should call siteServerFactory.getEvent method', () => {
      const eventIds = [111, 222],
        options = { racingFormOutcome: true, externalKeysEvent: true },
        externalIds = '~ext-OBEvLinkScoop6:event:111,~ext-OBEvLinkScoop6:event:222';
      service['loadEventsByScoop6EventIds'](eventIds);
      expect(siteServerService.getEvent).toHaveBeenCalledWith(externalIds, options, false);
    });
  });

  describe('getPrimaryMarket', () => {
    it('should return first market according to displayOrder property', () => {
        expect(service['getPrimaryMarket'](({
          markets: [
            {
              id: 123,
              displayOrder: 100
            },
            {
              id: 549,
              displayOrder: 0
            }
          ]
        }) as any)).toEqual(({
          id: 549,
          displayOrder: 0
        }) as any);
    });

    it('should return null market propery missed', () => {
      expect(service['getPrimaryMarket'](({}) as any)).toEqual(null);
    });
  });
});
