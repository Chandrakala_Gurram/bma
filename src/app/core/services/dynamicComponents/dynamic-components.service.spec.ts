import { DynamicComponentsService } from '@core/services/dynamicComponents/dynamic-components.service';
import { of as observableOf } from 'rxjs';
import { ComponentRef } from '@angular/core';

describe('DynamicComponentsService', () => {
  let service: DynamicComponentsService;
  let componentFactoryResolver, dynamicComponentLoader, appRef, injector, componentReference, componentInstance;


  beforeEach(() => {
    componentInstance = {
      instance: {},
      hostView: {
        rootNodes: [{}]
      },
      destroy: jasmine.createSpy('destroy')
    };
    componentReference = {
      create: jasmine.createSpy('create').and.returnValue(componentInstance),
      instance: componentInstance
    };
    componentFactoryResolver = {
      resolveComponentFactory: jasmine.createSpy('resolveComponentFactory')
        .and.returnValue(componentReference)
    };
    appRef = {
      attachView: jasmine.createSpy('attachView'),
      detachView: jasmine.createSpy('detachView')
    };
    injector = {};
    dynamicComponentLoader = {};
    service = new DynamicComponentsService(
      componentFactoryResolver,
      dynamicComponentLoader,
      appRef,
      injector
    );
  });

  it('constructor', () => {
    expect(service).toBeDefined();
  });

  describe('#addComponent', () => {
    it('should create proper component', () => {
      const target = {
        insertBefore: jasmine.createSpy('insertBefore')
      } as any;
      const ref = document.createElement('p');
      service.addComponent({}, {}, target, ref);
      expect(componentFactoryResolver.resolveComponentFactory).toHaveBeenCalledWith({});
      expect(appRef.attachView).toHaveBeenCalledWith(componentInstance.hostView);
    });

    it('should return proper component instance', () => {
      const target = {
        insertBefore: jasmine.createSpy('insertBefore')
      } as any;
      const ref = document.createElement('p');
      const comp = service.addComponent({}, {}, target, ref);
      comp.destroy();

      expect(appRef.detachView).toHaveBeenCalledWith(componentInstance.hostView);
      expect(componentInstance.destroy).toHaveBeenCalled();
      expect(comp.instance.isPanelShown).toBeDefined();
    });
  });

  it('#setBindings should set and call proper bindings', () => {
    const ref = {
      instance: {
        fn$: observableOf({}),
        staticVal: null
      }
    } as ComponentRef<any>;
    const params = {
      fn$: jasmine.createSpy(),
      staticVal: 1
    };
    service.setBindings(ref, params);

    expect(params.fn$).toHaveBeenCalled();
    expect(ref.instance.staticVal).toBe(1);
  });
});
