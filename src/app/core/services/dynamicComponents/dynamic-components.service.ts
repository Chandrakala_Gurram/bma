import {
  Injectable,
  Injector,
  ComponentFactoryResolver,
  ApplicationRef,
  EmbeddedViewRef,
  ComponentFactory,
  ComponentRef,
  ViewContainerRef } from '@angular/core';
import * as _ from 'underscore';
import { IDynamicComponent, IComponentInstance } from '@core/services/dynamicComponents/dynamic-components.model';
import { DynamicLoaderService } from '@app/dynamicLoader/dynamic-loader.service';
import { Subject } from 'rxjs';

@Injectable()
export class DynamicComponentsService {

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private dynamicComponentLoader: DynamicLoaderService,
    private appRef: ApplicationRef,
    private injector: Injector
  ) { }

  /**
   * Adds component's element to DOM, attaches to the appRef
   *
   * @param newComponent - the angular component to be added
   * @param data - data (basically inputs) of component' instance
   * @param target - target (parent) node that receives new component. Omit to append component to body.
   * @param referenceNode - existing node to put component before. Omit to append component to parent.
   * @return IDynamicComponent - api with destroy function and reference to component instance
   */
  addComponent(
    newComponent: any,
    data: IComponentInstance = {},
    target: Node = document.body,
    referenceNode: Node = null
  ): IDynamicComponent {
    const appRef = this.appRef;
    const componentRef = this.componentFactoryResolver
      .resolveComponentFactory(newComponent)
      .create(this.injector);
    const componentInstance = <IComponentInstance>componentRef.instance;

    Object.assign(componentInstance, data, {isPanelShown: true} );
    appRef.attachView(componentRef.hostView);
    const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    target.insertBefore(domElem, referenceNode);

    return {
      destroy: function () {
        appRef.detachView(componentRef.hostView);
        componentRef.destroy();
      },
      instance: componentInstance
    };
  }

  /**
   * Creates dynamic component
   *
   * @param ref - reference
   * @param view - view container
   * @param factory - factory component creator
   * @param params - optional parameter if we want to pass some data
   */
  createDynamicComponent(ref: ComponentRef<any>,
                         view: ViewContainerRef,
                         factory: ComponentFactory<any>,
                         params?: { [key: string]: any }): ComponentRef<any> {
    ref && ref.destroy();
    ref = view.createComponent(factory, 0, view.parentInjector);
    this.setBindings(ref, params);
    return ref;
  }

  setBindings(ref: ComponentRef<any>, params?: { [key: string]: any }) {
    if (params) {
      _.each(params, (val, key) => {
        if (_.isFunction(val)) {
          ref.instance[key].subscribe((...args) => val(...args));
        } else {
          ref.instance[key] = val;
        }
      });
    }
  }

  /**
   * Adds component's element to DOM, attaches to the appRef
   *
   * @param dynamicComp - registered component
   * @param ref - reference
   * @param view - view container
   * @param factory - factory component creator
   */
  getDynamicComponent(dynamicComp,
                      ref: ComponentRef<any>,
                      view: ViewContainerRef,
                      factory: ComponentFactory<any>,
                      params?: { [key: string]: any }): Subject<any> {
    const s = new Subject();
    this.dynamicComponentLoader.getComponentFactory(dynamicComp, view && view.parentInjector)
      .subscribe((componentData: any) => {
        factory = componentData.factory;
        s.next(this.createDynamicComponent(ref, view, factory, params));
        s.complete();
      });
    return s;
  }
}
