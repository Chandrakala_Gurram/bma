import { debounceTime, filter, first } from 'rxjs/operators';
import { AsyncSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';

import * as _ from 'underscore';

import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { ISeoPagesPaths, ISeoPage } from '@core/services/cms/models';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';

/**
 * SEO data factory
 * Gets SEO configuration from CMS
 * Provides data for Static Block, meta description and page title.
 * @return {Object}
 */

@Injectable()
export class SeoDataService {
  defaultPage: Partial<ISeoPage> = {
    title: 'Online Sports Betting and Latest Odds - Coral',
    description: 'Betting has never been better with pre-event and in-play markets available from all over the world.' +
      ' Get the latest betting odds at Coral. Don\'t Bet Silly, Bet Savvy!'
  };

  private paths$: Observable<ISeoPagesPaths>;
  private flagPage$: Observable<string>;
  private page$: AsyncSubject<Partial<ISeoPage>> = new AsyncSubject<Partial<ISeoPage>>();
  private activePage: Partial<ISeoPage>;
  private executed: boolean = false;
  private isHashChangeListenerSet: boolean = false;

  constructor(
    private windowRef: WindowRefService,
    private cms: CmsService,
    private pubsub: PubSubService,
    private router: Router
  ) {
    this.getSeoPagesPaths();
  }

  /**
   * getPage should be handled only once.
   * pagePromiseHandled flag is used to prevent
   * seoStaticBlock component from executing on each 'link'.
   */
  getPage(): Observable<Partial <ISeoPage> | string> {
    if (!this.executed) {
      this.executed = true;
      return this.page$;
    }

    this.flagPage$ = new Observable<string>(observer => {
      observer.error('pagePromiseHandled');
      observer.complete();
    });

    return this.flagPage$;
  }

  private getSeoPagesPaths(): void {
    this.paths$ = new Observable<ISeoPagesPaths>(observer => {
      this.cms.getSeoPagesPaths()
        .subscribe(paths => {
          observer.next(paths);

          this.handlePaths(paths);

          if (!this.isHashChangeListenerSet) {
            /* tslint:disable */
            this.router.events.pipe(
                filter((event: Event) => event instanceof NavigationEnd),
                debounceTime(100)
              )
              .subscribe((event: Event) => {
                this.handlePaths(paths);
              });
            /* tslint:enable */

            this.isHashChangeListenerSet = true;
          }
        });
    }).pipe(first());

    this.paths$.subscribe();
  }

  private handlePaths(paths: ISeoPagesPaths): void {
    const fullPath: string = this.windowRef.nativeWindow.location.pathname,
      lookupPath: string = fullPath.indexOf('/static') === 0 ? fullPath.substring(7) : fullPath;
    // if URL is /static/football only /football should be used

    // If SEO is configured - get seoData for current location and update page
    if (paths[lookupPath]) {
      this.cms.getSeoPage(paths[lookupPath]).subscribe(data => this.doUpdate(data));

      // If SEO is not configured - set default values
    } else if (!_.isEqual(this.activePage, this.defaultPage)) {
      this.doUpdate(this.defaultPage);
    }
  }

  private doUpdate(page: Partial<ISeoPage>): void {
    const document: Document = this.windowRef.nativeWindow.document;
    // Notify listeners on initial load.
    // Promise is used instead of Connect to notify listeners on initial load.
    // This is needed because listeners may initialize after getSeoPagesPaths call is completed.
    this.page$.next(page);
    this.page$.complete();

    // Notify listeners on subsequent changes
    this.pubsub.publish(this.pubsub.API.SEO_DATA_UPDATED, [page]);

    this.activePage = page;

    // Set meta description
    document.querySelector('meta[name=description]').setAttribute('content', page.description);
    document.title = page.title;
  }
}
