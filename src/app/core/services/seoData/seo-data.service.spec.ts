import { of as observableOf, Subject } from 'rxjs';
import { NavigationEnd, NavigationStart } from '@angular/router';
import { fakeAsync, tick } from '@angular/core/testing';

import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';
import { SeoDataService } from './seo-data.service';

describe('SeoDataService', () => {
  let service: SeoDataService;
  let windowRef;
  let cms;
  let pubsub;
  let router;
  let routeSubject;
  let domElement;

  beforeEach(() => {
    routeSubject = new Subject();
    domElement = {
      setAttribute: jasmine.createSpy('setAttribute')
    };
    windowRef = {
      nativeWindow: {
        location: {
          pathname: '/'
        },
        document: {
          querySelector: jasmine.createSpy('querySelector').and.returnValue(domElement)
        }
      }
    };
    cms = {
      getSeoPagesPaths: jasmine.createSpy('getSeoPagesPaths').and.returnValue(observableOf({
        '/': 'askjdhsakjd',
        'test': 'bcd'
      })),
      getSeoPage: jasmine.createSpy('getSeoPage').and.returnValue(observableOf({
        '/': 'askjdhsakjd',
        'test': 'bcd'
      }))
    };
    pubsub = {
      publish: jasmine.createSpy('publish'),
      API: pubSubApi
    };
    router = {
      events: routeSubject
    };
  });

  function createService() {
    service = new SeoDataService(windowRef, cms, pubsub, router);
  }

  it('should have default title and description', () => {
    createService();

    expect(service.defaultPage).toEqual(jasmine.objectContaining({
      title: 'Online Sports Betting and Latest Odds - Coral',
      description: 'Betting has never been better with pre-event and in-play markets available from all over the world.' +
        ' Get the latest betting odds at Coral. Don\'t Bet Silly, Bet Savvy!'
    }));
  });

  it('should getSeoPagesPaths', fakeAsync(() => {
    createService();

    expect(cms.getSeoPagesPaths).toHaveBeenCalledTimes(1);
    expect(cms.getSeoPage).toHaveBeenCalledWith('askjdhsakjd');

    windowRef.nativeWindow.location.pathname = 'test';
    routeSubject.next(new NavigationEnd(0, 'test', 'test'));
    tick(150);

    expect(cms.getSeoPage).toHaveBeenCalledWith('bcd');
    expect(cms.getSeoPage).toHaveBeenCalledTimes(2);
  }));

  it('getPage', () => {
    createService();

    service['executed'] = false;
    expect(service.getPage()).toBeDefined();
    service['executed'] = true;
    expect(service.getPage()).toBeDefined();
  });

  it('doUpdate', () => {
    createService();

    service['doUpdate'](<any>{
      description: 'abc'
    });
    expect(windowRef.nativeWindow.document.querySelector).toHaveBeenCalledWith('meta[name=description]');
    expect(domElement.setAttribute).toHaveBeenCalledWith('content', 'abc');
    expect(pubsub.publish).toHaveBeenCalledWith(pubSubApi.SEO_DATA_UPDATED, [{
      description: 'abc'
    }]);
  });

  describe('constructor with getSeoPagesPaths', () => {
    it('should subscribe only once to router events', fakeAsync(() => {
      spyOn(router.events, 'subscribe').and.callThrough();

      createService();
      service['getSeoPagesPaths']();
      tick();

      expect(cms.getSeoPagesPaths).toHaveBeenCalledTimes(2);
      expect(router.events.subscribe).toHaveBeenCalledTimes(1);
    }));

    it('should handle only NavigationEnd router event', fakeAsync(() => {
      createService();

      windowRef.nativeWindow.location.pathname = 'test';
      routeSubject.next(new NavigationStart(0, 'test', ));
      tick(150);

      expect(cms.getSeoPage).toHaveBeenCalledTimes(1);
    }));
  });
});
