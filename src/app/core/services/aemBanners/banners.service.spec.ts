import { BannersService } from 'app/core/services/aemBanners/banners.service';
import { IOfferFromServer } from '../../models/aem-banners-section.model';
import Utils, { BRANDS_FOR_AEM } from './utils';
import { of } from 'rxjs';
import { fakeAsync, flush } from '@angular/core/testing';
import * as _ from 'underscore';
import environment from '@environment/oxygenEnvConfig';
import { AsyncScriptLoaderService } from '@core/services/asyncScriptLoader/async-script-loader.service';
const singleOfferFromLibrary = require('./test/data/singleOfferFromLibrary.json');
const { fromServer, toComponent } = singleOfferFromLibrary;
const offersFromTarget = require('./test/data/targetOffersParsing.json');

describe('BannersService', () => {
  let service: BannersService;
  let httpClient;
  let asyncLoader: AsyncScriptLoaderService;

  const AT_PROPERTY_VALUE = '123';
  const BETSLIP_AT_PROPERTY_VALUE = '456';

  beforeEach(() => {
    asyncLoader = {
      loadJsFile: () => of([])
    } as any;

    environment.AEM_CONFIG = {
      server: 'http://lc-aem.com',
      at_property: AT_PROPERTY_VALUE,
      betslip_at_property: BETSLIP_AT_PROPERTY_VALUE
    };

    httpClient = {
      get: jasmine.createSpy('get')
    };

    service = new BannersService(
      httpClient,
      asyncLoader
    );
  });

  it('Library success and target fail executes', (done: DoneFn) => {
    spyOn<any>(service, 'getTargetOffers').and.returnValue(of({resolved: false, data: null, message: null}));
    spyOn<any>(service, 'getLibraryOffers').and.returnValue(of({resolved: true, data: fromServer, message: null}));

    service.fetchOffersFromAEM({page: 'page', brand: 'coral', maxOffers: 10}).subscribe((dataForCarousel) => {
      done();
    });
  });

  it('Library success but empty result', (done: DoneFn) => {
    spyOn<any>(service, 'getTargetOffers').and.returnValue(of({resolved: false, data: null, message: null}));
    spyOn<any>(service, 'getLibraryOffers').and.returnValue(of({resolved: true, data: null, message: null}));

    service.fetchOffersFromAEM({page: 'page', brand: 'coral', maxOffers: 10}).subscribe((dataForCarousel) => {
      expect(dataForCarousel.offers).toEqual([]);
      done();
    });
  });

  it('Test offers not resolved when TARGET_ENABLED = false', fakeAsync(() => {
    (<any>service)['TARGET_ENABLED'] = false;
    service['getTargetOffers']('target-global-mbox', '423432', {}).subscribe((result) => {
      expect(result.resolved).toEqual(false);
      expect(result.data).toEqual( null);
    });
    flush();
  }));

  it('Test offers not resolved if not all required params present', fakeAsync(() => {
    (<any>service)['TARGET_ENABLED'] = true;
    (<any>service)['REQUIRED_TARGET_PARAMS'] = ['par1'];
    service['getTargetOffers']('target-global-mbox', '423432', {par2: 2}).subscribe((result) => {
      expect(result.resolved).toEqual(false);
      expect(result.data).toEqual( null);
    });
    flush();
  }));

  it('Test offers not resolved if Adobe Target library not resolved', fakeAsync(() => {
    (<any>window).adobe = undefined;
    (<any>service)['_settings'] = {atJsLoadingTimeout: 3000};
    (<any>service)['TARGET_ENABLED'] = true;
    (<any>service)['REQUIRED_TARGET_PARAMS'] = ['par1'];

    let resolved = false;

    const offers = service['getTargetOffers']('target-global-mbox', '423432', {par1: 1}).subscribe((result) => {
      expect(result.resolved).toEqual(false);
      expect(result.data).toEqual(null);
      resolved = true;
    });
    flush();
    offers.unsubscribe();
    expect(resolved).toBeFalsy();
  }));

  it('Test offers resolved if all preconditions passing', (done: DoneFn) => {
    (<any>window).adobe = {
      target: {
        getOffer: (params: any) => {
          params.success([{name: 'banner1'}, {name: 'banner2'}]);
        }
      }
    };
    spyOn<any>(service, 'resetAtPropertyAndRequestTargetBanners').and.returnValue(of({
      resolved: true,
      data: [{name: 'banner1'}, {name: 'banner2'}]
    }));
    (<any>service)['_settings'] = {atJsLoadingTimeout: 3000};
    (<any>service)['TARGET_ENABLED'] = true;
    (<any>service)['REQUIRED_TARGET_PARAMS'] = ['par1'];

    service['getTargetOffers']('target-global-mbox', '423432', {par1: 1}).subscribe((result) => {
      expect(result.resolved).toEqual(true);
      expect(result.data).toEqual( [{name: 'banner1'}, {name: 'banner2'}]);
      done();
    }).unsubscribe();
  });

  it('Test resetAtPropertyAndRequestTargetBanners calling', (done: DoneFn) => {
    spyOn<any>(Utils, 'doPoll').and.returnValue(of({resolved: true}));
    spyOn<any>(service, 'parseTargetResponse').and.returnValue([{name: 'banner1'}, {name: 'banner2'}]);
    (<any>window).adobe = {
      target: {
        getOffer: (params: any) => {
          params.success([{name: 'banner1'}, {name: 'banner2'}]);
        }
      }
    };
    spyOn<any>(service, 'resetAtPropertyAndRequestTargetBanners').and.callThrough();
    (<any>service)['_settings'] = {atJsLoadingTimeout: 3000};
    (<any>service)['TARGET_ENABLED'] = true;
    (<any>service)['REQUIRED_TARGET_PARAMS'] = ['par1'];

    service['getTargetOffers']('target-global-mbox', '423432', {par1: 1}).subscribe((result) => {
      expect(result.resolved).toEqual(true);
      expect(result.data).toEqual( [{name: 'banner1'}, {name: 'banner2'}]);
      expect(service['resetAtPropertyAndRequestTargetBanners']).toHaveBeenCalled();

      done();
    }).unsubscribe();
  });

  it('Test resetAtPropertyAndRequestTargetBanners calling with error handled properly', (done: DoneFn) => {
    spyOn<any>(Utils, 'doPoll').and.returnValue(of({resolved: true}));
    spyOn<any>(service, 'parseTargetResponse').and.returnValue([{name: 'banner1'}, {name: 'banner2'}]);
    (<any>window).adobe = {
      target: {
        getOffer: (params: any) => {
          params.error('status', 'message');
        }
      }
    };
    spyOn<any>(service, 'resetAtPropertyAndRequestTargetBanners').and.callThrough();
    (<any>service)['_settings'] = {atJsLoadingTimeout: 3000};
    (<any>service)['TARGET_ENABLED'] = true;
    (<any>service)['REQUIRED_TARGET_PARAMS'] = ['par1'];

    service['getTargetOffers']('target-global-mbox', '423432', {par1: 1}).subscribe((result) => {
      expect(result.resolved).toEqual(false);
      expect(result.data).toEqual( null);
      expect(service['resetAtPropertyAndRequestTargetBanners']).toHaveBeenCalled();

      done();
    }).unsubscribe();
  });

  it('Throw error on missing required params  for response.json', (done: DoneFn) => {
    try {
      service['createLibraryEndpoint'](<any>{
        sth: 'sth',
      });
    } catch (error) {
      expect(error).toBeDefined();
      done();
    }
  });

  it('Generates proper request for response.json', (done: DoneFn) => {
    const requestString: string = service['createLibraryEndpoint']({
      page: 'page',
      brand: 'coral',
      channel: 'mobile',
      locale: 'en_gb',
      userType: 'plain',
      imsLevel: '11',
    });
    expect(requestString).toEqual(`${service['AEM_CONFIG'].server}/bin/lc/coral/offers.json` +
      `/locale/en_gb/channels/mobile/pages/page/userType/plain/imsLevel/11/response.json`);
    done();
  });

  it('Error is thrown when library fail and target fail', (done: DoneFn) => {
    spyOn<any>(service, 'getTargetOffers').and.returnValue(of({resolved: false, data: null, message: null}));
    spyOn<any>(service, 'getLibraryOffers').and.returnValue(of({resolved: false, data: null, message: null}));

    service.fetchOffersFromAEM({page: 'page', brand: 'coral', maxOffers: 10}).subscribe((dataForCarousel) => {}, (error => {
      expect(error).toBeDefined();
      done();
    }));
  });

  it('Requests offers from library', (done: DoneFn) => {
    spyOn<any>(service, 'getTargetOffers').and.returnValue(of({resolved: false, data: null, message: 'Rejection reason'}));
    spyOn<any>(service, 'getLibraryOffers').and.callThrough();
    (<any>service['httpClient']) = {
      get: (url: string) => {
        return of(fromServer);
      }
    };

    service.fetchOffersFromAEM({
      page: 'page',
      brand: 'coral',
      maxOffers: 7,
      device: 'web',
      channel: 'mobile',
      locale: 'en_gb',
      userType: 'plain',
      imsLevel: '11'
    }).subscribe((dataForCarousel) => {
      const actualOffer = dataForCarousel.offers[0];
      const expectedOffer = toComponent.offers[0];

      expect(actualOffer.brand).toBe(expectedOffer.brand);
      expect(actualOffer.imgUrl).toBe(expectedOffer.imgUrl);
      expect(actualOffer.altText).toBe(expectedOffer.altText);
      expect(actualOffer.title).toBe(expectedOffer.title);
      expect(actualOffer.link).toBe(expectedOffer.link);
      expect(actualOffer.target).toBe(expectedOffer.target);
      expect(actualOffer.tcText).toBe(expectedOffer.tcText);
      expect(actualOffer.tcLink).toBe(expectedOffer.tcLink);
      expect(actualOffer.position).toBe(expectedOffer.position);
      expect(actualOffer.lazy).toBe(expectedOffer.lazy);
      expect(actualOffer.imgClass).toBe(expectedOffer.imgClass);

      done();
    });
  });

  it('Requests offers from Target', (done: DoneFn) => {
    spyOn<any>(service, 'getTargetOffers').and
      .returnValue(of({resolved: true, data: offersFromTarget.fromTargetParsed, message: null}));
    spyOn<any>(service, 'getLibraryOffers').and
      .returnValue(of({resolved: false, data: null, message: 'Rejection reason'}));

    service.fetchOffersFromAEM({
      page: 'page',
      brand: 'coral',
      maxOffers: 7,
      device: 'web'
    }).subscribe((dataForCarousel) => {
      const actualOffer = dataForCarousel.offers[0];
      expect(actualOffer.imgUrl).toEqual('https://banners-cms-assets-stg.coral.co.uk' +
        '/is/image/stageladbrokescoral/ce=is%7Bstageladbrokescoral%2Fsingle_price_hero_NFL_gronkowski%7D');
      expect(actualOffer.personalised).toEqual(true);

      done();
    });
  });

  it('Parses and formats offer from library', (done: DoneFn) => {
    spyOn<any>(service, 'getTargetOffers').and.returnValue(of({resolved: false, data: null, message: 'Rejection reason'}));
    spyOn<any>(service, 'getLibraryOffers').and.returnValue(of({resolved: true, data: fromServer, message: 'Resolved'}));

    service.fetchOffersFromAEM({
      page: 'page',
      brand: 'coral',
      maxOffers: 7,
      device: 'web'
    }).subscribe((dataForCarousel) => {
      const actualOffer = dataForCarousel.offers[0];
      const expectedOffer = toComponent.offers[0];

      expect(actualOffer.brand).toBe(expectedOffer.brand);
      expect(actualOffer.imgUrl).toBe(expectedOffer.imgUrl);
      expect(actualOffer.altText).toBe(expectedOffer.altText);
      expect(actualOffer.title).toBe(expectedOffer.title);
      expect(actualOffer.link).toBe(expectedOffer.link);
      expect(actualOffer.target).toBe(expectedOffer.target);
      expect(actualOffer.tcText).toBe(expectedOffer.tcText);
      expect(actualOffer.tcLink).toBe(expectedOffer.tcLink);
      expect(actualOffer.position).toBe(expectedOffer.position);
      expect(actualOffer.lazy).toBe(expectedOffer.lazy);
      expect(actualOffer.imgClass).toBe(expectedOffer.imgClass);

      done();
    });
  });

  it('Pin offers order', (done: DoneFn) => {
    const pinnedOffers  = require('./test/data/fewPinnedOffersFromLibrary.json');
    spyOn<any>(service, 'getTargetOffers').and.returnValue(of({resolved: false, data: null, message: 'Rejection reason'}));
    spyOn<any>(service, 'getLibraryOffers').and.returnValue(of({resolved: true, data: pinnedOffers, message: 'Resolved'}));

    service.fetchOffersFromAEM({
      page: 'page',
      brand: 'coral',
      maxOffers: 7,
      device: 'web'
    }).subscribe((dataForCarousel) => {
      const actualOffer = dataForCarousel.offers;
      expect(actualOffer[0].id).toBe('id5');
      expect(actualOffer[0].position).toBe(1);
      expect(actualOffer[1].id).toBe('id2');
      expect(actualOffer[1].position).toBe(2);
      expect(actualOffer[actualOffer.length - 2].id).toBe('id4');
      expect(actualOffer[actualOffer.length - 2].position).toBe(5);
      expect(actualOffer[actualOffer.length - 1].id).toBe('idrg');
      expect(actualOffer[actualOffer.length - 1].position).toBe(6);
      done();
    });
  });

  it('Pin offers order: removed duplicates', (done: DoneFn) => {
    const pinnedOffers  = require('./test/data/fewPinnedOffersFromLibrary.json');
    pinnedOffers.offers[0].id = 'id3';
    spyOn<any>(service, 'getTargetOffers').and.returnValue(of({resolved: false, data: null, message: 'Rejection reason'}));
    spyOn<any>(service, 'getLibraryOffers').and.returnValue(of({resolved: true, data: pinnedOffers, message: 'Resolved'}));

    service.fetchOffersFromAEM({
      page: 'page',
      brand: 'coral',
      maxOffers: 7,
      device: 'web'
    }).subscribe((dataForCarousel) => {
      const actualOffer = dataForCarousel.offers;
      expect(actualOffer.length).toBe(5);
      expect(actualOffer[0].id).toBe('id5');
      expect(actualOffer[0].position).toBe(1);
      expect(actualOffer[1].id).toBe('id3');
      expect(actualOffer[1].position).toBe(2);
      expect(actualOffer[actualOffer.length - 2].id).toBe('id4');
      expect(actualOffer[actualOffer.length - 2].position).toBe(4);
      expect(actualOffer[actualOffer.length - 1].id).toBe('idrg');
      expect(actualOffer[actualOffer.length - 1].position).toBe(5);
      done();
    });
  });

  it('Parses response from target to server-offers format', (done: DoneFn) => {
    const actualOffers: IOfferFromServer[] = service.parseTargetResponse(offersFromTarget.fromTarget);
    const expectedOffers: IOfferFromServer[] = offersFromTarget.fromTargetParsed;

    expect(actualOffers.length).toBe(expectedOffers.length);
    for (let i = 0; i < actualOffers.length; ++i) {
      const actualOffer = actualOffers[i];
      const expectedOffer = actualOffers[i];

      expect(actualOffer.offerTitle).toBe(expectedOffer.offerTitle);
      expect(actualOffer.imgUrl).toBe(expectedOffer.imgUrl);
      expect(actualOffer.webTarget).toBe(expectedOffer.webTarget);
      expect(actualOffer.webTandC).toBe(expectedOffer.webTandC);
      expect(actualOffer.webTandCLink).toBe(expectedOffer.webTandCLink);
      expect(actualOffer.webUrl).toBe(expectedOffer.webUrl);
    }
    done();
  });

  it('Check case of target not resolved', (done: DoneFn) => {
    spyOn<any>(service, 'getLibraryOffers').and.returnValue(of({resolved: true, data: fromServer, message: null}));
    spyOn<any>(Utils, 'doPoll').and.returnValue(of({resolved: false}));

    service.fetchOffersFromAEM({
      page: 'page',
      brand: 'coral',
      maxOffers: 7,
      device: 'web'
    }).subscribe((dataForCarousel) => {
      done();
    });

  });

  it('Test Adobe Target banners requested for Betslip banners and error thrown on failure', (done: DoneFn) => {
    spyOn<any>(service, 'getTargetOffers').and.returnValue(of({
      resolved: false,
      data: null,
      message: null
    }));

    service.fetchBetslipOffersFromAEM({
      atJsLoadingTimeout: 3000
    }, {
      page: 'page'
    }).subscribe((betslipOffers) => {
      // never reached
    }, (error) => {
      expect(error).toBeDefined();
      done();
    });
  });

  it('Test Adobe Target banners requested for Betslip', (done: DoneFn) => {
    // emulate Adobe Target presence
    spyOn<any>(service, 'getTargetOffers').and.returnValue(of({
      resolved: true,
      data: [{link: 'http://link.com', name: 'banner1'}],
      message: null
    }));

    service.fetchBetslipOffersFromAEM({
      atJsLoadingTimeout: 3000
    }, {
      page: 'page'
    }).subscribe((betslipOffers) => {
      expect(service['getTargetOffers']).toHaveBeenCalled();
      done();
    });
  });

  it('Test getTargetBanners is calling resetAtPropertyAndRequestTargetBanners', (done: DoneFn) => {
    spyOn<any>(service, 'resetAtPropertyAndRequestTargetBanners').and.returnValue(of({resolved: false}));
    spyOn<any>(Utils, 'doPoll').and.returnValue(of({resolved: true}));

    service['_settings'] = {atJsLoadingTimeout: 3000};
    service['getTargetOffers']('target-global-mbox', '8923479143', {page: 'home'}).subscribe((betslipOffers) => {
      expect(service['resetAtPropertyAndRequestTargetBanners']).toHaveBeenCalled();
      done();
    });
  });

  it('Test Adobe Target workspace set', (done: DoneFn) => {
    (<any>window).adobe = {target: {getOffer: () => true}};
    (<any>window).targetUtilities = {};

    service['resetAtProperty']('325345');
    expect((<any>window).targetUtilities.at_property()).toEqual('325345');
    done();
  });

  it('Test fetching betslip banners from Target', (done: DoneFn) => {
    // emulate Adobe Target presence
    (<any>window).adobe = {target: {getOffer: () => true }};
    spyOn<any>(Utils, 'doPoll').and.returnValue(of({resolved: true}));
    spyOn<any>(service, 'getTargetOffers').and.returnValue(of({
      resolved: true,
      data: [{link: 'http://link.com', name: 'banner1'}],
      message: null
    }));
    spyOn(service, 'formatOffer');

    service.fetchBetslipOffersFromAEM({
      atJsLoadingTimeout: 3000
    }, {
      page: 'page'
    }).subscribe((betslipOffers) => {
      expect(service.formatOffer).toHaveBeenCalled();
      done();
    });
  });

  it('Test feeding not all required parameters, error thrown', (done: DoneFn) => {
    spyOn<any>(service, 'getTargetOffers').and.returnValue(of({
      resolved: true,
      data: {test: 'test'},
      message: null
    }));

    service.fetchBetslipOffersFromAEM({
      atJsLoadingTimeout: 3000
    }, (<any>{wrongParam: '1'})).subscribe((betslipOffers) => {
      // never reached here
      expect(false);
      done();
    }, (error) => {
      // reassure that error handler activated
      expect(true);
      done();
    });
  });

  it('Test not feeding required params, error thrown', (done: DoneFn) => {
    spyOn<any>(service, 'getTargetOffers').and.returnValue(of({
      resolved: true,
      data: {test: 'test'},
      message: null
    }));

    service.fetchBetslipOffersFromAEM({
      atJsLoadingTimeout: 3000
    }, undefined).subscribe((betslipOffers) => {
      // never reached here
      expect(false);
      done();
    }, (error) => {
      // reassure that error handler activated
      expect(true);
      done();
    });
  });

  // Fix after stopping combining Roxanne and Mobenga
  describe('Links setting for brands', () => {
    const webUrl = 'http://weburl.com';
    const appUrl = 'http://appurl.com';
    const roxanneWebUrl = 'http://roxanneweburl.com';
    const roxanneAppUrl = 'http://appurl.com';

    beforeEach(() => {
      service['_settings'] = {};
    });

    it('Test temp link for Roxanne Web', (done: DoneFn) => {
      service['_settings'].device = 'web';
      service['_settings'].brand = BRANDS_FOR_AEM.ladbrokes;

      const offerWithLink = service.formatOffer(<any>{webUrl, appUrl, roxanneWebUrl, roxanneAppUrl});

      expect(offerWithLink.link).toEqual(roxanneWebUrl);
      done();
    });

    it('Test temp link for Roxanne App', (done: DoneFn) => {
      service['_settings'].device = 'app';
      service['_settings'].brand = BRANDS_FOR_AEM.ladbrokes;

      const offerWithLink = service.formatOffer(<any>{webUrl, appUrl, roxanneWebUrl, roxanneAppUrl});

      expect(offerWithLink.link).toEqual(roxanneAppUrl);
      done();
    });

    it('Test temp link for Roxanne App if special Roxanne field missing', (done: DoneFn) => {
      service['_settings'].device = 'app';
      service['_settings'].brand = BRANDS_FOR_AEM.ladbrokes;

      const offerWithLink = service.formatOffer(<any>{webUrl, appUrl, roxanneWebUrl });

      expect(offerWithLink.link).toEqual(appUrl);
      done();
    });

    it('Test temp link for OX Web', (done: DoneFn) => {
      service['_settings'].device = 'web';
      service['_settings'].brand = BRANDS_FOR_AEM.coral;

      const offerWithLink = service.formatOffer(<any>{webUrl, appUrl, roxanneWebUrl, roxanneAppUrl});

      expect(offerWithLink.link).toEqual(webUrl);
      done();
    });

    it('Test temp link for OX App', (done: DoneFn) => {
      service['_settings'].device = 'app';
      service['_settings'].brand = BRANDS_FOR_AEM.coral;

      const offerWithLink = service.formatOffer(<any>{webUrl, appUrl, roxanneWebUrl, roxanneAppUrl});

      expect(offerWithLink.link).toEqual(appUrl);
      done();
    });
  });

  describe('#mergeOffers', () => {
    const offers = <any>{
        library: [{ offerTitle: 'ffs lib 1' }, { offerTitle: 'ffs lib 2' }, { offerTitle: 'ffs lib 3' }],
        pinned: { first: { offerTitle: 'ffs pinned first' }, last: { offerTitle: 'ffs pinned last' } },
        rg: { offerTitle: 'ffs rd' }
      },
      equal = <any>[
        { offerTitle: 'ffs pinned first' },
        { offerTitle: 'ffs lib 1' },
        { offerTitle: 'ffs lib 2' },
        { offerTitle: 'ffs lib 3' },
        { offerTitle: 'ffs pinned last' },
        { offerTitle: 'ffs rd' }
      ];

    it('should show all 6 offers', () => {
      const itOffers = _.clone(offers),
        itEqual = _.clone(equal);
      service['_settings'] = { maxOffers: 6 };
      expect(service.mergeOffers(itOffers)).toEqual(itEqual);
    });

    it('should show 5 offers - without rg', () => {
      const itOffers = _.clone(offers),
        itEqual = _.clone(equal).slice(0, -1);

      delete itOffers.rg;

      service['_settings'] = { maxOffers: 6 };
      expect(service.mergeOffers(itOffers)).toEqual(itEqual);
    });

    it('should show 5 offers - including rg', () => {
      const itOffers = _.clone(offers),
        itEqual = _.clone(equal);
      itEqual.splice(3, 1);

      service['_settings'] = { maxOffers: 5 };
      expect(service.mergeOffers(itOffers)).toEqual(itEqual);
    });

    it('should show 2 offers - including rg', () => {
      const itOffers = _.clone(offers);

      service['_settings'] = { maxOffers: 2 };
      expect(service.mergeOffers(itOffers)).toEqual(<any>[{ offerTitle: 'ffs pinned first' }, { offerTitle: 'ffs rd' }]);
    });

    it('should show 2 offers - without rg', () => {
      const itOffers = _.clone(offers);

      delete itOffers.rg;

      service['_settings'] = { maxOffers: 2 };
      expect(service.mergeOffers(itOffers)).toEqual(<any>[{ offerTitle: 'ffs pinned first' }, { offerTitle: 'ffs pinned last' }]);
    });

    it('should show 1 offer - without rg', () => {
      const itOffers = _.clone(offers);

      service['_settings'] = { maxOffers: 1 };
      expect(service.mergeOffers(itOffers)).toEqual(<any>[{ offerTitle: 'ffs pinned first' }]);
    });

    it('should show 1 plain offer - without rg when pinned are missing', () => {
      const itOffers = _.clone(offers);

      delete itOffers.pinned;

      service['_settings'] = { maxOffers: 1 };
      expect(service.mergeOffers(itOffers)).toEqual(<any>[{ offerTitle: 'ffs lib 1' }]);
    });

  });
});
