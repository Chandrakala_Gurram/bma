import { forkJoin, of , Observable } from 'rxjs';

import { concatMap, delay, exhaustMap, map, timeout } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import Utils, { BRANDS_FOR_AEM } from './utils';
import { HttpClient } from '@angular/common/http';
import * as _ from 'underscore';

import {
  IOffer,
  IOfferFromServer,
  IOfferReport,
  IOfferGroupsFromServer,
  IBannersEndpointParams,
  IParams, IAEMTargetBetslipParameters, IAemConfig
} from '@core/models/aem-banners-section.model';

import {
  IBannerResponseData
} from '@core/services/aemBanners/banner.service.model';
import environment from '@environment/oxygenEnvConfig';
import { AsyncScriptLoaderService } from '@core/services/asyncScriptLoader/async-script-loader.service';

@Injectable({ providedIn: 'root' })
export class BannersService {
  readonly TARGET_MBOX = 'target-global-mbox';
  readonly TARGET_ENABLED = true;
  readonly REQUIRED_TARGET_PARAMS = ['page'];
  readonly REQUIRED_LIBRARY_PARAMS = [
    'brand',
    'locale',
    'channel',
    'page',
    'userType'
  ];

  private _settings;
  private AEM_CONFIG: IAemConfig;

  constructor(
    private httpClient: HttpClient,
    protected asyncScriptLoaderService: AsyncScriptLoaderService
  ) {
    this.AEM_CONFIG = environment.AEM_CONFIG;
  }

  public fetchBetslipOffersFromAEM(settings: IParams, targetParams: IAEMTargetBetslipParameters): Observable<{ offers: IOffer[] }> {
    this._settings = settings;

    const targetRequest = this.getTargetOffers(this.TARGET_MBOX, this.AEM_CONFIG.betslip_at_property, targetParams);

    return targetRequest.pipe(map((targetInspection: IBannerResponseData) => {
        if (targetInspection.resolved) {
          const offers: IOffer[] = targetInspection.data.map(offer => this.formatOffer(offer));
          return {offers};
        } else {
          throw(new Error('Fetching betslip banners failed'));
        }
      }));
  }

  /**
   * Gets offers to display in carousel - according to settings
   * @returns {Observable<IOfferReport>}
   */
  public fetchOffersFromAEM(optionsPar: IParams): Observable<IOfferReport> {
    this._settings = optionsPar;

    const targetRequest = this.getTargetOffers(this.TARGET_MBOX, this.AEM_CONFIG.at_property, {page: this._settings.page});
    const libraryRequest = this.getLibraryOffers(this._settings);

    // Wait for responses
    return forkJoin(targetRequest, libraryRequest)
      .pipe(map(([targetInspection, libraryInspection]: any[]) => {
        if (libraryInspection.resolved || targetInspection.resolved) {
          const offers: IOffer[] = this.groupAndMergeAndFormatOffers(targetInspection, libraryInspection);
          return ({offers});
        } else {
          throw (new Error('Providers failed'));
        }
      }));
  }

  /**
   *
   * @param targetInspection - object that represents results of request to Adobe Target server
   * @param libraryInspection - object that represents results of request to AEM server
   */
  public groupAndMergeAndFormatOffers(targetInspection: IBannerResponseData, libraryInspection: IBannerResponseData): IOffer[] {
    const offerGroups: IOfferGroupsFromServer = {};

    if (targetInspection.resolved) {
      offerGroups.target = targetInspection.data;
    }

    if (libraryInspection.resolved) {
      const _libraryInspection_value = libraryInspection.data ? libraryInspection.data : {};
      offerGroups.library = _libraryInspection_value.offers;
      offerGroups.pinned = _libraryInspection_value.pinned;
      if (_libraryInspection_value.rg) {
        offerGroups.rg = _libraryInspection_value.rg;
      }
    }

    this.setPersonalisedForGroups(offerGroups);
    const mergedOffers = this.mergeOffers(offerGroups);
    const formattedOffers: IOffer[] = mergedOffers.map(this.formatOffer.bind(this)).map(this.addPosition);

    return this.setOffersLazyOrNot(formattedOffers);
  }

  public setPersonalisedForGroups({target = [], library = [], pinned = {}, rg = null}): void {
    // set true for target
    target.forEach(offer => this.setPersonalised(offer, true));
    // set false for all other
    library.forEach(offer => this.setPersonalised(offer, false));
    this.setPersonalised((<any>pinned).first, false);
    this.setPersonalised((<any>pinned).last, false);
    this.setPersonalised(rg, false);
  }

  public mergeOffers(offerGroups: IOfferGroupsFromServer): IOffer[] {
    // separate by parts
    const target = offerGroups.target === undefined ? [] : offerGroups.target;
    const library = offerGroups.library === undefined ? [] : offerGroups.library;
    const pinned = offerGroups.pinned === undefined ? {} : offerGroups.pinned;
    const rg = offerGroups.rg;
    const startList = pinned.first ? [pinned.first] : [];
    const endList = pinned.last ? [pinned.last] : [];

    // deduplicate
    const _dedupe = Utils.dedupe([startList, endList, target, library]);
    const [ddStartList, ddEndList, ddTarget, ddLibrary] = _dedupe;

    // middle of carousel => plain + target
    const baseList = ddTarget.concat(ddLibrary);
    const atLeastOneNonRgPresent = !(_.isEmpty(baseList) && _.isEmpty(ddStartList) && _.isEmpty(ddEndList));
    const showRg = atLeastOneNonRgPresent && (this._settings.maxOffers !== 1) && (!_.isEmpty(rg)) ;
    const positionedItemsCount = ddStartList.length + ddEndList.length + (showRg ? 1 : 0);
    const upperLimit = (this._settings.maxOffers - positionedItemsCount > 0) ? (this._settings.maxOffers - positionedItemsCount) : (0);
    // trim plain + target
    const trimmedBaseList = baseList.slice(0, upperLimit);

    // construct carousel banners list => plain + target + pinned
    const orderedList: IOffer[] = [].concat(ddStartList, trimmedBaseList, ddEndList);
    if (showRg) {
      orderedList.push(rg);
    }

    let finalTrimmedList = orderedList;
    // reassure that exact number of banners that comes from settings is shown
    if (this._settings.maxOffers < finalTrimmedList.length) {
      finalTrimmedList = finalTrimmedList.slice(0, this._settings.maxOffers);
      if (showRg && (finalTrimmedList.length > 1)) {
        finalTrimmedList[finalTrimmedList.length - 1] = rg;
      }
    }

    return finalTrimmedList;
  }

  public formatOffer(offer: IOfferFromServer): IOffer {
    const isWebDevice = this._settings.device === 'web';
    const brand = this._settings.brand;
    let link;
    if (BRANDS_FOR_AEM.ladbrokes === brand) {
      link = isWebDevice ? offer.roxanneWebUrl : offer.roxanneAppUrl;
    }
    if (!link) {
      link = isWebDevice ? offer.webUrl : offer.appUrl;
    }
    return {
      brand: this._settings.brand,
      id: offer.id,
      imgUrl: offer.imgUrl,
      altText: offer.offerTitle,
      title: offer.offerTitle,
      link: link,
      target: isWebDevice ? offer.webTarget : offer.appTarget,
      tcText: offer.webTandC,
      tcLink: isWebDevice ? offer.webTandCLink : offer.mobTandCLink,
      personalised: offer.personalised
    };
  }

  public parseTargetResponse(res): IOfferFromServer[] {
    const regex = /<(target-banner)>([\s\S]*?)<\/\1>/g;
    const contentOffers = res.filter(type => {
      return type.action === 'setContent';
    });

    return Utils.flatMap(contentOffers, ({ content }) => {
      const offers = [];
      let result;
      // tslint:disable-next-line:no-conditional-assignment
      while ((result = regex.exec(content)) !== null) {
        offers.push(result[2]);
      }
      return offers;
    }).filter(string => {
      try {
        return JSON.parse(string);
      } catch (error) {
        return false;
      }
    }).map(JSON.parse);
  }

  public getTargetOffers(mbox: string, at_property: string, params: any): Observable<any> {
    return new Observable<{ enabled: boolean, message: string }>((observer) => {
      if (!this.TARGET_ENABLED) {
        observer.next({enabled: false, message: 'Request to Target disabled.'});
        observer.complete();
        return;
      }

      let validParams: boolean = true;
      this.REQUIRED_TARGET_PARAMS.forEach((key) => {
        if (Utils.isEmpty(params[key])) {
          validParams = false;
        }
      });
      if (!validParams) {
        const msg = `Required params to Adobe Target were not supplied.`;
        observer.next({enabled: false, message: msg});
        observer.complete();
        return;
      }

      observer.next({enabled: true, message: null});
      observer.complete();
    }).pipe(
      exhaustMap((statusObservable: { enabled: boolean, message: string }) => {
        if (!statusObservable.enabled) {
          return of(statusObservable);
        }
        let atLibsLoading: Observable<String[]> = forkJoin([
          this.asyncScriptLoaderService.loadJsFile('/assets/adobe/aam.js'),
          this.asyncScriptLoaderService.loadJsFile('/assets/adobe/at.js')
        ]);
        if (!this.isTargetLoaded()) {
          // add timeout for first lib loading - for initialization
           atLibsLoading = atLibsLoading.pipe(delay(500));
        }
        return atLibsLoading.pipe(map(() => statusObservable));
      }),
      concatMap((statusObservable: any) => {
        if (statusObservable.enabled) {
          return Utils.doPoll(this.isTargetLoaded.bind(this), this._settings.atJsLoadingTimeout);
        } else {
          return of({resolved: false, data: null, message: statusObservable.message});
        }
      }),
      concatMap((adobeTargetStatus: {resolved: boolean, message?: string}) => {
        if (adobeTargetStatus.resolved) {
          return this.resetAtPropertyAndRequestTargetBanners(mbox, at_property, params);
        } else {
          // window.adobe.target object undefined
          const msg = `at.js was not loaded during timeout or wrong parameters passed. ${adobeTargetStatus.message}`;
          console.warn(msg);
          return of({resolved: false, data: null, message: msg});
        }
      })
    );
  }

  public getLibraryOffers(params: IBannersEndpointParams) {
    const url = this.createLibraryEndpoint(params);
    return this.httpClient.get(url)
      .pipe(
        timeout(5000),
        map(value => {
          return {data: value, resolved: true, message: null};
        }));
  }

  private setPersonalised(offer: IOffer, personalised: boolean): void {
    if (offer) {
      offer.personalised = personalised;
    }
  }

  // That logic should be probably changed in the future
  private setOffersLazyOrNot(offers: any[]) {
    if (offers.length === 1) {
      offers[0].lazy = false;
      offers[0].imgClass = '';
    } else {
      offers.forEach((offer) => {
        offer.lazy = true;
        offer.imgClass = 'aem-lazy';
      });
    }
    return offers;
  }

  private resetAtPropertyAndRequestTargetBanners(mbox: string, at_property: string, params): Observable<IBannerResponseData> {
    return new Observable((obs) => {
      this.resetAtProperty(at_property);

      (<any>window).adobe.target.getOffer({
        mbox: mbox,
        params: params,
        success: (res) => {
          obs.next({resolved: true, data: this.parseTargetResponse(res), message: null});
          obs.complete();
        },
        // error from Adobe Target
        error: (status, message) => {
          obs.next({resolved: false, data: null, message: `${status}: ${message}`});
          obs.complete();
        }
      });
    });
  }

  private resetAtProperty(at_property: string) {
    if ((<any>window).targetUtilities) {
      (<any>window).targetUtilities.at_property = function () {
        return at_property;
      };
    }
  }

  private createLibraryEndpoint(params: IBannersEndpointParams) {
    this.REQUIRED_LIBRARY_PARAMS.forEach(key => {
      if (Utils.isEmpty(params[key])) {
        throw Error(`Invalid value for '${key}'`);
      }
    });

    const { brand, locale, channel, page, userType } = params;
    const baseURL = `${this.AEM_CONFIG.server}/bin/lc/` +
      `${brand}/offers.json/locale/${locale}/channels/${channel}/pages/${page}/userType/${userType}`;

    const optionalKeys = ['imsLevel'];
    const optionalParams = optionalKeys
      .filter(key => Object.keys(params).indexOf(key) !== -1)
      .reduce((str, key) => `${str}/${key}/${params[key]}`, '');

    return `${baseURL + optionalParams}/response.json`;
  }

  // tslint:disable-next-line
  private addPosition(offer, index) { // ToDo: Kosar to review
    return Utils.assign({}, offer, { position: index + 1 });
  }

  private isTargetLoaded() {
    try {
      if ((<any>window).adobe.target) {
        return true;
      }
    } catch (err) {
      return false;
    }
  }
}
