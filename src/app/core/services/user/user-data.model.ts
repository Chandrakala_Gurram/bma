import { Injectable } from '@angular/core';
import * as _ from 'underscore';

import { StorageService } from '../storage/storage.service';
import { ROULETTE_JOURNEY_KEY } from '@bma/constants/roulette-journey.constant';

const defaultData = {
  // main
  bppToken: null,
  sessionToken: null,
  username: null,
  // balances
  sportBalance: null,
  sportBalanceWithSymbol: null,
  pendingWithdrawals: null,
  // user info
  advertiser: null,
  address: null,
  accountBusinessPhase: null,
  accountClosed: false,
  ageVerificationStatus: null,
  birthDate: '',
  cardNumber: '',
  city: '',
  countryCode: null,
  currencySymbol: '£',
  currency: 'GBP',
  email: null,
  firstname: null,
  firstNews: null,
  lastname: null,
  loginPending: false,
  logoutPending: false,
  oddsFormat: 'frac',
  pin: null,
  playerCode: null,
  playerDepositLimits: null,
  postCode: null,
  previousLoginTime: null,
  profileId: null,
  sessionLimit: 0,
  signupDate: '',
  tcVersion: null,
  vipLevel: null,
  vipInfo: null,
  contactPreferences: null,
  payPalBA: '',
  payPalDepositFraudNetScriptsLoaded: false,

  // states
  firstLogin: false,
  isTemporaryCard: false,
  passwordResetLogin: false,
  previouslyLogged: false,
  quickDepositTriggered: false,
  showBalance: true,
  isRedirecting: false,
  isSignUpPending: false,
  showLogoutPopup: true,
  status: false,
  quickBetNotification: true,
  timeline: true,
  logInMessage: [],
  winAlertsToggled: false,
};

@Injectable()
export class IUserDataModel {

  public data: any = Object.preventExtensions(_.clone(defaultData));

  constructor(public storage: StorageService) {}

  set(...x) {
    _.extend(this.data, ...x);

    this.storage.set('USER', this.data);
  }

  get bppToken() {
    return this.data.bppToken;
  }
  get sessionToken() {
    return this.data.sessionToken;
  }
  get username() {
    return this.data.username;
  }
  get sportBalance(): string | number {
    return this.data.sportBalance;
  }
  get sportBalanceWithSymbol() {
    return this.data.sportBalanceWithSymbol;
  }
  get pendingWithdrawals() {
    return this.data.pendingWithdrawals;
  }
  get advertiser() {
    return this.data.advertiser;
  }
  get accountBusinessPhase() {
    return this.data.accountBusinessPhase;
  }
  get ageVerificationStatus() {
    return this.data.ageVerificationStatus;
  }
  get accountClosed() {
    return this.data.accountClosed;
  }
  get cardNumber() {
    return this.data.cardNumber;
  }
  get countryCode() {
    return this.data.countryCode;
  }
  get currency(): string {
    return this.data.currency;
  }
  get currencySymbol(): string {
    return this.data.currencySymbol;
  }
  get email() {
    return this.data.email;
  }
  get firstname() {
    return this.data.firstname;
  }
  get firstNews() {
    return this.data.firstNews;
  }
  get lastname() {
    return this.data.lastname;
  }
  get loginPending() {
    return this.data.loginPending;
  }
  get logoutPending() {
    return this.data.logoutPending;
  }
  get LCCP() {
    return this.data.LCCP;
  }
  get oddsFormat() {
    return this.data.oddsFormat;
  }
  get timeline() {
    return this.data.timeline;
  }
  get previouslyLogged() {
    return this.data.previouslyLogged;
  }
  get quickDepositTriggered() {
    return this.data.quickDepositTriggered;
  }
  get previousLoginTime() {
    return this.data.previousLoginTime;
  }
  get playerCode() {
    return this.data.playerCode;
  }
  get playerDepositLimits() {
    return this.data.playerDepositLimits;
  }
  get postCode() {
    return this.data.postCode;
  }
  get profileId() {
    return this.data.profileId;
  }
  get signupDate() {
    return this.data.signupDate;
  }
  get sessionLimit() {
    return this.data.sessionLimit;
  }
  get tcVersion() {
    return this.data.tcVersion;
  }
  get vipLevel() {
    return this.data.vipLevel;
  }
  get vipInfo() {
    return this.data.vipInfo;
  }
  get firstLogin() {
    return this.data.firstLogin;
  }
  get isTemporaryCard() {
    return this.data.isTemporaryCard;
  }
  get passwordResetLogin() {
    return this.data.passwordResetLogin;
  }
  get isRedirecting() {
    return this.data.isRedirecting;
  }
  get showBalance() {
    return this.data.showBalance;
  }
  get showLogoutPopup() {
    return this.data.showLogoutPopup;
  }
  get status(): boolean {
    return this.data.status;
  }
  get quickBetNotification() {
    return this.data.quickBetNotification;
  }
  get contactPreferences() {
    return this.data.contactPreferences;
  }
  get payPalBA() {
    return this.data.payPalBA;
  }
  get payPalDepositFraudNetScriptsLoaded() {
    return this.data.payPalDepositFraudNetScriptsLoaded;
  }
  get isSignUpPending() {
    return this.data.isSignUpPending;
  }

  get logInMessage(): string[] {
    return this.data.logInMessage;
  }

  get winAlertsToggled(): boolean {
    return this.data.winAlertsToggled;
  }

  isRouletteJourney(): boolean {
    return !!this.storage.get(ROULETTE_JOURNEY_KEY);
  }

  isInShopUser(): boolean {
    return this.data.accountBusinessPhase === 'in-shop';
  }

  isMultiChannelUser(): boolean {
    return this.data.accountBusinessPhase === 'multi-channel';
  }

  isRetailUser(): boolean {
    return this.isInShopUser() || this.isMultiChannelUser();
  }

  resetData() {
    const changelessKeys = ['oddsFormat', 'timeline', 'quickBetNotification', 'logoutPending'];

    this.set(_.omit(defaultData, changelessKeys));
  }
}
