import { forkJoin, Observable, of, ReplaySubject } from 'rxjs';
import { catchError, first, map, shareReplay } from 'rxjs/operators';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Inject, Injectable, OnDestroy } from '@angular/core';
import * as _ from 'underscore';
import environment from '@environment/oxygenEnvConfig';
import { PubSubService } from '../communication/pubsub/pubsub.service';
import { DeviceService } from '../device/device.service';
import { CmsToolsService } from './cms.tools';
import {
  IBybMarket,
  IBYBSwitcher,
  ICountryCode,
  IDesktopQuickLink,
  IEdpMarket,
  IFootball3DBanner,
  IFooterMenu,
  IHeaderSubMenu,
  IInitialData,
  ILeague,
  IMaintenancePage,
  IOddsBoostConfig,
  IOffersList,
  IPromotionLiteList,
  IPromotionsList,
  IRetailMenu,
  ISeoPage,
  ISeoPagesPaths,
  ISportCategory,
  IStaticBlock,
  ISystemConfig,
  IUserMenu,
  IWidget,
  IYourCallLeague,
  IYourCallStaticBlock,
  IOtfStaticContent,
  IOtfIosToggle,
  ISportConfig,
  IFormation,
  IQuizSettings
} from './models';
import { INavigationPoint } from '@core/services/cms/models';
import { ISvgItem } from '@core/services/cms/models/svg-item.model';
import { ICompetitionsConfig } from '@core/services/cms/models/system-config';
import {
  CONNECT_PROMOTION_CATEGORY_ID,
  FEATURE_TOGGLE_KEYS
} from '@core/services/cms/cms.constants'; // TODO: rename to retail after changes in cms.
import { ICouponSegment } from '@sb/components/couponsList/coupons-list.model';
import { ICouponMarketSelector } from '@shared/components/marketSelector/market-selector.model';
import { IFeaturedModule } from '@featured/components/featured-tab/featured-module.model';
import { CoreToolsService } from '@core/services/coreTools/core-tools.service';
import { IEdpSurfaceBetDto } from '@core/services/cms/models/surface-bet-dto.model';
import { StorageService } from '@core/services/storage/storage.service';
import { CasinoLinkService } from '@core/services/casinoLink/casino-link.service';
import { NativeBridgeService } from '@root/app/core/services/nativeBridge/native-bridge.service';
import { UserService } from '@core/services/user/user.service';
import { IQuizPopupSettings } from '@core/services/cms/models/quiz-settings.model';
import { ITimelineSettings } from '@core/services/cms/models/timeline-settings.model';
import { IVirtualSportAliasesDto, IVirtualSports } from '@core/services/cms/models/virtual-sports.model';
import { ITimelineDetails } from '@core/services/cms/models/timeline-tutorial.model';
import { IJourneyStaticBlocks } from '@core/services/cms/models/five-a-side-journey.model';

@Injectable({ providedIn: 'root' })
export class CmsService implements OnDestroy {

  initialData: IInitialData;
  lang: string = 'en-us';

  CMS_ENDPOINT: string;
  CMS_ROOT_URI: string;
  SURFACE_BETS_URL: string;

  brand: string = environment.brand;

  systemConfiguration: ISystemConfig;
  navPointsData: INavigationPoint[];
  leaguesData: IYourCallLeague[];
  featureConfigMap: Map<string, Observable<ISystemConfig>> = new Map<string, Observable<ISystemConfig>>();

  private widgets: IWidget[];
  private initialData$: ReplaySubject<IInitialData>;
  private readonly sportNameRegexp: RegExp = /\s|\/|\||\-/g;

  constructor(
    protected pubsub: PubSubService,
    protected cmsTools: CmsToolsService,
    protected device: DeviceService,
    protected http: HttpClient,
    protected coreToolsService: CoreToolsService,
    protected storageService: StorageService,
    protected casinoDecoratorService: CasinoLinkService,
    protected nativeBridgeService: NativeBridgeService,
    protected userService: UserService,
    @Inject('CMS_CONFIG') protected cmsInitConfigPromise: Promise<IInitialData>
  ) {
    this.CMS_ENDPOINT = environment.CMS_ENDPOINT;
    this.SURFACE_BETS_URL = environment.SURFACE_BETS_URL;
  }

  ngOnDestroy(): void {
    if (this.initialData$) {
      this.initialData$.unsubscribe();
    }
  }

  getStaticBlock(serviceName: string, locale?: string): Observable<IStaticBlock> {
    return this.getData(locale ? `static-block/${serviceName}-${locale}` : `static-block/${serviceName}`)
      .pipe(
        map((data: HttpResponse<IStaticBlock>) => data.body)
      );
  }

  getSystemConfig(preventCache: boolean = false): Observable<ISystemConfig> {
    if (this.systemConfiguration && !preventCache) {
      return of(this.systemConfiguration);
    }

    return this.getInitialSystemConfig();
  }

  getFeatureConfig(featureName: string, preventCache: boolean = false, shouldHandleError?: boolean): Observable<ISystemConfig> {
    if (this.featureConfigMap.has(featureName) && !preventCache) {
      return shouldHandleError ? this.featureConfigMap.get(featureName).pipe(catchError(() => of({})))
        : this.featureConfigMap.get(featureName);
    }
    return shouldHandleError ?
      this.getFeatureConfigByName(featureName).pipe(catchError(() => of({}))) :
      this.getFeatureConfigByName(featureName);
  }

  getCompetitions(sportName: string = '', preventCache: boolean = false): Observable<ICompetitionsConfig> {
    const competitionsCategory =
      `Competitions${sportName.charAt(0).toUpperCase() + sportName.slice(1).toLowerCase()}`;

    return this.getSystemConfig(preventCache)
      .pipe(
        map((config: ISystemConfig) => config[competitionsCategory])
      ); }

  getToggleStatus(featureName: FEATURE_TOGGLE_KEYS, preventCache: boolean = false): Observable<boolean> {
    return (this.getSystemConfig(preventCache))
      .pipe(
        map((config: ISystemConfig) => featureName && config && config.FeatureToggle && config.FeatureToggle[featureName])
      );
  }

  getCmsCountries(): Observable<ICountryCode[]> {
    return this.getData(`countries-settings`)
      .pipe(
        map((data: HttpResponse<ICountryCode[]>) => data.body)
      );
  }

  getInitialSystemConfig(): Observable<ISystemConfig> {
    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => data.systemConfiguration)
      );
  }

  triggerSystemConfigUpdate(): void {
    this.getSystemConfig(true)
      .subscribe((config: ISystemConfig) => {
        this.pubsub.publish(this.pubsub.API.SYSTEM_CONFIG_UPDATED, [config]);
      });
  }

  getRibbonModule(): Observable<any> {
    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => {
          const modularContent = this.filterScheduleTabs(data.modularContent);
          return ({ getRibbonModule: modularContent });
        })
      );
  }

  getQuizPopupSetting(): Observable<IQuizPopupSettings> {
    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => data.quizPopupSetting)
      );
  }

  getTimelineSetting(): Observable<ITimelineSettings> {
    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => data.timelineConfig)
      );
  }

  getVirtualSports(): Observable<IVirtualSports[]> {
    return this.getData('virtual-sports')
      .pipe(
        map((data: HttpResponse<IVirtualSports[]>) => data.body)
      );
  }

  getVirtualSportAliases(): Observable<IVirtualSportAliasesDto[]> {
    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => data.vsAliases)
      );
  }

  getContactUs(): Observable<IStaticBlock> {
    return this.getData(`static-block/contact-us${this.lang ? `-${this.lang}` : ''}`)
      .pipe(
        map((data: HttpResponse<IStaticBlock>) => data.body)
      );
  }

  getPrivateMarketsTermsAndConditions(): Observable<IStaticBlock> {
    return this.getData(`static-block/private-markets-terms-and-conditions${this.lang ? `-${this.lang}` : ''}`)
      .pipe(
        map((data: HttpResponse<IStaticBlock>) => data.body)
      );
  }

  getMenuItems(): Observable<ISportCategory[]> {
    const observableMenuItems = this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => {
          this.nativeBridgeService.isRemovingGamingEnabled
            && this.isGamingEnabled()
            && this.casinoDecoratorService.filterGamingLinkForIOSWrapper(data.sportCategories);

          return this.cmsTools.processResult(data.sportCategories);
        })
      );

    return observableMenuItems;
  }

  getSportCategoryById(categoryId: number | string): Observable<ISportCategory> {
    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => {
          return this.cmsTools.processResult(data.sportCategories).find((sportCategory: ISportCategory) => {
            const id = sportCategory.categoryId;
            return id ? sportCategory.categoryId.toString() === categoryId.toString() : false;
          });
        })
      );
  }

  getSportCategoryByName(categoryName: string): Observable<ISportCategory> {
    const sportName = categoryName === 'greyhound' ? 'greyhoundracing' :
      categoryName.toLowerCase().replace(this.sportNameRegexp, '');
    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => {
          return this.cmsTools.processResult(data.sportCategories).find((sportCategory: ISportCategory) => {
            const sportCategoryName = sportCategory.sportName
              && sportCategory.sportName.split('/').pop().toLowerCase().replace(this.sportNameRegexp, '');
            return sportCategoryName === sportName;
          });
        })
      );
  }

  getSportCategoriesByName(categoryNames: string[]): Observable<ISportCategory[]> {
    const sportNames: string[] = categoryNames.map(categoryName =>
      categoryName === 'greyhound' ? 'greyhoundracing' : categoryName.toLowerCase().replace(this.sportNameRegexp, ''));

    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => {
          return this.cmsTools.processResult(data.sportCategories).filter((sportCategory: ISportCategory) => {
            const sportCategoryName = sportCategory.sportName
              && sportCategory.sportName.split('/').pop().toLowerCase().replace(this.sportNameRegexp, '');
            return sportNames.includes(sportCategoryName);
          });
        })
      );
  }

  getSports(): Observable<ISportCategory[]> {
    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => this.cmsTools.processResult(data.sports))
      );
  }

  getOddsBoost(): Observable<IOddsBoostConfig> {
    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => {
          return data.oddsBoost || {} as IOddsBoostConfig;
        })
      );
  }

  getCouponSegment(): Observable<ICouponSegment[]> {
    return this.getData('coupon-segments')
      .pipe(
        map((data: HttpResponse<ICouponSegment[]>) => data.body)
      );
  }

  getCouponMarketSelector(): Observable<ICouponMarketSelector[]> {
    return this.getData('coupon-market-selector')
      .pipe(
        map((data: HttpResponse<ICouponMarketSelector[]>) => data.body)
      );
  }

  getQuizPopupSettingDetails(): Observable<IQuizSettings> {
    return this.getData(`quiz-popup-setting-details`)
      .pipe(
        map((data: HttpResponse<IQuizSettings>) => data.body)
      );
  }

  getTimelineTutorialDetails(): Observable<ITimelineDetails> {
    return this.getData(`timeline-splash-config`)
      .pipe(
        map((data: HttpResponse<ITimelineDetails>) => data.body)
      );
  }

  /**
   * Extract svg icons from cms initial data
   */
  extractInitialIcons(): Observable<string> {
    return this.getCmsInitData().pipe(
      map((data: IInitialData) => data.svgSpriteContent)
    );
  }

  getItemSvg(name: string): Observable<ISvgItem> {
    return this.getMenuItems()
      .pipe(
        map((menuItems: ISportCategory[]) => {
          const menuItem = _.find(menuItems, { imageTitle: name });
          return menuItem ? _.pick(menuItem, 'svg', 'svgId') : { svg: null, svgId: null };
        })
      );
  }

  getAllPromotions(): Observable<IPromotionsList> {
    return this.getPromotions();
  }

  getRetailPromotions(): Observable<IPromotionsList> {
    return this.getPromotions(CONNECT_PROMOTION_CATEGORY_ID, true); // TODO: rename to retail after changes in cms.
  }

  getGroupedPromotions(): Observable<IPromotionsList> {
    return this.getData('grouped-promotions')
      .pipe(
        map((res: HttpResponse<IPromotionsList>) => {
          res.body.promotionsBySection.forEach((group) => {
            this.cmsTools.processResult(group.promotions);
          });

          return res.body;
        })
      );
  }

  getSignpostingPromotionsLight(): Observable<IPromotionLiteList> {
    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => {
          return {
            promotions: this.cmsTools.processResult(data.initSignposting)
          };
        })
      );
  }

  getDesktopQuickLinks(): Observable<IDesktopQuickLink[]> {
    return this.getData(`desktop-quick-links`)
      .pipe(
        map((data: HttpResponse<IDesktopQuickLink[]>) => {
          return this.cmsTools.processResult(data.body);
        })
      );
  }

  getFooterMenu(): Observable<IFooterMenu[]> {
    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => {
          this.nativeBridgeService.isRemovingGamingEnabled
            && this.isGamingEnabled() && this.casinoDecoratorService.filterGamingLinkForIOSWrapper(data.footerMenu);

          return this.cmsTools.processResult(data.footerMenu);
        })
      );
  }

  getHeaderSubMenu(): Observable<IHeaderSubMenu[]> {
    return this.getData(`header-submenu`)
      .pipe(
        map((data: HttpResponse<IHeaderSubMenu[]>) => {
          return this.cmsTools.processResult(data.body);
        })
      );
  }

  getRetailMenu(): Observable<IRetailMenu[]> {
    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => {
          return this.cmsTools.processResult(data.connectMenu); // TODO: rename to retail after changes in cms.
        })
      );
  }

  serviceGetCmsUserMenu(): Observable<IUserMenu[]> {
    return this.getData(`user-menu`)
      .pipe(
        map((data: HttpResponse<IUserMenu[]>) => {
          return this.cmsTools.processResult(data.body);
        })
      );
  }


  /**
   * Set Device Type
   * Strange Data tructure it is an array of Offers lists. LIke group of lists.
   * @param {String} deviceType
   * @returns {IOffersList[]}
   */
  getOffers(deviceType: string): Observable<IOffersList[]> {
    return this.getV2Data(`offers/${deviceType}`)
      .pipe(
        map((data: HttpResponse<IOffersList[]>) => {
          const offersData = data.body;
          offersData.forEach((item: IOffersList) => this.cmsTools.processResult(item.offers));
          return offersData;
        })
      );
  }

  /**
   * if no widgets at the moment -
   *  get data from CMS
   * else - return widgets
   */
  getWidgets(): Observable<IWidget[]> {
    if (!this.widgets) {
      return this.getData(`widgets`)
        .pipe(
          map((data: HttpResponse<IWidget[]>) => {
            this.widgets = data.body;

            return this.widgets;
          })
        );
    }

    return of(this.coreToolsService.deepClone(this.widgets));
  }

  /**
   * get saved widgets
   * get saved system config
   * filter active widgets
   */
  getActiveWidgets(): Observable<IWidget[]> {
    return forkJoin(this.getWidgets(), this.getSystemConfig())
      .pipe(
        map(([widgets, sysConfig]: [IWidget[], ISystemConfig]) => {
          const activeWidgets = widgets.filter((widgetData) => {
            if (widgetData && widgetData.directiveName === 'favourites') {
              return this.checkFavouritesWidget(sysConfig);
            }
            return true;
          });

          return activeWidgets;
        })
      );
  }

  /**
   * check favourites widget
   * favourites widget can be activated for mobile, table, desktop
   */
  checkFavouritesWidget(sysConfig: ISystemConfig): boolean {
    if (this.device.strictViewType === 'mobile' && sysConfig.Favourites && sysConfig.Favourites.displayOnMobile) {
      return true;
    }
    if (this.device.strictViewType === 'tablet' && sysConfig.Favourites && sysConfig.Favourites.displayOnTablet) {
      return true;
    }
    return !!(this.device.strictViewType === 'desktop' && sysConfig.Favourites && sysConfig.Favourites.displayOnDesktop);
  }

  getSeoPage(id: string): Observable<ISeoPage> {
    return this.getData(`seo-page/${id}`)
      .pipe(
        map((data: HttpResponse<ISeoPage>) => data.body)
      );
  }

  getSportConfig(categoryId: number): Observable<ISportConfig[]> {
    return this.getData(`sport-config/${categoryId}`)
      .pipe(
        map((data: HttpResponse<ISportConfig>) => [data.body])
      );
  }

  getSportConfigs(categoryIds: number[]): Observable<ISportConfig[]> {
    return this.getData(`sport-config?categoryIds=${categoryIds.join(',')}`)
      .pipe(
        map((data: HttpResponse<ISportConfig[]>) => data.body)
      );
  }

  getSeoPagesPaths(): Observable<ISeoPagesPaths> {
    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => data.seoPages)
      );
  }

  getFootball3DBanners(): Observable<IFootball3DBanner[]> {
    return this.getData(`3d-football-banners`)
      .pipe(
        map((data: HttpResponse<IFootball3DBanner[]>) => data.body)
      );
  }

  getLeagues(): Observable<ILeague[]> {
    return this.getData(`leagues`)
      .pipe(
        map((data: HttpResponse<ILeague[]>) => data.body)
      );
  }

  // TODO: check returned data with two cases.
  // possibly unnecessary request if we have navPointsData
  getNavigationPoints(): Observable<INavigationPoint[]> {
    if (this.navPointsData) {
      return this.getData(`navigation-points`)
        .pipe(
          map((data: HttpResponse<INavigationPoint[]>) => data.body)
        );
    }

    return this.getCmsInitData()
      .pipe(
        map((data: IInitialData) => {
          this.navPointsData = data.navigationPoints;

          return this.navPointsData;
        })
      );
  }

  getCmsYourCallLeaguesConfig(): Observable<IYourCallLeague[]> {
    const request = this.leaguesData ? of(this.leaguesData) :
      this.getData(`yc-leagues`)
        .pipe(
          map((data: HttpResponse<IYourCallLeague[]>) => {
            this.leaguesData = data.body;
            return data.body;
          })
        );

    return request;
  }

  getYourCallStaticBlock(): Observable<IYourCallStaticBlock[]> {
    return this.getData(`yc-static-block`)
      .pipe(
        map((data: HttpResponse<IYourCallStaticBlock[]>) => data.body)
      );
  }

  getEDPMarkets(): Observable<IEdpMarket[]> {
    return this.getData(`edp-markets`)
      .pipe(
        map((data: HttpResponse<IEdpMarket[]>) => data.body)
      );
  }

  getEDPSurfaceBets(eventId: number): Observable<IEdpSurfaceBetDto[]> {
    return this.getSurfaceBetsData(`edp-surface-bets/${eventId}`)
      .pipe(
        map((data: HttpResponse<IEdpSurfaceBetDto[]>) => data.body)
      );
  }

  getTeamsColors(teamNames: Array<string>, sportId: string | number): Observable<any[]> {
    return this.getData('asset-management', { teamNames: teamNames.join(',').toUpperCase(), sportId })
      .pipe(
        map((data: HttpResponse<any[]>) => data.body)
      );
  }

  getYourCallBybMarkets(): Observable<IBybMarket[]> {
    return this.getData(`byb-markets`)
      .pipe(
        map((data: HttpResponse<IBybMarket[]>) => data.body)
      );
  }

  getBYBSwitchers(): Observable<IBYBSwitcher[]> {
    return this.getData(`byb-switchers`)
      .pipe(
        map((data: HttpResponse<IBYBSwitcher[]>) => data.body)
      );
  }

  /**
   * Loads 1-2-Free static content from CMS
   * @returns Promise
   */
  getOTFStaticContent(): Observable<IOtfStaticContent[]> {
    return this.getData('one-two-free/static-texts')
      .pipe(
        map((data: HttpResponse<IOtfStaticContent[]>) => data.body)
      );
  }

  /**
   * Loads formations for Five A Side
   * @returns Observable
   */
  getFiveASideFormations(): Observable<IFormation[]> {
    return this.getData(`five-a-side-formations`)
      .pipe(
        map((data: HttpResponse<IFormation[]>) => data.body)
      );
  }

  /**
   * Loads 1-2-Free get IosToggle from CMS
   * @returns Observable
   */
  getOTFIosToggle(): Observable<IOtfIosToggle> {
    return this.getData('one-two-free/otf-ios-app-toggle')
      .pipe(
        map((data: HttpResponse<IOtfIosToggle>) => data.body)
      );
  }

  getMaintenancePage(): Observable<IMaintenancePage[]> {
    return this.getData(`maintenance-page/${this.device.requestPlatform}`)
      .pipe(
        map((data: HttpResponse<IMaintenancePage[]>) => data.body || [])
      );
  }

  /**
   * This function is used to valdate to sow/hide market switcher dropdown based on global flag and sport level flag
   * @param sportName
   */
  getMarketSwitcherFlagValue(sportName: string): Observable<boolean> {
    const marketSwitcherFlag = (this.getFeatureConfig('MarketSwitcher', false, true) as Observable<ISystemConfig>)
      .pipe(
        map((config: ISystemConfig) => {
          if (config && config.AllSports && config[sportName]) {
            return true;
          } else {
            return false;
          }
        })
      );
    return marketSwitcherFlag;
  }

  /**
   * This functions go throw params and replace htmlMarkup from CMS Static Blocks. Template for parameter - [['param1']].
   * @param content
   * @param params
   * @returns {String} htmlMarkup from CMS
   */
  parseContent(content: string, params: string[] | string): string {
    let modifiedContent = content;
    const paramsArray = typeof params === 'string' ? JSON.parse(params) : params;
    paramsArray.forEach((param: string, index: number) => {
      modifiedContent = modifiedContent
        .replace(`[['currency']]`, this.userService.currencySymbol)
        .replace(`[['param${(index + 1)}']]`, param);
    });
    return modifiedContent;
  }

  /**
   * Check if BogToggle is true/false on cms
   * @returns {Observable<boolean>}
   */
  isBogFromCms(): Observable<boolean> {
    return this.getSystemConfig().pipe(
      map((config: ISystemConfig) => config.BogToggle && config.BogToggle.bogToggle));
  }

  /**
   * Gets static blocks for 5-a-side journey, which are configured in CMS -> BYB -> BYB STATIC BLOCKS
   * and are active for 5-a-side
   * @returns {Observable<IJourneyStaticBlocks[]>}
   */
  getFiveASideStaticBlocks(): Observable<IJourneyStaticBlocks[]> {
    return this.getData(`5a-side-static-block`)
      .pipe(
        map((data: HttpResponse<IJourneyStaticBlocks[]>) => data.body)
      );
  }

  protected getCmsInitData(): Observable<IInitialData> {
    if (this.initialData$) {
      return this.initialData$.asObservable();
    }

    this.initialData$ = new ReplaySubject<IInitialData>(1);
    if (this.cmsInitConfigPromise) {
      this.cmsInitConfigPromise.then((data: IInitialData) => {
        this.releaseSubject(data);
      });
    } else {
      const device = this.device.requestPlatform;

      this.getData(`initial-data/${device}`)
        .pipe(
          first(),
          map((response: HttpResponse<IInitialData>) => response.body)
        )
        .subscribe((data: IInitialData) => {
          this.releaseSubject(data);
        });
    }

    return this.initialData$.asObservable();
  }

  protected releaseSubject(data: IInitialData): void {
    this.initialData = data;
    this.systemConfiguration = data.systemConfiguration;

    this.initialData$.next(this.initialData);
    this.initialData$.complete();
  }

  protected getData<T>(url: string, params: any = {}): Observable<HttpResponse<T>> {
    return this.http.get<T>(`${this.CMS_ENDPOINT}/${this.brand}/${url}`, {
      observe: 'response',
      params: params
    });
  }

  protected getSurfaceBetsData<T>(url, params: any = {}): Observable<HttpResponse<T>> {
    return this.http.get<T>(`${this.SURFACE_BETS_URL}/${this.brand}/${url}`, {
      observe: 'response',
      params: params
    });
  }

  private getV2Data<T>(url, params: any = {}): Observable<HttpResponse<T>> {
    return this.http.get<T>(`${this.CMS_ENDPOINT}/v2/${this.brand}/${url}`, {
      observe: 'response',
      params: params
    });
  }

  private getFeatureConfigByName(feature: string): Observable<ISystemConfig> {
    const reqData = this.getData(`system-configurations/${feature}`)
      .pipe(
        map((data: HttpResponse<ISystemConfig>) => data.body),
        shareReplay(1)
      );
    this.featureConfigMap.set(feature, reqData);
    return reqData;
  }

  private isGamingEnabled(): boolean {
    return !!(this.systemConfiguration && this.systemConfiguration.GamingEnabled && !this.systemConfiguration.GamingEnabled.enabled);
  }

  private getPromotions(categoryId?: string, withV2?: boolean): Observable<IPromotionsList> {
    const promoUrl = categoryId ? `promotions/${categoryId}` : 'promotions';

    return (!withV2 ? this.getData(promoUrl) : this.getV2Data(`promotions/${categoryId}`))
      .pipe(
        map((data: HttpResponse<IPromotionsList>) => {
          const promotionsListData = data.body;
          promotionsListData.promotions = this.cmsTools.processResult(promotionsListData.promotions);
          return promotionsListData;
        })
      );
  }

  /**
   * valida scheduling for EventHub tabs and remove which are in not time range.
   * @param ribbonData
   */
  private filterScheduleTabs(ribbonData: IFeaturedModule[]): IFeaturedModule[] {
    return _.filter(ribbonData, (ribbon: IFeaturedModule) => {
      if (ribbon.directiveName && ribbon.directiveName.toLowerCase() === 'eventhub') {
        return this.isActiveRange(ribbon.displayFrom, ribbon.displayTo);
      }

      return true;
    });
  }

  /**
   * Schedule time should be in present time range
   * @param displayFrom
   * @param displayTo
   */
  private isActiveRange(displayFrom: string, displayTo: string): boolean {
    const dateNow = (new Date()).getTime(),
      startTime = (new Date(displayFrom)).getTime(),
      endTime = (new Date(displayTo)).getTime();

    return startTime < dateNow && endTime > dateNow;
  }
}
