export interface ISportTab {
  name: string;
  displayName: string;
}
