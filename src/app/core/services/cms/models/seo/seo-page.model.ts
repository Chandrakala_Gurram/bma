
import { IBase } from '../base.model';

export interface ISeoPage extends IBase {
  changefreq: string;
  description: string;
  disabled: boolean;
  lang: string;
  staticBlock: string;
  title: string;
  url: string;
  urlBrand: string;
  priority: string;
}
