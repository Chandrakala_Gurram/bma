export interface ISeoPagesPaths {
  [key: string]: string;
}
