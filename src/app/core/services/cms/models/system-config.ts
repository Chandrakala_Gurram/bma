export interface ISystemConfig {
  [name: string]: any;

  Betslip?: any;
  Connect?: IRetailConfig; // TODO: rename to retail after changes in cms.
  Banners?: IBannersModel;
  Layouts?: ILayoutsModel;
  Header?: IHeaderModel;
  LCCP?: ILCCPModule;
  Generals?: IGeneralModel;
  ExternalUrls?: IExternalUrls;
  RouletteJourney?: IRouletteModel;
  GreyhoundNextRacesToggle?: IGreyhoundNextRacesConfig;
  RacingDataHub?: IRacingDataHub;
  NextRaces?: INextRaces;
  GreyhoundNextRaces?: IGreyhoundNextRaces;
  NextRacesToggle?: INextRacesToggle;
  Promotions?: IPromotionsConfig;
  VirtualScrollConfig?: IVirtualScrollConfig;
  Overask?: IOverAskSysConfigModel;
  SportCompetitionsTab?: ISportCompetitionsTab;
}

export interface IRetailConfig {
  footballFilter: boolean;
  login: boolean;
  menu: boolean;
  overlay: boolean;
  promotions: boolean;
  shopBetHistory: boolean;
  shopBetTracker: boolean;
  shopLocator: boolean;
  upgrade: boolean;
}

export interface ICompetitionsConfig {
  'A-ZClassIDs': string;
  InitialClassIDs: string;
}

interface IHeaderModel {
  gameButtonLink: string;
  rr: string;
  showInApp: string;
  tt: string;
}

interface ILayoutsModel {
  ShowLeftMenu: string;
  ShowRightMenu: string;
  ShowTopMenu: string;
}

interface IBannersModel {
  newName: boolean;
  transitionDelay: string;
}

interface ILCCPModule {
  gameFrequency: string;
  gameFrequencyValues: string;
  hourlyAlerts: string;
}

interface IGeneralModel {
  betSlipAnimation: string;
  title: string;
}

export interface IExternalUrls {
  [key: string]: string;
}

interface IRouletteModel {
  isRouletteEnabled: boolean;
  successNavigationUrl: string;
  timeToRedirect?: number;
}

export interface IInplayModule {
  enabled: boolean;
  virtualScroll: boolean;
}

export interface IGreyhoundNextRacesConfig {
  nextRacesTabEnabled: boolean;
  nextRacesComponentEnabled: boolean;
}

export interface INextRacesToggle {
  nextRacesTabEnabled: boolean;
  nextRacesComponentEnabled: boolean;
}

export interface IRacingDataHub {
  isEnabledForGreyhound?: boolean;
  isEnabledForHorseRacing?: boolean;
}

export interface INextRaces {
  isInUK: string;
  isInternational: string;
  isIrish: string;
  numberOfEvents: string;
  numberOfSelections: string;
  showPricedOnly: string;
  title: string;
  typeDateRange: {
    from: string;
    to: string;
  };
  typeID: string;
}

export interface IGreyhoundNextRaces {
  numberOfEvents: string;
  numberOfSelections: string;
}

export interface IPromotionsConfig {
  expandedAmount: string;
  groupBySections: boolean;
}

export interface IVirtualScrollConfig {
  enabled: boolean;
  iOSInnerScrollEnabled: boolean;
  androidInnerScrollEnabled: boolean;
}

export interface IOverAskSysConfigModel {
  title: string;
  bottomMessage: string;
  topMessage: string;
  traderOfferNotificationMessage: string;
  traderOfferExpiresMessage: string;
}

export type ICombinedRacingConfig = Pick<ISystemConfig, 'NextRaces' | 'RacingDataHub' | 'GreyhoundNextRaces'>;

export interface ISportCompetitionsTab {
  eventsLimit: number;
}

export interface IVirtualSportsConfig {
  [key: string]: boolean;
}
