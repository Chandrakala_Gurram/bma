export interface IVirtualChild {
  id: string;
  title: string;
  classId: string;
  streamUrl: string;
  numberOfEvents: number;
  showRunnerNumber: boolean;
  showRunnerImages: boolean;
  eventAliases?: { [event: string]: string };
}

export interface IVirtualSports {
  id: string;
  title: string;
  svg: string;
  svgId: string;
  ctaButtonUrl: string;
  ctaButtonText: string;
  tracks?: IVirtualChild[];
}

export interface IVirtualSportAliasesDto {
  classId: string;
  parent: string;
  child: string;
  events?: { [event: string]: string };
}
