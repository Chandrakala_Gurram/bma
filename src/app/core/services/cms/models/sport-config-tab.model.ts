import { ISportConfigSubTab } from './sport-config-sub-tab.model';

export interface ISportConfigTab {
  label: string;
  enabled?: boolean;
  index?: number;
  id?: string;
  url?: string;
  name?: string;
  hidden?: boolean;
  sortOrder?: number;
  subTabs?: ISportConfigSubTab[];
  displayInConnect?: boolean;
}
