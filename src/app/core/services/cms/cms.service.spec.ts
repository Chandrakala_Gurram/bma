import { Observable, of, ReplaySubject, throwError } from 'rxjs';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';
import { CmsService } from './cms.service';
import environment from '@environment/oxygenEnvConfig';
import { ISvgItem, ISystemConfig, IWidget } from '@core/services/cms/models';
import { fakeAsync, tick } from '@angular/core/testing';
import { CONNECT_PROMOTION_CATEGORY_ID } from '@core/services/cms/cms.constants';

describe('@CmsService', () => {
  const initialDataMock = {
    systemConfiguration: {systemConfiguration: {}},
    modularContent: {modularContent: {}},
    navigationPoints: [{a: 1, b: 2}],
    sportCategories: [{ categoryId: 1, sportName: 'category1' },
    { categoryId: 2, sportName: 'category2' },
    { categoryId: 3, sportName: 'greyhoundracing' },
    { imageTitle: 'football', svgId: '#1'}],
    svgSpriteContent: '<svg>'
  } as any;

  let service: CmsService,
    pubSubService,
    cmsToolsService,
    deviceService,
    httpClient,
    coreToolsService,
    storageService,
    casinoLinkService,
    nativeBridgeService,
    userService,
    cmsInitConfigPromise;

  beforeEach(() => {
    pubSubService = {
      publish: jasmine.createSpy('publish'),
      API: pubSubApi
    };

    cmsToolsService = {
      processResult: jasmine.createSpy('processResult').and.returnValue([])
    };

    deviceService = {
      strictViewType: 'mobile',
      requestPlatform: 'mobile'
    };

    httpClient = {
      get: jasmine.createSpy('get').and.returnValue(of(
        {
          body: []
        }))
    };

    coreToolsService = {
      deepClone: jasmine.createSpy('deepClone')
    };

    storageService = {
      get: jasmine.createSpy('get')
    };

    casinoLinkService = {
      filterGamingLinkForIOSWrapper: jasmine.createSpy('filterGamingLinkForIOSWrapper')
    };

    nativeBridgeService = {
      isRemovingGamingEnabled: false
    };

    userService = {
      currencySymbol: '$'
    };

    cmsInitConfigPromise = undefined;

    service = new CmsService(
      pubSubService,
      cmsToolsService,
      deviceService,
      httpClient,
      coreToolsService,
      storageService,
      casinoLinkService,
      nativeBridgeService,
      userService,
      cmsInitConfigPromise
    );

    service['initialData$'] = new ReplaySubject<any>(1);
    service['initialData$'].next(initialDataMock as any);
    service['initialCmsDataPromise'] = undefined;
  });

  afterEach(() => {
    service = null;
  });

  it('should create instance', () => {
    expect(service).toBeDefined();
    expect(service instanceof CmsService).toBeTruthy();
  });

  it('should call getStaticBlock()', () => {
    service.getStaticBlock('contact-us');

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/static-block/contact-us`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getSystemConfig() once', () => {
    service['getInitialSystemConfig'] = jasmine.createSpy('getInitialSystemConfig').and
      .returnValue(of(initialDataMock.systemConfiguration));
    (<Observable<ISystemConfig>>service.getSystemConfig(true)).subscribe();

    service.systemConfiguration = initialDataMock.systemConfiguration as any;

    (<Observable<ISystemConfig>>service.getSystemConfig()).subscribe();

    expect(service['getInitialSystemConfig']).toHaveBeenCalledTimes(1);
  });

  it('should return systemConfiguration', () => {
    service['systemConfiguration'] = 'test' as any;

    service.getSystemConfig().subscribe(data => {
      expect(data as any).toEqual('test');
    });
  });

  describe('@getFeatureConfig()', () => {
    const preventCache = false;

    beforeEach(() => {
      service['getFeatureConfigByName'] = jasmine.createSpy('getFeatureConfigByName');
      service.featureConfigMap = new Map as any;
    });

    it('should call getFeatureConfigByName() ', () => {
      service.getFeatureConfig('football');
      expect(service['getFeatureConfigByName']).toHaveBeenCalledWith('football');
    });

    it('should call getFeatureConfigByName() ', () => {
      service.getFeatureConfig('football', preventCache);
      expect(service['getFeatureConfigByName']).toHaveBeenCalledWith('football');
    });

    it('should not call getFeatureConfigByName() ', () => {
      const successHandler = jasmine.createSpy('successHandler'),
        errorHandler = jasmine.createSpy('errorHandler');
      const featureConfigMock = {visible: true, showHeader: false};
      service.featureConfigMap.set('football', of(featureConfigMock));
      service.getFeatureConfig('football', preventCache).subscribe(successHandler, errorHandler);
      expect(successHandler).toHaveBeenCalledWith(featureConfigMock);
      expect(errorHandler).not.toHaveBeenCalled();
      expect(service['getFeatureConfigByName']).not.toHaveBeenCalled();
    });

    it('should call getFeatureConfigByName() method when preventCache was passed', () => {
      const successHandler = jasmine.createSpy('successHandler'),
        errorHandler = jasmine.createSpy('errorHandler');
      const featureConfigMock = {visible: true, showHeader: false};
      service.featureConfigMap.set('football', of(featureConfigMock));
      service['getFeatureConfigByName'] = jasmine.createSpy('getFeatureConfigByName').and.returnValue(of(featureConfigMock));

      service.getFeatureConfig('football', true).subscribe(successHandler, errorHandler);

      expect(successHandler).toHaveBeenCalledWith(featureConfigMock);
      expect(errorHandler).not.toHaveBeenCalled();
      expect(service['getFeatureConfigByName']).toHaveBeenCalled();
    });

    it('should not call getFeatureConfigByName() and not handle throwing error', () => {
      const successHandler = jasmine.createSpy('successHandler'),
        errorHandler = jasmine.createSpy('errorHandler');
      service.featureConfigMap.set('football', throwError('error'));

      service.getFeatureConfig('football', preventCache).subscribe(successHandler, errorHandler);

      expect(successHandler).not.toHaveBeenCalled();
      expect(errorHandler).toHaveBeenCalledWith('error');
      expect(service['getFeatureConfigByName']).not.toHaveBeenCalled();
    });

    it('should call getFeatureConfigByName() with error handling', () => {
      service['getFeatureConfigByName'] = jasmine.createSpy('getFeatureConfigByName').and.returnValue(throwError('error'));
      service.getFeatureConfig('football', preventCache, true).subscribe((data) => {
        expect(data).toEqual({});
      });

      expect(service['getFeatureConfigByName']).toHaveBeenCalledWith('football');
    });

    it('should not call getFeatureConfigByName() with error handling', () => {
      const successHandler = jasmine.createSpy('successHandler'),
        errorHandler = jasmine.createSpy('errorHandler');
      service.featureConfigMap.set('football', throwError('error'));
      service.getFeatureConfig('football', preventCache, true).subscribe(successHandler, errorHandler);

      expect(service['getFeatureConfigByName']).not.toHaveBeenCalled();
      expect(successHandler).toHaveBeenCalledWith({});
      expect(errorHandler).not.toHaveBeenCalled();
    });
  });

  describe('@getCompetitions()', () => {
    const competitions = { 'A-ZClassIDs': '', InitialClassIDs: '123' },
      competitionsFootball = { 'A-ZClassIDs': '', InitialClassIDs: '456' },
      competitionsBasketball = { 'A-ZClassIDs': '', InitialClassIDs: '789' };

    beforeEach(() => {
      service['systemConfiguration'] = {
        Competitions: competitions,
        CompetitionsFootball: competitionsFootball,
        CompetitionsBasketball: competitionsBasketball
      };
    });

    it('should return proper config for football sport', fakeAsync(() => {
      service.getCompetitions('Football')
        .subscribe(data => {
          expect(data).toEqual(competitionsFootball);
        });

      tick();
    }));

    it('should return proper config for CAPITALIZED basketball sport', fakeAsync(() => {
      service.getCompetitions('BASKETBALL')
        .subscribe(data => {
          expect(data).toEqual(competitionsBasketball);
        });

      tick();
    }));

    it('should return proper config for basketball sport', fakeAsync(() => {
      service.getCompetitions('basketball')
        .subscribe(data => {
          expect(data).toEqual(competitionsBasketball);
        });

      tick();
    }));

    it('should return fallback config', fakeAsync(() => {
      service.getCompetitions()
        .subscribe(data => {
          expect(data).toEqual(competitions);
        });

      tick();
    }));
  });

  it('should call getData()', fakeAsync(() => {
    const featureConfigMock = {visible: true, showHeader: false};
    service['getData'] = jasmine.createSpy('getData').and.returnValue(of(featureConfigMock));
    service['getFeatureConfigByName']('CompetitionsBasketball').subscribe();
    tick();

    expect(service['getData']).toHaveBeenCalledWith(`system-configurations/CompetitionsBasketball`);
    })
  );


  describe('@getToggleStatus()', () => {
    it('should return true if feature is enabled in CMS', fakeAsync(() => {
      service['systemConfiguration'] = { FeatureToggle: { PromoSignposting: true } };
      service.getToggleStatus('PromoSignposting')
        .subscribe(toggleStatus => {
          expect(toggleStatus).toBeTruthy();
        });

      tick();
    }));

    it('should return false if feature is disabled in CMS', fakeAsync(() => {
      service['systemConfiguration'] = { FeatureToggle: { PromoSignposting: false } };
      service.getToggleStatus('PromoSignposting')
        .subscribe(toggleStatus => {
          expect(toggleStatus).toBeFalsy();
        });

      tick();
    }));

    it('should return false if feature is not configured in CMS', fakeAsync(() => {
      service['systemConfiguration'] = { FeatureToggle: {} };
      service.getToggleStatus('PromoSignposting')
        .subscribe((toggleStatus) => {
          expect(toggleStatus).toBeFalsy();
        });

      tick();
    }));
  });

  it('should call getCmsCountries()', () => {
    service.getCmsCountries().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/countries-settings`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getInitialSystemConfig()', () => {
    service['getCmsInitData'] = jasmine.createSpy('getCmsInitData').and.returnValue(of(initialDataMock));
    service.getInitialSystemConfig().subscribe();

    expect(service['getCmsInitData']).toHaveBeenCalled();
  });

  it('should call triggerSystemConfigUpdate()', () => {
    service['getSystemConfig'] = jasmine.createSpy('getSystemConfig').and.returnValue(of(initialDataMock.systemConfiguration));
    service.triggerSystemConfigUpdate();

    expect(service['getSystemConfig']).toHaveBeenCalledWith(true);
    expect(pubSubService.publish).toHaveBeenCalledWith(pubSubApi.SYSTEM_CONFIG_UPDATED, [initialDataMock.systemConfiguration]);
  });

  it('should call getRibbonModule()', () => {
    service['getCmsInitData'] = jasmine.createSpy('getCmsInitData').and.returnValue(of(initialDataMock));
    service['filterScheduleTabs'] = jasmine.createSpy('getCmsInitData').and.returnValue(initialDataMock.modularContent);
    service.getRibbonModule().subscribe((data) => {
      expect(data).toEqual({ getRibbonModule: initialDataMock.modularContent});
    });

    expect(service['getCmsInitData']).toHaveBeenCalled();
    expect(service['filterScheduleTabs']).toHaveBeenCalledWith(initialDataMock.modularContent);
  });

  it('should call getContactUs()', () => {
    service.getContactUs().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/static-block/contact-us-en-us`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getQuizPopupSetting()', () => {
    service['initialData$'] = undefined;
    service.getQuizPopupSetting().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/initial-data/mobile`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getTimelineSetting()', () => {
    service['initialData$'] = undefined;
    service.getTimelineSetting().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/initial-data/mobile`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getVirtualSports()', () => {
    service['initialData$'] = undefined;
    service.getVirtualSports().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/virtual-sports`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getVirtualSportAliases()', () => {
    service['initialData$'] = undefined;
    service.getVirtualSportAliases().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/initial-data/mobile`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getPrivateMarketsTermsAndConditions()', () => {
    service.getPrivateMarketsTermsAndConditions().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/static-block/private-markets-terms-and-conditions-en-us`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getMenuItems() once', () => {
    service['getCmsInitData'] = jasmine.createSpy('getCmsInitData').and.returnValue(of(initialDataMock));
    nativeBridgeService.isRemovingGamingEnabled = true;

    service['isGamingEnabled'] = jasmine.createSpy('isGamingEnabled').and.returnValue(true);
    service.getMenuItems().subscribe();

    expect(casinoLinkService.filterGamingLinkForIOSWrapper).toHaveBeenCalled();
  });

  it('should call getSports()', () => {
    service['getCmsInitData'] = jasmine.createSpy('getCmsInitData').and.returnValue(of(initialDataMock));

    service.getSports().subscribe();

    expect(service['getCmsInitData']).toHaveBeenCalled();
    expect(cmsToolsService.processResult).toHaveBeenCalled();
  });

  it('should call getOddsBoost()', () => {
    service['getCmsInitData'] = jasmine.createSpy('getCmsInitData').and.returnValue(of(initialDataMock));
    service.getOddsBoost().subscribe((data) => {
      expect(data).toEqual({} as any);
    });

    expect(service['getCmsInitData']).toHaveBeenCalled();
  });

  it('should call getCouponSegment()', () => {
    service.getCouponSegment().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/coupon-segments`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getCouponMarketSelector()', () => {
    service.getCouponMarketSelector().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/coupon-market-selector`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getQuizPopupSettingDetails()', () => {
    service.getQuizPopupSettingDetails().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/quiz-popup-setting-details`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getTimelineTutorialDetails()', () => {
    service.getTimelineTutorialDetails().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/timeline-splash-config`,
      { observe: 'response', params: {} }
    );
  });

  it('should call extractInitialIcons()', () => {
    service['getCmsInitData'] = jasmine.createSpy('getCmsInitData').and.returnValue(of(initialDataMock));
    service.extractInitialIcons().subscribe(res => {
      expect(service['getCmsInitData']).toHaveBeenCalled();
      expect(res).toBe(initialDataMock.svgSpriteContent);
    });
  });

  describe('getItemSvg', () => {
    beforeEach(() => {
      service['getMenuItems'] = jasmine.createSpy('getMenuItems').and.returnValue(of(initialDataMock.sportCategories));
    });

    it('should call to get observable', () => {
      service.getItemSvg('football').subscribe((data: ISvgItem) => {
        expect(data).toEqual({ svgId: '#1' } as any);
      });

      expect(service['getMenuItems']).toHaveBeenCalledWith();
    });
  });

  it('should call getAllPromotions()', () => {
    service.getAllPromotions().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/promotions`,
      { observe: 'response', params: {} }
    );
    expect(cmsToolsService.processResult).toHaveBeenCalled();
  });

  it('should call getRetailPromotions()', () => {
    service.getRetailPromotions().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/v2/${ environment.brand }/promotions/${ CONNECT_PROMOTION_CATEGORY_ID }`,
      { observe: 'response', params: {} }
    );
    expect(cmsToolsService.processResult).toHaveBeenCalled();
  });

  it('should call getGroupedPromotions()', () => {
    httpClient.get = jasmine.createSpy().and.returnValue(of({
      body: {
        promotionsBySection: [{ uriMedium: 'http://promo' }]
      }
    }));

    service.getGroupedPromotions().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/grouped-promotions`,
      { observe: 'response', params: {} }
    );
    expect(cmsToolsService.processResult).toHaveBeenCalled();
  });

  it('should call getSignpostingPromotionsLight()', () => {
    service['getCmsInitData'] = jasmine.createSpy('getCmsInitData').and.returnValue(of(initialDataMock));
    service.getSignpostingPromotionsLight().subscribe();

    expect(cmsToolsService.processResult).toHaveBeenCalled();
    expect(service['getCmsInitData']).toHaveBeenCalled();
  });

  it('should call getDesktopQuickLinks()', () => {
    service.getDesktopQuickLinks().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/desktop-quick-links`,
      { observe: 'response', params: {} }
    );
    expect(cmsToolsService.processResult).toHaveBeenCalled();
  });

  it('should call getFooterMenu()', () => {
    service['getCmsInitData'] = jasmine.createSpy('getCmsInitData').and.returnValue(of(initialDataMock));
    nativeBridgeService.isRemovingGamingEnabled = true;

    service['isGamingEnabled'] = jasmine.createSpy('isGamingEnabled').and.returnValue(true);
    service.getFooterMenu().subscribe();

    expect(service['isGamingEnabled']).toHaveBeenCalled();
    expect(casinoLinkService.filterGamingLinkForIOSWrapper).toHaveBeenCalled();
    expect(cmsToolsService.processResult).toHaveBeenCalled();
    expect(service['getCmsInitData']).toHaveBeenCalled();
  });

  it('should call getHeaderSubMenu()', () => {
    service.getHeaderSubMenu().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/header-submenu`,
      { observe: 'response', params: {} }
    );
    expect(cmsToolsService.processResult).toHaveBeenCalled();
  });

  it('should call getRetailMenu()', () => {
    service['getCmsInitData'] = jasmine.createSpy('getCmsInitData').and.returnValue(of(initialDataMock));
    service.getRetailMenu().subscribe();

    expect(cmsToolsService.processResult).toHaveBeenCalled();
    expect(service['getCmsInitData']).toHaveBeenCalled();
  });

  it('should call serviceGetCmsUserMenu()', () => {
    service.serviceGetCmsUserMenu().subscribe();

    expect(cmsToolsService.processResult).toHaveBeenCalled();
  });

  describe('@getOffers()', () => {
    beforeEach(() => {
      httpClient.get = jasmine.createSpy().and.returnValue(of({
        body: [{
          name: 'offer1',
          offers: []
        }, {
          name: 'offer2',
          offers: []
        }]
      }));
    });

    it('should call getOffers() for mobile', () => {
      service.getOffers('mobile').subscribe();

      expect(httpClient.get).toHaveBeenCalled();
      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.CMS_ENDPOINT }/v2/${ environment.brand }/offers/mobile`,
        { observe: 'response', params: {} }
      );
      expect(cmsToolsService.processResult).toHaveBeenCalled();
    });

    it('should call getOffers() for desktop', () => {
      service.getOffers('desktop').subscribe();

      expect(httpClient.get).toHaveBeenCalled();
      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.CMS_ENDPOINT }/v2/${ environment.brand }/offers/desktop`,
        { observe: 'response', params: {} }
      );
      expect(cmsToolsService.processResult).toHaveBeenCalled();
    });
  });

  it('should call getWidgets()', fakeAsync(() => {
    service['getData'] = jasmine.createSpy().and.returnValue(of({}));

    service['widgets'] = null;
    service.getWidgets().subscribe();
    tick();

    service['widgets'] = [{}] as any[];
    service.getWidgets().subscribe();
    tick();

    expect(service['getData']).toHaveBeenCalledTimes(1);
    expect(coreToolsService.deepClone).toHaveBeenCalledTimes(1);
  }));

  describe('@getActiveWidgets()', () => {
    it('should call getActiveWidgets() and return full list', () => {
      const sysConfig: ISystemConfig = {
        Favourites: {
          displayOnMobile: false,
          displayOnTablet: true,
          displayOnDesktop: true
        }
      };

      const widgets: IWidget[] = [
        { directiveName: 'favourites' },
        { directiveName: 'betslip' },
        { directiveName: 'testW' }
      ] as any;

      service.getWidgets = jasmine.createSpy('getWidgets').and.returnValue(of());
      service.getSystemConfig = jasmine.createSpy('getSystemConfig').and.returnValue(of(sysConfig));
      service.checkFavouritesWidget = jasmine.createSpy('checkFavouritesWidget').and.returnValue(true);

      service.getActiveWidgets().subscribe((filteredWidgets: IWidget[]) => {
        expect(service.getWidgets).toHaveBeenCalledTimes(1);
        expect(service.getSystemConfig).toHaveBeenCalledTimes(1);
        expect(service.checkFavouritesWidget).toHaveBeenCalledTimes(1);
        expect(service.checkFavouritesWidget).toHaveBeenCalledWith(sysConfig);
        expect(filteredWidgets).toEqual(widgets);
      });
    });

    it('should call getActiveWidgets() and return without favourites', () => {
      const widgets: IWidget[] = [
        { directiveName: 'favourites' },
        { directiveName: 'betslip' },
        { directiveName: 'testW' }
      ] as any;

      service.getWidgets = jasmine.createSpy('getWidgets').and.returnValue(of(widgets));
      service.getSystemConfig = jasmine.createSpy('getSystemConfig').and.returnValue(of({}));
      service.checkFavouritesWidget = jasmine.createSpy('checkFavouritesWidget').and.returnValue(false);

      service.getActiveWidgets().subscribe((filteredWidgets: IWidget[]) => {
        expect(filteredWidgets).toEqual([
          { directiveName: 'betslip' },
          { directiveName: 'testW' }
        ] as any);
      });
    });
  });

  it('should call checkFavouritesWidget() and check favourites widget state', () => {
    const sysConfig: ISystemConfig = {
      Favourites: {
        displayOnMobile: true,
        displayOnTablet: true,
        displayOnDesktop: true
      }
    };

    service['device'] = {
      strictViewType: ''
    } as any;

    expect(service.checkFavouritesWidget(sysConfig)).toBeFalsy();

    service['device'].strictViewType = 'mobile';
    expect(service.checkFavouritesWidget(sysConfig)).toBeTruthy();

    sysConfig.Favourites.displayOnMobile = false;
    expect(service.checkFavouritesWidget(sysConfig)).toBeFalsy();

    service['device'].strictViewType = 'tablet';
    expect(service.checkFavouritesWidget(sysConfig)).toBeTruthy();

    sysConfig.Favourites.displayOnTablet = false;
    expect(service.checkFavouritesWidget(sysConfig)).toBeFalsy();

    service['device'].strictViewType = 'desktop';
    expect(service.checkFavouritesWidget(sysConfig)).toBeTruthy();

    sysConfig.Favourites.displayOnDesktop = false;
    expect(service.checkFavouritesWidget(sysConfig)).toBeFalsy();

    sysConfig.Favourites = undefined;
    expect(service.checkFavouritesWidget(sysConfig)).toBeFalsy();
  });

  it('should call getSeoPage()', () => {
    const id = '1';

    service.getSeoPage(id).subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/seo-page/${ id }`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getSportConfig()', () => {
    const id = 1;

    service.getSportConfig(id).subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/sport-config/${ id }`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getSportConfigs()', () => {
    const ids = [1, 2];

    service.getSportConfigs(ids).subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${environment.CMS_ENDPOINT}/${environment.brand}/sport-config?categoryIds=${ids.join(',')}`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getFootball3DBanners()', () => {
    httpClient.get = jasmine.createSpy().and.returnValue(of({
      body: {
        bannersData: [{
          uriMedium: 'http://uriMedium'
        }, {
          uriMedium: 'http://uriMedium'
        }]
      }
    }));

    service.getFootball3DBanners().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/3d-football-banners`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getLeagues()', () => {
    service.getLeagues().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/leagues`,
      { observe: 'response', params: {} }
    );
  });

  describe('getNavigationPoints', () => {
    beforeEach(() => {
      service.navPointsData = undefined;
      service['getData'] = jasmine.createSpy('getData').and.returnValue(of({
        body: { navigationPoints: initialDataMock.navigationPoints }
      }));
      service['getCmsInitData'] = jasmine.createSpy('getData').and.returnValue(of(initialDataMock));
    });

    it('should define navPointsData and call getCmsInitData if navPointsData is not present', () => {
      service.getNavigationPoints().subscribe();

      expect(service.navPointsData).toBe(initialDataMock.navigationPoints as any);
      expect(service['getData']).not.toHaveBeenCalled();
      expect(service['getCmsInitData']).toHaveBeenCalled();
    });

    it('should call getData if navPointsData is present', () => {
      const navPointsData = [{}] as any;
      service.navPointsData = navPointsData;
      service.getNavigationPoints().subscribe();

      expect(service.navPointsData).toBe(navPointsData);
      expect(service['getData']).toHaveBeenCalledWith('navigation-points');
      expect(service['getCmsInitData']).not.toHaveBeenCalled();
    });
  });

  it('should call getCmsYourCallLeaguesConfig() once', () => {
    (<any>service.getCmsYourCallLeaguesConfig()).subscribe();

    expect(httpClient.get).toHaveBeenCalledTimes(1);
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/yc-leagues`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getYourCallStaticBlock()', () => {
    service.getYourCallStaticBlock().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/yc-static-block`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getEDPMarkets()', () => {
    service.getEDPMarkets().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/edp-markets`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getEDPSurfaceBets()', () => {
    const id = 1;

    service.getEDPSurfaceBets(id).subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.SURFACE_BETS_URL }/${ environment.brand }/edp-surface-bets/${ id }`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getTeamsColors()', () => {
    service.getTeamsColors(['team1', 'team2'], '12').subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${environment.CMS_ENDPOINT}/${environment.brand}/asset-management`,
      { observe: 'response', params: {
        teamNames: 'TEAM1,TEAM2', sportId: '12'
      } }
    );
  });

  it('should call getYourCallBybMarkets()', () => {
    service.getYourCallBybMarkets().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/byb-markets`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getBYBSwitchers()', () => {
    service.getBYBSwitchers().subscribe();

    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${ environment.CMS_ENDPOINT }/${ environment.brand }/byb-switchers`,
      { observe: 'response', params: {} }
    );
  });

  it('should call getOTFIosToggle()', () => {
      service.getOTFIosToggle().subscribe();
      expect(httpClient.get).toHaveBeenCalled();
      expect(httpClient.get).toHaveBeenCalledWith(
          `${ environment.CMS_ENDPOINT }/${ environment.brand }/one-two-free/otf-ios-app-toggle`,
          { observe: 'response', params: {} }
        );
    });

  it('should call getFiveASideFormations()', () => {
    service.getFiveASideFormations().subscribe();
    expect(httpClient.get).toHaveBeenCalled();
    expect(httpClient.get).toHaveBeenCalledWith(
      `${environment.CMS_ENDPOINT}/${environment.brand}/five-a-side-formations`,
      { observe: 'response', params: {} }
    );
  });

  describe('@getMaintenancePage()', () => {
    it('(desktop maintenance page)', () => {
      deviceService.requestPlatform = 'desktop';

      service.getMaintenancePage().subscribe();

      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.CMS_ENDPOINT }/${ environment.brand }/maintenance-page/desktop`,
        { observe: 'response', params: {} }
      );
    });

    it('(mobile maintenance page)', () => {
      service.getMaintenancePage().subscribe();

      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.CMS_ENDPOINT }/${ environment.brand }/maintenance-page/mobile`,
        { observe: 'response', params: {} }
      );
    });
  });

  describe('@getData()', () => {
    it('should call getData() with params', () => {
      const url = 'test-link',
        options = { option: 'option' };

      service['getData'](url, options);

      expect(httpClient.get).toHaveBeenCalled();
      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.CMS_ENDPOINT }/${ environment.brand }/${ url }`,
        { observe: 'response', params: options }
      );
    });

    it('should call getData() without params', () => {
      const url = 'test-link';

      service['getData'](url);

      expect(httpClient.get).toHaveBeenCalled();
      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.CMS_ENDPOINT }/${ environment.brand }/${ url }`,
        { observe: 'response', params: {} }
      );
    });
  });

  describe('@getSurfaceBetsData()', () => {
    it('should call getSurfaceBetsData() with params', () => {
      const url = 'edp-surface-bets/1',
        options = { option: 'option' };

      service['getSurfaceBetsData'](url, options);

      expect(httpClient.get).toHaveBeenCalled();
      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.SURFACE_BETS_URL }/${ environment.brand }/${ url }`,
        { observe: 'response', params: options }
      );
    });

    it('should call getSurfaceBetsData() without params', () => {
      const url = 'edp-surface-bets/1';

      service['getSurfaceBetsData'](url);

      expect(httpClient.get).toHaveBeenCalled();
      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.SURFACE_BETS_URL }/${ environment.brand }/${ url }`,
        { observe: 'response', params: {} }
      );
    });
  });

  describe('@getV2Data()', () => {
    it('should call getV2Data() with params', () => {
      const url = 'offers',
        options = { option: 'option' };

      service['getV2Data'](url, options);

      expect(httpClient.get).toHaveBeenCalled();
      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.CMS_ENDPOINT }/v2/${ environment.brand }/${ url }`,
        { observe: 'response', params: options }
      );
    });

    it('should call getV2Data() without params', () => {
      const url = 'offers';

      service['getV2Data'](url);

      expect(httpClient.get).toHaveBeenCalled();
      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.CMS_ENDPOINT }/v2/${ environment.brand }/${ url }`,
        { observe: 'response', params: {} }
      );
    });
  });

  describe('@isGamingEnabled()', () => {
    it('should call isGamingEnabled() and return true', () => {
      service.systemConfiguration = {
        GamingEnabled: {
          enabled: false
        }
      } as any;

      expect(service['isGamingEnabled']()).toEqual(true);
    });

    it('should call isGamingEnabled() and return false if no GamingEnabled configuration', () => {
      service.systemConfiguration = {}  as any;

      expect(service['isGamingEnabled']()).toBe(false);
    });

    it('should call isGamingEnabled() and return false if gaming enabled', () => {
      service.systemConfiguration = {
        GamingEnabled: {
          enabled: true
        }
      } as any;

      expect(service['isGamingEnabled']()).toBe(false);
    });
  });

  describe('@getPromotions()', () => {
    it('should call getPromotions() without params', () => {
      service['getPromotions']().subscribe();

      expect(httpClient.get).toHaveBeenCalled();
      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.CMS_ENDPOINT }/${ environment.brand }/promotions`,
        { observe: 'response', params: {} }
      );
      expect(cmsToolsService.processResult).toHaveBeenCalled();
    });

    it('should call getPromotions() wit category id', () => {
      const id = '1';

      service['getPromotions'](id).subscribe();

      expect(httpClient.get).toHaveBeenCalled();
      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.CMS_ENDPOINT }/${ environment.brand }/promotions/${ id }`,
        { observe: 'response', params: {} }
      );
      expect(cmsToolsService.processResult).toHaveBeenCalled();
    });

    it('should call getPromotions() wit category id and V2', () => {
      const id = '1';

      service['getPromotions'](id, true).subscribe();

      expect(httpClient.get).toHaveBeenCalled();
      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.CMS_ENDPOINT }/v2/${ environment.brand }/promotions/${ id }`,
        { observe: 'response', params: {} }
      );
      expect(cmsToolsService.processResult).toHaveBeenCalled();
    });
  });

  describe('@filterScheduleTabs()', () => {
    it('should call filterScheduleTabs() without range and return the same ribbonData', () => {
      const ribbonData = [{
          devices: ['s1', 's2'],
          directiveName: 'featured',
          id: '1',
          showTabOn: 'tab',
          title: 'title',
          url: 'http://url',
          visible: true
        }],
        result = service['filterScheduleTabs'](ribbonData);

      expect(result).toEqual(ribbonData);
    });

    it('should call filterScheduleTabs() with one element, filter ribbonData and return the same ribbonData', () => {
      const ribbonData = [{
          devices: ['s1', 's2'],
          directiveName: 'featured',
          id: '1',
          showTabOn: 'tab',
          title: 'title',
          url: 'http://url',
          visible: true,
          displayFrom: new Date(new Date().setDate(new Date().getDate() - 1)).toDateString(),
          displayTo: new Date(new Date().setDate(new Date().getDate() + 1)).toDateString()
        }],
        result = service['filterScheduleTabs'](ribbonData);

      expect(result).toEqual(ribbonData);
    });

    it('should call filterScheduleTabs() with multiple elements, filter ribbonData and return the same ribbonData', () => {
      const ribbonData = [{
          devices: ['s1', 's2'],
          directiveName: 'featured',
          id: '1',
          showTabOn: 'tab',
          title: 'title',
          url: 'http://url',
          visible: true,
          displayFrom: new Date(new Date().setDate(new Date().getDate() - 1)).toDateString(),
          displayTo: new Date(new Date().setDate(new Date().getDate() + 1)).toDateString()
        }, {
          devices: ['s1', 's2'],
          directiveName: 'featured',
          id: '1',
          showTabOn: 'tab',
          title: 'title',
          url: 'http://url',
          visible: true,
          displayFrom: new Date(new Date().setDate(new Date().getDate() + 4)).toDateString(),
          displayTo: new Date(new Date().setDate(new Date().getDate() + 5)).toDateString()
        }],
        result = service['filterScheduleTabs'](ribbonData);

      expect(result).toEqual(ribbonData);
    });

    it('should call filterScheduleTabs(), filter ribbonData and return not the same ribbonData', () => {
      const ribbonData = [{
          devices: ['s1', 's2'],
          directiveName: 'eventhub',
          id: '1',
          showTabOn: 'tab',
          title: 'title',
          url: 'http://url',
          visible: true,
          displayFrom: new Date(new Date().setDate(new Date().getDate() - 1)).toDateString(),
          displayTo: new Date(new Date().setDate(new Date().getDate() + 1)).toDateString()
        }, {
          devices: ['s1', 's2'],
          directiveName: 'eventhub',
          id: '1',
          showTabOn: 'tab',
          title: 'title',
          url: 'http://url',
          visible: true,
          displayFrom: new Date(new Date().setDate(new Date().getDate() + 4)).toDateString(),
          displayTo: new Date(new Date().setDate(new Date().getDate() + 5)).toDateString()
        }],
        result = service['filterScheduleTabs'](ribbonData);

      expect(result).not.toEqual(ribbonData);
    });
  });

  it('should be in active range', () => {
    const from = new Date(new Date().setDate(new Date().getDate() - 1)).toDateString(),
      to = new Date(new Date().setDate(new Date().getDate() + 1)).toDateString(),
      result = service['isActiveRange'](from, to);

    expect(result).toBeTruthy();
  });

  it('should not be in active range', () => {
    const from = new Date(new Date().setDate(new Date().getDate())).toDateString(),
      to = new Date(new Date().setDate(new Date().getDate() + 1)).toDateString(),
      result = service['isActiveRange'](from, to);

    expect(result).toBeTruthy();
  });

  it('@parseContent: should replace params in cms static block', () => {
    const content = 'Place your 5 team accumulator or upwards on selected matches on the match betting market' +
      'and get money back as a free bet up to [[\'currency\']][[\'param1\']] if one team lets you down.';
    const params = ['20'];

    const actualResult = service.parseContent(content, params);

    expect(actualResult).toEqual('Place your 5 team accumulator or upwards on selected matches on the match betting market' +
      'and get money back as a free bet up to $20 if one team lets you down.');
  });

  it('@parseContent: should replace params in cms static block when params is string', () => {
    const content = 'Place your 5 team accumulator or upwards on selected matches on the match betting market' +
      'and get money back as a free bet up to [[\'currency\']][[\'param1\']] if one team lets you down.';
    const params = '[20]';

    const actualResult = service.parseContent(content, params);

    expect(actualResult).toEqual('Place your 5 team accumulator or upwards on selected matches on the match betting market' +
      'and get money back as a free bet up to $20 if one team lets you down.');
  });

  describe('isBogFromCms()', () => {
    it('should call isBogFromCms()', () => {
      const sysConfig: ISystemConfig = {
        BogToggle: {
          bogToggle: true
        }
      };
      service.getSystemConfig = jasmine.createSpy('getSystemConfig').and.returnValue(of(sysConfig));
      service.isBogFromCms().subscribe();

      expect(service.getSystemConfig).toHaveBeenCalledTimes(1);
    });
  });

  describe('getFiveASideStaticBlocks', () => {
    it('should get five-a-side static blocks', () => {
      service.getFiveASideStaticBlocks().subscribe();
      expect(httpClient.get).toHaveBeenCalled();
      expect(httpClient.get).toHaveBeenCalledWith(
        `${ environment.CMS_ENDPOINT }/${ environment.brand }/5a-side-static-block`,
        { observe: 'response', params: {} }
      );
    });
  });


  describe('getMarketSwitcherFlagValue()', () => {
    it('should call getMarketSwitcherFlagValue()', () => {
      const sysConfig: ISystemConfig = {
        MarketSwitcher: {
          cricket: true
        }
      };
      service.getFeatureConfig = jasmine.createSpy('getFeatureConfig').and.returnValue(of(sysConfig));
      service.getMarketSwitcherFlagValue('cricket').subscribe();
      expect(service.getFeatureConfig).toHaveBeenCalledTimes(1);
    });
    it('should return true if MarketSwitcher feature is enabled in CMS', fakeAsync(() => {
      const sysConfig: ISystemConfig = { cricket: true, AllSports: true };
      service.getFeatureConfig = jasmine.createSpy('getFeatureConfig').and.returnValue(of(sysConfig));
      service.getMarketSwitcherFlagValue('cricket')
        .subscribe(marketSwitcherFlag => {
          expect(marketSwitcherFlag).toBeTruthy();
        });
      tick();
    }));
    it('should return false if MarketSwitcher feature is disabled globally in CMS', fakeAsync(() => {
      const sysConfig: ISystemConfig = { cricket: true, AllSports: false };
      service.getFeatureConfig = jasmine.createSpy('getFeatureConfig').and.returnValue(of(sysConfig));
      service.getMarketSwitcherFlagValue('cricket')
        .subscribe(marketSwitcherFlag => {
          expect(marketSwitcherFlag).toBeFalsy();
        });
      tick();
    }));
    it('should return false if MarketSwitcher feature is disabled per sport level in CMS', fakeAsync(() => {
      const sysConfig: ISystemConfig = { cricket: false, AllSports: true };
      service.getFeatureConfig = jasmine.createSpy('getFeatureConfig').and.returnValue(of(sysConfig));
      service.getMarketSwitcherFlagValue('cricket')
        .subscribe(marketSwitcherFlag => {
          expect(marketSwitcherFlag).toBeFalsy();
        });
      tick();
    }));
    it('should return false if MarketSwitcher feature is not configured in CMS', fakeAsync(() => {
      const sysConfig: ISystemConfig = { };
      service.getFeatureConfig = jasmine.createSpy('getFeatureConfig').and.returnValue(of(sysConfig));
      service.getMarketSwitcherFlagValue('cricket')
        .subscribe((marketSwitcherFlag) => {
          expect(marketSwitcherFlag).toBeFalsy();
        });
      tick();
    }));
  });

  describe('getCmsInitData', () => {
    beforeEach(() => {
      service.initialData = undefined;
      service['initialCmsDataPromise'] = undefined;
      service['initialData$'] = undefined;
      service['getData'] = jasmine.createSpy('getData').and.returnValue(of({body: initialDataMock}));
      service['releaseSubject'] = jasmine.createSpy('releaseSubject');
    });

    it('should resolve initialCmsDataPromise if present in cmsInitConfigPromise Injected token', fakeAsync(() => {
      service['windowRef'] = { nativeWindow: {} } as any;
      service['cmsInitConfigPromise'] = Promise.resolve(initialDataMock);
      service['getCmsInitData']().subscribe();

      tick();

      expect(service['releaseSubject']).toHaveBeenCalledWith(initialDataMock);
      expect(service['getData']).not.toHaveBeenCalled();
    }));

    it('should do call if no Promise', fakeAsync(() => {
      service['windowRef'] = { nativeWindow: {} } as any;

      service['getCmsInitData']().subscribe();

      tick();

      expect(service['releaseSubject']).toHaveBeenCalledWith(initialDataMock);
      expect(service['getData']).toHaveBeenCalledWith('initial-data/mobile');
    }));

    it('should return stored data', () => {
      service['initialData$'] = new ReplaySubject<any>(1);
      service['initialData$'].next(initialDataMock);

      service['getCmsInitData']().subscribe((data) => {
        expect(data).toBe(initialDataMock);
      });
      expect(service['releaseSubject']).not.toHaveBeenCalled();
      expect(service['getData']).not.toHaveBeenCalled();
    });
  });

  it('releaseSubject', fakeAsync(() => {
    service.initialData = undefined;
    service.systemConfiguration = undefined;
    service['initialData$'] = new ReplaySubject<any>(1);
    service['initialData$'].subscribe((data) => {
      expect(data).toBe(initialDataMock);
    });
    service['releaseSubject'](initialDataMock);
    tick();

    expect(service['initialData$'].closed).toBe(false);
    expect(service['initialData$'].isStopped).toBe(true);
    expect(service.initialData).toBe(initialDataMock as any);
    expect(service.systemConfiguration).toBe(initialDataMock.systemConfiguration as any);
  }));

  describe('ngOnDestroy' , () => {
    it('should unsubscribe ReplaySubject', () => {
      service['initialData$'] = new ReplaySubject<any>(1);

      service.ngOnDestroy();

      expect(service['initialData$'].closed).toBe(true);
      expect(service['initialData$'].isStopped).toBe(true);
    });

    it('should not unsubscribe', function () {
      service['initialData$'] = null;

      service.ngOnDestroy();

      expect(service['initialData$']).toBe(null);
    });
  });
  it('getSportCategoryById', fakeAsync(() => {
    service['getCmsInitData'] = jasmine.createSpy('getCmsInitData').and.returnValue(of(initialDataMock));
    cmsToolsService.processResult.and.returnValue(initialDataMock.sportCategories);

    service.getSportCategoryById('1').subscribe((result) => {
      expect(result).toEqual({ categoryId: 1, sportName: 'category1' } as any);
    });
    tick();

    expect(cmsToolsService.processResult).toHaveBeenCalledWith(initialDataMock.sportCategories);
  }));

  it('getSportCategoryById when category id null', fakeAsync(() => {
    service['getCmsInitData'] = jasmine.createSpy('getCmsInitData').and.returnValue(of(initialDataMock));
    cmsToolsService.processResult.and.returnValue(
      [{ categoryId: null, sportName: 'categorynull' },
        { categoryId: 1, sportName: 'category1' },
        { categoryId: 3, sportName: 'greyhoundracing' },
        { imageTitle: 'football', svgId: '#1' }]
      );

    service.getSportCategoryById('1').subscribe((result) => {
      expect(result).toEqual({ categoryId: 1, sportName: 'category1' } as any);
    });
    tick();

    expect(cmsToolsService.processResult).toHaveBeenCalledWith(initialDataMock.sportCategories);
  }));

  describe('getSportCategoryByName', () => {
    beforeEach(() => {
      service['getCmsInitData'] = jasmine.createSpy('getCmsInitData').and.returnValue(of(initialDataMock));
      cmsToolsService.processResult.and.returnValue(initialDataMock.sportCategories);
    });

    it('football', fakeAsync(() => {
      service.getSportCategoryByName('category1').subscribe(res => {
        expect(res).toEqual({ categoryId: 1, sportName: 'category1' } as any);
      });
      tick();

      expect(cmsToolsService.processResult).toHaveBeenCalledWith(initialDataMock.sportCategories);
    }));

    it('greyhound', fakeAsync(() => {
      service.getSportCategoryByName('greyhound').subscribe(res => {
        expect(res).toEqual({ categoryId: 3, sportName: 'greyhoundracing' } as any);
      });
      tick();

      expect(cmsToolsService.processResult).toHaveBeenCalledWith(initialDataMock.sportCategories);
    }));
  });

  describe('getSportCategoriesByName', () => {
    beforeEach(() => {
      service['getCmsInitData'] = jasmine.createSpy('getCmsInitData').and.returnValue(of(initialDataMock));
      cmsToolsService.processResult.and.returnValue(initialDataMock.sportCategories);
    });

    it('football', fakeAsync(() => {
      service.getSportCategoriesByName(['category1', 'category2']).subscribe(res => {
        expect(res).toContain({ categoryId: 1, sportName: 'category1' } as any);
        expect(res).toContain({ categoryId: 2, sportName: 'category2' } as any);
      });
      tick();

      expect(cmsToolsService.processResult).toHaveBeenCalledWith(initialDataMock.sportCategories);
    }));

    it('greyhound', fakeAsync(() => {
      service.getSportCategoriesByName(['greyhound']).subscribe(res => {
        expect(res).toContain({ categoryId: 3, sportName: 'greyhoundracing' } as any);
      });
      tick();

      expect(cmsToolsService.processResult).toHaveBeenCalledWith(initialDataMock.sportCategories);
    }));
  });
});
