import { from as observableFrom, of } from 'rxjs';
import { FreeBetsService } from './free-bets.service';
import { pubSubApi } from '../communication/pubsub/pubsub-api.constant';
import { fakeAsync, flush, tick } from '@angular/core/testing';
import { IFreebetToken } from '@app/bpp/services/bppProviders/bpp-providers.model';

describe('FreeBetsService', () => {
  let service: FreeBetsService;
  let siteServer;
  let extensionsStorage;
  let user;
  let pubsub;
  let storage;
  let session;
  let time;
  let bpp;
  let nativeBridge;
  let dialog;
  let routingHelper;
  let filters;
  let locale;
  let sportsConfigHelperService;
  let device;
  let event;
  let initState;
  let now;
  let logoutCb;
  let pubsubStoreCb;
  let storeFreeebetsCallback;

  beforeEach(() => {
    now = new Date();
    siteServer = {
      getData: jasmine.createSpy('getData'),
      getCategories: jasmine.createSpy('getCategories')
    };
    extensionsStorage = {
      getList: jasmine.createSpy('getList')
    };

    sportsConfigHelperService = {
      getSportPathByCategoryId: jasmine.createSpy('getSportPathByCategoryId').and.returnValue(of('americanfootball'))
    };

    user = {
      status: true,
      username: 'test',
      isInShopUser: jasmine.createSpy('isInShopUser').and.returnValue(false)
    };
    pubsub = {
      subscribe: jasmine.createSpy('subscribe').and.callFake((subscriberName, channel, channelFunction) => {
        if (channel === 'SESSION_LOGOUT') {
          logoutCb = channelFunction;
        } else if (channel === 'STORE_FREEBETS') {
          pubsubStoreCb = channelFunction;
        } else if (channel === pubSubApi.STORE_FREEBETS_ON_REFRESH) {
          storeFreeebetsCallback = channelFunction;
        }
      }),
      publish: jasmine.createSpy('publish'),
      API: pubSubApi
    };
    initState = JSON.stringify([{
      freebetTokenId: '12',
      freebetTokenExpiryDate: '2050-12-11T22:00:00.000Z',
      freebetOfferName: 'testOffer',
      freebetTokenValue: 300,
      tokenPossibleBet: {
        betId: '12',
        betLevel: 'EVENT'
      }
    }]);
    storage = {
      get: jasmine.createSpy().and.returnValue(initState),
      set: jasmine.createSpy(),
      remove: jasmine.createSpy(),
      getCookie: jasmine.createSpy().and.returnValue(true)
    };
    session = {
      whenProxySession: jasmine.createSpy().and.returnValue(
        new Promise(function (resolve) {
          resolve();
        }))
    };
    time = {
      compareDate: jasmine.createSpy('compareDate'),
      formatByPattern: jasmine.createSpy('formatByPattern').and.returnValue('time string'),
      parseDateTime: jasmine.createSpy('formatByPattern'),
      daysDifference: jasmine.createSpy('daysDifference'),
      isInNext24HoursRange: jasmine.createSpy('isInNext24HoursRange').and.returnValue(false),
      parseDateInLocalFormat: jasmine.createSpy('parseDateInLocalFormat').and.returnValue(now)
    };
    bpp = {
      send: jasmine.createSpy().and.returnValue(observableFrom([{
        response: {
          model: {
            freebetToken: [{
              freebetTokenExpiryDate: '1',
              freebetTokenId: '12',
              tokenPossibleBet: {
                betId: null,
                betLevel: 'EVENT'
              }
            }]
          }
        }
      }]))
    };
    nativeBridge = {
      onFreeBetUpdated: jasmine.createSpy()
    };
    dialog = {
      openDialog: jasmine.createSpy('openDialog').and.callFake((dialogName, component, closeOther, params) => {
        params.onBeforeClose();
      })
    };
    routingHelper = {
      formEdpUrl: jasmine.createSpy('formEdpUrl').and.returnValue('testUrl')
    };
    filters = {
      currencyPosition: jasmine.createSpy().and.returnValue('$300')
    };
    locale = {
      getString: jasmine.createSpy('getString').and.returnValue('test')
    };
    device = {
      isDesktop: false
    };
    event = {
      cashoutAvail: 'test',
      categoryCode: 'test',
      categoryId: '12',
      categoryName: 'test',
      comments: {
        teams: {
          player_1: {
            id: '1'
          }
        }
      },
      displayOrder: 1,
      drilldownTagNames: 'test',
      eventIsLive: false,
      eventSortCode: 'test',
      eventStatusCode: '1',
      id: 12,
      isStarted: false,
      isUS: false,
      liveServChannels: 'ch.com',
      liveServChildrenChannels: 'ch.com',
      liveStreamAvailable: false,
      markets: [],
      marketsCount: 0,
      name: 'test',
      originalName: 'test',
      responseCreationTime: '0.04s',
      racingFormEvent: {
        class: ''
      },
      startTime: '12:00',
      streamProviders: {
        AtTheRaces: false,
        IMG: false,
        Perform: false,
        RPGTV: false,
        RacingUK: false,
        iGameMedia: false
      },
      svgId: '12',
      typeId: '4',
      typeName: 'event',
      outcomeId: 12
    };

    spyOn(console, 'info');

    service = new FreeBetsService(
      siteServer,
      extensionsStorage,
      user,
      pubsub,
      storage,
      session,
      time,
      bpp,
      nativeBridge,
      dialog,
      routingHelper,
      filters,
      locale,
      device,
      sportsConfigHelperService
    );

    spyOn(service, 'store').and.callThrough();
  });

  describe('constructor', () => {
    it('with free bets', () => {
      expect(service['freeBetsState']).toBeDefined();
      expect(pubsub.subscribe).toHaveBeenCalledWith('freeBetsFactory', pubsub.API.SESSION_LOGOUT, jasmine.any(Function));
      expect(service['freeBetsState'].available).toBeTruthy();
      expect(nativeBridge.onFreeBetUpdated).toHaveBeenCalledWith(true, jasmine.any(Array));
    });

    it('with callbacks', function () {
      const syncData = {
        data: [
          { tokenId: 1 },
        ],
        isPageRefresh: false
      };
      pubsubStoreCb(syncData);
      pubsub.subscribe.and.callFake((name, listeners, handler) => handler('someData'));
      expect(service['freeBetsState'].available).toBeTruthy();
      expect(nativeBridge.onFreeBetUpdated).toHaveBeenCalledTimes(2);
      expect(service.store).toHaveBeenCalledWith('test', syncData, false);
    });

    it('with callbacks and no data', function () {
      pubsubStoreCb();
      expect(service['freeBetsState'].available).toBeTruthy();
      expect(nativeBridge.onFreeBetUpdated).toHaveBeenCalledTimes(1);
      expect(service.store).not.toHaveBeenCalled();
    });
  });

  describe('store', () => {
    it('should store user free bets', () => {
      service.store('testUser', { data: [] });
      expect(nativeBridge.onFreeBetUpdated).toHaveBeenCalledTimes(2);
      expect(pubsub.publish).toHaveBeenCalledWith(pubsub.API.FREEBETS_UPDATED, jasmine.any(Object));
      expect(storage.set).toHaveBeenCalledTimes(1);
      expect(storage.remove).toHaveBeenCalledWith('hideFreeBetsFortestUser');
    });

    it('should store user free bets - sorted by freebetTokenExpiryDate', () => {
      time.compareDate.and.returnValue(1);
      const notSorted = {
        data: [
          { freebetTokenExpiryDate: '2025-06-25 12:16:10' },
          { freebetTokenExpiryDate: '2019-12-31 12:04:33' },
          { freebetTokenExpiryDate: '2019-06-25 15:16:10' }
        ]
      };
      const sorted = {
        data: [
          { freebetTokenExpiryDate: '2019-06-25 15:16:10', expires: '1 day' },
          { freebetTokenExpiryDate: '2019-12-31 12:04:33', expires: '1 day' },
          { freebetTokenExpiryDate: '2025-06-25 12:16:10', expires: '1 day' }
        ]
      };
      // @ts-ignore
      service.store('testUser', notSorted);
      // @ts-ignore
      expect(service['freeBetsState'].data).toEqual(sorted.data);
    });

    it('should handle params error', () => {
      service.store('testUser', { data: [], error: 'testError' });
      expect((console as any).info).toHaveBeenCalledWith('Freebets server error:', 'testError');
    });

    it('should clear stored free bets', () => {
      service.store('testUser', { data: undefined });
      expect(storage.remove).toHaveBeenCalledWith('freeBets-testUser');
    });
  });

  it('should get free bet', () => {
    service.getFreeBet('12').subscribe(
      () => {
        expect(time.compareDate).toHaveBeenCalledTimes(2);
      });

    service.getFreeBet('1').subscribe(
      () => {
      },
      () => {
        expect((console as any).info).toHaveBeenCalledWith(
          'No freebet available by ID 1', undefined
        );
      });
  });

  it('should get free bet with different method for session refresh', () => {
    service['processFreebetsRequest'](true).subscribe(
      () => {
        expect(bpp.send).toHaveBeenCalledWith('allAccountFreebets', 'SPORTS');
      });

    service['processFreebetsRequest']().subscribe(
      () => {
        expect(bpp.send).toHaveBeenCalledWith('accountFreebets', 'SPORTS');
      });
  });


  it('should handle storefreebets on refrech connect event', () => {
    spyOn(service, 'getFreeBets').and.returnValue(of({}));

    storeFreeebetsCallback();

    expect(service.getFreeBets).toHaveBeenCalledWith(true);
  });

  describe('getFreeBets', () => {
    it('should get free bets and set usedBy', fakeAsync(() => {
      time.compareDate.and.returnValue(8);
      service.getFreeBets().subscribe(
        () => {
          expect(time.compareDate).toHaveBeenCalledTimes(2);
          expect(time.formatByPattern).toHaveBeenCalledTimes(2);
        });
      flush();
    }));
    it('should get free bets and set not usedBy', fakeAsync(() => {
      time.compareDate.and.returnValue(1);
      service.getFreeBets().subscribe(
        () => {
          expect(time.compareDate).toHaveBeenCalledTimes(2);
        });
      flush();
    }));

    it('should not call bpp service in case if user is in-shop', fakeAsync(() => {
      const successHandler = jasmine.createSpy('successHandler');
      const errorHandler = jasmine.createSpy('errorHandler');

      user.isInShopUser.and.returnValue(true);
      service.getFreeBets().subscribe(successHandler, errorHandler);
      tick();

      expect(session.whenProxySession).not.toHaveBeenCalled();
      expect(successHandler).toHaveBeenCalledWith([]);
      expect(errorHandler).not.toHaveBeenCalled();
    }));
  });

  it('getFreeBetsData should return free bets data', () => {
    const result: any[] = service.getFreeBetsData();

    expect(result).toEqual([{
      freebetTokenId: '12',
      freebetTokenExpiryDate: '2050-12-11T22:00:00.000Z',
      freebetOfferName: 'testOffer',
      freebetTokenValue: 300,
      tokenPossibleBet: {
        betId: '12',
        betLevel: 'EVENT'
      }
    }]);
  });

  it('getFreeBetsData should return empty list', () => {
    storage.get.and.returnValue([]);
    const result: any[] = service.getFreeBetsData();

    expect(result).toEqual([]);
  });

  it('getBetNowLink no data', () => {
    expect(service['getBetNowLink']({} as any)).toEqual('/');
  });

  it('getBetNowLink ', () => {
    expect(service['getBetNowLink']({categoryId: 12, eventData: {}} as any)).toEqual('/testUrl');
  });

  it('getBetNowLink no eventData', fakeAsync(() => {
    service['getSportlink'] = jasmine.createSpy('getSportlink').and.returnValue('sportLink');
    expect(service['getBetNowLink']({categoryId: 12, eventData: null} as any)).toEqual('/sportLink');
    tick();
    expect(service['getSportlink']).toHaveBeenCalled();
  }));

  it('getSportlink', () => {
    expect(service['getSportlink']('horseracing', 'greyhound')).toEqual('sport/greyhound');
  });

  it('getFreeBetsSum should return string representing freebets sum', () => {
    storage.get.and.returnValue([]);
    const result: string = service.getFreeBetsSum();

    expect(filters.currencyPosition).toHaveBeenCalled();
    expect(result).toEqual('$300');
  });

  describe('@isFreeBetVisible', () => {
    it('should get free bet visibility (true)', () => {
      expect(service.isFreeBetVisible(event)).toBe(true);
    });

    it('should get free bet visibility (false, no data)', () => {
      service['freeBetsState'] = {
        data: []
      } as any;
      expect(service.isFreeBetVisible(event)).toBe(false);
    });

    it('should get free bet visibility (false, no token)', () => {
      service['freeBetsState'] = {
        data: [
          { tokenPossibleBet: {} },
          {},
        ]
      } as any;
      expect(service.isFreeBetVisible(event)).toBe(false);
    });

    it('should get free bet visibility (false)', () => {
      service['freeBetsState'] = {
        data: [
          {
            tokenPossibleBet: {
              betId: '231',
              betLevel: 'MARKET'
            }
          }
        ]
      } as any;
      event.markets = [{ id: '231' }];
      expect(service.isFreeBetVisible(event)).toBe(true);
    });

    it('should get free bet visibility (false)', () => {
      service['freeBetsState'] = {
        data: [
          {
            tokenPossibleBet: {
              betId: '77',
              betLevel: 'SELECTION'
            }
          }
        ]
      } as any;
      event.markets = [{ id: '231', outcomes: [{ id: '77' }] }];
      expect(service.isFreeBetVisible(event)).toBe(true);
    });
  });

  it('should get free bets state', () => {
    expect(service.getFreeBetsState().available).toBe(true);
  });

  it('should get free bets in split format', () => {
    const state = {
      expiry: '2050-12-11T22:00:00.000Z.000Z',
      id: '12',
      offerName: 'testOffer ',
      value: 300
    };
    expect(JSON.stringify(service.getFreeBetInBetSlipFormat('12'))).toBe(JSON.stringify(state));
  });

  describe('@showFreeBetsInfo', () => {
    it('should show free bets info', () => {
      service.showFreeBetsInfo().subscribe(
        (result) => {
          expect(storage.get).toHaveBeenCalledTimes(4);
          expect(result).toEqual(null);
        });
    });

    it('should not update stored data ', () => {
      storage.get.and.returnValue(false);
      service.showFreeBetsInfo().subscribe((result) => {
        expect(storage.set).not.toHaveBeenCalled();
        expect(result).toEqual(null);
      });
    });

    it('should not update stored data (no token)', () => {
      storage.getCookie.and.returnValue(undefined);
      service.showFreeBetsInfo().subscribe((result) => {
        expect(storage.set).not.toHaveBeenCalled();
        expect(result).toEqual(null);
      });
    });

    it('should show dialog', () => {
      storage.getCookie.and.returnValue(true);
      storage.get.and.returnValues(
        false,
        '[{"tokenId":32,"freebetTokenExpiryDate":"11-11-2019"}]'
      );
      filters.currencyPosition.and.returnValue('10$');

      service.showFreeBetsInfo().subscribe((result) => {
        expect(dialog.openDialog).toHaveBeenCalledWith(
          'freeBetsDialog',
          jasmine.any(Function),
          false,
          {
            freeBetsSum: '10$',
            freeBetsData: [{ tokenId: 32, freebetTokenExpiryDate: 'time string' }],
            onBeforeClose: jasmine.any(Function)
          }
        );
        expect(storage.set).toHaveBeenCalledWith('hideFreeBetsFortest', true);
        expect(pubsub.publish).toHaveBeenCalledWith('USER_INTERACTION_REQUIRED');
        expect(result).toEqual(null);
      });
    });
  });

  describe('showExpiryMessage', () => {
    it('should showExpiryMessage', () => {
      const cmp = {};
      service.getClosestExpiringFreebetIn24h = jasmine.createSpy('getClosestExpiringFreebetIn24h').and.returnValue({});
      service.showExpiryMessage(<any>cmp);
      expect(pubsub.publish).toHaveBeenCalledWith(pubsub.API.NOTIFICATION_SHOW, cmp);
    });

    it('should not showExpiryMessage (no expiring tokens)', () => {
      const cmp = {};
      service.getClosestExpiringFreebetIn24h = jasmine.createSpy('getClosestExpiringFreebetIn24h').and.returnValue(null);
      service.showExpiryMessage(<any>cmp);
      expect(pubsub.publish).not.toHaveBeenCalledWith(pubsub.API.NOTIFICATION_SHOW, cmp);
    });

    it('should not showExpiryMessage (desktop)', () => {
      const cmp = {};
      device.isDesktop = true;
      service.getClosestExpiringFreebetIn24h = jasmine.createSpy('getClosestExpiringFreebetIn24h').and.returnValue({});
      service.showExpiryMessage(<any>cmp);
      expect(pubsub.publish).not.toHaveBeenCalledWith(pubsub.API.NOTIFICATION_SHOW, cmp);
    });
  });

  describe('showExpiryMessage', () => {
    it('should getFreeBetExpiryMessage (hours)', () => {
      time.parseDateTime.and.returnValue(new Date(now.setHours(now.getHours() + 2)));
      service['closestExpiringFreeBet'] = <any>{ freebetTokenExpiryDate: 'date string' };
      expect(typeof service.getFreeBetExpiryMessage()).toEqual('string');
      expect(locale.getString).toHaveBeenCalledWith('bma.freebetsExpiryHours');
    });

    it('should getFreeBetExpiryMessage (minutes)', () => {
      time.parseDateTime.and.returnValue(new Date(now.setMinutes(now.getMinutes() + 20)));
      service['closestExpiringFreeBet'] = <any>{ freebetTokenExpiryDate: 'date string' };
      expect(typeof service.getFreeBetExpiryMessage()).toEqual('string');
      expect(locale.getString).toHaveBeenCalledWith('bma.freebetsExpiryMins');
    });
  });

  it('should getFreeBetAvailableMessage', () => {
    expect(service.getFreeBetAvailableMessage()).toBe('test');
  });

  describe('getHideFreeBetIDs', () => {
    it('should getHideFreeBetIDs', () => {
      const hideIDs = ['12', '13'];
      storage.get.and.returnValue(JSON.stringify(hideIDs));
      expect(service.getHideFreeBetIDs()).toEqual(hideIDs);
    });

    it('should getHideFreeBetIDs (empty)', () => {
      storage.get.and.returnValue(null);
      expect(service.getHideFreeBetIDs()).toEqual([]);
    });
  });

  it('should hideExpiringMessageForFreeBet', () => {
    service['closestExpiringFreeBet'] = <any>{ freebetTokenId: '300' };
    storage.get.and.returnValue(JSON.stringify(['1', '2']));
    user.username = 'test';
    service.hideExpiringMessageForFreeBet();
    expect(storage.set).toHaveBeenCalledWith('hideFreeBetIDs-test', JSON.stringify(['1', '2', '300']));
  });

  describe('getClosestExpiringFreebetIn24h', () => {
    it('should getClosestExpiringFreebetIn24h', () => {
      const freeBets = <any>[{ freebetTokenExpiryDate: '2050-12-11T22:00:00.00' }, {}];
      storage.get.and.callFake(key => {
        if (key.indexOf('freeBets-') > -1) {
          return JSON.stringify(freeBets);
        } else {
          return JSON.stringify(['1', '2']);
        }
      });
      time.parseDateTime.and.returnValue(new Date());
      time.daysDifference.and.returnValue(-0.8);
      time.isInNext24HoursRange.and.returnValue(true);
      expect(service.getClosestExpiringFreebetIn24h()).toEqual(freeBets[0]);
    });

    it('should getClosestExpiringFreebetIn24h (hidden for IDs)', () => {
      const freeBets = <any>[{ id: '1' }, { id: '2' }];

      storage.get.and.callFake(key => {
        if (key.indexOf('freeBets-') > -1) {
          return JSON.stringify(freeBets);
        } else {
          return JSON.stringify(['1', '2']);
        }
      });
      expect(service.getClosestExpiringFreebetIn24h()).toEqual(null);
    });

    it('should getClosestExpiringFreebetIn24h (no freeBets)', () => {
      const freeBets = <any>[];

      storage.get.and.callFake(key => {
        if (key.indexOf('freeBets-') > -1) {
          return JSON.stringify(freeBets);
        } else {
          return JSON.stringify([]);
        }
      });
      expect(service.getClosestExpiringFreebetIn24h()).toEqual(null);
    });

    it('should getClosestExpiringFreebetIn24h (no freeBets)', () => {
      expect(service.getClosestExpiringFreebetIn24h()).toEqual(null);
    });
  });

  it('should hide expiry notification on logout', () => {
    logoutCb();
    expect(pubsub.publish).toHaveBeenCalledWith(pubsub.API.NOTIFICATION_HIDE);
  });

  describe('getBetLevelName', () => {
    it('should return empty string because of lack of data from SideSrev for event', fakeAsync(() => {
      siteServer.getData.and.returnValue([]);
      service.getBetLevelName('1', 'EVENT').subscribe(data => {
        expect(data).toBe('');
      });
      flush();
    }));

    it('should return event name', fakeAsync(() => {
      siteServer.getData.and.returnValue([{
        event: {
          name: 'eventName'
        }
      }]);
      service.getBetLevelName('1', 'EVENT').subscribe(data => {
        expect(data).toBe('eventName');
      });
      flush();
    }));

    it('should return type name', fakeAsync(() => {
      siteServer.getData.and.returnValue([{
        class: {
          children: [{
            type: {
              name: 'typeName'
            }
          }]
        }
      }]);
      service.getBetLevelName('1', 'TYPE').subscribe(data => {
        expect(data).toBe('typeName');
      });
      flush();
    }));

    it('should return class name', fakeAsync(() => {
      siteServer.getData.and.returnValue([{
        class: {
          name: 'className'
        }
      }]);
      service.getBetLevelName('1', 'CLASS').subscribe(data => {
        expect(data).toBe('className');
      });
      flush();
    }));

    it('should return empty string because of lack of data from SideSrev for category', fakeAsync(() => {
      siteServer.getCategories.and.returnValue([]);
      service.getBetLevelName('1', 'CATEGORY').subscribe(data => {
        expect(data).toBe('');
      });
      flush();
    }));

    it('should return category name', fakeAsync(() => {
      siteServer.getCategories.and.returnValue([[{ name: 'className' }]]);
      service.getBetLevelName('1', 'CATEGORY').subscribe(data => {
        expect(data).toBe('className');
      });
      flush();
    }));


    it('handle incorrect betLevel', fakeAsync(() => {
      service.getBetLevelName('', 'SOME').subscribe(data => {
        expect(data).toBe('');
      });
      flush();
    }));
  });

  it('@enhanceFreeBetItem with less then 7', () => {

    const date = '2050-12-11T22:00:00.000Z';
    const freeBetItem = ({ freebetTokenExpiryDate: date, expires: null, usedBy: null }) as any;
    time.compareDate.and.returnValue(1);
    service['enhanceFreeBetItem'](freeBetItem);

    expect(freeBetItem.expires).not.toBeNull();
  });

  it('@enhanceFreeBetItem with more then 7', () => {

    const date = '2050-12-11T22:00:00.000Z';
    const freeBetItem = ({ freebetTokenExpiryDate: date, expires: null, usedBy: null }) as any;
    time.compareDate.and.returnValue(8);
    service['enhanceFreeBetItem'](freeBetItem);

    expect(time.formatByPattern).toHaveBeenCalledWith(jasmine.any(Object), 'dd/MM/yyyy');
    expect(freeBetItem.usedBy).not.toBeNull();

  });

  describe('@getOddsBoostsWithCategories', () => {
    it('should ', () => {
      siteServer.getCategories.and.returnValue(Promise.resolve(['112', '111, 114']));
      siteServer.getData.and.returnValue(Promise.resolve([
        {
          id: '111',
          event: {
            children: [
              { market: { id: 'market_1' } },
              { market: { id: 'market_2' } },
            ]
          }
        }
      ]));

      const tokens = [
        { tokenId: '1', tokenPossibleBet: { betLevel: 'MARKET', betId: '111' } },
        { tokenId: '81', tokenPossibleBet: { betLevel: 'EVENT', betId: '112' } },
        { tokenId: '94', tokenPossibleBet: { betLevel: 'CATEGORY', betId: '114' } },
        { tokenId: '4' },
      ] as any;

      const expectedTokens: IFreebetToken[] = [
        {
          tokenId: '1',
          tokenPossibleBet: { betLevel: 'MARKET', betId: '111' },
          betNowLink: '/'
        }, {
          tokenId: '81',
          tokenPossibleBet: { betLevel: 'EVENT', betId: '112' },
          betNowLink: '/'
        },
        {
          tokenId: '94',
          tokenPossibleBet: { betLevel: 'CATEGORY', betId: '114' },
          betNowLink: '/'
        },
        { tokenId: '4', betNowLink: '/' }
      ] as any;

      service.getOddsBoostsWithCategories(tokens).subscribe((freeBetTokes: IFreebetToken[]) => {
        expect(freeBetTokes).toEqual(expectedTokens);
      });
    });
  });

  describe('@getFreeBetWithBetNowLink', () => {
    it('should handle token without tokenPossibleBet', () => {
      const token = {} as any;
      service.getFreeBetWithBetNowLink(token).subscribe((freeBet) => {
        expect(siteServer.getData).not.toHaveBeenCalled();
        expect(freeBet).toEqual({ betNowLink: '/' } as any);
      });
    });

    it('should handle token with betLevel = CATEGORY', () => {
      const token = {
        tokenPossibleBet: {
          betId: '1',
          betLevel: 'CATEGORY'
        }
      } as any;
      service.getFreeBetWithBetNowLink(token).subscribe((freeBet) => {
        expect(siteServer.getData).not.toHaveBeenCalled();
        expect(freeBet).toEqual({
          tokenPossibleBet: { betId: '1', betLevel: 'CATEGORY' },
          betNowLink: '/sport/americanfootball'
        } as any);
      });
    });

    it('should handle token with betLevel = BET', () => {
      const token = {
        tokenPossibleBet: {
          betId: '1',
          betLevel: 'MARKET'
        }
      } as any;
      siteServer.getData.and.returnValue(Promise.resolve({}));
      service.getFreeBetWithBetNowLink(token).subscribe((freeBet) => {
        expect(freeBet).toEqual({
          tokenPossibleBet: { betId: '1', betLevel: 'MARKET' },
          betNowLink: '/'
        } as any);
      });
    });

    it('should handle error', () => {
      const token = {
        tokenPossibleBet: {
          betId: '1',
          betLevel: 'MARKET'
        }
      } as any;
      siteServer.getData.and.returnValue(Promise.reject({}));
      service.getFreeBetWithBetNowLink(token).subscribe((freeBet) => {
        expect(freeBet).toEqual({
          tokenPossibleBet: { betId: '1', betLevel: 'MARKET' },
          betNowLink: '/'
        } as any);
      });
    });
  });

  it('@getSelectionOutcomeIds should extract outcome ids', () => {
    const sportEvent = {
      event: {
        children: [
          { market: { children: [{ outcome: { id: '31' } }] } },
          { market: { children: [{ outcome: { id: '5' } }, { outcome: { id: '7' } }] } },
          { market: { children: [{ outcome: { id: '10' } }] } },
        ]
      }
    } as any;
    const expectedResult = ['31', '5', '7', '10'];
    expect(service['getSelectionOutcomeIds'](sportEvent)).toEqual(expectedResult);
  });

  it('@getBetNowLink', () => {
    extensionsStorage.getList.and.returnValue([
      {
        name: 'extension-name',
        sportsConfig: { id: '6' },
        extendsModule: 'sb'
      }
    ]);
    routingHelper.formEdpUrl.and.returnValue('router-helper-link');
    const categoryId = '6';
    const eventData = {};

    expect(service['getBetNowLink']({ categoryId, eventData } as any)).toEqual('/router-helper-link');
  });
});
