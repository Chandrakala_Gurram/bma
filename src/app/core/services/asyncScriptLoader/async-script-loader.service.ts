import { of as observableOf, Observable, Observer } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WindowRefService } from '../windowRef/window-ref.service';

@Injectable({
  providedIn: 'root'
})
export class AsyncScriptLoaderService {

  private loadedFiles: Map<string, boolean> = new Map();

  constructor(
    private windowRef: WindowRefService,
    private http: HttpClient
  ) { }

  loadJsFile(fileName: string, attrs?: {[key: string]: string}): Observable<string> {
    if (this.loadedFiles.has(fileName) && this.loadedFiles.get(fileName)) {
      return observableOf(null);
    }
    return Observable.create((observer: Observer<string>) => {
      const script = this.windowRef.document.createElement('script');
      script.type = 'text/javascript';

      if (attrs) {
        const attributes = Object.keys(attrs);
        attributes.forEach((attribute: string) => {
          script.setAttribute(attribute, attrs[attribute]);
        });
      }

      script.src = fileName;
      script.onload = () => this.onLoad(observer, fileName);
      script.onerror = () => this.onError(observer, fileName);

      this.windowRef.document.body.appendChild(script);
      this.loadedFiles.set(fileName, true);
    });
  }

  loadCssFile(fileName: string, isHeadAppend?: boolean): Observable<string> {
    if (this.loadedFiles.has(fileName) && this.loadedFiles.get(fileName)) {
      return observableOf(null);
    }
    return Observable.create((observer: Observer<string>) => {
      const link = this.windowRef.document.createElement('link');
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.media = 'all';
      link.href = fileName;
      link.onload = () => this.onLoad(observer, fileName);
      link.onerror = () => this.onError(observer, fileName);

      if (isHeadAppend) {
        this.windowRef.document
                      .getElementsByTagName('head')[0].appendChild(link);
      } else {
        this.windowRef.document.body.appendChild(link);
      }

      this.loadedFiles.set(fileName, true);
    });
  }

  /**
   * Get svg sprite from cms (S3)
   *
   * @param filePath absolute or relative url to sprite
   */
  getSvgSprite(filePath: string): Observable<string> {
    return this.http.get(filePath).pipe(
      map((data: {[key: string]: string}) => data.content)
    );
  }

  loadSvgIcons(fileName: string, isLoaded: boolean = true): Observable<string> {
    if (this.loadedFiles.has(fileName) && isLoaded) {
      return observableOf(null);
    }

    const httpOptions = {
      responseType: 'text' as 'text'
    };

    return this.http.get(fileName, httpOptions).pipe(
      map((data: string) => {

        this.loadedFiles.set(fileName, true);
        return data;
      }));
  }

  private onError(observer, fileName: string): void {
    this.loadedFiles.set(fileName, false);
    observer.error(`Fail to Load ${fileName}`);
    observer.complete();
  }

  private onLoad(observer, fileName: string): void {
    observer.next(fileName);
    observer.complete();
  }
}
