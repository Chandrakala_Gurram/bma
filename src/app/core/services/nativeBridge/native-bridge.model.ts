export interface INaviveBridgeDetails {
  detail: {
    settingValue?: boolean|string;
    selectionId?: number|string;
  };
}
