import { PreloadingStrategy, Route } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of as observableOf } from 'rxjs';
import {
  VanillaPortalModuleLoadNotifier
} from './services/vanillaPortalModuleLoadNotifier/vanilla-portal-module-load-notifier.service';

@Injectable()
export class CustomPreloadingStrategy implements PreloadingStrategy {
  constructor(private vanillaPortalModuleLoadNotifier: VanillaPortalModuleLoadNotifier) {

  }
  preload(route: Route, load: () => Observable<any>): Observable<any> {
    if (route && route.data && route.data['preload']) {
      this.vanillaPortalModuleLoadNotifier.loadPortalNotificationObservable.subscribe((loadModule: boolean) => {
        if (loadModule) {
          return load();
        }
      });
    }
    return observableOf(null);
  }
}
