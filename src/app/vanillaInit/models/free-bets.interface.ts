import { MenuSection } from '@vanilla/core';

export interface IFreeBetsBadgeModel {
    section: MenuSection.Header | MenuSection.Menu;
    item: string;
    count: string | number;
    cssClass?: string;
}
