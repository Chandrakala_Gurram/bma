import { Routes } from '@angular/router';
import { PublicPageLoaderComponent } from '@vanilla/core/features';
import { ClientConfigGuard } from '@app/host-app/client-config.guard';

export const ROOT_APP_ROUTES: Routes = [
  {
    path: 'p',
    children: [
      {
        path: '**',
        component: PublicPageLoaderComponent,
        data: {
          publicPageRoot: 'App-v1.0/PublicPages/'
        }
      }
    ]
  },
  {
    path: 'mobileportal',
    canActivate: [ClientConfigGuard],
    data: {
      product: 'portal',
      preload: true
    },
    loadChildren: '@app/host-app/plugins/portal#PortalModuleLoader'
  },
  {
    path: '**',
    redirectTo: '/'
  }
];
