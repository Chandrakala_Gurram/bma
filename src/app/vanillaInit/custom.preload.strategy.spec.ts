import { CustomPreloadingStrategy } from './custom.preload.strategy';
import { VanillaPortalModuleLoadNotifier } from './services/vanillaPortalModuleLoadNotifier/vanilla-portal-module-load-notifier.service';
import { Route } from '@angular/router';

describe('CustomPreloadingStrategy', () => {
  let service: CustomPreloadingStrategy;
  let route;
  let load;
  let portalModuleLoadNotifierService: VanillaPortalModuleLoadNotifier;
  let stylesService;
  let coralSportsConfig;
  let windowRefMock;

  beforeEach(() => {
    stylesService = {
      load: jasmine.createSpy()
    };
    coralSportsConfig = {
      preloadPortalModule: true,
      portalModuleLoadDelay: 5000
    };
    windowRefMock = {
      nativeWindow: {
        setTimeout: jasmine.createSpy().and.callFake(fn => fn && fn())
      }
    };
    portalModuleLoadNotifierService = new VanillaPortalModuleLoadNotifier(stylesService, coralSportsConfig, windowRefMock);
    service = new CustomPreloadingStrategy(portalModuleLoadNotifierService);
    route = {
      data: {
        preload: true
      }
    };
    load = jasmine.createSpy();
  });

  it('calls preload with true and subscribes to loadportalmodule and calls load', () => {
    portalModuleLoadNotifierService.loadPortalModule();
    service.preload(route, load).subscribe(() => {
      expect(load).toHaveBeenCalled();
    });
  });

  it('calls preload with true and does not subscribe to loadportalmodule and does not call load', () => {
    coralSportsConfig.preloadPortalModule = false;
    portalModuleLoadNotifierService = new VanillaPortalModuleLoadNotifier(stylesService, coralSportsConfig, windowRefMock);
    portalModuleLoadNotifierService.loadPortalModule();
    service.preload(route, load).subscribe(() => {
      expect(load).not.toHaveBeenCalled();
    });
  });

  it('calls preload with route as null, doesnt call load even after event is fired', () => {
    route = null;
    assertWhenRouteIsNullOrEmpty(route);
  });

  it('calls preload with route as empty, doesnt call load even after event is fired', () => {
    route = {};
    assertWhenRouteIsNullOrEmpty(route);
  });

  it('calls preload with route data as null, doesnt call load even after event is fired', () => {
    route.data = null;
    assertWhenRouteIsNullOrEmpty(route);
  });

  it('calls preload with route data as empty, doesnt call load even after event is fired', () => {
    route.data = {};
    assertWhenRouteIsNullOrEmpty(route);
  });

  it('calls preload with false and doesnt call load', () => {
    route.data.preload = false;
    assertWhenRouteIsNullOrEmpty(route);
  });

  it('calls preload with true and does not call load if observable is not fired', () => {
    service.preload(route, load).subscribe(() => {
      expect(load).not.toHaveBeenCalled();
    });
  });

  function assertWhenRouteIsNullOrEmpty(routeObj: Route) {
    portalModuleLoadNotifierService.loadPortalModule();
    service.preload(routeObj, load).subscribe(() => {
      expect(load).not.toHaveBeenCalled();
    });
  }

});
