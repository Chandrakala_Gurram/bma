import { Injectable } from '@angular/core';
import { ApiBase } from '@vanilla/core/core';

@Injectable({
  providedIn: 'root'
})
export class VanillaApiService extends ApiBase {
}
