import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpErrorHandler } from '@labelhost/core';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

@Injectable()
export class LabelHostHttpErrorHandler implements HttpErrorHandler {
  constructor(
    private windowRefService: WindowRefService
  ) {}

  handleError(response: HttpErrorResponse): boolean {
    return !this.isVanillaError(response);
  }

  private isVanillaError(response: HttpErrorResponse): boolean {
    return response.url && response.url.includes(this.windowRefService.nativeWindow.location.origin);
  }
}
