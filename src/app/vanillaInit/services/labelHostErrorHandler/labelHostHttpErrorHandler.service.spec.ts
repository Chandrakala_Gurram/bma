import { HttpErrorResponse } from '@angular/common/http';
import {
  LabelHostHttpErrorHandler
} from '@vanillaInitModule/services/labelHostErrorHandler/labelHostHttpErrorHandler.service';

describe('LabelHostHttpErrorHandler', () => {
  let service: LabelHostHttpErrorHandler;

  let windowRefService;

  beforeEach(() => {
    windowRefService = {
      nativeWindow: {
        location: {
          origin: 'https://some-host.com'
        }
      }
    };
    service = new LabelHostHttpErrorHandler(windowRefService);
  });

  it('#handleError', () => {
    const response = {} as HttpErrorResponse;
    service['isVanillaError'] = jasmine.createSpy().and.returnValue(false);
    expect(service.handleError(response)).toBeTruthy();
    expect(service['isVanillaError']).toHaveBeenCalledWith(response);
  });

  it('#isVanillaError should return false when there is no url in error', () => {
    const response = {} as HttpErrorResponse;
    expect(service['isVanillaError'](response)).toBeFalsy();
  });

  it('#isVanillaError should return false when url from error has another origin', () => {
    const response = { url: 'https://host.com' } as HttpErrorResponse;
    expect(service['isVanillaError'](response)).toBeFalsy();
  });

  it('#isVanillaError should return true', () => {
    const response = { url: 'https://some-host.com' } as HttpErrorResponse;
    expect(service['isVanillaError'](response)).toBeTruthy();
  });
});
