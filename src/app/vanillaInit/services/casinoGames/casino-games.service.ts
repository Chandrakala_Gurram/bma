import { Injectable } from '@angular/core';
import { UserService, ClaimsConfig } from '@vanilla/core/core';
import { CasinoGamesClientConfig } from '@app/client-config/bma-casino-games-config';
import { ProductHomepagesConfig } from '@labelhost/core';

@Injectable({
  providedIn: 'root'
})
export class CasinoGamesService {

  constructor(
    private user: UserService,
    private casinoGamesConfig: CasinoGamesClientConfig,
    private productHomepagesConfig: ProductHomepagesConfig,
    private claimsConfig: ClaimsConfig
  ) { }

  get isMiniGamesEnabled(): boolean {
    return this.casinoGamesConfig.miniGamesEnabled;
  }

  get miniGamesUrl(): string {
    return this.casinoGamesConfig.miniGamesHost + this.templateUrl;
  }

  get isRecentlyPlayedGamesEnabled(): boolean {
    return this.casinoGamesConfig.recentlyPlayedGamesEnabled;
  }

  get recentlyPlayedGamesUrl(): string {
    return this.productHomepagesConfig.casino + this.casinoGamesConfig.recentlyPlayedGamesUrl;
  }

  get isSeeAllEnabled(): boolean {
    return this.casinoGamesConfig.seeAllEnabled;
  }

  get seeAllUrl(): string {
    return this.productHomepagesConfig.casino + this.casinoGamesConfig.seeAllUrl;
  }

  private get templateUrl(): string {
  const nameidentifier = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier';

    return this.casinoGamesConfig.miniGamesTemplate
      .replace('$USERIP$', this.casinoGamesConfig.userHostAddress)
      .replace('$CURRENCY$', this.user.claims.get('currency'))
      .replace('$SESSION_KEY$', this.user.claims.get('ssotoken'))
      .replace('$HOSTURL$', this.productHomepagesConfig.sports)
      .replace('$accountName$', this.claimsConfig[nameidentifier]);
  }
}
