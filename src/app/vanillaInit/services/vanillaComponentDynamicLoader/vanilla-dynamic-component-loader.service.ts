import { Injectable } from '@angular/core';
import { first } from 'rxjs/operators';

import { UserLoginEvent, UserService } from '@vanilla/core/core';
import { HeaderService } from '@vanilla/core/features';

import { BetslipHeaderIconComponent } from '@sharedModule/components/betslipHeaderIcon/betslip-header-icon.component';
import { MyBetsButtonComponent } from '@shared/components/myBetsButton/my-bets-button.component';

@Injectable({
    providedIn: 'root'
})
export class VanillaDynamicComponentLoaderService {

    protected VANILLA_HEADER_SLOTS: { [key: string]: string } = {
        betslip: 'betslip',
        mybets: 'mybets'
    };

    constructor(
      protected user: UserService,
      protected headerService: HeaderService,
    ) {
    }

    init(): void {
        this.user.events.pipe(first(e => e instanceof UserLoginEvent)).subscribe(() => {
            this.addComponentsToVanillaHeader();
        });

        this.addComponentsToVanillaHeader();
    }

    protected addComponentsToVanillaHeader(): void {
        if (this.user.isAuthenticated) {
            // Add betslip icon and my bets button to Vanilla header dynamically
            this.headerService.setHeaderComponent(this.VANILLA_HEADER_SLOTS.mybets, MyBetsButtonComponent);
            this.headerService.setHeaderComponent(this.VANILLA_HEADER_SLOTS.betslip, BetslipHeaderIconComponent);
            return;
        }
        // Add betslip icon to Vanilla header dynamically
        this.headerService.setHeaderComponent('betslip', BetslipHeaderIconComponent);
    }
}
