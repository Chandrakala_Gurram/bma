import {
  VanillaDynamicComponentLoaderService
} from '@vanillaInitModule/services/vanillaComponentDynamicLoader/vanilla-dynamic-component-loader.service';
import { Subject } from 'rxjs';
import { UserLoginEvent } from '@vanilla/core/core';
import { MyBetsButtonComponent } from '@shared/components/myBetsButton/my-bets-button.component';
import { BetslipHeaderIconComponent } from '@shared/components/betslipHeaderIcon/betslip-header-icon.component';

describe('VanillaDynamicComponentLoaderService', () => {
  let service: VanillaDynamicComponentLoaderService;

  let user;
  let headerService;

  beforeEach(() => {
    user = {
      isAuthenticated: true,
      events: new Subject()
    };
    headerService = {
      setHeaderComponent: jasmine.createSpy()
    };

    service = new VanillaDynamicComponentLoaderService(user, headerService);
  });

  it('#init should call correct methods', () => {
    const event = new UserLoginEvent();
    service['addComponentsToVanillaHeader'] = jasmine.createSpy();
    service.init();
    user.events.next(event);
    expect(service['addComponentsToVanillaHeader']).toHaveBeenCalledTimes(2);
  });

  it('#addComponetsToVanillaHeader should call correct methods when user is authenticated', () => {
    service['addComponentsToVanillaHeader']();
    expect(headerService.setHeaderComponent)
      .toHaveBeenCalledWith(service['VANILLA_HEADER_SLOTS']['mybets'], MyBetsButtonComponent);
    expect(headerService.setHeaderComponent)
      .toHaveBeenCalledWith(service['VANILLA_HEADER_SLOTS']['betslip'], BetslipHeaderIconComponent);
  });

  it('#addComponetsToVanillaHeader should call correct methods when user isn\'t authenticated', () => {
    user.isAuthenticated = false;
    service = new VanillaDynamicComponentLoaderService(user, headerService);
    service['addComponentsToVanillaHeader']();
    expect(headerService.setHeaderComponent)
      .toHaveBeenCalledWith('betslip', BetslipHeaderIconComponent);
  });
});
