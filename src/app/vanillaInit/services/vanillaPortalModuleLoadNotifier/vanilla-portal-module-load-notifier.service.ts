import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { StylesService } from '@vanilla/core/core';
import { UserInterfaceClientConfig } from '@app/client-config/bma-user-interface-config';
import { WindowRefService } from '@root/app/core/services/windowRef/window-ref.service';

@Injectable()
export class VanillaPortalModuleLoadNotifier {

  loadPortalNotifier = new BehaviorSubject<boolean>(null);
  loadPortalNotificationObservable: Observable<boolean> = this.loadPortalNotifier.asObservable();

  constructor(private stylesService: StylesService,
    private userInterfaceConfig: UserInterfaceClientConfig,
    private windowRef: WindowRefService) {
  }

  loadPortalModule(): void {
    if (this.userInterfaceConfig.preloadPortalModule) {
      this.windowRef.nativeWindow.setTimeout(() => {
        this.loadPortalNotifier.next(true);
        if (process.env.NODE_ENV === 'production') {
          this.stylesService.load('portalStyles');
        }
      }, this.userInterfaceConfig.portalModuleLoadDelay);
    }
  }
}
