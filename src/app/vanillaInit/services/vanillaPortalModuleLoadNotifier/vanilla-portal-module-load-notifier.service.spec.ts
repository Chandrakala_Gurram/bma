import { VanillaPortalModuleLoadNotifier } from './vanilla-portal-module-load-notifier.service';

describe('VanillaPortalModuleLoadNotifier', () => {
  let service: VanillaPortalModuleLoadNotifier;
  let stylesService;
  let coralSportsConfig;
  let windowRefMock;

  beforeEach(() => {
    stylesService = {
      load: jasmine.createSpy()
    };
    coralSportsConfig = {
      preloadPortalModule: true,
      portalModuleLoadDelay: 5000
    };
    windowRefMock = {
      nativeWindow: {
        setTimeout: jasmine.createSpy().and.callFake(fn => fn && fn())
      }
    };
    service = new VanillaPortalModuleLoadNotifier(stylesService, coralSportsConfig, windowRefMock);
  });

  it('raises an event with true when loadPortalModule called and calls stylesService load if production environment', () => {
    service.loadPortalModule();
    expect(windowRefMock.nativeWindow.setTimeout).toHaveBeenCalled();
    service.loadPortalNotificationObservable.subscribe((result) => expect(result).toBe(true));
    if (process.env.NODE_ENV === 'production') {
      expect(stylesService.load).toHaveBeenCalled();
    }
  });

  it('does not raise an event when loadPortalModule called if preloadPortalModule is set to false', () => {
    coralSportsConfig.preloadPortalModule = false;
    service = new VanillaPortalModuleLoadNotifier(stylesService, coralSportsConfig, windowRefMock);
    service.loadPortalModule();
    expect(windowRefMock.nativeWindow.setTimeout).not.toHaveBeenCalled();
    service.loadPortalNotificationObservable.subscribe((result) => expect(result).toBe(null));
    expect(stylesService.load).not.toHaveBeenCalled();
  });

});
