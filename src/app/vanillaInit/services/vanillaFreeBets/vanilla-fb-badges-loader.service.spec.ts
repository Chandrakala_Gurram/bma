import {
    VanillaFreebetsBadgeDynamicLoaderService
} from '@vanillaInitModule/services/vanillaFreeBets/vanilla-fb-badges-loader.service';

describe('VanillaFreebetsBadgeDynamicLoaderService', () => {
    let fbBadgeLoaderService;
    let menuCountersService;
    let freebetsBadgeService;

    beforeEach(() => {
        menuCountersService = {
            update: jasmine.createSpy()
        };

        freebetsBadgeService = {
            freeBetCounters: []
        };

        fbBadgeLoaderService = new VanillaFreebetsBadgeDynamicLoaderService(
            menuCountersService,
            freebetsBadgeService
        );
    });

    it('addCounter', () => {
        fbBadgeLoaderService.addCounter({} as any);
        expect(freebetsBadgeService.freeBetCounters.length).toBe(1);
    });

    describe('sportsFreebetsCount', () => {
        it('#when freebetTokenType === "SPORTS" ', () => {
            const freeBetArr = [{ freebetTokenType: 'SPORTS' } as any];
            expect(fbBadgeLoaderService.sportsFreebetsCount(freeBetArr)).toBe(1);
        });
        it('#when we have no FreeBets', () => {
            const freeBetArr = [];
            expect(fbBadgeLoaderService.sportsFreebetsCount(freeBetArr)).toBe(0);
        });
        it('#when freebetTokenType !== "SPORTS"', () => {
            const freeBetArr = [{ freebetTokenType: 'FreeBets' } as any];
            expect(fbBadgeLoaderService.sportsFreebetsCount(freeBetArr)).toBe(0);
        });
    });
    it('update', () => {
        fbBadgeLoaderService.update();
        expect(menuCountersService.update).toHaveBeenCalled();
    });

    describe('addBadgesToVanillaElements', () => {
        it('add when free-bets are available', () => {
            const freeBetsState = { data: [], available: true };
            const oddsboostCounter = [{}];
            fbBadgeLoaderService.addCounter = jasmine.createSpy();
            fbBadgeLoaderService.update = jasmine.createSpy();
            fbBadgeLoaderService.sportsFreebetsCount = jasmine.createSpy();
            fbBadgeLoaderService.addBadgesToVanillaElements(freeBetsState, oddsboostCounter);
            expect(fbBadgeLoaderService.addCounter).toHaveBeenCalledTimes(3);
            expect(fbBadgeLoaderService.update).toHaveBeenCalled();
        });

        it('add sports freebets and oddsboosts counters', () => {
            const freeBetsState = { data: [], available: false };
            const oddsboostCounter = [{}];
            fbBadgeLoaderService.addCounter = jasmine.createSpy('addCounter');
            fbBadgeLoaderService.update = jasmine.createSpy('update');
            fbBadgeLoaderService.sportsFreebetsCount = jasmine.createSpy();
            fbBadgeLoaderService.addBadgesToVanillaElements(freeBetsState, oddsboostCounter);
            expect(fbBadgeLoaderService.addCounter).toHaveBeenCalledTimes(3);
            expect(fbBadgeLoaderService.update).toHaveBeenCalledTimes(2);
        });

        it('not add sports freebets and oddsboosts counters', () => {
            const oddsboostCounter = [{}];
            fbBadgeLoaderService.addCounter = jasmine.createSpy('addCounter');
            fbBadgeLoaderService.update = jasmine.createSpy('update');
            fbBadgeLoaderService.sportsFreebetsCount = jasmine.createSpy().and.returnValue(0);
            fbBadgeLoaderService.addBadgesToVanillaElements(1, oddsboostCounter);
            expect(fbBadgeLoaderService.addCounter).toHaveBeenCalledTimes(3);
            expect(fbBadgeLoaderService.update).toHaveBeenCalledTimes(2);
        });

        it('remove oddsboosts counters', () => {
            const oddsboostCounter = [];
            fbBadgeLoaderService.addCounter = jasmine.createSpy('addCounter');
            fbBadgeLoaderService.update = jasmine.createSpy('update');
            fbBadgeLoaderService.sportsFreebetsCount = jasmine.createSpy().and.returnValue(0);
            fbBadgeLoaderService.addBadgesToVanillaElements(1, oddsboostCounter);
            expect(fbBadgeLoaderService.addCounter).toHaveBeenCalledTimes(3);
            expect(fbBadgeLoaderService.update).toHaveBeenCalledTimes(2);
        });

    });


    it('addOddsBoostCounter', () => {
        const oddsboostCounter = [{}];
        fbBadgeLoaderService.addCounter = jasmine.createSpy('addCounter');
        fbBadgeLoaderService.update = jasmine.createSpy('update');
        fbBadgeLoaderService.addOddsBoostCounter(oddsboostCounter);
        expect(fbBadgeLoaderService.addCounter).toHaveBeenCalled();
        expect(fbBadgeLoaderService.update).toHaveBeenCalled();
    });

});
