import { Injectable } from '@angular/core';
import { MenuCountersService, MenuSection } from '@vanilla/core';
import { FreeBetsBadgeService } from './vanilla-freebets-badge.service';

import { IFreeBetsBadgeModel } from '@vanillaInitModule/models/free-bets.interface';
import { IFreebetToken } from '@bpp/services/bppProviders/bpp-providers.model';
import { IFreeBetState } from '@core/services/freeBets/free-bets.model';

const badgeCount: string = 'FB';
@Injectable({
    providedIn: 'root'
})
export class VanillaFreebetsBadgeDynamicLoaderService {
    headerBadge: IFreeBetsBadgeModel = {
        section: MenuSection.Header,
        item: 'avatar',
        count: null
    };

    menuOffersBadge: IFreeBetsBadgeModel = {
        section: MenuSection.Menu,
        item: 'offers',
        count: null
    };

    menuSportBadge: IFreeBetsBadgeModel = {
        section: MenuSection.Menu,
        item: 'sportsfreebets',
        count: null
    };
    menuOddsBoostBadge: IFreeBetsBadgeModel = {
        section: MenuSection.Menu,
        item: 'oddsboost',
        count: null
    };


    constructor(private menuCountersService: MenuCountersService,
        private freebetsBadgeService: FreeBetsBadgeService,
    ) { }

    /**
     * add badge and counter to vanilla element
     * @param item
     */
    addCounter(item: IFreeBetsBadgeModel): void {
        this.freebetsBadgeService.freeBetCounters.push(item);
    }

    /**
     * count freebets
     * @param freebetArr
     */

    sportsFreebetsCount(freebetArr: IFreebetToken[]): number | string {
        const count = [];
        freebetArr.forEach((element: IFreebetToken) => {
            if (element.freebetTokenType === 'SPORTS') {
                count.push(element);
            }
        });
        return count.length;
    }
    /**
     * update all counters and badge
     */
    update(): void {
        this.menuCountersService.update();
    }
    /**
     * set value to menuSportBadge.count and menuOddsBoostBadge.count and execute updateBadge, updateCounter, update methods
     * @param freeBetsState
     * @param oddsboostCounter
     */
    addBadgesToVanillaElements(freeBetsState: IFreeBetState): void {
        this.menuSportBadge.count = this.sportsFreebetsCount(freeBetsState.data);
        this.updateBadge(freeBetsState.available);
        this.updateCounter(this.menuSportBadge);
    }

    addOddsBoostCounter(oddsboostArr: IFreebetToken[]) {
        this.menuOddsBoostBadge.count = oddsboostArr.length;
        this.updateCounter(this.menuOddsBoostBadge);
    }
    /**
     * Check if fbStatus equal false set null to headerBadge.count and menuOffersBadge.count  and execute addCounter method
     * @param fbStatus
     */
    updateBadge(fbStatus: boolean): void {
        this.headerBadge.count = fbStatus ? badgeCount : null;
        this.menuOffersBadge.count = fbStatus ? badgeCount : null;
        this.addCounter(this.headerBadge);
        this.addCounter(this.menuOffersBadge);
        this.update();
    }

    /**
     * Check if item.count equal zero then set null to item.count and execute addCounter method
     * @param item
     */
    updateCounter(item: IFreeBetsBadgeModel): void {
        item.count = item.count || null;
        this.addCounter(item);
        this.update();
    }
}
