import { Injectable } from '@angular/core';
import { UserService } from '@vanilla/core/core';
import { ProductHomepagesConfig } from '@labelhost/core';
import { UserInterfaceClientConfig } from '@app/client-config/bma-user-interface-config';

@Injectable({
  providedIn: 'root'
})
export class AccountUpgradeLinkService {

  constructor(
    private user: UserService,
    private productHomepagesConfig: ProductHomepagesConfig,
    private userInterfaceConfig: UserInterfaceClientConfig
  ) {}

  get businessPhase(): string {
    return this.user.claims.get('accbusinessphase');
  }

  get inShopToMultiChannelLink(): string {
    return this.productHomepagesConfig.portal + this.userInterfaceConfig.accountUpgradeLink.imc;
  }

  get onlineToMultiChannelLink(): string {
    return this.productHomepagesConfig.portal + this.userInterfaceConfig.accountUpgradeLink.omc;
  }
}
