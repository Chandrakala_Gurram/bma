import { catchError, finalize, first, map, switchMap } from 'rxjs/operators';
import { IAEMCarousel, ICMSBannerConfig, IOffer, IOfferReport, IParams } from '@core/models/aem-banners-section.model';
import { Component, ElementRef, Input, OnDestroy, OnInit } from '@angular/core';
import * as _ from 'underscore';
import { Observable, of, throwError } from 'rxjs';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { DeviceService } from '@core/services/device/device.service';
import { StorageService } from '@core/services/storage/storage.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import environment from '@environment/oxygenEnvConfig';
import { GtmService } from '@core/services/gtm/gtm.service';
import { UserService } from '@core/services/user/user.service';
import { IGtmBannerEvent } from '@core/models/gtm.event.model';
import { BannersService } from '@app/core/services/aemBanners/banners.service';
import { BRANDS_FOR_AEM, DEFAULT_OPTIONS, dynamicBannersPageMap } from '@core/services/aemBanners/utils';
import Utils from '@app/core/services/aemBanners/utils';
import { CarouselService } from '@shared/directives/ng-carousel/carousel.service';
import { Carousel } from '@shared/directives/ng-carousel/carousel.class';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { ISystemConfig } from '@core/services/cms/models';
import { DomToolsService } from '@coreModule/services/domTools/dom.tools.service';
import { BannerClickService } from '@core/services/aemBanners/banner-click.service';
import { RendererService } from '@shared/services/renderer/renderer.service';

@Component({
  selector: 'banners-section',
  templateUrl: 'banners-section.component.html',
  styleUrls: ['styles/banners-section.component.less', 'styles/lc-carousel.less'],
  providers: [ BannersService ]
})
export class BannersSectionComponent implements OnInit, OnDestroy {
  @Input() page: string;

  offers: any;
  brand: string = '';
  isBannerSectionEmpty: boolean = false;
  bannersVisibility: string = 'hidden';
  showBanners: boolean = true;
  carouselName: string = 'aem-banners-carousel';
  isBannersEnabled: boolean = false;
  firstImageLoaded: boolean = false;
  isLoaded: boolean = false;
  isSlided: boolean = false;

  public timePerSlide: number = 7000;

  readonly SWIPER_RESET_TIMEOUT = 3;

  public activeSlideIndex: number = 0;

  public carousel: IAEMCarousel;

  public animatedPictureRegex: RegExp  = /(.+)\/(content)\/(.+)/;

  // GA Tracking properties
  private isGTMSuccess: boolean = false;

  private readonly ASPECT_RATIO: number = 2.67; // banner image aspect ratio = w:660 / h:247

  private readonly DESKTOP_MARGIN_BOTTOM: number = 10; // taken from @componets-edge-gap in objects.banner.less

  private orientationChangeListener: any;

  private cmsBannerConfig: ICMSBannerConfig;

  private element: HTMLElement;

  private reloadTimer: number;

  constructor(
    private windowRef: WindowRefService,
    private pubsub: PubSubService,
    private deviceService: DeviceService,
    private storage: StorageService,
    private gtmService: GtmService,
    private userService: UserService,
    private bannerService: BannersService,
    private rendererService: RendererService,
    private carouselService: CarouselService,
    private cmsService: CmsService,
    private elementRef: ElementRef,
    private domTools: DomToolsService,
    private bannerClickService: BannerClickService
  ) {
    this.brand = Utils.resolveBrandOrDefault(environment.brand);
    this.element = this.elementRef.nativeElement;

    this.handleReload = this.handleReload.bind(this);
  }


  ngOnInit(): void {
    this.cmsService.getSystemConfig()
      .pipe(switchMap((config: ISystemConfig) => {
        if (config && config.DynamicBanners) {
          this.applyConfigFromCMS(config.DynamicBanners);
        }
        if (this.isBannersEnabled) {
          const _options = this.getBannerDynamicParams();
          const settings = Utils.assign({}, DEFAULT_OPTIONS, _options);
          this.carousel = {_options, settings};

          if (!this.firstImageLoaded) {
            this.setBannerSectionHeight();
          }
          this.initOrientationChangeListener();

          return this.requestOffersAndCarouselInit().pipe(finalize(() => {
            this.initPubSubAndSubscriptions();
          }), first());
        } else {
          return of({});
        }
      })).subscribe(() => {}, () => {});
  }

  isIrishUser(): boolean {
    return this.userService.countryCode === 'IE' || this.storage.get('countryCode') === 'IE';
  }

  gotToSlide(index: number): void {
    this.bannersCarousel.toIndex(index);
  }

  applyConfigFromCMS(config: ICMSBannerConfig) {
    this.cmsBannerConfig = config;
    this.isBannersEnabled = this.cmsBannerConfig.enabled;
    if (!this.isBannersEnabled) {
      this.isBannersEnabled = false;
    }

    if (this.cmsBannerConfig.timePerSlide && this.cmsBannerConfig.timePerSlide > 0) {
      // convert seconds to milliseconds
      this.timePerSlide = this.cmsBannerConfig.timePerSlide * 1000;
    }
  }

  ngOnDestroy(): void {
    this.orientationChangeListener && this.orientationChangeListener();
    this.unsubscribeFromEvents();
    this.windowRef.nativeWindow.clearTimeout(this.reloadTimer);
  }

  actionHandler($event: MouseEvent, offer: IOffer): void {
    $event.preventDefault();
    this.trackClickGTMEvent($event);
    this.bannerClickService.handleBannerClick($event, offer, false);
  }

  handleActiveSlide(slideIndex: number): void {
    if (!Number.isInteger(slideIndex)) {
      return;
    }
    if (!this.isSlided) {
      this.isSlided = true;
    }
    const i = slideIndex - 1;

    if (this.offers[i]) {
      this.offers[this.activeSlideIndex].active = false;
      this.offers[i].active = true;
      this.activeSlideIndex = i;
    }
    this.trackBannerView();
  }

  resetSwiper(offers: IOffer[]): void {
    this.findGifsAndLoadInBackground(offers);
    this.offers = offers;

    offers[0].active = true;

    this.activeSlideIndex = 0;
    this.bannersVisibility = 'visible';
    this.trackBannerView();
  }

  findGifsAndLoadInBackground(offers: IOffer[]): void {
    offers.forEach((offer: IOffer) => {
      if (this.animatedPictureRegex.test(offer.imgUrl)) {
        // set plain image until gif will be loaded
        const originalGif = offer.imgUrl;
        const pictureBasedOnGif = originalGif.replace(this.animatedPictureRegex, '$1/image/$3');
        offer.imgUrl = pictureBasedOnGif;

        // load animated image in background
        this.loadImageInBackgroundForOffer(offer, originalGif);
      }
    });
  }

  loadImageInBackgroundForOffer(offer: IOffer, originalGif: string): HTMLImageElement {
    const animatedImage = new Image();
    animatedImage.src = originalGif;
    animatedImage.onload = () => {
      offer.imgUrl = originalGif;
    };
    return animatedImage;
  }

  onFirstImageLoaded(): void {
    this.firstImageLoaded = true;
    this.unSetBannerSectionHeight();
  }

  removeSlideOnError(index: number): void {
    this.offers.splice(index, 1);
  }

  nextSlide(): void {
    this.bannersCarousel.next();
  }

  prevSlide(): void {
    this.bannersCarousel.previous();
  }

  public initPubSubAndSubscriptions(): void {
    this.pubsub.publish(this.pubsub.API.IS_BANNER_SECTION_AVAILABLE, [_.isEmpty(this.offers)]);

    this.pubsub.subscribe('dynamicBanners', this.pubsub.API.RELOAD_COMPONENTS, this.handleReload);
    this.isBannerSectionEmpty = _.isEmpty(this.offers);

    this.subscribeForEvents();
  }

  public requestOffersAndCarouselInit(): Observable<any> {
    return this.bannerService
      .fetchOffersFromAEM(this.carousel.settings).pipe(
      map((response: IOfferReport) => {
        this.showBanners = (response.offers.length > 0);

        if (this.showBanners) {
          this.resetSwiper(response.offers);
        } else {
          this.unSetBannerSectionHeight();
        }
      }),
      catchError(error => {
        this.unSetBannerSectionHeight();
        this.showBanners = false;
        return throwError(error);
      }), finalize(() => this.isLoaded = true));
  }

  public trackBannerView(): void {
    const activeOffer = this.offers[this.activeSlideIndex];

    if (!activeOffer.tracked) {
      this.gtmService.push('trackEvent', {
        eventCategory: 'banner',
        eventAction: 'view',
        eventLabel: activeOffer.title,
        personalised: activeOffer.personalised,
        location: this.windowRef.nativeWindow.location.pathname,
        vipLevel: this.userService.vipLevel || null,
        position: activeOffer.position
      });
      activeOffer.tracked = true;
    }
  }

  /**
   * AEM Banners GA tracking logic by collecting info about slide (offer) clicked
   * @param {MouseEvent} event
   */
  public trackClickGTMEvent(event: MouseEvent): void {
    if (!this.offers.length || this.isGTMSuccess) {
      return;
    }
    const banner = this.offers[this.activeSlideIndex];

    const vipLevel: string = this.userService.vipLevel || null;
    const gtmEvent: IGtmBannerEvent = {
      event: 'trackEvent',
      eventCategory: 'banner',
      eventAction: 'click',
      eventLabel: banner.title,
      personalised: banner.personalised,
      location: this.windowRef.nativeWindow.location.pathname,
      position: (banner.position ? banner.position : (this.activeSlideIndex + 1)),
      vipLevel: vipLevel
    };

    this.gtmService.push('aemBannerClick', gtmEvent);
    // Set true as we need to send GTM one time
    this.isGTMSuccess = true;
  }

  public trackByPosition(offer) {
    return offer.position;
  }

  /*
* Update dynamic banners with new properties
* @param {object} params
* */
  public updateDynamicBanners(): void {
    const options = {
      userType: this.getUserType()
    };

    const vipLevel = this.storage.get('vipLevel');
    if (vipLevel) {
      options['imsLevel'] = vipLevel;
    }

    this.carousel._options = Utils.assign({}, this.carousel._options, options);
    this.carousel.settings = Utils.assign({}, DEFAULT_OPTIONS, this.carousel._options);

    this.requestOffersAndCarouselInit().pipe(first()).subscribe(() => {}, () => {});
  }

  /*
  * Get user type
  * @return {string}
  * */
  private getUserType(): string {
    const user = this.storage.get('existingUser');
    if (!user) {
      return this.brand === BRANDS_FOR_AEM.ladbrokes ? 'new' : 'anonymous';
    } else {
      return this.userService.isRetailUser() ? this.userService.accountBusinessPhase : 'existing';
    }
  }

  /**
   * Set height for banner section to make fixed place for banner.
   */
  private setBannerSectionHeight() {
    /*
    Temporary setting the display to 'block' to be able to get element width;
     */
    this.domTools.css(this.element, 'display', 'block');

    const bannerSectionWidth = this.domTools.getWidth(this.element);
    const viewPortWidth = this.windowRef.nativeWindow.innerWidth;
    const bannerSectionHeight = (viewPortWidth > 1099 && this.deviceService.isDesktop ?
        (bannerSectionWidth / 100 * 60) : bannerSectionWidth) / this.ASPECT_RATIO;
    const bannerSectionTemporaryHeight = !this.deviceService.isDesktop ?
        bannerSectionHeight : bannerSectionHeight + this.DESKTOP_MARGIN_BOTTOM;
    /*
    Temporary setting the height on banners load to prevent banners block leaping on UI.
     */
    this.domTools.css(this.element, 'height', `${(bannerSectionTemporaryHeight)}px`);
  }

  /**
   * Remove temporary styles for banner section
   */
  private unSetBannerSectionHeight() {
    /*
    Removing temporary display and height values to not broke banners and offers styles
     */
    this.domTools.css(this.element, 'display', '');
    this.domTools.css(this.element, 'height', '');
  }

  private initOrientationChangeListener(): void {
    this.orientationChangeListener = this.rendererService.renderer.listen(
      this.windowRef.nativeWindow,
      'orientationchange',
      () => {
        this.showBanners = false;
        this.windowRef.nativeWindow.setTimeout(() => {
          this.showBanners = this.offers.length > 0;
          this.resetSwiper(this.offers.slice());
        }, this.SWIPER_RESET_TIMEOUT);
      }
    );
  }

  /**
   * Generates params needed for dynamic banner
   * @return {object}
   */
  private getBannerDynamicParams(): IParams {
    const baseParams: IParams = {
      locale: this.isIrishUser() && this.brand === BRANDS_FOR_AEM.ladbrokes ? 'en-ie' : 'en-gb',
      brand: this.brand,
      channel: this.getAppType(),
      userType: this.getUserType()
    };
    baseParams.page = dynamicBannersPageMap[this.page] ? dynamicBannersPageMap[this.page] : this.page;

    const vipLevel = this.storage.get('vipLevel');
    // This check is needed for requirement: if no vip level then do not send this key at all
    if (vipLevel) {
      baseParams.imsLevel = vipLevel;
    }

    if (this.deviceService.viewType !== 'desktop') {
      baseParams.device = 'mobile';
    }

    if (this.cmsBannerConfig.maxOffers && this.cmsBannerConfig.maxOffers > 0) {
      baseParams.maxOffers = this.cmsBannerConfig.maxOffers;
    }

    return baseParams;
  }

  /**
   * Subscribes for events (like login, logout) to rerender banner
   * @param {object} params
   */
  private subscribeForEvents(): void {
    this.pubsub.subscribe('dynamicBanners', this.pubsub.API.SESSION_LOGIN, () => {
      if (this.carousel) {
        this.showBanners = false;
        // Timeout added to get ngx-swiper-wrapper ready to update slides
        this.windowRef.nativeWindow.setTimeout(() => {
          this.updateDynamicBanners();
        }, this.SWIPER_RESET_TIMEOUT);
      }
    });
  }

  /**
   * Unsubscribe from events
   */
  private unsubscribeFromEvents(): void {
    this.pubsub.unsubscribe('dynamicBanners');
  }

  /*
  * Get application type
  * @return {string}
  * */
  private getAppType(): string {
    if (this.page === 'retail') {
      return 'connect'; // TODO: rename to retail after changes in cms.
    } else if (this.deviceService.isWrapper) {
      return 'app';
    } else if (this.deviceService.isMobile || this.deviceService.isTablet || this.deviceService.isMobileOrigin) {
      return 'mobile';
    } else if (this.deviceService.isDesktop) {
      return 'mobile';  // temporary added, until we will have 'desktop' channel
    }
    return '';
  }

  private get bannersCarousel(): Carousel {
    return this.carouselService.get(this.carouselName);
  }

  private handleReload(): void {
    this.windowRef.nativeWindow.clearTimeout(this.reloadTimer);
    this.reloadTimer = this.windowRef.nativeWindow.setTimeout(() => {
      this.ngOnDestroy();
      this.ngOnInit();
    }, DEFAULT_OPTIONS.atJsLoadingTimeout);
  }
}
