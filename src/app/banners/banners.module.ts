import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { BannersSectionComponent } from '@app/banners/bannersSection/banners-section.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@sharedModule/shared.module';


@NgModule({
  imports: [
    FormsModule,
    SharedModule
  ],
  declarations: [BannersSectionComponent],
  entryComponents: [BannersSectionComponent],
  exports: [BannersSectionComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class BannersModule { }
