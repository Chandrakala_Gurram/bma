import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ModalModule } from 'angular-custom-modal';
import { SharedModule } from '@sharedModule/shared.module';
import { SessionService } from '@authModule/services/session/session.service';

@NgModule({
  imports: [
    ModalModule,
    SharedModule
  ],
  exports: [
  ],
  declarations: [
  ],
  entryComponents: [
  ],
  providers: [
    SessionService
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AuthModule { }
