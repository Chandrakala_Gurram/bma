import { BehaviorSubject, from, from as observableFrom, Observable, of, throwError } from 'rxjs';
import { catchError, concatMap, first, map, mergeMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { IFreebetToken, IRespAccountValidate } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { ITempTokenResponse } from '@vanillaInitModule/models/temp-token-reponse.interface';
import { TempTokenService } from '@vanillaInitModule/services/tempToken/temp-token.service';
import { ITempToken } from '@authModule/services/auth/auth.model';
import { UserService } from '@core/services/user/user.service';
import { StorageService } from '@coreModule/services/storage/storage.service';
import { DeviceService } from '@coreModule/services/device/device.service';
import { BppAuthService } from '@app/bpp/services/bppProviders/bpp-auth.service';
import { ProxyHeadersService } from '@app/bpp/services/proxyHeaders/proxy-headers.service';
import { SessionService } from '@authModule/services/session/session.service';
import { CommandService } from '@root/app/core/services/communication/command/command.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { CmsService } from '@core/services/cms/cms.service';
import { NewRelicService } from '@core/services/newRelic/new-relic.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  sessionLoggedIn: Observable<null>;
  innerSessionLoggedIn = new BehaviorSubject(null);

  constructor(
    private tempTokenService: TempTokenService,
    private userService: UserService,
    private bppAuthService: BppAuthService,
    private storage: StorageService,
    private proxyHeadersService: ProxyHeadersService,
    private device: DeviceService,
    private sessionService: SessionService,
    private commandService: CommandService,
    private pubsub: PubSubService,
    private cmsService: CmsService,
    private newRelicService: NewRelicService
  ) {
    this.reloginBppToCommand = this.reloginBppToCommand.bind(this);
    this.sessionLoggedIn = this.innerSessionLoggedIn.asObservable();

    this.commandService.register(this.commandService.API.BPP_AUTH_SEQUENCE, this.reloginBppToCommand);
  }

  /**
   * Performs needed operations for BPP authentication:
   * - initializes new promises needed for WhenSesionFactory;
   * - retrieves new temporary token from OpenApi;
   * - perform BPP authentication.
   * @return {Observable}
   */
  bppAuthSequence(): Observable<void> {
    if (this.userService.isInShopUser()) {
      return of();
    }

    this.userService.initProxyAuth();
    return this.getTempToken().pipe(
      concatMap(tokenData => {
        if (tokenData) {
          return this.bppLogin(tokenData);
        }
        return throwError('No Vanilla Temp Token');
      })
    );
  }

  getTempToken(token?: string): Observable<any> {
    return this.tempTokenService.fetchTemporaryToken().pipe(
      map((response: ITempTokenResponse) => {
        const tempToken = response && response.sessionToken;
        if (tempToken) {
          this.newRelicService.addPageAction('authService=>getTempToken=>Success', { tempToken: tempToken.substr(0, 7) });
          return {
            username: this.userService.username,
            tempToken
          };
        }
        this.newRelicService.addPageAction('authService=>getTempToken=>NoResponse', { response });
        console.warn('No Vanilla Temp Token');
        return;
      }),
      catchError(error => {
        this.newRelicService.addPageAction('authService=>getTempToken=>Error', { error });
        console.error('No Vanilla Temp Token');
        return of(error);
      })
    );
  }

  /**
   * Performs authentication to BPP with given "username" and "token".
   * @param {Object} params Needed parameters for BPP authentication: "username" and "token".
   * @return {Observable}
   */
  bppLogin(params: ITempToken): Observable<void> {
    const { tempToken, username } = params;
    const loginParams = { username, token: tempToken, channel: this.device.freeBetChannel };
    this.newRelicService.addPageAction('BPP Login=>Request', { tempToken: tempToken.substr(0, 7) });

    return this.bppAuthService.validate(loginParams)
      .pipe(
        map((res: IRespAccountValidate) => {
          if (res && !res.error) {
            this.newRelicService.addPageAction('BPP Login=>Success', { newBppToken: res.token.substr(0, 7) });
            const privateMarkets = res.privateMarkets ? res.privateMarkets.data : [];

            this.storage.set('previousBppUsername', this.userService.username);
            this.pubsub.publish(this.pubsub.API.STORE_FREEBETS, {...res.freeBets, isPageRefresh: true});

            this.pubsub.publishSync('STORE_PRIVATE_MARKETS', [privateMarkets]);

            this.userService.set({ bppToken: res.token });
            this.proxyHeadersService.generateBppAuthHeaders();
            this.userService.resolveProxyAuth();
          } else {
            this.handleBppLoginError(res);
          }

          return res;
        }),
        mergeMap((bppResponseData: IRespAccountValidate) => this.initOddsBoost(bppResponseData)),
        catchError((error) => {
          return this.handleBppLoginError(error);
        })
      ) as Observable<void>;
  }

  /**
   * Init oddsBoost
   * - with data => after auth/user response
   * - without data => on page refresh, when we need to call allFreebets request
   * @param res
   */
  initOddsBoost(res?: IRespAccountValidate): Observable<void | IFreebetToken[]> {
    return this.cmsService.getOddsBoost().pipe(
      mergeMap(config => {
        if (config.enabled) {
          const tokens = (res && !res.error && res.betBoosts) ? res.betBoosts.data : [];
          if (res) {
            return from(this.commandService.executeAsync(this.commandService.API.ODDS_BOOST_INIT, [tokens]));
          } else {
            return from(this.commandService.executeAsync(this.commandService.API.GET_ODDS_BOOST_TOKENS, [true]));
          }
        } else {
          return of(null);
        }
      })
    );
  }

  reLoginBpp(): Observable<void> {
    if (this.userService.proxyPromiseResolved()) {
      return this.bppAuthSequence();
    } else {
      return observableFrom(this.sessionService.whenProxySession());
    }
  }

  reLoginSequence(credentials, options = {}): Observable<void> {
    return of(null);
  }

  acceptTermsAndConditions(): Observable<void> {
    return of(null);
  }

  handleLogoutNotification(performRefresh: boolean): void {}

  loginSequence(credentials, options = {}, isUpgradedInShopUser = false): Observable<void> {
    return of(null);
  }

  logout(reason: string, showLogoutPopup: boolean = false): Observable<void> {
    return of(null);
  }

  mainInit(): void {}

  private handleBppLoginError(error: IRespAccountValidate): Observable<void> {
    this.userService.rejectProxyAuth();
    this.newRelicService.addPageAction('BPP Login=>Error', typeof error === 'string' ? { error } : error);
    // Proceed with login flow if BPP is unavailable.
    // User should stay logged in OpenAPI if auth to BPP failed.
    return throwError(error);
  }

  private reloginBppToCommand(): Promise<void> {
    return this.reLoginBpp().pipe(first()).toPromise();
  }
}
