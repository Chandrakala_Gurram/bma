export interface IQuickbetRequestModel {
  additional?: {
    scorecastMarketId: number;
  };
  outcomeIds?: number[];
  selectionType?: string;
  gtmTracking?: any;
  token?: string;
}
