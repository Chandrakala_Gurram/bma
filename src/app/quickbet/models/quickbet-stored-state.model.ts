import { IQuickbetFreebetModel } from './quickbet-freebet.model';

export interface IQuickbetStoredStateModel {
  userEachWay?: boolean;
  userStake?: string;
  isLP?: boolean;
  freebet?: IQuickbetFreebetModel;
  isBoostActive?: boolean;
}
