import { from as observableFrom, of as observableOf } from 'rxjs';

import { mergeMap, map } from 'rxjs/operators';
import {
  Component, Input, Output, OnInit, EventEmitter, OnDestroy, ChangeDetectorRef
} from '@angular/core';
import * as _ from 'underscore';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { UserService } from '@core/services/user/user.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { QuickbetService } from '@app/quickbet/services/quickbetService/quickbet.service';
import { QuickbetUpdateService } from '@app/quickbet/services/quickbetUpdateService/quickbet-update.service';
import { QuickbetDepositService } from '@quickbetModule/services/quickbetDepositService/quickbet-deposit.service';
import { FreeBetsService } from '@coreModule/services/freeBets/free-bets.service';
import { QuickbetNotificationService } from '@app/quickbet/services/quickbetNotificationService/quickbet-notification.service';
import { CommandService } from '@core/services/communication/command/command.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { IQuickbetDepositModel } from '@app/quickbet/models/quickbet-deposit.model';
import { IFreebetToken, ITokenPossibleBet } from '@app/bpp/services/bppProviders/bpp-providers.model.ts';
import { IQuickbetSelectionModel } from '@core/models/quickbet-selection.model';
import { IPrice } from '@oddsBoost/components/oddsBoostPrice/odds-boost-price.model';
import { GtmService } from '@core/services/gtm/gtm.service';
import { ILazyComponentOutput } from '@root/app/shared/components/lazy-component/lazy-component.model';
import { WindowRefService } from '@root/app/core/services/windowRef/window-ref.service';

@Component({
  selector: 'quickbet-selection',
  templateUrl: 'quickbet-selection.component.html',
  styleUrls: ['quickbet-selection.component.less']
})

export class QuickbetSelectionComponent implements OnInit, OnDestroy {

  @Input() selection: IQuickbetSelectionModel;
  @Input() placeBetPending: { state: boolean };
  @Input() leftBtnLocale: string;
  @Input() ycOddsValue?: Function;
  @Input() isLogAndQuickBetPending?: boolean;
  @Input() showIFrame: boolean;
  @Input() quickDepositFormExpanded: boolean;

  @Output() readonly placeBetFn: EventEmitter<void> = new EventEmitter();
  @Output() readonly closeFn: EventEmitter<void> = new EventEmitter();
  @Output() readonly addToBetslipFn: EventEmitter<void> = new EventEmitter();
  @Output() readonly openQuickDepositFn: EventEmitter<void> = new EventEmitter();

  quickDeposit: IQuickbetDepositModel;
  selectedFreeBet: IFreebetToken;
  leftBtnText: string;
  freebetsList: IFreebetToken[];

  isBoostEnabled: boolean; // True, if odds boost enabled in cms
  boostOldPrice: IPrice;
  boostNewPrice: IPrice;
  quickStakeVisible: boolean = true;
  readonly promoLabelsFlagsExcluded = 'EVFLAG_EPR,MKTFLAG_EPR';

  protected QB_SELECTION_NAME = 'QuickbetSelectionController';
  protected LOCK_PLACE_BET_TIMEOUT: number = 1500;
  protected hasBeenReloaded: boolean = true;

  constructor(protected pubsub: PubSubService,
              protected user: UserService,
              protected locale: LocaleService,
              protected filtersService: FiltersService,
              protected quickbetDepositService: QuickbetDepositService,
              protected quickbetService: QuickbetService,
              protected quickbetUpdateService: QuickbetUpdateService,
              protected freeBetsFactory: FreeBetsService,
              protected quickbetNotificationService: QuickbetNotificationService,
              protected commandService: CommandService,
              protected cmsService: CmsService,
              protected gtmService: GtmService,
              protected cdr: ChangeDetectorRef,
              protected windowRef: WindowRefService
              ) {
    this.onlineListener = this.onlineListener.bind(this);
    this.offlineListener = this.offlineListener.bind(this);
  }

  ngOnInit(): void {
    this.leftBtnText = this.locale.getString(this.leftBtnLocale || 'quickbet.buttons.addToBetslip');
    this.quickDeposit = this.quickbetDepositService.quickDepositModel;

    this.addEventListeners();

    this.pubsub.subscribe(this.QB_SELECTION_NAME, this.pubsub.API.SUCCESSFUL_LOGIN, () => {
      this.initOddsBoost();
      this.getFreebetsList();
    });

    this.pubsub.subscribe(this.QB_SELECTION_NAME, this.pubsub.API.GET_QUICKBET_SELECTION_STATUS, (...args) => {
      if (!args[0] && this.canBoostSelection) {
        this.initOddsBoost();
      }
    });

    this.pubsub.subscribe(this.QB_SELECTION_NAME, this.pubsub.API.QUICKBET_OPENED, (selectionData: IQuickbetSelectionModel) => {
      if (selectionData && selectionData.freebet) {
        this.onFreebetChange({ output: 'selectedChange', value: selectionData.freebet });
      }
    });

    this.pubsub.subscribe(this.selection.eventId, this.pubsub.API.SET_ODDS_FORMAT, () => this.changeOddsFormat());
    this.getFreebetsList();
    this.quickbetDepositService.update(this.selection.stake, this.selection.isEachWay);

    const disabledMap = {
      event: false,
      market: false,
      selection: false
    };

    if (this.selection.disabled) {
      const placeMap = {
          eventStatusCode: 'event',
          marketStatusCode: 'market',
          outcomeStatusCode: 'selection'
        },
        suspensionMap = [];

      for (const key in this.selection) {
        if (this.selection[key] === 'S') {
          suspensionMap.push(placeMap[key]);
          disabledMap[placeMap[key]] = true;
        }
      }
      const suspensionPlace = (suspensionMap.length > 1 ? 'selection' : suspensionMap[0]);

      this.quickbetNotificationService.saveErrorMessage(
        this.filtersService.getComplexTranslation('quickbet.singleDisabled', '%1', suspensionPlace),
        'warning',
        'bet-status'
      );
    }

    this.quickbetUpdateService.fillDisableMap(disabledMap);
    this.initOddsBoost();
  }

  changeOddsFormat(): void {
    if (this.ycOddsValue) {
      this.selection.price = {
        ...this.selection.price,
        isPriceUp: false,
        isPriceChanged: false,
        isPriceDown: false
      };

      this.selection.oldOddsValue = this.ycOddsValue();
    }
  }

  ngOnDestroy(): void {
    this.pubsub.unsubscribe(this.QB_SELECTION_NAME);
    this.pubsub.unsubscribe(this.selection.eventId);
    this.windowRef.nativeWindow.removeEventListener('online', this.onlineListener);
    this.windowRef.nativeWindow.removeEventListener('offline', this.offlineListener);
  }

  placeBetFnHandler(): void {
    this.hasBeenReloaded && this.placeBetFn.emit();
  }
  closeFnHandler(): void {
    this.closeFn.emit();
  }
  addToBetslipFnHandler(): void {
    this.addToBetslipFn.emit();
  }
  openQuickDepositFnHandler(): void {
    this.openQuickDepositFn.emit();
  }

  priceTypeChange(event: { output: string, value: string }): void {
    this.selection.isLP = event.value === 'LP';
    this.onStakeChange();
  }

  /**
   * Handler for stake model change.
   */
  onStakeChange(): void {
    this.selection.onStakeChange();
    this.quickbetDepositService.update(this.selection.stake, this.selection.isEachWay);

    if (this.quickbetService.selectionData) {
      this.quickbetService.saveQBStateInStorage({
        userStake: this.selection.stake,
        userEachWay: this.selection.isEachWay,
        isLP: this.selection.isLP,
        freebet: this.selection.freebet,
        isBoostActive: this.selection.isBoostActive
      });
    }

    this.handleOddsBoostSP();
    this.isMaxStakeExceeded();
    this.cdr.detectChanges();
  }

  onFreebetChange(event: ILazyComponentOutput): void {
    if (event.output === 'selectedChange') {
      const value = event.value ? event.value.name : event.value;
      this.selectedFreeBet = event.value;
      this.setFreebet(value);
      this.handleOddsBoostFreeBet();
      this.onStakeChange();
      this.quickbetDepositService.update(this.selection.stake, this.selection.isEachWay);
    }
  }

  /**
   * Set frebet model
   * @param {string} freebetValue
   */
  setFreebet(freebetValue: string): void {
    const freebetObj: IFreebetToken = _.find(this.freebetsList, freebet => freebet.name === freebetValue);
    const value = freebetObj && freebetObj.freebetTokenValue && freebetObj.freebetTokenValue.match(/[0-9.]/g).join('');

    this.selection.freebetValue = parseFloat(value) || 0;
    this.selection.freebet = freebetObj;
    this.selection.onStakeChange();
  }

  /**
   * checks if spinner on quick deposit button should be displayed
   */
  showSpinnerOnQuickDeposit(): boolean {
    return this.isIFrameLoadingInProgress() || this.placeBetPending.state;
  }

  /**
   * Check if can click the button
   * @returns {boolean}
   */
  isSelectionDisabled(): boolean {
    return !this.selection.stake || this.selection.disabled || this.placeBetPending.state
      || this.isIFrameLoadingInProgress();
  }

  /**
   * Used in view to check if stake is typed
   * @returns {boolean}
   */
  isMakeQuickDeposit(): boolean {
    if (this.placeBetPending.state) {
      return false;
    }
    return this.user.status && (!!this.quickDeposit.neededAmountForPlaceBet ||
      (!Number(this.user.sportBalance) && !this.selection.freebetValue));
  }

  /**
   * Used in view to check if stake is typed
   * @returns {boolean}
   */
  isInputFilled(): boolean {
    return !_.isNaN(parseFloat(this.selection.stake));
  }

  /**
   * Show spinner when place bet process is in progress
   * @returns {boolean}
   */
  showPendingSpinner(): boolean {
    return this.placeBetPending.state;
  }

  /**
   * Filter outcome names
   * @param {string} name
   * @returns {string}
   */
  filterPlayerName(name: string): string {
    return this.filtersService.filterPlayerName(name);
  }

  filterAddScore(marketName: string, outcomeName: string) {
    return this.filtersService.filterAddScore(marketName, outcomeName);
  }

  onBoostClick(): void {
    if (this.placeBetPending.state) {
      return;
    }

    if (!this.selection.isLP) {
      this.commandService.execute(this.commandService.API.ODDS_BOOST_SHOW_SP_DIALOG);
      return;
    }

    if (this.selection.freebet) {
      this.commandService.execute(this.commandService.API.ODDS_BOOST_SHOW_FB_DIALOG, [false]);
      return;
    }

    if (this.selection.reboost) {
      this.reboostSelection();
    } else {
      const isActive = !this.selection.isBoostActive;
      this.pubsub.publish(this.pubsub.API.ODDS_BOOST_CHANGE, isActive);
      this.pubsub.publish(this.pubsub.API.ODDS_BOOST_SEND_GTM, { origin: 'quickbet', state: isActive });
    }
  }

  onQuickStakeSelect(value: string): void {
    const intVal = parseInt(value, 10);
    const intStake = parseFloat(this.selection.stake) || 0;

    this.selection.stake = (intStake + intVal).toFixed(2);
    this.cdr.detectChanges();
    this.onStakeChange();
    this.pubsub.publish(this.pubsub.API.QB_QUICKSTAKE_PRESSED, [this.selection.stake]);

    this.gtmService.push('trackEvent', {
      event: 'trackEvent',
      eventCategory: 'quickbet',
      eventAction: 'quick stake',
      eventLabel: value
    });
  }

  onKeyboardToggle(status: boolean): void {
    this.quickStakeVisible = status;
    this.cdr.detectChanges();
  }

  get canBoostSelection(): boolean {
    return (
      this.isBoostEnabled &&
      !this.selection.disabled &&
      !this.selection.hasSP &&
      !!this.selection.oddsBoost
    );
  }

  get selectionAmountClasses(): {[key: string]: boolean} {
    return {'filled-input': this.isInputFilled(), [`currency-${this.selection.currency}`]: true};
  }

  /**
   * Check if user is logged in
   * @returns {boolean}
   */
  get isUserLoggedIn(): boolean {
    return this.user.status;
  }

  /**
   * Disable Place Bet button when stake or freebet value are empty or event is suspended
   * @returns {boolean}
   */
  get disablePlaceBet(): boolean {
    return (!parseFloat(this.selection.stake) && !this.selection.freebet) || this.selection.disabled;
  }

  get getPlaceBetText(): string {
    return this.selection.price && this.selection.price.isPriceChanged
    || this.quickbetUpdateService.isHandicapChanged(this.selection) ?
      'quickbet.buttons.acceptPlaceBet' :
      'quickbet.buttons.placeBet';
  }

  /**
   * Retrieves freebets list.
   * @protected
   */
  protected getFreebetsList(): void {
    // Free bets should not be visible for anonymous user or for Connect app
    this.freebetsList = this.user.status ? this.mapFreebets(this.selection) : [];
    this.freebetsList.sort((a, b) => Number(a.freebetTokenValue) -  Number(b.freebetTokenValue));
  }

  /**
   * checks if iframe loading is in progress
   */
  private isIFrameLoadingInProgress(): boolean {
    return !this.showIFrame && this.quickDepositFormExpanded;
  }

  private initOddsBoost(): void {
    if (!this.user.status) { return; }

    this.cmsService.getOddsBoost().pipe(
      map(config => config.enabled),
      mergeMap(enabled => {
        if (!enabled) {
          return observableOf([false, false]);
        } else {
          return observableFrom(
            this.commandService.executeAsync(this.commandService.API.GET_ODDS_BOOST_ACTIVE)
          ).pipe(map(active => [enabled, active]));
        }
      }))
      .subscribe((data: [boolean, boolean]) => {
        [this.isBoostEnabled, this.selection.isBoostActive] = data;

        if (!this.isBoostEnabled) {
          return;
        }

        this.selection.onStakeChange();
        if (this.selection.oddsBoost) {
          this.commandService.execute(this.commandService.API.ODDS_BOOST_SET_MAX_VAL, [this.selection.oddsBoost.betBoostMaxStake]);
        }

        if (this.canBoostSelection) {
          this.boostOldPrice = this.commandService.execute(this.commandService.API.ODDS_BOOST_OLD_QB_PRICE, [this.selection]);
          this.boostNewPrice = this.commandService.execute(this.commandService.API.ODDS_BOOST_NEW_QB_PRICE, [this.selection]);
        }

        this.pubsub.subscribe(this.QB_SELECTION_NAME, this.pubsub.API.ODDS_BOOST_CHANGE, (active: boolean) => {
          this.selection.isBoostActive = active;
          this.selection.reboost = false;
          if (this.selection.isBoostActive && this.selection.price && this.selection.price.isPriceChanged) {
            this.reboostSelection();
          }
          if (!this.selection.disabled) {
            this.onStakeChange();
          }
        });

        this.pubsub.subscribe(this.QB_SELECTION_NAME, this.pubsub.API.ODDS_BOOST_UNSET_FREEBETS, () => {
          this.selectedFreeBet = null;
          this.setFreebet(null);
        });
      });
  }

  private isMaxStakeExceeded(): void {
    if (this.selection.isBoostActive) {
      const stake = +this.selection.stake;
      const totalStake = this.selection.isEachWay ? stake * 2 : stake;
      this.commandService.execute(this.commandService.API.ODDS_BOOST_MAX_STAKE_EXCEEDED, [totalStake]);
    }
  }

  private handleOddsBoostSP(): void {
    if (
      this.isBoostEnabled && this.selection.isBoostActive && !this.selection.isLP
    ) {
      this.commandService.execute(this.commandService.API.ODDS_BOOST_SHOW_SP_DIALOG);
    }
  }

  private handleOddsBoostFreeBet(): void {
    if (
      this.isBoostEnabled && this.selection.isBoostActive && this.selection.freebet
    ) {
      this.commandService.execute(this.commandService.API.ODDS_BOOST_SHOW_FB_DIALOG, [true]);
    }
  }

  /**
   * Form list of freebets which are available for selection
   * @param {IQuickbetSelectionModel} selection
   * @return {Array}
   * @private
   */
  private mapFreebets(selection: IQuickbetSelectionModel): IFreebetToken[] {

    const freeBetsState = this.freeBetsFactory.getFreeBetsState();

    return _.filter(freeBetsState.data, freebet => {
      const amount = _.isNaN(+freebet.freebetTokenValue) ? freebet.freebetTokenValue
        : this.filtersService.setCurrency(freebet.freebetTokenValue, this.user.currencySymbol);
      const tokenPossibleBets = freebet.tokenPossibleBets ||
        (freebet.tokenPossibleBet ? [freebet.tokenPossibleBet] : []);
      const tokenPossibleBetName = freebet.tokenPossibleBet ? freebet.tokenPossibleBet.name : '';

      freebet.name = `${amount} ${freebet.freebetOfferName} (${tokenPossibleBetName})`;

      return this.isQualifiedFreebet(selection, tokenPossibleBets);
    });
  }

  /**
   * Checks if freebet has different redemptions and at least one of them matches selection channel.
   * @param {Object} selection
   * @param {Array} tokenPossibleBets
   * @return {boolean}
   * @private
   */
  private isQualifiedFreebet(selection: IQuickbetSelectionModel, tokenPossibleBets: ITokenPossibleBet[]): boolean {
    return _.some(tokenPossibleBets, tokenPossibleBet => {
      return Number({
        SELECTION: selection.outcomeId,
        MARKET: selection.marketId,
        EVENT: selection.eventId,
        TYPE: selection.typeId,
        CLASS: selection.classId,
        CATEGORY: selection.categoryId,
        ANY: tokenPossibleBet.betId
      }[tokenPossibleBet.betLevel]) === Number(tokenPossibleBet.betId);
    });
  }

  private addEventListeners(): void {
    this.windowRef.nativeWindow.addEventListener('online', this.onlineListener);
    this.windowRef.nativeWindow.addEventListener('offline', this.offlineListener);
  }

  private onlineListener(): void {
    this.windowRef.nativeWindow.setTimeout(() => {
      this.hasBeenReloaded = true;
    }, this.LOCK_PLACE_BET_TIMEOUT);
  }

  private offlineListener(): void {
    this.hasBeenReloaded = false;
  }

  private reboostSelection() {
    this.pubsub.publish(this.pubsub.API.REUSE_QUICKBET_SELECTION, this.selection.requestData);
    this.pubsub.publish(this.pubsub.API.ODDS_BOOST_SEND_GTM, { origin: 'quickbet', state: true });
  }
}
