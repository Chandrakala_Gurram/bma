import { fakeAsync, tick } from '@angular/core/testing';
import { of as observableOf } from 'rxjs';
import { commandApi } from '@app/core/services/communication/command/command-api.constant';
import { pubSubApi } from '@app/core/services/communication/pubsub/pubsub-api.constant';
import { QuickbetSelectionComponent } from './quickbet-selection.component';

describe('QuickbetSelectionComponent', () => {
  let pubSubService;
  let userService;
  let localeService;
  let filtersService;
  let quickbetDepositService;
  let quickbetService;
  let quickbetUpdateService;
  let freeBetsService;
  let quickbetNotificationService;
  let commandService;
  let cmsService;
  let component: QuickbetSelectionComponent;
  let loginCb;
  let gtmService;
  let windowRef;
  let cdr;

  beforeEach(() => {
    pubSubService = {
      API: pubSubApi,
      publish: jasmine.createSpy(),
      subscribe: jasmine.createSpy().and.callFake((file, method, callback) => {
          if (method === 'SET_ODDS_FORMAT') {
            callback('frac');
          } else if (method === 'SUCCESSFUL_LOGIN') {
            loginCb = callback;
          }  else if (method === 'ODDS_BOOST_CHANGE') {
            callback(true);
          } else if (method === 'QUICKBET_OPENED') {
            callback({
              freebet: {
                name: 'freebet'
              }
            });
          } else {
            callback();
          }
      }),
      unsubscribe: jasmine.createSpy()
    };
    userService = {
      status: true,
      currencySymbol: '$',
      isInShopUser: jasmine.createSpy('isInShopUser')
    };
    localeService = {
      getString: jasmine.createSpy().and.returnValue('test_str')
    };
    filtersService = {
      getComplexTranslation: jasmine.createSpy().and.returnValue(jasmine.any(String)),
      filterPlayerName: jasmine.createSpy('filterPlayerName'),
      filterAddScore: jasmine.createSpy('filterAddScore'),
      setCurrency: jasmine.createSpy('setCurrency')
    };
    quickbetDepositService = {
      update: jasmine.createSpy()
    };
    quickbetService = {
      saveQBStateInStorage: jasmine.createSpy()
    };
    quickbetUpdateService = {
      fillDisableMap: jasmine.createSpy(),
      isHandicapChanged: jasmine.createSpy('isHandicapChanged')
    };
    freeBetsService = {
      getFreeBetsState: jasmine.createSpy('getFreeBetsState').and.returnValue({
        data: [{ freebetTokenValue: '14', tokenPossibleBets: [] }],
        available: true
      })
    };
    quickbetNotificationService = {
      saveErrorMessage: jasmine.createSpy()
    };
    commandService = {
      API: commandApi,
      execute: jasmine.createSpy(),
      executeAsync: jasmine.createSpy('executeAsync').and.returnValue(() => observableOf({}))
    };
    cmsService = {
      getOddsBoost: jasmine.createSpy().and.returnValue(observableOf({}))
    };
    gtmService = {
      push: jasmine.createSpy('push')
    };
    cdr = {
      detectChanges: jasmine.createSpy('detectChanges')
    };
    windowRef = {
      nativeWindow: {
        addEventListener: jasmine.createSpy('addEventListener'),
        removeEventListener: jasmine.createSpy('removeEventListener'),
        setTimeout: jasmine.createSpy('setTimeout')
      }
    };
    windowRef = {
      nativeWindow: {
        addEventListener: jasmine.createSpy('addEventListener'),
        removeEventListener: jasmine.createSpy('removeEventListener'),
        setTimeout: jasmine.createSpy('setTimeout')
      }
    };

    component = new QuickbetSelectionComponent(pubSubService, userService, localeService, filtersService,
      quickbetDepositService, quickbetService, quickbetUpdateService, freeBetsService, quickbetNotificationService, commandService,
      cmsService, gtmService, cdr, windowRef);
  });

  describe('ngOnInit', () => {
    it('ngOnInit', () => {
      component.selection = {
        eventId: 345,
        disabled: true,
        newOddsValue: '11/4',
        oldOddsValue: '14/5',
        isEachWay: true
      } as any;
      component['getFreebetsList'] = jasmine.createSpy();
      component['initOddsBoost'] = jasmine.createSpy();
      component['onFreebetChange'] = jasmine.createSpy();

      component.ngOnInit();
      loginCb('quickbet');

      expect(localeService.getString).toHaveBeenCalledTimes(1);
      expect(pubSubService.subscribe).toHaveBeenCalledTimes(4);
      expect(pubSubService.subscribe).toHaveBeenCalledWith(
          'QuickbetSelectionController',
          pubSubService.API.SUCCESSFUL_LOGIN,
          jasmine.any(Function)
      );
      expect(pubSubService.subscribe).toHaveBeenCalledWith(
        'QuickbetSelectionController',
        pubSubService.API.QUICKBET_OPENED,
        jasmine.any(Function)
      );
      expect(pubSubService.subscribe).toHaveBeenCalledWith(345, 'SET_ODDS_FORMAT', jasmine.any(Function));
      expect(component['getFreebetsList']).toHaveBeenCalledTimes(2);
      expect(quickbetUpdateService.fillDisableMap).toHaveBeenCalledWith(jasmine.any(Object));
      expect(filtersService.getComplexTranslation).toHaveBeenCalledWith(
        'quickbet.singleDisabled',
        '%1',
        undefined
      );
      expect(quickbetNotificationService.saveErrorMessage).toHaveBeenCalledWith(
        jasmine.any(String),
        'warning',
        'bet-status'
      );
      expect(component['onFreebetChange']).toHaveBeenCalledWith({
        output: 'selectedChange', value: {
          name: 'freebet'
        } });
      expect(quickbetDepositService.update)
        .toHaveBeenCalledWith(component.selection.stake, component.selection.isEachWay);
    });

    it('should call addEventListeners method', () => {
      component.selection = {
        eventId: 345,
        disabled: true,
        newOddsValue: '11/4',
        oldOddsValue: '14/5',
        isEachWay: true
      } as any;
      component['getFreebetsList'] = jasmine.createSpy();
      component['initOddsBoost'] = jasmine.createSpy();
      component['onFreebetChange'] = jasmine.createSpy();

      component.ngOnInit();

      expect(windowRef.nativeWindow.addEventListener).toHaveBeenCalledTimes(2);
      expect(windowRef.nativeWindow.addEventListener).toHaveBeenCalledWith('online', jasmine.any(Function));
      expect(windowRef.nativeWindow.addEventListener).toHaveBeenCalledWith('offline', jasmine.any(Function));
    });

    it('should call addEventListeners method', () => {
      component.selection = {
        eventId: 345,
        disabled: true,
        newOddsValue: '11/4',
        oldOddsValue: '14/5',
        isEachWay: true
      } as any;
      component['getFreebetsList'] = jasmine.createSpy();
      component['initOddsBoost'] = jasmine.createSpy();
      component['onFreebetChange'] = jasmine.createSpy();

      component.ngOnInit();

      expect(windowRef.nativeWindow.addEventListener).toHaveBeenCalledTimes(2);
      expect(windowRef.nativeWindow.addEventListener).toHaveBeenCalledWith('online', jasmine.any(Function));
      expect(windowRef.nativeWindow.addEventListener).toHaveBeenCalledWith('offline', jasmine.any(Function));
    });

    it('ngOnInit with selection disabled', () => {
      component.selection = {
        disabled: false,
        newOddsValue: '11/4',
        oldOddsValue: '14/5',
        hasSP: false,
        oddsBoost: {},
      } as any;
      component.isBoostEnabled = true;

      component['getFreebetsList'] = jasmine.createSpy();
      component['initOddsBoost'] = jasmine.createSpy();
      component['onFreebetChange'] = jasmine.createSpy();

      component.ngOnInit();
      loginCb('quickbet');

      expect(pubSubService.subscribe).toHaveBeenCalledWith(
        'QuickbetSelectionController',
        pubSubApi.GET_QUICKBET_SELECTION_STATUS,
        jasmine.any(Function)
      );

      expect(localeService.getString).toHaveBeenCalledTimes(1);
      expect(pubSubService.subscribe).toHaveBeenCalledWith(
          'QuickbetSelectionController',
          pubSubService.API.SUCCESSFUL_LOGIN,
          jasmine.any(Function)
      );
      expect(component['getFreebetsList']).toHaveBeenCalledTimes(2);
      expect(component['initOddsBoost']).toHaveBeenCalledTimes(3);
      expect(quickbetUpdateService.fillDisableMap).toHaveBeenCalledWith({
        event: false,
        market: false,
        selection: false
      });
    });


    it('ngOnInit else cases', () => {
      component.selection = {
        eventStatusCode: 'S',
        isYourCallBet: false,
        disabled: true
      } as any;
      component['getFreebetsList'] = jasmine.createSpy();
      component['initOddsBoost'] = jasmine.createSpy();
      component['onFreebetChange'] = jasmine.createSpy();

      component.ngOnInit();

      expect(localeService.getString).toHaveBeenCalledTimes(1);
      expect(pubSubService.subscribe).toHaveBeenCalledWith(
          'QuickbetSelectionController',
          pubSubService.API.SUCCESSFUL_LOGIN,
          jasmine.any(Function)
      );
      expect(component['getFreebetsList']).toHaveBeenCalled();
      expect(quickbetUpdateService.fillDisableMap).toHaveBeenCalledWith({
        event: true,
        market: false,
        selection: false
      });
      expect(filtersService.getComplexTranslation).toHaveBeenCalledWith(
        'quickbet.singleDisabled', '%1', jasmine.any(String)
      );
      expect(quickbetNotificationService.saveErrorMessage).toHaveBeenCalledWith(
        jasmine.any(String),
        'warning',
        'bet-status'
      );
      expect(component['initOddsBoost']).toHaveBeenCalled();
    });

    it ('should reload freebets list on successful login', () => {
      component.selection = {
        disabled: true,
        newOddsValue: '11/4',
        oldOddsValue: '14/5',
        isYourCallBet: true,
        eventStatusCode: 'S'
      } as any;
      spyOn<any>(component, 'getFreebetsList');
      component['onFreebetChange'] = jasmine.createSpy();
      component['initOddsBoost'] = jasmine.createSpy();

      component.ngOnInit();
      loginCb('test');

      expect(component['getFreebetsList']).toHaveBeenCalledTimes(2);
      expect(component['initOddsBoost']).toHaveBeenCalled();
    });

    it('ngOnInit (false)', () => {
      component.selection = {
        disabled: true,
        newOddsValue: '11/4',
        oldOddsValue: '14/5',
        isYourCallBet: true,
        eventStatusCode: 'S'
      } as any;
      component['getFreebetsList'] = jasmine.createSpy('getFreebetsList');
      component['initOddsBoost'] = jasmine.createSpy();
      component['onFreebetChange'] = jasmine.createSpy();

      component.ngOnInit();
      loginCb('test');

      expect(localeService.getString).toHaveBeenCalledTimes(1);
      expect(component['getFreebetsList']).toHaveBeenCalledTimes(2);
      expect(quickbetUpdateService.fillDisableMap).toHaveBeenCalledWith(jasmine.any(Object));
      expect(filtersService.getComplexTranslation).toHaveBeenCalledWith(
        'quickbet.singleDisabled',
        '%1',
        'event'
      );
    });

    it('ngOnInit (false) (suspensionMap)', () => {
      component.selection = {
        disabled: true,
        newOddsValue: '11/4',
        oldOddsValue: '14/5',
        isYourCallBet: true,
        eventStatusCode: 'S',
        marketStatusCode: 'S',
      } as any;
      component['getFreebetsList'] = jasmine.createSpy('getFreebetsList');
      component['initOddsBoost'] = jasmine.createSpy();
      component['onFreebetChange'] = jasmine.createSpy();

      component.ngOnInit();
      loginCb('test');

      expect(localeService.getString).toHaveBeenCalledTimes(1);
      expect(component['getFreebetsList']).toHaveBeenCalledTimes(2);
      expect(quickbetUpdateService.fillDisableMap).toHaveBeenCalledWith({
        event: true,
        market: true,
        selection: false
      });
      expect(filtersService.getComplexTranslation).toHaveBeenCalledWith(
        'quickbet.singleDisabled',
        '%1',
        'selection'
      );
    });

    it('ngOnInit no freebets in selectionData', () => {
      pubSubService.subscribe.and.callFake((file, method, callback) => {
        if (method === 'SET_ODDS_FORMAT') {
          callback('frac');
        } else if (method === 'SUCCESSFUL_LOGIN') {
          loginCb = callback;
        } else if (method === 'QUICKBET_OPENED') {
          callback({});
        } else {
          callback();
        }
      });
      component.selection = {
        disabled: true,
        newOddsValue: '11/4',
        oldOddsValue: '14/5',
        isYourCallBet: true,
        eventStatusCode: 'S',
        marketStatusCode: 'S',
      } as any;
      component['getFreebetsList'] = jasmine.createSpy('getFreebetsList');
      component['initOddsBoost'] = jasmine.createSpy();
      component['onFreebetChange'] = jasmine.createSpy();

      component.ngOnInit();
      expect(component['onFreebetChange']).not.toHaveBeenCalled();
    });

    it('ngOnInit no selectionData', () => {
      pubSubService.subscribe.and.callFake((file, method, callback) => {
          if (method === 'SET_ODDS_FORMAT') {
            callback('frac');
          } else if (method === 'SUCCESSFUL_LOGIN') {
            loginCb = callback;
          } else if (method === 'QUICKBET_OPENED') {
            callback(null);
          } else {
            callback();
          }
        });
      component.selection = {
        disabled: true,
        newOddsValue: '11/4',
        oldOddsValue: '14/5',
        isYourCallBet: true,
        eventStatusCode: 'S',
        marketStatusCode: 'S',
      } as any;
      component['getFreebetsList'] = jasmine.createSpy('getFreebetsList');
      component['initOddsBoost'] = jasmine.createSpy();
      component['onFreebetChange'] = jasmine.createSpy();

      component.ngOnInit();
      expect(component['onFreebetChange']).not.toHaveBeenCalled();
    });
  });

  it('get canBoostSelection', () => {
    component.isBoostEnabled = true;
    component.selection = {
      disabled: false,
      hasSP: false,
      oddsBoost: {}
    } as any;
    expect(component.canBoostSelection).toBeTruthy();
  });

  it('ngOnDestroy', () => {
    component.selection = { eventId: '1' } as any;
    component.ngOnDestroy();
    expect(pubSubService.unsubscribe).toHaveBeenCalledWith('QuickbetSelectionController');
    expect(pubSubService.unsubscribe).toHaveBeenCalledWith('1');
    expect(windowRef.nativeWindow.removeEventListener).toHaveBeenCalledTimes(2);
    expect(windowRef.nativeWindow.removeEventListener).toHaveBeenCalledWith('online', jasmine.any(Function));
    expect(windowRef.nativeWindow.removeEventListener).toHaveBeenCalledWith('offline', jasmine.any(Function));
  });

  it('onStakeChange', () => {
    component['handleOddsBoostSP'] = jasmine.createSpy();
    component.selection = {
      stake: '19',
      isEachWay: false,
      isLP: true,
      hasSPLP: true,
      price: {},
      onStakeChange: jasmine.createSpy()
    } as any;
    quickbetService.selectionData = {};

    component.onStakeChange();

    expect(component.selection.onStakeChange).toHaveBeenCalled();
    expect(quickbetDepositService.update).toHaveBeenCalledWith(
      component.selection.stake,
      component.selection.isEachWay
    );
    expect(quickbetService.saveQBStateInStorage).toHaveBeenCalledWith({
      userStake: component.selection.stake,
      userEachWay: component.selection.isEachWay,
      isLP: component.selection.isLP,
      freebet: undefined,
      isBoostActive: undefined
    });
    expect(component['handleOddsBoostSP']).toHaveBeenCalled();
  });

  it('onStakeChange (max stake)', () => {
    component.selection = {
      stake: '10',
      isBoostActive: true,
      onStakeChange: jasmine.createSpy()
    } as any;

    component.onStakeChange();
    expect(commandService.execute).toHaveBeenCalledWith(
      commandService.API.ODDS_BOOST_MAX_STAKE_EXCEEDED,
      [10]
    );
  });

  it('getFreebetsList (userService.status = true', () => {
    component['mapFreebets'] = jasmine.createSpy().and.returnValue(['test']);
    userService.status = true;
    component['getFreebetsList']();
    expect(component.freebetsList as any).toEqual(['test']);
  });

  it('getFreebetsList (userService.status = false', () => {
    userService.status = false;
    component['getFreebetsList']();
    expect(component.freebetsList as any).toEqual([]);
  });

  it('onFreebetChange', () => {
    component.selection = {
      stake: null,
      isEachWay: false
    } as any;
    component.setFreebet = jasmine.createSpy();
    component['handleOddsBoostFreeBet'] = jasmine.createSpy();
    component['onStakeChange'] = jasmine.createSpy();

    component.onFreebetChange({
      output: 'selectedChange',
      value: null
    });

    expect(component.setFreebet).toHaveBeenCalledWith(null);
    expect(quickbetDepositService.update).toHaveBeenCalledWith(null, false);
    expect(component['handleOddsBoostFreeBet']).toHaveBeenCalled();
    expect(component['onStakeChange']).toHaveBeenCalled();
  });

  it('setFreebet', () => {
    component.selection = {
      onStakeChange: jasmine.createSpy()
    } as any;
    component.freebetsList = [
      {
        name: 'Weekly freebet 9.99$',
        freebetTokenValue: '9.99$'
      }
    ] as any[];

    component.setFreebet(component.freebetsList[0].name);

    expect(component.selection.freebetValue).toBe(9.99);
    expect(component.selection.freebet).toBe(component.freebetsList[0]);
    expect(component.selection.onStakeChange).toHaveBeenCalled();
  });

  it('setFreebet ("")', () => {
    component.selection = {
      onStakeChange: jasmine.createSpy()
    } as any;
    component.freebetsList = [
      {
        name: 'Weekly freebet 9.99$',
        freebetTokenValue: '9.99$'
      }
    ] as any[];

    component.setFreebet('');

    expect(component.selection.freebetValue).toBe(0);
    expect(component.selection.freebet).toBe(undefined);
    expect(component.selection.onStakeChange).toHaveBeenCalled();
  });

  describe('onBoostClick', () => {
    beforeEach(() => {
      component.placeBetPending = {} as any;
    });

    it('not LP', () => {
      component.selection = { isLP: false } as any;
      component.onBoostClick();
      expect(commandService.execute).toHaveBeenCalledWith(commandApi.ODDS_BOOST_SHOW_SP_DIALOG);
    });

    it('freebet active', () => {
      component.selection = { isLP: true, freebet: {} } as any;
      component.onBoostClick();
      expect(commandService.execute).toHaveBeenCalledWith(commandApi.ODDS_BOOST_SHOW_FB_DIALOG, [false]);
    });

    it('reboost active', () => {
      component.selection = { isLP: true, reboost: true, requestData: {} } as any;

      component.onBoostClick();

      expect(pubSubService.publish).toHaveBeenCalledWith(
        pubSubService.API.REUSE_QUICKBET_SELECTION,
        component.selection.requestData
      );
      expect(pubSubService.publish).toHaveBeenCalledWith(
        pubSubService.API.ODDS_BOOST_SEND_GTM, { origin: 'quickbet', state: true }
      );
    });

    it('boost available', () => {
      component.selection = { isLP: true, freebet: null, isBoostActive: true } as any;

      component.onBoostClick();

      expect(pubSubService.publish).toHaveBeenCalledWith(pubSubService.API.ODDS_BOOST_CHANGE, false);
      expect(pubSubService.publish).toHaveBeenCalledWith(
        pubSubService.API.ODDS_BOOST_SEND_GTM, { origin: 'quickbet', state: false }
      );
    });

    it('place bets pending', () => {
      component.placeBetPending.state = true;
      component.onBoostClick();
      expect(pubSubService.publish).not.toHaveBeenCalled();
    });
  });

  it('initOddsBoost (logged out)', () => {
    component['user'] = { status: false } as any;
    component['initOddsBoost']();
    expect(cmsService.getOddsBoost).not.toHaveBeenCalledWith();
  });

  it('initOddsBoost (logged in, boost disabled)', fakeAsync(() => {
    component['user'] = { status: true } as any;
    component.selection = {} as any;
    cmsService.getOddsBoost.and.returnValue(observableOf({ enabled: false }));

    component['initOddsBoost']();
    tick();

    expect(cmsService.getOddsBoost).toHaveBeenCalled();
    expect(commandService.execute).not.toHaveBeenCalledWith(commandApi.GET_ODDS_BOOST_ACTIVE);
  }));

  it('initOddsBoost (logged in, boost enabled)', fakeAsync(() => {
    component['user'] = { status: true } as any;
    component.selection = {
      onStakeChange: jasmine.createSpy(),
      oddsBoost: {
        betBoostMaxStake: '10'
      }
    } as any;
    pubSubService.subscribe.and.callFake((name, api, cb) => { cb(); } );
    component.isBoostEnabled = true;
    cmsService.getOddsBoost.and.returnValue(observableOf({ enabled: true }));
    commandService.executeAsync.and.returnValue(observableOf(true));
    component.onStakeChange = jasmine.createSpy('onStakeChange');
    component.setFreebet = jasmine.createSpy('setFreebet');

    component['initOddsBoost']();
    tick();

    expect(cmsService.getOddsBoost).toHaveBeenCalled();
    expect(commandService.executeAsync).toHaveBeenCalledWith(commandApi.GET_ODDS_BOOST_ACTIVE);
    expect(commandService.execute).toHaveBeenCalledWith(commandApi.ODDS_BOOST_SET_MAX_VAL, [
      component.selection.oddsBoost.betBoostMaxStake
    ]);

    expect(commandService.execute).toHaveBeenCalledWith(commandApi.ODDS_BOOST_OLD_QB_PRICE, [
      component.selection
    ]);
    expect(commandService.execute).toHaveBeenCalledWith(commandApi.ODDS_BOOST_NEW_QB_PRICE, [
      component.selection
    ]);
    expect(pubSubService.subscribe).toHaveBeenCalledWith(
      'QuickbetSelectionController',
      pubSubApi.ODDS_BOOST_CHANGE,
      jasmine.any(Function)
    );
    expect(component.selection.reboost).toBeFalsy();
    expect(component.selection.onStakeChange).toHaveBeenCalledTimes(1);

    expect(pubSubService.subscribe).toHaveBeenCalledWith(
      'QuickbetSelectionController',
      pubSubApi.ODDS_BOOST_UNSET_FREEBETS,
      jasmine.any(Function)
    );

    expect(component.selectedFreeBet).toEqual(null);
    expect(component.onStakeChange).toHaveBeenCalled();
  }));

  it('initOddsBoost (logged in, boost enabled, oddsBoost = undefined)', fakeAsync(() => {
    component['user'] = { status: true } as any;
    component.selection = {
      onStakeChange: jasmine.createSpy()
    } as any;
    component.isBoostEnabled = true;
    cmsService.getOddsBoost.and.returnValue(observableOf({ enabled: true }));
    commandService.executeAsync.and.returnValue(observableOf(true));

    pubSubService.subscribe = jasmine.createSpy().and.callFake((file, method, callback) => {
      if (callback) {
        if (method === 'ODDS_BOOST_CHANGE') {
          callback();
        }
      }
    });

    component['initOddsBoost']();
    tick();

    expect(commandService.execute).not.toHaveBeenCalled();
    expect(component['boostButtonDisabled']).toBeFalsy();
  }));

  describe('initOddsBoost',() => {
    beforeEach(() => {
      component['user'] = { status: true } as any;
      component.isBoostEnabled = true;
      cmsService.getOddsBoost.and.returnValue(observableOf({ enabled: true }));
      commandService.executeAsync.and.returnValue(observableOf(true));
    });
    it('initOddsBoost (selection is disabled)', fakeAsync(() => {
      component.selection = {
        onStakeChange: jasmine.createSpy(),
        oddsBoost: {
          betBoostMaxStake: '10'
        },
        disabled: true
      } as any;

      component['initOddsBoost']();
      tick();

      expect(pubSubService.subscribe).toHaveBeenCalledWith(
        'QuickbetSelectionController',
        pubSubApi.ODDS_BOOST_CHANGE,
        jasmine.any(Function)
      );
      expect(component.selection.reboost).toBeFalsy();
      expect(component.selection.onStakeChange).toHaveBeenCalledTimes(2);
    }));

    it('should reboost on boosting selection with changed price', fakeAsync(() => {
      component.selection = {
        isLP: true,
        reboost: false,
        requestData: {},
        isBoostActive: true,
        price: {
          isPriceChanged: true
        },
        onStakeChange: jasmine.createSpy(),
        oddsBoost: {
          betBoostMaxStake: '10'
        },
      } as any;
      component['initOddsBoost']();
      tick();

      expect(pubSubService.publish).toHaveBeenCalledWith(
        pubSubService.API.REUSE_QUICKBET_SELECTION,
        component.selection.requestData
      );
      expect(pubSubService.publish).toHaveBeenCalledWith(
        pubSubService.API.ODDS_BOOST_SEND_GTM, { origin: 'quickbet', state: true }
      );
    }));
  });

  it('handleOddsBoostSP', () => {
    component.isBoostEnabled = true;
    component.selection = {
      isBoostActive: true,
      isLP: false
    } as any;

    component['handleOddsBoostSP']();

    expect(commandService.execute).toHaveBeenCalledWith(commandApi.ODDS_BOOST_SHOW_SP_DIALOG);
  });

  it('handleOddsBoostFreeBet (true)', () => {
    component.isBoostEnabled = true;
    component.selection = {
      isBoostActive: true,
      freebet: {}
    } as any;

    component['handleOddsBoostFreeBet']();

    expect(commandService.execute).toHaveBeenCalledWith(commandApi.ODDS_BOOST_SHOW_FB_DIALOG, [
      true
    ]);
  });

  it('handleOddsBoostFreeBet (false)', () => {
    component.isBoostEnabled = false;
    component.selection = {
      isBoostActive: true,
      freebet: {}
    } as any;

    component['handleOddsBoostFreeBet']();

    expect(commandService.execute).not.toHaveBeenCalledWith(commandApi.ODDS_BOOST_SHOW_FB_DIALOG, [
      true
    ]);
  });

  it('should check if stake is typed', () => {
    const selection = {
      stake: '20'
    };
    component.selection = <any>selection;
    expect(component.isInputFilled()).toEqual(true);
  });

  it('should show spinner', () => {
    component.placeBetPending = { state: true } as any;
    expect(component.showPendingSpinner()).toEqual(true);
    component.placeBetPending.state = false;
    expect(component.showPendingSpinner()).toEqual(false);
  });

  it('should filter player name', () => {
    component.filterPlayerName('name');
    expect(filtersService.filterPlayerName).toHaveBeenCalledWith('name');
  });

  it('should filter add score', () => {
    component.filterAddScore('market', 'outcome');
    expect(filtersService.filterAddScore).toHaveBeenCalledWith('market', 'outcome');
  });

  it('should get logged in status', () => {
    expect(component.isUserLoggedIn).toBe(true);
  });

  it('should get place bet text', () => {
    component.selection = {} as any;

    expect(component.getPlaceBetText).toBe('quickbet.buttons.placeBet');

    component.selection = {
      price: {
        isPriceChanged: true
      }
    } as any;

    expect(component.getPlaceBetText).toBe('quickbet.buttons.acceptPlaceBet');
  });

  it('should get place bet text when handicap value was changed', () => {
    quickbetUpdateService.isHandicapChanged.and.returnValue(true);
    component.selection = {
      price: {
        isPriceChanged: false
      }
    } as any;

    expect(component.getPlaceBetText).toBe('quickbet.buttons.acceptPlaceBet');
  });

  it('should get place bet text when handicap value and prices were not changed', () => {
    quickbetUpdateService.isHandicapChanged.and.returnValue(false);
    component.selection = {
      price: {
        isPriceChanged: false
      }
    } as any;

    expect(component.getPlaceBetText).toBe('quickbet.buttons.placeBet');
  });


  it('should get selection amount classes', () => {
    component.selection = {
      currency: 'usd'
    } as any;

    expect(component.selectionAmountClasses).toEqual(
      jasmine.objectContaining({
        'filled-input': false,
        'currency-usd': true
      })
    );
  });

  it('should get disable plecebet value', () => {
    component.selection = {
      stake: '12',
      freebet: {},
      disabled: false
    } as any;

    expect(component.disablePlaceBet).toEqual(false);
  });

  it('disablePlaceBet (true)', () => {
    component.selection = {
      stake: {},
      disabled: false
    } as any;

    expect(component.disablePlaceBet).toBeTruthy();
  });

  describe('#placeBetFnHandler', () => {
    it('should emit placeBetFn', () => {
      component['hasBeenReloaded'] = true;
      spyOn(component.placeBetFn, 'emit');
      component.placeBetFnHandler();
      expect(component.placeBetFn.emit).toHaveBeenCalled();
    });

    it('should not emit placeBetFn (hasBeenReloaded = false)', () => {
      component['hasBeenReloaded'] = false;
      spyOn(component.placeBetFn, 'emit');
      component.placeBetFnHandler();
      expect(component.placeBetFn.emit).not.toHaveBeenCalled();
    });
  });

  it('should emit closeFn', () => {
    spyOn(component.closeFn, 'emit');
    component.closeFnHandler();
    expect(component.closeFn.emit).toHaveBeenCalled();
  });

  it('should emit addToBetslipFn', () => {
    spyOn(component.addToBetslipFn, 'emit');
    component.addToBetslipFnHandler();
    expect(component.addToBetslipFn.emit).toHaveBeenCalled();
  });

  it('should emit openQuickDepositFn', () => {
    spyOn(component.openQuickDepositFn, 'emit');
    component.openQuickDepositFnHandler();
    expect(component.openQuickDepositFn.emit).toHaveBeenCalled();
  });

  it('should get freebet list on successfull login', () => {
    component.selection = {
      price: {
        isPriceChanged: true,
        isYourCallBet: true
      },
      newOddsValue: '11/4',
      oldOddsValue: '14/5',
      onStakeChange: () => { }
    } as any;
    component.ngOnInit();
    loginCb('quickbet');
    expect(freeBetsService.getFreeBetsState).toHaveBeenCalled();
    expect(filtersService.setCurrency).toHaveBeenCalledWith('14', '$');
  });

  it('#changeOddsFormat', () => {
    component.selection = {
      price: {
        id: '1',
        isPriceUp: true,
        isPriceChanged: true,
        isPriceDown: true
      },
      oldOddsValue: '14/5'
    } as any;

    component.ycOddsValue = () => '1/2';
    component.changeOddsFormat();

    expect(component.selection.price).toEqual({
      id: '1',
      isPriceUp: false,
      isPriceChanged: false,
      isPriceDown: false
    });
    expect(component.selection.oldOddsValue).toBe('1/2');
  });

  it('isQualifiedFreebet (true)', () => {
    const selection = { outcomeId: 10 } as any;
    const tokenPossibleBets = [{ betId: 10, betLevel: 'SELECTION' }] as any;
    expect(component['isQualifiedFreebet'](selection, tokenPossibleBets)).toBeTruthy();
  });

  it('isQualifiedFreebet (false)', () => {
    const selection = {} as any;
    const tokenPossibleBets = [] as any;
    expect(component['isQualifiedFreebet'](selection, tokenPossibleBets)).toBeFalsy();
  });

  it('mapFreebets (freebetTokenValue: "test", tokenPossibleBet: 10)', () => {
    const selection = {} as any;
    freeBetsService.getFreeBetsState = jasmine.createSpy('getFreeBetsState').and.returnValue({
      data: [{ freebetTokenValue: 'test', tokenPossibleBet: 10 }],
      available: true
    });
    component['isQualifiedFreebet'] = jasmine.createSpy();

    component['mapFreebets'](selection);
    expect(freeBetsService.getFreeBetsState).toHaveBeenCalled();
    expect(filtersService.setCurrency).not.toHaveBeenCalled();
    expect(component['isQualifiedFreebet']).toHaveBeenCalledWith(selection, [10]);
  });

  it('should toggle state of quick stake', () => {
    expect(component.quickStakeVisible).toEqual(true);

    component.onKeyboardToggle(false);
    expect(component.quickStakeVisible).toEqual(false);
  });

  describe('onQuickStakeSelect', () => {
    it('should update amount from 0 and notify about change', () => {
      const newAmount = '50';

      component.selection = {
        stake: null,
        onStakeChange: jasmine.createSpy('onStakeChange')
      } as any;
      component.onQuickStakeSelect(newAmount);

      expect(component.selection.stake).toEqual('50.00');
      expect(pubSubService.publish).toHaveBeenCalledWith(pubSubService.API.QB_QUICKSTAKE_PRESSED, ['50.00']);
      expect(gtmService.push).toHaveBeenCalledWith('trackEvent', jasmine.objectContaining({
        event: 'trackEvent',
        eventCategory: 'quickbet',
        eventAction: 'quick stake',
        eventLabel: newAmount
      }));
    });

    it('should update amount from old value and notify about change', () => {
      const newAmount = '50';

      component.selection = {
        stake: '10.50',
        onStakeChange: jasmine.createSpy('onStakeChange')
      } as any;
      component.onQuickStakeSelect(newAmount);

      expect(component.selection.stake).toEqual('60.50');
      expect(pubSubService.publish).toHaveBeenCalledWith(pubSubService.API.QB_QUICKSTAKE_PRESSED, ['60.50']);
    });

    it('#isSelectionDisabled should return true',  () => {
      component.selection = {
        stake: '10.50',
        disabled: true
      } as any;

      expect(component.isSelectionDisabled()).toBeTruthy();
    });

    it('#isSelectionDisabled should return true',  () => {
      component.selection = {
        stake: null,
        disabled: true
      } as any;

      expect(component.isSelectionDisabled()).toBeTruthy();
    });
  });

  describe('priceTypeChange', () => {
    it('priceTypeChange LP', () => {
      component.selection = {
        stake: '10.50',
        onStakeChange: jasmine.createSpy('onStakeChange'),
        isLP: false
      } as any;
      component.priceTypeChange({ output: 'test', value: 'LP' });
      expect(component.selection.isLP).toEqual(true);
    });

    it('priceTypeChange SP', () => { component.selection = {
      stake: '10.50',
      onStakeChange: jasmine.createSpy('onStakeChange'),
      isLP: true
    } as any;
      component.priceTypeChange({ output: 'test', value: 'SP' });
      expect(component.selection.isLP).toEqual(false);
    });
  });

  describe('isMaxStakeExceeded', () => {
    it('boost is not active', () => {
      component.selection = { isBoostActive: false } as any;
      component['isMaxStakeExceeded']();
      expect(commandService.execute).not.toHaveBeenCalled();
    });

    it('boost active', () => {
      component.selection = { isBoostActive: true, stake: 5 } as any;
      component['isMaxStakeExceeded']();
      expect(commandService.execute).toHaveBeenCalledWith(commandApi.ODDS_BOOST_MAX_STAKE_EXCEEDED, [5]);
    });

    it('boost active (e/w)', () => {
      component.selection = { isBoostActive: true, isEachWay: true, stake: 5 } as any;
      component['isMaxStakeExceeded']();
      expect(commandService.execute).toHaveBeenCalledWith(commandApi.ODDS_BOOST_MAX_STAKE_EXCEEDED, [10]);
    });
  });

  it('#isIFrameLoadingInProgress should return false when iframe is shown and panel is expanded', () => {
    component.showIFrame = true;
    component.quickDepositFormExpanded = true;
    expect(component['isIFrameLoadingInProgress']()).toBeFalsy();
  });

  it('#isIFrameLoadingInProgress should return false when iframe is shown and panel is collapsed', () => {
    component.showIFrame = true;
    component.quickDepositFormExpanded = false;
    expect(component['isIFrameLoadingInProgress']()).toBeFalsy();
  });

  it('#isIFrameLoadingInProgress should return true when iframe is not shown and panel is expanded', () => {
    component.showIFrame = false;
    component.quickDepositFormExpanded = true;
    expect(component['isIFrameLoadingInProgress']()).toBeTruthy();
  });

  it('#isIFrameLoadingInProgress should return false when iframe is not shown and panel is collapsed', () => {
    component.showIFrame = false;
    component.quickDepositFormExpanded = false;
    expect(component['isIFrameLoadingInProgress']()).toBeFalsy();
  });

  describe('isMakeQuickDeposit', () => {
    beforeEach(() => {
      component.placeBetPending = { state: false };
    });

    it('should check if quick deposit made if amount needed', () => {
      const deposit = {
        neededAmountForPlaceBet: '20'
      };
      userService.sportBalance = '10';
      component.quickDeposit = <any>deposit;
      expect(component.isMakeQuickDeposit()).toBeTruthy();
    });

    it('should check if quick deposit made if user balance = 0 and have freeBet', () => {
      const deposit = {
        neededAmountForPlaceBet: undefined
      };
      userService.sportBalance = '0';
      component.selection = { freebetValue: 5 } as any;
      component.quickDeposit = <any>deposit;
      expect(component.isMakeQuickDeposit()).toBeFalsy();
    });

    it('should check if quick deposit disabled if user balance = 0 and do not have freeBet', () => {
      const deposit = {
        neededAmountForPlaceBet: undefined
      };
      userService.sportBalance = '0';
      component.selection = { freebetValue: 0 } as any;
      component.quickDeposit = <any>deposit;
      expect(component.isMakeQuickDeposit()).toBeTruthy();
    });

    it('should check if quick deposit disabled if user balance is exist', () => {
      const deposit = {
        neededAmountForPlaceBet: undefined
      };
      userService.sportBalance = '10';
      component.quickDeposit = <any>deposit;
      expect(component.isMakeQuickDeposit()).toBeFalsy();
    });

    it('should check if quick deposit disabled if amount not needed', () => {
      const deposit = {
        neededAmountForPlaceBet: ''
      };
      userService.sportBalance = '10';
      component.quickDeposit = <any>deposit;
      expect(component.isMakeQuickDeposit()).toBeFalsy();
    });

    it('should check if quick deposit disabled if placeBetPending', () => {
      component.placeBetPending = {
        state: true
      };

      expect(component.isMakeQuickDeposit()).toBeFalsy();
    });
  });

  it('#isSelectionDisabled should return true',  () => {
    component['isIFrameLoadingInProgress'] = jasmine.createSpy().and.returnValue(true);
    component.selection = {
      stake: '10.50',
      disabled: true
    } as any;
    component.placeBetPending = {
      state: false
    };

    expect(component.isSelectionDisabled()).toBeTruthy();
  });

  it('#isSelectionDisabled should return true',  () => {
    component['isIFrameLoadingInProgress'] = jasmine.createSpy().and.returnValue(false);
    component.selection = {
      stake: '10.50',
      disabled: true
    } as any;
    component.placeBetPending = {
      state: false
    };

    expect(component.isSelectionDisabled()).toBeTruthy();
  });

  it('#isSelectionDisabled should return false',  () => {
    component['isIFrameLoadingInProgress'] = jasmine.createSpy().and.returnValue(false);
    component.selection = {
      stake: '0',
      disabled: false
    } as any;
    component.placeBetPending = {
      state: false
    };

    expect(component.isSelectionDisabled()).toBeFalsy();
  });

  it('#isSelectionDisabled should return false',  () => {
    component.selection = {
      stake: '20.45',
      disabled: false
    } as any;
    component.placeBetPending = {
      state: false
    };

    expect(component.isSelectionDisabled()).toBeFalsy();
  });

  it('#isSelectionDisabled should return true',  () => {
    component['isIFrameLoadingInProgress'] = jasmine.createSpy().and.returnValue(true);
    component.selection = {
      stake: '20.45',
      disabled: false
    } as any;
    component.placeBetPending = {
      state: false
    };

    expect(component.isSelectionDisabled()).toBeTruthy();
  });

  it('#isSelectionDisabled should return true',  () => {
    component.selection = {
      stake: null,
      disabled: true
    } as any;
    component.placeBetPending = {
      state: false
    };

    expect(component.isSelectionDisabled()).toBeTruthy();
  });

  it('#isSelectionDisabled should return true', () => {
    component['isIFrameLoadingInProgress'] = jasmine.createSpy().and.returnValue(false);
    component.selection = {
      stake: '20.45',
      disabled: false
    } as any;
    component.placeBetPending = {
      state: true
    };

    expect(component.isSelectionDisabled()).toBeTruthy();
  });

  it('#showSpinnerOnQuickDeposit should return false', () => {
    component['isIFrameLoadingInProgress'] = jasmine.createSpy().and.returnValue(false);
    component.placeBetPending = {
      state: false
    };

    expect(component.showSpinnerOnQuickDeposit()).toBeFalsy();
  });

  it('#showSpinnerOnQuickDeposit should return true when iframe loading is in progress', () => {
    component['isIFrameLoadingInProgress'] = jasmine.createSpy().and.returnValue(true);

    expect(component.showSpinnerOnQuickDeposit()).toBeTruthy();
  });

  it('#showSpinnerOnQuickDeposit should return true when bet placing is in progress', () => {
    component['isIFrameLoadingInProgress'] = jasmine.createSpy().and.returnValue(false);
    component.placeBetPending = {
      state: true
    };

    expect(component.showSpinnerOnQuickDeposit()).toBeTruthy();
  });

  describe('#onlineListener', () => {
    it('should call onlineListener method', () => {
      windowRef.nativeWindow.setTimeout.and.callFake((cb) => {
        cb && cb();
      });
      component['onlineListener']();

      expect(component['hasBeenReloaded']).toEqual(true);
      expect(windowRef.nativeWindow.setTimeout).toHaveBeenCalledWith(jasmine.any(Function), 1500);
    });
  });

  describe('#offlineListener', () => {
    it('should call offlineListener method', () => {
      component['offlineListener']();

      expect(component['hasBeenReloaded']).toEqual(false);
    });
  });
});
