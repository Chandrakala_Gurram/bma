import { QuickbetReceiptComponent } from './quickbet-receipt.component';

import { IQuickbetReceiptDetailsModel } from '@app/quickbet/models/quickbet-receipt.model';
import { IQuickbetSelectionModel } from '@core/models/quickbet-selection.model';
import { BYBBet } from '@yourcall/models/bet/byb-bet';

describe('QuickBetReceiptComponent', () => {
  let component: QuickbetReceiptComponent;
  let userService;
  let filtersService;
  let quickbetService;
  let nativeBridge;
  let window;
  let storageService;

  beforeEach(() => {
    userService = {
      currencySymbol: '$',
      receiptViewsCounter: 0,
      winAlertsToggled: false,
      set: jasmine.createSpy(),
      username: 'test'
    };

    filtersService = {
      setCurrency: jasmine.createSpy(),
      filterPlayerName: jasmine.createSpy('filterPlayerName').and.returnValue(''),
      filterAddScore: jasmine.createSpy('filterAddScore').and.returnValue('')
    };

    quickbetService = {
      getOdds: jasmine.createSpy('getOdds'),
      getLinesPerStake: jasmine.createSpy('getLinesPerStake').and.returnValue('2 Lines at £1 per line'),
      getEWTerms: jasmine.createSpy('getEWTerms').and.returnValue('Each Way Odds 1/5 Places 1-2-3'),
      isVirtualSport: jasmine.createSpy('isVirtualSport').and.returnValue(true),
      getBybSelectionType: jasmine.createSpy('getBybSelectionType')
    };

    nativeBridge = {
      onActivateWinAlerts: jasmine.createSpy()
    };

    window = {
      nativeWindow: {
        NativeBridge: { pushNotificationsEnabled: true }
      }
    };

    storageService = {
      set: jasmine.createSpy('set'),
      get: jasmine.createSpy('get').and.returnValue(undefined)
    };

    component = new QuickbetReceiptComponent(
      userService,
      filtersService,
      quickbetService,
      nativeBridge,
      window,
      storageService
    );

    component.selection = {
      stake: '1.22'
    } as IQuickbetSelectionModel;

    component.betReceipt = {
      bet: { id: 1 },
      receipt: {
        id: 'test'
      },
      stake: {
        amount: '100',
        freebet: '10',
        stakePerLine: '111',
      },
      legParts: [{
        eventDesc: '',
        marketDesc: '',
        handicap: '',
        outcomeDesc: ''
      }],
      price: {
        priceType: 'test_price',
        priceTypeRef: {
          id: 'GUARANTEED'
        }
      },
      payout: { potential: '99' },
      oddsValue: '1/2'
    } as IQuickbetReceiptDetailsModel;
    component['filtersService']['isGreyhoundsEvent'] = jasmine.createSpy().and.returnValue(false);
  });

  it('ngOnInit', () => {
    spyOn(component, 'filterOutcomeName');
    spyOn(component, 'filterMarketName');
    spyOn(component, 'filterEventName');

    component.ngOnInit();

    expect(component.hasFreebet).toBe(true);
    expect(quickbetService.getOdds).toHaveBeenCalled();
    expect(component.isEachWay).toBe(true);
    expect(filtersService.setCurrency).toHaveBeenCalledTimes(5);
    expect(component.filterOutcomeName).toHaveBeenCalled();
    expect(component.filterMarketName).toHaveBeenCalled();
    expect(component.filterEventName).toHaveBeenCalled();
    expect(component.eachEayTerms).toEqual('Each Way Odds 1/5 Places 1-2-3');
    expect(component.linesPerStake).toEqual('2 Lines at £1 per line');
  });

  it('should init eachWayTerms only if selection is each Way', () => {
    component.betReceipt.stake.stakePerLine = component.betReceipt.stake.amount = '100';

    component.ngOnInit();

    expect(component.isEachWay).toBe(false);
    expect(filtersService.setCurrency).toHaveBeenCalledTimes(5);
    expect(component.eachEayTerms).toBeUndefined();
  });

  it('should toggleWinAlerts onInit if enabled', () => {
    storageService.get.and.returnValue(true);
    component.ngOnInit();

    expect(storageService.set).toHaveBeenCalledWith('winAlertsEnabled', true );
  });

  it('ngOnDestroy should call native bridge', () => {
    component.winAlertsBet = '111';
    component.ngOnDestroy();

    expect(nativeBridge.onActivateWinAlerts).toHaveBeenCalled();
  });

  it('ngOnDestroy should call native bridge', () => {
    component.ngOnDestroy();

    expect(nativeBridge.onActivateWinAlerts).not.toHaveBeenCalled();
  });

  describe('getStake', () => {
    it('should return stake with currency', () => {
      filtersService.setCurrency = jasmine.createSpy().and.returnValue('5$');
      const result = component.getStake('5');

      expect(filtersService.setCurrency).toHaveBeenCalled();
      expect(result).toBe('5$');
    });

    it('should not return stake with currency', () => {
      filtersService.setCurrency = jasmine.createSpy().and.returnValue('5$');
      const result = component.getStake(null);

      expect(filtersService.setCurrency).not.toHaveBeenCalled();
      expect(result).toBe('');
    });
  });

  it('toggleWinAlerts should set win alert bet id', () => {
    const receipt = {
      receipt: { id: '111' }
    } as IQuickbetReceiptDetailsModel;

    component.ngOnInit();
    expect(!component.winAlertsReceiptId).toBeTruthy();
    component.toggleWinAlerts(receipt, true);
    expect(component.winAlertsReceiptId).toBe('111');
    expect(component.winAlertsBet).toBe('111');
    expect(userService.set).toHaveBeenCalledWith({ winAlertsToggled: true });
  });

  it('toggleWinAlerts should remove win alert bet id', () => {
    const receipt = {
      receipt: { id: '111' }
    } as IQuickbetReceiptDetailsModel;

    component.ngOnInit();
    component.toggleWinAlerts(receipt, false);

    expect(component.winAlertsBet).toBeNull();
    expect(storageService.set).toHaveBeenCalledWith('winAlertsEnabled', false );
  });

  it('toggleWinAlerts should not call user set and set win alerts receipt id', () => {
    const receipt = {
      receipt: { id: '111' }
    } as IQuickbetReceiptDetailsModel;

    component.ngOnInit();
    component['user'] = {
      winAlertsToggled: true,
    } as any;
    component.toggleWinAlerts(receipt, true);

    expect(component.winAlertsReceiptId).toBe('111');
    expect(component.winAlertsBet).toBe('111');
    expect(userService.set).not.toHaveBeenCalled();
    expect(storageService.set).toHaveBeenCalledWith('winAlertsEnabled', true );
  });

  it('toggleWinAlerts with winAlertsReceiptId', () => {
    const receipt = {
      receipt: { id: '111' }
    } as IQuickbetReceiptDetailsModel;
    component.winAlertsReceiptId = '111';

    component.ngOnInit();
    component['user'] = {
      winAlertsToggled: true,
    } as any;
    component.toggleWinAlerts(receipt, true);
    expect(component.winAlertsBet).toBe('111');
    expect(userService.set).not.toHaveBeenCalled();
  });

  it('toggleWinAlerts should not call any action', () => {
    const receipt = {
      receipt: { id: '111' }
    } as IQuickbetReceiptDetailsModel;
    window.nativeWindow.NativeBridge.pushNotificationsEnabled = false;
    component.ngOnInit();
    component.toggleWinAlerts(receipt, true);

    expect(component.winAlertsBet).toBeUndefined();
  });

  it('toggleWinAlerts should set winAlertsBet value as receiptId', () => {
    const receipt = {
      receipt: { id : '111'}
    } as IQuickbetReceiptDetailsModel ;

    component.ngOnInit();
    component['user'] = {
      winAlertsToggled : true
    } as any;
    component['winAlertsReceiptId'] = '345';
    component.toggleWinAlerts(receipt, true);

    expect(component.winAlertsBet).toBe('111');
    expect(userService.set).not.toHaveBeenCalled();
  });

  describe('#showWinAlertsTooltip', () => {
    it('showWinAlertsTooltip should be true', () => {
      const result = component.showWinAlertsTooltip();
      expect(result).toBeTruthy();
    });
    it('showWinAlertsTooltip should be false', () => {
      storageService.get = jasmine.createSpy('').and.returnValue({'receiptViewsCounter-test': 2});
      const result = component.showWinAlertsTooltip();
      expect(result).toBeFalsy();
    });
  });

  describe('getPotentialPayoutValue', () => {
    it('payout: "200$" returns "200$" ', () => {
      expect(component.getPotentialPayoutValue('200$')).toEqual('200$');
    });

    it('payout: "" returns "N/A" ', () => {
      expect(component.getPotentialPayoutValue('')).toEqual('N/A');
    });

    it('payout: undefined returns "N/A"', () => {
      expect(component.getPotentialPayoutValue(undefined)).toEqual('N/A');
    });
  });

  describe('filterEventName', () => {
    it('empty string case" ', () => {
      component.betReceipt = {};
      component.selection = undefined;
      component.clearName = jasmine.createSpy();
      component.filterEventName();
      expect(component.clearName).toHaveBeenCalledWith('');
    });

    it('non-empty string', () => {
      component.selection = undefined;
      component.betReceipt = {
        legParts: [{
          eventDesc: 'one'
        }, {}]
      } as any;
      component.clearName = jasmine.createSpy();
      component.filterEventName();
      expect(component.clearName).toHaveBeenCalledWith('one');
    });

    it('filterEventName (virtual)', () => {
      component.selection = <any>{
        categoryName: 'Virtual Sports',
        eventName: 'test event'
      };
      component.betReceipt = <any>{
        legParts: [{
          eventDesc: 'one'
        }, {}]
      };
      expect(component.filterEventName()).toEqual('test event');
    });

    it('exclude extra place icon rom unnamed favourites', () => {
      component.selection = <any>{
        isUnnamedFavourite: true,
        categoryName: 'Virtual Sports',
        eventName: 'test event'
      };

      expect(component.getExcludedDrillDownTagNames()).toEqual('MKTFLAG_EPR,EVFLAG_EPR');

      component.selection = <any>{
        isUnnamedFavourite: false,
        categoryName: 'Virtual Sports',
        eventName: 'test event'
      };

      expect(component.getExcludedDrillDownTagNames()).toEqual('');
    });

  });

  describe('filterMarketName', () => {
    it('betReceipt.legParts.length = 2 ', () => {
      component.betReceipt = {
        legParts: [{
          eventDesc: 'one',
          marketDesc: 'one1',
          outcomeDesc: 'one2',
        }, {
          eventDesc: 'two',
          marketDesc: 'two1',
          outcomeDesc: 'two2',
        }]
      } as any;
      component.clearName = jasmine.createSpy();
      component.filterMarketName();
      expect(filtersService.filterAddScore).toHaveBeenCalledTimes(2);
      expect(component.clearName).toHaveBeenCalledTimes(2);
    });
  });

  describe('filterOutcomeName', () => {
    it('betReceipt.legParts.length = 2 + handicap', () => {
      component.betReceipt = {
        legParts: [{
          eventDesc: 'one',
          outcomeDesc: 'outcomeDesc',
          handicap: 'handicap'
        }, {
          eventDesc: 'two',
          outcomeDesc: 'outcomeDesc'
        }]
      } as any;
      component.clearName = jasmine.createSpy();
      component.filterOutcomeName();
      expect(component.betReceipt.legParts[0].handicap).toBeTruthy();
      expect(component.betReceipt.legParts[1].handicap).toBeFalsy();
      expect(filtersService.filterPlayerName).toHaveBeenCalledTimes(2);
      expect(component.clearName).toHaveBeenCalledTimes(2);
    });
  });

  it('clearName(name string) with symbols to be replaced', () => {
    expect(component.clearName('|,string,|')).toEqual('string');
  });

  describe('isEachWayBet', () => {
    it('stake.stakePerLine !== stake.amount', () => {
      const stake = {
        amount: '200',
        stakePerLine: '10'
      };
      expect(component.isEachWayBet(stake)).toEqual(true);
    });
    it('stake.stakePerLine === stake.amount', () => {
      const stake = {
        amount: '10',
        stakePerLine: '10'
      };
      expect(component.isEachWayBet(stake)).toEqual(false);
    });
  });

  it('getOdds, price: {priceDec: 200}', () => {
    component.getOdds({ priceDec: 200 });
    expect(quickbetService.getOdds).toHaveBeenCalledWith({ priceDec: 200 });
  });

  it('setCurrency, val: "val"', () => {
    component.setCurrency('val');
    expect(filtersService.setCurrency).toHaveBeenCalledWith('val', '$');
  });

  describe('oddsValue', () => {
    it('oddsValue returns bet receipt odds value', () => {
      expect(component.oddsValue).toBe('1/2');
    });

    it('oddsValue returns yc odds value', () => {
      component.ycOddsValue = () => '1.2';
      expect(component.oddsValue).toBe('1.2');
    });
  });


  it('should filter outcome name with handicap value', () => {
    filtersService.filterPlayerName.and.returnValue('tie');
    component.betReceipt.legParts = [{
      eventDesc: 'Man City v Watford"',
      marketDesc: 'Handicap Match Result - Man City +2.0 goals',
      outcomeDesc: 'tie',
      outcomeId: '955100247',
      handicap: '+2.0'
    }];

    const actualResult = component.filterOutcomeName();

    expect(filtersService.filterPlayerName).toHaveBeenCalledWith('tie');
    expect(actualResult).toEqual('tie (+2.0)');
  });

  it('should filter outcome name without handicap value', () => {
    filtersService.filterPlayerName.and.returnValue('Man City');
    component.betReceipt.legParts = [{
      eventDesc: 'Man City v Watford"',
      marketDesc: 'Handicap Match Result - Man City +2.0 goals',
      outcomeDesc: 'Man City',
      outcomeId: '955100247',
    }];

    const actualResult = component.filterOutcomeName();

    expect(filtersService.filterPlayerName).toHaveBeenCalledWith('Man City');
    expect(actualResult).toEqual('Man City');
  });

  it('should filter market name', () => {
    filtersService.filterAddScore.and.returnValue('Handicap Match Result - Man City');
    component.betReceipt.legParts = [{
      eventDesc: 'Man City v Watford"',
      marketDesc: 'Handicap Match Result - Man City +2.0 goals',
      outcomeDesc: 'Man City',
      outcomeId: '955100247',
    }];

    const actualResult = component.filterMarketName();

    expect(filtersService.filterAddScore).toHaveBeenCalledWith('Handicap Match Result - Man City +2.0 goals', 'Man City');
    expect(actualResult).toEqual('Handicap Match Result - Man City');
  });

  it('should filter event name', () => {
    component.selection = undefined;
    component.betReceipt.legParts = [{
      eventDesc: 'Man City v Watford"',
      marketDesc: 'Handicap Match Result - Man City +2.0 goals',
      outcomeDesc: 'Man City',
      outcomeId: '955100247',
    }];

    const actualResult = component.filterEventName();

    expect(actualResult).toEqual('Man City v Watford"');
  });

  it('should filter event name with default eventDesc', () => {
    component.selection = undefined;
    component.betReceipt.legParts = [{
      marketDesc: 'Handicap Match Result - Man City +2.0 goals',
      outcomeDesc: 'Man City',
      outcomeId: '955100247',
    }] as any;

    const actualResult = component.filterEventName();

    expect(actualResult).toEqual('');
  });

  it('should return potential payout value', () => {
    const actualResult = component.getPotentialPayoutValue('1.0');

    expect(actualResult).toEqual('1.0');
  });

  it('should return N/A when no potential payout value were found', () => {
    const actualResult = component.getPotentialPayoutValue(null);

    expect(actualResult).toEqual('N/A');
  });

  describe('#getEWTerms', () => {
    it('should call getEWTerms method', () => {
      component.getEWTerms({} as any);

      expect(quickbetService.getEWTerms).toHaveBeenCalledWith({});
    });
  });

  describe('#getLinesPerStake', () => {
    it('should call getLinesPerStake method', () => {
      component.getLinesPerStake({} as any);

      expect(quickbetService.getLinesPerStake).toHaveBeenCalledWith({});
    });
  });

  describe('Should check isBogEnabled', () => {
    it('should call isBogFromPriceType()', () => {
      expect(component.isBogFromPriceType()).toBe(true);
    });

    it('should call isBogFromPriceType() with facke value of priceTypeRef id and should check isBogEnabled', () => {
      component.betReceipt = {
        ...component.betReceipt,
        price: {
          priceType: 'test_price',
          priceTypeRef: {
            id: 'SP'
          }
        },
      } as any;
      component.ngOnInit();

      expect(component.isBogFromPriceType()).toBe(false);
      expect(component.isBogEnabled).toBe(false);
    });

    it('should check isBogEnabled when isBogFromPriceType() = false', () => {
      component.isBogFromPriceType = jasmine.createSpy('isBogFromPriceType').and.returnValue(false);
      component.ngOnInit();

      expect(component.isBogEnabled).toBe(false);
    });
  });

  describe('setBybData', () => {
    it('should set byb data', () => {
      component.selection = new BYBBet({
        dashboardData: {
          game: { title: '|A| |vs| |B|' }
        }
      }) as any;
      component['setBybData']();
      expect(component.bybEventName).toBe('A vs B');
      expect(quickbetService.getBybSelectionType).toHaveBeenCalled();
    });

    it('should set byb data', () => {
      component.selection = {} as any;
      component['setBybData']();
      expect(component.bybEventName).toBeUndefined();
      expect(quickbetService.getBybSelectionType).not.toHaveBeenCalled();
    });
  });
});
