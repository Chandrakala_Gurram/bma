import { Component, Input, OnInit, OnDestroy } from '@angular/core';

import { UserService } from '@core/services/user/user.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { QuickbetService } from '@app/quickbet/services/quickbetService/quickbet.service';
import {
  IQuickbetReceiptDetailsModel,
  IQuickbetReceiptStakeModel,
  IQuickbetReceiptLegPartsModel
} from '@app/quickbet/models/quickbet-receipt.model';
import { IQuickbetSelectionPriceModel } from '@app/quickbet/models/quickbet-selection-price.model';
import { IQuickbetSelectionModel } from '@core/models/quickbet-selection.model';
import { NativeBridgeService } from '@core/services/nativeBridge/native-bridge.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

import * as _ from 'underscore';
import { StorageService } from '@core/services/storage/storage.service';
import { BYBBet } from '@yourcall/models/bet/byb-bet';

@Component({
  selector: 'quickbet-receipt',
  templateUrl: 'quickbet-receipt.component.html',
  styleUrls: ['quickbet-receipt.component.less']
})

export class QuickbetReceiptComponent implements OnInit, OnDestroy {
  @Input() betReceipt: IQuickbetReceiptDetailsModel;
  @Input() selection: IQuickbetSelectionModel;
  @Input() winAlertsEnabled: boolean;
  @Input() ycOddsValue?: Function;

  currencySymbol: string;
  stakePerLine: string;
  freebet: string;
  amount: string;
  payout: string;
  isEachWay: boolean;
  odds: string;
  hasFreebet: boolean;
  filteredOutcomeName: string;
  filteredMarketName: string;
  filteredEventName: string;
  winAlertsBet: string;
  winAlertsReceiptId: string;
  eachEayTerms: string;
  linesPerStake: string;
  winAlertsActive: boolean;
  finalStake: string;
  isBogEnabled: boolean;
  excludedDrillDownTagNames: string;

  bybSelectionType: string;
  bybEventName: string;

  constructor(protected user: UserService,
    protected filtersService: FiltersService,
    protected quickbetService: QuickbetService,
    protected nativeBridge: NativeBridgeService,
    protected window: WindowRefService,
    protected storageService: StorageService) {
  }

  ngOnInit(): void {
    this.hasFreebet = Number(this.betReceipt.stake.freebet) > 0;
    this.odds = this.getOdds(this.betReceipt.price);
    this.isEachWay = this.isEachWayBet(this.betReceipt.stake);
    this.stakePerLine = this.setCurrency(this.betReceipt.stake.stakePerLine);
    this.freebet = this.setCurrency(this.betReceipt.stake.freebet);
    this.amount = this.setCurrency(this.betReceipt.stake.amount);
    this.payout = +this.betReceipt.payout.potential && this.setCurrency(this.betReceipt.payout.potential);
    this.filteredOutcomeName = this.filterOutcomeName();
    this.filteredMarketName = this.filterMarketName();
    this.filteredEventName = this.filterEventName();
    this.linesPerStake = this.getLinesPerStake(this.betReceipt.stake);
    this.finalStake = this.getStake(this.selection.stake);
    this.excludedDrillDownTagNames = this.getExcludedDrillDownTagNames();

    if (this.isEachWay) {
      this.eachEayTerms = this.getEWTerms(this.betReceipt.legParts[0]);
    }

    this.winAlertsActive = this.storageService.get('winAlertsEnabled');
    if (this.winAlertsActive) {
      this.toggleWinAlerts(this.betReceipt, true);
    }

    this.isBogEnabled = this.isBogFromPriceType();

    this.setBybData();
  }

  ngOnDestroy(): void {
    if (this.winAlertsBet) {
      this.nativeBridge.onActivateWinAlerts(this.winAlertsReceiptId, [this.winAlertsBet]);
    }
  }

  setCurrency(val: string): string {
    return this.filtersService.setCurrency(val, this.user.currencySymbol);
  }

  getStake(val: string): string {
    if (!val) {
      return '';
    }

    const stake = this.isEachWay ? parseFloat(val) * 2 : parseFloat(val);
    return this.setCurrency(stake.toString());
  }

  /**
   * Get Each Way terms - Each Way Odds 1/5 Places 1-2-3
   * @param price {object}
   * @returns {string}
   */
  getEWTerms(legPart: IQuickbetReceiptLegPartsModel): string {
    return this.quickbetService.getEWTerms(legPart);
  }

  /**
   * Get Lines per stake - 2 Lines at £1 per line
   * @param receipt {object}
   * @returns {string}
   */
  getLinesPerStake(stake: IQuickbetReceiptStakeModel): string {
    return this.quickbetService.getLinesPerStake(stake);
  }

  /**
   * Get odds in correct format
   * @param price {object}
   * @returns {string}
   */
  getOdds(price: IQuickbetSelectionPriceModel): string {
    return this.quickbetService.getOdds(price);
  }

  /**
   * Check if eachway option was chosen by user
   * @param stake
   * @returns {boolean}
   */
  isEachWayBet(stake: IQuickbetReceiptStakeModel): boolean {
    return stake.stakePerLine !== stake.amount;
  }

  /**
   * Clear name(e.g. |Al Dancer| => Al Dancer)
   * @param name
   * @returns {string}
   */
  clearName(name: string): string {
    return name.replace(/[|,]/g, '');
  }

  /**
   * Filter outcome name (add handicap value to name if it is present).
   * @returns {string}
   */
  filterOutcomeName(): string {
    const names = _.map(this.betReceipt.legParts, ({ outcomeDesc, handicap }) => {
      let name = this.filtersService.filterPlayerName(outcomeDesc);
      name = this.clearName(name);

      if (handicap) {
        name += ` (${handicap})`;
      }

      return name;
    });

    return names.join(', ');
  }

  /**
   * to exclude Racing unnamed favourites signposting icon from bet-receipt
   */
  getExcludedDrillDownTagNames(): string {
    const excludedArray = [];

    if (this.selection.isUnnamedFavourite) {
      excludedArray.push('MKTFLAG_EPR', 'EVFLAG_EPR');
    }

    return excludedArray.join(',');
  }

  /**
   * Filters market name.
   * @return {string}
   */
  filterMarketName(): string {
    return _.map(this.betReceipt.legParts, ({ outcomeDesc, marketDesc }) => {
      const name = this.filtersService.filterAddScore(marketDesc, outcomeDesc);
      return this.clearName(name);
    }).join(', ');
  }

  /**
   * Filters event name from bet receipt.
   * @return {string}
   */
  filterEventName(): string {
    const { eventDesc = '' } = _.isArray(this.betReceipt.legParts) ? this.betReceipt.legParts[0] : {};
    return this.selection && this.quickbetService.isVirtualSport(this.selection.categoryName) ?
      this.selection.eventName : this.clearName(eventDesc);
  }

  /**
   * Get potential payout value
   * @param {string} payout
   * @returns {string}
   */
  getPotentialPayoutValue(payout: string): string {
    return payout || 'N/A';
  }

  showWinAlertsTooltip(): boolean {
    const MAX_VIEWS_COUNT = 1;
    const tooltipData = this.storageService.get('tooltipsSeen') || {};
    return (tooltipData[`receiptViewsCounter-${this.user.username}`] || null) <= MAX_VIEWS_COUNT && !this.user.winAlertsToggled;
  }

  toggleWinAlerts(receipt: IQuickbetReceiptDetailsModel, event: boolean): void {
    if (this.window.nativeWindow.NativeBridge.pushNotificationsEnabled) {
      if (event) {
        const receiptId = receipt.receipt.id;
        if (!this.user.winAlertsToggled) { this.user.set({ winAlertsToggled: true }); }
        if (!this.winAlertsReceiptId) { this.winAlertsReceiptId = receiptId; }
        this.winAlertsBet = receiptId;
        this.storageService.set('winAlertsEnabled', true);
      } else {
        this.winAlertsBet = null;
        this.storageService.set('winAlertsEnabled', false);
      }
    }
  }

  /**
   * Check if BogToggle is true/false from priceTypeRef.id --> 'GUARANTEED' and it is not a Greyhounds event
   * @returns <boolean>
   */
  isBogFromPriceType(): boolean {
    const betReceiptPrice = this.betReceipt.price,
      eventCategoryId = this.selection.categoryId;
    return betReceiptPrice && betReceiptPrice.priceTypeRef && betReceiptPrice.priceTypeRef.id
      && betReceiptPrice.priceTypeRef.id === 'GUARANTEED' && !this.filtersService.isGreyhoundsEvent(eventCategoryId);
  }

  get oddsValue(): string | number {
    return this.ycOddsValue ? this.ycOddsValue() : this.betReceipt.oddsValue;
  }

  private setBybData(): void {
    if (this.selection instanceof BYBBet) {
      const sel = this.selection as BYBBet;
      this.bybSelectionType = this.quickbetService.getBybSelectionType(sel.channel);
      this.bybEventName = this.clearName(sel.game.title);
    }
  }
}
