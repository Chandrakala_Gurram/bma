import { Component, Input } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { UserService } from '@core/services/user/user.service';

import { IQuickbetSelectionModel } from '@core/models/quickbet-selection.model';

@Component({
  selector: 'bet-summary',
  templateUrl: 'bet-summary.component.html',
  styleUrls: ['bet-summary.component.less']
})
export class BetSummaryComponent {

  @Input() selection: IQuickbetSelectionModel;

  constructor(protected user: UserService,
              protected currencyPipe: CurrencyPipe) {
  }

  /**
   * Formats total stake.
   * @returns {string}
   */
  getTotalStake(): string {
    const stake = (this.selection.isEachWay ? 2 : 1) * parseFloat(this.selection.stake);

    return this.currencyPipe.transform((stake || 0) + (this.selection.freebetValue || 0), this.user.currencySymbol, 'code');
  }

  getStake(): string {
    const stake = (this.selection.isEachWay ? 2 : 1) * parseFloat(this.selection.stake);
    const stakeValue = this.currencyPipe.transform((stake || 0), this.user.currencySymbol, 'code');
    return stakeValue;
  }

  /**
   * Formats potential payout.
   * @returns {string}
   */
  getPotentialPayout(): string {
    return Number.isNaN(+this.selection.potentialPayout) ? this.selection.potentialPayout
      : this.currencyPipe.transform(this.selection.potentialPayout, this.user.currencySymbol, 'code');
  }
}
