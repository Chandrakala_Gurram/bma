import {
  Component,
  Input,
  Output,
  OnInit,
  OnDestroy,
  EventEmitter,
  AfterContentInit,
  ViewEncapsulation,
  ChangeDetectorRef
} from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import * as _ from 'underscore';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { InfoDialogService } from '@coreModule/services/infoDialogService/info-dialog.service';
import { UserService } from '@core/services/user/user.service';
import { DeviceService } from '@core/services/device/device.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { QuickbetService } from '@app/quickbet/services/quickbetService/quickbet.service';
import { QuickbetDepositService } from '@quickbetModule/services/quickbetDepositService/quickbet-deposit.service';
import { QuickbetDataProviderService } from '@app/core/services/quickbetDataProviderService/quickbet-data-provider.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { ISystemConfig } from '@core/services/cms/models';

import { IQuickbetSelectionModel } from '@core/models/quickbet-selection.model';
import { IQuickbetReceiptDetailsModel, IYCBetReceiptModel } from '@app/quickbet/models/quickbet-receipt.model';
import { QuickbetNotificationService } from '@app/quickbet/services/quickbetNotificationService/quickbet-notification.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { DomToolsService } from '@coreModule/services/domTools/dom.tools.service';
import { RendererService } from '@shared/services/renderer/renderer.service';
import { ISuspendedOutcomeError } from '@betslip/models/suspended-outcome-error.model';
import { QuickbetUpdateService } from '@app/quickbet/services/quickbetUpdateService/quickbet-update.service';

@Component({
  selector: 'quickbet-panel',
  templateUrl: 'quickbet-panel.component.html',
  styleUrls: ['quickbet-panel.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class QuickbetPanelComponent implements OnInit, OnDestroy, AfterContentInit {
  @Input() title: string;
  @Input() showCloseIcon: boolean;
  @Input() isFiveASideBet: boolean;
  @Input() selection: IQuickbetSelectionModel;
  @Input() leftBtnLocale?: string;
  @Input() bodyClass: string;
  @Input() ycOddsValue?: Function;
  @Output() readonly placeBetFn: EventEmitter<void> = new EventEmitter();
  @Output() readonly reuseSelectionFn?: EventEmitter<void> = new EventEmitter();
  @Output() readonly closeFn: EventEmitter<boolean> = new EventEmitter();
  @Output() readonly addToBetslipFn: EventEmitter<void> = new EventEmitter();

  viewState: string;
  initialTitle: string;
  placeBetPending: { state: boolean };
  betReceipt: IQuickbetReceiptDetailsModel;
  loginAndPlaceBets: boolean;
  sysConfig: ISystemConfig;
  slideUpAnimation: boolean;

  quickDepositFormExpanded = false;
  showIFrame = false;
  showPriceChangeMessage = false;
  showSuspendedNotification = false;
  iframeLoaded = false;
  placeSuspendedErr: ISuspendedOutcomeError = { multipleWithDisableSingle: false, disableBet: false, msg: '' };
  priceChangeText: string;
  estimatedReturnAfterPriceChange: number;

  private eventSuspensionSubscription: Subscription;
  private priceChangeSubscription: Subscription;

  protected QB_PANEL_NAME = 'QuickbetPanel';

  private BODY_CLASS: string;
  private quickbeReceiptSubscriber: Subscription;
  private windowScrollY: number;
  private werePopupsShown = false;

  constructor(protected rendererService: RendererService,
    protected pubsub: PubSubService,
    protected userService: UserService,
    protected locale: LocaleService,
    protected quickbetDepositService: QuickbetDepositService,
    protected device: DeviceService,
    protected infoDialog: InfoDialogService,
    protected quickbetService: QuickbetService,
    protected quickbetDataProviderService: QuickbetDataProviderService,
    protected quickbetNotificationService: QuickbetNotificationService,
    protected cmsService: CmsService,
    protected windowRefService: WindowRefService,
    protected domToolsService: DomToolsService,
    protected router: Router,
    protected quickbetUpdateService: QuickbetUpdateService,
    protected changeDetectorRef: ChangeDetectorRef
  ) {
    this.viewState = 'initial';
    this.placeBetPending = { state: false };
    this.betReceipt = null;
    this.loginAndPlaceBets = false;
  }

  ngOnInit(): void {
    this.initialTitle = this.title;
    this.BODY_CLASS = this.bodyClass || 'quickbet-opened';

    // Allow place bet on 'login and place bets' action

    this.pubsub.subscribe(this.QB_PANEL_NAME, [this.pubsub.API.SUCCESSFUL_LOGIN, this.pubsub.API.SESSION_LOGOUT], placeBet => {
      if (this.isState('receipt') || this.userService.isInShopUser()) {
        this.closePanel();
        return;
      }

      if (!this.werePopupsShown) {
        this.loginAndPlaceBets = true;
        this.selection.updateCurrency();
        this.quickbetDepositService.init(true);
      } else {
        this.goToState('initial');
        this.quickbetNotificationService.clear();
        this.reuseSelection();
      }
    });

    // stop placing bet if notification popup is displayed after used has logged in
    this.pubsub.subscribe(this.QB_PANEL_NAME,  this.pubsub.API.USER_INTERACTION_REQUIRED, () => {
      this.werePopupsShown = true;
      this.loginAndPlaceBets = false;

      // update quick deposit model to show insufficient funds message
      this.quickbetDepositService.update(this.selection.stake, this.selection.isEachWay);
    });

    // Place bets on 'login and place bets' action after all popup will be shown
    this.pubsub.subscribe(this.QB_PANEL_NAME, this.pubsub.API.LOGIN_POPUPS_END, () => {
      this.werePopupsShown = false;
      if (this.loginAndPlaceBets) {
        this.loginAndPlaceBets = false;
        this.pubsub.subscribe(this.QB_PANEL_NAME, this.pubsub.API.PAYMENT_ACCOUNTS_PASSED, () => {
          this.placeBet();
        });
        this.quickbetDepositService.init(true);
      }
    });

    this.pubsub.subscribe(this.QB_PANEL_NAME, this.pubsub.API.UPDATE_QUICKBET_NOTIFICATION, message => {
      if (message && message.msg) {
        this.quickbetNotificationService.saveErrorMessage(message.msg, message.type);
      }
    });

    this.pubsub.subscribe(this.QB_PANEL_NAME, this.pubsub.API.QUICKBET_CARD_CHANGE, () => {
      this.quickbetNotificationService.clear();

      if (this.selection && this.selection.stake) {
        this.quickbetDepositService.update(this.selection.stake, this.selection.isEachWay);
      }
    });

    this.pubsub.subscribe(this.QB_PANEL_NAME, `SELECTION_PRICE_UPDATE_${this.selection.outcomeId}`, () => {
      this.changeDetectorRef.detectChanges();
    });

    this.toggleBodyScroll(true);
    // Quickbet Receipt Subscriber(get receipt data from quickbet/yourcall components)
    this.placeBetListener();

    if (this.quickbetService.acceptChangedBoost()) {
      this.selection.isBoostActive = true;
      this.placeBet();
    }

    this.cmsService.getSystemConfig().subscribe((config) => this.sysConfig = config);

    // Slide up(animation) quickbet panel
    this.windowRefService.nativeWindow.setTimeout(() => {
      this.slideUpAnimation = true;
      this.changeDetectorRef.detectChanges();
    }, 0);

    this.eventSuspensionSubscription = this.quickbetUpdateService.getEventSuspension().subscribe((status: ISuspendedOutcomeError) => {
      this.placeSuspendedErr = status;
      this.showSuspendedNotification = status.disableBet;
      this.showPriceChangeMessage = false;
    });
    this.priceChangeSubscription = this.quickbetUpdateService.getPriceChange().subscribe((msg: string) => {
      if (!this.showSuspendedNotification) {
        this.selection.onStakeChange();
        this.estimatedReturnAfterPriceChange = parseFloat(this.selection.potentialPayout);
        this.showPriceChangeMessage = true;
        this.priceChangeText = msg;
      }
    });

    this.setupModificationWatchDogForLogInAndQuickBetPending();
  }

  ngOnDestroy(): void {
    this.toggleBodyScroll(false);
    this.pubsub.unsubscribe(this.QB_PANEL_NAME);

    // remove from storage data if not a qucikbet
    if (!this.isQuickbet()) {
      this.quickbetService.removeQBStateFromStorage();
    }
    this.eventSuspensionSubscription.unsubscribe();
    this.priceChangeSubscription.unsubscribe();
  }

  ngAfterContentInit(): void {
    this.pubsub.publishSync(this.pubsub.API.AFTER_PANEL_RENDER);
  }

  /**
   * Required to check weather is betslip or quickbet
   * @return {boolean}
   */
  isQuickbet(): boolean {
    return this.BODY_CLASS === 'quickbet-opened';
  }

  /**
   * Checks if passed state is active.
   * @param {string} state
   * @return {boolean}
   */
  isState(state: string): boolean {
    return this.selection && !this.selection.error && this.viewState === state;
  }

  /**
   * Changes active state
   * @param {string} state
   */
  goToState(state: string): void {
    this.viewState = state;
  }

  /**
   * Closes panel.
   */
  closePanel(event?: Event): void {
    !!event && event.stopPropagation();
    this.quickbetDepositService.clearQuickDepositModel();
    this.pubsub.publishSync(this.pubsub.API.ODDS_BOOST_CHANGE);
    this.closeFnHandler();
    this.quickbetNotificationService.clear();
  }

  /**
   * Places bet from active selection.
   */
  placeBet(): void {
    const bet = this.selection.formatBet(),
      quickDepositModel = this.quickbetDepositService.quickDepositModel;
    this.quickbetDepositService.update(this.selection.stake);

    if (!this.device.isOnline()) {
      this.infoDialog.openConnectionLostPopup();
    } else if (!this.userService.status) {
      this.pubsub.publish(this.pubsub.API.OPEN_LOGIN_DIALOG, {
        placeBet: 'quickbet',
        moduleName: 'quickbet'
      });
    } else {
      if (!quickDepositModel.neededAmountForPlaceBet && !this.placeBetPending.state && this.userService.bppToken) {
        // Show Place bet spinner
        this.placeBetPending.state = true;

        if (this.selection.reboost) {
          this.quickbetService.activateReboost();
          this.selection.reboost = false;
          this.reuseSelection();
          return;
        }

        this.placeBetFnHandler();
        this.quickbetDataProviderService.quickbetPlaceBetListener.next(bet);
      }
    }
  }

  /**
   * Reuse selection handler.
   */
  reuseSelection(): void {
    if (this.isQuickbet()) {
      this.pubsub.publish(this.pubsub.API.REUSE_QUICKBET_SELECTION, this.selection.requestData);
      this.pubsub.publish(this.pubsub.API.ODDS_BOOST_CHANGE, this.selection.isBoostActive);
    } else {
      this.reuseSelectionFnHandler();
    }
  }

  placeBetFnHandler(): void {
    this.placeBetFn.emit();
  }

  closeFnHandler(): void {
    if (this.viewState === 'receipt' && !this.isQuickbet()) {
      this.closeFn.emit(this.isState('receipt'));
    } else if (this.isQuickbet()) {
      this.closeFn.emit(this.isState('receipt'));
      if (this.quickbetNotificationService.config.type && this.quickbetNotificationService.config.type === 'warning') {
        this.quickbetNotificationService.clear();
      }
    } else {
      this.reuseSelectionFnHandler();
    }
  }

  reuseSelectionFnHandler(): void {
    this.reuseSelectionFn.emit();
  }

  addToBetslipFnHandler(): void {
    this.addToBetslipFn.emit();
  }

  /**
   * closes quick deposit window
   */
  onCloseQuickDepositWindow(): void {
    this.quickbetDepositService.update(this.selection.stake);
    this.showIFrame = false;
    this.quickDepositFormExpanded = false;
    this.showPriceChangeMessage = false;
  }

  closeIFrame(): void {
    this.showIFrame = false;
    this.quickDepositFormExpanded = false;
    this.placeBet();
  }

  onOpenIframe(): void {
    this.showIFrame = true;
    this.iframeLoaded = true;
    this.showPriceChangeMessage = false;
  }

  getTotalStake(): number {
    return this.selection.isEachWay ? +this.selection.stake * 2 : +this.selection.stake;
  }

  onQuickDepositEvents({ output }): void {
    if (output === 'openIframeEmit') {
      this.onOpenIframe();
    } else if (output === 'closeWindow') {
      this.onCloseQuickDepositWindow();
    } else if (output === 'closeIframeEmit') {
      this.closeIFrame();
    }
  }

  /**
   * Subscribe on login dialog and login events to prevent QuickBet stake update
   * through QuickBetPanel - by setting _loginAndPlaceBets_ variable to true.
   */
  private setupModificationWatchDogForLogInAndQuickBetPending() {
    this.pubsub.subscribe(this.QB_PANEL_NAME, this.pubsub.API.SESSION_LOGIN, () => {
      this.loginAndPlaceBets = true;
    });
    this.pubsub.subscribe(this.QB_PANEL_NAME, this.pubsub.API.LOGIN_DIALOG_CLOSED, () => {
      if (this.userService.loginPending) {
        this.loginAndPlaceBets = true;
        this.pubsub.subscribe(this.QB_PANEL_NAME, this.pubsub.API.FAILED_LOGIN, () => {
          this.loginAndPlaceBets = false;
        });
      }
    });
  }

  private placeBetListener() {
    this.quickbeReceiptSubscriber = this.quickbetDataProviderService.quickbetReceiptListener
      .subscribe(betReceipt => {
        this.placeBetPending.state = false;

        if (_.isArray(betReceipt) && betReceipt[0].error) {
          console.warn('Error while placing quickbet', betReceipt[0].error);
          this.quickbetNotificationService.saveErrorMessageWithCode(betReceipt[0].error, 'warning', '', betReceipt[0].errorCode);
        } else {
          // Go to bet receipt state
          if (this.isQuickbet()) {
            this.betReceipt = _.isArray(betReceipt) && betReceipt.length && betReceipt[0];
            this.title = this.locale.getString(`quickbet.betReceiptTitle`);
          } else {
            this.setYCBetReceiptProps(<IYCBetReceiptModel>betReceipt);
            this.title = this.locale.getString(`quickbet.${this.isFiveASideBet ? 'fiveASideBetreceipt' : 'yourCallBetreceipt'}`);
          }

          // Go to bet receipt state
          this.goToState('receipt');

          this.quickbeReceiptSubscriber.unsubscribe();
        }
      });
  }

  /**
   * Toggles class to body element to enable/disable scrolling depending on passed state.
   * @param {boolean} state
   * @private
   */
  private toggleBodyScroll(state: boolean): void {
    if (this.windowRefService.document.body) {
      state ? this.rendererService.renderer.addClass(this.windowRefService.document.body, this.BODY_CLASS) :
        this.rendererService.renderer.removeClass(this.windowRefService.document.body, this.BODY_CLASS);
      this.handleScroll(state);
    }
  }

  /**
   * Handle background scrolling for android wrappers
   */
  private handleScroll(state: boolean): void {
    if (this.device.isWrapper && this.device.isAndroid) {
      const htmlElement = this.windowRefService.document.querySelector('html');

      if (state) {
        this.windowScrollY = this.windowRefService.nativeWindow.pageYOffset;
        this.rendererService.renderer.addClass(htmlElement, this.BODY_CLASS);
      } else {
        this.rendererService.renderer.removeClass(htmlElement, this.BODY_CLASS);
        this.domToolsService.scrollPageTop(this.windowScrollY);
      }
    }
  }

  /**
   * Sets YC bet receipt properties
   * @param betReceipt {object}
   * @private
   */
  private setYCBetReceiptProps(betReceipt: IYCBetReceiptModel): void {
    this.betReceipt = {
      selections: true,
      date: betReceipt.data.date,
      payout: { potential: betReceipt.selection.potentialPayout },
      oddsValue: betReceipt.selection.oldOddsValue,
      receipt: betReceipt.data.receipt,
      stake: { stakePerLine: betReceipt.data.totalStake, amount: betReceipt.data.totalStake }
    };
    this.betReceipt.stake.freebet = betReceipt.selection.freebet ? betReceipt.selection.freebet.freebetTokenValue : '0';
  }
}
