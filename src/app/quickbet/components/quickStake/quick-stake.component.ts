import {
  Component,
  Input,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { UserService } from '@core/services/user/user.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'quick-stake',
  templateUrl: 'quick-stake.component.html',
  styleUrls: ['quick-stake.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuickStakeComponent implements OnInit, OnDestroy {

  @Input() disabled?: boolean;
  @Output() readonly quickStakeSelect: EventEmitter<string> = new EventEmitter();

  quickStakePrefix: string;
  quickStakeItems: string[];

  private readonly title = 'QuickStakeController';

  constructor(
    private user: UserService,
    private pubsubService: PubSubService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.quickStakePrefix = `+${this.user.currencySymbol}`;
    this.quickStakeItems = ['5', '10', '50', '100'];

    this.reformatKrCurrency = this.reformatKrCurrency.bind(this);
  }

  ngOnInit(): void {
    this.pubsubService.subscribe(this.title, [this.pubsubService.API.SESSION_LOGIN,
      this.pubsubService.API.SESSION_LOGOUT], this.reformatKrCurrency);

    this.reformatKrCurrency();
  }

  ngOnDestroy(): void {
    this.pubsubService.unsubscribe(this.title);
  }

  setQuickStake(value: string): void {
    this.quickStakeSelect.emit(value);
  }

  /**
   * ngFor trackBy function
   * @param {number} index
   * @return {number}
   */
  trackByIndex(index: number): number {
    return index;
  }

  /**
   * Reformats quick stake values for currency
   * @private
   */
  private reformatKrCurrency(): void {
    const radix = 10;

    this.quickStakePrefix = `+${this.user.currencySymbol}`;

    if (this.user.currencySymbol === 'Kr') {
      if (!this.isFormatted()) {
        this.quickStakeItems = this.quickStakeItems.map(num => (parseInt(num, radix) * 10).toString());
      }
    } else if (this.isFormatted()) {
      this.quickStakeItems = this.quickStakeItems.map( num => (parseInt(num, radix) / 10).toString());
    }

    this.changeDetectorRef.markForCheck();
  }

  /**
   * Is quick stake already formatted
   * @private
   */
  private isFormatted(): boolean {
    return Number(this.quickStakeItems[0]) % 10 === 0;
  }
}

