import { QuickStakeComponent } from '@app/quickbet/components/quickStake/quick-stake.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';
import { fakeAsync, tick } from '@angular/core/testing';

describe('QuickStakeComponent', () => {
  let component;
  let userService;
  let pubsubService;
  let changeDetectorRef;

  beforeEach(() => {
    userService = {
      currencySymbol: '$'
    };
    pubsubService = {
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: pubSubApi
    };

    changeDetectorRef = {
      markForCheck: jasmine.createSpy('markForCheck')
    };

    component = new QuickStakeComponent(userService, pubsubService, changeDetectorRef);
  });

  describe('ngOnInit', () => {
    it('should subscribe to session events to handle currency change', () => {
      component.ngOnInit();

      expect(pubsubService.subscribe).toHaveBeenCalledWith('QuickStakeController', [pubsubService.API.SESSION_LOGIN,
        pubsubService.API.SESSION_LOGOUT], component.reformatKrCurrency);

      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    });

    it('should format quick stakes for Kr user', () => {
      userService.currencySymbol = 'Kr';
      component.ngOnInit();

      expect(component.quickStakePrefix).toEqual('+Kr');
      expect(component.quickStakeItems).toEqual(['50', '100', '500', '1000']);
    });

    it('should format quick stakes for Kr user is formatted', () => {
      userService.currencySymbol = 'Kr';
      component.quickStakeItems = ['20', '30', '40'];
      component.ngOnInit();

      expect(component.quickStakePrefix).toEqual('+Kr');
      expect(component.quickStakeItems).toEqual(['20', '30', '40']);
    });

    it('should format quick stakes for Kr user', () => {
      userService.currencySymbol = '$';
      component.ngOnInit();

      expect(component.quickStakePrefix).toEqual('+$');
      expect(component.quickStakeItems).toEqual(['5', '10', '50', '100']);
    });

    it('should format quick stakes is formatted', () => {
      userService.currencySymbol = '$';
      component.quickStakeItems = ['20', '30', '40'];
      component.ngOnInit();

      expect(component.quickStakePrefix).toEqual('+$');
      expect(component.quickStakeItems).toEqual(['2', '3', '4']);
    });
  });

  it('should unsubscribe onDestroy', () => {
    component.ngOnDestroy();

    expect(pubsubService.unsubscribe).toHaveBeenCalledWith('QuickStakeController');
  });

  it('should handle quick stake select', fakeAsync(() => {
    const notifyListener = jasmine.createSpy('notifyListener');
    const quickStake = '50';

    component.quickStakeSelect.subscribe(notifyListener);
    component.setQuickStake(quickStake);
    tick();

    expect(notifyListener).toHaveBeenCalledWith(quickStake);
  }));

  it('trackByIndex', () => {
    const result = component.trackByIndex(2);

    expect(result).toBe(2);
  });
});
