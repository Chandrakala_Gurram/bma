export const quickbetConstants = {
  SIMPLE_TYPE: 'simple',
  SCORECAST_TYPE: 'scorecast',
  CORRECT_SCORE_MEANING_CODE: 'CS',
  AVAILABLE_STATUS: 'A',
  SUSPENDED_STATUS: 'S'
};
