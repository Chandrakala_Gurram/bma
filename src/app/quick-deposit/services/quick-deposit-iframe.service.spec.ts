import { QuickDepositIframeService } from '@app/quick-deposit/services/quick-deposit-iframe.service';
import { of } from 'rxjs';

describe('QuickDepositIframeService', () => {
  let service: QuickDepositIframeService;

  let windowRef;
  let userService;
  let sanitizer;
  let device;
  let vanillaQuickDepositService;
  let vanillaNavigationService;

  const enabled = of(true);

  beforeEach(() => {
    windowRef = {
      nativeWindow: {
        clientConfig: {
          lhCashier: {
            host: 'host'
          }
        }
      }
    };
    userService = {
      ssoToken: 'token',
      username: 'name'
    };
    sanitizer = {
      bypassSecurityTrustResourceUrl: jasmine.createSpy().and.callFake((param) => param)
    };
    device = {
      viewType: 'mobile'
    };
    vanillaQuickDepositService = {
      isEnabled: jasmine.createSpy().and.returnValue(enabled)
    };
    vanillaNavigationService = {
      goToCashierDeposit: jasmine.createSpy()
    };

    service = new QuickDepositIframeService(
      windowRef,
      userService,
      sanitizer,
      device,
      vanillaQuickDepositService,
      vanillaNavigationService
    );
  });

  it('should return correct url', () => {
    const expectedUrl = 'host/cashierapp/cashier.html?userId=cl_name&brandId=CORAL&'
      + 'productId=SPORTSBOOK&channelId=MW&langId=en&sessionKey=token&stake=10&estimatedReturn=500#/';
    const url = service.getUrl(10, 500);
    expect(url).toBe(expectedUrl);
  });

  it('#isEnabled', () => {
    const result = service.isEnabled();
    expect(result).toBe(enabled);
    expect(vanillaQuickDepositService.isEnabled).toHaveBeenCalled();
  });

  it('#redirectToDepositPage', () => {
    service.redirectToDepositPage();
    expect(vanillaNavigationService.goToCashierDeposit).toHaveBeenCalledWith(jasmine.objectContaining({}));
  });
});
