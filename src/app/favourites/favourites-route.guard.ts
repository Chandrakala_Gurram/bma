import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { FavouritesService } from '@app/favourites/services/favourites.service';

@Injectable()
export class FavouritesRouteGuard implements CanActivate {

  constructor(private favouritesService: FavouritesService) {
  }

  canActivate() {
    return this.favouritesService.showFavourites();
  }
}
