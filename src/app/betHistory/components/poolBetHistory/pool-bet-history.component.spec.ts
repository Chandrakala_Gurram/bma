import { PoolBetHistoryComponent } from './pool-bet-history.component';
import { of, throwError } from 'rxjs';
import quadpodBet from './../../mocks/quadpotBet.mock';
import trifectaBet from './../../mocks/trifectaBet.mock';

describe('PoolBetHistoryComponent', () => {
  let component: PoolBetHistoryComponent;

  let localeService;
  let userService;
  let betHistoryMainService;
  let timeService;
  let cashoutMapIndexService;
  let cashOutLiveUpdatesSubscribeService;
  let sbFiltersService;
  let currencyPipe;
  let toteBetsExtendingService;
  let pubsub;

  beforeEach(() => {

    localeService = {
      getString: jasmine.createSpy('getString').and.callFake(x => x)
    };
    userService = {};
    betHistoryMainService = {
      generateBetsMap: jasmine.createSpy('generateBetsMap').and.returnValue([]),
      getBetStatus: jasmine.createSpy('getBetStatus')
    };
    timeService = {};
    cashoutMapIndexService = {};
    cashOutLiveUpdatesSubscribeService = {
      addWatch: jasmine.createSpy('addWatch'),
      unsubscribeFromLiveUpdates: jasmine.createSpy('unsubscribeFromLiveUpdates')
    };
    sbFiltersService = {
      orderOutcomeEntities: jasmine.createSpy('orderOutcomeEntities').and.callFake(x => x)
    };
    currencyPipe = {
      transform: jasmine.createSpy('transform')
    };
    toteBetsExtendingService = {
      extendToteBetsWithEvents: jasmine.createSpy('extendToteBetsWithEvents').and.returnValue(of([]))
    };
    pubsub = {
      API: {
        RELOAD_COMPONENTS: '',
      } as Record<string, string>,
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy('unsubscribe')
    };

    component = new PoolBetHistoryComponent(
      localeService,
      userService,
      betHistoryMainService,
      timeService,
      cashoutMapIndexService,
      cashOutLiveUpdatesSubscribeService,
      sbFiltersService,
      currencyPipe,
      toteBetsExtendingService,
      pubsub
    );

    component.poolBets = [
      quadpodBet as any,
      trifectaBet as any,
      {
        poolType: 'unknown'
      } as any
    ];
  });

  describe('constructor', () => {
    it('should define properties from super class', () => {
      expect(component.isUsedFromWidget).toEqual(false);
    });
  });

  describe('#subscirbeForLiveUpdates', () => {
    it('should call subscirbeForLiveUpdates success', () => {
      component['subscirbeForLiveUpdates']();

      expect(toteBetsExtendingService.extendToteBetsWithEvents).toHaveBeenCalledWith([]);
      expect(betHistoryMainService.generateBetsMap).toHaveBeenCalledWith([]);
      expect(toteBetsExtendingService.extendToteBetsWithEvents).toHaveBeenCalledWith([]);
    });

    it('should call subscirbeForLiveUpdates error', () => {
      toteBetsExtendingService.extendToteBetsWithEvents.and.returnValue(throwError('error'));
      component['subscirbeForLiveUpdates']();

      expect(betHistoryMainService.generateBetsMap).toHaveBeenCalledWith([]);
      expect(toteBetsExtendingService.extendToteBetsWithEvents).toHaveBeenCalledWith([]);
      expect(cashOutLiveUpdatesSubscribeService.addWatch).not.toHaveBeenCalled();
    });
  });

  describe('ngOnDestroy', () => {
    it('should unsubscribe clear comonent subscriptions', () => {
      component.ngOnDestroy();
      expect(cashOutLiveUpdatesSubscribeService.unsubscribeFromLiveUpdates).toHaveBeenCalled();
      expect(pubsub.unsubscribe).toHaveBeenCalledWith('PoolBetHistoryComponent');
    });
  });

  it('trackByBet should return unique identifier for bet object', () => {
    const bet = {
      id: 159,
      receipt: 'r/p121234234',
      isSuspended: true
    };
    expect(component.trackByBet(3, bet as any)).toEqual('3159r/p121234234true');
  });
  describe('ngOnInit',() => {
    it('should set noBetsMessage for Bet History tab', () => {
      component.isBetHistoryTab = true;
      component.ngOnInit();
      expect(component.noBetsMessage).toEqual('bethistory.noHistoryInfo');
    });
    it('should set noBetsMessage for Open Bets tab', () => {
      component.isBetHistoryTab = false;
      component.ngOnInit();
      expect(component.noBetsMessage).toEqual('bethistory.noOpenBets');
    });
  });
  describe('ngOnChanges', () => {
    it('ngOnChanges souldn`t init comopnent if no bets received', () => {
      spyOn(component as any, 'initializeBets');
      component.ngOnChanges({} as any);
      expect(component['initializeBets']).not.toHaveBeenCalled();
    });
    it('ngOnChanges sould init comopnent if bets received', () => {
      spyOn(component as any, 'initializeBets');
      component.ngOnChanges({ poolBets: {} } as any);
      expect(component['initializeBets']).toHaveBeenCalled();
    });
  });

  describe('generatePoolHistory', () => {
    it('initialize poolHistory with bet models', () => {
      expect(component.poolHistory.length).toEqual(0);
      component['generatePoolHistory']();
      expect(component.poolHistory.length).toEqual(2);
      expect(component.poolHistory[0].constructor.name).toEqual('TotePotPoolBetClass');
      expect(component.poolHistory[1].constructor.name).toEqual('TotePoolBet');
    });
  });

  describe('initializeBets', () => {
    it('should reinitialize component', () => {
      spyOn(component as any, 'generatePoolHistory');
      spyOn(component as any, 'unsubcribeFromLiveUpdates');
      spyOn(component as any, 'subscirbeForLiveUpdates');
      component['initializeBets']();
      expect(component['generatePoolHistory']).toHaveBeenCalled();
      expect(component['unsubcribeFromLiveUpdates']).toHaveBeenCalled();
      expect(component['subscirbeForLiveUpdates']).toHaveBeenCalled();
    });
  });
});
