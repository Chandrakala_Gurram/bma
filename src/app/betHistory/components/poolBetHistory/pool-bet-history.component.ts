import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import * as _ from 'underscore';

import { LocaleService } from '@core/services/locale/locale.service';
import { UserService } from '@core/services/user/user.service';
import { BetHistoryMainService } from '@app/betHistory/services/betHistoryMain/bet-history-main.service';
import { TimeService } from '@core/services/time/time.service';
import { CashoutMapIndexService } from '@app/betHistory/services/cashOutMapIndex/cashout-map-index.service';
import {
  CashOutLiveUpdatesSubscribeService
} from '@app/betHistory/services/cashOutLiveUpdatesSubscribeService/cashOutLiveUpdatesSubscribeService';
import { betHistoryConstants } from '@app/betHistory/constants/bet-history.constant';
import { IBetHistoryBet, IBetHistoryPoolBet } from '@app/betHistory/models/bet-history.model';
import { SbFiltersService } from '@sb/services/sbFilters/sb-filters.service';

import TotePotPoolBet from '../../betModels/totePotPoolBetClass/tote-pot-pool-bet.class';
import TotePoolBet from '../../betModels/totePoolBet/tote-pool-bet.class';
import FootballJackpotBet from '../../betModels/footballJackpotBet/football-jackpot-bet.class';
import { CurrencyPipe } from '@angular/common';
import { ToteBetsExtendingService } from '@app/betHistory/services/toteBetsExtending/tote-bets-extending.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { UsedFromWidgetAbstractComponent } from '@core/abstract-components/used-from-widget-abstract.component';


@Component({
  selector: 'pool-bet-history',
  templateUrl: './pool-bet-history.component.html'
})
export class PoolBetHistoryComponent extends UsedFromWidgetAbstractComponent implements OnInit, OnChanges, OnDestroy {
  @Input() poolBets: IBetHistoryPoolBet[];
  @Input() isBetHistoryTab: boolean;

  noBetsMessage: string;
  betModelsMap: { [key: string]: IBetHistoryPoolBet};
  poolHistory: IBetHistoryPoolBet[] = [];
  ctrlName: string = 'PoolBetHistoryComponent';

  constructor(
    private localeService: LocaleService,
    private userService: UserService,
    private betHistoryMainService: BetHistoryMainService,
    private timeService: TimeService,
    private cashoutMapIndexService: CashoutMapIndexService,
    private cashOutLiveUpdatesSubscribeService: CashOutLiveUpdatesSubscribeService,
    private sbFiltersService: SbFiltersService,
    private currencyPipe: CurrencyPipe,
    private toteBetsExtendingService: ToteBetsExtendingService,
    private pubsub: PubSubService,
  ) {
    super();
    this.betModelsMap = this.generateBetModelsMap();
    this.pubsub.subscribe(this.ctrlName, this.pubsub.API.RELOAD_COMPONENTS, () => {
        this.initializeBets();
      }
    );
  }

  ngOnInit(): void {
    this.noBetsMessage = this.isBetHistoryTab
      ? this.localeService.getString('bethistory.noHistoryInfo')
      : this.localeService.getString('bethistory.noOpenBets');
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.poolBets) { return; }
    this.initializeBets();
  }

  /**
   * Track by function
   * @param {number} index
   * @param {class} item
   * @returns {string}
   */
  trackByBet(index: number, item: IBetHistoryBet): string {
    return `${index}${item.id}${item.receipt}${item.isSuspended}`;
  }

  ngOnDestroy(): void {
    this.unsubcribeFromLiveUpdates();
    this.pubsub.unsubscribe(this.ctrlName);
  }

  /**
   * Generates bets objects and updates subscriptions to live updates
   */
  private initializeBets(): void {
    this.generatePoolHistory();
    this.unsubcribeFromLiveUpdates();
    this.subscirbeForLiveUpdates();
  }

  /**
   * Generate map: pool Type to pool Model
   * @returns Object - bet models map
   * @private
   */
  private generateBetModelsMap(): { [key: string]: IBetHistoryPoolBet} {
    const betModelsMap = {},
      TOTE_MULTIPLE_LEGS_BET_TYPES: string[] = betHistoryConstants.TOTE_MULTIPLE_LEGS_BET_TYPES,
      allToteBetTypes: string[] = betHistoryConstants.TOTE_ONE_LEG_BET_TYPES.concat(TOTE_MULTIPLE_LEGS_BET_TYPES);
    betModelsMap[betHistoryConstants.BET_TYPES.FOOTBALL_JACKPOT] = FootballJackpotBet;

    _.reduce(allToteBetTypes, (memo: IBetHistoryPoolBet, betType: string, index: number) => {
      memo[betType] = index < betHistoryConstants.TOTE_ONE_LEG_BET_TYPES.length
        ? TotePoolBet
        : TotePotPoolBet;
      return memo;
    }, betModelsMap);
    return betModelsMap;
  }

  /**
   * Generate Pool History
   * @private
   */
  private generatePoolHistory(): void {
    const tempData: IBetHistoryPoolBet[] = [];
    this.poolBets.forEach((item: IBetHistoryPoolBet) => {
      const PoolItemBetModel = this.betModelsMap[item.poolType];
      if (PoolItemBetModel) {
        const bet = new PoolItemBetModel(
          item,
          this.betHistoryMainService,
          this.userService,
          this.localeService,
          this.timeService,
          this.cashoutMapIndexService,
          this.currencyPipe,
          this.sbFiltersService
        );
        tempData.push(bet);
      }
    });
    this.poolHistory = tempData;
  }

  /**
   * Subscribe for live updates
   * @private
   */
  private subscirbeForLiveUpdates(): void {
    const betsMap: { [key: string]: (TotePotPoolBet | TotePoolBet)}
      = (this.betHistoryMainService.generateBetsMap(this.poolHistory) as { [key: string]: (TotePotPoolBet | TotePoolBet)});

    this.toteBetsExtendingService.extendToteBetsWithEvents(betsMap)
      .subscribe(() => {
        this.cashOutLiveUpdatesSubscribeService.addWatch(betsMap);
      }, () => { }); // added error handling for BMA-41334
  }

  /**
   * Unsubscribe from live updates
   * @private
   */
  private unsubcribeFromLiveUpdates(): void {
    this.cashOutLiveUpdatesSubscribeService.unsubscribeFromLiveUpdates();
  }
}
