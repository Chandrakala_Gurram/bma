import { FiltersService } from '@core/services/filters/filters.service';
import { EventHeaderComponent } from '@app/betHistory/components/eventHeader/event-header.component';
import { CoreToolsService } from '@core/services/coreTools/core-tools.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { EventService } from '@sb/services/event/event.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { fakeAsync, tick } from '@angular/core/testing';

describe('EventHeaderComponent', () => {

  let component: EventHeaderComponent;

  const today = new Date();
  const future = new Date();
  future.setDate(future.getDate() + 1);

  function fakeCall(time) {
      const formatted = new Date(time);
      /* tslint:disable */
      return time === `${today}` ? `${formatted.getHours()}:${today.getMinutes()}, Today` :
        `${formatted.getHours()}:${formatted.getMinutes()} ${future.toLocaleString('en-US', { day: '2-digit' })} ${formatted.toLocaleString('en-US', { month: 'short' })}`;
      /* tslint:enable */
  }

  const timeServiceStub = {
    getEventTime: jasmine.createSpy().and.callFake(fakeCall),
    animationDelay: 5000,
    formatByPattern: jasmine.createSpy()
  };

  const filterServiceStub: Partial<FiltersService> = {};
  const coreToolsStub: Partial<CoreToolsService> = {
    getDaySuffix: jasmine.createSpy('getDaySuffix').and.returnValue('st')
  };
  const localeStub: Partial<LocaleService> = {
    getString: () => 'test'
  };
  const eventServiceStub: Partial<EventService> = {
    isLiveStreamAvailable: () => {
      return { liveStreamAvailable: false };
    }
  } as any;
  const windowRefStub: Partial<WindowRefService> = {
    nativeWindow: {
      clearTimeout: jasmine.createSpy('clearTimeout'),
      setTimeout: jasmine.createSpy('setTimeout').and.callFake((fn, time) => { fn(); })
    }
  };

  beforeEach(() => {
    component = new EventHeaderComponent(
      timeServiceStub as any,
      coreToolsStub as any,
      localeStub as any,
      eventServiceStub as any,
      windowRefStub as any,
      filterServiceStub as any
    );
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  it('should check correct today event startTime format', fakeAsync(() => {
    component.event = { startTime: today } as any;
    const formatted = `${today.getHours()}:${today.getMinutes()}, Today`;
    component.ngOnInit();
    tick();

    expect(timeServiceStub.getEventTime).toHaveBeenCalledWith(`${today}`);
    expect(component.timerLabel).toEqual(formatted);
  }));

  it('should check correct future event startTime format', fakeAsync(() => {
    /* tslint:disable */
    const formatted = `${future.getHours()}:${future.getMinutes()} ${future.toLocaleString('en-US', { day: '2-digit' })} ${future.toLocaleString('en-US', { month: 'short' })}`;
    /* tslint:enable */

    component.event = { startTime: future } as any;
    component.ngOnInit();
    tick();

    expect(timeServiceStub.getEventTime).toHaveBeenCalledWith(`${future}`);
    expect(component.timerLabel).toEqual(formatted);
  }));


  describe('setPlaceWithFormat', () => {
    it('when place = 1', () => {
      const place = '1';
      component['setPlaceWithFormat'](place);

      expect(coreToolsStub.getDaySuffix).toHaveBeenCalledWith(place);
      expect(component.placeWithFormat).toEqual(`1st Place`);
    });

    it('when place = 2', () => {
      const place = '2';
      coreToolsStub.getDaySuffix = jasmine.createSpy('getDaySuffix').and.returnValue('nd');
      component['setPlaceWithFormat'](place);

      expect(coreToolsStub.getDaySuffix).toHaveBeenCalledWith(place);
      expect(component.placeWithFormat).toEqual(`2nd Place`);
    });

    it('when place = 3', () => {
      const place = '3';
      coreToolsStub.getDaySuffix = jasmine.createSpy('getDaySuffix').and.returnValue('rd');
      component['setPlaceWithFormat'](place);

      expect(coreToolsStub.getDaySuffix).toHaveBeenCalledWith(place);
      expect(component.placeWithFormat).toEqual(`3rd Place`);
    });

    it('when place = 10', () => {
      const place = '10';
      coreToolsStub.getDaySuffix = jasmine.createSpy('getDaySuffix').and.returnValue('th');
      component['setPlaceWithFormat'](place);

      expect(coreToolsStub.getDaySuffix).toHaveBeenCalledWith(place);
      expect(component.placeWithFormat).toEqual(`10th Place`);
    });
  });

  describe('hasScores', () => {
    describe('football', () => {
      it('falsy 3', () => {
        component.isFootball = true;
        component.event = {} as any;
        expect(component.hasScores).toBeFalsy();
      });

      it('falsy 4', () => {
        component.isFootball = true;
        component.event = {
          comments: {}
        } as any;
        expect(component.hasScores).toBeFalsy();
      });

      it('falsy 5', () => {
        component.isFootball = true;
        component.event = {
          comments: { teams: {} }
        } as any;
        expect(component.hasScores).toBeFalsy();
      });

      it('falsy 6', () => {
        component.isFootball = true;
        component.event = {
          comments: {
            teams: { home: {} }
          }
        } as any;
        expect(component.hasScores).toBeFalsy();
      });

      it('should return false if score is not defined', () => {
        component.isFootball = true;
        component.event = {
          comments: {
            teams: {
              home: { score: undefined }
            }
          }
        } as any;
        expect(component.hasScores).toBeFalsy();
      });

      it('should return true if score is a string', () => {
        component.isFootball = true;
        component.event = {
          comments: {
            teams: {
              home: { score: '0' }
            }
          }
        } as any;
        expect(component.hasScores).toBeTruthy();
      });

      it('should return True if some score is defined', () => {
        component.isFootball = true;
        component.event = {
          comments: {
            teams: {
              home: { score: undefined },
              away: { score: '1' }
            }
          }
        } as any;
        expect(component.hasScores).toBeTruthy();
      });
    });

    it('should return true if score is a number', () => {
      component.isFootball = true;
      component.event = {
        comments: {
          teams: {
            home: { score: 0 }
          }
        }
      } as any;
      expect(component.hasScores).toBeTruthy();
    });
  });

  describe('Cricket', () => {
    beforeEach(() => {
      component.event = {
        comments: {
          teams: {
            home: { score: 'A U21' },
            away: { score: 'A 234/5d' }
          }
        }
      } as any;
    });

    it('should return true if score is a string', () => {
      expect(component.hasScores).toBeTruthy();
    });

    it('should return true if away score is a number', () => {
      delete component.event.comments.teams.home;
      component.event.comments.teams.away.score = 0;
      expect(component.hasScores).toBeTruthy();
    });
  });

  describe('ngOnChanges', () => {
    it('should call updateMatchTimerLabel method', () => {
      spyOn(component, 'updateMatchTimerLabel' as any);
      component['ngOnChanges']({
        runningSetIndex: {
          firstChange: false
        }
      } as any);
      expect(component['updateMatchTimerLabel']).toHaveBeenCalled();
    });
    it('should set suspended status true method if status not suspended', () => {
      component['status'] = 'suspended';
      component['ngOnChanges']({
        status: {
          firstChange: false
        }
      } as any);
      expect(component['isSuspendedEvent']).toBeTruthy();
    });
    it('should set suspended status false method if status not suspended', () => {
      component['status'] = 'open';
      component['ngOnChanges']({
        status: {
          firstChange: false
        }
      } as any);
      expect(component['isSuspendedEvent']).toBeFalsy();
    });
  });

  describe('ngOnInit', () => {
    it('should set event name as original name for horse racing event', fakeAsync(() => {
      component.event = {
        categoryId: '21',
        originalName: '16:25 Nottingham',
        name: 'Nottingham'
      } as any;

      component.ngOnInit();
      tick();

      expect(component.eventName).toBe('16:25 Nottingham');
    }));

    it('should set event name as original name for greyhounds event', fakeAsync(() => {
      component.event = {
        categoryId: '19',
        originalName: '16:25 Nottingham',
        name: 'Nottingham'
      } as any;

      component.ngOnInit();
      tick();

      expect(component.eventName).toBe('16:25 Nottingham');
    }));

    it('should set event name as event name with local time for virtual horse racing event', fakeAsync(() => {
      timeServiceStub.formatByPattern.and.returnValue('22:00');
      component.event = {
        categoryId: '39',
        classId: 285,
        startTime: 1582747200000,
        originalName: '20:00 Nottingham',
        name: 'Nottingham'
      } as any;

      component.ngOnInit();
      tick();

      expect(component.eventName).toBe('22:00 Nottingham');
    }));

    it('should set event name as event name with local time for virtual greyhounds event', fakeAsync(() => {
      timeServiceStub.formatByPattern.and.returnValue('22:00');
      component.event = {
        categoryId: '39',
        classId: 286,
        startTime: 1582747200000,
        originalName: '20:00 Nottingham',
        name: 'Nottingham'
      } as any;

      component.ngOnInit();
      tick();

      expect(component.eventName).toBe('22:00 Nottingham');
    }));

    it('should set event name as name for greyhounds event if originalName does not exists', fakeAsync(() => {
      component.event = {
        categoryId: '19',
        name: 'Nottingham'
      } as any;

      component.ngOnInit();
      tick();

      expect(component.eventName).toBe('Nottingham');
    }));

    it('should set event name as name for horse racing event if originalName does not exists', fakeAsync(() => {
      component.event = {
        categoryId: '21',
        name: 'Nottingham'
      } as any;

      component.ngOnInit();
      tick();

      expect(component.eventName).toBe('Nottingham');
    }));

    it('should set event name as name for all other sports event(not greyhounds or horse racing virtual or not)', fakeAsync(() => {
      component.event = {
        categoryId: '16',
        originalName: 'Arsenal vs Manchester City 2:0',
        name: 'Arsenal vs Manchester City'
      } as any;

      component.ngOnInit();
      tick();

      expect(component.eventName).toBe('Arsenal vs Manchester City');
    }));
  });
});
