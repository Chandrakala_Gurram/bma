import { Component, HostListener, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { of, Subscription } from 'rxjs';
import { concatMap, distinct } from 'rxjs/operators';
import * as _ from 'underscore';

import { cashoutConstants } from '../../constants/cashout.constant';
import { LocaleService } from '@core/services/locale/locale.service';
import { CashoutSectionService } from '@app/betHistory/services/cashOutSection/cash-out-section.service';
import { EditMyAccaService } from '@app/betHistory/services/editMyAcca/edit-my-acca.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { IBetHistoryBet } from '@app/betHistory/models/bet-history.model';
import { ICashOutData } from '@app/betHistory/models/cashout-section.model';
import { CashoutBet } from '@app/betHistory/betModels/cashoutBet/cashout-bet.class';
import { PlacedBet } from '@app/betHistory/betModels/placedBet/placed-bet.class';
import { IBetDetail } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { BetTrackingService } from '@lazy-modules/bybHistory/services/betTracking/bet-tracking.service';
import {
  HandleScoreboardsStatsUpdatesService
} from '@lazy-modules/bybHistory/services/handleScoreboardsStatsUpdates/handle-scoreboards-stats-updates.service';

@Component({
  selector: 'my-bets',
  templateUrl: '../cashOutBets/cash-out-bets.component.html',
  styleUrls: ['my-bets.component.less']
})
export class MyBetsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() cashoutIds: { id: number }[];
  @Input() placedBets: IBetHistoryBet[];
  @Input() cashoutBets: IBetDetail[];
  @Input() eventId: string;

  noBetsMessage: string;
  ctrlName: string = cashoutConstants.controllers.MY_BETS_CTRL;
  betLocation: string = cashoutConstants.betLocation.MY_BETS;
  loadComplete: boolean = true;
  betsMap: { [key: string ]: PlacedBet | CashoutBet };
  bets: ICashOutData[];
  isCashOutBets: boolean = false;
  openWhatIsCashOut?: Function;
  optaDisclaimer: string;
  betTrackingEnabled: boolean;

  private betTrackingEnabledSubscription: Subscription;
  private getEventIdStatisticsSubscription: Subscription;

  constructor(
    public emaService: EditMyAccaService,
    private locale: LocaleService,
    private cashOutSectionService: CashoutSectionService,
    private pubsub: PubSubService,
    private betTrackingService: BetTrackingService,
    private handleScoreboardsStatsUpdatesService: HandleScoreboardsStatsUpdatesService
  ) {
    this.noBetsMessage = this.locale.getString('bethistory.noCashoutBets');
  }

  ngOnInit(): void {
    this.init();
    this.getEventIdStatisticsSubscription = this.handleScoreboardsStatsUpdatesService.getStatisticsEventIds().pipe(
      distinct()
    ).subscribe((eventId: string) => {
      this.bets.forEach((bet: ICashOutData) => {
        if (bet.eventSource.event.includes(eventId)) {
          bet.optaDisclaimerAvailable = true;
        }
      });
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.betsMap) {
      this.removeListeners();
      this.updateBets();
    }
  }
  @HostListener('window:beforeunload', ['$event'])
  ngOnDestroy(): void {
    this.removeListeners();
    this.betTrackingEnabledSubscription && this.betTrackingEnabledSubscription.unsubscribe();
    this.getEventIdStatisticsSubscription && this.getEventIdStatisticsSubscription.unsubscribe();
  }

  trackByBet(index: number, bet: { eventSource: PlacedBet | CashoutBet, location: string }): string {
    return `${index}${bet.eventSource.betId}${bet.eventSource.receipt}`;
  }

  isCashoutError(bet: ICashOutData) {
    return this.cashOutSectionService.isCashoutError(bet.eventSource);
  }

  getCashoutError(bet: ICashOutData) {
    return this.cashOutSectionService.getCashoutError(bet.eventSource);
  }

  private updateBets(): void {
    this.bets = this.cashOutSectionService.generateBetsArray(this.betsMap, this.betLocation).filter((bet: ICashOutData) => {
      return bet.eventSource.event.indexOf(this.eventId) > -1;
    });
    this.cashOutSectionService.emitMyBetsCounterEvent(this.bets);
    this.registerEventListeners();
  }

  private init(): void {
    this.cashOutSectionService.registerController(this.ctrlName);
    const myBetsInstances = this.cashOutSectionService.createTempDataForMyBets(this.cashoutIds, this.placedBets);
    this.betsMap = this.cashOutSectionService.generateBetsMap(myBetsInstances, this.betLocation);
    const isByb = Object.values(this.betsMap).some((bet) => bet.bybType !== undefined);
    if (isByb) {
      this.betTrackingEnabledSubscription = this.betTrackingService.isTrackingEnabled().pipe(
        concatMap((res: boolean) => {
          this.betTrackingEnabled = res;
          return this.betTrackingEnabled ? this.betTrackingService.getStaticContent() : of(null);
        })
      ).subscribe( (content) => {
        this.optaDisclaimer = content;
      });
    }

    this.updateBets();
  }

  private removeListeners(): void {
    this.cashOutSectionService.removeListeners(this.ctrlName);
    this.pubsub.unsubscribe(this.ctrlName);
  }

  private registerEventListeners(): void {
    this.pubsub.subscribe(this.ctrlName, this.pubsub.API.LIVE_BET_UPDATE, options => {
      this.cashOutSectionService.removeErrorMessageWithTimeout(this.bets, options);
    });


    this.pubsub.subscribe(this.ctrlName, [this.pubsub.API.CASH_OUT_MAP_UPDATED, this.pubsub.API.MY_BETS_UPDATED], () => {
      const myBetsInstances = this.cashOutSectionService.createTempDataForMyBets(this.cashoutIds, this.placedBets);
      this.betsMap = this.cashOutSectionService.generateBetsMap(myBetsInstances, this.betLocation);
      this.removeListeners();
      this.updateBets();
      this.init();
    });

    this.pubsub.subscribe(this.ctrlName, this.pubsub.API.UPDATE_CASHOUT_BET, bet => {
      this.cashOutSectionService.updateBet(bet, this.bets);
      this.updateCashoutBetValue(bet);

      const placedBet = _.findWhere(this.placedBets, { betId: bet.betId });
      if (placedBet) {
        placedBet.isCashOutedBetSuccess = bet.isCashOutedBetSuccess;
      }
    });
  }

  private updateCashoutBetValue(newBet: IBetDetail): void {
    const bet = _.findWhere(this.cashoutBets, { betId: newBet.betId });
    if (bet) {
      bet.cashoutValue = newBet.cashoutValue;
    }
  }
}
