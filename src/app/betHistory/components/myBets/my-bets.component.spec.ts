import { fakeAsync, tick } from '@angular/core/testing';
import { of as observableOf } from 'rxjs';
import { MyBetsComponent } from './my-bets.component';

describe('MyBetsComponent', () => {
  let component,
    locale,
    cashOutSectionService,
    pubsub,
    editMyAccaService,
    liveBetUpdateHandler,
    updCashoutBetHandler,
    cashoutMapUpdateHandler,
    callbackHandler,
    betTrackingService,
    handleScoreboardsStatsUpdatesService;

  const bet = { betId: '615079', cashoutValue: '2' } as any;
  const content = 'static block content';

  beforeEach(() => {
    editMyAccaService = {};
    locale = {
      getString: jasmine.createSpy()
    };
    callbackHandler = (ctrlName: string, eventName: string, callback) => {
      if (eventName === 'UPDATE_CASHOUT_BET') {
        updCashoutBetHandler = callback;
      } else if (eventName.indexOf('CASH_OUT_MAP_UPDATED') > -1) {
        cashoutMapUpdateHandler = callback;
      } else if (eventName === 'LIVE_BET_UPDATE') {
        liveBetUpdateHandler = callback;
      }
    };
    betTrackingService = {
      isTrackingEnabled: jasmine.createSpy('isTrackingEnabled').and.returnValue(observableOf(true)),
      getStaticContent: jasmine.createSpy('getStaticContent').and.returnValue(observableOf(content)),
    };
    cashOutSectionService = {
      registerController: jasmine.createSpy(),
      generateBetsMap: jasmine.createSpy().and.returnValue('betsMap'),
      generateBetsArray: jasmine.createSpy().and.returnValue([]),
      cashOutSectionService: jasmine.createSpy(),
      emitMyBetsCounterEvent: jasmine.createSpy(),
      createTempDataForMyBets: jasmine.createSpy().and.returnValue('createTempDataForMyBets'),
      removeListeners: jasmine.createSpy(),
      removeErrorMessageWithTimeout: jasmine.createSpy(),
      updateBet: jasmine.createSpy(),
      isCashoutError: jasmine.createSpy('isCashoutError'),
      getCashoutError: jasmine.createSpy('getCashoutError')
    };
    pubsub = {
      unsubscribe: jasmine.createSpy(),
      subscribe: jasmine.createSpy().and.callFake(callbackHandler),
      API: {
        UPDATE_CASHOUT_BET: 'UPDATE_CASHOUT_BET',
        CASH_OUT_MAP_UPDATED: 'CASH_OUT_MAP_UPDATED',
        MY_BETS_UPDATED: 'MY_BETS_UPDATED',
        LIVE_BET_UPDATE: 'LIVE_BET_UPDATE'
      }
    };
    handleScoreboardsStatsUpdatesService = {
      getStatisticsEventIds: jasmine.createSpy('getStatisticsEventIds').and.returnValue(observableOf('123456'))
    };

    component = new MyBetsComponent(
      editMyAccaService,
      locale,
      cashOutSectionService,
      pubsub,
      betTrackingService,
      handleScoreboardsStatsUpdatesService
    );
    component.ctrlName = 'MyBetsController';
    component.cashoutBets = [{
      betId: '615079',
      cashoutValue: '2'
    }] as any;
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  describe('@ngOnInit', () => {
    it('should init data, update it', () => {
      component.ngOnInit();
      expect(cashOutSectionService.registerController).toHaveBeenCalledWith('MyBetsController');
      expect(cashOutSectionService.emitMyBetsCounterEvent).toHaveBeenCalledWith([]);
      liveBetUpdateHandler();
      expect(cashOutSectionService.removeErrorMessageWithTimeout).toHaveBeenCalledWith([], undefined);
      updCashoutBetHandler(bet);
      expect(cashOutSectionService.updateBet).toHaveBeenCalledWith(bet, []);
      expect(component.cashoutBets[0].cashoutValue).toEqual('2');
    });

    it('should not update cashout value', () => {
      bet.betId = '123456';
      component.ngOnInit();
      updCashoutBetHandler(bet);
      expect(cashOutSectionService.updateBet).toHaveBeenCalledWith(bet, []);
      expect(component.cashoutBets[0].cashoutValue).toEqual('2');
    });

    it('should update cashout map on CASH_OUT_MAP_UPDATED', () => {
      component.ngOnInit();
      cashoutMapUpdateHandler();

      expect(cashOutSectionService.removeListeners).toHaveBeenCalledWith('MyBetsController');
      expect(cashOutSectionService.removeListeners).toHaveBeenCalledWith('MyBetsController');
      expect(cashOutSectionService.registerController).toHaveBeenCalled();
      expect(cashOutSectionService.createTempDataForMyBets).toHaveBeenCalled();
      expect(cashOutSectionService.generateBetsMap).toHaveBeenCalled();
    });

    it('should show opta disclaimer', fakeAsync(() => {
      component.eventId = '123456';
      cashOutSectionService.generateBetsArray.and.returnValue([
        {
          eventSource: {
            event: ['123456']
          }
        }
      ] as any);
      component.ngOnInit();

      tick();
      expect(handleScoreboardsStatsUpdatesService.getStatisticsEventIds).toHaveBeenCalled();
      expect(component.bets[0].optaDisclaimerAvailable).toBeTruthy();
    }));

    it('should not show opta disclaimer', fakeAsync(() => {
      component.eventId = '654321';
      cashOutSectionService.generateBetsArray.and.returnValue([
        {
          eventSource: {
            event: ['654321']
          }
        }
      ] as any);
      component.ngOnInit();

      tick();
      expect(handleScoreboardsStatsUpdatesService.getStatisticsEventIds).toHaveBeenCalled();
      expect(component.bets[0].optaDisclaimerAvailable).toBeFalsy();
    }));
  });

  describe('@ngOnChanges', () => {
    let changes;

    beforeEach(() => {
      changes = { betsMap: false } as any;
    });

    it('removeListeners & updateBets should not be called', () => {
      component.ngOnChanges(changes);
      expect(cashOutSectionService.removeListeners).not.toHaveBeenCalledWith('MyBetsController');
      expect(cashOutSectionService.emitMyBetsCounterEvent).not.toHaveBeenCalledWith([]);
      liveBetUpdateHandler();
      expect(cashOutSectionService.removeErrorMessageWithTimeout).not.toHaveBeenCalledWith([], undefined);
      updCashoutBetHandler(bet);
      expect(cashOutSectionService.updateBet).not.toHaveBeenCalledWith(bet, []);
    });

    it('removeListeners & updateBets should be called', () => {
      changes.betsMap = true;
      component.ngOnChanges(changes);
      expect(cashOutSectionService.removeListeners).toHaveBeenCalledWith('MyBetsController');
      expect(cashOutSectionService.emitMyBetsCounterEvent).toHaveBeenCalledWith([]);
      liveBetUpdateHandler();
      expect(cashOutSectionService.removeErrorMessageWithTimeout).toHaveBeenCalledWith([], undefined);
      updCashoutBetHandler(bet);
      expect(cashOutSectionService.updateBet).toHaveBeenCalledWith(bet, []);
    });
  });

  it('@ngOnDestroy should perform unsubscribe and unsync', () => {
    component['betTrackingEnabledSubscription'] = {
      unsubscribe: jasmine.createSpy('unsubscribe')
    } as any;
    component['getEventIdStatisticsSubscription'] = {
      unsubscribe: jasmine.createSpy('unsubscribe')
    } as any;
    component.ngOnDestroy();

    expect(cashOutSectionService.removeListeners).toHaveBeenCalledWith('MyBetsController');
    expect(pubsub.unsubscribe).toHaveBeenCalled();
    expect(component['betTrackingEnabledSubscription'].unsubscribe).toHaveBeenCalled();
    expect(component['getEventIdStatisticsSubscription'].unsubscribe).toHaveBeenCalled();
  });

  it('@trackByBet should return joined string', () => {
    const index: number = 11;
    const betData: any = {
      eventSource: {
        betId: 12,
        receipt: 'receipt'
      }
    };
    expect(component.trackByBet(index, betData)).toEqual('1112receipt');
  });

  it('@registerEventListeners should register callbacks on server updates', () => {
    spyOn<any>(component, 'removeListeners');
    spyOn<any>(component, 'updateBets');
    spyOn<any>(component, 'updateCashoutBetValue');
    const updatedBet = {
      betId: 42,
      isCashOutedBetSuccess: true
    } as any;
    pubsub.subscribe.and.callFake((name, listeners, handler) => {
      if (listeners === 'LIVE_BET_UPDATE') {
        handler('sync_options');
      } else {
        handler(updatedBet);
      }
    });
    component.bets = [{ id: 931 }] as any;
    component.cashoutIds = [{ id: 4 }];
    component.placedBets = [{ id: 42, betId: 42 }] as any;
    component.betLocation = 'bet location';
    component['registerEventListeners']();

    expect(pubsub.subscribe).toHaveBeenCalledWith(component.ctrlName, 'LIVE_BET_UPDATE', jasmine.any(Function));
    expect(cashOutSectionService.removeErrorMessageWithTimeout).toHaveBeenCalledWith([{ id: 931 }], 'sync_options');

    expect(pubsub.subscribe).toHaveBeenCalledWith(
      component.ctrlName, ['CASH_OUT_MAP_UPDATED', 'MY_BETS_UPDATED'], jasmine.any(Function)
    );
    expect(component['removeListeners']).toHaveBeenCalled();
    expect(component['updateBets']).toHaveBeenCalled();
    expect(cashOutSectionService.createTempDataForMyBets).toHaveBeenCalledWith([{ id: 4 }],
      [{ id: 42, betId: 42, isCashOutedBetSuccess: true }]);
    expect(cashOutSectionService.generateBetsMap).toHaveBeenCalledWith('createTempDataForMyBets', 'bet location');

    expect(pubsub.subscribe).toHaveBeenCalledWith(component.ctrlName, 'UPDATE_CASHOUT_BET', jasmine.any(Function));
    expect(cashOutSectionService.updateBet).toHaveBeenCalledWith(updatedBet, [{ id: 931 }]);
    expect(component.placedBets[0].isCashOutedBetSuccess).toBeTruthy();
    expect(component['updateCashoutBetValue']).toHaveBeenCalledWith(updatedBet);
  });

  it('isCashoutError should call isCashoutError method of cashout section service', () => {
    component['isCashoutError']({} as any);
    expect(cashOutSectionService.isCashoutError).toHaveBeenCalled();
  });

  it('getCashoutError should call getCashoutError method of cashout section service', () => {
    component['getCashoutError']({} as any);
    expect(cashOutSectionService.getCashoutError).toHaveBeenCalled();
  });

  it('updateBets', () => {
    component.eventId = '234';
    const bets = <any>[
      {
        eventSource: {
          event: ['234', '255']
        }
      },
      {
        eventSource: {
          event: ['123', '355']
        }
      }
    ];
    component['cashOutSectionService'].generateBetsArray = () => bets;
    component['updateBets']();

    expect(component.bets.length).toEqual(1);
  });

  describe('Init', () => {
    it('should get static block if feature toggle is on', fakeAsync(() => {
      const betsMapMock = {bybType: '5-A-SIDE'};
      cashOutSectionService.generateBetsMap.and.returnValue(observableOf(betsMapMock));
      component.betTrackingEnabled = true;

      component['init']();
      tick();

      expect(cashOutSectionService.generateBetsMap).toHaveBeenCalled();
      expect(betTrackingService.getStaticContent).toHaveBeenCalled();
      expect(betTrackingService.isTrackingEnabled).toHaveBeenCalled();
      expect(component.betsMap).toBeTruthy();
      expect(component.optaDisclaimer).toEqual(content);
      expect(component.betTrackingEnabled).toBeTruthy();
    }));

    it('shouldnt get static block if feature toggle is off', fakeAsync(() => {
      const betsMapMock = {bybType: '5-A-SIDE'};
      cashOutSectionService.generateBetsMap.and.returnValue(observableOf(betsMapMock));
      betTrackingService.isTrackingEnabled.and.returnValue(observableOf(false));

      component['init']();
      tick();

      expect(cashOutSectionService.generateBetsMap).toHaveBeenCalled();
      expect(betTrackingService.getStaticContent).not.toHaveBeenCalled();
      expect(betTrackingService.isTrackingEnabled).toHaveBeenCalled();
      expect(component.betsMap).toBeTruthy();
      expect(component.optaDisclaimer).toEqual(null);
      expect(component.betTrackingEnabled).toBeFalsy();
    }));
  });
});
