import { Component, Input } from '@angular/core';

import { IBetHistoryBet, IBetHistoryLeg } from '../../models/bet-history.model';
import { UsedFromWidgetAbstractComponent } from '@core/abstract-components/used-from-widget-abstract.component';

@Component({
  selector: 'bet-leg-list',
  templateUrl: './bet-leg-list.component.html',
  styleUrls: ['./bet-leg-list.component.less']
})
export class BetLegListComponent extends UsedFromWidgetAbstractComponent {

  @Input() bet: { eventSource: IBetHistoryBet, location: string};
  @Input() betLocation: string;

  constructor() {
    super();
  }

  trackByLeg(index: number, leg: IBetHistoryLeg): string {
    return `${index}${leg.status}`;
  }
}
