import { BetLegListComponent  } from './bet-leg-list.component';
import { IBetHistoryBet } from '../../models/bet-history.model';

describe('BetLegListComponent', () => {
  let component: BetLegListComponent;

  beforeEach(() => {
    component = new BetLegListComponent();
    component.bet = {
      eventSource: {
        leg: []
      },
      location: ''
    } as any;

    component.bet = {eventSource : {leg : [{}, {}]} as IBetHistoryBet, location : 'Test'};
  });

  it('should create a comonent', () => {
    expect(component).toBeTruthy();
  });

  it('trackByLeg should return joined string', () => {
    const index: number = 1;
    const leg: any = {
      id: '22',
      status: 'test'
    };
    const result: string = component.trackByLeg(index, leg);

    expect(result).toEqual('1test');
  });
});
