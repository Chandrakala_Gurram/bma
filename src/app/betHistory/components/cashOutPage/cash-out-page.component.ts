import { from as observableFrom, of as observableOf, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { LocaleService } from '@core/services/locale/locale.service';
import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';

import { SessionService } from '@authModule/services/session/session.service';
import { LiveServConnectionService } from '@core/services/liveServ/live-serv-connection.service';
import { MaintenanceService } from '@core/services/maintenance/maintenance.service';
import { UserService } from '@core/services/user/user.service';
import { CashOutMapService } from '../../services/cashOutMap/cash-out-map.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { DeviceService } from '@core/services/device/device.service';
import { CashoutBetsStreamService } from '@app/betHistory/services/cashoutBetsStream/cashout-bets-stream.service';

import { CashOutBetsMap, IMapState } from '../../betModels/cashOutBetsMap/cash-out-bets-map.class';
import { IBetDetail } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { InfoDialogService } from '@coreModule/services/infoDialogService/info-dialog.service';
import { BetHistoryMainService } from '@app/betHistory/services/betHistoryMain/bet-history-main.service';

@Component({
  selector: 'cash-out-page',
  templateUrl: 'cash-out-page.component.html'
})
export class CashOutPageComponent extends AbstractOutletComponent implements OnInit, OnDestroy {
  @Input() area?: string;

  title: string;
  data: CashOutBetsMap;
  /**
   * Current state for cash out map (isEmpty | isUserLogOut | isSpinnerActive)
   * @member {object}
   */
  mapState: IMapState;
  errorMsg: string;

  private betsStreamOpened: boolean = false;
  private cashoutDataSubscription: Subscription;

  constructor(private cashOutMapService: CashOutMapService,
              private userService: UserService,
              private maintenanceService: MaintenanceService,
              private sessionService: SessionService,
              private liveServService: LiveServConnectionService,
              private pubsubService: PubSubService,
              private localeService: LocaleService,
              private deviceService: DeviceService,
              private infoDialogService: InfoDialogService,
              private cashoutBetsStreamService: CashoutBetsStreamService,
              private betHistoryMainService: BetHistoryMainService) {
    super();
  }

  ngOnInit(): void {
    this.cashoutBetsStreamService.clearCashoutBetsObservable();
    this.title = this.area || 'cashout-area';

    if (!this.deviceService.isOnline()) {
      this.infoDialogService.openConnectionLostPopup();
      this.errorHandler();
    } else {
      this.cashoutDataSubscription = this.maintenanceService.siteServerHealthCheck(true).pipe(
        switchMap(() => {
          return observableFrom(this.sessionService.whenProxySession());
        }),
        switchMap(() => {
          if (this.userService.isInShopUser()) {
            return observableOf([]);
          }
          this.betsStreamOpened = true;
          return this.cashoutBetsStreamService.getCashoutBets();
        })
      ).subscribe((data: IBetDetail[]) => {
        this.betHistoryMainService.extendCashoutBets(data);
        const bets = this.filterCashoutBets(data);
        this.mapState = this.cashOutMapService.cashoutBetsMap.mapState;
        this.mapState.isUserLogOut = false;

        this.extendCashOutDataWithMap(bets);
        this.hideSpinner();
      }, () => {
        this.errorHandler();
      });
    }

    this.pubsubService.subscribe(this.title, 'EDIT_MY_ACCA', () => {
      this.reloadSegment();
    });
    this.pubsubService.subscribe(this.title,
      [this.pubsubService.API.LOAD_CASHOUT_BETS,
      this.pubsubService.API.RELOAD_COMPONENTS],
      () => this.reload());
  }

  ngOnDestroy(): void {
    this.pubsubService.publish(this.pubsubService.API.EMA_UNSAVED_IN_WIDGET, false);
    this.pubsubService.publish(this.pubsubService.API.CASHOUT_CTRL_STATUS, { ctrlName: 'fullCashout', isDestroyed: true });
    this.pubsubService.unsubscribe(this.title);

    this.cashoutBetsStreamService.clearCashoutBetsObservable();

    if (this.cashoutDataSubscription) {
      this.cashoutDataSubscription.unsubscribe();
    }

    if (this.betsStreamOpened) {
      this.cashoutBetsStreamService.closeBetsStream();
      this.betsStreamOpened = false;
    }
  }

  get userStatus(): boolean {
    return this.userService.status;
  }

  /**
   * Reloads cash out segment
   * @private
   */
  reloadSegment(): void {
    this.showSpinner();
    this.ngOnDestroy();
    this.ngOnInit();
  }

  /**
   * Filter bets with cashout available
   * @param {IBetDetail[]} bets - Open Bets  array
   * @returns {BetDetail[]} bets with cashut abailable
   */
  private filterCashoutBets(bets: IBetDetail[]) {
    return (bets || []).filter((bet: IBetDetail) => !isNaN(Number(bet.cashoutValue)) || bet.cashoutValue === 'CASHOUT_SELN_SUSPENDED');
  }

  /**
   * reload component when subscribe triggered.
   * @private
   */
  private reload(): void {
    this.sessionService.whenProxySession().then(() => {
      this.liveServService.connect().subscribe(() => this.reloadSegment());
    });
  }

  /**
   * Assign cash out map to cash out data object to be used for directives
   * @param {Array} bets
   * @private
   */
  private extendCashOutDataWithMap(bets: IBetDetail[]): void {
    this.data = this.cashOutMapService.createCashoutBetsMap(bets, this.userService.currency, this.userService.currencySymbol);
  }

  private errorHandler(): void {
    const page: string = this.localeService.getString('app.betslipTabs.cashout').toLowerCase();

    this.errorMsg = this.localeService.getString('app.loginToSeePageMessage',
      { page: `${page} bets` });
    this.showError();
  }
}
