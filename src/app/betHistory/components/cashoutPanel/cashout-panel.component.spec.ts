import { CashoutPanelComponent } from './cashout-panel.component';

describe('CashoutPanelComponent', () => {
  let component: CashoutPanelComponent;
  let cashoutPanelService;
  let pubsub;
  let timeService;

  const betLocation: string = 'betLocation';
  const bet: any = {
    eventSource: {
      partialCashOutPercentage: 5
    },
    location: 'location'
  };
  const data: any[] = [
    {
      eventSource: {
        errorDictionary: 'errorDictionary',
        partialCashOutPercentage: 4,
        partialCashoutAvailable: 'partialCashoutAvailable',
        isCashOutUnavailable: false
      },
      location: 'location'
    }
  ];

  beforeEach(() => {
    cashoutPanelService = {
      isButtonShown: jasmine.createSpy(),
      isPartialAvailable: jasmine.createSpy(),
      getButtonState: jasmine.createSpy(),
      getStateConfig: jasmine.createSpy(),
      doCashOut: jasmine.createSpy(),
      setPartialState: jasmine.createSpy()
    };
    pubsub = {
      publish: jasmine.createSpy('publish'),
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: {
        UPDATE_CASHOUT_BET: 'UPDATE_CASHOUT_BET',
        CASHOUT_COUNTDOWN_TIMER: 'CASHOUT_COUNTDOWN_TIMER'
      }
    };
    timeService = jasmine.createSpyObj(['countDownTimer']);

    component = new CashoutPanelComponent(
      cashoutPanelService,
      pubsub,
      timeService
    );

    component.bet = bet;
  });

  it('constructor', () => {
    expect(component).toBeTruthy();
  });

  it('isButtonShown', () => {
    component.betLocation = betLocation;
    // tslint:disable-next-line
    const isButtonShownInit = component.isButtonShown;

    expect(cashoutPanelService.isButtonShown).toHaveBeenCalledWith(component.bet.eventSource, component.betLocation);
  });

  it('isPartialAvailable', () => {
    // tslint:disable-next-line
    const isPartialAvailableInit = component.isPartialAvailable;

    expect(cashoutPanelService.isPartialAvailable).toHaveBeenCalledWith(component.bet.eventSource);
  });

  it('buttonState', () => {
    // tslint:disable-next-line
    const buttonStateInit = component.buttonState;

    expect(cashoutPanelService.getButtonState).toHaveBeenCalledWith(component.bet.eventSource);
  });

  it('stateConfig', () => {
    // tslint:disable-next-line
    const stateConfigInit = component.stateConfig;

    expect(cashoutPanelService.getStateConfig).toHaveBeenCalledWith(component.bet.eventSource);
  });

  it('doCashOut', () => {
    component.data = data;
    component.doCashOut('type');

    expect(cashoutPanelService.doCashOut).toHaveBeenCalledWith(component.data, component.bet.location, component.bet.eventSource, 'type');
    expect(pubsub.publish).toHaveBeenCalled();
  });

  it('doCashOut should start timer if "confirm" action', () => {
    spyOn(component as any, 'iniTimer');
    component.doCashOut('type');
    expect(component['iniTimer']).not.toHaveBeenCalled();

    component.doCashOut();
    expect(component['iniTimer']).toHaveBeenCalled();
  });

  it('should start timer defining subscription name', () => {
    expect(component.subscriberName).not.toBeDefined();

    component.doCashOut();

    expect(component.subscriberName).toBeDefined();
  });

  it('should start timer triggering countdown once', () => {
    pubsub.subscribe.and.callFake((a, b, cb) => cb(3));
    timeService.countDownTimer.and.returnValue({} as any);

    expect(component.countDownTimer).not.toBeDefined();

    component.doCashOut();

    expect(component.countDownTimer).toBeDefined();
    expect(timeService.countDownTimer).toHaveBeenCalledWith(3);
    expect(timeService.countDownTimer).toHaveBeenCalledTimes(1);
    expect(pubsub.unsubscribe).toHaveBeenCalled();
    expect(component.subscriberName).toBe(null);
  });

  it('should not start timer but cancel subscription', () => {
    pubsub.subscribe.and.callFake((a, b, cb) => cb(null));
    timeService.countDownTimer.and.returnValue({} as any);

    expect(component.countDownTimer).not.toBeDefined();

    component.doCashOut();

    expect(component.countDownTimer).not.toBeDefined();
  });

  it('partialPercentageChange', () => {
    const updatedPercentage: number = 123;
    component.partialPercentageChange(updatedPercentage);

    expect(component.bet.eventSource.partialCashOutPercentage).toEqual(updatedPercentage);
  });

  it('should clean on ngOnDestroy', () => {
    component.subscriberName = 'foo';
    component.countDownTimer = jasmine.createSpyObj(['stop']);
    component.ngOnDestroy();

    expect(pubsub.unsubscribe).toHaveBeenCalledWith('foo');
    expect(component.countDownTimer.stop).toHaveBeenCalled();
  });

  it('#isPartialCashOutAvailable Should update bet.isPartialActive', () => {
    component.isPartialCashOutAvailable = true;
    component.isPartialCashOutAvailable = false;

    expect(cashoutPanelService.setPartialState).toHaveBeenCalledWith(component.bet.eventSource, false);
  });

  it('#isPartialCashOutAvailable Should not update bet.isPartialActive', () => {
    component.isPartialCashOutAvailable = true;
    component.isPartialCashOutAvailable = true;

    expect(cashoutPanelService.setPartialState).not.toHaveBeenCalled();
  });
});
