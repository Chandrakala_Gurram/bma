import { Component, Input, OnDestroy } from '@angular/core';

import { IPanelStateConfig } from '@app/betHistory/models/cashout-panel.model';
import { ICashOutData } from '@app/betHistory/models/cashout-section.model';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { TimeService } from '@core/services/time/time.service';
import { CashoutPanelService } from './cashout-panel.service';
import { CashoutBet } from '@app/betHistory/betModels/cashoutBet/cashout-bet.class';
import { ICountDownTimer } from '@core/services/time/time-service.model';

@Component({
  selector: 'cashout-panel',
  templateUrl: 'cashout-panel.component.html',
  styleUrls: ['cashout-panel.component.less']
})
export class CashoutPanelComponent implements OnDestroy {

  @Input() data: ICashOutData[];
  @Input() betLocation: string;
  @Input() bet: { eventSource: CashoutBet; location: string; };

  countDownTimer: ICountDownTimer;
  subscriberName: string;

  private _isPartialCashOutAvailable: boolean;
  private isFirstInit: boolean = true;

  constructor(
    private cashoutPanelService: CashoutPanelService,
    private pubSubService: PubSubService,
    private timeService: TimeService
  ) { }

  @Input() set isPartialCashOutAvailable(value: boolean) {
    if (this.isFirstInit) {
      this.isFirstInit = false;
      this._isPartialCashOutAvailable = value;
      return;
    }
    if (this._isPartialCashOutAvailable !== value && value === false) {
      this.cashoutPanelService.setPartialState(this.bet.eventSource, false);
    }
  }

  ngOnDestroy(): void {
    this.subscriberName && this.pubSubService.unsubscribe(this.subscriberName);
    this.countDownTimer && this.countDownTimer.stop && this.countDownTimer.stop();
  }

  get isButtonShown(): boolean {
    return this.cashoutPanelService.isButtonShown((this.bet.eventSource), this.betLocation);
  }

  get isPartialAvailable(): boolean {
    return this.cashoutPanelService.isPartialAvailable(this.bet.eventSource);
  }

  get buttonState(): string {
    return this.cashoutPanelService.getButtonState(this.bet.eventSource);
  }

  get stateConfig(): IPanelStateConfig {
    return this.cashoutPanelService.getStateConfig(this.bet.eventSource);
  }

  /**
   * Cashout buttons handler
   *
   * @param {string} type? - type of the cashOut, is 'undefined' for "confirm" action
   */
  doCashOut(type?: string): void {
    !type && this.iniTimer();
    this.cashoutPanelService.doCashOut(this.data, this.bet.location, this.bet.eventSource, type);
    this.pubSubService.publish(this.pubSubService.API.UPDATE_CASHOUT_BET, this.bet.eventSource);
  }

  partialPercentageChange(updatedPercentage: number): void {
    this.bet.eventSource.partialCashOutPercentage = updatedPercentage;
  }

  /**
   * Add one-off subscription for countdown timer,
   * start timer only if pending state with given delay.
   */
  private iniTimer(): void {
    this.subscriberName = `CashoutPanelComponent-${this.bet.eventSource.betId}${this.bet.eventSource.receipt}`;
    this.pubSubService.subscribe(this.subscriberName, this.pubSubService.API.CASHOUT_COUNTDOWN_TIMER, (time?: number) => {
      if (time) {
        this.countDownTimer = this.timeService.countDownTimer(time);
      }

      this.pubSubService.unsubscribe(this.subscriberName);
      this.subscriberName = null;
    });
  }
}
