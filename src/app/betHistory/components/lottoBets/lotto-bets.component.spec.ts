import { LottoBetsComponent } from './lotto-bets.component';

describe('LottoBetsComponent', () => {
  let component, betHistoryMainService, timeService, userService, filtersService, locale;

  beforeEach(() => {
    locale = {
      getString: jasmine.createSpy()
    };

    timeService = {
      getLocalDateFromString: jasmine.createSpy()
    };

    filtersService = {
      removeLineSymbol: jasmine.createSpy().and.returnValue('test'),
      date: jasmine.createSpy().and.returnValue(new Date())
    };

    userService = {
      currencySymbol: jasmine.createSpy()
    };

    betHistoryMainService = {
      getBetStatus: jasmine.createSpy().and.returnValue('lost')
    };

    component = new LottoBetsComponent(betHistoryMainService, timeService, userService, filtersService, locale);
  });

  it('constructor', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    const token = 'bethistory.noLottoBets';
    component.ngOnInit();

    expect(locale.getString).toHaveBeenCalledWith(token);
  });

  describe('#ngOnChanges', () => {
    it('generateLottoHistory should not be called', () => {
      const changes = {
        ngModel: { currentValue: 10}
      } as any;
      component['generateLottoHistory'] = jasmine.createSpy();
      component.ngOnChanges(changes);

      expect(component['generateLottoHistory']).not.toHaveBeenCalled();
    });

    it('generateLottoHistory should be called', () => {
      const changes = {
        lottoBets: { currentValue: 10}
      } as any;
      component['generateLottoHistory'] = jasmine.createSpy();
      component.ngOnChanges(changes);

      expect(component['generateLottoHistory']).toHaveBeenCalled();
    });
  });

  it('trackByBall should return joined string', () => {
    const index: number = 11;
    const item: any = {
      ballNo: '22'
    };
    const result = component.trackByBall(index, item);

    expect(result).toEqual('1122');
  });

  it('trackByBet should return joined string', () => {
    const index: number = 11;
    const item: any = {
      id: '22',
      betReceiptId: '33'
    };
    const result = component.trackByBet(index, item);

    expect(result).toEqual('112233');
  });

  it('generateLottoHistory', () => {
    const lottoBets: any[] = [
      {
        drawName: '',
        ycBet: true,
        ycStatus: 0,
        date: '',
        lotteryDraw: {
          drawAt: '',
          pick: [
            {
              ballName: 'ballName',
              ballNo: 'ballNo'
            }
          ]
        },
        id: 'id',
        lotteryName: 'lotteryName',
        lotterySub: {
          subReceipt: 'subReceipt'
        },
        stake: {
          value: 'value'
        },
        settled: 'Y',
        winnings: {
          value: 'value'
        }
      }
    ];
    component.lottoBets = lottoBets;
    component['generateLottoHistory']();
    const result = component.lottoHistory;

    expect(timeService.getLocalDateFromString).toHaveBeenCalledTimes(1);
    expect(filtersService.removeLineSymbol).toHaveBeenCalledTimes(2);
    expect(filtersService.date).toHaveBeenCalledTimes(1);
    expect(result[0].balls).toBeDefined();
    expect(result[0].betDate).toBeDefined();
    expect(result[0].betReceiptId).toBeDefined();
    expect(result[0].currency).toBeDefined();
    expect(result[0].date).toBeDefined();
    expect(result[0].drawName).toBeDefined();
    expect(result[0].id).toBeDefined();
    expect(result[0].name).toBeDefined();
    expect(result[0].settled).toBeDefined();
    expect(result[0].stake).toBeDefined();
    expect(result[0].status).toBeDefined();
    expect(result[0].totalReturns).toBeDefined();
  });
});
