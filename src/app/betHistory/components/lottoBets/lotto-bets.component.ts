import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as _ from 'underscore';

import { TimeService } from '@core/services/time/time.service';
import { UserService } from '@core/services/user/user.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { ILottoBet, ILottoModel, IBall } from '@app/betHistory/models/lotto.model';
import { LocaleService } from '@core/services/locale/locale.service';
import { BetHistoryMainService } from './../../services/betHistoryMain/bet-history-main.service';
import { UsedFromWidgetAbstractComponent } from '@core/abstract-components/used-from-widget-abstract.component';


@Component({
  selector: 'lotto-bets',
  styleUrls: ['./lotto-bets.component.less'],
  templateUrl: './lotto-bets.component.html'
})
export class LottoBetsComponent extends UsedFromWidgetAbstractComponent implements OnInit, OnChanges {
  @Input() lottoBets: ILottoBet[];

  lottoHistory: ILottoModel[];
  noBetsMessage: string;

  constructor(
    private betHistoryMainService: BetHistoryMainService,
    private timeService: TimeService,
    private userService: UserService,
    private filtersService: FiltersService,
    private locale: LocaleService,
  ) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.lottoBets === undefined) { return; }
    this.generateLottoHistory();
  }

  ngOnInit(): void {
    this.noBetsMessage = this.locale.getString('bethistory.noLottoBets');
  }

  /**
   * Track Events by index
   * @param {number} index
   * @returns {number}
   */
  trackByBall(index: number, item: IBall): string {
    return `${index}${item.ballNo}`;
  }

  /**
   * Track Events by index
   * @param {number} index
   * @returns {number}
   */
  trackByBet(index: number, item: ILottoModel): string {
    return `${index}${item.id}${item.betReceiptId}`;
  }

  /**
   * Generate Lotto History
   * @private
   */
  private generateLottoHistory(): void {
    this.lottoHistory = _.map(this.lottoBets, (bet: any) => {
      const localDrawDate: Date = this.timeService.getLocalDateFromString(bet.lotteryDraw.drawAt);
      const lottoModel: ILottoModel = {
        id: bet.id,
        name: this.filtersService.removeLineSymbol(bet.lotteryName),
        balls: bet.lotteryDraw.pick,
        drawName: this.filtersService.removeLineSymbol(bet.drawName),
        date: this.filtersService.date(localDrawDate, 'dd.MM h:mm a'),
        betDate: bet.date,
        betReceiptId: bet.lotterySub.subReceipt,
        stake: bet.stake.value,
        currency: this.userService.currencySymbol,
        status: this.betHistoryMainService.getBetStatus(bet),
        settled: bet.settled,
        totalReturns: bet.settled === 'Y' ? bet.winnings.value : null
      };
      return lottoModel;
    });
  }
}
