import environment from '@environment/oxygenEnvConfig';
import { BetLegItemComponent } from './bet-leg-item.component';
import { IBetHistoryLeg, IBetHistoryPart } from '@app/betHistory/models/bet-history.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { of } from 'rxjs';

describe('BetLegItemComponent', () => {
  let component: BetLegItemComponent;

  let leg;
  let commentsService, eventService;
  let footballUpdateExtendSpy;
  let cashOutServiceStub;
  let raceOutcomeDetailsServiceStub;
  let localeStub;
  let fracToDecServiceStub;
  let filtersServiceStub;
  let editMyAccaService;
  let pubSubService;
  let routerStub;
  let routingHelperStub;
  let cmsService;
  let betTrackingService;
  let sportsConfigHelperService;
  const HORSE_RACING_CATEGORY_ID = environment.HORSE_RACING_CATEGORY_ID;

  beforeEach(() => {
    cashOutServiceStub = {
      getEachWayTerms: jasmine.createSpy().and.returnValue('#YourCall getEachWayTerms')
    };
    betTrackingService= {
      isTrackingEnabled: jasmine.createSpy('isTrackingEnabled'),
      checkIsBuildYourBet: jasmine.createSpy('checkIsBuildYourBet')
    };
    raceOutcomeDetailsServiceStub = {
      getSilkStyleForPage: jasmine.createSpy('getSilkStyleForPage'),
      isSilkAvailableForOutcome: jasmine.createSpy('isSilkAvailableForOutcome'),
      isUnnamedFavourite: jasmine.createSpy('isUnnamedFavourite')
    };
    localeStub = {
      getString: jasmine.createSpy().and.returnValue('Build Your Bet')
    };
    fracToDecServiceStub = {
      getFormattedValue: jasmine.createSpy('getFormattedValue')
    };
    filtersServiceStub = {
      filterPlayerName: jasmine.createSpy('filterPlayerName'),
      filterAddScore: jasmine.createSpy('filterAddScore')
    };
    editMyAccaService = {
      isLegSuspended: jasmine.createSpy('isLegSuspended'),
      isLegResulted: jasmine.createSpy('isLegResulted')
    };
    pubSubService = {
      subscribe: jasmine.createSpy().and.callFake((a, b, cb) => cb && cb()),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: {
        UPDATE_EMA_ODDS: 'UPDATE_EMA_ODDS',
        CASHOUT_LIVE_SCORE_EVENT_UPDATE: 'CASHOUT_LIVE_SCORE_EVENT_UPDATE',
        CASHOUT_LIVE_SCORE_UPDATE: 'CASHOUT_LIVE_SCORE_UPDATE'
      }
    };
    routerStub = {
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    };
    routingHelperStub = {
      formEdpUrl: jasmine.createSpy('formEdpUrl').and.returnValue('url')
    };
    footballUpdateExtendSpy = jasmine.createSpy('footballUpdateExtendSpy');
    commentsService = {
      sportUpdateExtend: jasmine.createSpy('sportUpdateExtend'),
      footballUpdateExtend: footballUpdateExtendSpy,
      extendWithScoreType: jasmine.createSpy('extendWithScoreType')
    };
    eventService = {
      isUKorIRE: jasmine.createSpy('isUKorIRE')
    };

    cmsService = {
    };
    leg = {
      name: 'test name',
      startTime: '1542273861984',
      poolPart: [],
      legSort: '',
      part: [
        {
          outcomeId: '51',
          outcome: [{
            eventCategory: {
              id: '1'
            }
          }],
          priceNum: 3,
          priceDen: 6,
          price: [
            {
              priceType: {
                code: ''
              }
            },
            {
              priceType: {
                code: ''
              }
            }
          ]
        } as any
      ],
      status: 'won',
      removing: false,
      removedLeg: false,
      resultedBeforeRemoval: true
    } as any;

    sportsConfigHelperService = {
      getSportPathByCategoryId: jasmine.createSpy('getSportPathByCategoryId').and.returnValue(of('horsePath'))
    };

    component = new BetLegItemComponent(cashOutServiceStub, raceOutcomeDetailsServiceStub, localeStub, fracToDecServiceStub,
      pubSubService, filtersServiceStub, editMyAccaService, routerStub, routingHelperStub,
      commentsService, eventService, sportsConfigHelperService, cmsService, betTrackingService);
    component.bet = {
      eventSource: { totalStatus : 'lost' }
    } as any;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('@HostListener onClick', () => {
    beforeEach(() => {
      component.goToEvent = jasmine.createSpy('goToEvent');
      component.bet = { eventSource: { isAccaEdit: undefined } } as any;
      component.leg = { eventEntity: { isDisplayed: true } } as any;
    });

    it('should navigate to event if all conditions are met', () => {
      component.onClick();
      expect(component.goToEvent).toHaveBeenCalled();
    });

    describe('should not navigate to event', () => {
      it('when bet.eventSource.isAccaEdit is true', () => {
        component.bet.eventSource.isAccaEdit = true;
      });
      it('when leg.eventEntity is unavailable', () => {
        component.leg.eventEntity = null;
      });
      it('when leg.eventEntity.isDisplayed is falsy', () => {
        component.leg.eventEntity.isDisplayed = false;
      });

      afterEach(() => {
        component.onClick();
        expect(component.goToEvent).not.toHaveBeenCalled();
      });
    });
  });

  describe('ngOnInit', () => {
    const eventEntity = {
      id: 8,
      name: 'Leg event',
      markets: []
    } as any;

    beforeEach(() => {
      component.leg = leg;

      component['filtersService']['isGreyhoundsEvent'] = jasmine.createSpy().and.returnValue(false);
      spyOn(component as any, 'formatOdds');
      spyOn(component as any, 'getOutcomeName').and.returnValue([]);
      spyOn(component as any, 'parseEventMarketDescription');
      spyOn(component as any, 'getRuleFourDeduction');
    });

    it('should set isFCTC to true', () => {
      component.leg.legSort = { code: 'CF' } as any;
      component.ngOnInit();
      expect(component.isFCTC).toEqual(true);
      expect(component.excludedDrilldownTagNames).toEqual('EVFLAG_MB,MKTFLAG_MB,EVFLAG_PB,MKTFLAG_PB');
    });

    it('should set isFCTC to false', () => {
      component.leg.legSort = 'MC';
      component.ngOnInit();
      expect(component.isFCTC).toEqual(false);
    });

    it('should set shouldShowFiveASideIcon to true', () => {
      component.bet.eventSource = { source: 'f' } as any;
      component.ngOnInit();
      expect(component.shouldShowFiveASideIcon).toEqual(true);
    });

    it('should set shouldShowFiveASideIcon to false', () => {
      component.bet.eventSource = { source: 'M' } as any;
      component.ngOnInit();
      expect(component.shouldShowFiveASideIcon).toEqual(false);
    });

    it('should set shouldShowFiveASideIcon to false', () => {
      component.ngOnInit();
      expect(component.shouldShowFiveASideIcon).toEqual(false);
    });

    it('should set isUKorIRE to true', () => {
      eventService.isUKorIRE.and.returnValue(true);
      component.leg.eventEntity = {
        categoryId: 16,
        markets: []
      } as any;
      component.ngOnInit();
      expect(eventService.isUKorIRE).toHaveBeenCalledWith({ categoryId: 16,  markets: [] });
      expect(component.isUKorIRE).toEqual(true);
    });

    it('should set isUKorIRE to false', () => {
      component.ngOnInit();
      expect(eventService.isUKorIRE).not.toHaveBeenCalled();
      expect(component.isUKorIRE).toEqual(false);
    });

    it('should call stack of private methods', () => {
      component.ngOnInit();

      expect(component['formatOdds']).toHaveBeenCalledWith(leg);
      expect(component['getOutcomeName']).toHaveBeenCalledWith(leg);
      expect(component['parseEventMarketDescription']).toHaveBeenCalled();
      expect(component['getRuleFourDeduction']).toHaveBeenCalledWith(leg);
      expect(pubSubService.subscribe).toHaveBeenCalled();
    });

    it(`should define 'event'`, () => {
      component.leg.eventEntity = eventEntity;

      component.ngOnInit();

      expect(component.event).toEqual(eventEntity);
    });

    it(`should call 'init' if 'event' is defined`, () => {
      spyOn(component as any, 'init');
      component.leg.eventEntity = eventEntity;

      component.ngOnInit();

      expect(component['init']).toHaveBeenCalled();
    });

    it(`should Not call 'init' if 'event' is Not defined`, () => {
      spyOn(component as any, 'init');

      component.ngOnInit();

      expect(component['init']).not.toHaveBeenCalled();
    });

    it(`should call checkForBogOddsAndResetOddsCaptions`, () => {
      spyOn(component as any, 'checkForBogOddsAndResetOddsCaptions');
      component.leg.eventEntity = eventEntity;

      component.ngOnInit();

      expect(component['checkForBogOddsAndResetOddsCaptions']).toHaveBeenCalled();
    });

    it(`if bet is settled than BOG enabled is checked from CMS`, () => {
      component.leg.eventEntity = eventEntity;
      (<any>component.bet) = {eventSource: {settled: 'Y'}};
      cmsService.isBogFromCms = jasmine.createSpy().and.returnValue(of(true));

      component.ngOnInit();

      expect(cmsService['isBogFromCms']).toHaveBeenCalled();
    });

    describe('isMultiples', () => {
      beforeEach(() => {
        spyOn(component as any, 'init');
      });

      it(`should be Truthy if outcomeNames.length === 1`, () => {
        component.ngOnInit();

        expect(component.isMultiples).toBeFalsy();
      });

      it(`should be Truthy if outcomeNames.length === 1`, () => {
        (component['getOutcomeName'] as jasmine.Spy).and.returnValue([1]);

        component.ngOnInit();

        expect(component.isMultiples).toBeFalsy();
      });

      it(`should be Truthy if outcomeNames.length > 1`, () => {
        (component['getOutcomeName'] as jasmine.Spy).and.returnValue([1, 2, 3]);

        component.ngOnInit();

        expect(component.isMultiples).toBeTruthy();
      });

    });

    it('should set showRemovedLabel and statusName', () => {
      component.ngOnInit();
      expect(component.showRemovedLabel).toBeFalsy();
      expect(component.statusName).toEqual('won');
      expect(component.isRemovingState).toEqual(false);
    });
  });

  it('#ngOnDestroy should call pubSubService.unsubscribe', () => {
    const controllerIdentifier = 'BetLegItemComponent123';
    component['controllerIdentifier'] = controllerIdentifier;
    component.ngOnDestroy();
    expect(pubSubService.unsubscribe).toHaveBeenCalledWith(controllerIdentifier);
  });


  describe('#goToEvent', () => {
    beforeEach(() => {
      component.event = <any>{};
      component.isEnhanced = false;
      component.isVirtuals = false;
      component.sportPath = 'racing';
    });

    describe('should return empty string', () => {
      it('if sportpath is empty', () => {
        component.sportPath = '';
      });
      it('if isEnhanced', () => {
        component.isEnhanced = true;
      });
      it('if isVirtuals', () => {
        component.isVirtuals = true;
      });
      afterEach(() => {
        expect(component.goToEvent()).toEqual('');
      });
    });

    it('should retunn url string if no preventing conditions are met', () => {
      expect(component.goToEvent()).toEqual('url');
    });

    it('#goToEvent if could not get sportType', () => {
      component.event = <any>{};
      component.sportPath = 'racing';
      component.goToEvent();
      expect(component.goToEvent()).toEqual('url');
    });

    it(`should return if Not sportPath and isEnhanced equal false`, () => {
      component.sportPath = undefined;
      component.isEnhanced = false;

      expect(component.goToEvent()).toEqual('');
    });

    describe('categoryName', () => {
      beforeEach(() => {
        routingHelperStub.formEdpUrl.and.callFake(url => url);
        component.isEnhanced = false;
        component.sportPath = 'text';
      });

      it(`should define as sportPath if event.categoryName is Falthy`, () => {
        expect(component.goToEvent()).toEqual(jasmine.objectContaining({ categoryName: 'text' }));
      });

      it(`should not change`, () => {
        const categoryName = 'some category';
        component.event.categoryName = categoryName;

        expect(component.goToEvent()).toEqual(jasmine.objectContaining({ categoryName }));
      });
    });
  });

  it('#ngOnChanges should not set showRemovedLabel, showRemovedLabel, isRemovingState', () => {
    component.showRemovedLabel = false;
    component.statusName = 'won';
    component.ngOnChanges({} as any);
    expect(component.showRemovedLabel).toBeFalsy();
    expect(component.statusName).toEqual('won');
    expect(component.isRemovingState).toBeFalsy(false);
  });

  it('#ngOnChanges should set showRemovedLabel and showRemovedLabel', () => {
    component.leg = {
      status: 'lost',
      removing: true
    } as any;
    component.ngOnChanges({
      leg: component.leg
    } as any);
    expect(component.showRemovedLabel).toBeTruthy();
    expect(component.statusName).toEqual('lost');
  });
  describe('getStatusName', () => {
    it('#statusIconName should return icon name for singles', () => {
      component.leg = leg;
      component.leg.status = 'won';
      expect(component.getStatusName).toEqual('won');
      component.leg.status = 'suspended';
      localeStub.getString = () => 'suspended';
      expect(component.getStatusName).toEqual('suspended');
      localeStub.getString = () => 'void';
      component.leg.status = 'void';
      expect(component.getStatusName).toEqual('void');
    });
    it('#statusIconName should return icon name for multiples', () => {
      component.leg = leg;
      component.isMultiples = true;
      component.bet.eventSource.totalStatus = 'won';
      component.leg.status = 'won';
      expect(component.getStatusName).toEqual('won');
      component.bet.eventSource.totalStatus = 'won';
      component.leg.status = 'lost';
      expect(component.getStatusName).toEqual('won');
      component.bet.eventSource.totalStatus = 'test';
      component.leg.status = 'test';
      expect(component.getStatusName).toEqual('');
      component.bet.eventSource.totalStatus = 'lost';
      component.leg.status = 'lost';
    });
  });

  it('#eventEntity should return correct event', () => {
    const backupEventEntity = {
      id: 123,
      name: 'Backup event'
    };

    const eventEntity = {
      id: 8,
      name: 'Leg event'
    } as any;

    component.leg = leg;
    expect(component.eventEntity).toBeFalsy();

    component.leg.noEventFromSS = false;
    component.leg.backupEventEntity = backupEventEntity as any;
    expect(component.eventEntity).toBeFalsy();

    component.leg.noEventFromSS = true;
    expect(component.eventEntity).toEqual(backupEventEntity as any);

    component.leg.eventEntity = eventEntity;
    expect(component.eventEntity).toEqual(eventEntity);
  });

  it('#winLosIndicator should return correct indicator or call #getWinLosIndicator', () => {
    component['getWinLosIndicator'] = jasmine.createSpy().and.returnValue('getWinLosIndicatorValue');

    component.leg = leg;
    component.leg.status = 'open';
    expect(component.winLosIndicator).toBeFalsy();

    component.leg.eventEntity = {
      categoryCode: 'FOOTBALL',
      comments: {}
    } as any;
    component.eventMarketDescription = 'Match Result';

    expect(component.winLosIndicator).toEqual('getWinLosIndicatorValue');
    expect(component['getWinLosIndicator']).toHaveBeenCalledWith(component.leg);

    component.eventMarketDescription = 'Match Betting';
    expect(component.winLosIndicator).toEqual('getWinLosIndicatorValue');
    expect(component['getWinLosIndicator']).toHaveBeenCalledWith(component.leg);
  });

  it('#trackByOutcomeName should return concatenated value', () => {
    expect(component.trackByOutcomeName(321, 'test-string')).toEqual('321_test-string');
  });

  it('#showSilk should return showSilk status', () => {
    expect(component.showSilk(leg)).toBeFalsy();

    leg.eventEntity = { categoryId: HORSE_RACING_CATEGORY_ID } as any;
    expect(component.showSilk(leg)).toBe(true);
  });

  it('#getSilkStyle should call RaceOutcomeDetailsService.getSilkStyleForPage', () => {
    const allSilkyNames = ['name1', 'name2'];
    const eventEntity: ISportEvent = { id: 32, name: 'event name' } as any;
    leg.allSilkNames = allSilkyNames;
    leg.eventEntity = eventEntity;

    component.getSilkStyle(leg, 0);
    expect(raceOutcomeDetailsServiceStub.getSilkStyleForPage).toHaveBeenCalledWith('51', eventEntity, allSilkyNames, true);
  });

  it('#getSilkStyle should call RaceOutcomeDetailsService.getSilkStyleForPage with outcome', () => {
    const allSilkyNames = ['name1', 'name2'];
    const eventEntity: ISportEvent = { id: 32, name: 'event name' } as any;
    leg.allSilkNames = allSilkyNames;
    leg.eventEntity = eventEntity;
    leg.part[0].outcomeId = undefined;
    leg.part[0].outcome = '51';

    component.getSilkStyle(leg);
    expect(raceOutcomeDetailsServiceStub.getSilkStyleForPage).toHaveBeenCalledWith('51', eventEntity, allSilkyNames, true);
  });

  it('#isSilkAvailable should call RaceOutcomeDetailsService.isSilkAvailableForOutcome', () => {
    const eventEntity: ISportEvent = { id: 32, name: 'event name' } as any;
    leg.eventEntity = eventEntity;
    component.isSilkAvailable(leg, 0);
    expect(raceOutcomeDetailsServiceStub.isSilkAvailableForOutcome).toHaveBeenCalledWith('51', eventEntity);
  });

  it('#isSilkAvailable should call RaceOutcomeDetailsService.isSilkAvailableForOutcome with outcome', () => {
    const eventEntity: ISportEvent = { id: 32, name: 'event name' } as any;
    leg.eventEntity = eventEntity;
    leg.part[0].outcomeId = undefined;
    leg.part[0].outcome = '51';

    component.isSilkAvailable(leg);
    expect(raceOutcomeDetailsServiceStub.isSilkAvailableForOutcome).toHaveBeenCalledWith('51', eventEntity);
  });

  describe('#isGenericSilk should check whether default silk image should be shown', () => {
    const index = 0,
      eventEntity: ISportEvent = { id: 32, name: 'event name' } as any;

    beforeEach(() => {
      leg.eventEntity = eventEntity;
      component.bet = {eventSource: {leg: [leg]}, location: 'location'} as any;
      spyOn(component, 'isSilkAvailable').and.returnValue(false);
      (raceOutcomeDetailsServiceStub as any).isUnnamedFavourite.and.returnValue(false);
    });

    it('should return false when not isUnnamedFavourite but silks are not available', () => {
      expect(component.isGenericSilk(leg, index)).toEqual(false);
      expect(raceOutcomeDetailsServiceStub.isUnnamedFavourite).toHaveBeenCalledWith('51', eventEntity);
    });

    it('should return true when isUnnamedFavourite', () => {
      (raceOutcomeDetailsServiceStub as any).isUnnamedFavourite.and.returnValue(true);
      expect(component.isGenericSilk(leg, index)).toEqual(true);
      expect(component.isSilkAvailable).toHaveBeenCalledWith(leg, index);
    });

    it('should return true when isUnnamedFavourite calls with outcome', () => {
      (raceOutcomeDetailsServiceStub as any).isUnnamedFavourite.and.returnValue(true);
      leg.part[0].outcomeId = undefined;
      leg.part[0].outcome = '51';
      const result = component.isGenericSilk(leg);

      expect(result).toEqual(true);
      expect(component.isSilkAvailable).toHaveBeenCalledWith(leg, index);
    });

    it('should return false when not isUnnamedFavourite but silks are available', () => {
      (component as any).isSilkAvailable.and.returnValue(true);
      expect(component.isGenericSilk(leg, index)).toEqual(false);
    });

    it('should return false when it is isUnnamedFavourite and silks are available', () => {
      (raceOutcomeDetailsServiceStub as any).isUnnamedFavourite.and.returnValue(true);
      (component as any).isSilkAvailable.and.returnValue(true);
      expect(component.isGenericSilk(leg, index)).toEqual(false);
    });

    afterEach(() => {
      expect(raceOutcomeDetailsServiceStub.isUnnamedFavourite).toHaveBeenCalledWith('51', eventEntity);
    });
  });

  describe('getClasses', () => {
    beforeEach(() => {
      component.bet = {
        eventSource: {
          isAccaEdit: true
        }
      } as any;

      component.leg = {
        eventEntity: {}
      } as any;
      component.isEnhanced = false;
      component.isVirtuals = false;
    });
    it('classes should be "lost removed"', () => {
      expect(component.getClasses({ status: 'lost', removedLeg: [{}] } as any)).toEqual('lost removed');
    });

    it('classes should be "won arrowed-item"', () => {
      component.leg.eventEntity.isDisplayed = true;
      component.bet.eventSource.isAccaEdit = false;
      component.sportPath = 'Football';
      expect(component.getClasses({ status: 'won' } as any)).toEqual('won arrowed-item');
    });

    it(`classes should not set "arrowed-item" if isAccaEdit is equal true`, () => {
      component.leg.eventEntity.isDisplayed = true;
      component.bet.eventSource.isAccaEdit = true;
      component.sportPath = 'Football';

      expect(component.getClasses({ status: 'won' } as any)).not.toContain('arrowed-item');
    });

    describe('classes should not set "arrowed-item" class', () => {
      beforeEach(() => {
        component.leg.eventEntity.isDisplayed = true;
        component.bet.eventSource.isAccaEdit = false;
        component.sportPath = 'Football';
      });

      it('on Enhanced Multiples', () => {
        component.isEnhanced = true;
        expect(component.getClasses({ status: 'won' } as any)).toEqual('won');
      });

      it('on Virtual Sports', () => {
        component.isVirtuals = true;
        expect(component.getClasses({ status: 'won' } as any)).toEqual('won is-virtual');
      });
    });

    it('classes should not set "arrowed-item" class', () => {
      component.leg.eventEntity.isDisplayed = true;
      component.bet.eventSource.isAccaEdit = false;
      component.sportPath = undefined;
      expect(component.getClasses({ status: 'won' } as any)).toEqual('won');
    });

    it('classes should not set "arrowed-item" class and set "byb-list"', () => {
      component.leg.eventEntity.isDisplayed = true;
      component.bet.eventSource.isAccaEdit = false;
      component.sportPath = 'sport-path';
      component.isBuildYourBet = true;
      expect(component.getClasses({ status: 'open' } as any)).toEqual('open byb-list');
    });

    it('classes should be "open removed is-acca-undo"', () => {
      component.leg.removing = true;
      expect(component.getClasses({ status: 'open', removing: true, removedLeg: false } as any)).toEqual('open removed is-acca-undo');
    });

    it('classes should be "open is-acca-remove"', () => {
      component.leg.removing = false;
      expect(component.getClasses({ status: 'open', removing: false } as any)).toEqual('open is-acca-remove');
    });

    it('classes should be "open is-virtual"', () => {
      component.bet.eventSource.isAccaEdit = false;
      component.isVirtuals = true;
      expect(component.getClasses({ status: 'open' } as any)).toEqual('open is-virtual');
    });

    it('should not crash if leg.eventEntity is missing', () => {
      delete component.leg.eventEntity;
      component.bet.eventSource.isAccaEdit = false;
      expect(component.getClasses({ status: 'open' } as any)).toEqual('open');
    });
  });

  describe('init', () => {
    beforeEach(() => {
      component.event = { categoryId: '1' } as any;
      spyOn(component, 'showSilk');
      spyOn(component as any, 'setSportConfig');
      component.outcomeNames = jasmine.createSpyObj('outcomeNames', ['some']);
    });

    describe('isEnhanced should be Falsy if', () => {
      it(`if typeName is Not equal 'Enhanced Multiples'`, () => {
        component.event.typeName = 'typeName';
      });
      it(`isEnhanced should be Falsy if event is not defined`, () => {
        component.event = null;
      });
      afterEach(() => {
        component['init']();
        expect(component.isEnhanced).toBeFalsy();
      });
    });
    it(`isEnhanced should be Truthy if typeName equal 'Enhanced Multiples'`, () => {
      component.event.typeName = 'Enhanced Multiples';
      component['init']();
      expect(component.isEnhanced).toBeTruthy();
    });

    describe('isVirtuals should be Falsy if', () => {
      it('if categoryCode is Not equal VIRTUAL', () => {
        component.event.categoryCode = 'categoryCode';
      });
      it('isVirtuals should be Falsy if event is not defined', () => {
        component.event = null;
      });
      afterEach(() => {
        component['init']();
        expect(component.isVirtuals).toBeFalsy();
      });
    });
    it('isVirtuals should be Truthy if categoryCode equal VIRTUAL', () => {
      component.event.categoryCode = 'VIRTUAL';
      component['init']();
      expect(component.isVirtuals).toBeTruthy();
    });

    it(`should define shouldShowSilk`, () => {
      component.shouldShowSilk = undefined;
      (component['showSilk'] as jasmine.Spy).and.returnValue(true);
      component['init']();
      expect(component['showSilk']).toHaveBeenCalledWith(component.leg);
      expect(component.shouldShowSilk).toBeTruthy();
    });

    describe('silkSpace', () => {
      beforeEach(() => {
        spyOn(component, 'isGenericSilk').and.callFake((elemLeg, i) => elemLeg.part[i].hasGenericSilk);
        spyOn(component, 'isSilkAvailable').and.callFake((elemLeg, i) => elemLeg.part[i].hasSilkAvailable);
        component.outcomeNames = ['someName', 'name'];
        (component['showSilk'] as jasmine.Spy).and.returnValue(false);
      });
      describe('should be false', () => {
        it(`should be false if isMultiple equal false`, () => {
          component.isMultiples = false;
        });

        it(`should be false if isMultiple equal false and shouldShowSilk equal false`, () => {
          component.isMultiples = false;
        });

        it(`should be false if isMultiple equal true and have not GenericSilk or SilkAvailable`, () => {
          component.isMultiples = true;
          component.leg = { part: [{ hasGenericSilk: false }, { hasSilkAvailable: false }] } as any;
        });

        it(`should be false if isMultiple equal false and have not GenericSilk or SilkAvailable and shouldShowSilk equal true`, () => {
          component.isMultiples = false;
          (component['showSilk'] as jasmine.Spy).and.returnValue(true);
          component.leg = { part: [{ hasGenericSilk: false }, { hasSilkAvailable: false }] } as any;
        });

        it(`should be false if isMultiple equal false and have not GenericSilk or SilkAvailable`, () => {
          component.isMultiples = false;
          component.leg = { part: [{ hasGenericSilk: false }, { hasSilkAvailable: false }] } as any;
        });

        it(`should be false if isMultiple equal true and have not GenericSilk or SilkAvailable and shouldShowSilk equal true`, () => {
          component.isMultiples = true;
          (component['showSilk'] as jasmine.Spy).and.returnValue(true);
          component.leg = { part: [{ hasGenericSilk: false }, { hasSilkAvailable: false }] } as any;
        });

        it(`should be false if isMultiple equal true and have not GenericSilk or SilkAvailable and shouldShowSilk equal false`, () => {
          component.isMultiples = true;
          component.leg = { part: [{ hasGenericSilk: false }, { hasSilkAvailable: false }] } as any;
        });

        afterEach(() => {
          component['init']();
          expect(component.silkSpace).toBeFalsy();
        });
      });

      describe('should be Truthy if isMultiples and shouldShowSilk and some of outcomeNames', () => {
        beforeEach(() => {
          component.isMultiples = true;
          (component['showSilk'] as jasmine.Spy).and.returnValue(true);
          component.outcomeNames.some = jasmine.createSpy('some').and.returnValue(true);
        });

        it(`and isGenericSilk`, () => {
          component.leg = { part: [{ hasGenericSilk: false }, { hasGenericSilk: true }] } as any;
        });

        it(`and isSilkAvailable`, () => {
          component.leg = { part: [{ hasSilkAvailable: false }, { hasSilkAvailable: true }] } as any;
        });

        afterEach(() => {
          component.shouldShowSilk  = true;
          component['init']();

          expect(component.silkSpace).toBeTruthy();
        });
      });
    });
  });

  describe('resetOddsCaption', () => {
    beforeEach(() => {
      component.event = { categoryId: '1' }as any;
      component['fracToDecService'].getFormattedValue = (num, den) => `${num}/${den}`;

      (<any>component.leg) = {part: [{priceNum: 4, priceDen: 8, price: [{priceStartingNum: 8, priceStartingDen: 9}]}]};
      cmsService.isBogFromCms = jasmine.createSpy().and.returnValue(of(true));
    });

    it(`odds captions were reset properly - plain and bog-prev`, () => {
      component.displayBogPrice = true;
      component.resetOddsCaptions();

      expect(component.takenOddsCaption).toBe('4/8');
      expect(component.startingOddsCaption).toBe('8/9');
    });

    it(`startingOddsCaption were not reset if bog is not enabled`, () => {
      component.displayBogPrice = false;
      component.resetOddsCaptions();

      expect(component.takenOddsCaption).toBe('4/8');
      expect(component.startingOddsCaption).toBeFalsy();
    });

    it(`startingOddsCaption were not reset if they are equal to taken`, () => {
      (<any>component.leg) = {part: [{priceNum: 4, priceDen: 8, price: [{priceStartingNum: 4, priceStartingDen: 8}]}]};
      component.displayBogPrice = true;
      component.resetOddsCaptions();

      expect(component.takenOddsCaption).toBe('4/8');
      expect(component.startingOddsCaption).toBeFalsy();
    });

    it(`startingOddsCaption were not reset if they are no prices`, () => {
      (<any>component.leg) = {part: [{priceNum: undefined, priceDen: undefined, price: [{priceStartingNum: '', priceStartingDen: null}]}]};
      component.displayBogPrice = true;
      component.resetOddsCaptions();

      expect(component.startingOddsCaption).toBeFalsy();
    });
  });

  describe('setSportConfig', () => {
    beforeEach(() => {
      component.event = { categoryId: '1' }as any;
    });

    it(`should define 'sportType'`, () => {
      component['setSportConfig']();

      expect(component.sportPath).toEqual('horsePath');
      expect(sportsConfigHelperService.getSportPathByCategoryId).toHaveBeenCalled();
    });

    it(`should define 'sportType'`, () => {
      component.sportPath = 'racing';
      component['setSportConfig']();

      expect(sportsConfigHelperService.getSportPathByCategoryId).not.toHaveBeenCalled();
    });
  });

  describe('formatOdds', () => {
    it(`should return SP price (priceTypeCodes = 'SP,')`, () => {
      component['formatOdds']({
        part: [{ priceNum: 1, priceDen: 2 }],
        eventEntity: {
          markets: [{ priceTypeCodes: 'SP,' }]
        }
      } as any);
      expect(localeStub.getString).toHaveBeenCalledWith('bethistory.SP');
    });

    it(`should return SP price (no price num/den)`, () => {
      component['formatOdds']({
        part: [{ priceNum: 0, priceDen: 0 }],
      } as any);
      expect(localeStub.getString).toHaveBeenCalledWith('bethistory.SP');
    });

    it(`should return SP price (priceType = 'S')`, () => {
      component['formatOdds']({
        part: [{
          priceNum: 1, priceDen: 2,
          price: [{
            priceType: { code: 'S' }
          }]
        }],
      } as any);
      expect(localeStub.getString).toHaveBeenCalledWith('bethistory.SP');
    });

    it(`should return decimal price`, () => {
      component['formatOdds']({
        part: [{ priceNum: 1, priceDen: 2 }],
      } as any);
      expect(fracToDecServiceStub.getFormattedValue).toHaveBeenCalledWith(1, 2);
    });

    it(`should return decimal price if on market level priceType equal 'SP' but on leg level it's 'L'`, () => {
      component['formatOdds']({
        part: [{ priceNum: 1, priceDen: 2, price: [{
            priceType: { code: 'L' }
          }]  }],
        eventEntity: {
          markets: [{ priceTypeCodes: 'SP,' }]
        }
      } as any);
      expect(fracToDecServiceStub.getFormattedValue).toHaveBeenCalledWith(1, 2);
    });
  });

  it('#fracToDec should call FracToDecService.getFormattedValue', () => {
    const priceNum: number = 3,
      priceDen: number = 7;

    component['fracToDec'](priceNum, priceDen);
    expect(fracToDecServiceStub.getFormattedValue).toHaveBeenCalledWith(priceNum, priceDen);
  });

  it('#getOutcomeName should call #getHandicapOutcomeName', () => {
    component['getHandicapOutcomeName'] = jasmine.createSpy();

    component['getOutcomeName'](leg);
    expect(component['getHandicapOutcomeName']).toHaveBeenCalledWith(leg);
  });

  it('#getHandicapOutcomeName should calculate outcome name with handicap value', () => {
    const part: IBetHistoryPart[] = [
      {
        description: 'description1',
        handicap: 'Handicap1'
      },
      {
        description: 'description2',
        handicap: 'Handicap2'
      }
    ] as any;

    leg.part = part;

    const expectResult = ['description1 (Handicap1)', 'description2 (Handicap2)'];

    expect(component['getHandicapOutcomeName'](leg)).toEqual(expectResult);
  });

  it('#calcDeductionForGP should Calculate deduction value for GP price type', () => {
    const part: IBetHistoryPart = {
      deduction: [
        {
          priceType: 'L',
          value: '78'
        }
      ],
      price: [
        {
          priceStartingNum: '3',
          priceStartingDen: '41',
          priceNum: 31,
          priceDen: 57
        }
      ]
    } as any;

    expect(component['calcDeductionForGP'](part)).toEqual('78');
  });

  it('#getRuleFourDeduction should Calculate Rule 4 deduction value and return number', () => {

    const part0: IBetHistoryPart[] = [
      {}
    ] as any;

    const part1: IBetHistoryPart[] = [{
      deduction: [
        {
          priceType: 'L',
          value: '78'
        }
      ]
    }] as any;

    const part2: IBetHistoryPart[] = [{
      deduction: [
        {
          priceType: 'L',
          value: '32'
        },
        {
          priceType: 'L',
          value: '78'
        }
      ]
    }] as any;

    const part3: IBetHistoryPart[] = [{
      deduction: [
        {
          priceType: 'L',
          value: '32'
        },
        {
          priceType: 'L',
          value: '78'
        },
        {
          priceType: 'L',
          value: '83'
        }
      ]
    }] as any;

    leg.part = part0;
    expect(component['getRuleFourDeduction'](leg)).toEqual(0);

    leg.part = part1;
    expect(component['getRuleFourDeduction'](leg)).toEqual(78);

    component['calcDeductionForGP'] = jasmine.createSpy().and.returnValue(121);
    leg.part = part2;
    expect(component['getRuleFourDeduction'](leg)).toEqual(121);
    expect(component['calcDeductionForGP']).toHaveBeenCalledWith(part2[0]);

    leg.part = part3;

    expect(component['getRuleFourDeduction'](leg)).toEqual(0);
  });

  describe('getWinLosIndicator', () => {
    it('should calculate win indicator', () => {
      const eventEntity: ISportEvent = {
        comments: {
          teams: {
            away: {
              name: 'Win',
              score: '0'
            },
            home: {
              name: 'Lose',
              score: 0
            }
          }
        }
      } as any;

      const part: IBetHistoryPart[] = [
        {
          description: 'Draw'
        }
      ] as any;

      leg.eventEntity = eventEntity;
      leg.part = part;

      expect(component['getWinLosIndicator'](leg)).toEqual('winning');

      leg.eventEntity.comments.teams.away.score = 2;
      expect(component['getWinLosIndicator'](leg)).toEqual('losing');

      leg.part[0].description = 'Win';
      expect(component['getWinLosIndicator'](leg)).toEqual('winning');

      leg.eventEntity.comments.teams.away.score = '0';
      expect(component['getWinLosIndicator'](leg)).toEqual('losing');
    });

    it('should not return any indicator if score of against team is not defined', () => {
      const eventEntity: ISportEvent = {
        comments: {
          teams: {
            away: {
              name: 'Win',
              score: 1
            },
            home: {
              name: 'Lose',
              score: null
            }
          }
        }
      } as any;

      const part: IBetHistoryPart[] = [
        {
          description: 'Win'
        }
      ] as any;

      leg.eventEntity = eventEntity;
      leg.part = part;

      expect(component['getWinLosIndicator'](leg)).toBeFalsy();

      leg.eventEntity.comments.teams.away.score = '';
      leg.eventEntity.comments.teams.home.score = 0;

      expect(component['getWinLosIndicator'](leg)).toBeFalsy();
    });
  });

  it('#parseEventMarketDescription should get market description', () => {
    const part0: IBetHistoryPart[] = [
      {
        eventMarketDesc: 'Build Your Bet test1'
      },
      {
        eventMarketDesc: 'Build Your Bet test2',
      }
    ] as any;

    component.leg = leg;
    component.leg.part = part0;
    expect(component['parseEventMarketDescription']()).toEqual('Build Your Bet');

    const part1: IBetHistoryPart[] = [
      {
        eventMarketDesc: 'Build Your Bet test1'
      }
    ] as any;

    const part2: IBetHistoryPart[] = [
      {
        eventMarketDesc: '#YourCall test1'
      }
    ] as any;

    const part3: IBetHistoryPart[] = [] as any;

    cashOutServiceStub.getEachWayTerms.and.returnValue('Build Your Bet getEachWayTerms');
    localeStub.getString.and.returnValue('Bet Builder');
    component.leg.part = part1;
    component.legType = 'TestLegType';

    expect(component['parseEventMarketDescription']()).toEqual('BET BUILDER getEachWayTerms');
    expect(cashOutServiceStub.getEachWayTerms).toHaveBeenCalledWith(part1[0], 'TestLegType');

    component.leg.part = part1;
    component.legType = 'TestLegType';

    expect(component['parseEventMarketDescription']()).toEqual('BET BUILDER getEachWayTerms');
    expect(cashOutServiceStub.getEachWayTerms).toHaveBeenCalledWith(part1[0], 'TestLegType');

    component.leg.part = part2;
    component.legType = 'TestLegType';

    expect(component['parseEventMarketDescription']()).toEqual('Build Your Bet getEachWayTerms');
    expect(cashOutServiceStub.getEachWayTerms).toHaveBeenCalledWith(part2[0], 'TestLegType');

    component.leg.part = part3;
    component.legType = 'TestLegType';

    expect(component['parseEventMarketDescription']()).toEqual('Build Your Bet getEachWayTerms');
    expect(cashOutServiceStub.getEachWayTerms).toHaveBeenCalledWith(part3[0], 'TestLegType');
  });

  it('#parseEventMarketDescription should return 5-A-Side', () => {
    localeStub.getString = jasmine.createSpy('getString').and.returnValue('5-A-Side');
    component.bet.eventSource.source = 'f';

    const result = component['parseEventMarketDescription']();

    expect(result).toEqual('5-A-Side');
  });

  it('should check if isLegSuspended', () => {
    component.isLegSuspended({} as any);
    expect(editMyAccaService.isLegSuspended).toHaveBeenCalled();
  });

  describe('getRemovingState', () => {
    it('should return true when leg is in removing state and bet not suspended', () => {
      component.leg = {
        removing: true,
        status: 'open'
      } as any;
      expect(component.getRemovingState).toBeTruthy();
    });
    it('should return false when leg is in removing state and bet is suspended', () => {
      component.leg = {
        removing: true,
        status: 'suspended'
      } as any;
      expect(component.getRemovingState).toBeFalsy();
    });
    it('should return false when leg is in not in removing state', () => {
      component.leg = {
        removing: false,
        status: 'open'
      } as any;
      expect(component.getRemovingState).toBeFalsy();
    });
  });

  describe('showRemovedLabel', () => {
    it('should showRemovedLabel', () => {
      component.leg = { removing: false, removedLeg: false } as any;
      component.isRemovingState = false;
      expect(component.getShowRemovedLabelValue).toBeFalsy();
    });

    it('should showRemovedLabel(removing)', () => {
      component.leg = { removing: true } as any;
      component.isRemovingState = true;
      expect(component.getShowRemovedLabelValue).toBeTruthy();
    });

    it('should showRemovedLabel(removed)', () => {
      component.leg = { removedLeg: true } as any;
      component.isRemovingState = false;
      expect(component.getShowRemovedLabelValue).toBeTruthy();
    });

    it('should showRemovedLabel(removed resultedBeforeRemoval)', () => {
      component.leg = { removedLeg: true, resultedBeforeRemoval: true } as any;
      component.isRemovingState = false;
      expect(component.getShowRemovedLabelValue).toBeFalsy();
    });
  });

  describe('BMA-40873. Live Score Updates', () => {
    beforeEach(() => {
      component['filtersService']['isGreyhoundsEvent'] = jasmine.createSpy().and.returnValue(false);
      spyOn<any>(component, 'formatOdds');
      spyOn<any>(component, 'getOutcomeName').and.returnValue([]);
      spyOn<any>(component, 'parseEventMarketDescription');
      spyOn<any>(component, 'getRuleFourDeduction');
    });

    it('should subscribe on live score updates', () => {
      component['pubSubService'].subscribe = jasmine.createSpy();
      component.leg = {
        ...leg,
        eventEntity: {
          id: 555,
          categoryCode: 'football',
          comments: {},
          markets: []
        }
      } as any;
      component.ngOnInit();

      expect(component['pubSubService'].subscribe).toHaveBeenCalledTimes(3);
    });

    describe('CASHOUT_LIVE_SCORE_EVENT_UPDATE subscription', () => {
      let update;

      beforeEach(() => {
        update = {
          id: 555,
          payload: {
            scores: {}
          }
        } as any;

        component.leg = {
          ...leg,
          eventEntity: {
            id: 555,
            comments: {},
            markets: []
          }
        } as any;

        component['pubSubService'].subscribe = jasmine.createSpy().and.callFake((sb, ch, fn) => {
          if (ch === pubSubService.API.CASHOUT_LIVE_SCORE_EVENT_UPDATE) {
            fn(update);
          }
        });
      });

      it('should update scores', () => {
        component.ngOnInit();

        expect(commentsService.sportUpdateExtend).toHaveBeenCalledWith(component.leg.eventEntity.comments, update.payload.scores);
      });

      it('should not update scores if not the same event', () => {
        update.id = 1000;
        component.ngOnInit();

        expect(commentsService.sportUpdateExtend).not.toHaveBeenCalled();
      });

      it('should not update scores if no update data', () => {
        update = null;
        component.ngOnInit();

        expect(commentsService.sportUpdateExtend).not.toHaveBeenCalled();
      });

      it('should not update scores if no payload for update', () => {
        update.payload = null;
        component.ngOnInit();

        expect(commentsService.sportUpdateExtend).not.toHaveBeenCalled();
      });

      it('should not update scores if no scores for update', () => {
        update.payload.scores = null;
        component.ngOnInit();

        expect(commentsService.sportUpdateExtend).not.toHaveBeenCalled();
      });

      it('should not update scores if event has for scores', () => {
        component.leg.eventEntity.comments = null;
        component.ngOnInit();

        expect(commentsService.sportUpdateExtend).not.toHaveBeenCalled();
      });
    });

    describe('CASHOUT_LIVE_SCORE_UPDATE subscription', () => {
      let update;

      beforeEach(() => {
        update = {
          id: 555,
          payload: {
            scores: {}
          }
        } as any;

        component.leg = {
          ...leg,
          legSort: '',
          eventEntity: {
            id: 555,
            categoryCode: 'football',
            comments: {},
            markets: []
          }
        } as any;

        component['pubSubService'].subscribe = jasmine.createSpy().and.callFake((sb, ch, fn) => {
          if (ch === pubSubService.API.CASHOUT_LIVE_SCORE_UPDATE) {
            fn(update);
          }
        });
      });

      it('should update scores', () => {
        component.ngOnInit();

        expect(footballUpdateExtendSpy).toHaveBeenCalledWith(component.leg.eventEntity.comments, update.payload);
      });

      it('should not update scores if not the same event', () => {
        update.id = 1000;
        component.ngOnInit();

        expect(footballUpdateExtendSpy).not.toHaveBeenCalled();
      });

      it('should not update scores if no update data', () => {
        update = null;
        component.ngOnInit();

        expect(footballUpdateExtendSpy).not.toHaveBeenCalled();
      });

      it('should not update scores if no payload data', () => {
        update.payload = null;
        component.ngOnInit();

        expect(footballUpdateExtendSpy).not.toHaveBeenCalled();
      });

      it('should not update scores if event has for scores', () => {
        component.leg.eventEntity.comments = null;
        component.ngOnInit();

        expect(footballUpdateExtendSpy).not.toHaveBeenCalled();
      });

      it('should not update scores if event has for scores', () => {
        component.leg.eventEntity.comments = null;
        component.ngOnInit();

        expect(footballUpdateExtendSpy).not.toHaveBeenCalled();
      });

      it('should not update scores if unknown sport to update', () => {
        component['commentsService']['footballUpdateExtend'] = undefined;
        component.ngOnInit();

        expect(footballUpdateExtendSpy).not.toHaveBeenCalled();
      });
    });

    describe('Test isBogFromPriceType', () => {
      beforeEach(() => {
        component.leg = leg;
        component['filtersService']['isGreyhoundsEvent'] = jasmine.createSpy().and.returnValue(false);
      });

      it('should call isBogEnabledFromPriceType() and return isBog=false if priceType.codes is "" ', () => {
        component.isBogFromPriceType();

        expect(component.leg.part[0].isBog).toBe(false);
      });

      it('should call isBogEnabledFromPriceType() and return isBog=false if priceType is "null" ', () => {
        component.leg.part[0].price[0].priceType = null;
        component.isBogFromPriceType();

        expect(component.leg.part[0].isBog).toBe(false);
      });

      it('should call isBogEnabledFromPriceType() and return isBog=false if priceType.codes is "null" ', () => {
        component.leg.part[0].price[0].priceType.code = null;
        component.isBogFromPriceType();

        expect(component.leg.part[0].isBog).toBe(false);
      });

      it('should call isBogEnabledFromPriceType() and return isBog=true if priceType.codes is "GP" ', () => {
        component.leg.part[0].price[0].priceType.code = 'GP';
        component.isBogFromPriceType();

        expect(component.leg.part[0].isBog).toBe(true);
      });

      it('should call isBogEnabledFromPriceType() and return isBog=false if priceType.codes is "S" ', () => {
        component.leg.part[0].price[0].priceType.code = 'S';
        component.isBogFromPriceType();

        expect(component.leg.part[0].isBog).toBe(false);
      });

      it('should set isBog to false in case of receiving Greyhounds categoryId (19)', () => {
        component['filtersService']['isGreyhoundsEvent'] = jasmine.createSpy().and.returnValue(true);
        component.isBogFromPriceType();

        expect(component.leg.part[0].isBog).toBe(false);
      });
    });
  });

  describe('formatStartingOdds', () => {
    it('should return empty string if no data', () => {
      expect(component['formatStartingOdds']({} as IBetHistoryLeg)).toBe('');
    });

    it('should return number if price is present and market is not SP', () => {
      const leg1 = {
        eventEntity: {
          markets: [{
            priceTypeCodes: 'not SP'
          }]
        },
        part: [{
          price: [{
            priceStartingNum: 1,
            priceStartingDen: 2
          }]
        }]
      };

      component['fracToDec'] = jasmine.createSpy('fracToDec').and.callFake(
        (num: number, den: number) =>  (1 + (num / den)).toFixed(2)
      );

      expect(component['formatStartingOdds'](leg1 as any)).toBe('1.50');
    });

    it('should return empty string in case if SP event despite price info absence', () => {
      const leg1 = {
        eventEntity: {
          markets: [{
            priceTypeCodes: 'SP'
          }]
        }
      };

      expect(component['formatStartingOdds'](leg1 as any)).toBe('');
    });
  });
});
