import { Component, Input, OnDestroy, OnInit, HostListener, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'underscore';
import environment from '@environment/oxygenEnvConfig';

import { RaceOutcomeDetailsService } from '@core/services/raceOutcomeDetails/race-outcome-details.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { FracToDecService } from '@core/services/fracToDec/frac-to-dec.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { CashOutService } from '../../services/cashOutService/cash-out.service';
import { EditMyAccaService } from '@app/betHistory/services/editMyAcca/edit-my-acca.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { CommentsService } from '@core/services/comments/comments.service';
import { EventService } from '@sb/services/event/event.service';

import { IBetHistoryLeg, IBetHistoryPart, IBetHistoryBet, ILegItemPrice } from '@app/betHistory/models/bet-history.model';
import { ISilkStyleModel } from '@core/services/raceOutcomeDetails/silk-style.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { ISportsConfigObject } from '@core/models/sports-config.model';
import { ILiveServeUpd } from '@core/models/live-serve-update.model';
import { SportsConfigHelperService } from '@sb/services/sportsConfig/sport-config-helper.service';
import { CmsService } from '@core/services/cms/cms.service';
import { IPrice } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { CHANNEL } from '@shared/constants/channel.constant';
import { BetTrackingService } from '@lazy-modules/bybHistory/services/betTracking/bet-tracking.service';
import { UsedFromWidgetAbstractComponent } from '@core/abstract-components/used-from-widget-abstract.component';

@Component({
  selector: 'bet-leg-item',
  templateUrl: './bet-leg-item.component.html',
  styleUrls: ['./bet-leg-item.component.less']
})
export class BetLegItemComponent extends UsedFromWidgetAbstractComponent implements OnInit, OnDestroy, OnChanges {

  @Input() bet: { eventSource: IBetHistoryBet, location: string };
  @Input() leg: IBetHistoryLeg;
  @Input() legType: string;

  readonly RULE_FOUR_URL: string = environment.RULE_FOUR_URL;

  excludedDrilldownTagNames: string;
  takenOddsCaption: string | number;
  startingOddsCaption: string | number;
  outcomeNames: string[];
  eventMarketDescription: string;
  ruleFourDeduction: number;
  filterPlayerName: Function;
  filterAddScore: Function;
  sportPath: string;
  event: ISportEvent;
  isEnhanced: boolean;
  isVirtuals: boolean;
  sportsConfig: ISportsConfigObject;
  showRemovedLabel: boolean;
  isRemovingState: boolean;
  statusName: string;
  shouldShowSilk: boolean;
  silkSpace: boolean;
  isMultiples: boolean;
  isUKorIRE: boolean;
  isFCTC: boolean;
  displayBogPrice: boolean;
  isBuildYourBet: boolean = false;
  shouldShowFiveASideIcon: boolean;

  private readonly HORSE_RACING_CATEGORY_ID: string = environment.HORSE_RACING_CATEGORY_ID;
  private readonly START_PRICE_TYPE = 'S';
  private readonly LIVE_PRICE_TYPE = 'L';
  private componentId: string;
  private controllerIdentifier: string;

  constructor(
    private cashOutService: CashOutService,
    private raceOutcomeDetails: RaceOutcomeDetailsService,
    private locale: LocaleService,
    private fracToDecService: FracToDecService,
    private pubSubService: PubSubService,
    private filtersService: FiltersService,
    private editMyAccaService: EditMyAccaService,
    private router: Router,
    private routingHelperService: RoutingHelperService,
    private commentsService: CommentsService,
    private eventService: EventService,
    private sportsConfigHelperService: SportsConfigHelperService,
    private cmsService: CmsService,
    private betTrackingService: BetTrackingService
  ) {
    super();
    this.filterPlayerName = this.filtersService.filterPlayerName;
    this.filterAddScore = this.filtersService.filterAddScore;
  }

  @HostListener('click') onClick() {
   (!this.bet.eventSource.isAccaEdit && this.leg.eventEntity && Boolean(this.leg.eventEntity.isDisplayed)) && this.goToEvent();
  }

  ngOnInit(): void {
    // Check if bet has ForeCast or TriCast market (legSort.code contain this info)
    this.isFCTC = this.leg.legSort && /^(SF|RF|CF|TC|CT)$/.test(typeof this.leg.legSort === 'string' ?
      this.leg.legSort : this.leg.legSort.code);
    this.isUKorIRE = !!this.leg.eventEntity && this.eventService.isUKorIRE(this.leg.eventEntity);
    this.shouldShowFiveASideIcon = this.bet.eventSource.source === CHANNEL.fiveASide;
    this.resetOddsCaptions();
    this.isBuildYourBet = this.betTrackingService.checkIsBuildYourBet(this.leg.part);
    this.outcomeNames = this.getOutcomeName(this.leg);
    this.isMultiples = this.outcomeNames.length > 1;
    this.eventMarketDescription = this.parseEventMarketDescription();
    this.ruleFourDeduction = this.getRuleFourDeduction(this.leg);
    this.componentId = _.uniqueId();
    this.controllerIdentifier = `BetLegItemComponent${this.componentId}`;
    this.pubSubService.subscribe(this.controllerIdentifier,
      [this.pubSubService.API.UPDATE_EMA_ODDS, this.pubSubService.API.SET_ODDS_FORMAT], () => {
        this.resetOddsCaptions();
    });
    this.isRemovingState = this.getRemovingState;
    this.showRemovedLabel = this.getShowRemovedLabelValue;
    this.statusName = this.getStatusName;

    this.event = this.eventEntity;
    this.excludedDrilldownTagNames = this.getExcludedDrilldownTagNames();

    if (!this.event) { return; }

    this.init();

    // TODO (BMA-40873): must be removed after refactoring services for live updates
    this.pubSubService.subscribe(this.componentId, this.pubSubService.API.CASHOUT_LIVE_SCORE_EVENT_UPDATE, (update: ILiveServeUpd) => {
      if (update && this.eventEntity.id === update.id && update.payload && update.payload.scores && this.eventEntity.comments) {
        this.commentsService.sportUpdateExtend(this.eventEntity.comments, update.payload.scores);
      }
    });

    // TODO (BMA-40873): must be removed after refactoring services for live updates
    this.pubSubService.subscribe(this.componentId, this.pubSubService.API.CASHOUT_LIVE_SCORE_UPDATE, (update: ILiveServeUpd) => {
      if (update && this.eventEntity.id === update.id && update.payload && this.eventEntity.comments) {
        const methodName = `${this.eventEntity.categoryCode.toLowerCase()}UpdateExtend`;
        const extender = this.commentsService[methodName];

        if (extender) {
          extender(this.eventEntity.comments, update.payload);
          this.commentsService.extendWithScoreType(this.eventEntity, this.eventEntity.categoryCode);
        }
      }
    });
    this.isBogFromPriceType();
    this.checkForBogOddsAndResetOddsCaptions();
  }

  checkForBogOddsAndResetOddsCaptions(): void {
    // only for settled bets
    const betSettled: boolean = this.bet && this.bet.eventSource && this.bet.eventSource.settled === 'Y';
    const legPart = this.leg && this.leg.part && this.leg.part[0];
    if (betSettled) {
      this.cmsService.isBogFromCms().subscribe((bog: boolean) => {
        this.displayBogPrice = bog && legPart && legPart.isBog;
        this.resetOddsCaptions();
      });
    } else {
      this.resetOddsCaptions();
    }
  }

  getExcludedDrilldownTagNames(): string {
    return 'EVFLAG_MB,MKTFLAG_MB,EVFLAG_PB,MKTFLAG_PB';
  }

  extractNumericPricesFromLeg(): ILegItemPrice {
    const legPart = this.leg && this.leg.part && this.leg.part[0];
    const priceContainer = legPart && legPart.price && legPart.price[0];

    return  {
      startingPrice: {
        num: parseFloat(priceContainer.priceStartingNum),
        den: parseFloat(priceContainer.priceStartingDen)
      },
      price: {
        num: legPart.priceNum,
        den: legPart.priceDen
      }
    };
  }

 /**
  * Price comparison in leg.part:  priceNum / priceDen with priceStartingNum / priceStartingDec
  * @returns {Boolean} [The value which indicates which price (Price Taken or Starting Price) is bigger]
  */
  startingPricesBigger(): boolean {
    const {startingPrice, price} = this.extractNumericPricesFromLeg();

    if (startingPrice.num && startingPrice.den && price.num && price.den) {
      return (startingPrice.num / startingPrice.den) > (price.num / price.den);
    }
  }

  resetOddsCaptions(): void  {
    this.takenOddsCaption = this.formatOdds(this.leg);
    if (this.displayBogPrice) {
      const startingOddsToSet = this.formatStartingOdds(this.leg);

      if (this.startingPricesBigger() && this.takenOddsCaption !== startingOddsToSet) {
        this.startingOddsCaption = startingOddsToSet;
      }
    }
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe(this.controllerIdentifier);
    this.pubSubService.unsubscribe(this.componentId);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.leg) {
      this.isRemovingState = this.getRemovingState;
      this.showRemovedLabel = this.getShowRemovedLabelValue;
      this.statusName = this.getStatusName;
    }
  }

  /**
   * Getter for leg status icon
   * @returns {string}
   */
  get getStatusName(): string {
    switch (this.leg.status) {
      case 'suspended': {
        return this.locale.getString('bethistory.suspended');
      }
      case 'void': {
        return this.locale.getString('bethistory.void');
      }
      case 'won': {
        return this.isMultiples ? this.bet.eventSource.totalStatus : this.leg.status;
      }
      case 'lost': {
        return this.isMultiples ? this.bet.eventSource.totalStatus : this.leg.status;
      }
      default: {
        return '';
      }
    }
  }

  /**
   * Get event entity from ss or backup event entity form bet
   * @returns {(Object|undefined)}
   */
  get eventEntity(): ISportEvent {
    return this.leg.eventEntity || (this.leg.noEventFromSS && this.leg.backupEventEntity);
  }

  /**
   * Get Winning/Losing Indicator for Football Accumulator
   * @return
   */
  get winLosIndicator(): string {
    let winLosIndicator;
    const isFootball = this.leg.eventEntity && this.leg.eventEntity.categoryCode === 'FOOTBALL';
    const isMatchResult = this.eventMarketDescription === 'Match Result' || this.eventMarketDescription === 'Match Betting';
    if (isFootball && isMatchResult && this.leg.status === 'open' && this.leg.eventEntity.comments) {
      winLosIndicator = this.getWinLosIndicator(this.leg);
    }
    return winLosIndicator;
  }

  trackByOutcomeName(index: number, outcomeName: string): string {
    return `${index}_${outcomeName}`;
  }

  isLegSuspended(leg: IBetHistoryLeg): boolean {
    return this.editMyAccaService.isLegSuspended(leg);
  }

  get getShowRemovedLabelValue(): boolean {
    return (this.isRemovingState || this.leg.removedLeg) && !this.leg.resultedBeforeRemoval;
  }

  get getRemovingState(): boolean {
    return this.leg.removing && this.leg.status !== 'suspended';
  }

  /**
   * [Checks whether silk should be shown for leg
   * @param  {Object} leg [leg object]
   * @return {Boolean}     [Value which indicates whether silk should be shown
   * be shown for chosen leg object]
   */
  showSilk(leg: IBetHistoryLeg): boolean {
    const isHorseRacingEvent = () => leg.eventEntity.categoryId === this.HORSE_RACING_CATEGORY_ID;
    return leg.eventEntity && isHorseRacingEvent();
  }

  /**
   * [Return object with silk styles for leg]
   * @param  {Object} leg [leg object]
   * @return {Object}     [object with silk styles]
   */
  getSilkStyle(leg: IBetHistoryLeg, index: number = 0): ISilkStyleModel {
    const id = leg.part[index].outcomeId || leg.part[index].outcome;
    return this.raceOutcomeDetails.getSilkStyleForPage((id as string), leg.eventEntity, leg.allSilkNames, true);
  }

  /**
   * [Check whether silk is available for chosen part]
   * @param  {Object}  leg   [let object]
   * @param  {Number}  index [Index of part in array of parts]
   * @return {Boolean}
   */
  isSilkAvailable(leg: IBetHistoryLeg, index: number = 0): boolean {
    const id = leg.part[index].outcomeId || leg.part[index].outcome;
    return this.raceOutcomeDetails.isSilkAvailableForOutcome((id as string), leg.eventEntity);
  }

  /**
   * [Check whether default silk image should be shown]
   * Note: the generic silk is shown only for Unnamed Favourite.
   * @param  {Object} leg [leg object]
   * @param  {number} index - part index
   * @return {Boolean}     [Value which identifies whether silk image should be shown]
   */
  isGenericSilk(leg: IBetHistoryLeg, index = 0): boolean {
    const id = leg.part[index].outcomeId || leg.part[index].outcome;

    return this.raceOutcomeDetails.isUnnamedFavourite((id as string), leg.eventEntity) && !this.isSilkAvailable(leg, index);
  }

  getClasses(leg: IBetHistoryLeg): string {
    return `${leg.status}${(leg.removedLeg && !leg.resultedBeforeRemoval) || leg.removing ? ' removed' : ''}` +
          `${!this.bet.eventSource.isAccaEdit && this.sportPath && this.leg.eventEntity && !this.isBuildYourBet
          && !this.isEnhanced && !this.isVirtuals && !!this.leg.eventEntity.isDisplayed ? ' arrowed-item' : ''}` +
          `${this.bet.eventSource.isAccaEdit && !leg.removing&& !leg.removedLeg ? ' is-acca-remove' : ''}` +
          `${this.bet.eventSource.isAccaEdit && leg.removing ? ' is-acca-undo' : ''}` +
          `${this.isBuildYourBet ? ' byb-list' : ''}` +
          `${this.isVirtuals ? ' is-virtual' : ''}`;
  }

  /**
   * Go to event details page
   * @returns {string}
   */
  goToEvent(): string {
    // if could not get sportType(e.g. yourcall) - do nothing
    if (!this.sportPath || this.isEnhanced || this.isVirtuals) {
      return '';
    }

    const edpObj = this.event.categoryName ? this.event : _.extend(this.event, { categoryName: this.sportPath });
    const edpUrl = this.routingHelperService.formEdpUrl(edpObj);

    this.router.navigateByUrl(edpUrl);

    return edpUrl;
  }

  /**
   * Show BOG icon if Best Odds guarantied for horse racing events.
   * Best odds are guarantied when priceType.code is GP (for Ladbrokes brand) or G (for Coral)
   * @return {boolean}
   */
  isBogFromPriceType(): void {
    this.leg.part.forEach((part: IBetHistoryPart) => {
      const eventCategoryId: string = part.outcome[0].eventCategory.id;
      if (this.filtersService.isGreyhoundsEvent(eventCategoryId)) {
        part.isBog = false;
      } else {
        part.isBog = part.price.some((price: IPrice) => {
          const priceTypeCode = price && price.priceType && price.priceType.code;
          return priceTypeCode && (priceTypeCode === 'G' || priceTypeCode === 'GP');
        });
      }
    });
  }

  private init(): void {
    this.shouldShowSilk = this.showSilk(this.leg);
    this.isEnhanced = this.event && this.event.typeName === 'Enhanced Multiples';
    this.isVirtuals = this.event && this.event.categoryCode === 'VIRTUAL';
    this.silkSpace = this.shouldShowSilk && this.isMultiples
      && this.outcomeNames.some((name: string, i: number) => this.isGenericSilk(this.leg, i) || this.isSilkAvailable(this.leg, i));

    this.setSportConfig();
  }

  /**
   * Sets sportConfig and sportType
   * @private
   */
  private setSportConfig(): void {
    if (this.sportPath) {
      return;
    }

    this.sportsConfigHelperService.getSportPathByCategoryId(Number(this.event.categoryId))
      .subscribe(sportPath => this.sportPath = sportPath);
  }

  /**
   * Formats odds by given prices and user configurations.
   * @param selection {object}
   * @return {string|number}
   * @private
   */
  private formatOdds(leg: IBetHistoryLeg): string | number {
    const legPart = leg.part[0];
    const priceNum = legPart.priceNum;
    const priceDen = legPart.priceDen;
    const marketEntity = leg.eventEntity && leg.eventEntity.markets[0];
    const isLP = (legPart.price && legPart.price[0].priceType && legPart.price[0].priceType.code === 'L');

    const isSP = !isLP && (
      (marketEntity && marketEntity.priceTypeCodes === 'SP,') ||
      (!priceNum && !priceDen) ||
      (legPart.price && legPart.price[0].priceType && legPart.price[0].priceType.code === 'S')
    );

    return isSP ? this.locale.getString('bethistory.SP') : this.fracToDec(Number(priceNum), Number(priceDen));
  }

  private formatStartingOdds(leg: IBetHistoryLeg): string | number {
    const priceContainer = leg.part && leg.part[0] && leg.part[0].price && leg.part[0].price[0] && leg.part[0].price[0],
      priceNum = priceContainer && priceContainer.priceStartingNum,
      priceDen = priceContainer && priceContainer.priceStartingDen,
      marketEntity = leg.eventEntity && leg.eventEntity.markets && leg.eventEntity.markets[0],
      isSP = (marketEntity && marketEntity.priceTypeCodes === 'SP,') || (!priceNum && !priceDen);
    return isSP ? '' : this.fracToDec(Number(priceNum), Number(priceDen));
  }

  /**
   * Convert odds format
   * @param priceNum {number}
   * @param priceDen {number}
   * @returns {*}
   * @private
   */
  private fracToDec(priceNum: number, priceDen: number): string | number {
    return this.fracToDecService.getFormattedValue(priceNum, priceDen);
  }

  /**
   * Calculates outcome name with handicap value
   * @param leg
   * @returns {string[]}
   * @private
   */
  private getOutcomeName(leg: IBetHistoryLeg): string[] {
    return this.getHandicapOutcomeName(leg);
  }

  /**
   * Calculates outcome name with handicap value
   * @param leg
   * @returns {string[]}
   * @private
   */
  private getHandicapOutcomeName(leg: IBetHistoryLeg): string[] {
    return _.map(leg.part, (part: IBetHistoryPart) => {
      const { description, handicap } = part,
        isPlusNeeded = (Number(handicap) > 0) && ((handicap as string).indexOf('+') === -1),
        handicapValue = handicap && _.isString(handicap) ? ` (${isPlusNeeded ? '+' : ''}${handicap})` : '';
      return `${description}${handicapValue}`;
    });
  }

  /**
   * [Calculate deduction value for GP price type]
   * @param  {Object} part [part object]
   * @return {String}      [deduction value]
   */
  private calcDeductionForGP(part: IBetHistoryPart): string {
    const price = part.price[0],
      deductions = part.deduction,
      startPriceIsSet = price.priceStartingNum !== '',
      getStartPrice = () => +price.priceStartingNum / +price.priceStartingDen,
      livePrice = +price.priceNum / +price.priceDen,
      priceUsedForDeduction = !startPriceIsSet || livePrice >= getStartPrice() ? this.LIVE_PRICE_TYPE : this.START_PRICE_TYPE;

    return _.chain(deductions)
      .filter(deduction => deduction.priceType === priceUsedForDeduction)
      .pluck('value')
      .first()
      .value();
  }

  /**
   * Calculate Rule 4 deduction value
   * @param  {Object} leg [Leg object]
   * @return {Number}     [Deduction value]
   */
  private getRuleFourDeduction(leg: IBetHistoryLeg): number {
    const deductions = leg.part[0].deduction;
    let deductionValue: number | string = 0;

    if (_.isArray(deductions)) {
      switch (deductions.length) {
        /**
         * Start price and Live price case
         */
        case 1: {
          deductionValue = deductions[0].value;
          break;
        }
        /**
         * Guarantee price case
         */
        case 2: {
          deductionValue = this.calcDeductionForGP(leg.part[0]);
          break;
        }
        default: {
          deductionValue = 0;
        }
      }
    }

    return +deductionValue;
  }

  /**
   * Winning/Losing Indicator calculating
   * @return {String} result: 'Winning'/'Losing'
   */
  private getWinLosIndicator(leg: IBetHistoryLeg): string {
    const placedBet = leg.part[0].description;
    const scores = {
      bet: null,
      against: null
    };
    let result;

    if (placedBet === 'Draw') {
      result = parseInt(leg.eventEntity.comments.teams.home.score, 10) ===
        parseInt(leg.eventEntity.comments.teams.away.score, 10) ? 'winning' : 'losing';
    } else {
      _.each((leg.eventEntity.comments.teams as { name: string, score: string; }[]),
        (team: { name: string, score: string; }) => {
          if (team.name === placedBet) {
            scores.bet = parseInt(team.score, 10);
          } else {
            scores.against = parseInt(team.score, 10);
          }
        });
      if (!isNaN(scores.bet) && !isNaN(scores.against)) {
        result = scores.bet > scores.against ? 'winning' : 'losing';
      }
    }

    return result;
  }

  /**
   * Get event market description
   * @returns {string}
   * @private
   */
  private parseEventMarketDescription(): string {
    if (this.bet.eventSource.source === CHANNEL.fiveASide) {
      return this.locale.getString('yourCall.fiveASide');
    }
    if (this.leg.part.length > 1 && /Build Your Bet/gi.test(this.leg.part[0].eventMarketDesc)) {
      return this.locale.getString('yourcall.buildYourBet');
    } else if (this.leg.part.length === 1 && /Build Your Bet/gi.test(this.leg.part[0].eventMarketDesc)) {
      return this.cashOutService.getEachWayTerms(this.leg.part[0], this.legType).
      replace(/Build Your Bet/ig, this.locale.getString('yourcall.buildYourBet').toUpperCase());
    }
    return this.cashOutService.getEachWayTerms(this.leg.part[0], this.legType).
    replace(/#YourCall/ig, this.locale.getString('yourcall.buildYourBet'));
  }
}
