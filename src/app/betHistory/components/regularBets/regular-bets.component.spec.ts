import { of as observableOf } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';

import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';
import { RegularBetsComponent } from '@app/betHistory/components/regularBets/regular-bets.component';
import { cashoutConstants } from '../../constants/cashout.constant';

describe('RegularBetsComponent', () => {
  let component: RegularBetsComponent,
    locale,
    cashOutSectionService,
    pubsub,
    emaService,
    callbackHandler,
    windowRef,
    betTrackingService,
    changeDetectorRef,
    handleScoreboardsStatsUpdatesService;

  const content = 'static block content';
  const areaInput = 'open-bets-page';

  beforeEach(() => {
    callbackHandler = (ctrlName: string, eventName: string, callback) => {
      if (eventName === 'LIVE_BET_UPDATE') {
        callback && callback({});
      } else if(eventName === 'UPDATE_CASHOUT_BET') {
        callback('bet');
      }
    };

    emaService = {
      clearAccas: jasmine.createSpy('emaService.clearAccas')
    };
    locale = {
      getString: jasmine.createSpy(),
    };
    cashOutSectionService = {
      updateBet: jasmine.createSpy(),
      createDataForRegularBets: jasmine.createSpy(),
      generateBetsMap: jasmine.createSpy('generateBetsMap').and.returnValue(observableOf({'123': {}})),
      generateBetsArray: jasmine.createSpy().and.returnValue(
        [
          {
            eventSource: {
              event: ['123456']
            }
          }
          ] as any),
      removeListeners: jasmine.createSpy(),
      registerController: jasmine.createSpy(),
      removeCashoutItemWithTimeout: jasmine.createSpy().and.returnValue(observableOf()),
      isCashoutError: jasmine.createSpy('isCashoutError'),
      getCashoutError: jasmine.createSpy('getCashoutError')
    };
    windowRef = {
      nativeWindow: {
        setInterval: jasmine.createSpy().and.callFake((callback) => {
          callback && callback();
        }),
        clearInterval: jasmine.createSpy(),
      }
    };
    changeDetectorRef = {
      detectChanges: jasmine.createSpy(),
      detach: jasmine.createSpy()
    };
    pubsub = {
      subscribe: jasmine.createSpy('subscribe').and.callFake(callbackHandler),
      API: pubSubApi,
      unsubscribe: jasmine.createSpy(),
    };
    betTrackingService = {
      isTrackingEnabled: jasmine.createSpy('isTrackingEnabled').and.returnValue(observableOf(true)),
      getStaticContent: jasmine.createSpy('getStaticContent').and.returnValue(observableOf(content)),
    };
    handleScoreboardsStatsUpdatesService = {
      getStatisticsEventIds: jasmine.createSpy('getStatisticsEventIds').and.returnValue(observableOf('123456'))
    };

    component = new RegularBetsComponent(
      emaService,
      locale,
      cashOutSectionService,
      windowRef,
      changeDetectorRef,
      pubsub,
      betTrackingService,
      handleScoreboardsStatsUpdatesService
    );
  });

  describe('ngOnInit', () => {

    it('should ini bets map', fakeAsync(() => {
      component.ngOnInit();
      tick();

      expect(cashOutSectionService.generateBetsMap).toHaveBeenCalled();
      expect(component.betsMap).toBeTruthy();
    }));

    it('should not sync with cashout update flow on settled tab', () => {
      component.isBetHistoryTab = true;
      component.ngOnInit();

      expect(pubsub.subscribe).not.toHaveBeenCalledWith(jasmine.any(String), 'LIVE_BET_UPDATE', jasmine.any(Function));
    });

    it('should sync with cashout update flow extending passed params', () => {
      component.isBetHistoryTab = false;
      component.ngOnInit();

      expect(pubsub.subscribe).toHaveBeenCalledWith(jasmine.any(String), 'LIVE_BET_UPDATE', jasmine.any(Function));
      expect(cashOutSectionService.removeCashoutItemWithTimeout).toHaveBeenCalled();
      const args = cashOutSectionService.removeCashoutItemWithTimeout.calls.argsFor(0);
      expect(args[0]).toEqual(jasmine.any(Object));
      expect(args[1]).toEqual(jasmine.any(Object));
      expect(args[1].isRegularBets).toBeTruthy();
    });

    it('should show opta disclaimer', fakeAsync(() => {
      cashOutSectionService.generateBetsArray.and.returnValue([
        {
          eventSource: {
            event: ['123456']
          }
        }
      ] as any);
      component.ngOnInit();

      tick();
      expect(handleScoreboardsStatsUpdatesService.getStatisticsEventIds).toHaveBeenCalled();
      expect(component.bets[0].optaDisclaimerAvailable).toBeTruthy();
    }));

    it('should not show opta disclaimer', fakeAsync(() => {
      cashOutSectionService.generateBetsArray.and.returnValue([
        {
          eventSource: {
            event: ['654321']
          }
        }
      ] as any);
      component.ngOnInit();

      tick();
      expect(handleScoreboardsStatsUpdatesService.getStatisticsEventIds).toHaveBeenCalled();
      expect(component.bets[0].optaDisclaimerAvailable).toBeFalsy();
    }));
  });

  describe('Init', () => {
    it('should get static block if feature toggle is on', fakeAsync(() => {
      const betsMapMock = {bybType: '5-A-SIDE'};
      cashOutSectionService.generateBetsMap.and.returnValue(observableOf(betsMapMock));
      component.betTrackingEnabled = true;
      component.area = 'open-bets-page';
      component.isUsedFromWidget = true;
      component['init']();
      tick();
      expect(cashOutSectionService.generateBetsMap).toHaveBeenCalled();
      expect(betTrackingService.getStaticContent).toHaveBeenCalled();
      expect(betTrackingService.isTrackingEnabled).toHaveBeenCalled();
      expect(component.betsMap).toBeTruthy();
      expect(component.optaDisclaimer).toEqual(content);
      expect(component.betTrackingEnabled).toBeTruthy();
    }));

    it('shouldnt get static block if feature toggle is off', fakeAsync(() => {
      const betsMapMock = {bybType: '5-A-SIDE'};
      cashOutSectionService.generateBetsMap.and.returnValue(observableOf(betsMapMock));
      betTrackingService.isTrackingEnabled.and.returnValue(observableOf(false));
      component.area = 'open-bets-page';
      component['init']();
      tick();
      expect(cashOutSectionService.generateBetsMap).toHaveBeenCalled();
      expect(betTrackingService.getStaticContent).not.toHaveBeenCalled();
      expect(betTrackingService.isTrackingEnabled).toHaveBeenCalled();
      expect(component.betsMap).toBeTruthy();
      expect(component.optaDisclaimer).toEqual(null);
      expect(component.betTrackingEnabled).toBeFalsy();
    }));
  });

  it('cashout updateBet', () => {
    component.area = areaInput;
    component.ngOnInit();

    expect(cashOutSectionService.updateBet).toHaveBeenCalledTimes(1);
    expect(cashOutSectionService.updateBet).toHaveBeenCalledWith('bet', [
      {
        eventSource: {
          event: ['123456']
        },
        optaDisclaimerAvailable: true
      }
    ] as any);

    expect(changeDetectorRef.detectChanges).toHaveBeenCalledTimes(2);

    expect(cashOutSectionService.registerController).toHaveBeenCalledTimes(1);
    expect(cashOutSectionService.registerController).toHaveBeenCalledWith(`${cashoutConstants.controllers.REGULAR_BETS_CTRL}-${areaInput}`);

    expect(cashOutSectionService.createDataForRegularBets).toHaveBeenCalledTimes(1);
    expect(cashOutSectionService.generateBetsArray).toHaveBeenCalled();
  });

  it(`should updateBet when UPDATE_CASHOUT_BET and define detectListener`, () => {
    delete component['detectListener'];
    const callbacks = {};
    pubsub.subscribe.and.callFake((subscriber, key, fn) => {
      callbacks[key] = fn;
    });
    windowRef.nativeWindow.setInterval.and.returnValue(123);
    component.ngOnInit();

    callbacks[pubsub.API.UPDATE_CASHOUT_BET]();

    expect(cashOutSectionService.updateBet).toHaveBeenCalled();
    expect(cashOutSectionService.updateBet).toHaveBeenCalledBefore(changeDetectorRef.detectChanges);
    expect(changeDetectorRef.detectChanges).toHaveBeenCalled();

    expect(component['detectListener']).toEqual(123);
  });

  it(`should detectListener on 100ms`, () => {
    component.ngOnInit();

    expect(windowRef.nativeWindow.setInterval).toHaveBeenCalledWith(jasmine.any(Function), 100);
  });

  it('ngOnDestroy', () => {
    component.area = areaInput;
    component.ngOnInit();
    component['betTrackingEnabledSubscription'] = {
      unsubscribe: jasmine.createSpy('unsubscribe')
    } as any;
    component['getEventIdStatisticsSubscription'] = {
      unsubscribe: jasmine.createSpy('unsubscribe')
    } as any;
    component.ngOnDestroy();

    expect(component['betTrackingEnabledSubscription'].unsubscribe).toHaveBeenCalled();
    expect(component['getEventIdStatisticsSubscription'].unsubscribe).toHaveBeenCalled();

    expect(pubsub.unsubscribe).toHaveBeenCalledTimes(1);
    expect(pubsub.unsubscribe).toHaveBeenCalledWith(`${cashoutConstants.controllers.REGULAR_BETS_CTRL}-${areaInput}`);

    expect(cashOutSectionService.removeListeners).toHaveBeenCalledTimes(1);
    expect(emaService.clearAccas).toHaveBeenCalled();
  });

  it('should call needed methods in constructor', () => {
    spyOn<any>(component, 'init');
    component.ngOnChanges({ regularBets: true } as any);
    component.bets = [
      {
        eventSource: {
          event: ['654321']
        }
      }
    ] as any;
    component.ngOnInit();

    expect(component['init']).toHaveBeenCalledTimes(1);
  });

  it('should call needed methods in constructor', () => {
    spyOn<any>(component, 'init');
    component.bets = [
      {
        eventSource: {
          event: ['654321']
        }
      }
    ] as any;
    component.ngOnInit();
    component.initialized = true;
    component.ngOnChanges({ regularBets: true } as any);

    expect(component['init']).toHaveBeenCalledTimes(2);
  });

  it('should return cashout value', () => {
    let bet = { cashoutValue: '10' } as any;
    expect(component.getCashedOutValue(bet)).toEqual('10');
    bet = { cashoutValue: 'test', potentialPayout: '12' };
    expect(component.getCashedOutValue(bet)).toEqual('12');
  });

  it('isCashoutError should call isCashoutError method of cashout section service', () => {
    component['isCashoutError']({} as any);
    expect(cashOutSectionService.isCashoutError).toHaveBeenCalled();
  });

  it('getCashoutError should call getCashoutError method of cashout section service', () => {
    component['getCashoutError']({} as any);
    expect(cashOutSectionService.getCashoutError).toHaveBeenCalled();
  });
});
