import { ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { of, Subscription } from 'rxjs';
import { concatMap, distinct } from 'rxjs/operators';

import { cashoutConstants } from '../../constants/cashout.constant';
import { LocaleService } from '@core/services/locale/locale.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { IBetHistoryBet } from '@app/betHistory/models/bet-history.model';
import { CashoutSectionService } from '@app/betHistory/services/cashOutSection/cash-out-section.service';
import { RegularBet } from '@app/betHistory/betModels/regularBet/regular-bet.class';
import { ICashOutData } from '@app/betHistory/models/cashout-section.model';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { UsedFromWidgetAbstractComponent } from '@core/abstract-components/used-from-widget-abstract.component';
import { EditMyAccaService } from '../../services/editMyAcca/edit-my-acca.service';
import { BetTrackingService } from '@lazy-modules/bybHistory/services/betTracking/bet-tracking.service';
import {
  HandleScoreboardsStatsUpdatesService
} from '@lazy-modules/bybHistory/services/handleScoreboardsStatsUpdates/handle-scoreboards-stats-updates.service';

@Component({
  selector: 'regular-bets',
  templateUrl: './regular-bets.component.html'
})
export class RegularBetsComponent extends UsedFromWidgetAbstractComponent implements OnInit, OnDestroy, OnChanges {

  @Input() isBetHistoryTab: boolean;
  @Input() origin: string;
  @Input() regularBets: IBetHistoryBet[];
  @Input() isUsedFromWidget: boolean;
  @Input() area: string;

  noBetsMessage: string;
  betLocation: string = cashoutConstants.betLocation.REGULAR_BETS;
  betsMap: { [key: string ]: RegularBet };
  bets: ICashOutData[];
  initialized: boolean;
  currencySymbol: string;
  optaDisclaimer: string;
  betTrackingEnabled: boolean;

  private detectListener: number;
  private ctrlName: string;
  private betTrackingEnabledSubscription: Subscription;
  private getEventIdStatisticsSubscription: Subscription;

  constructor(
    public emaService: EditMyAccaService,
    private locale: LocaleService,
    private cashOutSectionService: CashoutSectionService,
    private windowRef: WindowRefService,
    private changeDetectorRef: ChangeDetectorRef,
    private pubsub: PubSubService,
    private betTrackingService: BetTrackingService,
    private handleScoreboardsStatsUpdatesService: HandleScoreboardsStatsUpdatesService
  ) {
    super();
  }

  ngOnInit(): void {
    this.ctrlName = `${cashoutConstants.controllers.REGULAR_BETS_CTRL}-${this.area}`;

    this.changeDetectorRef.detach();
    this.detectListener = this.windowRef.nativeWindow.setInterval(() => {
      this.changeDetectorRef.detectChanges();
      }, 100);

    this.noBetsMessage = this.isBetHistoryTab ? this.locale.getString('bethistory.noHistoryInfo')
      : this.locale.getString('bethistory.noOpenBets');

    this.init();
    this.pubsub.subscribe(this.ctrlName, this.pubsub.API.UPDATE_CASHOUT_BET, bet => {
      this.cashOutSectionService.updateBet(bet, this.bets);
      this.changeDetectorRef.detectChanges();
    });

    this.getEventIdStatisticsSubscription = this.handleScoreboardsStatsUpdatesService.getStatisticsEventIds().pipe(
      distinct()
    ).subscribe((eventId: string) => {
      this.bets.forEach((bet: ICashOutData) => {
        if (bet.eventSource.event.includes(eventId)) {
          bet.optaDisclaimerAvailable = true;
        }
      });
    });

    !this.isBetHistoryTab && this.pubsub.subscribe(this.ctrlName, this.pubsub.API.LIVE_BET_UPDATE, options => {
      options.isRegularBets = true;
      this.cashOutSectionService.removeCashoutItemWithTimeout(this.betsMap, options).subscribe(() => {
        this.updateBets();
      });
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.initialized) {
      if (changes.regularBets) {
        this.init();
      }

      if (changes.betsMap) {
        this.removeListeners();
        this.updateBets();
        this.changeDetectorRef.detectChanges();
      }
    }
  }

  ngOnDestroy(): void {
    this.pubsub.unsubscribe(this.ctrlName);
    this.removeListeners();
    this.windowRef.nativeWindow.clearInterval(this.detectListener);
    this.emaService.clearAccas();
    this.betTrackingEnabledSubscription && this.betTrackingEnabledSubscription.unsubscribe();
    this.getEventIdStatisticsSubscription && this.getEventIdStatisticsSubscription.unsubscribe();
  }

  trackByBet(index: number, bet: { eventSource: RegularBet, location: string }): string {
    return `${index}${bet.eventSource.betId}${bet.eventSource.receipt}`;
  }

  getCashedOutValue(bet) {
    return isNaN(bet.cashoutValue) ? bet.potentialPayout : bet.cashoutValue;
  }

  isCashoutError(bet: ICashOutData) {
    return this.cashOutSectionService.isCashoutError(bet.eventSource);
  }

  getCashoutError(bet: ICashOutData) {
    return this.cashOutSectionService.getCashoutError(bet.eventSource);
  }

  private removeListeners(): void {
    this.cashOutSectionService.removeListeners(this.ctrlName);
  }

  private updateBets(): void {
    this.bets = this.cashOutSectionService.generateBetsArray(this.betsMap, this.betLocation);
  }

  private init(): void {
    this.cashOutSectionService.registerController(this.ctrlName);
    const regularBetClassInstances = this.cashOutSectionService.createDataForRegularBets(this.regularBets);
    this.betsMap = (this.cashOutSectionService.generateBetsMap(regularBetClassInstances,
      this.betLocation) as { [key: string ]: RegularBet });
    const isByb = Object.values(this.betsMap).some((bet) => bet.bybType !== undefined);
    const openBetsArea = 'open-bets-page';
    if (isByb && this.area === openBetsArea) {
      this.betTrackingEnabledSubscription = this.betTrackingService.isTrackingEnabled().pipe(
        concatMap((res: boolean) => {
          this.betTrackingEnabled = res;
          return this.betTrackingEnabled ? this.betTrackingService.getStaticContent() : of(null);
        })
      ).subscribe( (content) => {
          this.optaDisclaimer = content;
      });
    }

    this.updateBets();
    this.initialized = true;
  }
}
