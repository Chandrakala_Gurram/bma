import { ProfitLossLinkComponent } from './profit-loss-link.component';

describe('VanillaProfitLossLinkComponent', () => {
  let rtsLinkService,
      productHomepagesConfig,
      coralSportsConfig,
      component: ProfitLossLinkComponent,
      link;

  beforeEach(() => {
    productHomepagesConfig = {
      portal: jasmine.createSpy()
    };

    coralSportsConfig = {
      rtsLink: jasmine.createSpy()
    };

    rtsLinkService = {
      getRtsLink: {
        coralSportsConfig,
        productHomepagesConfig,
      },
    };

    component = new ProfitLossLinkComponent(rtsLinkService);
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  it('should gnerate RTS link', () => {
    component['generateRtsLink'] = jasmine.createSpy();
    rtsLinkService['getRtsLink'] = jasmine.createSpy();
    component.ngOnInit();
    expect(component['generateRtsLink']).toHaveBeenCalled();
  });

  it('should add link to rtsLink variable', () => {
    link = 'http://test.com';
    rtsLinkService['getRtsLink'] = jasmine.createSpy('getRtsLink').and.returnValue(link);
    component.ngOnInit();
    expect(component['rtsLink']).toBe(link);
    expect(component['generateRtsLink']()).toEqual(link);
  });
});
