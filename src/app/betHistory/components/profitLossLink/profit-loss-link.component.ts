import { Component, OnInit } from '@angular/core';
import { RtsLinkService } from '@vanillaInitModule/services/rtsLink/rts-link.service';

@Component({
  selector: 'profit-loss-link',
  templateUrl: './profit-loss-link.component.html',
  styleUrls: ['./profit-loss-link.component.less']
})
export class ProfitLossLinkComponent implements OnInit {
  public rtsLink: string;

  constructor(private rtsLinkService: RtsLinkService) {}

  ngOnInit() {
    this.rtsLink = this.generateRtsLink();
  }

  private generateRtsLink(): string {
    return this.rtsLinkService.getRtsLink();
  }
}

