import { RegularBetHeaderComponent } from './regular-bet-header.component';

describe('RegularBetHeaderComponent', () => {
  let component: RegularBetHeaderComponent;
  let locale;
  let editMyAccaService;
  let timeService;

  beforeEach(() => {
    timeService = {
      formatByPattern: jasmine.createSpy('formatByPattern').and.returnValue('formatted time')
    };
    locale = {
      getString: jasmine.createSpy()
    };
    editMyAccaService = {
      EMAEnabled: true,
      isBetOpen: jasmine.createSpy('isBetOpen').and.returnValue(true),
      canEditBet: jasmine.createSpy('canEditBet').and.returnValue(true),
    };

    component = new RegularBetHeaderComponent(
      locale,
      editMyAccaService,
      timeService
    );
  });

  it('constructor', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it(`should define fullBetType`, () => {
      locale.getString.and.returnValue('localeString');
      component.bet = {
        eventSource: {
          betType: 'betType'
        }
      };

      component.ngOnInit();

      expect(component.fullBetType).toEqual('localeString');
    });

    it('should set acca time to empty string', () => {
      expect(component.accaTime).toEqual('');
      component.bet = {
        eventSource: {}
      };
      component.ngOnInit();
      expect(component.accaTime).toEqual('');
    });

    it('should set acca time', () => {
      component.bet = {
        eventSource: {
          accaHistory: {
            time: ''
          }
        }
      };
      component.ngOnInit();
      expect(component.accaTime).toEqual('formatted time');
    });
  });

  describe('getFullBetType', () => {
    it('should return bybType', () => {
      component.bet = { eventSource: { bybType: 'Build Your Bet' } };
      expect(component.getFullBetType()).toEqual(component.bet.eventSource.bybType);
    });

    it('should get string by betType code', () => {
      component.bet = { eventSource: { betType: 'SGL' } };
      component.getFullBetType();
      expect(locale.getString).toHaveBeenCalledWith(`bethistory.betTypes.${component.bet.eventSource.betType}`);
    });
  });

  it('isEMAEnabled', () => {
    expect(component.isEMAEnabled).toEqual(true);
  });

  it('showEditAccaButton', () => {
    editMyAccaService.canEditBet = () => true;
    component.betHistoryHeader = false;
    component.bet = {
      eventSource: {
        isPartialActive: false,
        isConfirmInProgress: false,
        inProgress: false,
        cashoutValue: '1'
      }
    };
    expect(component.showEditAccaButton()).toBeTruthy();
  });

  it('showEditAccaButton (canEditBet = false)', () => {
    editMyAccaService.canEditBet.and.returnValue(false);
    component.betHistoryHeader = false;
    component.bet = {
      eventSource: {
        isPartialActive: false,
        isConfirmInProgress: false,
        inProgress: false,
        cashoutValue: '1'
      }
    };
    expect(component.showEditAccaButton()).toBeFalsy();
  });

  it('showEditAccaButton when cashout value is not number', () => {
    editMyAccaService.canEditBet = () => true;
    component.betHistoryHeader = false;
    component.bet = {
      eventSource: {
        isPartialActive: false,
        isConfirmInProgress: false,
        inProgress: false,
        cashoutValue: 'fa'
      }
    };
    expect(component.showEditAccaButton()).toBeTruthy();
  });

  it('showEditAccaButton when cashout value is CASHOUT_SELN_NO_CASHOUT', () => {
    editMyAccaService.canEditBet = () => true;
    component.bet = {
      eventSource: {
        isPartialActive: false,
        isConfirmInProgress: false,
        inProgress: false,
        cashoutValue: 'CASHOUT_SELN_NO_CASHOUT'
      }
    };
    expect(component.showEditAccaButton()).toBeFalsy();
  });

  it('showEditAccaButton(partial cashout in progress)', () => {
    editMyAccaService.canEditBet = () => true;
    component.bet = {
      eventSource: {
        isPartialActive: true,
        isConfirmInProgress: true,
        inProgress: false
      }
    };
    expect(component.showEditAccaButton()).toBeTruthy();
  });

  it('showEditAccaButton(cashout in progress)', () => {
    editMyAccaService.canEditBet = () => true;
    component.bet = {
      eventSource: {
        isPartialActive: false,
        isConfirmInProgress: true,
        inProgress: true
      }
    };
    expect(component.showEditAccaButton()).toBeFalsy();
  });

  it('showEditAccaButton(bet not defined)', () => {
    editMyAccaService.canEditBet = () => true;
    component.bet = undefined;
    expect(component.showEditAccaButton()).toBeFalsy();
  });

  describe('showStatus', () => {
    it('should return false if bet is open', () => {
      component.bet = {
        eventSource: {
          totalStatus: 'open'
        }
      };
      expect(component.showStatus).toEqual(false);
    });
    it('should return true if bet is pending', () => {
      component.bet = {
        eventSource: {
          totalStatus: 'pending'
        }
      };
      expect(component.showStatus).toEqual(false);
    });
    it('should return true if bet is not pending and open', () => {
      component.bet = {
        eventSource: {
          totalStatus: 'cashed out'
        }
      };
      expect(component.showStatus).toEqual(true);
    });
  });
});
