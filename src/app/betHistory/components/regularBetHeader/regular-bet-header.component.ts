import { Component, Input, OnInit } from '@angular/core';

import { LocaleService } from '@core/services/locale/locale.service';
import { EditMyAccaService } from '@app/betHistory/services/editMyAcca/edit-my-acca.service';
import { TimeService } from '@core/services/time/time.service';

@Component({
  selector: 'regular-bet-header',
  templateUrl: './regular-bet-header.component.html',
  styleUrls: ['./regular-bet-header.component.less']
})
export class RegularBetHeaderComponent implements OnInit {
  @Input() bet: any;
  @Input() betHistoryHeader: boolean;
  @Input() gtmLocation: string;
  @Input() showArrow: boolean;

  public accaTime: string = '';

  fullBetType: string;

  constructor(
    private locale: LocaleService,
    private editMyAccaService: EditMyAccaService,
    private timeService: TimeService
) {}

  get isEMAEnabled(): boolean {
    return this.editMyAccaService.EMAEnabled;
  }

  get showStatus() {
    return !(['open', 'pending'].includes(this.bet.eventSource.totalStatus));
  }

  ngOnInit(): void {
    this.fullBetType = this.getFullBetType();
    if (this.bet.eventSource.accaHistory) {
      const date = new Date(this.bet.eventSource.accaHistory.time);
      this.accaTime = this.timeService.formatByPattern(date, 'HH:mm - dd MMM');
    }
  }

  /**
   * Return full bet type
   * @return {string}
   */
  getFullBetType(): string {
    const bet = this.bet.eventSource;
    return bet.bybType ? bet.bybType : this.locale.getString(`bethistory.betTypes.${bet.betType}`);
  }

  showEditAccaButton(): boolean {
    const bet = this.bet ? this.bet.eventSource : null;
    return (
      !this.betHistoryHeader &&
      bet &&
      this.availabilityPerCashOutValue() &&
      !bet.isCashOutedBetSuccess &&
      !(bet.isConfirmInProgress && !bet.isPartialActive) &&
      !(bet.inProgress && !bet.isPartialActive) &&
      this.editMyAccaService.canEditBet(bet) ||
      bet && bet.isAccaEdit
    );
  }

  private availabilityPerCashOutValue(): boolean {
    return (!isNaN(this.bet.eventSource.cashoutValue) &&
      Number(this.bet.eventSource.cashoutValue) > 0) ||
      (isNaN(this.bet.eventSource.cashoutValue) && this.bet.eventSource.cashoutValue !== 'CASHOUT_SELN_NO_CASHOUT');
  }
}
