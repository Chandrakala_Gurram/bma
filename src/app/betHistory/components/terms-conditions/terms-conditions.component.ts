import { Component, OnInit, ElementRef } from '@angular/core';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { ISystemConfig } from '@core/services/cms/models';
import { DomToolsService } from '@coreModule/services/domTools/dom.tools.service';

@Component({
  selector: 'terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.less']
})
export class TermsConditionsComponent implements OnInit {

  enabled: boolean = false;
  shopBetHistory: boolean = false;

  constructor(
    private cmsService: CmsService,
    private domTools: DomToolsService,
    private elementRef: ElementRef
  ) {
  }

  ngOnInit(): void {
    this.cmsService.getSystemConfig().subscribe((config: ISystemConfig) => {
      this.shopBetHistory = config.Connect && config.Connect.shopBetHistory;
      this.enabled = config.CashOut.terms && !this.domTools.closest(this.elementRef.nativeElement, '#home-betslip-tabs');
    });
  }

}
