import { TermsConditionsComponent } from './terms-conditions.component';
import { of } from 'rxjs';

describe('TermsConditionsComponent', () => {
  let component: TermsConditionsComponent;
  let cmsService;
  let domTools;
  const elementRef = {
    nativeElement: {}
  };

  beforeEach(() => {
    cmsService = {
      getSystemConfig: jasmine.createSpy().and.returnValue(of({
        CashOut: {
          terms: true
        },
        Connect: {
          shopBetHistory: true
        }
      }))
    };
    domTools = {
      closest: jasmine.createSpy()
    };

    component = new TermsConditionsComponent(
      cmsService,
      domTools,
      elementRef
    );

    component['elementRef'] = <any>elementRef;
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(cmsService.getSystemConfig).toHaveBeenCalled();
    expect(domTools.closest).toHaveBeenCalledTimes(1);
    expect(domTools.closest).toHaveBeenCalledWith(elementRef.nativeElement, '#home-betslip-tabs');
  });
});
