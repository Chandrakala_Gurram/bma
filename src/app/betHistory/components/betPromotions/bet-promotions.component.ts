import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { IBetTermsChange, IClaimedOffer } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { IBetPromotion } from '@app/betHistory/models/bet-promotion.model';
import { CmsService } from '@core/services/cms/cms.service';
import { BET_PROMO_CONFIG as bet_promos } from '@betHistoryModule/constants/bet-promotions.constant';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { CashoutBet } from '@app/betHistory/betModels/cashoutBet/cashout-bet.class';
import { RegularBet } from '@app/betHistory/betModels/regularBet/regular-bet.class';
import { PlacedBet } from '@app/betHistory/betModels/placedBet/placed-bet.class';

@Component({
  selector: 'bet-promotions',
  templateUrl: './bet-promotions.component.html',
  styleUrls: ['./bet-promotions.component.less']
})
export class BetPromotionComponent implements OnInit, OnDestroy {
  @Input() betEventSource: CashoutBet | RegularBet | PlacedBet;

  promoIcons: IBetPromotion[];
  ctrlName: string;

  constructor(
    private cmsService: CmsService,
    private pubSubService: PubSubService) { }

  ngOnInit(): void {
    this.ctrlName = 'BetPromotionComponent';
    this.updatePromos();

    this.pubSubService.subscribe(`${this.ctrlName}_${this.betEventSource.betId}`, this.pubSubService.API.BET_EVENTENTITY_UPDATED,
    () => this.updatePromos());
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe(`${this.ctrlName}_${this.betEventSource.betId}`);
  }

  private updatePromos(): void {
    this.promoIcons = [];
    let betBoosted: IBetPromotion;

    // Boosted icon
    if (this.betEventSource.betTermsChange && this.isBetBoosted()) {
      betBoosted = { ...bet_promos.find((item: IBetPromotion) => item.name === 'boosted')};
      this.promoIcons.push(betBoosted);
    }

    // Money Back icon
    this.cmsService.getToggleStatus('PromoSignposting')
      .subscribe((toggleStatus: boolean) => {
        if (toggleStatus && this.betEventSource.betType === 'SGL' && this.isMoneyBack()) {
          this.promoIcons.push(bet_promos.find((item: IBetPromotion) => item.name === 'money-back'));
        }

        if (this.promoIcons.length <= 1 && this.isAccaInsurance()) {
          this.promoIcons.push(bet_promos.find((item: IBetPromotion) => item.name === 'acca-insurance'));
        }

        // Shortened boosted label text, show two only icons
        if (this.promoIcons.length >= 2 && betBoosted) {
          betBoosted.label = 'bs.boostedMsg2';
          this.promoIcons = this.promoIcons.slice(0, 2);
        }
      });
  }

  private isBetBoosted(): boolean {
    const terms = this.betEventSource.betTermsChange;
    return terms && terms.some((term: IBetTermsChange) => term.reasonCode === 'ODDS_BOOST');
  }

  private isMoneyBack(): boolean {
    const leg = this.betEventSource.leg;
    const marketLevel = leg[0] && leg[0].eventEntity && leg[0].eventEntity.markets && leg[0].eventEntity.markets[0]
    && leg[0].eventEntity.markets[0].drilldownTagNames && leg[0].eventEntity.markets[0].drilldownTagNames.search('MKTFLAG_MB') !== -1;
    return  marketLevel;
  }

  /**
   * check if bet is ACC5 or more type
   * betEventSource.betType expl: SGL, TBL, ACC4, ACC5,...
   * @returns {boolean}
   */
  private isAccaInsurance(): boolean {
    const accaNumber = this.betEventSource.betType && this.betEventSource.betType.replace( /^\D+/g, '');
    return +accaNumber >= 5 && this.betEventSource.claimedOffers && this.betEventSource.claimedOffers.claimedOffer &&
      this.betEventSource.claimedOffers.claimedOffer
      .some((claimedOffer: IClaimedOffer) => claimedOffer.offerCategory === 'Acca Insurance' && claimedOffer.status === 'qualified');
  }
}
