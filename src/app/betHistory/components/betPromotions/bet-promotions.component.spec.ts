import { BetPromotionComponent } from '@app/betHistory/components/betPromotions/bet-promotions.component';
import { BET_PROMO_CONFIG as bet_promos } from '@betHistoryModule/constants/bet-promotions.constant';
import { of } from 'rxjs';

describe('BetPromotionComponent', () => {
  let component: BetPromotionComponent;
  let cmsService, pubSubService;
  let boost, boost2, moneyBack, accaInsurance;

  beforeEach(() => {
    cmsService = {
      getToggleStatus: jasmine.createSpy('getToggleStatus').and.returnValue(of(true))
    };

    pubSubService = {
      subscribe: jasmine.createSpy('subscribe').and.callFake((...args) => args[2]()),
      unsubscribe: jasmine.createSpy('ununsubscribe'),
      API: {
        BET_EVENTENTITY_UPDATED: 'BET_EVENTENTITY_UPDATED'
      }
    };

    component = new BetPromotionComponent(cmsService, pubSubService);
    component.betEventSource = {
      betTermsChange: [{
        reasonCode: 'ODDS_BOOST'
      }, {
        reasonCode: 'none'
      }],
      betType: 'SGL',
      leg: [{
        eventEntity: {
          drilldownTagNames: 'EVFLAG_MB'
        }
      }],
      claimedOffers: {
        claimedOffer: [{
          offerCategory: 'Acca Insurance',
          status: 'qualified'
        }, {
          offerCategory: 'Acca Insurance',
          status: 'none'
        }, {
          offerCategory: 'none'
        }]
      }
    } as any;
    boost = bet_promos[0];
    boost2 = {
      name: 'boosted',
      label: 'bs.boostedMsg2',
      svgId: 'boost-icon'
    } as any;
    moneyBack = bet_promos[1];
    accaInsurance = bet_promos[2];
  });

  it('should Init component', () => {
    component.ngOnInit();
    expect(component.ctrlName).toBe('BetPromotionComponent');
    expect(pubSubService.subscribe).toHaveBeenCalled();
  });

  it('should unsync on destroy component', () => {
    component.ngOnDestroy();
    expect(pubSubService.unsubscribe).toHaveBeenCalled();
  });

  describe('#updatePromos should create array of icons', () => {
    it('moneyBack icon', () => {
      component.betEventSource = {
        betType: 'SGL',
        leg: [{
          eventEntity: {
            markets: [{drilldownTagNames: 'MKTFLAG_MB'}]
          }
        }]
      } as any;

      component['updatePromos']();
      expect(component.promoIcons).toEqual([moneyBack]);
    });

    it('boost icon', () => {
      (cmsService.getToggleStatus as jasmine.Spy).and.returnValue(of(false));
      component.betEventSource = {
        betTermsChange: [{
          reasonCode: 'ODDS_BOOST'
        }, {
          reasonCode: 'none'
        }],
        betType: 'SGL'
      } as any;

      component['updatePromos']();
      expect(component.promoIcons).toEqual([boost]);
    });

    it('boost 2 icon', () => {
      component.betEventSource = {
        betTermsChange: [{
          reasonCode: 'ODDS_BOOST'
        }, {
          reasonCode: 'none'
        }],
        betType: 'SGL',
        leg: [{
          eventEntity: {
            markets: [{drilldownTagNames: 'MKTFLAG_MB'}]
          }
        }]
      } as any;

      component['updatePromos']();
      expect(component.promoIcons).toEqual([boost2, moneyBack]);
    });

    it('accaInsurance icon', () => {
      (cmsService.getToggleStatus as jasmine.Spy).and.returnValue(of(false));
      component.betEventSource = {
        betType: 'ACC5',
        claimedOffers: {
          claimedOffer: [{
            offerCategory: 'Acca Insurance',
            status: 'qualified'
          }, {
            offerCategory: 'Acca Insurance',
            status: 'none'
          }, {
            offerCategory: 'none'
          }]
        }
      } as any;

      component['updatePromos']();
      expect(component.promoIcons).toEqual([accaInsurance]);
    });

    it('should have no icons', () => {
      (cmsService.getToggleStatus as jasmine.Spy).and.returnValue(of(false));
      component.betEventSource = {} as any;
      component['updatePromos']();
      expect(component.promoIcons).toEqual([]);
    });
  });

  it('#isAccaInsurance with defect data', () => {
    component.betEventSource.betType = 'SGL';
    expect(component['isAccaInsurance']()).toBe(false);

    component.betEventSource.betType = 'AC15';
    component.betEventSource.claimedOffers.claimedOffer[0].offerCategory = undefined;
    expect(component['isAccaInsurance']()).toBe(false);

    component.betEventSource.claimedOffers.claimedOffer = undefined;
    expect(component['isAccaInsurance']()).toBe(undefined);
  });

  it('#isMoneyBack with defect data', () => {
    component.betEventSource.leg[0].eventEntity.drilldownTagNames = 'EVFLAG_FI';
    expect(component['isMoneyBack']()).toBe(undefined);

    component.betEventSource.leg[0].eventEntity.drilldownTagNames = undefined;
    expect(component['isMoneyBack']()).toBe(undefined);

    component.betEventSource.leg[0].eventEntity = undefined;
    expect(component['isMoneyBack']()).toBe(undefined);

    component.betEventSource.leg[0] = undefined;
    expect(component['isMoneyBack']()).toBe(undefined);
  });

  it('#isMoneyBack on market level', () => {
    component.betEventSource.leg[0].eventEntity.drilldownTagNames = undefined;
    component.betEventSource.leg[0].eventEntity.markets = [{
      drilldownTagNames: 'MKTFLAG_MB'
    }] as any;
    expect(component['isMoneyBack']()).toBe(true);

    component.betEventSource.leg[0].eventEntity.markets[0].drilldownTagNames = undefined;
    expect(component['isMoneyBack']()).toBe(undefined);

    component.betEventSource.leg[0].eventEntity.markets[0] = undefined;
    expect(component['isMoneyBack']()).toBe(undefined);

    component.betEventSource.leg[0].eventEntity.markets = undefined;
    expect(component['isMoneyBack']()).toBe(undefined);

    component.betEventSource.leg[0].eventEntity = undefined;
    expect(component['isMoneyBack']()).toBe(undefined);

    component.betEventSource.leg[0] = undefined;
    expect(component['isMoneyBack']()).toBe(undefined);
  });
});
