import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'no-bets-section',
  styleUrls: ['./no-bets-section.component.less'],
  templateUrl: './no-bets-section.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NoBetsSectionComponent {
  @Input() noBetsMessage: string;

  /**
   * Button should not be shown in betslip widget
   */
  @Input() showStartGamingButton = true;
}
