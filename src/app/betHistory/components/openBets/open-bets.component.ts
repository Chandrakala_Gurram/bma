import { Observable, of, from as observableFrom, Subscription } from 'rxjs';

import { finalize, mergeMap } from 'rxjs/operators';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import { CashoutBetsStreamService } from '@app/betHistory/services/cashoutBetsStream/cashout-bets-stream.service';
import { SessionService } from '@authModule/services/session/session.service';
import { LiveServConnectionService } from '@core/services/liveServ/live-serv-connection.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { ISwitcherConfig } from '@core/models/switcher-config.model';
import { LocaleService } from '@core/services/locale/locale.service';
import { IBetHistoryBet, IPageBets } from '@app/betHistory/models/bet-history.model';
import { MaintenanceService } from '@core/services/maintenance/maintenance.service';
import { BetHistoryMainService } from '@app/betHistory/services/betHistoryMain/bet-history-main.service';
import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { BetsLazyLoadingService } from '../../services/betsLazyLoading/bets-lazy-loading.service';
import { UserService } from '@core/services/user/user.service';
import { EditMyAccaService } from '@app/betHistory/services/editMyAcca/edit-my-acca.service';

@Component({
  selector: 'open-bets',
  templateUrl: './open-bets.component.html'
})
export class OpenBetsComponent extends AbstractOutletComponent implements OnInit, OnDestroy {
  static COMPONENT_ID: number = 0;

  @Input() area?: string;

  bets: IBetHistoryBet[];
  regularType: ISwitcherConfig;
  lottoType: ISwitcherConfig;
  poolType: ISwitcherConfig;
  loadFailed: boolean;
  isLoading: boolean;
  betTypes: ISwitcherConfig[];
  filter: string;
  errorMsg: string;
  protected readonly TYPES: string[] = ['regularType', 'lottoType', 'poolType'];
  readonly betType: string = 'open';
  private ctrlName: string;
  private betsStreamOpened: boolean = false;
  private loadDataSub: Subscription;
  private editMyAccaReload: boolean;

  constructor(
    private cashoutBetsStreamService: CashoutBetsStreamService,
    private sessionService: SessionService,
    private liveServConnectionService: LiveServConnectionService,
    private pubSubService: PubSubService,
    private localeService: LocaleService,
    private maintenanceService: MaintenanceService,
    private betHistoryMainService: BetHistoryMainService,
    private userService: UserService,
    public betsLazyLoading: BetsLazyLoadingService,
    private editMyAccaService: EditMyAccaService
  ) {
    super();
    this.changeFilter = this.changeFilter.bind(this);
  }

  ngOnInit(): void {
    this.ctrlName = `OpenBets_${OpenBetsComponent.COMPONENT_ID++}`;

    this.betTypes = this.betHistoryMainService.buildSwitchers(this.changeFilter);

    this.initTypes();

    this.filter = this.betTypes[0].viewByFilters;

    this.betsLazyLoading.reset();
    this.maintenanceService.siteServerHealthCheck().pipe(
      finalize(() => {
        this.hideSpinner();
      }))
      .subscribe(() => {
        this.loadData(this.filter);
      });

    this.pubSubService.subscribe(this.ctrlName, ['RELOAD_OPEN_BETS'], () => {
      this.reloadComponent();
    });

    this.pubSubService.subscribe(this.ctrlName, this.pubSubService.API.EDIT_MY_ACCA, () => {
      this.reloadComponent(true);
    });

    this.pubSubService.subscribe(this.ctrlName, this.pubSubService.API.RELOAD_COMPONENTS, () => {
        observableFrom(this.sessionService.whenProxySession())
          .subscribe(() => this.reload());
      }
    );
  }

  /**
   * Unsubscribe
   */
  ngOnDestroy(): void {
    this.pubSubService.publish(this.pubSubService.API.EMA_UNSAVED_IN_WIDGET, false);
    this.pubSubService.unsubscribe(this.ctrlName);
    this.betsLazyLoading.reset();

    this.closeBetsStream();

    if (this.loadDataSub) {
      this.loadDataSub.unsubscribe();
    }
  }

  get userStatus(): boolean {
    return this.userService.status;
  }

  /**
   * Set error for error template
   * @param {boolean} state
   */
  setError(state: boolean = true): void {
    this.loadFailed = state;
    if (!state) { return; }
    this.showError();
  }

  /**
   * Reloads component
   * @protected
   */
  reloadComponent(editMyAcca: boolean = false): void {
    this.editMyAccaReload = editMyAcca;
    super.reloadComponent();
    this.editMyAccaReload = false;
  }

  /**
   * Init components types
   */
  private initTypes(): void {
    this.TYPES.forEach((type: string) => this[type] = this.betHistoryMainService.getSwitcher(type));
  }

  /**
   * reload component when LS MS socket is reestablised
   * @private
   */
  private reload(): void {
    this.liveServConnectionService
      .connect()
      .subscribe(() => {
        this.reloadComponent();
      });
  }

  private changeFilter(filter: string): void {
    const openBetsArea = 'open-bets-page';

    if ((this.editMyAccaService.isUnsavedInWidget() && this.area !== openBetsArea) || this.area === openBetsArea &&
      !this.editMyAccaService.canChangeRoute()) {
      this.editMyAccaService.showEditCancelMessage();
    } else {
      if (filter !== 'digitalSportBet') {
        this.loadData(filter); }
      this.filter = filter;
    }
  }

  private addLazyLoadedBets(lazyLoadedBets: IBetHistoryBet[] = []) {
    this.bets = [...this.bets, ...lazyLoadedBets];
  }

  /**
   * If CO 2.0 enabled get bets from CO stream, otherwise get bets from BPP accountHistory
   * @returns {() => Observable<any>}
   */
  private loadBetsFromCashoutStream(): Observable<IPageBets> {
    return this.cashoutBetsStreamService.openBetsStream()
      .pipe(
        mergeMap((data: IBetHistoryBet[]) => {
          this.betsStreamOpened = true;
          return of({ bets: data });
      }));
  }

  /**
   * Close CO stream if it is opened
   */
  private closeBetsStream(): void {
    if (this.betsStreamOpened) {
      this.cashoutBetsStreamService.closeBetsStream();
      this.betsStreamOpened = false;
    }
  }

  private getOpenBets(filter: string): Observable<IPageBets> {
    if (filter === this.betTypes[0].viewByFilters) {
      return this.loadBetsFromCashoutStream();
    }
    this.closeBetsStream();

    return this.betHistoryMainService.getHistoryForYear(filter, this.betType);
  }

  private loadData(filter: string): void {
    if (this.userService.isInShopUser()) {
      this.bets = [];
      return;
    }

    this.isLoading = true;
    this.betsLazyLoading.reset();

    if (this.loadDataSub) {
      this.loadDataSub.unsubscribe();
    }

    this.loadDataSub = observableFrom(this.sessionService.whenProxySession())
      .pipe(
        mergeMap(() => this.userService.isInShopUser() ? of({ bets: [] }) : this.getOpenBets(filter)),
        mergeMap((bets: IPageBets) => this.betHistoryMainService.getEditMyAccaHistory(bets))
      ).subscribe((data: IPageBets) => {
        this.betHistoryMainService.extendCashoutBets(data.bets);
        this.bets = data.bets;
        if (!this.editMyAccaReload) {
          this.pubSubService.publish(this.pubSubService.API.BETS_COUNTER_OPEN_BETS, { data, filter: this.filter });
        }
        this.isLoading = false;
        if (!this.betsStreamOpened) {
          this.betsLazyLoading.initialize({
            initialData: data,
            addLazyLoadedBets: this.addLazyLoadedBets.bind(this),
            betType: this.betType
          });
        }
      }, () => {
        const page: string = this.localeService.getString('app.betslipTabs.openbets').toLowerCase();

      this.errorMsg = this.localeService.getString(
        'app.loginToSeePageMessage', { page }
      );
      this.showError();
    });
  }
}
