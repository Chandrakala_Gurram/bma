import { Component, OnInit, OnDestroy, ViewChild, ViewContainerRef, ComponentRef, ComponentFactory } from '@angular/core';

import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { UserService } from '@core/services/user/user.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { DYNAMIC_BET_TRACKER } from '@app/dynamicLoader/dynamic-loader-manifest';
import { DynamicComponentsService } from '@app/core/services/dynamicComponents/dynamic-components.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'in-shop-bets-page',
  templateUrl: 'in-shop-bets-page.component.html'
})
export class InShopBetsPageComponent extends AbstractOutletComponent implements OnInit, OnDestroy {
  errorMsg: string;

  @ViewChild('betTracker', {read: ViewContainerRef}) set betTracker(container: ViewContainerRef) {
    if (!container) {
      return;
    }

    const params = {
      mode: 'BET_HISTORY'
    };

    if (this.betTrackerFactory) {
      this.dynamicComponentsService
        .createDynamicComponent(this.betTrackerRef, container, this.betTrackerFactory, params);
    } else {
      this.dynamicComponentsService
        .getDynamicComponent(DYNAMIC_BET_TRACKER, this.betTrackerRef, container, this.betTrackerFactory, params);
    }
  }

  private betTrackerFactory: ComponentFactory<any>;
  private betTrackerRef: ComponentRef<any>;
  private cmpName = 'InShopBetsPageComponent';

  constructor(private pubsubService: PubSubService,
              private userService: UserService,
              private localeService: LocaleService,
              private dynamicComponentsService: DynamicComponentsService
  ) {
    super()/* istanbul ignore next */;
  }

  ngOnInit(): void {
    this.init();
    this.pubsubService.subscribe(this.cmpName, this.pubsubService.API.SESSION_LOGIN, this.init);
  }

  ngOnDestroy(): void {
    this.pubsubService.unsubscribe(this.cmpName);
  }

  private init = () => {
    if (this.userStatus) {
      this.hideSpinner();
      this.hideError();
    } else {
      const page: string = this.localeService.getString('app.betslipTabs.inShopBets').toLowerCase();
      this.errorMsg = this.localeService.getString('app.loginToSeePageMessage', { page });
      this.showError();
    }
  }

  get userStatus(): boolean {
    return this.userService.status;
  }
}
