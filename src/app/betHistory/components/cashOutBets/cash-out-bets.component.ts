import { ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { of, Subscription } from 'rxjs';
import { concatMap, distinct } from 'rxjs/operators';
import { ICashOutData } from '@app/betHistory/models/cashout-section.model';

import { cashoutConstants } from '../../constants/cashout.constant';
import { LocaleService } from '@core/services/locale/locale.service';
import { CashoutSectionService } from '@app/betHistory/services/cashOutSection/cash-out-section.service';

import { IBetHistoryBet } from '@app/betHistory/models/bet-history.model';
import { CashoutBet } from '@app/betHistory/betModels/cashoutBet/cashout-bet.class';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { UsedFromWidgetAbstractComponent } from '@core/abstract-components/used-from-widget-abstract.component';
import { EditMyAccaService } from '../../services/editMyAcca/edit-my-acca.service';
import { BetTrackingService } from '@lazy-modules/bybHistory/services/betTracking/bet-tracking.service';
import {
  HandleScoreboardsStatsUpdatesService
} from '@lazy-modules/bybHistory/services/handleScoreboardsStatsUpdates/handle-scoreboards-stats-updates.service';

@Component({
  selector: 'cash-out-bets',
  templateUrl: 'cash-out-bets.component.html'
})
export class CashOutBetsComponent extends UsedFromWidgetAbstractComponent implements OnInit, OnDestroy, OnChanges {
  @Input() data: { [key: string ]: IBetHistoryBet }; // Cashout bets array
  @Input() area: string;

  noBetsMessage: string;
  betLocation: string = cashoutConstants.betLocation.CASH_OUT_SECTION;
  bets: ICashOutData[];
  betsMap: { [key: string ]: CashoutBet };
  currencySymbol: string;
  optaDisclaimer: string;
  betTrackingEnabled: boolean;

  private ctrlName: string;
  private detectListener: number;
  private betTrackingEnabledSubscription: Subscription;
  private getEventIdStatisticsSubscription: Subscription;

  constructor(
    public emaService: EditMyAccaService,
    private locale: LocaleService,
    private cashOutSectionService: CashoutSectionService,
    private pubsub: PubSubService,
    private windowRef: WindowRefService,
    private changeDetectorRef: ChangeDetectorRef,
    private betTrackingService: BetTrackingService,
    private handleScoreboardsStatsUpdatesService: HandleScoreboardsStatsUpdatesService
  ) {
    super();
    this.noBetsMessage = this.locale.getString('bethistory.noCashoutBets');
  }

  ngOnInit(): void {
    this.changeDetectorRef.detach();
    this.detectListener = this.windowRef.nativeWindow.setInterval(() => {
      this.changeDetectorRef.detectChanges();
    }, 100);

    this.ctrlName = `${cashoutConstants.controllers.CASH_OUT_WIDGET_CTRL}-${this.area}`;

    this.cashOutSectionService.registerController(this.ctrlName);
    this.init();

    this.getEventIdStatisticsSubscription = this.handleScoreboardsStatsUpdatesService.getStatisticsEventIds().pipe(
      distinct()
    ).subscribe((eventId: string) => {
      this.bets.forEach((bet: ICashOutData) => {
        if (bet.eventSource.event.includes(eventId)) {
          bet.optaDisclaimerAvailable = true;
        }
      });
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.betsMap) {
      this.removeListeners();
      this.updateBets();
      this.changeDetectorRef.detectChanges();
    }
  }

  ngOnDestroy(): void {
    this.removeListeners();
    this.windowRef.nativeWindow.clearInterval(this.detectListener);
    this.emaService.clearAccas();
    this.betTrackingEnabledSubscription && this.betTrackingEnabledSubscription.unsubscribe();
    this.getEventIdStatisticsSubscription && this.getEventIdStatisticsSubscription.unsubscribe();
  }

  trackByBet(index: number, bet: ICashOutData): string {
    const betModel = (bet.eventSource as CashoutBet);
    return `${index}${betModel.betId}${betModel.receipt}`;
  }

  isCashoutError(bet: ICashOutData) {
    return this.cashOutSectionService.isCashoutError(bet.eventSource);
  }

  getCashoutError(bet: ICashOutData) {
    return this.cashOutSectionService.getCashoutError(bet.eventSource);
  }

  private registerEventListeners(): void {
    this.pubsub.subscribe(this.ctrlName, this.pubsub.API.LIVE_BET_UPDATE, options => {
      this.cashOutSectionService.removeCashoutItemWithTimeout(this.betsMap, options).subscribe(() => {
        this.updateBets(false);
      });
    });

    this.pubsub.subscribe(this.ctrlName, this.pubsub.API.UPDATE_CASHOUT_BET, bet => {
      this.cashOutSectionService.updateBet(bet, this.bets);
      this.changeDetectorRef.detectChanges();
    });
  }

  private removeListeners(): void {
    this.cashOutSectionService.removeListeners(this.ctrlName);
    this.pubsub.unsubscribe(this.ctrlName);
  }

  private updateBets(registerEvents: boolean = true): void {
    this.bets = this.cashOutSectionService.generateBetsArray(this.betsMap, this.betLocation);
    if (registerEvents) {
      this.registerEventListeners();
    }
  }

  private init(betsOriginalMap = null, registerEvents: boolean = true): void {
    const betsMap = betsOriginalMap || this.data;
    this.betsMap = (this.cashOutSectionService.generateBetsMap(betsMap, this.betLocation) as { [key: string ]: CashoutBet });
    const isByb = Object.values(this.betsMap).some((bet) => bet.bybType !== undefined);
    const isCashoutArea = this.area === 'cashout-page' || this.area === 'cashout-area' ;
    if (isByb && isCashoutArea) {
      this.betTrackingEnabledSubscription = this.betTrackingService.isTrackingEnabled().pipe(
        concatMap((res: boolean) => {
          this.betTrackingEnabled = res;
          return this.betTrackingEnabled ? this.betTrackingService.getStaticContent() : of(null);
        })
      ).subscribe( (content) => {
        this.optaDisclaimer = content;
      });
    }

    this.updateBets(registerEvents);
  }
}
