import { of as observableOf } from 'rxjs';
import { CashOutBetsComponent } from './cash-out-bets.component';
import { fakeAsync, tick } from '@angular/core/testing';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('CashOutBetsComponent', () => {
  let component: CashOutBetsComponent;

  let emaService,
    locale,
    cashOutSectionService,
    pubsub,
    windowRef,
    changeDetectorRef,
    betTrackingService,
    handleScoreboardsStatsUpdatesService;

  let callbackHandler,
    callbacks;
  const areaInput = 'cashout-area';
  const content = 'static block content';


  beforeEach(() => {
    locale = {
      getString: jasmine.createSpy('getString')
    };
    callbackHandler = (ctrlName: string, eventName: string, callback) => {
      if (eventName === 'LIVE_BET_UPDATE') {
        callback && callback({});
      } else if(eventName === 'UPDATE_CASHOUT_BET') {
        callback('bet');
      }
    };
    betTrackingService = {
      isTrackingEnabled: jasmine.createSpy('isTrackingEnabled').and.returnValue(observableOf(true)),
      getStaticContent: jasmine.createSpy('getStaticContent').and.returnValue(observableOf(content)),
    };
    cashOutSectionService = {
      updateBet: jasmine.createSpy('updateBet'),
      registerController: jasmine.createSpy('registerController'),
      generateBetsMap: jasmine.createSpy('generateBetsMap').and.returnValue(observableOf({ bybType: '5-A-SIDE' })),
      generateBetsArray: jasmine.createSpy('generateBetsArray').and.returnValue([
        {
          eventSource: {
            event: ['123456']
          }
        }
      ] as any),
      removeCashoutItemWithTimeout: jasmine.createSpy('removeCashoutItem').and.returnValue(observableOf({})),
      removeListeners: jasmine.createSpy(),
      isCashoutError: jasmine.createSpy('isCashoutError'),
      getCashoutError: jasmine.createSpy('getCashoutError')
    };
    pubsub = {
      subscribe: jasmine.createSpy('subscribe').and.callFake(callbackHandler),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: pubSubApi
    };

    changeDetectorRef = {
      detectChanges: jasmine.createSpy('detectChanges'),
      detach: jasmine.createSpy('detach'),
    };
    emaService = {
      clearAccas: jasmine.createSpy('emaService.clearAccas')
    };
    windowRef = {
      nativeWindow: {
        setInterval: jasmine.createSpy('setInterval'),
        clearInterval: jasmine.createSpy('clearInterval')
      }
    };
    handleScoreboardsStatsUpdatesService = {
      getStatisticsEventIds: jasmine.createSpy('getStatisticsEventIds').and.returnValue(observableOf('123456'))
    };

    component = new CashOutBetsComponent(
      emaService,
      locale,
      cashOutSectionService,
      pubsub,
      windowRef,
      changeDetectorRef,
      betTrackingService,
      handleScoreboardsStatsUpdatesService
    );

    component.area = areaInput;
    component.bets = [{
      eventSource: {
        event: '123456'
      }
    }] as any;
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      spyOn<any>(component, 'init');
    });

    it('should set ctrlName based on input "area" param', () => {
      component.ngOnInit();

      expect(component['ctrlName']).toEqual(`CashoutWidgetController-${areaInput}`);
    });

    it('should show opta disclaimer', fakeAsync(() => {
      component.ngOnInit();
      tick();

      expect(handleScoreboardsStatsUpdatesService.getStatisticsEventIds).toHaveBeenCalled();
      expect(component.bets[0].optaDisclaimerAvailable).toBeTruthy();
    }));

    it('should not show opta disclaimer', fakeAsync(() => {
      component.bets[0].eventSource.event = ['111'];
      component.ngOnInit();
      tick();

      expect(handleScoreboardsStatsUpdatesService.getStatisticsEventIds).toHaveBeenCalled();
      expect(component.bets[0].optaDisclaimerAvailable).toBeFalsy();
    }));
  });

  describe('Init', () => {
    it('should get static block if feature toggle is on', fakeAsync(() => {
      const betsMapMock = {bybType: '5-A-SIDE'};
      cashOutSectionService.generateBetsMap.and.returnValue(observableOf(betsMapMock));
      component.betTrackingEnabled = true;
      component.area = 'cashout-page';
      component['init']();
      tick();
      expect(cashOutSectionService.generateBetsMap).toHaveBeenCalled();
      expect(betTrackingService.getStaticContent).toHaveBeenCalled();
      expect(betTrackingService.isTrackingEnabled).toHaveBeenCalled();
      expect(component.betsMap).toBeTruthy();
      expect(component.optaDisclaimer).toEqual(content);
      expect(component.betTrackingEnabled).toBeTruthy();
    }));

    it('shouldnt get static block if feature toggle is off', fakeAsync(() => {
      const betsMapMock = {bybType: '5-A-SIDE'};
      cashOutSectionService.generateBetsMap.and.returnValue(observableOf(betsMapMock));
      betTrackingService.isTrackingEnabled.and.returnValue(observableOf(false));
      component.area = 'cashout-page';
      component['init']();
      tick();
      expect(cashOutSectionService.generateBetsMap).toHaveBeenCalled();
      expect(betTrackingService.getStaticContent).not.toHaveBeenCalled();
      expect(betTrackingService.isTrackingEnabled).toHaveBeenCalled();
      expect(component.betsMap).toBeTruthy();
      expect(component.optaDisclaimer).toEqual(null);
      expect(component.betTrackingEnabled).toBeFalsy();
    }));
  });

  it('should clear listeners on destroy', () => {
    component['betTrackingEnabledSubscription'] = {
      unsubscribe: jasmine.createSpy('unsubscribe')
    } as any;
    component['ctrlName'] = `CashoutWidgetController-${areaInput}`;
    component.ngOnDestroy();

    expect(cashOutSectionService.removeListeners).toHaveBeenCalledWith(`CashoutWidgetController-${areaInput}`);
    expect(pubsub.unsubscribe).toHaveBeenCalledWith(`CashoutWidgetController-${areaInput}`);
    expect(emaService.clearAccas).toHaveBeenCalled();
    expect(component['betTrackingEnabledSubscription'].unsubscribe).toHaveBeenCalled();
  });

  describe('@registerEventListeners', () => {
    beforeEach(() => {
      callbacks = {};
      spyOn<any>(component, 'updateBets');
      pubsub.subscribe.and.callFake((subscriber, key, fn) => {
        callbacks[key] = fn;
      });
    });

    it('should call removeCashoutItem and updateBets methods on event "LIVE_BET_UPDATE"', () => {
      component['registerEventListeners']();

      callbacks['LIVE_BET_UPDATE']();
      expect(cashOutSectionService.removeCashoutItemWithTimeout).toHaveBeenCalled();
      expect(component['updateBets']).toHaveBeenCalled();
    });

    it(`should updateBet`, () => {
      component['registerEventListeners']();
      callbacks[pubsub.API.UPDATE_CASHOUT_BET]();

      expect(cashOutSectionService.updateBet).toHaveBeenCalled();
      expect(cashOutSectionService.updateBet).toHaveBeenCalledBefore(changeDetectorRef.detectChanges);
      expect(changeDetectorRef.detectChanges).toHaveBeenCalled();
    });
  });

  it('isCashoutError should call isCashoutError method of cashout section service', () => {
    component['isCashoutError']({} as any);
    expect(cashOutSectionService.isCashoutError).toHaveBeenCalled();
  });

  it('getCashoutError should call getCashoutError method of cashout section service', () => {
    component['getCashoutError']({} as any);
    expect(cashOutSectionService.getCashoutError).toHaveBeenCalled();
  });

});
