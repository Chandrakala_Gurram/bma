import { Component, Input, OnInit } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'top-success-message',
  templateUrl: './top-success-message.component.html',
  styleUrls: ['./top-success-message.component.less']
})
export class TopSuccessMessageComponent implements OnInit {
  @Input() messageTranslateValue: string;
  @Input() value: string;
  @Input() currencySymbol: string;

  valueWithCurrency: string;

  constructor(private currencyPipe: CurrencyPipe) {}

  ngOnInit(): void {
    this.valueWithCurrency =  this.currencyPipe.transform(this.value, this.currencySymbol, 'code');
  }

}
