import { StakeAndReturnsHeaderComponent } from '@app/betHistory/components/stakeAndReturnsHeader/stake-and-returns-header.component';

describe('StakeAndReturnsHeaderComponent', () => {
  let component, currencyPipe;

  beforeEach(() => {
    currencyPipe = {
      transform: (v: any, s: string) => (v + s)
    };
    component = new StakeAndReturnsHeaderComponent(currencyPipe);
  });

  it('constructor', () => {
    expect(component).toBeTruthy();
  });

  it('#estimatedReturnsValue should return estimatedReturns value', () => {
    expect(component.estimatedReturns).toBe(undefined);
    expect(component.estimatedReturnsValue).toBe('N/A');
    component.estimatedReturns = 'N/A';
    expect(component.estimatedReturnsValue).toBe('N/A');
    component.estimatedReturns = '10';
    component.currencySymbol = '$';
    expect(component.estimatedReturnsValue).toBe('10$');
    component.isEdit = true;
    expect(component.estimatedReturnsValue).toBe('10$');
    component.initialReturns = true;
    expect(component.estimatedReturnsValue).toBe('N/A');
  });
});
