import { Component, Input } from '@angular/core';
import { IStakeBet } from '@app/betHistory/models/lotto.model';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'stake-and-returns-header',
  templateUrl: './stake-and-returns-header.component.html'
})
export class StakeAndReturnsHeaderComponent {
  @Input() stake: IStakeBet;
  @Input() estimatedReturns: string;
  @Input() settled: string;
  @Input() currencySymbol: string;
  @Input() stakePerLine: string;
  @Input() legType: string;
  @Input() isEdit: boolean;
  @Input() initialReturns: boolean;

  get estimatedReturnsValue(): string {
    if (!this.estimatedReturns || this.estimatedReturns === 'N/A' || (this.isEdit && this.initialReturns)) {
      return 'N/A';
    }
    return this.currencyPipe.transform(this.estimatedReturns, this.currencySymbol, 'code');
  }

  constructor(private currencyPipe: CurrencyPipe) {}
}
