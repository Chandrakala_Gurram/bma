import { BetHistoryPageComponent } from '@app/betHistory/components/betHistoryPage/bet-history-page.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';
import { of, throwError, Subject } from 'rxjs';

describe('BetHistoryPageComponent', () => {
  let component: BetHistoryPageComponent;

  let pubSubService, liveServConnectionService,
    userService, timeService, betsLazyLoadingService, localeService, datepickerValidatorService,
    maintenanceService, resolveService, sessionService, route, loginHandler, betHistoryMainService: any;

  beforeEach(() => {
    liveServConnectionService = {
      connect: jasmine.createSpy('connect').and.returnValue(of({}))
    };

    userService = {
      status: true
    };

    timeService = {
      formatByPattern: jasmine.createSpy('formatByPattern').and.returnValue('2019-04-26 00:00:00')
    } as any;

    localeService = {
      getString: jasmine.createSpy('getString').and.returnValue('Lorem')
    } as any;

    datepickerValidatorService = {
      getDefaultErrorsState: jasmine.createSpy('getDefaultErrorsState').and.returnValue(false),
      isDatePickerError: jasmine.createSpy('isDatePickerError').and.returnValue(false),
      updateErrorsState: jasmine.createSpy('updateErrorsState')
    } as any;

    pubSubService = {
      subscribe: jasmine.createSpy('subscribe').and.callFake((subscriber, method, handler) => {
        if (method === 'SESSION_LOGIN') {
          loginHandler = handler;
        }
      }),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: pubSubApi
    } as any;

    maintenanceService = {
      siteServerHealthCheck: jasmine.createSpy('siteServerHealthCheck').and.returnValue(of({}))
    } as any;

    resolveService = {
      reset: jasmine.createSpy('reset'),
      set: jasmine.createSpy('set').and.returnValue(of({})),
      get: jasmine.createSpy('get').and.returnValue({})
    };

    sessionService = {
      whenProxySession: jasmine.createSpy('whenProxySession').and.returnValue(of({}))
    };

    route = {
      snapshot: {
        params: {}
      }
    };

    betHistoryMainService = {
      makeSafeCall: jasmine.createSpy('makeSafeCall').and.callFake(x => x),
      getSummaryTotals: jasmine.createSpy('getSummaryTotals').and.returnValue({}),
      getHistoryForTimePeriod: jasmine.createSpy('getHistoryForTimePeriod').and.returnValue(of([]))
    };

    betsLazyLoadingService = {
      reset: jasmine.createSpy('reset'),
      initialize: jasmine.createSpy('initialize')
    };

    component = new BetHistoryPageComponent(
      betHistoryMainService,
      timeService,
      pubSubService,
      sessionService,
      liveServConnectionService,
      datepickerValidatorService,
      localeService,
      resolveService,
      maintenanceService,
      route,
      userService,
      betsLazyLoadingService
    );

    component.dateObject = {
      startDate: '',
      endDate: ''
    } as any;
  });

  it('#ngOnInit should init the component', () => {
    component.ngOnInit();
    expect(localeService.getString).toHaveBeenCalledWith('app.loginToSeePageMessage', {
      page: 'lorem'
    });
    expect(component.errorMsg).toBe('Lorem');
    expect(component.contactUsMsg).toBe('Lorem');
    expect(component.regularType).toBeTruthy();
    expect(component.betTypes.length).toBe(3);
    expect(datepickerValidatorService.getDefaultErrorsState).toHaveBeenCalled();

    maintenanceService.siteServerHealthCheck().subscribe(() => {
      expect(component.isLoading).toBe(false);
      expect(resolveService.set).toHaveBeenCalled();
    });
  });

  it('#loginHandler', () => {
    component['assignListeners']();
    loginHandler();
    expect(localeService.getString).toHaveBeenCalled();
  });

  it('#memorizeSummaryState should set isExpanded state for summary', () => {
    component.memorizeSummaryState(true);
    expect(component.isExpandedSummary).toBe(true);
  });

  it('should test initial time range - 6 days', () => {
    timeService.oneDayInMiliseconds = 86400000;
    component['dateInit']();
    const startTime = new Date(component['startDate'].value).getTime();
    const endTime = new Date(component['endDate'].value).getTime();
    const daysCount = ((endTime - startTime) / timeService.oneDayInMiliseconds);
    expect(Math.floor(daysCount)).toEqual(6);
  });

  it('#createFilters should create data for switchers on the page', () => {
    component['changeFilter'] = jasmine.createSpy().and.returnValue('changefilter');
    component['createFilters']();

    expect(component.regularType.viewByFilters).toBe('bet');
    expect(component.regularType.onClick('bet')).toEqual('changefilter');
    expect(component.lottoType.name).toBe('Lorem');
    expect(component.lottoType.onClick('bet')).toEqual('changefilter');
    expect(component.poolType.refs).toBe('pool');
    expect(component.poolType.onClick('bet')).toEqual('changefilter');
    expect(component.betTypes.length).toBe(3);
    expect(component.filter).toBe('bet');
    expect(component.summarySelected).toBe('sb');
  });

  describe('#changeFilter', () => {
    beforeEach(() => {
      component.betTypes = [{
        viewByFilters: 'bet',
        refs: 'sb'
      }, {
        viewByFilters: 'digitalSportBet'
      }] as any;
    });

    it('if digitalSportBet', () => {
      component['changeFilter']('digitalSportBet');

      expect(component.filter).toBe('digitalSportBet');
      expect(component.showDatepicker).toBe(false);
    });

    it('if not digitalSportBet but dateError', () => {
      (datepickerValidatorService.isDatePickerError as jasmine.Spy).and.returnValue(true);
      component['changeFilter']('bet');

      expect(component.filter).toBe('bet');
      expect(component.showDatepicker).toBe(true);
    });

    it('if not digitalSportBet and not dateError', () => {
      component.startDate = { value: '' } as any;
      component.endDate = { value: '' } as any;
      component['changeFilter']('bet');

      expect(betsLazyLoadingService.reset).toHaveBeenCalled();
      expect(component.summarySelected).toBe('sb');
      expect(component.filter).toBe('bet');
      expect(component.showDatepicker).toBe(true);
    });
  });

  describe('processDateRangeData function', () => {
    let errorObject;

    beforeEach(() => {
      const startDate = new Date();
      const endDate = new Date();
      errorObject = {
        startDateInFuture: false,
        endDateInFuture: false,
        moreThanThreeMonth: false,
        moreThanThreeMonthRange: false,
        endDateLessStartDate: false,
        isValidstartDate: true,
        isValidendDate: true
      };

      component.startDate = { value: startDate };
      component.endDate = { value: endDate };
      component['loadData'] = jasmine.createSpy('loadData');
    });

    it('should call updateErrorsState with params', () => {
      component.processDateRangeData(errorObject);

      expect(datepickerValidatorService.updateErrorsState).toHaveBeenCalledWith(
        undefined,
        errorObject,
        component.startDate,
        component.endDate);
      expect(component['loadData']).toHaveBeenCalled();
    });

    it('if isDatePickerError', () => {
      component.isDatePickerError = jasmine.createSpy().and.returnValue(true);
      component.processDateRangeData(errorObject);
      expect(component['loadData']).not.toHaveBeenCalled();
    });
  });

  describe('reloadComponent', () => {
    beforeEach(() => {
      spyOn(component as any, 'getDateObject').and.returnValue({ startDate: '', endDate: '' });
    });

    it(`should call through ngDestroy, ngOnInit`, () => {
      component['reloadComponent']();

      expect(component.loadFailed).toBe(false);
      expect(betsLazyLoadingService.reset).toHaveBeenCalled();
      expect(component.state.loading).toBe(false);
      expect(maintenanceService.siteServerHealthCheck).toHaveBeenCalled();
    });
  });

  describe('reload', () => {
    it('should connect to LS and call reloadSegment method', () => {
      const connection = new Subject();
      spyOn(component as any, 'reloadComponent');
      liveServConnectionService.connect.and.returnValue(connection);
      component.reload();
      expect(liveServConnectionService.connect).toHaveBeenCalled();
      expect(component['reloadComponent']).not.toHaveBeenCalled();
      connection.next({});
      expect(component['reloadComponent']).toHaveBeenCalled();
    });
  });

  describe('loadData', () => {
    beforeEach(() => {
      component['getDateObject'] = jasmine.createSpy('getDateObject');
      component['addLazyLoadedBets'] = jasmine.createSpy('addLazyLoadedBets');
      component['hideSpinner'] = jasmine.createSpy('hideSpinner');
      component['filterBetHistory'] = jasmine.createSpy('filterBetHistory');
      component.setError = jasmine.createSpy('setErr');
    });

    it('should load data, succesfull betsLazyLoading initialize', () => {
      (resolveService.set as jasmine.Spy).and.callFake(() => of({}));
      component['loadData']();
      expect(component.isLoading).toBe(false);
      expect(betsLazyLoadingService.reset).toHaveBeenCalled();
      expect(betsLazyLoadingService.initialize).toHaveBeenCalled();
      expect(betHistoryMainService.makeSafeCall).toHaveBeenCalled();
      expect(sessionService.whenProxySession).toHaveBeenCalled();
      expect(component['hideSpinner']).toHaveBeenCalled();
      expect(component['filterBetHistory']).toHaveBeenCalled();
      expect(component.setError).not.toHaveBeenCalled();
    });

    it('betsLazyLoading initialize error', () => {
      (resolveService.set as jasmine.Spy).and.returnValue(throwError(''));
      component['loadData']();
      expect(component.setError).toHaveBeenCalledWith(true);
    });

  });

  it('#getSelectedSwitcher should get selected tab', () => {
    component.betTypes = [{
      viewByFilters: 'bet',
      refs: 'sb'
    }, {
      viewByFilters: 'digitalSportBet'
    }] as any;

    expect(component['getSelectedSwitcher']('bet')).toEqual({
      viewByFilters: 'bet',
      refs: 'sb'
    } as any);
  });

  it('#getSummarySwitcherRefs should get refs props from array of objects', () => {
    component.betTypes = [{
      refs: 'sb'
    }, {
      refs: 'lotto'
    }] as any;

    expect(component['getSummarySwitcherRefs']()).toEqual(['sb', 'lotto'] as any);
  });

  it('#assignListeners', () => {
    (pubSubService.subscribe as jasmine.Spy).and.callFake((...args) => args[2]());
    component.ngOnInit = jasmine.createSpy('ngOnInit');  // to prevent circular calls
    component['ctrlName'] = 'BetHistoryComponent';
    component.startDate = { value: '' } as any;
    component.endDate = { value: '' } as any;

    component['assignListeners']();

    expect(pubSubService.subscribe).toHaveBeenCalledWith('BetHistoryComponent',
      pubSubService.API.RELOAD_COMPONENTS, jasmine.any(Function));
    expect(sessionService.whenProxySession).toHaveBeenCalled();
    expect(datepickerValidatorService.updateErrorsState).toHaveBeenCalled();
  });

  describe('#addLazyLoadedBets', () => {
    it('should set bets to the component with parameters', () => {
      component.bets = [{ id: 1 }] as any;
      const lazyLoadedBets = [{ id: 2 }] as any;
      component['filterBetHistory'] = jasmine.createSpy().and.returnValue(lazyLoadedBets);
      component['addLazyLoadedBets'](lazyLoadedBets);
      expect(component['filterBetHistory']).toHaveBeenCalledWith(lazyLoadedBets);
      expect(component.bets.length).toBe(2);
    });
    it('should set bets to the component without parameters', () => {
      const bet = { id: 1 };
      component.bets = [bet] as any;
      component['filterBetHistory'] = jasmine.createSpy();
      component['addLazyLoadedBets']();
      expect(component['filterBetHistory']).toHaveBeenCalledWith([]);
      expect(component.bets.length).toBe(2);
    });
  });

  it('#userStatus should return userService.status', () => {
    expect(component.userStatus).toEqual(true);
  });

  it('#isBetsTab should return filter !== digitalSportBet value', () => {
    component.filter = '';
    expect(component.isBetsTab).toEqual(true);
  });

  it('#setError should call showError, false case', () => {
    component.showError = jasmine.createSpy('showError');
    component.setError(false);
    expect(component.showError).not.toHaveBeenCalled();
  });

  it('#setError should call showError, true case', () => {
    component.loadFailed = false;
    component.showError = jasmine.createSpy('showError');
    component.setError();
    expect(component.loadFailed).toEqual(true);
    expect(component.showError).toHaveBeenCalled();
  });

  it('filterBetHistory, negative case', () => {
    component.filter = 'notbet';
    component['betHistoryMainService'].getBetStatus = jasmine.createSpy().and.returnValue(false);
    expect(component['filterBetHistory']([])).toEqual([]);
  });

  it('filterBetHistory, positive case', () => {
    component.filter = 'bet';
    component['betHistoryMainService'].getBetStatus = jasmine.createSpy().and.returnValue(component['filteredBetsStatus']);
    component['filterBetHistory'](['bet1', 'bet2']);
    expect(component['betHistoryMainService'].getBetStatus).toHaveBeenCalledTimes(2);
  });
});
