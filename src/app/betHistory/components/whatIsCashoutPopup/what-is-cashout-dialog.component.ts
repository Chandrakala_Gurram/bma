import { Component, ViewChild } from '@angular/core';

import { DeviceService } from '@core/services/device/device.service';
import { AbstractDialog } from '@shared/components/oxygenDialogs/abstract-dialog';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

@Component({
  selector: 'what-is-cashout-dialog',
  templateUrl: './what-is-cashout-dialog.component.html'
})
export class WhatIsCashOutDialogComponent extends AbstractDialog {

  @ViewChild('whatIsCashOutDialog') dialog;

  constructor(
    device: DeviceService, windowRef: WindowRefService
  ) {
    super(device, windowRef);
  }
}
