import { CashoutBet } from '../betModels/cashoutBet/cashout-bet.class';
import { PlacedBet } from '../betModels/placedBet/placed-bet.class';
import { RegularBet } from '../betModels/regularBet/regular-bet.class';

export interface ICashOutData {
  eventSource: CashoutBet | RegularBet | PlacedBet;
  location: string;
  optaDisclaimerAvailable?: boolean;
}
