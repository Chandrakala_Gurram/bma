export interface IDateRangeObject {
  startDate: string;
  endDate?: string;
}
