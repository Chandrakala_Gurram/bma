export interface ILottoModel {
  id: string;
  name: string;
  balls: IBall[];
  drawName: string;
  date: string | number | Date;
  betDate: string | number | Date;
  betReceiptId: string;
  stake: string | IStakeBet;
  currency: string;
  status: string;
  settled: string;
  totalReturns: string;
}

export interface ILottoBet extends ILottoModel {
  betType: IBetType;
  id: string;
  lotteryDraw: ILotteryDraw;
  lotteryName: string;
  lotterySub: ILotterySub;
  numLines: string;
  numSelns: string;
  refund: IValue;
  settled: string;
  settledAt: string;
  source: string;
  subId: string;
  winnings: IValue;
}

export interface IBall {
  ballName: string;
  ballNo: string;
}

interface IBetType {
  code: string;
  name: string;
}


interface ILotteryDraw {
  drawAt: string;
  pick: IBall[];
  xgameId: string;
}

interface ILotterySub {
  date: string | number | Date;
  numSubs: string;
  outstandingSubs: string;
  stakePerBet: string;
  subId: string;
  subReceipt: string;
}

export interface IStakeBet {
  currency: string;
  stakePerLine: string;
  tokenValue: string;
  value: string;
}

interface IValue {
  value: string;
}
