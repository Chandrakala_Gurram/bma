import { ICashOutBet } from '@app/betHistory/models/bet-history-cash-out.model';
import { IBetDetail } from '@app/bpp/services/bppProviders/bpp-providers.model';

export interface ICashoutSocketError {
  error?: {
    code: string;
  };
}

export interface ICashoutSocketBets extends ICashoutSocketError {
  bets: IBetDetail[];
}

export interface ICashoutSocketBetUpdate extends ICashoutSocketError {
  bet: ICashOutBet;
}

export interface ISocketCashoutUpdateMessage extends ICashoutSocketError {
  cashoutData: ISocketCashoutValueUpdate;
}

export interface ISocketCashoutValueUpdate {
  betId: string;
  cashoutValue: string;
  cashoutStatus: string;
  shouldActivate?: boolean;
}
