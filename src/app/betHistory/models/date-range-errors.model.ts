export interface IDateRangeErrors {
  startDateInFuture: boolean;
  endDateInFuture: boolean;
  moreThanThreeMonth: boolean;
  moreThanThreeMonthRange: boolean;
  endDateLessStartDate: boolean;
  isValidstartDate: boolean;
  isValidendDate: boolean;
}


