export interface ICashoutMapItem {
  id: string;
  isSettled: boolean;
}
