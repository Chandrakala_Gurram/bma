import { PlacedBet } from '../placedBet/placed-bet.class';

export class CashoutBet extends PlacedBet {
  stake?: string;
  stakePerLine?: string;
  gtmCashoutValue?: number | string;
  bybType?: any;

  constructor(bet, betModelService, currency, currencySymbol, cashOutMapIndex, cashOutErrorMessage) {
    super(bet, betModelService, currency, currencySymbol, cashOutMapIndex, cashOutErrorMessage);

    this.setCashoutProperties(bet);
  }
}
