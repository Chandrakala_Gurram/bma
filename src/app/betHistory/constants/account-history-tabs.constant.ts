const LOCALE_CATEGORY = 'bethistory.accountHistoryTabs';

export const ACCOUNT_HISTORY_TABS = [
  { locale: `${LOCALE_CATEGORY}.settledBets`, url: '/account-history', iconId: '#icon-bet-history' },
  { locale: `${LOCALE_CATEGORY}.transactions`, url: '/transaction-history', iconId: '#icon-transactions-history' },
  { locale: `${LOCALE_CATEGORY}.gaming`, url: '/gaming-history', iconId: '#icon-gaming-history' }
];
