// TODO must have '#' in defined ids to match convention!
export const BET_PROMO_CONFIG = [{
  name: 'boosted',
  label: 'bs.boostedMsg',
  svgId: 'boost-icon'
}, {
  name: 'money-back',
  label: 'bs.moneyBackMsg',
  svgId: 'money-back'
}, {
  name: 'acca-insurance',
  label: 'bs.accaInsurance',
  svgId: 'icon-acca-insurance'
}];
