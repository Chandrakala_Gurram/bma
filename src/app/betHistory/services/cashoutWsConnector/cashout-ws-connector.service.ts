import { Injectable } from '@angular/core';
import { from as observableFrom, Observable, Subject, throwError } from 'rxjs';

import { BetHistoryApiModule } from '@app/betHistory/bet-history-api.module';
import { UserService } from '@core/services/user/user.service';
import { CashOutLiveServeUpdatesService } from '@app/betHistory/services/cashOutLiveServeUpdatesService/cashOutLiveServeUpdatesService';
import { NewRelicService } from '@core/services/newRelic/new-relic.service';
import { IBetDetail } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { CommandService } from '@core/services/communication/command/command.service';
import { WsConnectorService } from '@core/services/wsConnector/ws-connector.service';
import { WsConnector } from '@core/services/wsConnector/ws-connector';
import * as _ from 'underscore';
import environment from '@environment/oxygenEnvConfig';
import { IConstant } from '@core/services/models/constant.model';
import { catchError, map, takeUntil } from 'rxjs/operators';
import {
  ISocketCashoutUpdateMessage,
  ICashoutSocketBets,
  ICashoutSocketBetUpdate
} from '@app/betHistory/models/cashout-socket.model';
import { ICashOutBet } from '@app/betHistory/models/bet-history-cash-out.model';
import { ICashoutBet } from '@app/betHistory/models/bet-history-bet.model';

@Injectable({ providedIn: BetHistoryApiModule })
export class CashoutWsConnectorService {
  private readonly moduleName = 'cashout';
  private readonly CASHOUT_MS_ENDPOINT: string = environment.CASHOUT_MS;

  private connection: WsConnector;

  private eventHandlers = new Map<string, Function>();
  private connectionStateHandlers = new Map<string, Function>();

  private betDetails$ = new Subject<IBetDetail[]>();
  private disconnected$ = new Subject();

  private subscribers: number = 0;

  private unauthorised = false;
  private unknownErrorReconnectAttempts = 3;

  constructor(private wsConnectorService: WsConnectorService,
              private user: UserService,
              private cashOutLiveServeUpdatesService: CashOutLiveServeUpdatesService,
              private commandService: CommandService,
              private newRelicService: NewRelicService,
  ) {}

  /**
   * Opens cashout bets events stream and updates subscriptions amount
   * @returns {Observable}
   */
  streamBetDetails(): Observable<IBetDetail[]> {
    this.subscribers++;
    this.reconnect();
    return this.betDetails$.asObservable();
  }

  updateBet(bet: ICashoutBet): void {
    this.connection.emit('updateBet', { betId: bet.betId, updateType: 'cashedOut' });
  }

  /**
   * Removes subscriber from the listeners of cashout web socket stream and closes connection if needed.
   */
  closeStream() {
    if (this.subscribers-- === 1) {
      this.closeConnection();
    }
  }

  /**
   * Creates connection with cashout socket for initial bets, bet and cashout value live updates.
   */
  private createConnection(): WsConnector {
    const params = {
      path: '/socket.io',
      timeout: 10000,
      reconnectionAttempts: 10,
      reconnectionDelay: 1000
    };

    this.connection = this.wsConnectorService.create(
      `${this.CASHOUT_MS_ENDPOINT}:8443/?token=${this.user.bppToken}`,
      params,
      this.moduleName
    );

    this.newRelicService.addPageAction('cashoutUpdates->createConnection', {
      bppToken: `${this.user.bppToken}`.substr(0, 7),
    });

    return this.connection;
  }

  private createConnectionAndInitHandlers() {
    this.createConnection();
    this.initEventHandlers();
    this.initStateHandlers();
  }

  /**
   * Closes connection with cashout socket
   */
  private closeConnection(): void {
    if (this.connection) {
      this.connection.disconnect();
      this.connection = null;
      this.disconnected$.next(null);
      this.disconnected$.complete();
      this.disconnected$ = new Subject();
    }
  }

  /**
   * Re-creates new connection with cashout web socket
   */
  private reconnect(): WsConnector {
    this.closeConnection();
    this.createConnectionAndInitHandlers();
    this.connection.connect();
    return this.connection;
  }

  private initEventHandlers() {
    this.eventHandlers.set('initial', this.initialEventHandler.bind(this));
    this.eventHandlers.set('betUpdate', this.betUpdateEventHandler.bind(this));
    this.eventHandlers.set('cashoutUpdate', this.cashoutUpdateEventHandler.bind(this));

    this.connection.addAnyMessagesHandler(this.handleMessage.bind(this));
  }

  private initStateHandlers() {
    this.connectionStateHandlers.set('connect', () => {
      this.trackConnectionState('connected');
    });

    if (this.connection) {
      this.connection.state$
        .pipe(takeUntil(this.disconnected$))
        .subscribe(this.handleConnectionStateChange.bind(this));
    }
  }

  private handleConnectionStateChange(state: string) {
    const handler = this.connectionStateHandlers.get(state);
    if (typeof handler !== 'function') {
      console.error('Unknown socket error');
      return;
    }

    handler();
  }

  private handleMessage(messageName: string, body: any) {
    const handler = this.eventHandlers.get(messageName);
    if (typeof handler !== 'function') {
      console.error('Unknown socket message: ', messageName);
      this.trackUpdatesError('unknownSocketMessage', { messageName, body });
      return;
    }

    handler(body);
  }

  private initialEventHandler(update: ICashoutSocketBets) {
    if (update && _.isArray(update.bets)) {
      this.betDetails$.next(update.bets);
      this.betDetails$.complete();
      this.betDetails$ = new Subject<IBetDetail[]>();
    } else {
      this.trackUpdatesError('initial', update);

      if (this.isUnathorisedError(update)) {
        // Reconnect to BPP and create new connection with updated BPP token
        this.handleUnauthorisedError()
          .subscribe(() => {
            this.unauthorised = false;
          }, () => {
            this.throwBetDetailsError();
          });
      } else if (update.error.code === 'UNKNOWN_SERVICE_ERROR' && this.unknownErrorReconnectAttempts) {
        this.unknownErrorReconnectAttempts--;
        this.reconnect();
      } else {
        this.closeConnection();
        this.throwBetDetailsError();
      }
    }
  }

  private betUpdateEventHandler(update: ICashoutSocketBetUpdate) {
    if (update && update.bet) {
      this.cashOutLiveServeUpdatesService.updateBetDetails(update.bet, Date.now());
    } else {
      this.trackUpdatesError('betUpdate', update);

      // Reconnect to BPP and create new connection with updated BPP token
      if (this.isUnathorisedError(update)) {
        this.handleUnauthorisedError()
          .subscribe(() => {
            this.unauthorised = false;

            // replace "initial" message handler
            this.eventHandlers.set('initial', this.handleBetsUpdate);
          });
      }
    }
  }

  private handleBetsUpdate(update: ICashoutSocketBets): void {
    if (update && _.isArray(update.bets)) {
      _.each(update.bets, (bet: IBetDetail) => {
        this.cashOutLiveServeUpdatesService.updateBetDetails(bet as ICashOutBet, Date.now());
      });
    } else {
      this.trackUpdatesError('betUpdateReconnect', update);
    }
  }

  private cashoutUpdateEventHandler(update: ISocketCashoutUpdateMessage) {
    if (update && update.cashoutData) {
      this.cashOutLiveServeUpdatesService.applyCashoutValueUpdate(update.cashoutData);
    } else {
      this.trackUpdatesError('cashoutUpdate', update);
    }
  }

  private trackUpdatesError(channel: string, errorData: IConstant): void {
    this.newRelicService.addPageAction(`cashoutUpdates->error->${channel}`, {
      data: errorData
    });
  }

  private trackConnectionState(state: string, data?: any) {
    this.newRelicService.addPageAction(`cashoutConnection->${state}`, { data });
  }

  private isUnathorisedError(update: ICashoutSocketBets | ICashoutSocketBetUpdate): boolean {
    return !this.unauthorised && _.has(update, 'error') && update.error.code === 'UNAUTHORIZED_ACCESS';
  }

  private handleUnauthorisedError(): Observable<void> {
    this.unauthorised = true;

    return observableFrom(this.commandService.executeAsync(this.commandService.API.BPP_AUTH_SEQUENCE)).pipe(
      map(() => {
        this.reconnect();
      }),
      catchError((error: string) => {
        this.trackUpdatesError('bppReconnect', { error });
        this.closeConnection();
        return throwError(error);
      })
    );
  }

  private throwBetDetailsError() {
    this.betDetails$.error([]);
    this.betDetails$.complete();
    this.betDetails$ = new Subject<IBetDetail[]>();
  }
}
