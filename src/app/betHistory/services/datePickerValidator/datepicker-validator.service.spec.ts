import { DatepickerValidatorService } from '@app/betHistory/services/datePickerValidator/datepicker-validator.service';

describe('test DatepickerValidatorService', () => {
  let timeService;
  let service: DatepickerValidatorService;
  let errorState;

  beforeEach(() => {
    errorState = {
      startDateInFuture: false,
      endDateInFuture: false,
      moreThanThreeMonth: false,
      moreThanThreeMonthRange: false,
      endDateLessStartDate: false,
      isValidstartDate: true,
      isValidendDate: true
    };

    timeService = {
      moreThanThreeMonth: jasmine.createSpy(),
      getFullMonthRange: jasmine.createSpy(),
    } as any;

    service = new DatepickerValidatorService(timeService);
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
  });

  it('#getDefaultErrorsState should return default error Object', () => {
    expect(service.getDefaultErrorsState()).toEqual(errorState);
  });

  it('#isDateRangeError should update values in error Object', () => {
    const datePickError = errorState;
    datePickError.isValidstartDate = false;
    service.updateErrorsState(errorState, datePickError, { value: new Date }, { value: new Date });
    expect(errorState).toEqual(jasmine.objectContaining({
      isValidstartDate: false
    }));
  });
  describe('isDateRangeError', () => {
    describe('should return True if', () => {
      beforeEach(() => {
        expect(service['isDateRangeError'](errorState)).toBeFalsy();
      });

      it(`startDateInFuture error`, () => {
        errorState.startDateInFuture = true;
      });

      it(`moreThanThreeMonthRange error`, () => {
        errorState.moreThanThreeMonthRange = true;
      });

      it(`endDateLessStartDate error`, () => {
        errorState.endDateLessStartDate = true;
      });

      it(`moreThanThreeMonth error`, () => {
        errorState.moreThanThreeMonth = true;
      });

      afterEach(() => {
        expect(service['isDateRangeError'](errorState)).toBeTruthy();
      });
    });
  });

  it('#isDateRangeError should check input dates range condition', () => {
    expect(service['isDateRangeError'](errorState)).toBeFalsy();
    errorState.startDateInFuture = true;
    expect(service['isDateRangeError'](errorState)).toBeTruthy();
  });

  it('#isValidDatesEntered should check input dates for valid format', () => {
    expect(service['isValidDatesEntered'](errorState)).toBeTruthy();
    errorState.isValidstartDate = false;
    expect(service['isValidDatesEntered'](errorState)).toBeFalsy();

  });

  describe('addCustomErrors', () => {
    const date = new Date;

    it('#addCustomErrors clarify properties for errors object', () => {
      errorState.endDateLessStartDate = true;
      service['addCustomErrors'](errorState, { value: date }, { value: date });
      expect(errorState).toEqual(jasmine.objectContaining({
        endDateLessStartDate: false
      }));
      expect(timeService.moreThanThreeMonth).toHaveBeenCalledWith(date);
    });

    it(`moreThanThreeMonthRange should equal true if  moreThanThreeMonth equal true`, () => {
      timeService.moreThanThreeMonth.and.returnValue(true);

      service['addCustomErrors'](errorState, { value: date }, { value: date });

      expect(errorState.moreThanThreeMonthRange).toBeTruthy();
      expect(timeService.getFullMonthRange).not.toHaveBeenCalled();
    });

    describe('moreThanThreeMonthRange should equal True if getFullMonthRange return value >= 3', () => {
      beforeEach(() => {
        timeService.moreThanThreeMonth.and.returnValue(false);
      });

      it(`getFullMonthRange return`, () => {
        timeService.getFullMonthRange.and.returnValue(3);
      });

      it(`getFullMonthRange return`, () => {
        timeService.getFullMonthRange.and.returnValue(10);
      });

      afterEach(() => {
        service['addCustomErrors'](errorState, { value: date }, { value: date });

        expect(errorState.moreThanThreeMonthRange).toBeTruthy();
        expect(timeService.getFullMonthRange).toHaveBeenCalled();
      });
    });

    it(`moreThanThreeMonthRange should equal True if getFullMonthRange return value < 3`, () => {
      timeService.moreThanThreeMonth.and.returnValue(false);
      timeService.getFullMonthRange.and.returnValue(0);

      service['addCustomErrors'](errorState, { value: date }, { value: date });

      expect(errorState.moreThanThreeMonthRange).toBeFalsy();
      expect(timeService.getFullMonthRange).toHaveBeenCalled();
    });
  });
});
