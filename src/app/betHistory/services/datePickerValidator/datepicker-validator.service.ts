import { Injectable } from '@angular/core';
import * as _ from 'underscore';

import { BetHistoryApiModule } from '@betHistoryModule/bet-history-api.module';
import { TimeService } from '@core/services/time/time.service';
import { IDateRangeErrors } from '../../models/date-range-errors.model';
import { IDatePickerDate } from '../../models/date-picker-date.model';

/**
 * Service which provide fetching event functionality
 */
@Injectable({ providedIn: BetHistoryApiModule })
export class DatepickerValidatorService {

  constructor(private timeService: TimeService) {
  }

  getDefaultErrorsState(): IDateRangeErrors {
    return {
      startDateInFuture: false,
      endDateInFuture: false,
      moreThanThreeMonth: false,
      moreThanThreeMonthRange: false,
      endDateLessStartDate: false,
      isValidstartDate: true,
      isValidendDate: true
    };
  }

  updateErrorsState(errorsState: IDateRangeErrors, datePickerErrors: IDateRangeErrors,
                    startDate: IDatePickerDate, endDate: IDatePickerDate) {
    _.extend(errorsState, datePickerErrors);
    this.addCustomErrors(errorsState, startDate, endDate);
  }

  /** Check if DatePicker error
   * @return {boolean}
   */
  isDatePickerError(errorsState: IDateRangeErrors): boolean {
    return this.isDateRangeError(errorsState) || !this.isValidDatesEntered(errorsState);
  }

  /**
   * Check if date range error
   * @return {boolean}
   */
  private isDateRangeError(errorsState: IDateRangeErrors): boolean {
    return errorsState.startDateInFuture
      || errorsState.moreThanThreeMonthRange
      || errorsState.endDateLessStartDate
      || errorsState.moreThanThreeMonth;
  }

  private isValidDatesEntered(errorsState: IDateRangeErrors): boolean {
    return errorsState.isValidendDate && errorsState.isValidstartDate;
  }

  private addCustomErrors(errorsState: IDateRangeErrors, startDate, endDate): void {
    if (!startDate || !endDate) {
      return;
    }
    errorsState.endDateLessStartDate = startDate.value > endDate.value;
    errorsState.moreThanThreeMonth = this.timeService.moreThanThreeMonth(startDate.value);

    errorsState.moreThanThreeMonthRange = errorsState.moreThanThreeMonth
      ? true
      : this.timeService.getFullMonthRange(startDate.value, endDate.value) >= 3;
  }
}
