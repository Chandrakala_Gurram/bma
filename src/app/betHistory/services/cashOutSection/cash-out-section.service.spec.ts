import { fakeAsync, tick } from '@angular/core/testing';

import { CashoutSectionService } from '@app/betHistory/services/cashOutSection/cash-out-section.service';
import { CashoutBet } from '@app/betHistory/betModels/cashoutBet/cashout-bet.class';
import { ICashOutData } from '@app/betHistory/models/cashout-section.model';
import { cashoutConstants } from '../../constants/cashout.constant';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('CashoutSectionService', () => {
  let service: CashoutSectionService;

  let cashOutMapService,
    userService,
    cashOutLiveServeUpdatesService,
    pubSubService,
    cashOutLiveUpdatesSubscribeService,
    betModelService,
    cashOutMapIndexService,
    betHistoryMainService,
    cashOutErrorMessage,
    localeService;

  const part = {
    startTime: new Date().toISOString(),
    outcome: [{
      name: 'Wimbeldon To Win',
      event: {
        startTime: new Date().toISOString(),
      },
      market: {},
      result: {
        value: 1
      },
      eventCategory: {
        id: 16
      }
    }],
    price: [{
      priceNum: '1',
      priceDen: '2'
    }]
  };

  beforeEach(() => {
    cashOutMapService = {
      cashoutBetsMap: {
        1000: {
          outcome: [],
          market: [],
          event: []
        },
        5000: {
          outcome: [],
          market: [],
          event: []
        },
      }
    };
    userService = {};
    cashOutLiveServeUpdatesService = {};
    cashOutLiveUpdatesSubscribeService = {
      addWatchForRegularBets: jasmine.createSpy('addWatchForRegularBets'),
      addWatchForPlacedEventsOnly: jasmine.createSpy('addWatchForPlacedEventsOnly'),
    };
    betModelService = {
      getBetTimeString: jasmine.createSpy('getBetTimeString'),
      createOutcomeName: jasmine.createSpy('createOutcomeName').and.returnValue([part]),
      getPotentialPayout: jasmine.createSpy('getPotentialPayout'),
    };
    cashOutMapIndexService = {
      create: jasmine.createSpy()
    };
    betHistoryMainService = {
      getSortCode: jasmine.createSpy(),
      getBetStatus: jasmine.createSpy(),
      getBetReturnsValue: jasmine.createSpy().and.returnValue(5),
    };
    cashOutErrorMessage = {
      getErrorMessage: jasmine.createSpy()
    };

    localeService = {
      getString: jasmine.createSpy('getString').and.returnValue('locale test string')
    };

    pubSubService = {
      publish: jasmine.createSpy('publish'),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: pubSubApi
    };

    service = new CashoutSectionService(
      cashOutMapService,
      userService,
      cashOutLiveServeUpdatesService,
      pubSubService,
      cashOutLiveUpdatesSubscribeService,
      betModelService,
      cashOutMapIndexService,
      betHistoryMainService,
      cashOutErrorMessage,
      localeService
    );
  });

  it('constructor', () => {
    expect(service).toBeTruthy();
    expect(service['freeBetMsg']).toEqual('locale test string');
  });

  describe('removeCashoutItemWithTimeout', () => {
    let betsMap, options;

    beforeEach(() => {
      betsMap = {
        '123': {
          betId: 123,
          event: ['123'],
          market: [],
          outcome: []
        },
        'bar': {
          betId: 159,
          event: [],
          someBetDataBar: 'someBetDataBar'
        }
      } as any;

      options = {
        betId: '123'
      };
    });

    it('should remove cashout item', fakeAsync(() => {
      service.removeCashoutItemWithTimeout(betsMap, options).subscribe(() => {
        expect(betsMap).toEqual({
          'bar': {
            betId: 159,
            event: [],
            someBetDataBar: 'someBetDataBar',
          }
        } as any);
        expect(pubSubService.publish).toHaveBeenCalledWith('UNSUBSCRIBE_LS_UPDATES_MS', {
          event: ['123'],
          market: [],
          outcome: []
        });
      });
      tick(cashoutConstants.tooltipTime + 1);
    }));

    it('shouldn`t call UNSUBSCRIBE if ll items are matched for at lease one bet in the list', fakeAsync(() => {
      betsMap = {
        '123': {
          betId: 123,
          event: ['111'],
          market: ['MKT111'],
          outcome: ['OUTCOME111']
        },
        '222': {
          betId: 222,
          event: ['222', '111'],
          market: ['333', 'MKT111'],
          outcome: ['OUTCOME222', 'OUTCOME111']
        }
      } as any;
      service.removeCashoutItemWithTimeout(betsMap, options).subscribe(() => {
        expect(pubSubService.publish).not.toHaveBeenCalled();
      });
      tick(cashoutConstants.tooltipTime + 1);
    }));

    it('should call UNSUBSCRIBE only for items which are not duplicate in other bets', fakeAsync(() => {
      betsMap = {
        '123': {
          betId: 123,
          event: ['555', '111', '222', '333'],
          market: ['MKT555', 'MKT111', 'MKT222', 'MKT333'],
          outcome: ['OUTCOME555', 'OUTCOME111', 'OUTCOME222', 'OUTCOME333', 'qwerty']
        },
        '222': {
          betId: 222,
          event: ['222', '111'],
          market: ['333', 'MKT111'],
          outcome: ['OUTCOME222', 'OUTCOME111']
        },
        '333': {
          betId: 333,
          event: ['222', '141324123411'],
          market: ['MKT222', 'MKT13241234123411'],
          outcome: ['OUTCOME222', 'OUTCO234123412ME111']
        }
      } as any;
      service.removeCashoutItemWithTimeout(betsMap, options).subscribe(() => {
        expect(pubSubService.publish).toHaveBeenCalledWith('UNSUBSCRIBE_LS_UPDATES_MS', {
          event: ['555', '333'],
          market: ['MKT555', 'MKT333'],
          outcome: ['OUTCOME555', 'OUTCOME333', 'qwerty']
        });
      });
      tick(cashoutConstants.tooltipTime + 1);
    }));

    it('should not remove if bet not exists in map before removal', fakeAsync(() => {
      const betsMapCopy = Object.assign({}, betsMap);
      options.betId = '321';

      service.removeCashoutItemWithTimeout(betsMap, options).subscribe(() => {
        expect(betsMap).toEqual(betsMapCopy);
        expect(pubSubService.publish).not.toHaveBeenCalled();
      });
      tick(cashoutConstants.tooltipTime + 1);
    }));

    it('should remove if bet matches given status', fakeAsync(() => {
      betsMap['123'].cashoutStatus = 'BET_SETTLED';
      options.isRegularBets = true;
      const betsMapCopy = Object.assign({}, betsMap);

      service.removeCashoutItemWithTimeout(betsMap, options).subscribe(() => {
        expect(betsMap).not.toEqual(betsMapCopy);
        expect(betsMap['123']).toBeUndefined();
        expect(pubSubService.publish).toHaveBeenCalled();
      });

      tick(cashoutConstants.tooltipTime + 1);
    }));

    it('should remove if bet matches given status BET_CASHED_OUT', fakeAsync(() => {
      betsMap['123'].cashoutStatus = 'BET_CASHED_OUT';
      options.isRegularBets = true;
      const betsMapCopy = Object.assign({}, betsMap);

      service.removeCashoutItemWithTimeout(betsMap, options).subscribe(() => {
        expect(betsMap).not.toEqual(betsMapCopy);
        expect(betsMap['123']).toBeUndefined();
        expect(pubSubService.publish).toHaveBeenCalled();
      });

      tick(cashoutConstants.tooltipTime + 1);
    }));
  });

  describe('@updateBet', () => {
    const betItem = {
      betId: '123',
      cashoutValue: 23
    };

    it('should update cashout bet', () => {
      const bets = [{
        eventSource: {
          betId: '123',
          cashoutValue: 1
        },
        location: 'tst'
      }];
      service.updateBet(betItem as CashoutBet, bets as ICashOutData[]);

      expect(bets[0].eventSource.cashoutValue).toBe(23);
    });

    it('should Not update cashout bet', () => {
      const bets = [{
        eventSource: {
          betId: '456',
          cashoutValue: 1
        },
        location: 'tst'
      }];
      service.updateBet(betItem as CashoutBet, bets as ICashOutData[]);

      expect(bets[0].eventSource.cashoutValue).toBe(1);
    });
  });

  it('createDataForRegularBets', () => {
    const map = service.createDataForRegularBets([
      {
        id: '12345',
        betType: {
          code: 'a'
        },
        leg: [{
          legType: {
            code: 'a'
          },
          part: [part]
        }],
        stake: {
          value: 1
        }
      }
    ]);

    expect(map).toBeDefined();
  });

  it('registerController', () => {
    service.registerController(cashoutConstants.controllers.MY_BETS_CTRL);
    expect(pubSubService.publish).toHaveBeenCalledWith('CASHOUT_CTRL_STATUS', {
      ctrlName: cashoutConstants.controllers.MY_BETS_CTRL,
      isDestroyed: false
    });
    expect(service['liveServeSubscribers'].length).toBe(1);
  });

  describe('removeListeners', () => {
    it('removeListeners ', () => {
      service.registerController(cashoutConstants.controllers.MY_BETS_CTRL);
      service.registerController(cashoutConstants.controllers.REGULAR_BETS_CTRL);
      service.removeListeners(cashoutConstants.controllers.MY_BETS_CTRL);
      expect(pubSubService.publish).not.toHaveBeenCalledWith('UNSUBSCRIBE_LS_UPDATES_MS');
    });

    it('removeListeners UNSUBSCRIBE_LS_UPDATES_MS', () => {
      service.registerController(cashoutConstants.controllers.MY_BETS_CTRL);
      service.registerController(cashoutConstants.controllers.REGULAR_BETS_CTRL);
      service.removeListeners(cashoutConstants.controllers.MY_BETS_CTRL);
      service.removeListeners(cashoutConstants.controllers.REGULAR_BETS_CTRL);
      expect(pubSubService.publish).toHaveBeenCalledWith('UNSUBSCRIBE_LS_UPDATES_MS');
    });
  });

  describe('removeErrorMessageWithTimeout', () => {
    let bets: any;

    beforeEach(() => {
      bets = [{
        eventSource: {
          betId: 1,
          isCashOutUnavailable: false,
          isPartialCashOutAvailable: true,
          isCashOutBetError: true,
          type: ''
        }
      }];
    });

    it('should not change bet type if bet not matched with passed bets', fakeAsync(() => {
      const options = {
        betId: 2,
        prevCashoutStatus: ''
      };

      service.removeErrorMessageWithTimeout(bets, options);
      tick(5001);

      expect(bets[0].eventSource.isCashOutUnavailable).toBeFalsy();
      expect(bets[0].eventSource.isPartialCashOutAvailable).toBeTruthy();
      expect(bets[0].eventSource.type).toBeFalsy();
    }));

    it('should change bet type if there was previous cashout status and no change timer yet', fakeAsync(() => {
      const options = {
        betId: 1,
        prevCashoutStatus: 'Y'
      };

      service.removeErrorMessageWithTimeout(bets, options);

      expect(bets[0].eventSource.isCashOutUnavailable).toBeTruthy();
      expect(bets[0].eventSource.isPartialCashOutAvailable).toBeFalsy();
      expect(bets[0].eventSource.type).toEqual('placedBetsWithoutCashoutPossibility');
    }));

    it('should change bet type in timeout if there was no previous cashout status', fakeAsync(() => {
      const options = {
        betId: 1,
        prevCashoutStatus: ''
      };

      service.removeErrorMessageWithTimeout(bets, options);
      expect(bets[0].eventSource.type).toBeFalsy();
      tick(5001);

      expect(bets[0].eventSource.isCashOutUnavailable).toBeTruthy();
      expect(bets[0].eventSource.isPartialCashOutAvailable).toBeFalsy();
      expect(bets[0].eventSource.isCashOutBetError).toBeFalsy();
      expect(bets[0].eventSource.type).toEqual('placedBetsWithoutCashoutPossibility');
    }));

    it('should change bet type in timeout if there was previous cashout status and change timeout', fakeAsync(() => {
      const options = {
        betId: 1,
        prevCashoutStatus: 'Y'
      };

      service.sortNotFilered = 1;
      service.removeErrorMessageWithTimeout(bets, options);
      expect(bets[0].eventSource.type).toBeFalsy();
      tick(5001);

      expect(bets[0].eventSource.isCashOutUnavailable).toBeTruthy();
      expect(bets[0].eventSource.isPartialCashOutAvailable).toBeFalsy();
      expect(bets[0].eventSource.isCashOutBetError).toBeFalsy();
      expect(bets[0].eventSource.type).toEqual('placedBetsWithoutCashoutPossibility');
    }));
  });

  describe('createTempDataForMyBets', () => {
    it('not bets', () => {
      expect(service.createTempDataForMyBets(null, null)).toEqual({});
    });

    it('cashout bets', () => {
      cashOutMapService.cashoutBetsMap['2'] = {};

      const result = service.createTempDataForMyBets([
        { id: '1', isSettled: true },
        { id: '2', isSettled: false }
      ] as any, null);

      expect(result).toEqual({
        '2': cashOutMapService.cashoutBetsMap['2']
      });
    });

    it('placed bets', () => {
      cashOutMapService.cashoutBetsMap['1'] = {};
      service.getMyBetsIds = jasmine.createSpy('getMyBetsIds');

      const cashoutIds: any = [{ id: '1' }];

      const placedBets: any = [
        { betId: '1' },
        { betId: '2', cashoutValue: 'BET_CASHED_OUT' },
        { betId: '3', settled: 'Y' },
        { betId: '4', cashoutValue: '0.90' },
        { betId: '5', isCashOutUnavailable: true }
      ];

      const result = service.createTempDataForMyBets(cashoutIds, placedBets);

      expect(result).toEqual({
        '1': {},
        '4': jasmine.objectContaining({ betId: '4' }),
        '5': jasmine.objectContaining({ betId: '5', type: 'placedBetsWithoutCashoutPossibility' })
      } as any);

      expect(service.getMyBetsIds).toHaveBeenCalledTimes(1);
      expect(cashOutLiveUpdatesSubscribeService.addWatchForPlacedEventsOnly).toHaveBeenCalledTimes(1);
    });

    it('should skip not mapped bets', () => {
      const ids = <any>[
        {
          id: 1000,
          isSettled: false
        },
        {
          id: 5000,
          isSettled: false
        },
        {
          id: 10000,
          isSettled: false
        }
      ];
      const result = service.createTempDataForMyBets(ids, []);

      expect(result['1000']).toEqual(jasmine.any(Object));
      expect(result['5000']).toEqual(jasmine.any(Object));
      expect(result['10000']).toBeUndefined();
    });
  });

  describe('isCashoutError', () => {
    it('should return true for cashout unavaileble and cashout error case', () => {
      expect(service.isCashoutError({ isCashOutBetError: true, isCashOutUnavailable: false } as any)).toBeTruthy();
      expect(service.isCashoutError({ isCashOutBetError: false, isCashOutUnavailable: true } as any)).toBeTruthy();
      expect(service.isCashoutError({ isCashOutBetError: false, isCashOutUnavailable: false, hasFreeBet: true } as any)).toBeTruthy();
    });
    it('should return false for cashout unavaileble and cashout error case', () => {
      expect(service.isCashoutError({ isCashOutBetError: false, isCashOutUnavailable: false } as any)).toBeFalsy();
    });
  });

  describe('getCashoutError', () => {
    it('should attemptPanelMsg or panelMsg if exists', () => {
      expect(service.getCashoutError({ attemptPanelMsg: { msg: 'attemptPanelMsg' } } as any)).toEqual('attemptPanelMsg');
      expect(service.getCashoutError({ panelMsg: { msg: 'panelMsg' } } as any)).toEqual('panelMsg');
    });

    it('should return free bet notification', () => {
      expect(service.getCashoutError({ hasFreeBet: true, isCashOutUnavailable: false, } as any)).toEqual('locale test string');
    });

    it('should return empty value', () => {
      expect(service.getCashoutError({} as any)).toBeFalsy();
      expect(service.getCashoutError({ hasFreeBet: false } as any)).toBeFalsy();
      expect(service.getCashoutError({ hasFreeBet: true, isCashOutUnavailable: true } as any)).toBeFalsy();
    });
  });

  it('emitMyBetsCounterEvent', () => {
    const bets = [{}] as any;
    service.emitMyBetsCounterEvent(bets);
    expect(pubSubService.publish).toHaveBeenCalledWith('EVENT_MY_BETS_COUNTER', 1);
  });
});
