import { OneTwoFreeDialogComponent } from './one-two-free-dialog.component';
import { ModalComponent } from 'angular-custom-modal';

describe('OneTwoFreeDialogComponent', () => {
  let component: OneTwoFreeDialogComponent;
  let deviceService;
  let pubSubService;

  beforeEach(() => {
    const elementRef = {
      nativeElement: {
        id: 'elementId'
      }
    };

    const changeDetectorRef = {};

    deviceService = {
      isMobile: true
    };

    pubSubService = {
      publish: jasmine.createSpy(),
      API: 'TOGGLE_MOBILE_HEADER_FOOTER'
    };

    component = new OneTwoFreeDialogComponent(
      elementRef as any,
      changeDetectorRef as any,
      deviceService as any,
      pubSubService as any
    );

    component.modal = <any> {
      open: jasmine.createSpy()
    };
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should extend ModalComponent', () => {
    expect(component instanceof ModalComponent).toBeTruthy();
  });

  it('should hide footer & header', () => {
    deviceService.isMobile = true;
    component.ngOnInit();
    expect(pubSubService.publish).toHaveBeenCalledWith(
        pubSubService.API.TOGGLE_MOBILE_HEADER_FOOTER, false
    );
  });

  it('shouldn`t hide footer & header', () => {
    deviceService.isMobile = false;
    component.ngOnInit();
    expect(pubSubService.publish).toHaveBeenCalledWith(
        pubSubService.API.TOGGLE_MOBILE_HEADER_FOOTER, true
    );
  });

  it('shouldn`t open modal dialog', () => {
    deviceService.isMobile = false;
    component.ngOnInit();
    expect(component.modal.open).not.toHaveBeenCalled();
  });

  it('shouldn`t open modal dialog', () => {
    deviceService.isMobile = false;
    component.ngOnInit();
    expect(component.closeOnOutsideClick).toBe(true);
  });

  it('should show footer & header', () => {
    component.ngOnDestroy();
    expect(pubSubService.publish).toHaveBeenCalled();
  });
});
