import { Component, ViewChild, OnInit, ChangeDetectorRef, ElementRef, OnDestroy } from '@angular/core';
import { ModalComponent } from 'angular-custom-modal';
import { DeviceService } from '@coreModule/services/device/device.service';
import { PubSubService } from '@coreModule/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'one-two-free-dialog',
  templateUrl: './one-two-free-dialog.component.html',
  styleUrls: ['./one-two-free-dialog.component.less']
})

export class OneTwoFreeDialogComponent extends ModalComponent implements OnInit, OnDestroy {
  @ViewChild('OneTwoFreeDialog') modal: ModalComponent;

  isMobile: boolean = true;

  constructor(
    elementRef: ElementRef,
    changeDetectorRef: ChangeDetectorRef,
    private deviceService: DeviceService,
    private pubSubService: PubSubService
  ) {
    super(elementRef, changeDetectorRef);
  }

  ngOnInit(): void {
    this.isMobile = this.deviceService.isMobile;

    if (this.isMobile) {
      this.pubSubService.publish(this.pubSubService.API.TOGGLE_MOBILE_HEADER_FOOTER, false);
    } else {
      this.pubSubService.publish(this.pubSubService.API.TOGGLE_MOBILE_HEADER_FOOTER, true);
    }
  }

  ngOnDestroy(): void {
    this.isMobile && this.pubSubService.publish(this.pubSubService.API.TOGGLE_MOBILE_HEADER_FOOTER, true);
  }

}
