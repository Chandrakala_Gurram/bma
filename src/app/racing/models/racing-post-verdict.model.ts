export interface IRacingPostVerdict {
  starRatings: IStarRating[];
  tips: IRacingPostTip[];
  verdict: string;
  imgUrl: string;
  isFilled: boolean;
}

export interface IStarRating {
  name: string;
  rating: number;
}

export interface IRacingPostTip {
  name: string;
  value: string;
}
