import { Injectable } from '@angular/core';

@Injectable()
export class SortByOptionsService {
  option: string = 'Price';

  set(option: string): void {
    this.option = option;
  }

  get(): string {
    return this.option;
  }
}
