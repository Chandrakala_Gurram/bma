import { of as observableOf } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';

import { ForecastTricastGuard } from '@racing/guards/forecast-tricast-guard.service';

describe('#ForecastTricastGuardService', () => {
  let cmsService;
  let router;
  let guard;
  let route;
  let handlerFn;

  beforeEach(() => {
    handlerFn = jasmine.createSpy('handlerFn');
    route = {
      paramMap: new Map()
    };
    cmsService = {
      getSystemConfig: jasmine.createSpy().and.returnValue(observableOf({
        forecastTricastRacing: {
          enabled: true
        }
      }))
    };
    router = {
      navigate: jasmine.createSpy('navigate')
    };
    guard = new ForecastTricastGuard(cmsService, router);
  });

  it('constructor', () => {
    expect(guard).toBeTruthy();
  });

  it('canActivate: market param is "forecast"', fakeAsync(() => {
    route.paramMap.set('market', 'forecast');
    guard.canActivate(route).subscribe((result) => {
      expect(result).toEqual(true);
    });

    tick();
  }));

  it('canActivate: market param is "tricast"', fakeAsync(() => {
    route.paramMap.set('market', 'tricast');
    guard.canActivate(route).subscribe((result) => {
      expect(result).toEqual(true);
    });

    tick();
  }));

  it('canActivate: market param is "forecast", forecastTricastRacing is disabled', fakeAsync(() => {
    route.paramMap.set('market', 'forecast');
    cmsService = {
      getSystemConfig: jasmine.createSpy().and.returnValue(observableOf({
        forecastTricastRacing: {
          enabled: false
        }
      }))
    };
    router = {
      navigate: jasmine.createSpy('navigate')
    };

    guard = new ForecastTricastGuard(cmsService, router);

    guard.canActivate(route).subscribe((result) => {
      expect(result).toEqual(false);
      expect(router.navigate).toHaveBeenCalledWith(['/horse-racing/']);
    });

    tick();
  }));

  it('canActivate: market param is "tricast", forecastTricastRacing is disabled', fakeAsync(() => {
    route.paramMap.set('market', 'tricast');
    guard.cmsService = {
      getSystemConfig: jasmine.createSpy().and.returnValue(observableOf({
        forecastTricastRacing: {
          enabled: false
        }
      }))
    };

    guard.canActivate(route).subscribe((result) => {
      expect(result).toEqual(false);
      expect(router.navigate).toHaveBeenCalledWith(['/horse-racing/']);
    });

    tick();
  }));

  it('canActivate: market param is "test"', fakeAsync(() => {
    route.paramMap.set('market', 'test');
    guard.canActivate(route).subscribe((result) => {
      expect(result).toEqual(true);
    });

    tick();
  }));

  it('should handle non tricast/forcast markets', fakeAsync(() => {
    route.paramMap.set('market', 'to-finish');

    guard.canActivate(route).subscribe(handlerFn);
    tick();

    expect(handlerFn).toHaveBeenCalledWith(true);
    expect(router.navigate).not.toHaveBeenCalled();
  }));

  it('should handle empty market name', fakeAsync(() => {
    route.paramMap.set('market', null);

    guard.canActivate(route).subscribe(handlerFn);
    tick();

    expect(handlerFn).toHaveBeenCalledWith(true);
    expect(router.navigate).not.toHaveBeenCalled();
  }));

  it('should handle forecast market name when cms is disabled', fakeAsync(() => {
    route.paramMap.set('market', 'forecast');
    cmsService = {
      getSystemConfig: jasmine.createSpy().and.returnValue(observableOf({
        forecastTricastRacing: null
      }))
    };

    guard = new ForecastTricastGuard(cmsService, router);

    guard.canActivate(route).subscribe(handlerFn);
    tick();

    expect(handlerFn).toHaveBeenCalledWith(false);
    expect(router.navigate).toHaveBeenCalledWith(['/horse-racing/']);
  }));

  it('should handle tricast market name when cms is disabled', fakeAsync(() => {
    route.paramMap.set('market', 'tricast');
    cmsService = {
      getSystemConfig: jasmine.createSpy().and.returnValue(observableOf({
        forecastTricastRacing: {
          enabled: false
        }
      }))
    };

    guard = new ForecastTricastGuard(cmsService, router);

    guard.canActivate(route).subscribe(handlerFn);
    tick();

    expect(handlerFn).toHaveBeenCalledWith(false);
    expect(router.navigate).toHaveBeenCalledWith(['/horse-racing/']);
  }));

  it('should handle forecast market name when cms is enabled', fakeAsync(() => {
    route.paramMap.set('market', 'forecast');

    guard.canActivate(route).subscribe(handlerFn);
    tick();

    expect(handlerFn).toHaveBeenCalledWith(true);
    expect(router.navigate).not.toHaveBeenCalled();
  }));

  it('should handle forecast market name when cms is enabled', fakeAsync(() => {
    route.paramMap.set('market', 'tricast');

    guard.canActivate(route).subscribe(handlerFn);
    tick();

    expect(handlerFn).toHaveBeenCalledWith(true);
    expect(router.navigate).not.toHaveBeenCalled();
  }));
});
