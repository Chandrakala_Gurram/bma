import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';

import { CmsService } from '@coreModule/services/cms/cms.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ForecastTricastGuard implements CanActivate {
  constructor(
    private cmsService: CmsService,
    private router: Router
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const marketName = route.paramMap && route.paramMap.get('market') || '';
    const isForecastTricast = ['forecast', 'tricast'].indexOf(marketName.toLowerCase()) > -1;

    if (isForecastTricast) {
      return this.cmsService.getSystemConfig()
        .pipe(map(config => {
          if (config.forecastTricastRacing && config.forecastTricastRacing.enabled === true) {
            return true;
          } else {
            this.router.navigate(['/horse-racing/']);
            return false;
          }
        }));
    } else {
      return of(true);
    }
  }
}
