import { Component, Input } from '@angular/core';

@Component({
  selector: 'racing-post-widget',
  templateUrl: 'racing-post-widget.component.html'
})
export class RacingPostWidgetComponent {
  @Input() racingPostSummary: string;

  constructor() { }
}
