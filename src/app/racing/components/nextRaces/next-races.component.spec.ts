import { NextRacesModuleComponent } from './next-races.component';
import { RacingGaService } from '@racing/services/racing-ga.service';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';
import { of as observableOf, throwError } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';

describe('NextRacesModuleComponent', () => {
  let component: NextRacesModuleComponent;
  let pubSubService, cmsService, location, routingHelperService,
    nextRacesService, eventService, commandService, gtm, locale, racingPostService;
  let cmsData;
  let racingGaService;

  beforeEach(() => {
    pubSubService = {
      publish: jasmine.createSpy('publish'),
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: pubSubApi,
    };
    gtm = {};
    locale = {};
    cmsService = {
      triggerSystemConfigUpdate: jasmine.createSpy('triggerSystemConfigUpdate'),
    };
    location = {
      path: jasmine.createSpy('path')
    };
    routingHelperService = {
      formSportUrl: jasmine.createSpy('formSportUrl').and.returnValue(observableOf({}))
    };
    nextRacesService = {
      getNextRacesModuleConfig: jasmine.createSpy('getNextRacesModuleConfig').and.returnValue({}),
      subscribeForUpdates: jasmine.createSpy('subscribeForUpdates'),
      unSubscribeForUpdates: jasmine.createSpy('unSubscribeForUpdates'),
      getUpdatedEvents: jasmine.createSpy('getUpdatedEvents'),
      cacheKey: 'nextRaces'
    };
    eventService = {
      getNextEvents: jasmine.createSpy('getNextEvents').and.returnValue(Promise.resolve([{ id: 1 }, { id: 2 }]))
    };
    racingGaService = new RacingGaService(gtm, locale, pubSubService);
    racingGaService.sendGTM = jasmine.createSpy('sendGTM');
    racingGaService.trackNextRacesCollapse = jasmine.createSpy('trackNextRacesCollapse');
    commandService = {
      executeAsync: jasmine.createSpy('executeAsync').and.returnValue(Promise.resolve(racingGaService)),
      API: {
        RACING_GA_SERVICE: 'test'
      }
    };
    racingPostService = {
      updateRacingEventsList: jasmine.createSpy('updateRacingEventsList').and.returnValue(
        observableOf([{ id: 1, racingPostEvent: {} }, { id: 2, racingPostEvent: {} }]))
    };
    cmsData = {
      NextRaces: { title: 'Next races', numberOfSelections: '10' },
      GreyhoundNextRaces: { numberOfSelections: '4' },
      RacingDataHub: { isEnabledForGreyhound: true}
    };

    createComponent();
  });

  function createComponent() {
    component = new NextRacesModuleComponent(
      pubSubService,
      cmsService,
      location,
      routingHelperService,
      nextRacesService,
      eventService,
      commandService,
      racingPostService
    );

    component.moduleType = 'horseracing';
  }

  describe('ngOnInit racingSpecials', function () {
    it('ngOnInit: widget=true, headerVisible=true', () => {
      component.moduleType = 'horseracing';
      component.widget = true;
      component.headerVisible = true;
      component.moduleType = 'horseracing';
      pubSubService.subscribe.and.callFake((subscriberName, command, cb) => cb(cmsData));
      component.ngOnInit();
      expect(component.headerClass).toEqual('secondary-header');
      expect(component.leftTitleText).toEqual(component.moduleTitle);
      expect(component.className).toBe('next-races next-races-horseracing');
      expect(component.showBriefHeader).toBe(true);
    });

    it('ngOnInit: widget=false, headerVisible=false', () => {
      component.widget = false;
      component.headerVisible = false;
      pubSubService.subscribe.and.callFake((subscriberName, command, cb) => cb(cmsData));
      component.ngOnInit();
      expect(component.headerClass).toEqual('secondary-header');

      expect(component.leftTitleText).toEqual(component.moduleTitle);
    });

    it('ngOnInit: widget=true, headerVisible=false', () => {
      component.widget = true;
      component.headerVisible = false;
      pubSubService.subscribe.and.callFake((subscriberName, command, cb) => cb(cmsData));
      component.ngOnInit();
      expect(component.headerClass).toEqual('');
      expect(component.leftTitleText).toEqual('');
    });

    it('ngOnInit: widget=false, headerVisible=true', () => {
      component.widget = false;
      component.headerVisible = true;
      pubSubService.subscribe.and.callFake((subscriberName, command, cb) => cb(cmsData));
      component.ngOnInit();
      expect(component.headerClass).toEqual('secondary-header');
      expect(component.leftTitleText).toEqual(component.moduleTitle);
    });
    it(`default value of raceIndex should be 'next-races'`, () => {
      component.ngOnInit();

      expect(component.raceIndex).toEqual('next-races');
    });
  });

  describe('getCmsConfigs method', function () {
    let pubSubFn;

    beforeEach(() => {
      component.raceModule = 'NEXT_RACE';
      spyOn(component, 'getNextEvents');
      pubSubService.subscribe.and.callFake((subscriberName, command, cb) => pubSubFn = cb);
    });

    it('should subscribe to pubsub SYSTEM_CONFIG_UPDATED', () => {
      component.getCmsConfigs();
      expect(pubSubService.subscribe).toHaveBeenCalledWith('MODULE_NEXT_RACE', 'SYSTEM_CONFIG_UPDATED', jasmine.any(Function));
    });

    it('should execute nextRacesService.getNextRacesModuleConfig and getNextEvents methods and update props on subscription', () => {
      cmsData.NextRaces.numberOfSelections = 0;
      component.getCmsConfigs();
      pubSubFn(cmsData);
      expect(component.numberOfSelections).toEqual(3);
      expect(component.moduleTitle).toEqual('Next races');
      expect(nextRacesService.getNextRacesModuleConfig).toHaveBeenCalledWith('horseracing',
        {
          NextRaces: { title: 'Next races', numberOfSelections: 0 },
          GreyhoundNextRaces: { numberOfSelections: '4' },
          RacingDataHub: { isEnabledForGreyhound: true}
        });
      expect(component.getNextEvents).toHaveBeenCalled();
    });

    it('should execute nextRacesService.getNextRacesModuleConfig and getNextEvents methods and update props on subscription', () => {
      cmsData.NextRaces.numberOfSelections = 10;
      delete cmsData.RacingDataHub;
      component.getCmsConfigs();
      pubSubFn(cmsData);
      expect(component.numberOfSelections).toEqual(10);
      expect(nextRacesService.getNextRacesModuleConfig).toHaveBeenCalledWith('horseracing',
        {
          NextRaces: { title: 'Next races', numberOfSelections: 10 },
          GreyhoundNextRaces: { numberOfSelections: '4' },
          RacingDataHub: undefined,
        });
      expect(component.getNextEvents).toHaveBeenCalled();
    });

    describe('should not call nextRacesService.getNextRacesModuleConfig and getNextEvents methods', () => {
      it('when cms config does not contain NextRaces entry', () => {
        cmsData = {};
        component.getCmsConfigs();
        pubSubFn(cmsData);
      });

      it('when called more than once with the same config', () => {
        component.getCmsConfigs();
        pubSubFn(cmsData);
        nextRacesService.getNextRacesModuleConfig.calls.reset();
        (component.getNextEvents as any).calls.reset();
        pubSubFn(cmsData);
      });
      afterEach(() => {
        expect(component.getNextEvents).not.toHaveBeenCalled();
        expect(nextRacesService.getNextRacesModuleConfig).not.toHaveBeenCalled();
      });
    });
  });

  describe('getNextEvents method', () => {
    it('should set showLoader and ssDown on error', fakeAsync(() => {
      const nextRacesModule = {};
      component.eventsLoaded.emit = jasmine.createSpy('eventsLoaded.emit');
      component.nextRacesModule = <any>nextRacesModule;
      racingPostService.updateRacingEventsList = jasmine.createSpy('updateRacingEventsList').and.returnValue(
        throwError({}));

      component.getNextEvents();
      tick();

      expect(component.showLoader).toEqual(false);
      expect(component.ssDown).toEqual(true);
      expect(component.eventsLoaded.emit).toHaveBeenCalled();
    }));

    it('should call racingPostService.updateRacingEventsList method', fakeAsync(() => {
      const nextRacesModule = {};
      component.nextRacesModule = <any>nextRacesModule;
      component.getNextEvents();
      tick();
      expect(eventService.getNextEvents).toHaveBeenCalledWith(nextRacesModule, 'nextRaces');
      expect(racingPostService.updateRacingEventsList).toHaveBeenCalledWith([{ id: 1 }, { id: 2 }], true);
      expect(nextRacesService.getUpdatedEvents).toHaveBeenCalledWith(
        [{ id: 1, racingPostEvent: { } }, { id: 2, racingPostEvent: { } }], 'horseracing');
    }));

    it('should store data subscription', fakeAsync(() => {
      const nextRacesModule = {};
      component.nextRacesModule = <any>nextRacesModule;
      component.getNextEvents();
      tick();

      expect(component['loadDataSubscription']).toBeDefined();
    }));
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('registerEvents', () => {
    it('should registerEvents', () => {
      component.raceModule = 'W_NEXT_RACE';
      component.registerEvents();
      expect(pubSubService.subscribe).toHaveBeenCalledWith('MODULE_W_NEXT_RACE', 'RELOAD_COMPONENTS', jasmine.any(Function));
    });
  });

  describe('ngOnDestroy', () => {
    it('should unsubscribe from pubsub events', () => {
      component.ngOnDestroy();
      expect(pubSubService.unsubscribe).toHaveBeenCalledWith(component.MODULE_NAME);
    });

    it('should unsubscribe from data load subscription', () => {
      const loadDataSubscription = jasmine.createSpyObj('loadDataSubscription', ['unsubscribe']);
      const subscriptionId = 'moduleName';

      component['subscriptionId'] = subscriptionId;
      component['loadDataSubscription'] = loadDataSubscription;
      component.ngOnDestroy();

      expect(loadDataSubscription.unsubscribe).toHaveBeenCalled();
      expect(nextRacesService.unSubscribeForUpdates).toHaveBeenCalledWith(subscriptionId);
    });
  });

  describe('trackCollapse', () => {
    it('should call sendGTM with eventCategory equals to home, set flag to true and NOT call trackNextRacesCollapse', fakeAsync(() => {
      component.trackGaDesktop = true;
      component['location'].path = jasmine.createSpy().and.returnValue('/');

      component.trackCollapse();
      tick();

      expect(racingGaService.sendGTM).toHaveBeenCalledWith('collapse', 'home');
      expect(racingGaService.flag[racingGaService.CONST.WIDGET]).toEqual(true);
      expect(racingGaService.trackNextRacesCollapse).not.toHaveBeenCalled();
    }));

    it('should call sendGTM with eventCategory equals to widget, set flag to true and NOT call trackNextRacesCollapse', fakeAsync(() => {
      component.trackGaDesktop = true;
      component['location'].path = jasmine.createSpy().and.returnValue('test');

      component.trackCollapse();
      tick();

      expect(racingGaService.sendGTM).toHaveBeenCalledWith('collapse', 'widget');
      expect(racingGaService.flag[racingGaService.CONST.WIDGET]).toEqual(true);
      expect(racingGaService.trackNextRacesCollapse).not.toHaveBeenCalled();
    }));

    it('should NOT call sendGTM and set flag to true and call trackNextRacesCollapse', fakeAsync(() => {
      component.trackGa = true;
      component.trackGaDesktop = false;

      component.trackCollapse();
      tick();

      expect(racingGaService.sendGTM).not.toHaveBeenCalled();
      expect(racingGaService.trackNextRacesCollapse).toHaveBeenCalled();
    }));
  });

  it('sendToGTM should call sendGTM', fakeAsync(() => {
    component.sendToGTM();
    tick();

    expect(racingGaService.sendGTM).toHaveBeenCalledWith('view all', 'home');
  }));

  it('reloadComponent should set ssDown and call getNextEvents', () => {
    component.getNextEvents = jasmine.createSpy('component.getNextEvents');

    component.reloadComponent();

    expect(component.ssDown).toEqual(false);
    expect(component.getNextEvents).toHaveBeenCalled();
  });
});
