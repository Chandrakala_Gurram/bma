import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { TimeService } from '@core/services/time/time.service';
import { RacingResultsService } from '@core/services/sport/racing-results.service';
import { IRacingEvent } from '@core/models/racing-event.model';
import { LocaleService } from '@core/services/locale/locale.service';
import { IOutcome } from '@core/models/outcome.model';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import * as _ from 'underscore';

@Component({
  templateUrl: 'racing-event-resulted.component.html',
  styleUrls: ['./racing-event-resulted.component.less'],
  selector: 'racing-event-resulted'
})
export class RacingEventResultedComponent implements OnInit, OnDestroy {
  @Input() eventEntity: IRacingEvent;

  eventDateSufx: string;
  antepostTerms: string;
  isCashout: boolean;
  resultsResponseError: string;

  private readonly tagName: string = 'RacingEventResulted';

  constructor(protected timeService: TimeService,
              private racingResultsService: RacingResultsService,
              private locale: LocaleService,
              private pubsubService: PubSubService) {}

  ngOnInit(): void {
    // reload routeSegment after lost of internet connection or sleep mode
    this.pubsubService.subscribe(this.tagName, this.pubsubService.API.RELOAD_RACING_EVENT_RESULTED, () => {
      this.ngOnDestroy();
      this.ngOnInit();
    });

    this.racingResultsService.getRacingResults(this.eventEntity)
      .then(() => {
        const wewMarket = this.eventEntity.resultedWEWMarket;

        if (!wewMarket.outcomes.length) {
          this.resultsResponseError = this.locale.getString('racing.noRacingResultsFound');
        } else {
          this.antepostTerms = wewMarket.isEachWayAvailable ? this.formatAntepostTerms(wewMarket.terms) : '';
          this.isCashout = wewMarket.cashoutAvail === 'Y' || wewMarket.viewType === 'handicaps';
          this.extendOutcome(wewMarket.outcomes);

          if (wewMarket.nonRunners && wewMarket.nonRunners.length) {
            this.extendOutcome(wewMarket.nonRunners);
          }
        }
      }, () => {
        this.resultsResponseError = this.locale.getString('racing.noRacingResultsFound');
      });

    this.eventDateSufx = this.getEventDate();
  }

  ngOnDestroy(): void {
    this.pubsubService.unsubscribe(this.tagName);
  }

  getEventDate(): string {
    return this.timeService.getFullDateFormatSufx(new Date(this.eventEntity.startTime));
  }

  formatAntepostTerms(str: string): string {
    const newStr = str
      .replace(/(odds)/ig, 'Odds')
      .replace(/(places)/ig, 'Places')
      .replace(/\d+\/\d+( odds)/ig, match => {
        return `${match}`;
      });
    return newStr.replace(/[0-9]+(?!.*[0-9])/, match => `${match}`);
  }

  private extendOutcome(outcomes: IOutcome[]): void {
    _.each(outcomes, (outcome: IOutcome) => {
      const racingForm = _.findWhere(this.eventEntity.sortedMarkets[0].outcomes,
        { runnerNumber: outcome.runnerNumber });
      if (racingForm) {
        outcome.racingFormOutcome = racingForm.racingFormOutcome;
      }
    });
  }
}
