import { RacingEventResultedComponent } from './racing-event-resulted.component';
import { fakeAsync, tick } from '@angular/core/testing';

describe('RacingEventResultedComponent', () => {
  let component: RacingEventResultedComponent,
      timeService,
      racingResultsService,
      locale,
      pubsubService,
      reloadPage;

  const mockMarket = {
    id: 1,
    isEachWayAvailable: true,
    isGpAvailable: true,
    terms: 'Test',
    outcomes : [
      {
        id: 1,
        nonRunner : true,
        name: 'Test N/R',
        runnerNumber: '2',
        racingFormOutcome : { id : 1 }
      },
      {
        id: 2,
        name: 'Lorem',
        runnerNumber: '1',
        racingFormOutcome : { id : 2 }
      }
    ]
  };
  const resultedWEWMarket = {
    id : 1,
    cashoutAvail: 'N',
    viewType: 'handicaps',
    isEachWayAvailable: true,
    nonRunners: [{}],
    outcomes : [
      {
        id: 1,
        nonRunner : true,
        name: 'Test N/R',
        runnerNumber: '2',
        racingFormOutcome : { id : 1 }
      },
      {
        id: 2,
        name: 'Lorem',
        runnerNumber: '1',
        racingFormOutcome : { id : 2 }
      }
    ]
  };
  const mockEvent = {
    resultedWEWMarket,
    sortedMarkets : [ mockMarket ],
    startTime : 'Mon Feb 11 2019 12:00:59 GMT+0200 (Eastern European Standard Time)'
  } as any;

  beforeEach(() => {
    timeService = {
      getFullDateFormatSufx: jasmine.createSpy()
    };
    racingResultsService = {
      getRacingResults: jasmine.createSpy().and.returnValue(Promise.resolve({ voidResult: false })),
      isFavourite: jasmine.createSpy().and.returnValue(false)
    };
    locale = {
      getString: jasmine.createSpy().and.returnValue('noRacingResultsFound')
    };

    pubsubService = {
      subscribe: jasmine.createSpy('subscribe').and.callFake((name, listeners, handler) => {
        reloadPage = handler;
        spyOn(component, 'ngOnInit');
        spyOn(component, 'ngOnDestroy');
      }),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: {
        RELOAD_RACING_EVENT_RESULTED: 'RELOAD_RACING_EVENT_RESULTED'
      }
    };

    component = new RacingEventResultedComponent(
      timeService,
      racingResultsService,
      locale,
      pubsubService
    );
    component.eventEntity = mockEvent;
  });

  it('#ngOnInit', fakeAsync(() => {
    component.getEventDate = jasmine.createSpy();
    component.formatAntepostTerms = jasmine.createSpy().and.returnValue('Lorem');
    component.eventEntity.resultedWEWMarket.isEachWayAvailable = true;
    component.eventEntity.resultedWEWMarket.viewType = 'test-no-handicaps';
    component['extendOutcome'] = jasmine.createSpy();
    component.ngOnInit();

    expect(pubsubService.subscribe).toHaveBeenCalledWith('RacingEventResulted', 'RELOAD_RACING_EVENT_RESULTED', jasmine.any(Function));
    reloadPage();
    expect(component.ngOnInit).toHaveBeenCalled();
    expect(component.ngOnDestroy).toHaveBeenCalled();
    expect(racingResultsService.getRacingResults).toHaveBeenCalledWith(mockEvent);
    tick();
    expect(component.getEventDate).toHaveBeenCalled();
    expect(component.antepostTerms).toBe('Lorem');
    expect(component.isCashout).toBe(false);
    expect(component.formatAntepostTerms).toHaveBeenCalled();
    expect(component['extendOutcome']).toHaveBeenCalled();
  }));

  it('#ngOnInit cashoutAvail = N', fakeAsync(() => {
    component.getEventDate = jasmine.createSpy();
    component.formatAntepostTerms = jasmine.createSpy();
    component['extendOutcome'] = jasmine.createSpy();
    component.eventEntity.resultedWEWMarket.viewType = 'handicaps';
    component.ngOnInit();

    expect(racingResultsService.getRacingResults).toHaveBeenCalled();
    tick();
    expect(component.getEventDate).toHaveBeenCalled();
    expect(component.isCashout).toBe(true);
    expect(component.formatAntepostTerms).toHaveBeenCalled();
    expect(component['extendOutcome']).toHaveBeenCalledWith(component.eventEntity.resultedWEWMarket.nonRunners);
  }));

  it('#ngOnInit when there are nonRunners', fakeAsync(() => {
    component.getEventDate = jasmine.createSpy();
    component.formatAntepostTerms = jasmine.createSpy();
    component['extendOutcome'] = jasmine.createSpy();

    (racingResultsService.getRacingResults as jasmine.Spy).and.returnValue(Promise.resolve({}));
    component.ngOnInit();

    expect(racingResultsService.getRacingResults).toHaveBeenCalled();
    tick();
    expect(component.isCashout).toBe(true);
    expect(component.getEventDate).toHaveBeenCalled();
    expect(component['extendOutcome']).toHaveBeenCalled();
  }));

  it('#ngOnInit when there are no nonRunners', fakeAsync(() => {
    component['extendOutcome'] = jasmine.createSpy();
    component.eventEntity.resultedWEWMarket.nonRunners = [];
    component.eventEntity.resultedWEWMarket.isEachWayAvailable = false;
    component.ngOnInit();

    expect(component['extendOutcome']).not.toHaveBeenCalled();
  }));

  it('#ngOnInit isEachWayAvailable = false', fakeAsync(() => {
    component.getEventDate = jasmine.createSpy();
    component.formatAntepostTerms = jasmine.createSpy().and.returnValue('Each Way Text');
    component['extendOutcome'] = jasmine.createSpy();
    component.eventEntity.resultedWEWMarket.isEachWayAvailable = false;
    component.eventEntity.resultedWEWMarket.cashoutAvail = 'Y';
    component.eventEntity.resultedWEWMarket.viewType = 'handicaps';
    component.ngOnInit();

    expect(racingResultsService.getRacingResults).toHaveBeenCalled();
    tick();
    expect(component.antepostTerms).toBe('');
    expect(component.isCashout).toBe(true);
    expect(component['getEventDate']).toHaveBeenCalled();
    expect(component['extendOutcome']).toHaveBeenCalledWith(component.eventEntity.resultedWEWMarket.outcomes);
  }));

  it('#ngOnInit no results', fakeAsync(() => {
    component.eventEntity.resultedWEWMarket.outcomes = [];
    component.ngOnInit();
    tick();
    expect(component.resultsResponseError).toBe('noRacingResultsFound');
  }));

  it('#ngOnInit exception', fakeAsync(() => {
    (racingResultsService.getRacingResults as jasmine.Spy).and.returnValue(Promise.reject('error'));
    component.ngOnInit();
    tick();
    expect(component.resultsResponseError).toBe('noRacingResultsFound');
  }));

  it('#ngOnDestroy', () => {
    component.ngOnDestroy();

    expect(pubsubService.unsubscribe).toHaveBeenCalledWith('RacingEventResulted');
  });

  it('#getEventDate', () => {
    component.getEventDate();
    expect(timeService.getFullDateFormatSufx).toHaveBeenCalledWith(new Date(mockEvent.startTime));
  });

  it('#formatAntepostTerms', () => {
    const result = component.formatAntepostTerms('odds places testodds');
    expect(result).toBe('Odds Places testOdds');
  });

  it('#extendOutcome without racingForm', () => {
    component['extendOutcome'](mockMarket.outcomes as any);

    expect(mockMarket.outcomes).toEqual(
      [
        {id: 1, nonRunner: true, name: 'Test N/R', runnerNumber: '2', racingFormOutcome : { id : 1 }},
        {id: 2, name: 'Lorem', runnerNumber: '1', racingFormOutcome : { id : 2 }}
      ] as any
    );
  });

  it('#extendOutcome with racingForm', () => {
    const outcomes = [{ id: 1, runnerNumber: '2', racingFormOutcome: { some: 'lorem' } }] as any;
    component['extendOutcome'](outcomes);

    expect(outcomes).toEqual(
      [
        { id: 1, runnerNumber: '2', racingFormOutcome: { id : 1 } }
      ] as any
    );
  });

});
