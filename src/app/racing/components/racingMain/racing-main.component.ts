import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import {
  of as observableOf,
  forkJoin as observableForkJoin,
  Subscription,
  SubscriptionLike as ISubscription, Observable
} from 'rxjs';
import { mergeMap, map, switchMap } from 'rxjs/operators';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { TemplateService } from '@shared/services/template/template.service';
import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { ISportCategory, ISystemConfig } from '@core/services/cms/models';
import { IInitialSportConfig } from '@core/services/sport/config/initial-sport-config.model';
import { RoutesDataSharingService } from '@racing/services/routesDataSharing/routes-data-sharing.service';
import { HorseracingService } from '@coreModule/services/racing/horseracing/horseracing.service';
import { GreyhoundService } from '@coreModule/services/racing/greyhound/greyhound.service';
import { GtmService } from '@coreModule/services/gtm/gtm.service';
import { RoutingState } from '@sharedModule/services/routingState/routing-state.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { IBreadcrumb } from '@root/app/shared/models/breadcrumbs.model';
import { IGroupedSportEvent, ISportEvent } from '@root/app/core/models/sport-event.model';
import { IRacingHeader } from '@root/app/shared/models/racing-header.model';
import { PubSubService } from '@coreModule/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'racing-main-component',
  templateUrl: './racing-main.component.html'
})
export class RacingMainComponent extends AbstractOutletComponent implements OnInit, OnDestroy {

  defaultTab: string;
  racingInstance: any;
  config: ISystemConfig;
  url: string;
  racingPath: string;
  baseUrl: string;
  breadcrumbsItems: IBreadcrumb[];
  quickNavigationItems: IGroupedSportEvent[];

  // expected data are boolean, other
  racingData: [(HorseracingService | GreyhoundService), boolean, ISystemConfig] | [(HorseracingService | GreyhoundService), boolean];

  racingName: string;
  racingIconId: string;
  racingIconSvg: string;
  racingId: string;

  activeTab: { [id: string]: string } = {};
  racingDefaultPath: string;
  topBarInnerContent: boolean;
  categoryId: string;

  isSpecialsPresent: boolean;
  racingTabs: any;
  hasSubHeader: boolean = false;
  isBetFilterLinkAvailable: boolean = true;
  isEnhancedMultiplesEnabled: boolean = false;
  showMeetings: boolean;
  eventEntity: ISportEvent;
  meetingsTitle: {[key: string]: string};
  isHRDetailPage: boolean;
  topBarIndex: number;
  private routeChangeListener: Subscription;

  private timeOutListener;
  private intervalValue: number = 100;
  protected readonly RACING_MAIN_COMPONENT: string = 'RacingMainComponent';
  protected racingMainSubscription: ISubscription;
  private horseRacingsubscription: Subscription;

  constructor(
    public route: ActivatedRoute,
    public templateService: TemplateService,
    public routingHelperService: RoutingHelperService,
    public routesDataSharingService: RoutesDataSharingService,
    public router: Router,
    public horseRacingService: HorseracingService,
    public greyhoundService: GreyhoundService,
    public routingState: RoutingState,
    public cms: CmsService,
    public changeDetRef: ChangeDetectorRef,
    public windowRefService: WindowRefService,
    protected gtmService: GtmService,
    protected pubSubService: PubSubService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getTopBarData();
    this.addChangeDetection();
    this.racingMainSubscription = observableForkJoin([
      observableOf(this.racingService.getSport()),
      this.racingService.isSpecialsAvailable(this.router.url, true)
    ]).pipe(
      mergeMap((racingData: [(HorseracingService | GreyhoundService), boolean]): Observable<void> => {
        this.racingData = racingData;
        this.initModel();

        // Workaround, related to SEO Static Blocks
        // Needed if 'sb' module is initialized and user types /:static path into address bar
        return this.racingInstance ? observableOf(null) : observableOf();
      }),
      map(() => {
        this.defaultTab = (this.racingName === 'greyhound') ? 'today' : 'featured';
        this.applyRacingConfiguration(this.racingInstance);
        this.selectTabRacing();
      }),
      switchMap(() => this.routesDataSharingService.activeTabId),
      map(id => {
        if (id) {
          this.activeTab = { id };
        }
      }),
      switchMap(() => this.routesDataSharingService.hasSubHeader),
      map(hasSubHeader => this.hasSubHeader = hasSubHeader),
      switchMap(() => this.racingId ? this.templateService.getIconSport(this.racingId) : observableOf(null)),
    ).subscribe((icon: ISportCategory) => {
      if (icon) {
        this.racingIconId = icon.svgId;
        this.racingIconSvg = icon.svg;
      }
      this.hideSpinner();
      this.hideError();
    }, () => {
      this.showError();
    });

    this.horseRacingsubscription = this.router.events.subscribe(() => {
      this.isHRDetailPage = this.isHorseRacingDetailPage();
    });

    this.getSystemConfig();
  }

  goToDefaultPage(): void {
    this.router.navigateByUrl(this.racingDefaultPath);
  }

  getSystemConfig(): void {
    this.cms.getToggleStatus('EnhancedMultiples').subscribe((isEnhancedMultiplesEnabled: boolean) => {
      this.isEnhancedMultiplesEnabled = isEnhancedMultiplesEnabled;
    });
    this.cms.getSystemConfig(false)
      .subscribe((config: ISystemConfig) => {
        if (config.BetFilterHorseRacing && !config.BetFilterHorseRacing.enabled) {
          this.isBetFilterLinkAvailable = false;

        }
      });
  }

  get isDetailPage(): boolean {
    const currentSegment = this.routingState.getCurrentSegment();
    return currentSegment === 'horseracing.eventMain' ||
      currentSegment === 'horseracing.eventMain.market' ||
      currentSegment === 'horseracing.eventMain.market.marketType' ||
      currentSegment === 'greyhound.eventMain' ||
      currentSegment === 'greyhound.eventMain.market' ||
      currentSegment === 'horseracing.buildYourRaceCard';
  }

  ngOnDestroy(): void {
    this.timeOutListener && this.windowRefService.nativeWindow.clearInterval(this.timeOutListener);
    this.routeChangeListener && this.routeChangeListener.unsubscribe();
    this.racingMainSubscription && this.racingMainSubscription.unsubscribe();
    this.pubSubService.unsubscribe(this.RACING_MAIN_COMPONENT);
    this.horseRacingsubscription && this.horseRacingsubscription.unsubscribe();
  }

  addChangeDetection(): void {
    this.changeDetRef.detach();
    this.timeOutListener = this.windowRefService.nativeWindow.setInterval(() => {
      this.changeDetRef.detectChanges();
    }, this.intervalValue);
  }

  get racingService(): HorseracingService | GreyhoundService {
    const segment = this.routingState.getCurrentSegment();

    return segment.indexOf('horseracing') >= 0 ? this.horseRacingService : this.greyhoundService;
  }

  private get isRacingPage() {
    const segment = this.routingState.getCurrentSegment();
    return segment.indexOf('horceracing') > -1 || segment.indexOf('greyhound') > -1;
  }

  initModel(): void {
    this.racingInstance = (this.racingData && this.racingData[0]) || this.racingData;
    this.config = this.racingInstance && this.racingInstance.getConfig();
    this.isSpecialsPresent = this.racingData && this.racingData[1];
    this.racingName = this.config && this.config.name;
    this.categoryId = (this.config && this.config.request.categoryId) || '';
    this.url = this.router.url;
    this.racingPath = (this.config && (this.config.path || this.racingName)) || '';
    this.baseUrl = this.url.substring(0, this.url.indexOf(this.racingPath) + this.racingPath.length);

    // Allow inner content for 'horseracing'
    this.topBarInnerContent = this.racingName === 'horseracing';
  }

  /**
   * Store tab and start to track route change for racing
   */
  selectTabRacing(): void {
    const display: string = this.tabDisplay;
    this.activeTab.id = `tab-${display}`;
    this.routesDataSharingService.updatedActiveTabId(this.activeTab.id);

    if (!display && !this.isDetailPage && this.isRacingPage) {
      this.router.navigateByUrl(`${this.baseUrl}/${this.defaultTab}`);
    }

    this.routeChangeListener = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (this.isRacingLandingPage()) {
          this.goToDefaultPage();
        }
        const displayAfterSwicth: string = this.route.snapshot.firstChild && this.route.snapshot.firstChild.params['display'];
        this.activeTab.id = `tab-${(displayAfterSwicth || this.defaultTab)}`;
      }
    });
  }

  get tabDisplay(): string {
    const firstChild = this.route.snapshot.firstChild;

    if (!firstChild) {
      return this.defaultTab;
    }

    return firstChild.params['display'] || firstChild.routeConfig.path;
  }

  /**
   * Set racing configuration to model from racing config constant, for example: 'HORSERACING_CONFIG'
   * @param racingInstance
   */
  applyRacingConfiguration(racingInstance: any) {
    const racingConfiguration: IInitialSportConfig = racingInstance.getGeneralConfig();

    this.routingHelperService.formSportUrl(this.racingName,
      this.racingName === 'horseracing' ? 'featured' : 'today').subscribe((url: string) => {
        this.racingDefaultPath = url;
    });

    this.racingId = racingConfiguration.config.request.categoryId;

    // Racing tabs information
    this.racingTabs = racingInstance.configureTabs(this.racingName, racingConfiguration.tabs, this.isSpecialsPresent);

    this.routesDataSharingService.setRacingTabs(this.racingName, this.racingTabs);

    this.activeTab = {
      id: racingInstance.getGeneralConfig().tabs[0].id
    };
  }

  /**
   * To show the meetings list and to log click event
   */
  showMeetingsList(): void {
    this.showMeetings = !this.showMeetings;
    this.gtmService.push('trackEvent', {
      eventCategory: 'horse racing',
      eventAction: 'race card',
      eventLabel: 'meetings'
    });

    this.windowRefService.nativeWindow.scrollTo(0, 0);
  }

  /**
   * Check if it horse rading details page
   */
  protected isHorseRacingDetailPage(): boolean {
    const currentSegment = this.routingState.getCurrentSegment();
    const isHorseRacingEDP = currentSegment === 'horseracing.eventMain' ||
      currentSegment === 'horseracing.eventMain.market' ||
      currentSegment === 'horseracing.eventMain.market.marketType';
    this.topBarIndex = isHorseRacingEDP ? 1003 : 7;
    return isHorseRacingEDP;
  }

  /**
   * Is redirect to racing landing page
   * @returns {Boolean}
   */
  private isRacingLandingPage(): boolean {
    const routeSegment = this.routingState.getCurrentSegment();
    return ['horseracing', 'greyhound'].includes(routeSegment);
  }

  /**
   * To fetch breadcrums data on publish
   */
  private getTopBarData(): void {
    this.isHRDetailPage = this.isHorseRacingDetailPage();
    this.pubSubService.subscribe(this.RACING_MAIN_COMPONENT, 'TOP_BAR_DATA', (topBar: IRacingHeader) => {
      this.breadcrumbsItems = topBar.breadCrumbs;
      this.quickNavigationItems = topBar.quickNavigationItems;
      this.eventEntity = topBar.eventEntity;
      this.meetingsTitle = topBar.meetingsTitle;
      const breadCrumbsLength = this.breadcrumbsItems.length;
      if (breadCrumbsLength && this.isHRDetailPage) {
        const displayName = this.breadcrumbsItems[breadCrumbsLength - 1].name;
        this.breadcrumbsItems[breadCrumbsLength - 1].name = displayName.length > 7 ?
          `${this.breadcrumbsItems[breadCrumbsLength - 1].name.substring(0, 7)}...` : displayName;
      }
    });
  }
}
