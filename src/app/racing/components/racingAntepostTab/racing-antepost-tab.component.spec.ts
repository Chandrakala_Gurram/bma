import { RacingAntepostTabComponent } from './racing-antepost-tab.component';
import { ISportEvent } from '@root/app/core/models/sport-event.model';
import { RACING_CONFIG } from '@root/app/core/services/sport/config/racing.constant';
describe('RacingAntepostTabComponent', () => {
  let component: RacingAntepostTabComponent;

  let filterService,
    locale,
    racingService,
    routingHelperService;

  beforeEach(() => {
    filterService = {
      date: jasmine.createSpy()
    };
    locale = {
      getString: jasmine.createSpy().and.returnValue('RACING')
    };
    racingService = {
      ANTEPOST_SWITCHER_KEYS: RACING_CONFIG.ANTEPOST_SWITCHER_KEYS
    };
    routingHelperService = {
      formEdpUrl: jasmine.createSpy('formEdpUrl')
    };
    component = new RacingAntepostTabComponent(
      filterService,
      locale,
      racingService,
      routingHelperService
    );
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      component.switchers = [
        {
          name: 'myTab',
          viewByFilters: 'myFilter'
        },
        {
          name: 'myTab1',
          viewByFilters: 'myFilter1'
        }
      ] as any;
      component.racing = { events: [] };
    });
    it('should set filter if configured in CMS', () => {
      component.defaultAntepostTab = 'myTab1';
      component.racing = {
        events: [{ id: 1, name: 'testName', drilldownTagNames: 'TEST_TAG' }]
      } as any;
      component.ngOnInit();
      expect(component.filter).toBe('myFilter1');
    });
    it('should set first filter if not configured in CMS', () => {
      component.defaultAntepostTab = null;
      component.racing = {
        events: [{ id: 1, name: 'testName', drilldownTagNames: 'TEST_TAG' }]
      } as any;
      component.ngOnInit();
      expect(component.filter).toBe('myFilter');
    });
    it('should not set filter if there are no events', () => {
      spyOn(component, 'getAntepostEventsFlags').and.callThrough();
      spyOn<any>(component, 'setDefaultTab').and.callThrough();

      component.ngOnInit();

      expect(component.getAntepostEventsFlags).not.toHaveBeenCalled();
      expect(component['setDefaultTab']).not.toHaveBeenCalled();
    });
  });
  it('getDate', () => {
    component['getDate'](<any>{ startTime: '1232323525' });
    expect(filterService.date).toHaveBeenCalledWith('1232323525', 'dd-MM-yyyy | HH:mm');
  });


  it('trackById', () => {
    const checkedValue = {id: 5};
    expect(component.trackById(0, checkedValue)).toEqual(5);

    const otherCheckedValue = {typeNameEvents: [{id: 6}]};
    expect(component.trackById(0, otherCheckedValue)).toEqual(6);

  });

  it('formEdpUrl', () => {
    const sportEvent: ISportEvent = {id: 123} as ISportEvent;
    component['formEdpUrl'](sportEvent);

    expect(routingHelperService.formEdpUrl).toHaveBeenCalled();
  });

  it('selectEventList', () => {
    component.selectEventList('test');

    expect(component.filter).toEqual('test');
    expect(component.isExpanded).toEqual([true]);
  });


  it('', () => {
    const events = [
      {
        startTime: '1',
        drilldownTagNames: 'EVFLAG_FT'
      },
      {
        startTime: '2',
        drilldownTagNames: 'EVFLAG_FT'
      },
      {
        startTime: '3',
        drilldownTagNames: 'EVFLAG_FT'
      }
    ];

    component.getAntepostEventsFlags(events as ISportEvent[]);
  });
});
