import { fakeAsync, tick } from '@angular/core/testing';
import { QuickNavigationComponent } from '@racing/components/quickNavigation/quick-navigation.component';

describe('QuickNavigationComponent', () => {
  let component: QuickNavigationComponent;
  let routingHelperService, sortByOptionsService, router, filterService, rendererService, storage, locale;
  let event;
  const meetingsTitleMock = {
    ENHRCS: 'ENHRCS',
    INT: 'INT',
    UK: 'UK',
    VR: 'VR',
    US: 'USA',
    ZA: 'ZA',
    AE: 'AE',
    CL: 'CL',
    IN: 'IN',
    AU: 'AU',
    FR: 'FR',
    ALL: 'ALL'
  };

  beforeEach(() => {
    event = {
      cashoutAvail: 'cashoutAvail',
      categoryCode: 'categoryCode',
      categoryId: 'categoryId',
      categoryName: 'categoryName',
      displayOrder: 12,
      drilldownTagNames: 'drilldownTagNames',
      eventIsLive: false,
      eventSortCode: 'eventSortCode',
      eventStatusCode: 'eventStatusCode',
      name: 'name',
      id: 123,
      isExtraPlaceOffer: false
    } as any;

    router = {
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    };

    routingHelperService = {
      formEdpUrl: jasmine.createSpy('formEdpUrl').and.returnValue('url')
    };

    sortByOptionsService = {
      set: jasmine.createSpy('set'),
    };
    filterService = {};
    rendererService = {
      renderer: {
        addClass: jasmine.createSpy(),
        removeClass: jasmine.createSpy()
      }
    };

    locale = {
      getString: jasmine.createSpy('locale.getString').and.returnValue('Test')
    };

    storage = {
      get: jasmine.createSpy('storage.get')
    };

    component = new QuickNavigationComponent(
      filterService, routingHelperService, sortByOptionsService, router, rendererService, storage, locale
    );
    component.items = [];
  });

  it('should create QuickNavigationComponent instance', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('filter virtulas', () => {
      component.meetingsTitle = meetingsTitleMock;
      component.items = [
        {flag: 'UK', data: []},
        {flag: 'INT', data: []},
        {flag: 'VR', data: []}
      ];

      storage.get.and.returnValue({UIR: 'UK', IR: 'INT'});
      component.ngOnInit();

      expect(component.items).toEqual([
        {flag: 'UK', data: []},
        {flag: 'INT', data: []}
      ]);
    });

    it('filter international', () => {
      component.meetingsTitle = meetingsTitleMock;
      component.items = [
        {flag: 'UK', data: []},
        {flag: 'INT', data: []},
        {flag: 'US', data: []},
        {flag: 'FR', data: []},
        {flag: 'VR', data: []}
      ];

      storage.get.and.returnValue({UIR: 'UK', LVR: 'VR'});
      component.ngOnInit();

      expect(component.items).toEqual([
        {flag: 'UK', data: []},
        {flag: 'VR', data: []}
      ]);
    });

    it('filter UK', () => {
      component.meetingsTitle = meetingsTitleMock;
      component.items = [
        {flag: 'UK', data: []},
        {flag: 'INT', data: []},
        {flag: 'VR', data: []}
      ];

      storage.get.and.returnValue({IR: 'INT', LVR: 'VR'});
      component.ngOnInit();

      expect(component.items).toEqual([
        {flag: 'INT', data: []},
        {flag: 'VR', data: []}
      ]);
    });

    it('not filtered', () => {
      component.meetingsTitle = meetingsTitleMock;
      component.items = [
        {flag: 'UK', data: []},
        {flag: 'INT', data: []},
        {flag: 'VR', data: []}
      ];

      storage.get.and.returnValue(null);
      component.ngOnInit();

      expect(component.items).toEqual([
        {flag: 'UK', data: []},
        {flag: 'INT', data: []},
        {flag: 'VR', data: []}
      ]);

      storage.get.and.returnValue({UIR: 'UK', LVR: 'VR', IR: 'INT'});
      component.ngOnInit();

      expect(component.items).toEqual([
        {flag: 'UK', data: []},
        {flag: 'INT', data: []},
        {flag: 'VR', data: []}
      ]);
    });
  });

  describe('#ngOnChanges', () => {
    it('should add class to body', () => {
      component.meetingsTitle = {};
      storage.get.and.returnValue({UIR: 'UK', IR: 'INT'});
      const changes: any = {
        showMenu: {
          currentValue: 1
        }
      };
      component.ngOnChanges(changes);

      expect(component['rendererService']['renderer']['addClass']).toHaveBeenCalled();
    });

    it('should remove class from body', fakeAsync(() => {
      component.meetingsTitle = {};
      storage.get.and.returnValue({UIR: 'UK', IR: 'INT'});
      const changes: any = {
        showMenu: 0
      };
      component.ngOnChanges(changes);
      tick(300);

      expect(component['rendererService']['renderer']['removeClass']).toHaveBeenCalled();
    }));
  });

  it('#closeMenu', () => {
    component.showMeetingsListFn.emit = jasmine.createSpy();

    component.closeMenu();
    expect(component.showMeetingsListFn.emit).toHaveBeenCalled();
  });

  it('#trackById', () => {
    expect(component.trackById(null, { id: 123 })).toBe(123);
  });

  describe('#selectEvent', () => {
    it('navigateByUrl should be called', () => {
      component.eventEntity = {
        name: 'name2',
        id: 321
      } as any;
      component.selectEvent(event);

      expect(sortByOptionsService.set).toHaveBeenCalledWith('Price');
      expect(routingHelperService.formEdpUrl).toHaveBeenCalled();
      expect(router.navigateByUrl).toHaveBeenCalledWith('url');
    });

    it('closeMenu should be called', () => {
      component.closeMenu = jasmine.createSpy();
      component.eventEntity = {
        name: 'name',
        id: 123
      } as any;
      component.selectEvent(event);

      expect(component.closeMenu).toHaveBeenCalled();
    });

    it('closeMenu should be called', () => {
      component.closeMenu = jasmine.createSpy();
      event.isExtraPlaceOffer = true;
      component.eventEntity = {
        name: 'name',
        id: 123,
        isExtraPlaceOffer: true
      } as any;
      component.selectEvent(event);

      expect(component.closeMenu).toHaveBeenCalled();
    });
    it('closeMenu should be called(Scenario 2)', () => {
      const event2 = {
        cashoutAvail: 'cashoutAvail',
        categoryCode: 'categoryCode',
        categoryId: 'categoryId',
        categoryName: 'categoryName',
        displayOrder: 12,
        drilldownTagNames: 'drilldownTagNames',
        eventIsLive: false,
        eventSortCode: 'eventSortCode',
        eventStatusCode: 'eventStatusCode',
        name: 'name',
        id: 456,
        isExtraPlaceOffer: false
      } as any;
      component.closeMenu = jasmine.createSpy();
      event.isExtraPlaceOffer = true;
      component.eventEntity = {
        name: 'name',
        id: 123,
        isExtraPlaceOffer: true
      } as any;
      component.selectEvent(event2);
      expect(component.closeMenu).toHaveBeenCalled();
    });
  });

  describe('#getLink', () => {
    it('should return a link', () => {
      component.items = [
        {
          flag: 'UK',
          data: [
            {
              meeting: 'meeting',
              events: event
            }
          ]
        }
      ];
      const result = component.getLink(event);

      expect(result).toBe('url');
    });
    it('should not return a link', () => {
      component.items = null;
      const result = component.getLink(event);

      expect(result).toBe('');
    });
  });

  describe('#isActiveLink', () => {
    it('isActiveLink should return true', () => {
      component.eventEntity = {
        name: 'name'
      } as any;
      event.isExtraPlaceOffer = false;
      const result = component.isActiveLink(event);

      expect(result).toBeTruthy();
    });

    it('isActiveLink should return false', () => {
      component.eventEntity = {
        name: 'name2'
      } as any;
      event.isExtraPlaceOffer = true;
      const result = component.isActiveLink(event);

      expect(result).toBeFalsy();
    });
  });
});
