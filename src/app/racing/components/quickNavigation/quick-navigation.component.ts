import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { IGroupedSportEvent, ISportEvent } from '@core/models/sport-event.model';
import { FiltersService } from '@core/services/filters/filters.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { SortByOptionsService } from '@racing/services/sortByOptions/sort-by-options.service';
import { Router } from '@angular/router';
import { RendererService } from '@shared/services/renderer/renderer.service';

import { StorageService } from '@core/services/storage/storage.service';
import { LocaleService } from '@core/services/locale/locale.service';

@Component({
  templateUrl: './quick-navigation.component.html',
  selector: 'quick-navigation',
  styleUrls: ['./quick-navigation.component.less']
})
export class QuickNavigationComponent implements OnChanges, OnInit {
  @Input() items: IGroupedSportEvent[];
  @Input() showMenu: boolean;
  @Output() readonly showMeetingsListFn: EventEmitter<void> = new EventEmitter();
  @Input() meetingsTitle: any;
  @Input() eventEntity: ISportEvent;

  private flagsMap: {[key: string]: string} = {
    'UK': 'UIR' ,
    'INT': 'IR',
    'VR': 'LVR'
  };

  private titlesMap: {[key: string]: string} = {};

  constructor(
    protected filterService: FiltersService,
    private routingHelperService: RoutingHelperService,
    private sortByOptionsService: SortByOptionsService,
    private router: Router,
    private rendererService: RendererService,
    private storage: StorageService,
    private locale: LocaleService
  ) {}

  ngOnInit() {
    const titles: {[key: string]: string} = this.storage.get('racingFeatured');

    this.filterNavItems(titles);
    this.setNavTitles(titles);
  }

  /**
   * Handles selection of new racing event by redirecting to event's page.
   */
  selectEvent(event: ISportEvent): void {
    if (event.isExtraPlaceOffer && event.id === this.eventEntity.id
      || !event.isExtraPlaceOffer && this.eventEntity.name === event.name) {
      this.closeMenu();
      return;
    }

    const url = this.routingHelperService.formEdpUrl(event);
    this.sortByOptionsService.set('Price');

    this.router.navigateByUrl(url);
    this.closeMenu();
  }

  getLink(event: ISportEvent): string {
    if (!this.items) {
      return '';
    }
    return this.routingHelperService.formEdpUrl(event);
  }

  /**
   * ngFor trackBy function
   * @param {number} index
   * @param {object} value
   * @return {number}
   */
  trackById(index: number, value: { id: number }): number {
    return value.id;
  }

  isActiveLink(event: ISportEvent): boolean {
    return event.name === this.eventEntity.name && !event.isExtraPlaceOffer;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.showMenu && changes.showMenu.currentValue) {
      this.rendererService.renderer.addClass(document.body, 'menu-opened');
    } else {
      setTimeout(() => { // set timeout duration the same as animation duration in css
        this.rendererService.renderer.removeClass(document.body, 'menu-opened');
      }, 300);
    }
  }

  closeMenu() {
    this.showMeetingsListFn.emit();
  }

  private setNavTitles(titles: {[key: string]: string}): void {
    Object.keys(this.meetingsTitle).map((flag) => {
      if (titles && titles[this.flagsMap[flag]]) {
        this.titlesMap[flag] = titles[this.flagsMap[flag]];
      } else {
        this.titlesMap[flag] = this.locale.getString(this.meetingsTitle[flag]);
      }
    });
  }

  private filterNavItems(titles: {[key: string]: string}): void {
    if (titles) {
      if (!Object.keys(titles).includes('IR')) {
        this.items = this.items.filter(this.filterInternational);
      } else if (!Object.keys(titles).includes('UIR')) {
        this.items = this.items.filter(item => item.flag !== 'UK');
      } else if (!Object.keys(titles).includes('LVR')) {
        this.items = this.items.filter(item => item.flag !== 'VR');
      }
    }
  }

  private filterInternational(races: IGroupedSportEvent): boolean {
    return races.flag === 'UK' || races.flag === 'VR' || races.flag === 'ENHRCS';
  }
}
