import { Component, Input, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { from, Subscription } from 'rxjs';
import { GtmService } from '@core/services/gtm/gtm.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { ExtraPlaceService } from '@core/services/racing/extraPlace/extra-place.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { ISportEvent } from '@core/models/sport-event.model';
import { IOffersRacingEvent, IOffersRacingGroup } from './offers-and-featured-races.model';

@Component({
  selector: 'offers-and-featured-races',
  templateUrl: 'offers-and-featured-races.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OffersAndFeaturedRacesComponent implements OnInit, OnDestroy {
  @Input() sectionTitle: string;
  @Input() events: ISportEvent[];

  slidesOnPage: number = 1;
  groupedEvents: IOffersRacingGroup = {};
  allEvents: ISportEvent[] = [];
  subscribedChannelsId: string = '';
  readonly raceKeys: string[] = ['itv', 'epr'];
  private readonly tagName: string = 'offers-and-featured-races';
  private loadDataSubscription: Subscription;
  private isFirstTimeCollapsed: boolean = false;

  constructor(
    private extraPlaceService: ExtraPlaceService,
    private gtmService: GtmService,
    private routingHelperService: RoutingHelperService,
    private router: Router,
    private localeService: LocaleService,
    private pubSubService: PubSubService,
    private changeDetectorRef: ChangeDetectorRef) {}

  ngOnInit(): void {
    if (this.events && this.events.length) {
      this.allEvents = this.filterEvents(this.events);
      this.setGroupedEvents(this.allEvents);
    } else {
      this.loadDataSubscription = from(this.extraPlaceService.getEvents()).subscribe((events: ISportEvent[]) => {
        this.allEvents = events;
        this.setGroupedEvents(this.allEvents);
      }, () => {
        this.allEvents = [];
        this.groupedEvents = {};
        this.changeDetectorRef.markForCheck();
      });
    }

    this.pubSubService.subscribe(this.tagName,
      [
        this.pubSubService.API.DELETE_EVENT_FROM_CACHE,
        this.pubSubService.API.EXTRA_PLACE_RACE_OFF
      ], (eventId: number) => {
        this.updateEvents(eventId);
      });
  }

  ngOnDestroy(): void {
    this.loadDataSubscription && this.loadDataSubscription.unsubscribe();
    this.extraPlaceService.unSubscribeForUpdates(this.subscribedChannelsId);
    this.pubSubService.unsubscribe(this.tagName);
  }

  /**
   * ngFor trackBy function
   * @param {number} index
   * @param {ISportEvent} event
   * @return {string}
   */
  trackById(index: number, event: ISportEvent): string {
    return event.id ? `${index}${event.id}` : index.toString();
  }

  /**
   * Send GA on first collapse
   */
  sendCollapseGTM(): void {
    if (!this.isFirstTimeCollapsed) {
      this.gtmService.push('trackEvent', this.extraPlaceService.gtmObject);
      this.isFirstTimeCollapsed = true;
    }
  }

  /**
   * Go to the sport event
   */
  goToEvent(event: ISportEvent): void {
    const url = this.formEdpUrl(event);
    this.extraPlaceService.sendGTM(event.originalName);
    this.router.navigateByUrl(url);
  }

  /**
   * Set Grouped Racing Events
   */
  private setGroupedEvents(events: ISportEvent[]): void {
    this.groupedEvents = this.getRacingEvents(events);
    this.subscribedChannelsId = this.extraPlaceService.subscribeForUpdates(events);
    this.changeDetectorRef.markForCheck();
  }

  /**
   * Filter needed events
   * @param events
   */
  private filterEvents(events: ISportEvent[]): ISportEvent[] {
    return events.filter((event: ISportEvent) => {
      // Event is Open (not finished or resulted, not live event and does not have raceStage)
      const isOpen = event.raceStage !== 'O' && event.rawIsOffCode !== 'Y' && !event.isFinished && !event.isResulted;
      return event.markets.length && isOpen && (this.isEPR(event) || this.isITV(event));
    });
  }

  /**
   * Get Grouped Racing Events by drilldownTagNames
   * @param events
   */
  private getRacingEvents(events: ISportEvent[]): IOffersRacingGroup {
    events.sort((a: ISportEvent, b: ISportEvent) => (Number(a.startTime) - Number(b.startTime)))
      .forEach((event: IOffersRacingEvent) => {
        event.link = this.formEdpUrl(event);
        if (this.isOddsAvailable(event)) {
          event.odds = this.getOdds(event);
        }
    });
    return {
      itv: {
        title: this.localeService.getString('racing.itv'),
        name: this.localeService.getString('racing.itv'),
        svgId: '#itv',
        events: events.filter(event => this.isITV(event))
      },
      epr: {
        title: this.localeService.getString('racing.extraPlaceTitle'),
        name: this.localeService.getString('racing.extraPlace'),
        svgId: '#extra-place',
        events: events.filter(event => this.isEPR(event))
      }
    };
  }

  /**
   * Extra Place Event (Market Level)
   * @param event
   */
  private isEPR(event: ISportEvent): boolean {
    return event.markets[0].drilldownTagNames && event.markets[0].drilldownTagNames.includes('MKTFLAG_EPR');
  }

  /**
   * ITV Event (Event Level)
   * @param event
   */
  private isITV(event: ISportEvent): boolean {
    return event.drilldownTagNames && event.drilldownTagNames.includes('EVFLAG_FRT');
  }

  /**
   * @param {object} event
   * Get odds for extra place event
   * @returns {string}
   */
  private getOdds(event: ISportEvent): string {
    return `${event.markets[0].eachWayFactorNum}/${event.markets[0].eachWayFactorDen} the Odds ` +
      `${this.getPlaces(event.markets[0].eachWayPlaces)}` +
      `<b>${event.markets[0].eachWayPlaces}</b>`;
  }

  /**
   * @param {string} places
   * Get race places for extra place event
   * @returns string
   */
  private getPlaces(places: string): string {
    const length = +places || 0;
    let str = '';
    for (let i = 1; i < length; i++) {
      str += `${i}-`;
    }
    return str;
  }

  /**
   * @param {object} event
   * Checking valid odds for event
   * @returns boolean
   */
  private isOddsAvailable(event: ISportEvent): boolean {
    return !!(event.markets[0].isEachWayAvailable && event.markets[0].eachWayFactorNum && event.markets[0].eachWayFactorDen);
  }

  /**
   * Forms event details page.
   * @param {Object} event
   * @return {string}
   */
  private formEdpUrl(event: ISportEvent): string {
    return this.routingHelperService.formEdpUrl(event);
  }

  /**
   * Update Events
   * @param eventId
   */
  private updateEvents(eventId: number): void {
    this.raceKeys.forEach((key: string) => {
      if (this.groupedEvents[key]) {
        this.groupedEvents[key].events = this.groupedEvents[key].events.filter(item => item.id !== eventId);
      }
    });
    this.allEvents = this.allEvents.filter(item => item.id !== eventId);
    this.changeDetectorRef.markForCheck();
  }
}
