import { GreyhoundFutureTabComponent } from '@racing/components/greyhound/greyhoundFutureTab/greyhound-future-tab.component';

describe('GreyhoundFutureTabComponent', () => {
  let component: GreyhoundFutureTabComponent;
  let filterService, routingHelperService, eventService;

  beforeEach(() => {
    filterService = {
      date: jasmine.createSpy()
    };

    routingHelperService = {
      formSportUrl: jasmine.createSpy(),
      formEdpUrl: jasmine.createSpy()
    };
    eventService = {
      isAnyCashoutAvailable: jasmine.createSpy()
    };

    component = new GreyhoundFutureTabComponent(filterService, eventService, routingHelperService);
    component.filter = 'by-time';
    component.orderedEvents = [];
    component.orderedEventsByTypeNames = [];
    component.filteredTypeNames = [];
    component.racingEvents = [];
    component.isExpanded = true;
  });

  it('ngOnInit', () => {
    component.orderedEvents = [{ link: '', date: ''}] as any;
    component['formEdpUrl'] = jasmine.createSpy('formEdpUrl').and.returnValue('edpUrl');
    component['filteredTime'] = jasmine.createSpy('filteredTime').and.returnValue('filteredTime');

    component.ngOnInit();
    expect((component.orderedEvents[0] as any).link).toEqual('edpUrl');
    expect((component.orderedEvents[0] as any).date).toEqual('filteredTime');
  });

  it('should get filteredTime from filterservice', () => {
    const event = { startTime: '1232323525' } as any;
    component['filteredTime'](event);

    expect(filterService.date).toHaveBeenCalledWith(event.startTime, 'dd-MM-yyyy');
  });

  it('should get CacheOut', () => {
    component.checkCacheOut([], 'typeNameMock');

    expect(eventService.isAnyCashoutAvailable).toHaveBeenCalledWith([], [{ cashoutAvail: 'Y' }]);
  });

  it('should test trackById with id', () => {
    const result = component.trackById(1, {
      id: 111
    });

    expect(result).toEqual(111);
  });

  it('should test trackById with groupFlag', () => {
    const result = component.trackById(1, {
      groupFlag: 22
    });

    expect(result).toEqual(22);
  });

  it('should build URL from Event object', () => {
    const eventMock: any = {};

    component['formEdpUrl'](eventMock);

    expect(routingHelperService.formEdpUrl).toHaveBeenCalledWith(eventMock);
  });
});
