import { Component, Input, OnInit } from '@angular/core';
import { FiltersService } from '@core/services/filters/filters.service';
import { ISportEvent } from '@core/models/sport-event.model';
import * as _ from 'underscore';
import { EventService } from '@sb/services/event/event.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { IFutureEvent } from '@racing/models/racing-ga.model';

@Component({
  selector: 'greyhound-future-tab',
  styleUrls: ['greyhound-future-tab.less'],
  templateUrl: 'greyhound-future-tab.component.html'
})

export class GreyhoundFutureTabComponent implements OnInit {
  @Input() filter: string;
  @Input() orderedEvents: ISportEvent[];
  @Input() orderedEventsByTypeNames: ISportEvent[];
  @Input() filteredTypeNames: string[];
  @Input() racingEvents: ISportEvent[];
  @Input() isExpanded: boolean;

  limit: number;

  constructor(
    public filterService: FiltersService,
    public eventService: EventService,
    public routingHelperService: RoutingHelperService
  ) {}

  ngOnInit(): void {
    this.orderedEvents.forEach((event: IFutureEvent) => {
      event.link = this.formEdpUrl(event);
      event.date = this.filteredTime(event);
    });
  }

  checkCacheOut(events: ISportEvent[], typeName: string): boolean {
    const filteredEvents = _.filter(events, (event: ISportEvent) => event.typeName === typeName);

    return this.eventService.isAnyCashoutAvailable(filteredEvents, [{ cashoutAvail: 'Y' }]);
  }

  trackById(index, value): number {
    return value.id ? value.id : value.groupFlag;
  }

  private filteredTime(event: ISportEvent): string {
    return this.filterService.date(event.startTime, 'dd-MM-yyyy');
  }

  /**
   * Forms event details page.
   * @param {Object} event
   * @return {string}
   */
  private formEdpUrl(event: ISportEvent): string {
    return this.routingHelperService.formEdpUrl(event);
  }
}
