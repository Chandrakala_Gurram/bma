import { DailyRacingModuleComponent } from '@racing/components/dailyRacing/daily-racing.component';

describe('DailyRacingModuleComponent', () => {
  let component: DailyRacingModuleComponent;
  let routingHelperService,
    filterService;

  beforeEach(() => {
    routingHelperService = {
      formEdpUrl: jasmine.createSpy()
    };
    filterService = {
      orderBy: jasmine.createSpy().and.callFake((...args) => args[0])
    };

    component = new DailyRacingModuleComponent(routingHelperService, filterService);
  });

  it('should test ngOnInit', () => {
    const resultMock = [{
      sectionName: 'section1',
      events: ['1']
    }];
    component.eventsBySections = {
      section1: ['1']
    } as any;
    component.ngOnInit();
    expect(filterService.orderBy).toHaveBeenCalledWith(['1'], ['startTime', 'name']);
    expect(component.filteredEventsBySection).toEqual(resultMock as any);
  });

  it('should test formEdpUrl', () => {
    component.formEdpUrl('test' as any);
    expect(routingHelperService.formEdpUrl).toHaveBeenCalledWith('test');
  });

  it('should test trackByIndex', () => {
    component.formEdpUrl('test' as any);
    expect(component.trackByIndex(1)).toEqual(1);
  });
});
