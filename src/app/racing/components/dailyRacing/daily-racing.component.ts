import { Component, Input, OnInit } from '@angular/core';
import * as _ from 'underscore';

import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { ISportEvent } from '@core/models/sport-event.model';
import { FiltersService } from '@core/services/filters/filters.service';

@Component({
  selector: 'daily-racing-module',
  templateUrl: 'daily-racing.component.html'
})
export class DailyRacingModuleComponent implements OnInit {
  @Input() eventsBySections: {[key: string]: ISportEvent[]};
  @Input() collapsedSections?: {[key: string]: ISportEvent[]};
  @Input() sportName: string;

  filteredEventsBySection: {sectionName: string; events: ISportEvent[]}[];

  constructor(
    private routingHelperService: RoutingHelperService,
    private filterService: FiltersService) {}

  ngOnInit(): void {
    this.filteredEventsBySection = _.map(this.eventsBySections, (val: ISportEvent[], key: string) => {
      return { sectionName: key, events: this.filterService.orderBy(val, ['startTime', 'name'])};
    });
  }

  formEdpUrl(eventEntity: ISportEvent): string {
    return this.routingHelperService.formEdpUrl(eventEntity);
  }

  trackByIndex(index: number): number {
    return index;
  }
}
