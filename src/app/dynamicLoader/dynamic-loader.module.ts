import { ModuleWithProviders, NgModule } from '@angular/core';
import { ROUTES } from '@angular/router';

import { DynamicLoaderService } from './dynamic-loader.service';
import { DYNAMIC_COMPONENT_MANIFESTS, IDynamicComponentManifest } from './dynamic-loader-manifest';

@NgModule({})
export class DynamicLoaderModule {
  static forRoot(manifests: IDynamicComponentManifest[]): ModuleWithProviders {
    return {
      ngModule: DynamicLoaderModule,
      providers: [
        DynamicLoaderService,
        // provider for Angular CLI to analyze
        { provide: ROUTES, useValue: manifests, multi: true },
        { provide: DYNAMIC_COMPONENT_MANIFESTS, useValue: manifests }
      ],
    };
  }
}
