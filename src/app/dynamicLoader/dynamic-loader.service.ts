import {from as observableFrom,  Observable } from 'rxjs';
import {
  ComponentFactory,
  Inject,
  Injectable,
  InjectionToken,
  Injector,
  NgModuleFactoryLoader,
  NgModuleFactory,
  NgModuleRef,
  QueryList,
  ViewContainerRef
} from '@angular/core';
import * as _ from 'underscore';

import { ILazyComponent } from '@app/dynamicLoader/dynamic-loader.model';

import {
  DYNAMIC_COMPONENT_MANIFESTS,
  IDynamicComponentManifest
} from './dynamic-loader-manifest';

@Injectable()
export class DynamicLoaderService {

  private moduleResolvers = {};

  constructor(
      @Inject(DYNAMIC_COMPONENT_MANIFESTS) private manifests: IDynamicComponentManifest[],
      private loader: NgModuleFactoryLoader,
      private injector: Injector
  ) { }

  /**
   * get object with component factory and injector to be able dynamically create for view template
   */
  getComponentFactory<T>(componentToken: InjectionToken<string>, injector?: Injector): Observable<ComponentFactory<T>> {
    const manifest = this.manifests
        .find(m => m.componentToken === componentToken);

    const p: any = this.loadModule(manifest.loadChildren, injector)
      .then((moduleRef: NgModuleRef<any>) => {
        // Read from the moduleRef injector and locate the dynamic component type
        const dynamicComponentType: any = moduleRef.injector.get(manifest.componentToken);

        // Resolve this component factory
        const componentFac =  moduleRef.componentFactoryResolver.resolveComponentFactory(dynamicComponentType);

        return {
          factory: componentFac,
          injector: moduleRef.injector
        };
      });

    return observableFrom(p);
  }

  /**
   * Load module by given path (same as loadChildren in Router)
   */
  loadModule(modulePath: string, injector?: Injector): Promise<NgModuleRef<any>> {
    const loadedModulePromise = this.moduleResolvers[modulePath];

    if (loadedModulePromise) {
      return loadedModulePromise;
    }

    const p: Promise<NgModuleRef<any>> = this.loader.load(modulePath).then((modFac: NgModuleFactory<any>) => {
      return modFac.create(injector || this.injector);
    });

    this.moduleResolvers[modulePath] = p;

    p.catch(e => {
      delete this.moduleResolvers[modulePath];
      console.error(e);
    });

    return p;
  }

  /**
   * Creates dynamic component
   * NOTE: should be used when view container in parent component may be changed because of any structural directive
   */
  createDynamicComponent(dynamicComp: InjectionToken<string>, lazyComponent: ILazyComponent): void {
    // Lazy load module and get component factory and create then
    if (!lazyComponent.factory) {
      this.getDynamicComponent(dynamicComp, lazyComponent);
      return;
    }

    this.destroyDynamicComponent(lazyComponent);

    lazyComponent.componentRef = lazyComponent.viewContainer
      .createComponent(lazyComponent.factory, 0, lazyComponent.viewContainer.parentInjector);

    this.populateComponentRef(lazyComponent);
  }

  /**
   * Lazy load module, get component factory and create
   * NOTE: should be used when component is initialized once without any structural directives in parent component
   */
  getDynamicComponent(dynamicComp: InjectionToken<string>, lazyComponent: ILazyComponent): void {
    this.getComponentFactory(dynamicComp, lazyComponent.viewContainer.parentInjector)
      .subscribe((componentData: any) => {
        lazyComponent.factory = componentData.factory;
        this.createDynamicComponent(dynamicComp, lazyComponent);
      });
  }

  /**
   * Populate component instance with binding values
   * @param component
   */
  populateComponentRef(component: ILazyComponent): void {
    _.each(component.factory.inputs, (input: any) => {
      const value = component.viewContainer.element.nativeElement[input.templateName];
      if (value !== undefined) {
        component.componentRef.instance[input.propName] = value;
      }
    });
  }

  /**
   * Destroy component reference
   */
  destroyDynamicComponent(lazyComponent: ILazyComponent) {
    lazyComponent.viewContainer && lazyComponent.viewContainer.clear();
    lazyComponent.componentRef && lazyComponent.componentRef.destroy();
  }

  /**
   * Create multiple instances of dynamic component
   * @param componentToken
   * @param containers
   */
  createInstances(componentToken: InjectionToken<string>, containers: QueryList<ViewContainerRef>, keys?: string[]): ILazyComponent[] {
    const result: ILazyComponent[] = [];
    let element;
    if (containers.length) {
      this.getComponentFactory(componentToken, containers.first.parentInjector).subscribe((componentData: any) => {
        containers.forEach((container: ViewContainerRef) => {
          container.clear();

          const component: ILazyComponent = {
            viewContainer: container,
            factory: componentData.factory,
            componentRef: container.createComponent(componentData.factory, 0, componentData.injector),
          };

          if (keys) {
            element = container.element.nativeElement;

            _.each(keys, (key: string) => {
              if (_.isFunction(element[key])) {
                component.componentRef.instance[key].subscribe((...args) => element[key](...args));
              } else {
                component.componentRef.instance[key] = element[key];
              }
            });
          }

          this.populateComponentRef(component);
          result.push(component);
        });
      });
    }
    return result;
  }

  /**
   * Destroy multiple dynamic components
   * @param components
   */
  destroyInstances(components: ILazyComponent[]): void {
    if (!components) {
      return;
    }

    _.each(components, component => {
      component.componentRef && component.componentRef.destroy();
    });
  }
}
