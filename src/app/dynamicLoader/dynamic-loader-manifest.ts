import { InjectionToken } from '@angular/core';

export interface IDynamicComponentManifest {
  path: string;
  loadChildren: string;
  componentToken: InjectionToken<string>;
  componentName: string;
}

export const DYNAMIC_COMPONENT_MANIFESTS = new InjectionToken<string>('DYNAMIC_COMPONENT_MANIFESTS');

/**
 * Note: Each dynamically loaded component should have unique injection token to
 * be able resolved after lazy loaded module factory
 */

// Betslip
export const DYNAMIC_BET_HISTORY_PAGE = new InjectionToken<string>('DYNAMIC_BET_HISTORY_PAGE');
export const DYNAMIC_MY_BETS = new InjectionToken<string>('DYNAMIC_MY_BETS');
export const DYNAMIC_CASH_OUT_PAGE = new InjectionToken<string>('DYNAMIC_CASH_OUT_PAGE');
export const DYNAMIC_OPEN_BETS = new InjectionToken<string>('DYNAMIC_OPEN_BETS');
export const DYNAMIC_IN_SHOP_BETS_PAGE = new InjectionToken<string>('DYNAMIC_IN_SHOP_BETS_PAGE');
export const DYNAMIC_SLIDE_OUT_BETSLIP = new InjectionToken<string>('DYNAMIC_SLIDE_OUT_BETSLIP');

// YourCall
export const DYNAMIC_YOURCALL_TAB = new InjectionToken<any>('DYNAMIC_YOURCALL_TAB');
export const DYNAMIC_YOURCALL_TAB_CONTENT = new InjectionToken<any>('DYNAMIC_YOURCALL_TAB_CONTENT');
export const DYNAMIC_YOURCALL_STATIC_BLOCK = new InjectionToken<any>('DYNAMIC_YOURCALL_STATIC_BLOCK');
export const DYNAMIC_BYB_HOME = new InjectionToken<any>('DYNAMIC_BYB_HOME');
export const DYNAMIC_YC_ICON = new InjectionToken<any>('DYNAMIC_YC_ICON');

// Odds Boost
export const DYNAMIC_ODDS_BOOST_BS_HEADER = new InjectionToken<string>('DYNAMIC_ODDS_BOOST_BS_HEADER');
export const DYNAMIC_ODDS_BOOST_PRICE = new InjectionToken<string>('DYNAMIC_ODDS_BOOST_PRICE');
export const DYNAMIC_ODDS_BOOST_BUTTON = new InjectionToken<string>('DYNAMIC_ODDS_BOOST_BUTTON');
export const DYNAMIC_ODDS_BOOST_INFO_ICON = new InjectionToken<string>('DYNAMIC_ODDS_BOOST_INFO_ICON');
export const DYNAMIC_ODDS_BOOST_NOTIFICATION = new InjectionToken<string>('DYNAMIC_ODDS_BOOST_NOTIFICATION');

// Retail
export const DYNAMIC_RETAIL_OVERLAY = new InjectionToken<string>('DYNAMIC_RETAIL_OVERLAY');
export const DYNAMIC_BET_TRACKER = new InjectionToken<string>('DYNAMIC_BET_TRACKER');

// Racing
export const DYNAMIC_RACING_EVENT = new InjectionToken<string>('DYNAMIC_RACING_EVENT');
