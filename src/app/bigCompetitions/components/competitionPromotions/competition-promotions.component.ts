import { Component, Input, OnInit } from '@angular/core';

import { ICompetitionModules } from '@app/bigCompetitions/services/bigCompetitions/big-competitions.model';
import { IPromotion } from '@core/services/cms/models/promotion/promotion.model';

@Component({
  selector: 'competition-promotions',
  templateUrl: './competition-promotions.html'
})
export class CompetitionPromotionsComponent implements OnInit {

  @Input() moduleConfig: ICompetitionModules;

  promotions: IPromotion[];

  ngOnInit(): void {
    this.promotions = (this.moduleConfig.promotionsData && this.moduleConfig.promotionsData.promotions) || [];
  }
}
