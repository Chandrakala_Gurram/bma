import { Component, Input, OnInit } from '@angular/core';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { IBCModule } from '../../services/bigCompetitions/big-competitions.model';

@Component({
  selector: 'competition-aem',
  templateUrl: 'competition-aem.component.html'
})
export class CompetitionAemComponent implements OnInit {
  @Input() moduleConfig: IBCModule;

  transition: number;

  constructor(private cmsService: CmsService) {}

  ngOnInit(): void {
    this.cmsService.getSystemConfig().subscribe(data => {
      this.transition = data.Banners && +data.Banners.transitionDelay;
    });
  }
}
