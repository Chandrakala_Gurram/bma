import { CompetitionAemComponent } from '@app/bigCompetitions/components/competitionAem/competition-aem.component';
import { of } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';


describe('CompetitionAemComponent', () => {
  let component: CompetitionAemComponent;
  let cmsService;

  beforeEach(() => {
    cmsService = {
      getSystemConfig: jasmine.createSpy('getSystemConfig').and.returnValue(of({
        Banners: {
          transitionDelay: '1000'
        }
      }))
    };

    component = new CompetitionAemComponent(cmsService);
  });

  it('constructor', () => {
    expect(component).toBeTruthy();
    expect(component.transition).toBeUndefined();
  });

  it('ngOnInit', fakeAsync(() => {
    component.ngOnInit();
    tick();
    expect(cmsService.getSystemConfig).toHaveBeenCalled();
    expect(component.transition).toEqual(1000);
  }));
});
