import { finalize, flatMap } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { IBCData } from '@app/bigCompetitions/services/bigCompetitions/big-competitions.model';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import {
  BigCompetitionsLiveUpdatesService
} from '@app/bigCompetitions/services/bigCompetitionsLiveUpdates/big-competitions-live-updates.service';
import { BigCompetitionsService } from '@app/bigCompetitions/services/bigCompetitions/big-competitions.service';
import { ParticipantsService } from '@app/bigCompetitions/services/participants/participants.service';

import { EVENTS } from '@core/constants/websocket-events.constant';
import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { RoutingState } from '@sharedModule/services/routingState/routing-state.service';
import { UpdateEventService } from '@core/services/updateEvent/update-event.service';

@Component({
  selector: 'big-competition',
  templateUrl: 'big-competition.html'
})
export class BigCompetitionComponent extends AbstractOutletComponent implements OnInit, OnDestroy {

  activeTab: { id: string; };
  participantsFlags: string;
  competitionName: string;
  competitionTabs: any;
  subscriptionName: string;
  competition: IBCData;

  private routeUrlSubscription: Subscription;

  constructor(
    private competitionsService: BigCompetitionsService,
    private participantsService: ParticipantsService,
    private pubsubService: PubSubService,
    private liveUpdatesService: BigCompetitionsLiveUpdatesService,
    private routingState: RoutingState,
    private route: ActivatedRoute,
    private router: Router,
    // tslint:disable-next-line
    private updateEventService: UpdateEventService // for events subscription (done in service init)
   ) {
    super();
  }

  /*
   * Init function.
   */
  ngOnInit(): void {
    this.routeUrlSubscription = this.route.url.pipe(
      flatMap(() => {
        const competitionName = this.route.snapshot.paramMap.get('name');
        return this.competitionsService.getTabs(competitionName).pipe(
          finalize(() => {
            this.hideSpinner();
            this.subscribe();
          }));
      })).subscribe((competition: IBCData) => {
        this.competition = competition;

        this.competitionName = this.competition.name;
        this.competitionTabs = this.competition.competitionTabs;
        this.competitionsService.storeCategoryId(this.competition.categoryId.toString());
        this.setActiveTab();

        this.participantsService.store(this.competition.competitionParticipants || []);
        this.participantsFlags = this.participantsService.getFlagsList();
      }, () => {
        this.showError();
      });
  }

  ngOnDestroy(): void {
    this.pubsubService.unsubscribe(this.subscriptionName);
    this.routeUrlSubscription.unsubscribe();
  }

  /**
   * Subscribes for pubsub and router events.
   * @private
   */
  subscribe(): void {
    this.subscriptionName = 'BigCompetitionCtrl';
    const reloadEvents: Array<string> = [`liveServe.${EVENTS.SOCKET_RECONNECT_SUCCESS}`, this.pubsubService.API.RELOAD_COMPONENTS];

    this.pubsubService.subscribe(this.subscriptionName, reloadEvents, () => {
      this.reloadComponent();
    });
  }

  /*
   * Set active tab, and compare route with data, for proper redirections.
   * @param {Object} params - route segment params
   */
  setActiveTab(): void {
    const tabName = this.routingState.getRouteParam('tab', this.route.snapshot),
      tab = this.competitionsService.findTab();

    if (!tab) {
      return;
    }

    // Needs for tabs panel component
    this.activeTab = { id: tab.id };

    // if tab not specified we should redirect to the tab
    if (!tabName) {
      this.router.navigateByUrl(tab.path);
    }
  }

  /**
   * Reloads component when LiveServe connetction is reestablised.
   * @protected
   */
  reloadComponent(): void {
    this.liveUpdatesService.reconnect().subscribe(() => {
      super.reloadComponent();
    });
  }
}
