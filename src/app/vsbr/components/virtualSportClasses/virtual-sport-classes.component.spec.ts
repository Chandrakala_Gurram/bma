import { of as observableOf, Subscription, throwError } from 'rxjs';
import { fakeAsync, flush, tick } from '@angular/core/testing';

import { VirtualSportClassesComponent } from '@app/vsbr/components/virtualSportClasses/virtual-sport-classes.component';
import { ISportEventEntity } from '@core/models/sport-event-entity.model';

describe('VirtualSportClassesComponent', () => {
  let component: VirtualSportClassesComponent;

  const menuMock = [
    {
      name: 'football',
      inApp: true,
      svgId: 'svgId',
      targetUri: '/football',
      targetUriSegment: '/footballSegment',
      priority: 1,
      childMenuItems: [],
      label: null,
      displayOrder: 1,
      alias: 'hourse-racing',
      svg: 'svg'
    },
    {
      name: 'hourse',
      inApp: true,
      svgId: 'string',
      targetUri: '/hourse',
      targetUriSegment: '',
      priority: 1,
      childMenuItems: [
        {
          name: 'hourse-test',
          inApp: true,
          svgId: 'string',
          targetUri: '/hourse-test',
          targetUriSegment: '',
          priority: 1,
        }
      ],
      label: null,
      displayOrder: 1,
      alias: 'hourse-racing',
      svg: 'svg',
    }
  ];
  const categoryAliases = {
    parentAlias: 'parentAlias',
    childAlias: 'childAlias'
  };
  const data = [
    {
      id: 2,
      event: {
        startTimeUnix: '123',
        id: 2,
        children: [{
          market: {
            displayOrder: '1'
          }
        }]
      },
      title: 'any'
    },
    {
      id: 1,
      event: {
        startTimeUnix: '123',
        id: 1,
        children: [{
          market: {
            displayOrder: '2'
          }
        }]
      },
      title: 'any2'
    }
  ] as any;

  const filterService = {
    removeLineSymbol: () => '',
    orderBy: jasmine.createSpy('orderBy')
  } as any;
  const activatedRouteStub = {
    params: observableOf({ alias: 'alias' }),
    snapshot: {
      params: {
        alias: 'alias',
        eventId: 1
      }
    }
  } as any;
  const routerStub = {
    navigate: jasmine.createSpy('navigate')
  } as any;
  const windowRefStub = {
    nativeWindow: {
      setInterval: jasmine.createSpy('setInterval').and.callFake((callback) => {
        callback && callback();
      }),
      clearInterval: jasmine.createSpy('clearInterval')
    }
  } as any;

  const changeDetector = {
    detach: jasmine.createSpy('detach'),
    detectChanges: jasmine.createSpy('detectChanges')
  } as any;

  const panelStateStub = {
    getPanelsStates: jasmine.createSpy('getPanelsStates').and.returnValue('open'),
    changeStatePanel: () => {}
  } as any;

  const datePipeStub = {
    transform: jasmine.createSpy('transform')
  } as any;
  const timeServiceStub = {
    goForNextVsEvent: jasmine.createSpy('jasmine.createSpy(')
  } as any;

  const virtualSportsService = {
    subscribeVSBRForUpdates: jasmine.createSpy('subscribeVSBRForUpdates'),
    unSubscribeVSBRForUpdates: jasmine.createSpy('unSubscribeVSBRForUpdates'),
    getAliasesByClassId: jasmine.createSpy().and.returnValue(categoryAliases),
    getActiveClass: jasmine.createSpy().and.returnValue({ class: {} }),
    filterEvents: jasmine.createSpy().and.returnValue(data),
    getEventsWithRacingForms: jasmine.createSpy().and.returnValue(Promise.resolve(data)),
    getMarketSectionsArray: jasmine.createSpy().and.returnValue({ market: { displayOrder: '1' }}),
    genTerms: jasmine.createSpy().and.returnValue('terms'),
    showTerms: jasmine.createSpy().and.returnValue(true),
    getCategoryByAlias: jasmine.createSpy().and.returnValue('16'),
    getAliasByCategory: jasmine.createSpy().and.returnValue('16'),
    unsubscribeFromUpdates: jasmine.createSpy('unsubscribeFromUpdates'),
    normalizeData: jasmine.createSpy('normalizeData').and.returnValue({
      markets: [{
        template: 'WinEw'
      }]
    } as any)
  } as any;

  const deviceServiceStub = {} as any;

  const nativeBridgeStub = {
    onVirtualsSelected: jasmine.createSpy()
  } as any;

  const pubsub = {
    publish: jasmine.createSpy('publish'),
    subscribe: jasmine.createSpy('subscribe').and.callFake((a: string, p: string[] | string, fn: Function) => {
      if (p === 'VS_EVENT_FINISHED') {
        fn({ eventId: 1 });
      } else if (p === 'VIRTUAL_ORIENTATION_CHANGED' ) {
        fn(1);
      } else {
        fn();
      }
    }),
    unsubscribe: jasmine.createSpy(),
    API: {
      VIRTUAL_ORIENTATION_CHANGES: 'VIRTUAL_ORIENTATION_CHANGED',
      VS_EVENT_FINISHED: 'VS_EVENT_FINISHED',
      RELOAD_COMPONENTS: 'RELOAD_COMPONENTS'
    },
  } as any;

  const virtualMenuDataService = {
    setMenu: jasmine.createSpy('getChildMenuItems').and.returnValue(menuMock),
    getChildMenuItems: jasmine.createSpy('getChildMenuItems').and.returnValue(menuMock)
  } as any;

  const vsMapperService = {
    getParentByAlias: jasmine.createSpy('getParentByAlias').and.returnValue({ctaButtonText: 'ctaButtonText', ctaButtonUrl: '/ctaBUrl'})
  } as any;

  beforeEach(() => {
    component = new VirtualSportClassesComponent(windowRefStub, filterService,
      timeServiceStub, datePipeStub, panelStateStub, virtualSportsService, activatedRouteStub,
      routerStub, deviceServiceStub, nativeBridgeStub, pubsub, changeDetector, virtualMenuDataService, vsMapperService);

    component['eventId'] = '1';
    component.activeClass = { class: {} } as any;
    component.events = data;
    component['paramsSubscriber'] = new Subscription();
    component['currentEventIndex'] = 1;
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  describe('@ngOnInit', () => {
    it('should init component, reload if connection is lost, close dialogs', () => {
      component.ngOnInit();
      component.eventsData = [{ event: {} }] as any;
      expect(component.eventsData).toBeTruthy();
    });

    it('should init component data', fakeAsync(() => {
      component['updateFinishedStatus'] = jasmine.createSpy('updateFinishedStatus');
      deviceServiceStub.isWrapper = true;
      component.addChangeDetection = jasmine.createSpy('component');

      component.ngOnInit();
      tick();

      expect(component.addChangeDetection).toHaveBeenCalled();
      expect(component.categoryAlias).toEqual('alias');

      component['prepareEventsForTabs']();
      expect(datePipeStub.transform).toHaveBeenCalledWith('123', 'HH:mm');
      expect(component['currentEventIndex']).toBe(0);

      expect(component['panelsStates']).toEqual('open');
      expect(pubsub.subscribe)
        .toHaveBeenCalledWith('VirtualSportClassesCtrl', 'VS_EVENT_FINISHED', jasmine.any(Function));
      expect(component['requestedEventNotFound']).toBe(false);
      expect(component['updateFinishedStatus']).toHaveBeenCalledWith(1);
      flush();
    }));

    it('should handle error on subscription', fakeAsync(() => {
      virtualSportsService.getEventsWithRacingForms = jasmine.createSpy().and.returnValue(Promise.reject());
      component.ngOnInit();
      tick();
      expect(component['state'].error).toBe(true);
    }));

    it('should init component and route.params should throw error', fakeAsync(() => {
      component.showError = jasmine.createSpy('showError');
      activatedRouteStub.params = throwError('error');
      component.ngOnInit();
      tick(200);
      expect(component.showError).toHaveBeenCalled();
    }));

    it('should set param as virtual-horse-racing', fakeAsync(() => {
      activatedRouteStub.params = observableOf({ eventId: 1 });
      deviceServiceStub.isWrapper = false;
      component.ngOnInit();
      tick();
      expect(virtualSportsService.getEventsWithRacingForms).toHaveBeenCalled();
    }));

    it('should set currentEventIndex > 0', fakeAsync(() => {
      data[0].event.id = 0;
      data[1].event.id = 1;
      activatedRouteStub.snapshot.params.eventId = '1';
      virtualSportsService.getEventsWithRacingForms = jasmine.createSpy().and.returnValue(Promise.resolve(data));
      component.ngOnInit();
      tick();
      expect(component['currentEventIndex']).toBe(0);
      expect(component['requestedEventNotFound']).toBeFalsy();

      component.getEventStartTime(123);
      expect(datePipeStub.transform).toHaveBeenCalled();
    }));

    it('should call ngOnInit() and check currentEventIndex', fakeAsync(() => {
      activatedRouteStub.snapshot.params.eventId = '23';
      component.eventsData = [{ event: {id: '2'} }] as any;
      component.ngOnInit();
      tick();
      expect(component['currentEventIndex']).toBeFalsy();
    }));

    it('should set currentEventIndex === 0', fakeAsync(() => {
      activatedRouteStub.snapshot.params.eventId = undefined;
      deviceServiceStub.isWrapper = true;
      component.eventsData = data;
      component.ngOnInit();
      tick();
      expect(component['currentEventIndex']).toBeFalsy();
    }));

    it('should not navigate if category aliases are equal', fakeAsync(() => {
      pubsub.subscribe.and.callFake((a, p, fn) => {
        if (p === 'VS_EVENT_FINISHED') {
          fn({ eventId: undefined, alias: 16 });
        } else {
          fn();
        }
      });
      component['categoryAlias'] = '16';
      deviceServiceStub.isWrapper = true;
      component.ngOnInit();
      tick();
      expect(nativeBridgeStub.onVirtualsSelected).toHaveBeenCalledWith('16', 1);
    }));

    it('should not navigate to virtual sports if orientation changed', fakeAsync(() => {
      deviceServiceStub.isWrapper = true;
      activatedRouteStub.snapshot.params['alias'] = null;
      virtualSportsService.getAliasesByClassId = jasmine.createSpy().and.returnValue(null);
      component.ngOnInit();
      tick();
      expect(pubsub.subscribe).toHaveBeenCalled();
    }));

    it('should init events subscription for liveserve updates', fakeAsync(() => {
      component.ngOnInit();
      tick();

      expect(virtualSportsService.unSubscribeVSBRForUpdates).toHaveBeenCalled();
      expect(virtualSportsService.subscribeVSBRForUpdates).toHaveBeenCalledWith(data);
    }));
  });

  describe('@ngOnDestroy', () => {
    it('should destroy component, unsync, unsubscribe', () => {
      component['pollingTimer'] = 1;
      component['timeOutListener'] = 2;
      component['paramsSubscriber'] = {
        unsubscribe: jasmine.createSpy('unsubscribe')
      } as any;
      component.ngOnDestroy();
      expect(windowRefStub.nativeWindow.clearInterval).toHaveBeenCalledWith(2);
      expect(windowRefStub.nativeWindow.clearInterval).toHaveBeenCalledWith(1);
      expect(pubsub.unsubscribe).toHaveBeenCalledWith(jasmine.any(String));
      expect(component['paramsSubscriber'].unsubscribe).toHaveBeenCalled();
      expect(virtualSportsService.unSubscribeVSBRForUpdates).toHaveBeenCalled();
    });
  });

  describe('@onTabClick', () => {
    it('should check event tab click callback ', () => {
      deviceServiceStub.isWrapper = true;
      const mockTab = {
        tab: {
          event: {
            id: '111'
          }
        }
      };

      component.categoryId = '290';
      spyOn<any>(component, 'update').and.returnValue(true);

      component.onTabClick(mockTab as any);
      expect(component['nativeBridge'].onVirtualsSelected).toHaveBeenCalledWith('290', '111');
    });

    it('hould check event tab click and currentEventIndex', fakeAsync(() => {
      activatedRouteStub.snapshot.params.eventId = '111';
      component.events = [{ event: {id: '111'} }] as any;
      const mockTab = {
        tab: {
          event: {
            id: '111'
          }
        }
      };
      component.onTabClick(mockTab as any);
      tick();
      expect(component['currentEventIndex']).toBeFalsy();
    }));


    it('should check event tab click callback when deviceServiceStub.isWrapper = false', () => {
      deviceServiceStub.isWrapper = false;
      const mockTabs = {
        tab: {
          event: {
            id: '112'
          }
        }
      };

      component.categoryId = '291';
      spyOn<any>(component, 'update').and.returnValue(true);

      component.onTabClick(mockTabs as any);
      expect(component['nativeBridge'].onVirtualsSelected).not.toHaveBeenCalledWith('291', '112');
    });

  });

  describe('@updateCurrentEvent', () => {
    it('should set currentEvent if events are exist', () => {
      component['setSwitchers'] = jasmine.createSpy('setSwitchers');
      component.currentEventIndex = 0;
      component.events = [{ id: '1'}, { id: '2'}] as any;
      component['updateCurrentEvent']();
      expect(component.currentEvent).toEqual({ id: '1'} as any);
      expect(virtualSportsService.getMarketSectionsArray).toHaveBeenCalledWith({ id: '1'});
      expect(filterService.orderBy).toHaveBeenCalled();
      expect(component['setSwitchers']).toHaveBeenCalled();
    });

    it('should NOT set currentEvent if events are not exist', () => {
      component['setSwitchers'] = jasmine.createSpy('setSwitchers');
      component.currentEventIndex = 1;
      component.events = [] as any;
      component['updateCurrentEvent']();
      expect(component.currentEvent).toEqual(undefined);
      expect(component['setSwitchers']).not.toHaveBeenCalled();
    });
  });

  describe('@switchers', () => {
    it('should check switchers', () => {
      component.currentEvent = <any>{event: {className: 'footballClass'}};
      const event = {
        markets: [{
          template: 'WinEw',
          ncastTypeCodes: 'CF, CT',
          outcomes: [{}, {}, {}]
        }]
      } as any;
      virtualSportsService.normalizeData = jasmine.createSpy('normalizeData').and.returnValue(event);
      component['setSwitchers']();

      component.switchers[0].onClick();
      expect(component.filter).toEqual('winew');

      component.switchers[1].onClick();
      expect(component.filter).toEqual('forecast');

      component.switchers[2].onClick();
      expect(component.filter).toEqual('tricast');
    });
  });

  describe('@setSwitchers', () => {
    it('should set all WinOrEW Markets if they are exist and have outcomes', () => {
      component.currentEvent = <any>{event: {className: 'footballClass'}};
      const event = {
        markets: [{
          template: 'WinEw',
          ncastTypeCodes: 'CF, CT',
          outcomes: [{}, {}, {}]
        }]
      } as any;
      virtualSportsService.normalizeData = jasmine.createSpy('normalizeData').and.returnValue(event);
      component['setSwitchers']();

      expect(component.terms).toEqual('terms');
      expect(component.filter).toEqual('winew');
      expect(component.switchers).toEqual([{
        onClick: jasmine.any(Function),
        viewByFilters: 'winew',
        name: 'Win/Each Way'
      }, {
        onClick: jasmine.any(Function),
        viewByFilters: 'forecast',
        name: 'Forecast'
      }, {
        onClick: jasmine.any(Function),
        viewByFilters: 'tricast',
        name: 'Tricast'
      }]);
    });

    it('should set WinOrEW Markets even if ncastTypeCodes undefined', () => {
      component.currentEvent = <any>{event: {className: 'footballClass'}};
      const event = {
        markets: [{
          template: 'WinEw',
          outcomes: []
        }]
      } as any;
      virtualSportsService.normalizeData = jasmine.createSpy('normalizeData').and.returnValue(event);
      component['setSwitchers']();

      expect(component.filter).toEqual('winew');
      expect(component.hasWinOrEachWay).toEqual(true);
      expect(component.switchers.length).toEqual(1);
    });

    it('should set WinOrEW Market and ForecastMarket if all outcomes are available', () => {
      component.currentEvent = <any>{event: {className: 'footballClass'}};
      const event = {
        markets: [{
          template: 'WinEw',
          ncastTypeCodes: 'CF',
          outcomes: [{}, {}, {}]
        }]
      } as any;
      virtualSportsService.normalizeData = jasmine.createSpy('normalizeData').and.returnValue(event);
      component['setSwitchers']();

      expect(component.filter).toEqual('winew');
      expect(component.switchers).toEqual([{
        onClick: jasmine.any(Function),
        viewByFilters: 'winew',
        name: 'Win/Each Way'
      }, {
        onClick: jasmine.any(Function),
        viewByFilters: 'forecast',
        name: 'Forecast'
      }]);
    });

    it('should set only WinOrEW Market if Forecast Market is exist but outcomes are NOT available', () => {
      component.currentEvent = <any>{event: {className: 'footballClass'}};
      const event = {
        markets: [{
          template: 'WinEw',
          ncastTypeCodes: 'CF',
          outcomes: []
        }]
      } as any;
      virtualSportsService.normalizeData = jasmine.createSpy('normalizeData').and.returnValue(event);
      component['setSwitchers']();

      expect(component.filter).toEqual('winew');
      expect(component.switchers).toEqual([{
        onClick: jasmine.any(Function),
        viewByFilters: 'winew',
        name: 'Win/Each Way'
      }]);
    });

    it('should set WinOrEW Market and TriCast Market if all outcomes are available', () => {
      component.currentEvent = <any>{event: {className: 'footballClass'}};
      const event = {
        markets: [{
          template: 'WinEw',
          ncastTypeCodes: 'CT',
          outcomes: [{}, {}, {}]
        }]
      } as any;
      virtualSportsService.normalizeData = jasmine.createSpy('normalizeData').and.returnValue(event);
      component['setSwitchers']();

      expect(component.filter).toEqual('winew');
      expect(component.switchers).toEqual([{
        onClick: jasmine.any(Function),
        viewByFilters: 'winew',
        name: 'Win/Each Way'
      }, {
        onClick: jasmine.any(Function),
        viewByFilters: 'tricast',
        name: 'Tricast'
      }]);
    });

    it('should set only WinOrEW Market if TriCast Market is exist but outcomes are NOT available', () => {
      component.currentEvent = <any>{event: {className: 'footballClass'}};
      const event = {
        markets: [{
          template: 'WinEw',
          ncastTypeCodes: 'CT',
          outcomes: []
        }]
      } as any;
      virtualSportsService.normalizeData = jasmine.createSpy('normalizeData').and.returnValue(event);
      component['setSwitchers']();

      expect(component.filter).toEqual('winew');
      expect(component.switchers).toEqual([{
        onClick: jasmine.any(Function),
        viewByFilters: 'winew',
        name: 'Win/Each Way'
      }]);
    });

    it('should clear terms value if there is no terms', () => {
      const event = {
        markets: [{
          marketName: 'marketName',
          template: 'Vertical',
          ncastTypeCodes: undefined,
          outcomes: []
        }]
      } as any;
      virtualSportsService.normalizeData = jasmine.createSpy('normalizeData').and.returnValue(event);
      component['setSwitchers']();

      expect(component.terms).toEqual('');
    });
  });

  describe('@update', () => {
    it('should call updateCurrentEvent', () => {
      component['updateCurrentEvent'] = jasmine.createSpy('updateCurrentEvent');

      component['update']();

      expect(component['updateCurrentEvent']).toHaveBeenCalled();
    });
  });

  it('@updateFinishedStatus should update isFinished status', () => {
    component['updateFinishedStatus']('1');
    expect(component.events[1].event.isFinished).toEqual('true');
  });

  it('@addChangeDetection should run change detection', () => {
    component.addChangeDetection();

    expect(changeDetector.detach).toHaveBeenCalled();
    expect(windowRefStub.nativeWindow.setInterval).toHaveBeenCalledWith(jasmine.any(Function), 500);
    expect(changeDetector.detectChanges).toHaveBeenCalled();
  });

  describe('@goToVirtual', () => {
    it('should call goToVirtual()', fakeAsync(() => {
      component.goToCtaUrl();
      expect(routerStub.navigate).toHaveBeenCalled();
    }));

    it('should call goToVirtual() if ctaUrl = undefined', fakeAsync(() => {
      component.ctaUrl = '/football';
      component.goToCtaUrl();
      expect(routerStub.navigate).toHaveBeenCalled();
    }));
  });

  describe('goToNextIfStarted', () => {
    it('should return true', () => {
      component.events = [{
        event: { startTimeUnix: Date.now() / 2 }
      }] as any;

      expect(component['goToNextIfStarted'](0)).toBeTruthy();
      expect(component.events[0].event.isStarted).toBeDefined();
    });

    it('should return false', () => {
      component.events = [{
        event: { startTimeUnix: Date.now() * 2 }
      }] as any;

      expect(component['goToNextIfStarted'](0)).toBeFalsy();
      expect(component.events[0].event.isStarted).toBeDefined();
    });

    it('should return false if event is invalid', () => {
      component.events = [] as any;
      expect(component['goToNextIfStarted'](0)).toBeFalsy();
    });
  });

  describe('hasEventFinished', () => {
    it('should return true', () => {
      component.events = [{
        event: { isFinished: 'true' }
      }] as any;
      expect(component['hasEventFinished'](0)).toBeTruthy();
    });

    it('should return false', () => {
      component.events = [] as any;
      expect(component['hasEventFinished'](0)).toBeFalsy();
    });
  });

  describe('activeStatePanel', () => {
    beforeEach(() => {
      component.currentEventIndex = 0;
    });

    it('panel exists', () => {
      component.panelsStates = [{ '1': {} }];
      expect(component.activeStatePanel('1')).toBeTruthy();
    });

    it('panel doesn\'t exist', () => {
      component.panelsStates = [];
      expect(component.activeStatePanel('1')).toBeFalsy();
    });

    it('should call activeStatePanel', () => {
      const panelId = undefined;

      component.activeStatePanel(panelId);
      expect(component['panelsStates']).toBeDefined();
    });
  });

  it('getEventStartTime', () => {
    component.currentEvent = {
      event: { startTimeUnix: 123 }
    } as any;
    component.getEventStartTime(123);
    expect(datePipeStub.transform).toHaveBeenCalledWith(
      component.currentEvent.event.startTimeUnix, 'HH:mm');
  });

  it('getEventStartDate', () => {
    component.currentEvent = {
      event: { startTimeUnix: 123 }
    } as any;
    component.getEventStartDate(123);
    expect(datePipeStub.transform).toHaveBeenCalledWith(
      component.currentEvent.event.startTimeUnix, 'yyyy-MM-dd');
  });

  it('should call trackMarketSectionById method', () => {
    const result = component.trackMarketSectionById(1, { id: 'test' });
    expect(result).toEqual('1test');
  });

  it('should call changeStatePanel', () => {
    const panelId = 'panelId';

    component.changeStatePanel(panelId);
    expect(component['panelsStates']).toBeDefined();
    expect(component['panelsStates']).toBeTruthy();
  });

  describe('deleteEvents', () => {
    it('should call deleteEvents()', () => {
      component['eventId'] = '22';
      component.eventsData = [{ event: {id: '2'} }] as any;
      component.currentEvent = { id: 13 } as ISportEventEntity;
      component['deleteEvents'](['1']);

      expect(component['currentEventIndex']).toBe(0);
    });

    it('should do nothing if no events', () => {
      component['update'] = jasmine.createSpy('update');
      component.events = [] as any;
      component['deleteEvents'](['1']);

      expect(component['update']).not.toHaveBeenCalled();
    });

    it('should reload virtuals if last event is deleted', () => {
      component['update'] = jasmine.createSpy('update');
      component.events = [{ event: { id: 1 } }] as any;
      component['deleteEvents'](['1']);

      expect(pubsub.publish).toHaveBeenCalledWith(pubsub.API.RELOAD_COMPONENTS);
      expect(component['update']).not.toHaveBeenCalled();
    });
  });

  it('should not call deleteEvents()', () => {
    component['deleteEvents'] = jasmine.createSpy('deleteEvents');

    component.events = [{
      id: 1,
      event: { startTimeUnix: Date.now() * 2, id:1}
    },
    {
      id:2,
      event: { startTimeUnix: Date.now() * 2, id:2}
    },{
      id:3,
      event: { startTimeUnix: Date.now() * 2, id:3}
    }] as any;
    component['removeOutdatedEvents']();
    expect(component['deleteEvents']).not.toHaveBeenCalled();
  });

  it('getMarketSwitcherConfig should return switcher config for specific filter name', () => {
    const result = component['getMarketSwitcherConfig']('winew');
    expect(result).toEqual({
      onClick: jasmine.any(Function),
      viewByFilters: 'winew',
      name: 'Win/Each Way'
    });
  });

  it('getMarketSwitcherConfig should return undefined if filterName is invalid', () => {
    const result = component['getMarketSwitcherConfig']('test');

    expect(result).toBeUndefined();
  });

  it('setSwitchers should add switchers if there are few valid markets and one of them is winEw', () => {
    component.currentEvent = <any>{event: {className: 'footballClass'}};
    component['getMarketSwitcherConfig'] = jasmine.createSpy('getMarketSwitcherConfig');
    const localEvent = {
      markets: [{
        template: 'WinEw',
        ncastTypeCodes: 'CF, CT',
        outcomes: [{}, {}, {}]
      }, {
        template: 'Vertical',
        marketName: 'place',
        outcomes: [{}, {}, {}]
      }, {
        template: 'Vertical',
        marketName: 'show',
        outcomes: [{}, {}, {}]
      }]
    } as any;
    const winMarket = localEvent.markets[0];

    virtualSportsService.normalizeData = jasmine.createSpy('normalizeData').and.returnValue(localEvent);

    component['setSwitchers']();

    expect(component.event).toEqual(localEvent);
    expect(virtualSportsService.normalizeData).toHaveBeenCalled();

    expect(virtualSportsService.genTerms).toHaveBeenCalled();
    expect(virtualSportsService.showTerms).toHaveBeenCalled();

    expect(virtualSportsService.genTerms).toHaveBeenCalledWith(winMarket);
    expect(virtualSportsService.showTerms).toHaveBeenCalledWith(winMarket);

    expect(component.filter).toEqual('winew');
    expect(component.market).toEqual(winMarket);
    expect(component.hasWinOrEachWay).toBeTruthy();
    expect(component['getMarketSwitcherConfig']).toHaveBeenCalledTimes(5);

    expect(component.switchers.length).toEqual(5);
  });

  it('setSwitchers should add switcher if event contain only one market and it follow expectations', () => {
    component['getMarketSwitcherConfig'] = jasmine.createSpy('getMarketSwitcherConfig');
    const localEvent = {
      markets: [{
        template: 'Vertical',
        marketName: 'place',
        outcomes: [{}, {}, {}]
      }]
    } as any;
    virtualSportsService.normalizeData = jasmine.createSpy('normalizeData').and.returnValue(localEvent);

    component['setSwitchers']();

    expect(component.event).toEqual(localEvent);
    expect(virtualSportsService.normalizeData).toHaveBeenCalled();

    expect(component.hasWinOrEachWay).toBeFalsy();
    expect(component['getMarketSwitcherConfig']).toHaveBeenCalledTimes(1);

    expect(component.switchers.length).toEqual(1);
  });

  it('setSwitchers should add winEw market even if it does not contain ncastTypeCodes', () => {
    component.currentEvent = <any>{event: {className: 'footballClass'}};
    component['getMarketSwitcherConfig'] = jasmine.createSpy('getMarketSwitcherConfig');
    const localEvent = {
      markets: [{
        template: 'WinEw',
        ncastTypeCodes: undefined,
        outcomes: [{}, {}, {}]
      }]
    } as any;
    virtualSportsService.normalizeData = jasmine.createSpy('normalizeData').and.returnValue(localEvent);

    component['setSwitchers']();

    expect(component.event).toEqual(localEvent);
    expect(virtualSportsService.normalizeData).toHaveBeenCalled();
    expect(component.hasWinOrEachWay).toEqual(true);
    expect(component['getMarketSwitcherConfig']).toHaveBeenCalledTimes(1);
    expect(component.switchers.length).toEqual(1);
  });

  it('setSwitchers shouldn\'t add switchers if there are no markets in event', () => {
    const localEvent = {
      markets: []
    } as any;
    virtualSportsService.normalizeData = jasmine.createSpy('normalizeData').and.returnValue(localEvent);

    component['setSwitchers']();

    expect(component.event).toEqual(localEvent);
    expect(virtualSportsService.normalizeData).toHaveBeenCalled();
    expect(component.hasWinOrEachWay).toBeFalsy();
    expect(component.switchers.length).toEqual(0);
  });
});
