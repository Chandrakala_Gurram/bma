import { from as observableFrom, of } from 'rxjs';
import { catchError, concatMap } from 'rxjs/operators';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import * as _ from 'underscore';

import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { TimeService } from '@core/services/time/time.service';
import { PanelStateService } from '../../services/panel-state.service';
import { VirtualSportsService } from '../../services/virtual-sports.service';

import {
  forecastFilter,
  MARKETS_CONFIG, tricastFilter, verticalTemplateName,
  VIRTUAL_ROUTE_NAME, winEwFilter, winEwTemplateName
} from '@app/vsbr/constants/virtual-sports.constant';
import { ISportEventEntity } from '@core/models/sport-event-entity.model';
import { IMarketEntity } from '@core/models/market-entity.model';
import { ITab } from '../../models/virtual-sport-classes.model';
import { ISwitcherConfig } from '@core/models/switcher-config.model';
import { NativeBridgeService } from '@core/services/nativeBridge/native-bridge.service';
import { DeviceService } from '@core/services/device/device.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { Subscription } from 'rxjs/index';
import { VirtualMenuDataService } from '@app/vsbr/services/virtual-menu-data.service';
import { ICategoryAliases, IVirtualChildCategory } from '@app/vsbr/models/virtual-sports-structure.model';
import { IVirtualSportsMenuItem } from '@app/vsbr/models/menu-item.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { IMarket } from '@core/models/market.model';
import { IMarketsConfig } from '@app/vsbr/models/virtual-sports-config.model';
import { VirtualSportsMapperService } from '@app/vsbr/services/virtual-sports-mapper.service';

@Component({
  selector: 'virtual-sport-classes',
  styleUrls: ['./virtual-sport-classes.less'],
  templateUrl: './virtual-sport-classes.component.html'
})
export class VirtualSportClassesComponent extends AbstractOutletComponent implements OnInit, OnDestroy {

  childMenu: IVirtualSportsMenuItem[];
  eventsData: ISportEventEntity[];
  activeClass: IVirtualChildCategory;
  events: ISportEventEntity[];
  currentEventIndex: number;
  currentEvent: ISportEventEntity;
  event: ISportEvent;
  market: IMarket;
  panelsStates: Object = {};
  sections: IMarketEntity[];
  requestedEventNotFound: boolean = false;
  terms: string = '';
  showTerms: boolean;
  hasWinOrEachWay: boolean = false;
  categoryAlias: string;
  parentCategoryAlias: string;
  categoryId: string;
  switchers: ISwitcherConfig[] = [];
  activeChild: number;
  ctaText: string;
  ctaUrl: string;
  navigatedFromEventId: string;
  isDesktop: boolean = this.deviceService.isDesktop;

  filter: string = winEwFilter;
  forecastFilter: string = forecastFilter;
  tricastFilter: string = tricastFilter;
  MARKETS_CONFIG: IMarketsConfig = MARKETS_CONFIG;

  private eventId: string;
  private pollingTimer: number;
  private timeOutListener;
  private intervalValue: number = 500;
  private paramsSubscriber: Subscription;

  private readonly tagName: string = 'VirtualSportClassesCtrl';

  constructor(
    private windowRef: WindowRefService,
    private filterService: FiltersService,
    private timeService: TimeService,
    private datePipe: DatePipe,
    private panelState: PanelStateService,
    private virtualSportsService: VirtualSportsService,
    private route: ActivatedRoute,
    private router: Router,
    protected deviceService: DeviceService,
    private nativeBridge: NativeBridgeService,
    private pubsub: PubSubService,
    private changeDetRef: ChangeDetectorRef,
    private virtualMenuDataService: VirtualMenuDataService,
    private vsMapperService: VirtualSportsMapperService
  ) {
    super()/* istanbul ignore next */;
  }

  ngOnInit(): void {
    this.addChangeDetection();
    this.init();
    this.reloadListener();
  }

  ngOnDestroy(): void {
    this.timeOutListener && this.windowRef.nativeWindow.clearInterval(this.timeOutListener);
    this.pubsub.unsubscribe(this.tagName);
    this.windowRef.nativeWindow.clearInterval(this.pollingTimer);
    this.paramsSubscriber && this.paramsSubscriber.unsubscribe();
    this.virtualSportsService.unsubscribeFromUpdates();
    this.virtualSportsService.unSubscribeVSBRForUpdates();
  }

  goToCtaUrl(): void {
    this.ctaUrl = this.ctaUrl ? this.ctaUrl : '/';
    this.router.navigate([this.ctaUrl]);
  }

  trackMarketSectionById(index: number, item: { id?: string }) {
    return `${index}${item.id}`;
  }

  /**
   * Check if event ongoing
   * @returns {Boolean}
   */
  isEventOngoing(): boolean {
    return this.goToNextIfStarted(this.currentEventIndex) && !this.hasEventFinished(this.currentEventIndex);
  }

  /**
   * Set Collapsed Panel.
   * @param {String} panelId
   */
  changeStatePanel(panelId: string): void {
    this.panelState.changeStatePanel(this.currentEventIndex, panelId);
  }

  /**
   * Display active Collapsed Panel.
   * @param {String} panelId
   * @returns {Boolean}
   */
  activeStatePanel(panelId: string): boolean {
    return (this.panelsStates[this.currentEventIndex] !== undefined)
      ? this.panelsStates[this.currentEventIndex][panelId] : false;
  }

  /**
   * Get formatted event start date
   * @param {number} startDate
   * @returns {String}
   */
  getEventStartDate(startDate: number): string {
    return this.datePipe.transform(startDate, 'yyyy-MM-dd');
  }

  /**
   * Get formatted event start time
   * @param {number} startTime
   * @returns {String}
   */
  getEventStartTime(startTime: number): string {
    return this.datePipe.transform(startTime, 'HH:mm');
  }

  /**
   * Tabs panel callback
   * @param {object} tab
   */
  onTabClick({ tab }: { tab: ITab }): void {
    this.goToEvent(tab.event.id.toString());
  }

  goToEvent(eventId: string) {
    this.requestedEventNotFound = false;
    this.currentEventIndex = this.events.findIndex(e => e.event.id.toString() === eventId);

    if (this.currentEventIndex < 0) {
      this.currentEventIndex = 0;
    }

    this.update();

    if (this.deviceService.isWrapper) {
      this.nativeBridge.onVirtualsSelected(this.categoryId, eventId);
    }
  }

  /**
   * Add change detection
   */
  addChangeDetection(): void {
    this.changeDetRef.detach();
    this.timeOutListener = this.windowRef.nativeWindow.setInterval(() => {
      this.changeDetRef.detectChanges();
    }, this.intervalValue);
  }

  private reloadListener() {
    this.pubsub.subscribe(this.tagName, this.pubsub.API.RELOAD_COMPONENTS, () => {
      this.init();
    });
  }
  /**
   * Check if event started
   * @param {Number} eventIndex
   * @returns {Boolean}
   */
  private goToNextIfStarted(eventIndex: number): boolean {
    const currentEvent = this.events[eventIndex] && this.events[eventIndex].event;

    if (currentEvent) {
      currentEvent.isStarted = (currentEvent.startTimeUnix - Date.now() <= 0);
      if (currentEvent.isStarted && this.navigatedFromEventId !== this.events[eventIndex].id && this.events[eventIndex + 1]) {
          this.navigatedFromEventId = this.events[eventIndex].id;
          this.goToEvent(this.events[eventIndex + 1].event.id.toString());
      }
      return currentEvent.isStarted;
    }

    return false;
  }

  /**
   * check if event finished
   * @param {Number} eventIndex
   */
  private hasEventFinished(eventIndex: number): boolean {
    return this.events[eventIndex] ? this.events[eventIndex].event.isFinished === 'true' : false;
  }

  /**
   * Update data according to selected event
   */
  private updateCurrentEvent(): void {
    // get new current event
    this.currentEvent = this.events[this.currentEventIndex];

    if (this.currentEvent) {
      // get new market current sections
      const marketSections = this.virtualSportsService.getMarketSectionsArray(this.currentEvent);
      this.sections = this.filterService.orderBy(marketSections, ['displayOrder']);
      this.setSwitchers();
    }
  }

  /**
   * Set switchers configs for event markets
   */
  private setSwitchers(): void {
    const marketSwitchers: ISwitcherConfig[] = [];
    const marketNames: string[] = Object.keys(MARKETS_CONFIG);

    this.hasWinOrEachWay = false;
    this.showTerms = false;
    this.event = this.virtualSportsService.normalizeData(this.currentEvent, this.isEventOngoing());

    this.event.markets.filter(
      market => market.template === winEwTemplateName ||
        (market.template === verticalTemplateName &&
          marketNames.includes(market.marketName.toLowerCase())))
      .forEach((market: IMarket) => {
        const isWinEW = market.template === winEwTemplateName;
        const filterName = isWinEW ? market.template.toLowerCase() : market.marketName.toLowerCase();
        const marketSwitcher = this.getMarketSwitcherConfig(filterName);

        marketSwitchers.push(marketSwitcher);

        if (isWinEW) {
          this.hasWinOrEachWay = true;
          this.market = market;
          this.filter = filterName;
          this.terms = this.virtualSportsService.genTerms(market);
          this.showTerms = this.virtualSportsService.showTerms(market)
            && !(this.currentEvent.event && this.currentEvent.event.className === '|Virtual Speedway|');

          const isForecastMarket: boolean = market.ncastTypeCodes && market.ncastTypeCodes.includes('CF') && market.outcomes.length >= 2;
          const isTriCastMarket: boolean = market.ncastTypeCodes && market.ncastTypeCodes.includes('CT') && market.outcomes.length >= 3;

          if (isForecastMarket) {
            const switcher = this.getMarketSwitcherConfig(forecastFilter);
            marketSwitchers.push(switcher);
          }

          if (isTriCastMarket) {
            const switcher = this.getMarketSwitcherConfig(tricastFilter);
            marketSwitchers.push(switcher);
          }
        }
      });

    this.switchers = marketSwitchers;
  }

  /**
   * Create switcher config for specific market
   * @param filterName - name of specific market
   * return {ISwitcherConfig} - switcher config
   */
  private getMarketSwitcherConfig(filterName: string): ISwitcherConfig {
    const marketName: string = MARKETS_CONFIG[filterName];

    if (marketName) {
      return {
        onClick: () => this.filter = filterName,
        viewByFilters: filterName,
        name: marketName
      };
    }
  }

  /**
   * Delete current event, update indexes
   * param {String} eventId
   */
  private deleteEvents(eventIds: Array<string>): void {
    if (!this.events.length) {
      return;
    }
    this.events = this.events.filter(item => !eventIds.includes(item.event.id.toString()));
    if (!this.events.length) {
      this.pubsub.publish(this.pubsub.API.RELOAD_COMPONENTS);
      return;
    }

    if (this.currentEventIndex !== 0) {
      this.currentEventIndex = this.events.findIndex(e => e.event.id.toString() === this.currentEvent.id.toString());

      if (this.currentEventIndex < 0) {
        this.currentEventIndex = 0;
      }
    }

    this.update();
    this.activeClass.startTimeUnix = this.currentEvent && this.currentEvent.event.startTimeUnix;
  }

  /**
   * Removes outdated events
   */
  private removeOutdatedEvents(): void {
    let startedEventIds: string[] = this.events
      .filter(e => e.event.startTimeUnix < Date.now())
      .map(e => e.event.id.toString());

    startedEventIds = startedEventIds.slice(0, startedEventIds.length - 1);
    if(startedEventIds && startedEventIds.length) {
      this.deleteEvents(startedEventIds);
    }
  }

  /**
   * Add fields that are needed for tabs panel
   */
  private prepareEventsForTabs(): void {
    _.each(this.events, item => {
      item.title = this.datePipe.transform(item.event.startTimeUnix, 'HH:mm');
      item.id = item.event.id;
    });
  }

  /**
   * Update data
   */
  private update(): void {
    this.updateCurrentEvent();
    this.panelsStates = this.panelState.getPanelsStates();
  }

  /**
   * Init data
   */
  private init(): void {
    this.paramsSubscriber = this.route.params.pipe(
      concatMap((params: Params) => {
        this.showSpinner();
        return observableFrom(this.virtualSportsService.getEventsWithRacingForms(params.alias)).pipe(catchError(() => {
          this.showError();
          return of();
        }));
      })).subscribe((eventData: ISportEventEntity[]) => {
        this.eventsData = eventData;

        this.parentCategoryAlias = this.route.snapshot.params['category'];
        this.categoryAlias = this.route.snapshot.params['alias'];
        this.eventId = this.route.snapshot.params['eventId'];

        this.childMenu = this.virtualMenuDataService.getChildMenuItems(this.parentCategoryAlias);
        this.activeChild = this.virtualMenuDataService.activeChildIndex;

        this.activeClass = this.virtualSportsService.getActiveClass(this.categoryAlias);

        const parentCategory = this.vsMapperService.getParentByAlias(this.parentCategoryAlias);
        this.ctaText = parentCategory && parentCategory.ctaButtonText;
        this.ctaUrl = parentCategory && parentCategory.ctaButtonUrl;

        this.events = this.virtualSportsService.filterEvents(this.eventsData);
        this.prepareEventsForTabs();

        // first event shown by default first elem of array
        this.currentEventIndex = this.eventId ? this.events.findIndex(e => e.event.id.toString() === this.eventId.toString()) : 0;

        if (this.currentEventIndex < 0) {
          this.requestedEventNotFound = true;
          this.currentEventIndex = 0;
        }

        // subscribe for live serve updates
        this.virtualSportsService.unSubscribeVSBRForUpdates();
        this.virtualSportsService.subscribeVSBRForUpdates(this.events);

        this.goToNextIfStarted(this.currentEventIndex);
        this.update();

        if (this.deviceService.isWrapper) {
          this.categoryId = this.virtualSportsService.getCategoryByAlias(this.categoryAlias);
          const eventId = this.eventId ? this.eventId : this.events[0].id;
          this.nativeBridge.onVirtualsSelected(this.categoryId, eventId);

          this.pubsub.subscribe(this.tagName, this.pubsub.API.VIRTUAL_ORIENTATION_CHANGED, classId => {
            // return both parent and child aliases
            const aliases: ICategoryAliases = this.virtualSportsService.getAliasesByClassId(classId);

            if (aliases && aliases.childAlias !== this.categoryAlias) {
              this.router.navigate([VIRTUAL_ROUTE_NAME, aliases.parentAlias, aliases.childAlias]);
            }
          });
        }

        this.pubsub.subscribe(this.tagName, this.pubsub.API.VS_EVENT_FINISHED, args => {
          this.requestedEventNotFound = false;
          this.updateFinishedStatus(args.eventId);
          setTimeout(() => this.deleteEvents([args.eventId]), this.timeService.goForNextVsEvent);
        });

        // This is handling event switching in case we do not receive valid event updates finish status.
        this.pollingTimer = this.windowRef.nativeWindow
          .setInterval(() => this.removeOutdatedEvents(), this.timeService.eventPollingInterval);

        this.hideSpinner();
        this.hideError();
      }, () => {
        this.showError();
      });
  }

  private updateFinishedStatus(finishedId: string) {
    this.events.forEach((event: ISportEventEntity) => {
      if (event.event.id.toString() === finishedId) {
        event.event.isFinished = 'true';
      }
    });
  }
}
