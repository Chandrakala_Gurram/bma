import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class EventVideoStreamProviderService {

  private playVideoObservable: Subject<void>;
  private playSucessObservable: Subject<boolean>;
  private hideStreamObservable: Subject<boolean>;
  constructor() {
    this.playVideoObservable = new Subject<void>();
    this.playSucessObservable = new Subject<boolean>();
    this.hideStreamObservable = new Subject<boolean>();
  }

  get playListener(): Subject<void> {
    return this.playVideoObservable;
  }

  get playSuccessErrorListener(): Subject<boolean> {
    return this.playSucessObservable;
  }

  get showHideStreamListener(): Subject<boolean> {
    return this.hideStreamObservable;
  }
}
