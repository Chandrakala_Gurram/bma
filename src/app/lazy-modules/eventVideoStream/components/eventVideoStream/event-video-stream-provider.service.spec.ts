import {
  EventVideoStreamProviderService
} from '@lazy-modules/eventVideoStream/components/eventVideoStream/event-video-stream-provider.service';

describe('EventVideoStreamProviderService', () => {
  let service;
  let subscriber;

  beforeEach(() => {
    subscriber = jasmine.createSpy('subscriber');

    service = new EventVideoStreamProviderService();
  });

  it('should return playListener', () => {
    service.playListener.subscribe(subscriber);
    service.playListener.next();

    expect(subscriber).toHaveBeenCalled();
  });

  it('should return playSuccessErrorListener', () => {
    service.playSuccessErrorListener.subscribe(subscriber);
    service.playSuccessErrorListener.next(true);

    expect(subscriber).toHaveBeenCalledWith(true);
  });

  it('should return showHideStreamListener', () => {
    service.showHideStreamListener.subscribe(subscriber);
    service.showHideStreamListener.next(false);

    expect(subscriber).toHaveBeenCalledWith(false);
  });
});
