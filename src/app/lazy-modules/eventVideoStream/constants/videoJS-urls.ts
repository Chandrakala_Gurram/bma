export const videoJSUrls =   {
  js: [
    'assets/videojs/video.min.js',
    'assets/videojs/videojs-flash.min.js',
    'assets/videojs/videojs-contrib-hls.min.js'
  ],
  css: 'assets/videojs/video-js.min.css'
};
