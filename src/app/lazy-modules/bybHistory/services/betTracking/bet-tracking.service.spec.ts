import { of, Observable } from 'rxjs';
import { fakeAsync } from '@angular/core/testing';

import { BetTrackingService } from './bet-tracking.service';

describe('BetTrackingService', () => {
  let service: BetTrackingService,
    cmsService,
    configFromCms,
    betTrackingRulesService,
    domSanitizer;

  beforeEach(() => {
    configFromCms = {
      BetTracking: {
        enabled: true
      }
    };
    domSanitizer = {
      sanitize: jasmine.createSpy('sanitize'),
      bypassSecurityTrustHtml: jasmine.createSpy('bypassSecurityTrustHtml')
    };
    cmsService = {
      getStaticBlock: jasmine.createSpy('getStaticBlock').and.returnValue(of({htmlMarkup: 'some string'})),
      getSystemConfig: jasmine.createSpy('getSystemConfig').and.returnValue(of(configFromCms))
    };
    betTrackingRulesService = {
      matchBettingStatusHandler: jasmine.createSpy('betTrackingRulesService').and.returnValue(
        {
          status: 'Loosing',
          progress: {
            current: 1,
            target: 3,
            desc: '1 of 3 goals'
          }
        }
      )
    };

    service = new BetTrackingService(domSanitizer, cmsService, betTrackingRulesService);
  });

  describe('isTrackingEnabled', () => {
    it('should return true', () => {
      service.isTrackingEnabled().subscribe(res => {
        expect(cmsService.getSystemConfig).toHaveBeenCalled();
        expect(res).toBeTruthy();
      });
    });

    it('should return false', () => {
      configFromCms.BetTracking = { enabled : false };

      service.isTrackingEnabled().subscribe(res => {
        expect(cmsService.getSystemConfig).toHaveBeenCalled();
        expect(res).toBeFalsy();
      });
    });

    it('should return false', () => {
      configFromCms.BetTracking = { };

      service.isTrackingEnabled().subscribe(res => {
        expect(cmsService.getSystemConfig).toHaveBeenCalled();
        expect(res).toBeFalsy();
      });
    });
  });

  describe('getSelectionStatusAndProgress', () => {
    it('should return null', () => {
      const selection: any = {
        part: {},
        config: {
          name: 'name',
          methodName: 'methodName'
        }
      };
      expect(service['getSelectionStatusAndProgress'](selection, {} as any, {} as any)).toEqual({ status: '' });
    });

    it('should return progress', () => {
      const selection: any = {
        part: {},
        config: {
          name: 'name',
          methodName: 'matchBettingStatusHandler'
        }
      };
      expect(service['getSelectionStatusAndProgress'](selection, {} as any, {} as any)).toEqual({
        status: 'Loosing',
          progress: {
          current: 1,
          target: 3,
          desc: '1 of 3 goals'
          }
      });
    });

    it('should not return progress (no result)', () => {
      betTrackingRulesService.matchBettingStatusHandler = jasmine.createSpy('matchBettingStatusHandler').and.returnValue(null);
      const selection: any = {
        part: {
          outcome: [{
            externalStatsLink: { statValue: '3', statCategory: 'Goals', currentValue: '2' }
          }]
        },
        config: {
          name: 'name',
          methodName: 'matchBettingStatusHandler'
        }
      };
      const bet: any = { settled: 'Y' };

      expect(service['getSelectionStatusAndProgress'](selection, bet, {} as any)).toEqual({ status: '' });
    });
  });

  describe('getStaticContent', () => {
    it('should return static block', fakeAsync(() => {
      service.getStaticContent().subscribe( () => {
        expect(cmsService.getStaticBlock).toHaveBeenCalledWith('opta-disclaimer-short-en-us');
        expect(domSanitizer.bypassSecurityTrustHtml).toHaveBeenCalled();
        expect(domSanitizer.sanitize).toHaveBeenCalled();
      });
    }));

    it('should return static block  as empty', fakeAsync(() => {
      cmsService.getStaticBlock.and.returnValue(of({htmlMarkup: ''}));
      service.getStaticContent().subscribe();
      expect(cmsService.getStaticBlock).toHaveBeenCalledWith('opta-disclaimer-short-en-us');
      expect(domSanitizer.bypassSecurityTrustHtml).not.toHaveBeenCalled();
      expect(domSanitizer.sanitize).not.toHaveBeenCalled();
    }));

    it('should return observable if it already exist', fakeAsync(() => {
      service['staticContentObservable'] = new Observable();
      service.getStaticContent().subscribe( () => {
        expect(cmsService.getStaticBlock).not.toHaveBeenCalledWith('opta-disclaimer-short-en-us');
        expect(domSanitizer.bypassSecurityTrustHtml).not.toHaveBeenCalled();
        expect(domSanitizer.sanitize).not.toHaveBeenCalled();
      });
    }));
  });

  describe('updateProgress', ()=> {
    let selections;
    let updatedSelections;

    beforeEach(() => {
      selections = [
        {
          config: {
            template: 'range'
          },
          progress: 'progress',
          status: 'status',
          showBetStatusIndicator: 'showBetStatusIndicator'
        },
        {
          config: {
            template: 'binary'
          },
          status: 'status',
          showBetStatusIndicator: 'showBetStatusIndicator'
        },
        {
          config: {
            template: 'template'
          },
          progress: 'progress',
          status: 'status',
          showBetStatusIndicator: 'showBetStatusIndicator'
        }
      ] as any;
      updatedSelections = [
        {
          config: {
            template: 'range'
          },
          progress: 'updated progress',
          status: 'updated status',
          showBetStatusIndicator: true
        },
        {
          config: {
            template: 'binary'
          },
          status: 'updated status',
          showBetStatusIndicator: 'true'
        },
        {
          config: {
            template: 'template'
          },
          progress: null,
          status: null,
          showBetStatusIndicator: false
        }
      ] as any;
    });

    it('should update progress', () => {
      service['getSelectionStatusAndProgress'] = jasmine.createSpy('getSelectionStatusAndProgress').and.returnValue(
        {
          status: 'updated status',
          progress: 'updated progress'
        }
      );
      service.updateProgress(selections, {} as any, {} as any);

      expect(selections[0].progress).toEqual(updatedSelections[0].progress);
      expect(selections[0].status).toEqual(updatedSelections[0].status);
      expect(selections[0].showBetStatusIndicator).toEqual(updatedSelections[0].showBetStatusIndicator);
    });

    it('should not update progress', () => {
      service['getSelectionStatusAndProgress'] = jasmine.createSpy('getSelectionStatusAndProgress').and.returnValue(null);
      service.updateProgress(selections, {} as any, {} as any);

      expect(selections[0].progress).toEqual(selections[0].progress);
      expect(selections[0].status).toEqual(selections[0].status);
      expect(selections[0].showBetStatusIndicator).toEqual(selections[0].showBetStatusIndicator);
    });
  });

  it('should check is build your bet', () => {
    const part = [
      {
        eventMarketDesc: 'Build Your Bet'
      }
    ] as any;

    expect(service.checkIsBuildYourBet(part)).toBeTruthy();
  });

  describe('extendSelectionsWithTrackingConfig', () => {
    it('should extend selections if eventMarketDesc is present in config', () => {
      const selections = [
        {
          part: {
            eventMarketDesc: 'Build Your Bet MATCH BETTING'
          }
        },
        {
          part: {
            eventMarketDesc: 'eventMarketDesc'
          }
        }
      ] as any;
      service.extendSelectionsWithTrackingConfig(selections);

      expect(selections[0].config).toBeTruthy();
    });
  });
});
