import { BybSelectionsService } from '@lazy-modules/bybHistory/services/bybSelectionsService/byb-selections.service';

describe('BybSelectionsService', () => {
  let service: BybSelectionsService;
  beforeEach(() => {
    service = new BybSelectionsService();
  });

  describe('createSelection', () => {
    const leg = {
      eventEntity: { name: 'event name' },
      part: [{ description: '', eventMarketDesc: '' }]
    } as any;

    let part;

    it('anytime goalscorer', () => {
      part = {
        description: 'Gomez',
        eventMarketDesc: 'Build Your Bet Anytime Goalscorer'
      };
      expect(service['createSelection'](part, leg)).toEqual(
        jasmine.objectContaining({ title: 'Gomez Anytime Goalscorer' })
      );
    });

    it('to keep a clean sheet', () => {
      part = {
        description: 'Gomez To Keep A Clean Sheet - Yes',
        eventMarketDesc: 'Build Your Bet Player To Keep A Clean Sheet'
      };
      expect(service['createSelection'](part, leg)).toEqual(
        jasmine.objectContaining({ title: 'Gomez To Keep A Clean Sheet' })
      );
    });

    it('to be shown a card', () => {
      part = {
        description: 'Gomez',
        eventMarketDesc: 'Build Your Bet To Be Shown A Card'
      };
      expect(service['createSelection'](part, leg)).toEqual(
        jasmine.objectContaining({ title: 'Gomez To Be Carded' })
      );
    });

    it('to score 2 or more goals', () => {
      part = {
        description: 'Gomez',
        eventMarketDesc: 'Build Your Bet To Score 2 Or More Goals'
      };
      expect(service['createSelection'](part, leg)).toEqual(
        jasmine.objectContaining({ title: 'Gomez To Score 2+ Goals' })
      );
    });

    it('passes, assists, tackles', () => {
      part = {
        description: 'Gomez To Have 1 Or More Assists',
        eventMarketDesc: 'Build Your Bet Player Total Assists'
      };
      expect(service['createSelection'](part, leg)).toEqual(
        jasmine.objectContaining({ title: 'Gomez To Make 1+ Assists' })
      );
    });

    it('other player bets', () => {
      part =  {
        description: 'Gomez To Have 1 Or More Shots Outside Box',
        eventMarketDesc: 'Build Your Bet Player Total Shots Outside Box'
      };
      expect(service['createSelection'](part, leg)).toEqual(
        jasmine.objectContaining({ title: 'Gomez To Have 1+ Shots Outside Box' })
      );
    });

    it('default title and description', () => {
      part = {
        description: 'Draw',
        eventMarketDesc: 'Build Your Bet Corners Match Bet'
      };
      expect(service['createSelection'](part, leg )).toEqual(
        jasmine.objectContaining({ title: 'Draw', desc: 'Corners Match Bet' })
      );
    });
  });

  describe('formBYBTitle', () => {
    it('shoud get name from eventEntity', () => {
      const leg: any = { eventEntity: { name: 'Team1 v Team2' } };
      expect(
        service['formBYBTitle'](leg, 'participant_1 - participant_2')
      ).toEqual('Team1 - Team2');
    });

    it('shoud get name from backupEventEntity', () => {
      const leg: any = { backupEventEntity: { name: 'Team3 v Team4' } };
      expect(
        service['formBYBTitle'](leg, 'participant_1 - participant_2')
      ).toEqual('Team3 - Team4');
    });

    it('shoud get name from eventDesc', () => {
      const leg: any = { part: [{ eventDesc: 'Team5 v Team6' }] };
      expect(
        service['formBYBTitle'](leg, 'participant_1 - participant_2')
      ).toEqual('Team5 - Team6');
    });
  });

  describe('sortSelections', () => {
    it('should sort', () => {
      let leg = {
        eventEntity: { name: 'event name' },
        part: [{
          description: 'Gomez',
          eventMarketDesc: 'Build Your Bet Anytime Goalscorer'
        }, {
          description:'Ramirez',
          eventMarketDesc:'Build Your To Keep A Clean Sheet'
        }]
      } as any;
      let expectedResult = [
        {
          part:{
            description: 'Gomez',
            eventMarketDesc: 'Build Your Bet Anytime Goalscorer'
          },
          title: 'Gomez Anytime Goalscorer'
        },
        {
          part:{
            description: 'Ramirez',
            eventMarketDesc: 'Build Your To Keep A Clean Sheet'
          },
          title: 'Ramirez'
        }
      ];
      let actualResult = service.getSortedSelections(leg);

      expect(actualResult).toEqual(expectedResult as any);

      leg = {
        eventEntity: { name: 'event name' },
        part: [{
          description:'Ramirez',
          eventMarketDesc:'Build Your To Keep A Clean Sheet'
        },{
          description: 'Gomez',
          eventMarketDesc: 'Build Your Bet Anytime Goalscorer'
        }]
      } as any;
      expectedResult = [
        {
          part:{
            description:'Gomez',
            eventMarketDesc:'Build Your Bet Anytime Goalscorer'
          },
          title:'Gomez Anytime Goalscorer'
        },
        {
          part:{
            description:'Ramirez',
            eventMarketDesc:'Build Your To Keep A Clean Sheet'
          },
          title:'Ramirez'
        }
        ];
      actualResult = service.getSortedSelections(leg);

      expect(actualResult).toEqual(expectedResult as any);
    });

    it('should not sort', () => {
      const leg = {
        eventEntity: { name: 'event name' },
        part: [{
          description: 'Gomez',
          eventMarketDesc: 'Build Your To Keep A Clean Sheet'
        }, {
          description: 'Gomez',
          eventMarketDesc: 'Build Your To Keep A Clean Sheet'
        }]
      } as any;
      const expectedResult = [
        {
          part:{
            description:'Gomez',
            eventMarketDesc:'Build Your To Keep A Clean Sheet'
          },
          title:'Gomez'},
        {
          part:{
            description:'Gomez',
            eventMarketDesc:'Build Your To Keep A Clean Sheet'
          },
          title:'Gomez'
        }
        ];
      const actualResult = service.getSortedSelections(leg);

      expect(actualResult).toEqual(expectedResult as any);
    });
  });
});
