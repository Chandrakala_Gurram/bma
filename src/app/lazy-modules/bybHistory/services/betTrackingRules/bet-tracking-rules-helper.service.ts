import { Injectable } from '@angular/core';
import {
  IBybSelection,
  IBybSelectionProgress,
  IBybSelectionStatus,
  IConditions
} from '@lazy-modules/bybHistory/models/byb-selection.model';
import { IBetHistoryBet } from '@app/betHistory/models/bet-history.model';
import { IBetHistoryOutcome, IExternalStatsLink } from '@core/models/outcome.model';
import {
  DOUBLE_TEAMS,
  PERIODS,
  STATS_CATEGORIES,
  STATUSES,
  TEAMS,
  BET_STATUSES
} from '@lazy-modules/bybHistory/constants/byb-5aside-markets-config.constant';
import {
  PLAYER_STATS, StatCategoryUtilityService
} from '@lazy-modules/bybHistory/services/betTrackingRules/stat-category-utility.service';
import {
  ICardsInfo,
  IGoalInfo,
  IPlayer,
  IPlayersSimple,
  IRedCardsByTeams,
  IScore,
  IScoreboardStatsUpdate,
  IScoreByTeams,
  IScoreByTime,
  ITeams
} from '@lazy-modules/bybHistory/models/scoreboards-stats-update.model';

@Injectable()
export class BetTrackingRulesHelperService {
  constructor(protected statCategoryUtilityService: StatCategoryUtilityService) {}

  /**
   * Get particular stats category object from statCategoryUtilityService
   * e.g. getScore, getAssists
   * @param selection
   * @param update
   */
  protected getStatCategoryObj(selection: IBybSelection, update: IScoreboardStatsUpdate): any {
    const handler = this.statCategoryUtilityService[`get${selection.config.statCategory}`]; // Get stats from statCategoryUtilityService

    if (!handler) {
      console.warn(`No ${selection.config.statCategory} were found in statCategoryUtilityService`);
      return {};
    }

    return handler(selection.config.generalInformationRequired, update);
  }

  /**
   * Get progress of selection
   * @param selection  Byb selection
   * @param bet        History bet
   * @param current    current progress value
   */
  protected getSelectionProgress(selection: IBybSelection, bet: IBetHistoryBet, current: number): IBybSelectionProgress {
    const stats = this.getExternalStatsLink(selection);

    if (!stats) {
      return null;
    }

    const target = this.parseStatValue(stats.statValue);
    const desc = bet.settled === BET_STATUSES.SETTLED ? `${current} ${STATS_CATEGORIES[stats.statCategory]}` :
      `${current} of ${target} ${STATS_CATEGORIES[stats.statCategory]}`;

    return { current, target, desc };
  }

  protected getPlayerScoredInPeriodInfo(update: IScoreboardStatsUpdate, playerId: string) {
    const allGoals: IGoalInfo[] = this.statCategoryUtilityService.getAllGoals(update),
      playerScoredInPeriodInfo = {};

    [PERIODS['1ST_HALF'], PERIODS['2ND_HALF']].forEach((period: string) => {
      const goalsInPeriod = allGoals.filter(goalInfo => goalInfo.period === period);
      playerScoredInPeriodInfo[period] = goalsInPeriod.filter(goalInfo => goalInfo.player &&
        goalInfo.player.providerId === playerId).length;
    });
    return playerScoredInPeriodInfo;
  }

  /**
   * Parse string value as number
   * @param statValue string value
   * Rules:
   * statValue = ">0.5" => 1
   * statValue = "<0.5" => 0
   * statValue = ">=1" => 1
   * statValue = "1" => 1
   * statValue = "=1" => 1
   * @private
   * @returns {number}
   */
  protected parseStatValue(statValue: string): number {
    const pattern = new RegExp(/[\=\>\<]+/, 'g');

    if (statValue.includes('=') || statValue.includes('<')) {
      return Math.floor(Number(statValue.replace(pattern, '')));
    } else {
      return Math.ceil(Number(statValue.replace(pattern, '')));
    }
  }

  /**
   * Get team which scored first in chosen period
   * @param team
   * @param update
   * @param period
   */
  protected getTeamScoredFirst(update: IScoreboardStatsUpdate, period: string): string {
    const periodFilters = {
      [PERIODS['1ST_HALF']]: goalInfo => goalInfo.period === PERIODS['1ST_HALF'],
      [PERIODS['2ND_HALF']]: goalInfo => goalInfo.period === PERIODS['2ND_HALF'],
      [PERIODS.total]: () => true
    };

    const allGoals: IGoalInfo[] = this.statCategoryUtilityService.getAllGoals(update),
      goalsInPeriod = allGoals.filter(periodFilters[period]);

    return goalsInPeriod.length ? goalsInPeriod[0].team : undefined;
  }

  /**
   * Get Team via externalStatsLink contestantId or { home: 1, away: 5 } by outcome name
   * @param selection
   * @param update
   * @private
   * @returns {string} Home | Away | Draw
   */
  protected getTeamByExternalStatsLink(
    selection: IBybSelection,
    update: IScoreboardStatsUpdate,
    isCorrectScore?: boolean
  ): string | IScoreByTime {
    const { contestantId } = this.getExternalStatsLink(selection);
    const homeId = update.home && update.home.providerId;
    const awayId = update.away && update.away.providerId;
    const selectionName = selection.part.outcome[0].name;
    const selectionTeamsValue = isCorrectScore && selectionName.split(' ')[1].split('-');

    if (contestantId === homeId) {
      return isCorrectScore ? { home: +selectionTeamsValue[0], away: +selectionTeamsValue[1] } : TEAMS.HOME;
    } else
    if (contestantId === awayId) {
      return isCorrectScore ? { home: +selectionTeamsValue[1], away: +selectionTeamsValue[0]} : TEAMS.AWAY;
    } else {
      return isCorrectScore ? { home: +selectionTeamsValue[0], away: +selectionTeamsValue[1] } : TEAMS.DRAW;
    }
  }

  /**
   * Get goals by team and period
   * @param {IGoalInfo[]} goals
   * @param {string} period aka '15:00' | '30:00' | '60:00' | '75:00'
   * @param {string} periodStart for ranges 0-30 | 30 - 60
   * @private
   * @returns { home: IGoalInfo[], away: IGoalInfo[] }
   */
  protected getGoalsByTeamAndPeriod(goals: IGoalInfo[], period: string, periodStart: string = ''): { [key: string]: IGoalInfo[] } {
    return {
      [TEAMS.HOME]: goals.filter((goal: IGoalInfo) => {
        return goal.team === TEAMS.HOME && (periodStart
          ? this.isPeriodReached(goal.time, periodStart) && !this.isPeriodReached(goal.time, period)
          : !this.isPeriodReached(goal.time, period));
      }),
      [TEAMS.AWAY]: goals.filter((goal: IGoalInfo) => {
        return goal.team === TEAMS.AWAY && (periodStart
          ? this.isPeriodReached(goal.time, periodStart) && !this.isPeriodReached(goal.time, period)
          : !this.isPeriodReached(goal.time, period));
      })
    };
  }

  /**
   * Get Team Stats from both home and away team by period
   * Stat - Corners | Cards
   * @param {string} period
   * @param {ITeams} statsCategoryObj
   * @param {string} stat
   * @private
   * @returns { home?: number, away?: number }
   */
  protected getTeamStatsByPeriod(period: string, statsCategoryObj: ITeams, stat: string): { home?: number, away?: number } {
    switch (period) {
    case PERIODS.total: // TOTAL STAT
      return {
        home: statsCategoryObj.home.total[stat],
        away: statsCategoryObj.away.total[stat]
      };
    case PERIODS['1ST_HALF']: // 1ST HALF STAT
      return {
        home: statsCategoryObj.home[PERIODS['1ST_HALF']][stat],
        away: statsCategoryObj.away[PERIODS['1ST_HALF']][stat]
      };
    case PERIODS['2ND_HALF']: // 2ND HALF STATS
      if (statsCategoryObj.home[PERIODS['2ND_HALF']]) {
        return {
          home: statsCategoryObj.home[PERIODS['2ND_HALF']][stat],
          away: statsCategoryObj.away[PERIODS['2ND_HALF']][stat]
        };
      }

      return { home: 0, away: 0 };
    default:
      return  {};
    }
  }

  /**
   * Check if selection is UNDER X.Y
   * @param {string} statValue
   * @private
   * @returns {boolean}
   */
  protected isUnder(statValue: string): boolean {
    return statValue.includes('<');
  }

  /**
   * Check if selection is OVER X.Y
   * @param {string} statValue
   * @private
   * @returns {boolean}
   */
  protected isOver(statValue: string): boolean {
    return statValue.includes('>');
  }

  /**
   * Check if selection is EQUAL
   * @param {string} statValue
   * @private
   * @returns {boolean}
   */
  protected isEqual(statValue: string): boolean {
    return statValue.includes('=');
  }

  /**
   * Get status for MATCH BETTING markets
   * @param goalsObj
   * @param bet
   * @param team
   * @param period
   * @param currentPeriod
   */
  protected getMatchBettingStatus(goalsObj: IScoreByTeams, bet: IBetHistoryBet, team: string,
                                period: string, currentPeriod?: string): string {
    let status = '';
    const betSettled = bet.settled === BET_STATUSES.SETTLED;

    if (goalsObj.score[period]) {
      const { home, away } = goalsObj.score[period];
      const conditions = {
        [ TEAMS.DRAW ]: home === away,
        [ TEAMS.AWAY ]: home < away,
        [ TEAMS.HOME ]: home > away
      };

      if (this.isFirstHalf(period)) {
        status = this.getFirstHalfBettingStatus(conditions, betSettled, team, currentPeriod);
      } else {
        status = this.getFullMatchStatus(conditions, betSettled, team);
      }
    } else {
      status = betSettled ? STATUSES.LOSE : STATUSES.LOSING;
    }

    return status;
  }

  /**
   * Get full match status
   * @param conditions
   * @param betSettled
   * @param team
   */
  protected getFullMatchStatus(conditions: { [key: string]: boolean }, betSettled: boolean, team: string): string {
    let status = '';

    if (conditions[team] && !betSettled) {
      status =  STATUSES.WINNING;
    } else if (conditions[team] && betSettled) {
      status = STATUSES.WON;
    } else if (!betSettled) {
      status = STATUSES.LOSING;
    } else {
      status = STATUSES.LOSE;
    }

    return status;
  }

  /**
   * Get status for first half betting markets
   * @param conditions
   * @param betSettled
   * @param team
   * @param period
   */
  protected getFirstHalfBettingStatus(conditions: { [key: string]: boolean }, betSettled: boolean, team: string, period: string): string {
    let status = '';
    const currentPeriod = this.statCategoryUtilityService.getCurrentPeriod(period);
    const isFirstHalf = this.isFirstHalf(currentPeriod);
    const isSecondHalfOrTotal = this.isSecondHalfOrTotal(currentPeriod);

    if (conditions[team] && !betSettled && isFirstHalf) {
      status =  STATUSES.WINNING;
    } else if (conditions[team] && (betSettled || isSecondHalfOrTotal)) {
      status = STATUSES.WON;
    } else if (!betSettled && isFirstHalf) {
      status = STATUSES.LOSING;
    } else {
      status = STATUSES.LOSE;
    }

    return status;
  }

  /**
   * Get status for Double Chance markets
   * @param goalsObj
   * @param bet
   * @param team
   * @param period
   * @param currentPeriod
   */
  protected getDoubleChanceStatus(
    goalsObj: IScoreByTeams,
    bet: IBetHistoryBet,
    team: string,
    period: string,
    currentPeriod?: string
  ): string {
    const betSettled = bet.settled === BET_STATUSES.SETTLED;
    let status = '';

    if (goalsObj.score[period]) {
      const { home, away } = goalsObj.score[period];
      const conditions = {
        [ DOUBLE_TEAMS.HOME_OR_DRAW ]: home >= away,
        [ DOUBLE_TEAMS.AWAY_OR_DRAW ]: home <= away,
        [ DOUBLE_TEAMS.HOME_OR_AWAY ]: home !== away
      };

      if (this.isFirstHalf(period)) {
        status = this.getFirstHalfBettingStatus(conditions, betSettled, team, currentPeriod);
      } else {
        status = this.getFullMatchStatus(conditions, betSettled, team);
      }
    } else {
      status = betSettled ? STATUSES.LOSE : STATUSES.LOSING;
    }

    return status;
  }

  /**
   * Get status for Correct Score markets
   * @param goalsObj
   * @param bet
   * @param team
   * @param period
   * @param currentPeriod
   */
  protected getCorrectScoreStatus(
    goalsObj: IScoreByTeams,
    bet: IBetHistoryBet,
    objTeams: IScoreByTime,
    period: string,
    currentPeriod?: string
  ): string {
    const betSettled = bet.settled === BET_STATUSES.SETTLED;
    let status = '';

    if (goalsObj.score[period]) {
      const { home, away } = goalsObj.score[period];
      const conditions = {
        forWining: objTeams.home === home && objTeams.away === away,
        forLosing: objTeams.home < home || objTeams.away < away
      };

      if (this.isFirstHalf(period)) {
        return this.getFirstHalfCorrectScoreStatus(conditions, betSettled, currentPeriod);
      } else {
        return this.getTotalCorrectScoreStatus(conditions, betSettled);
      }
    } else {
      status = betSettled ? STATUSES.LOSE : STATUSES.LOSING;
    }

    return status;
  }

  /**
   * Calculate status for Correct Score(1st Half) by parameters
   * @param conditions
   * @param betSettled
   * @param currentPeriod
   */
  protected getFirstHalfCorrectScoreStatus(conditions, betSettled, currentPeriod): string {
    const isFirstHalf = this.isFirstHalf(currentPeriod);
    const isSecondHalfOrTotal = this.isSecondHalfOrTotal(currentPeriod);
    let status = '';

    if (conditions.forWining && !betSettled && isFirstHalf) {
      status = STATUSES.WINNING;
    } else
    if (conditions.forWining && (betSettled || isSecondHalfOrTotal)) {
      status = STATUSES.WON;
    } else
    if (betSettled || conditions.forLosing) {
      status = STATUSES.LOSE;
    } else {
      status = STATUSES.LOSING;
    }

    return status;
  }

  /**
   * Calculate status for Correct Score by parameters
   * @param conditions
   * @param betSettled
   */
  protected getTotalCorrectScoreStatus(conditions, betSettled): string {
    let status = '';

    if (conditions.forWining && !betSettled) {
      status = STATUSES.WINNING;
    } else
    if (conditions.forWining && betSettled) {
      status = STATUSES.WON;
    } else
    if (betSettled || conditions.forLosing) {
      status = STATUSES.LOSE;
    } else {
      status = STATUSES.LOSING;
    }

    return status;
  }

  /**
   * Get status for Red Card markets
   * @param outcomeName
   * @param cardsObj
   * @param bet
   * @param period
   * @param currentPeriod
   */
  protected getFullRedsStatus(outcomeName: string, cardsObj: IRedCardsByTeams, bet: IBetHistoryBet,
                            period: string, currentPeriod: string): string {
    const betSettled = bet.settled === BET_STATUSES.SETTLED;
    const isYesOutcome = outcomeName === BET_STATUSES.OUTCOME_NAME_YES;
    const isRedCard = cardsObj.away.periods[period] && cardsObj.away.periods[period].redCards
      || cardsObj.home.periods[period] && cardsObj.home.periods[period].redCards;

    if (this.isFirstHalf(period)) { // 1ST HALF
      return  this.getFirstHalfRedCardsStatus(!!isRedCard, isYesOutcome, betSettled, currentPeriod);
    }

    return this.getTotalRedCardsStatus(!!isRedCard, isYesOutcome, betSettled);
  }

  /**
   * Get status for Red Card by participant
   * @param cards
   * @param isYesOutcome
   * @param bet
   */
  protected getRedCardsParticipantStatus(cards: number, isYesOutcome: boolean, bet: IBetHistoryBet): string {
    const betSettled = bet.settled === BET_STATUSES.SETTLED;
    const isRedCard = cards > 0;

    return this.getTotalRedCardsStatus(isRedCard, isYesOutcome, betSettled);
  }

  /**
   * Calculate status for Red Card by parameters
   * @param isRedCard
   * @param isYesOutcome
   * @param isBetSettled
   */
  protected getTotalRedCardsStatus(isRedCard: boolean, isYesOutcome: boolean, isBetSettled: boolean): string {
    let status;

    if (isRedCard && !isBetSettled) {
      status = isYesOutcome ? STATUSES.WINNING : STATUSES.LOSING ;
    } else if (isRedCard && isBetSettled) {
      status = isYesOutcome ? STATUSES.WON : STATUSES.LOSE;
    } else if (!isBetSettled) {
      status = isYesOutcome ? STATUSES.LOSING : STATUSES.WINNING;
    } else {
      status = isYesOutcome ? STATUSES.LOSE : STATUSES.WON;
    }

    return status;
  }

  /**
   * Calculate status for Red Card(1st Half) by parameters
   * @param isRedCard
   * @param isYesOutcome
   * @param isBetSettled
   * @param period
   */
  protected getFirstHalfRedCardsStatus(isRedCard: boolean, isYesOutcome: boolean, isBetSettled: boolean, period: string): string {
    let status = '';
    const currentPeriod = this.statCategoryUtilityService.getCurrentPeriod(period);
    const isFirstHalf = this.isFirstHalf(currentPeriod);

    if (isRedCard && !isBetSettled && isFirstHalf) {
      status = isYesOutcome ? STATUSES.WINNING : STATUSES.LOSING ;
    } else if (isRedCard && isBetSettled) {
      status = isYesOutcome ? STATUSES.WON : STATUSES.LOSE;
    } else if (!isBetSettled && isFirstHalf) {
      status = isYesOutcome ? STATUSES.LOSING : STATUSES.WINNING;
    } else {
      status = isYesOutcome ? STATUSES.LOSE : STATUSES.WON;
    }

    return status;
  }

  /**
   * Get player status and progress base on opta statistics
   * @param {IBybSelection} selection
   * @param {IScoreboardStatsUpdate} update
   * @param {IBetHistoryBet} bet
   * @param {string} stat - depends on PLAYER_STATS
   * @param {string} playerShots - depends on PLAYER_SHOTS
   */
  protected getPlayerStatusAndProgress(
    selection: IBybSelection,
    update: IScoreboardStatsUpdate,
    bet: IBetHistoryBet,
    stat: string,
    playerShots?: string
  ): IBybSelectionStatus {
    const goalsObj = this.getStatCategoryObj(selection, update);
    const { playerId, statValue } = this.getExternalStatsLink(selection);
    const betSettled = bet.settled === BET_STATUSES.SETTLED;
    const optaStatValue = this.getOptaStatValue(goalsObj, playerId, stat, playerShots);
    if (optaStatValue === null) { // if no player and respective stat found, then no status
      return { status: '' };
    }

    const selectedValue = this.parseStatValue(statValue);
    const status = this.getOverTotalMarketsStatus(optaStatValue, selectedValue , betSettled);
    const progress = this.getSelectionProgress(selection, bet, +optaStatValue);

    return { status, progress };
  }

  /**
   * Get opta stat value
   * @param {IPlayersSimple} goalsObj
   * @param {string} playerId
   * @param {string} stat - depends on PLAYER_STATS
   * @param {string} playerShots - depends on PLAYER_SHOTS
   */
  protected getOptaStatValue(goalsObj: IPlayersSimple, playerId: string, stat: string, playerShots?: string): number {
    if (
      goalsObj &&
      (goalsObj.home && goalsObj.home.length ||
        goalsObj.away && goalsObj.away.length)
    ) {
      const player = goalsObj.home.find((p: IPlayer) => p.providerId === playerId) ||
        goalsObj.away.find((p: IPlayer) => p.providerId === playerId);

      if (player) {
        const playerStat = player[stat];
        return stat === PLAYER_STATS.SHOTS ? playerStat && playerStat[playerShots] : playerStat;
      }
    }

    return null;
  }

  /**
   * Get TOTAL/2nd HALF UNDER Markets status
   * @param currentValue
   * @param target
   * @param betSettled
   * @param isStrictConditions
   */
  protected getUnderTotalMarketsStatus(currentValue: number, target: number, betSettled: boolean, isStrictConditions?: boolean): string {
    const condition = isStrictConditions ? target > currentValue : target >= currentValue;
    if (condition && betSettled) {
      return STATUSES.WON;
    } else if (condition && !betSettled) {
      return STATUSES.WINNING;
    }

    return STATUSES.LOSE;
  }

  /**
   * Get First Half UNDER Markets status
   * @param currentValue
   * @param target
   * @param betSettled
   * @param period
   */
  protected getUnderFirstHalfMarketsStatus(currentValue: number, target: number, betSettled: boolean, period: string): string {
    const currentPeriod = this.statCategoryUtilityService.getCurrentPeriod(period);
    const isFirstHalf = this.isFirstHalf(currentPeriod);
    const isSecondHalfOrTotal = this.isSecondHalfOrTotal(currentPeriod);

    if (target >= currentValue && (betSettled || isSecondHalfOrTotal)) {
      return STATUSES.WON;
    } else if (target >= currentValue && !betSettled && isFirstHalf) {
      return STATUSES.WINNING;
    }

    return STATUSES.LOSE;
  }

  /**
   * Get TOTAL/2nd HALF OVER Markets status
   * @param currentValue
   * @param target
   * @param betSettled
   * @param isStrictConditions
   */
  protected getOverTotalMarketsStatus(currentValue: number, target: number, betSettled: boolean, isStrictConditions?: boolean): string {
    const conditionMore = isStrictConditions ? currentValue > target : currentValue >= target;
    const conditionLess = isStrictConditions ? currentValue <= target : currentValue < target;
    if (conditionLess && !betSettled) {
      return STATUSES.LOSING;
    } else if (conditionMore) {
      return STATUSES.WON;
    }

    return STATUSES.LOSE;
  }

  /**
   * Get First Half OVER Markets status
   * @param currentValue
   * @param target
   * @param betSettled
   * @param period
   */
  protected getOverFirstHalfMarketsStatus(currentValue: number, target: number, betSettled: boolean, period: string): string {
    const currentPeriod = this.statCategoryUtilityService.getCurrentPeriod(period);
    const isFirstHalf = this.isFirstHalf(currentPeriod);

    if (currentValue < target && !betSettled && isFirstHalf) {
      return STATUSES.LOSING;
    } else if (currentValue >= target) {
      return STATUSES.WON;
    }

    return STATUSES.LOSE;
  }

  /**
   * Get TOTAL/2nd HALF EXACT Markets status
   * @param current
   * @param target
   * @param betSettled
   */
  protected getEqualTotalMarketsStatus(current: number, target: number, betSettled: boolean): string {
    if (current === target && betSettled) {
      return STATUSES.WON;
    } else if (current === target) {
      return  STATUSES.WINNING;
    } else if (target > current && !betSettled) {
      return  STATUSES.LOSING;
    }

    return  STATUSES.LOSE;
  }

  /**
   * Get 1ST HALF EXACT Markets status
   * @param current
   * @param target
   * @param betSettled
   * @param period
   */
  protected getEqualFirstHalfMarketsStatus(current: number, target: number, betSettled: boolean, period: string): string {
    const currentPeriod = this.statCategoryUtilityService.getCurrentPeriod(period);
    const isFirstHalf = this.isFirstHalf(currentPeriod);

    if (current === target && (betSettled || !isFirstHalf)) {
      return STATUSES.WON;
    } else if (current === target && isFirstHalf) {
      return  STATUSES.WINNING;
    } else if (target > current && isFirstHalf && !betSettled) {
      return  STATUSES.LOSING;
    }

    return  STATUSES.LOSE;
  }

  /**
   * Get status for No Cards selection(TO BE SEND OFF market)
   * @param update
   * @param betSettled
   */
  protected getNoRedCardsSelectionStatus(update: IScoreboardStatsUpdate, betSettled: boolean): string {
    const isHomeRedCards = update.cards.home.find(card => card.type === 'red');
    const isAwayRedCards = update.cards.away.find(card => card.type === 'red');
    const isNoRedsCards = !isHomeRedCards && !isAwayRedCards;

    if (isNoRedsCards && !betSettled) {
      return STATUSES.WINNING;
    } else if (isNoRedsCards && betSettled) {
      return STATUSES.WON;
    }

    return this.getNoStatsCategorySelectionStatus(isNoRedsCards, betSettled);
  }

  /**
   * Get status for No Stats Category(e.g. red cards, goals etc) selection
   * @param condition
   * @param betSettled
   */
  protected getNoStatsCategorySelectionStatus(condition: boolean, betSettled: boolean): string {
    if (condition && !betSettled) {
      return STATUSES.WINNING;
    } else if (condition && betSettled) {
      return STATUSES.WON;
    }

    return STATUSES.LOSE;
  }

  /**
   * Get Ranged Market Status for '15:00' | '30:00' | '60:00' | '75:00' | '0 - 30' | '30 - 60' ranges
   * @param goalsStat
   * @param {boolean} periodReached
   * @param {string} team
   * @param {IBetHistoryBet} bet
   * @private
   * @returns {string}
   */
  protected getRangeMarketStatus(
    goalsStat: { [key: string]: IGoalInfo[] }, periodReached: boolean, team: string, bet: IBetHistoryBet
  ): string {
    const betSettled = bet.settled === BET_STATUSES.SETTLED;
    const home = goalsStat[TEAMS.HOME].length;
    const away = goalsStat[TEAMS.AWAY].length;
    const conditions = {
      [TEAMS.DRAW]: home === away,
      [TEAMS.AWAY]: home < away,
      [TEAMS.HOME]: home > away
    };

    let status: string;

    if (conditions[team] && !betSettled && !periodReached) {
      status = STATUSES.WINNING;
    } else if (conditions[team] && (betSettled || periodReached)) {
      status = STATUSES.WON;
    } else if (!betSettled && !periodReached) {
      status = STATUSES.LOSING;
    } else {
      status = STATUSES.LOSE;
    }

    return status;
  }

  protected isFirstHalf(period: string): boolean {
    return period === PERIODS['1ST_HALF'];
  }

  protected isSecondHalfOrTotal(period: string): boolean {
    return period === PERIODS['2ND_HALF'] || period === PERIODS.total;
  }

  protected getExternalStatsLink(selection: IBybSelection): IExternalStatsLink {
    return (selection.part.outcome[0] as IBetHistoryOutcome).externalStatsLink;
  }

  /**
   * Checks if match time is greater than the given period
   * @param time - 'mm:ss'
   * @param period - One from Periods enum variable '15:00' | '30:00' | '60:00' | '75:00'
   * @private
   * @returns boolean
   */
  protected isPeriodReached(time: string, period: string): boolean {
    if (time === period) {  // e.g. 15:00 === 15:00
      return true;
    }

    const current = time.split(':').map(Number);
    const target = period.split(':').map(Number);

    if (current[0] > target[0]) { // e.g. 16:00 > 15:00
      return true;
    } else {  // e.g. 15:01 > 15:00
      return current[0] === target[0] && current[1] > target[1];
    }
  }

  /**
   * Get status for 1ST/2ND half
   * @param scores
   * @param bet
   * @param update
   * @param selection
   */
  protected bothTeamsHalfScoredStatus(scores: IScore, bet: IBetHistoryBet,
                                      update: IScoreboardStatsUpdate, selection: IBybSelection): string {
    const { statValue } = this.getExternalStatsLink(selection);
    const betSettled = bet.settled === BET_STATUSES.SETTLED;
    const selectionPeriod = selection.config.period;
    const currentPeriod = this.statCategoryUtilityService.getCurrentPeriod(update.period);
    const isFirstHalf = this.isFirstHalf(currentPeriod);
    let status;

    if (scores[selectionPeriod] && !!scores[selectionPeriod].away && !!scores[selectionPeriod].home) {
      status = this.isUnder(statValue) ? STATUSES.LOSE : STATUSES.WON;
    } else if ((scores[selectionPeriod] && (scores[selectionPeriod].away === 0 || scores[selectionPeriod].home === 0)) &&
      ((selectionPeriod === PERIODS['1ST_HALF'] && (!isFirstHalf || betSettled))
        || (selectionPeriod === PERIODS['2ND_HALF'] && betSettled))) {
      status = this.isUnder(statValue) ?  STATUSES.WON : STATUSES.LOSE;
    } else if (betSettled && !scores[selectionPeriod]) {
      status = this.isUnder(statValue) ?  STATUSES.WON : STATUSES.LOSE;
    } else {
      status = this.isUnder(statValue)? STATUSES.WINNING : STATUSES.LOSING;
    }
    return status;
  }


  /**
   * Get status for both teams both halves
   * @param scores
   * @param bet
   * @param update
   * @param selection
   */
  protected bothTeamsBothHalves(scores: IScore, bet: IBetHistoryBet, update: IScoreboardStatsUpdate, selection: IBybSelection): string {
    let status;
    const { statValue } = this.getExternalStatsLink(selection);
    const betSettled = bet.settled === BET_STATUSES.SETTLED;
    const isYesOutcome = !this.isUnder(statValue);
    const scored = {
      [PERIODS['1ST_HALF']]: !!scores[PERIODS['1ST_HALF']].away && !!scores[PERIODS['1ST_HALF']].home,
      [PERIODS['2ND_HALF']]: scores[PERIODS['2ND_HALF']] && !!scores[PERIODS['2ND_HALF']].away && !!scores[PERIODS['2ND_HALF']].home
    };
    if (scored[PERIODS['1ST_HALF']] && scored[PERIODS['2ND_HALF']]) { // (1:1) and (1:1)
      status = isYesOutcome ? STATUSES.WON : STATUSES.LOSE;
      // IF  Selection = "No", period = "2h",  score: 1h: (0:1) AND score: 2h: (1:1), THEN Bet WON"
      // or betSettled and score: 2h: (1:0)
      // IF  Selection = "YES", period = "2h",  score: 1h: (0:1) , THEN Bet LOSE"
    } else if ((update.period !== PERIODS['1ST_HALF'] && !scored[PERIODS['1ST_HALF']]) || (!scored[PERIODS['2ND_HALF']] && betSettled)) {
      status = isYesOutcome ? STATUSES.LOSE : STATUSES.WON;
    } else {
      status = isYesOutcome ? STATUSES.LOSING : STATUSES.WINNING;
    }
    return status;
  }

  /**
   * Get match status for TEAM TO GET 2nd GOAL market or TEAM TO GET 1ST GOAL market
   * @param {IBybSelection} selection
   * @param {IScoreboardStatsUpdate} update
   * @param {IBetHistoryBet} bet
   * @param {Boolean} isFirstGoal
   * @returns {IBybSelectionStatus}
   */
  protected teamToGetFirstOrSecondGoalStatus(
    selection: IBybSelection,
    update: IScoreboardStatsUpdate,
    bet: IBetHistoryBet,
    isFirstGoal?: boolean
  ): IBybSelectionStatus {
    const allGoals = this.statCategoryUtilityService.getAllGoals(update);
    const team = this.getTeamByExternalStatsLink(selection, update) as string;
    const betSettled = bet.settled === BET_STATUSES.SETTLED;
    const isNoGoalCase = selection.title === TEAMS.NO_GOAL;
    const conditions = {
      [TEAMS.HOME]: this.isGoal(allGoals, isFirstGoal ? 0 : 1, TEAMS.HOME),
      [TEAMS.AWAY]: this.isGoal(allGoals, isFirstGoal ? 0 : 1, TEAMS.AWAY),
    };
    let status: string;

    if (isNoGoalCase) {
      status = this.getNoStatsCategorySelectionStatus(!allGoals.length, betSettled);
    } else
    if (conditions[team]) {
      status = STATUSES.WON;
    } else
    if (!betSettled && (!allGoals.length || (allGoals.length === 1 && !isFirstGoal))) {
      status = STATUSES.LOSING;
    } else {
      status = STATUSES.LOSE;
    }

    return { status };
  }

  /**
   * Checks first or second goal for the team
   * @param {IGoalInfo[]} allGoals
   * @param {number} goal
   * @param {TEAMS} team
   * @returns {boolean}
   */
  protected isGoal(allGoals: IGoalInfo[], goal: number, team: TEAMS): boolean {
    return allGoals.length && allGoals[goal].team === team;
  }

  /**
   * get status for win both halves
   * @param team
   * @param currentPeriod
   * @param conditionsFirst
   * @param conditionsSecond
   * @param isBetSettled
   */
  protected getWinBothHalvesStatus(team: string,
                                 currentPeriod: string,
                                 conditionsFirst: IConditions,
                                 conditionsSecond: IConditions,
                                 isBetSettled: boolean): string {
    switch (true) {
    case (currentPeriod === PERIODS['2ND_HALF']):
      if (!conditionsFirst[team]) {
        return STATUSES.LOSE;
      } else if (!isBetSettled) {
        return conditionsSecond[team] ? STATUSES.WINNING : STATUSES.LOSING;
      } else {
        return conditionsSecond[team] ? STATUSES.WON : STATUSES.LOSE;
      }
    case (currentPeriod === PERIODS.total || isBetSettled):
      return conditionsFirst[team] && conditionsSecond[team] ? STATUSES.WON : STATUSES.LOSE;
    default:
      // 1ND_HALF case
      return STATUSES.LOSING;
    }
  }

  /**
   * get status for win either half
   * @param team
   * @param currentPeriod
   * @param conditionsFirst
   * @param conditionsSecond
   * @param isBetSettled
   */
  protected getWinEitherHalfStatus(team: string,
                                 currentPeriod: string,
                                 conditionsFirst: IConditions,
                                 conditionsSecond: IConditions,
                                 isBetSettled: boolean): string {
    switch (true) {
    case (currentPeriod === PERIODS['2ND_HALF']):
      if (conditionsFirst[team]) {
        return STATUSES.WON;
      } else if (!isBetSettled) {
        return conditionsSecond[team] ? STATUSES.WINNING : STATUSES.LOSING;
      } else  {
        return conditionsSecond[team] ? STATUSES.WON : STATUSES.LOSE;
      }
    case (currentPeriod === PERIODS.total || isBetSettled):
      return conditionsFirst[team] || conditionsSecond[team] ? STATUSES.WON : STATUSES.LOSE;
    default:
      // 1ND_HALF case
      return conditionsFirst[team] ? STATUSES.WINNING : STATUSES.LOSING;
    }
  }

  /**
   * Checks first Booking for the team
   * @param {ICardsInfo[]} allCards
   * @param {TEAMS} team
   * @returns {boolean}
   */
  protected isFirstBooking(allCards: ICardsInfo[], team: TEAMS): boolean {
    return allCards.length && allCards[0].team === team;
  }
}
