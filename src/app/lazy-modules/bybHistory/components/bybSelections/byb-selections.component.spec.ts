import { of, Subscription } from 'rxjs';

import { BybSelectionsComponent } from './byb-selections.component';
import { ISportEvent } from '@core/models/sport-event.model';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';
import { IScoreboardStatsUpdate } from '@bybHistoryModule/models/scoreboards-stats-update.model';
import { IBybSelection } from '@bybHistoryModule/models/byb-selection.model';

describe('BybSelectionsComponent', () => {
  let component: BybSelectionsComponent;
  let bybSelectionsService;
  let handleScoreboardsStatsUpdatesService;
  let betTrackingService;
  let changeDetectorRef;
  let pubSubService;
  let timeService;
  let coreToolsService;
  let deviceService;

  let callbackHandler;
  let initBetTrackerHandler;

  beforeEach(() => {
    callbackHandler = (ctrlName: string, eventName: string, callback) => {
      if (eventName === 'EVENT_STARTED') {
        initBetTrackerHandler = callback;
      }
    };

    bybSelectionsService = {
      getSortedSelections: jasmine.createSpy('getSortedSelections')
    };
    handleScoreboardsStatsUpdatesService = {
      subscribeForUpdates: jasmine.createSpy('subscribeForUpdates'),
      unsubscribe: jasmine.createSpy('unsubscribe')
    };
    betTrackingService = {
      isTrackingEnabled: jasmine.createSpy('isTrackingEnabled').and.returnValue(of(true)),
      updateProgress: jasmine.createSpy('updateProgress'),
      extendSelectionsWithTrackingConfig: jasmine.createSpy('extendSelectionsWithTrackingConfig')
    };
    changeDetectorRef = {
      markForCheck: jasmine.createSpy('markForCheck')
    };
    pubSubService = {
      subscribe: jasmine.createSpy('subscribe').and.callFake(callbackHandler),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      publishSync: jasmine.createSpy('publishSync'),
      API: pubSubApi
    };
    timeService = {
      daysDifference: jasmine.createSpy('daysDifference').and.returnValue(-2)
    };
    coreToolsService = {
      uuid: jasmine.createSpy('uuid').and.returnValue('1111')
    };
    deviceService = {
      isDesktop: false
    };

    component = new BybSelectionsComponent(
      bybSelectionsService,
      handleScoreboardsStatsUpdatesService,
      betTrackingService,
      changeDetectorRef,
      pubSubService,
      timeService,
      coreToolsService,
      deviceService
    );
    component.leg = {
      eventEntity: { name: 'event name', id: '123456' },
      backupEventEntity: { name: 'backup event name', id: '123456' },
      part: [{ description: 'event', eventMarketDesc: 'event' }]
    } as any;

    component.bet = {} as any;
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      component.bet = {
        event: ['123456'],
        id: '123'
      } as any;
    });

    it('should prepare selections', () => {
      component.ngOnInit();

      expect(bybSelectionsService.getSortedSelections).toHaveBeenCalledWith(component.leg);
      expect(component['subscriptionName']).toEqual('BybSelectionsComponent123-1111');
      expect(pubSubService.subscribe).toHaveBeenCalledTimes(3);
      expect(pubSubService.subscribe).toHaveBeenCalledWith('BybSelectionsComponent123-1111', 'UPDATE_BYB_BET', jasmine.any(Function));
      expect(pubSubService.subscribe).toHaveBeenCalledWith('BybSelectionsComponent123-1111', 'CLOSE_TOOLTIPS', jasmine.any(Function));
    });

    it('should betTrackingEnabled to be true', () => {
      component.leg.eventEntity = {
        isStarted: true,
        id: 1234
      } as ISportEvent;
      component.ngOnInit();

      expect(betTrackingService.isTrackingEnabled).toHaveBeenCalled();
      expect(betTrackingService.extendSelectionsWithTrackingConfig).toHaveBeenCalled();
      expect(handleScoreboardsStatsUpdatesService.subscribeForUpdates).toHaveBeenCalledWith('1234');
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
      expect(component.betTrackingEnabled).toBeTruthy();
    });

    it('should betTrackingEnabled to be true when bet is settled', () => {
      component.leg.eventEntity = {
        isStarted: false,
        id: 1234
      } as ISportEvent;
      component.bet.settled = 'Y';

      component.ngOnInit();

      expect(betTrackingService.isTrackingEnabled).toHaveBeenCalled();
      expect(betTrackingService.extendSelectionsWithTrackingConfig).toHaveBeenCalled();
      expect(handleScoreboardsStatsUpdatesService.subscribeForUpdates).toHaveBeenCalledWith('1234');
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
      expect(component.betTrackingEnabled).toBeTruthy();
    });

    it('should betTrackingEnabled to be false when bet is not settled and event is not started', () => {
      component.leg.eventEntity = {
        isStarted: false,
        id: 1234
      } as ISportEvent;
      component.bet.settled = 'N';

      component.ngOnInit();

      expect(betTrackingService.isTrackingEnabled).toHaveBeenCalled();
      expect(betTrackingService.extendSelectionsWithTrackingConfig).not.toHaveBeenCalled();
      expect(handleScoreboardsStatsUpdatesService.subscribeForUpdates).not.toHaveBeenCalledWith('1234');
      expect(changeDetectorRef.markForCheck).not.toHaveBeenCalled();
      expect(component.betTrackingEnabled).toBeFalsy();
    });

    it('should betTrackingEnabled to be false', () => {
      betTrackingService.isTrackingEnabled = jasmine.createSpy('isTrackingEnabled').and.returnValue(of(false));
      component.ngOnInit();

      expect(betTrackingService.isTrackingEnabled).toHaveBeenCalled();
      expect(component.betTrackingEnabled).toBeFalsy();
    });

    it('should create event entity from leg backupEventEntity ', function () {
      delete component.leg.eventEntity;
      component.ngOnInit();

      expect(component.eventEntity).toEqual({ name: 'backup event name', id: '123456' } as any);
    });

    it('should init bet tracking on subscription', () => {
      component['initBetTracking'] = jasmine.createSpy('initBetTracking');
      component.bet = {
        event: ['123456']
      } as any;
      component.ngOnInit();

      initBetTrackerHandler('123456');

      expect(component['initBetTracking']).toHaveBeenCalledTimes(2);
      expect(component.eventEntity.isStarted).toBeTruthy();
    });

    it('should not init bet tracking on subscription', () => {
      component['initBetTracking'] = jasmine.createSpy('initBetTracking');
      component.bet = {
        event: ['123456']
      } as any;
      component.ngOnInit();

      initBetTrackerHandler('654321');

      expect(component['initBetTracking']).toHaveBeenCalledTimes(1);
    });
  });

  describe('ngOnDestroy', () => {
    beforeEach(() => {
      component.eventEntity = { name: 'event name', id: '123456' } as any;
    });

    it('should unsubscribe from betTrackingEnabled', () => {
      component['betTrackingEnabledSubscription'] = new Subscription();
      component['betTrackingEnabledSubscription'].unsubscribe = jasmine.createSpy();

      component.ngOnDestroy();

      expect(component['betTrackingEnabledSubscription'].unsubscribe).toHaveBeenCalled();
    });

    it('should not unsubscribe from betTrackingEnabled', () => {
      component['betTrackingEnabledSubscription'] = undefined;

      component.ngOnDestroy();

      expect(component['betTrackingEnabledSubscription']).not.toBeDefined();
    });

    it('should unsubscribe from betTrackingEnabled', () => {
      component.ngOnDestroy();

      expect(pubSubService.unsubscribe).toHaveBeenCalled();
    });

    it('should unsubscribe from scoreboards live updates', () => {
      component.betTrackingEnabled = true;
      component.ngOnDestroy();

      expect(handleScoreboardsStatsUpdatesService.unsubscribe).toHaveBeenCalledWith('123456');
    });

    it('should Not unsubscribe from scoreboards live updates when subscription not found', () => {
      component.betTrackingEnabled = false;
      component.ngOnDestroy();
      expect(handleScoreboardsStatsUpdatesService.unsubscribe).not.toHaveBeenCalled();
    });
  });

  it('should trackBySelectionId', () => {
    const selection = {
      part: {
        outcome: {
          id: '123456'
        }
      }
    } as any;

    expect(component.trackBySelectionId(1, selection)).toEqual('123456');
  });

  it('should updateTrackingParameters', () => {
    component.selections = {
      part: {
        outcome: {
          id: '123456'
        }
      }
    } as any;
    component.bet = {} as any;
    component['updateTrackingParameters']({} as any);

    expect(betTrackingService.updateProgress).toHaveBeenCalledWith(component.selections, component.bet, {});
  });

  describe('onLiveUpdateHandler', () => {
    it('should handle update', () => {
      component['eventEntity'] = { id: 123 } as any;
      component['onLiveUpdateHandler']({ obEventId: '123' } as IScoreboardStatsUpdate);

      expect(betTrackingService.updateProgress).toHaveBeenCalled();
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    });

    it('should not handle update when id is not from current event', () => {
      component['eventEntity'] = { id: 123 } as any;
      component['onLiveUpdateHandler']({ obEventId: '123123' } as IScoreboardStatsUpdate);

      expect(betTrackingService.updateProgress).not.toHaveBeenCalled();
      expect(changeDetectorRef.markForCheck).not.toHaveBeenCalled();
    });

    it('should not handle update', () => {
      component['onLiveUpdateHandler'](null);

      expect(betTrackingService.updateProgress).not.toHaveBeenCalled();
      expect(changeDetectorRef.markForCheck).not.toHaveBeenCalled();
    });
  });

  describe('isBetSettledWithin24hours', () => {
    it('should check if bet is settled and event start time is less than 24 hours ago.', () => {
      const event = {
        startTime: '2020-08-12T16:57:00Z',
      } as ISportEvent;
      component.betSettled = true;

      const actualResult = component['isBetSettledWithin24hours'](event);

      expect(timeService.daysDifference).toHaveBeenCalledWith(event.startTime);
      expect(actualResult).toBeTruthy();
    });

    it('should check if bet is settled and event start time is less than 24 hours ago(negative case)', () => {
      const event = {
        startTime: '2020-08-12T16:57:00Z',
      } as ISportEvent;
      component.betSettled = false;

      const actualResult = component['isBetSettledWithin24hours'](event);

      expect(timeService.daysDifference).toHaveBeenCalledWith(event.startTime);
      expect(actualResult).toBeFalsy();
    });

    it('should check if bet is settled and event start time is less than 24 hours ago(negative)', () => {
      const event = {
        startTime: '2020-08-12T16:57:00Z',
      } as ISportEvent;
      timeService.daysDifference.and.returnValue(2);
      component.betSettled = true;

      const actualResult = component['isBetSettledWithin24hours'](event);

      expect(timeService.daysDifference).toHaveBeenCalledWith(event.startTime);
      expect(actualResult).toBeFalsy();
    });
  });

  describe('toggleTooltip', () => {
    it('should show tooltip', () => {
      const event = {
        stopPropagation: jasmine.createSpy('stopPropagation'),
        srcElement: {
          getBoundingClientRect: jasmine.createSpy('getBoundingClientRect').and.returnValue({ left: 50 })
        }
      } as any;
      const selection = { showTooltip: false } as IBybSelection;


      component.toggleTooltip(event, selection);

      expect(selection.showTooltip).toBeTruthy();
      expect(component['infoTooltipIconXPosition']).toEqual(50);
    });

    it('should close all other tooltips and show tooltip which has been clicked', () => {
      const event = {
        stopPropagation: jasmine.createSpy('stopPropagation'),
        srcElement: {
          getBoundingClientRect: jasmine.createSpy('getBoundingClientRect').and.returnValue({ left: 50 })
        }
      } as any;
      const selection = { showTooltip: false } as IBybSelection;
      deviceService.isDesktop = true;

      component.toggleTooltip(event, selection);

      expect(selection.showTooltip).toBeTruthy();
      expect(pubSubService.publishSync).toHaveBeenCalledWith('CLOSE_TOOLTIPS');
      expect(component['infoTooltipIconXPosition']).toEqual(50);
    });

    it('should hide tooltip on Desktop', () => {
      const event = {
        stopPropagation: jasmine.createSpy('stopPropagation'),
        srcElement: {
          getBoundingClientRect: jasmine.createSpy('getBoundingClientRect').and.returnValue({ left: 50 })
        }
      } as any;
      const selection = { showTooltip: true } as IBybSelection;
      deviceService.isDesktop = true;

      component.toggleTooltip(event, selection);

      expect(selection.showTooltip).toBeFalsy();
      expect(pubSubService.publishSync).not.toHaveBeenCalled();
      expect(component['infoTooltipIconXPosition']).toEqual(50);
    });

    it('should Not show tooltip when event is not found', () => {
      const event = undefined;
      const selection = { showTooltip: false } as IBybSelection;


      component.toggleTooltip(event, selection);

      expect(selection.showTooltip).toBeFalsy();
      expect(component['infoTooltipIconXPosition']).toEqual(undefined);
    });
  });

  describe('closeSelectionsTooltips', () => {
    it('should close all selections tooltips when user tap away from tooltip', () => {
      component.selections = [{showTooltip: true} as IBybSelection];
      component['closeSelectionsTooltips']();

      expect(component.selections[0].showTooltip).toBeFalsy();
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    });
  });
});
