import { Component, Input, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { ISportEvent } from '@core/models/sport-event.model';
import { IBetHistoryLeg, IBetHistoryBet } from '@betHistoryModule/models/bet-history.model';
import { IBybSelection } from '@lazy-modules/bybHistory/models/byb-selection.model';
import { BybSelectionsService } from '@lazy-modules/bybHistory/services/bybSelectionsService/byb-selections.service';
import { BetTrackingService } from '@lazy-modules/bybHistory/services/betTracking/bet-tracking.service';
import { IScoreboardStatsUpdate } from '@lazy-modules/bybHistory/models/scoreboards-stats-update.model';
import {
  HandleScoreboardsStatsUpdatesService
} from '@lazy-modules/bybHistory/services/handleScoreboardsStatsUpdates/handle-scoreboards-stats-updates.service';
import { TimeService } from '@core/services/time/time.service';
import { BindDecorator } from '@core/decorators/bind-decorator/bind-decorator.decorator';
import { CoreToolsService } from '@core/services/coreTools/core-tools.service';
import { UsedFromWidgetAbstractComponent } from '@core/abstract-components/used-from-widget-abstract.component';
import { DeviceService } from '@core/services/device/device.service';

@Component({
  selector: 'byb-selections',
  templateUrl: './byb-selections.component.html',
  styleUrls: ['./byb-selections.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BybSelectionsComponent extends UsedFromWidgetAbstractComponent implements OnInit, OnDestroy {
  @Input() leg: IBetHistoryLeg;
  @Input() bet: IBetHistoryBet;

  selections: IBybSelection[];
  betTrackingEnabled: boolean;
  betSettled: boolean;
  eventEntity: ISportEvent;
  infoTooltipIconXPosition: number;

  private betTrackingEnabledSubscription: Subscription;
  private subscriptionName: string;

  constructor(
    private bybSelectionsService: BybSelectionsService,
    private handleScoreboardsStatsUpdatesService: HandleScoreboardsStatsUpdatesService,
    private betTrackingService: BetTrackingService,
    private changeDetectorRef: ChangeDetectorRef,
    private pubSubService: PubSubService,
    private timeService: TimeService,
    private coreToolsService: CoreToolsService,
    private deviceService: DeviceService
  ) {
    super();
  }

  ngOnInit(): void {
    this.betSettled = this.bet.settled === 'Y';
    this.eventEntity = this.leg.eventEntity || this.leg.backupEventEntity;
    this.selections = this.bybSelectionsService.getSortedSelections(this.leg);
    this.initBetTracking();
    this.subscriptionName = `BybSelectionsComponent${this.bet.id}-${this.coreToolsService.uuid()}`;

    this.pubSubService.subscribe(
      this.subscriptionName,
      this.pubSubService.API.EVENT_STARTED, (eventID: string) => {
        if(this.bet.event.includes(eventID)) {
          this.eventEntity.isStarted = true;
          this.initBetTracking();
        }
    });
    this.pubSubService.subscribe(this.subscriptionName, this.pubSubService.API.UPDATE_BYB_BET, this.onLiveUpdateHandler);
    this.pubSubService.subscribe(this.subscriptionName, this.pubSubService.API.CLOSE_TOOLTIPS, this.closeSelectionsTooltips);
  }

  ngOnDestroy(): void {
    this.betTrackingEnabledSubscription && this.betTrackingEnabledSubscription.unsubscribe();
    this.betTrackingEnabled && this.handleScoreboardsStatsUpdatesService.unsubscribe(this.eventEntity.id.toString());
    this.pubSubService.unsubscribe(this.subscriptionName);
  }

  trackBySelectionId(index: number, selection: IBybSelection): string {
    return `${selection.part.outcome.id}`;
  }

  /**
   * Show/hide tooltip
   * @param event
   * @param selection
   */
  toggleTooltip(event: MouseEvent, selection: IBybSelection): void {
    if (event) {
      event.stopPropagation();

      this.infoTooltipIconXPosition = event.srcElement.getBoundingClientRect().left;
      selection.showTooltip = !selection.showTooltip;
      if (this.deviceService.isDesktop && selection.showTooltip) {
        this.pubSubService.publishSync(this.pubSubService.API.CLOSE_TOOLTIPS);
        selection.showTooltip = true;
      }
    }
  }

  private initBetTracking(): void {
    this.betTrackingEnabledSubscription = this.betTrackingService.isTrackingEnabled().subscribe((enabled: boolean) => {
      if (enabled && (this.eventEntity.isStarted || this.isBetSettledWithin24hours(this.eventEntity))) {
        this.betTrackingEnabled = enabled;

        this.betTrackingService.extendSelectionsWithTrackingConfig(this.selections);
        this.handleScoreboardsStatsUpdatesService.subscribeForUpdates(this.eventEntity.id.toString());
        this.changeDetectorRef.markForCheck();
      }
    });
  }

  private updateTrackingParameters(update: IScoreboardStatsUpdate): void {
    this.betTrackingService.updateProgress(this.selections, this.bet, update);
  }

  /**
   * Handle scoreboards stats live update
   */
  @BindDecorator
  private onLiveUpdateHandler(update: IScoreboardStatsUpdate): void {
    if (update && String(this.eventEntity.id) === update.obEventId) {
      this.updateTrackingParameters(update);
      this.changeDetectorRef.markForCheck();
    }
  }

  /**
   * Check if bet is settled and event start time is less than 24 hours ago.
   * @param event
   */
  private isBetSettledWithin24hours(event: ISportEvent): boolean {
    // Checks if event start time is less than 24 hours ago.
    const days = this.timeService.daysDifference(event.startTime);

    return this.betSettled && days < 1;
  }

  /**
   * Close all selections tooltips when user tap away from tooltip
   */
  @BindDecorator
  private closeSelectionsTooltips(): void {
    this.selections.forEach((sel: IBybSelection) => {
      sel.showTooltip = false;
    });
    this.changeDetectorRef.markForCheck();
  }
}
