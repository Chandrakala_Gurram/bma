import { OptaInfoPopupComponent } from './opta-info-popup.component';
import { AbstractDialog } from '@shared/components/oxygenDialogs/abstract-dialog';

describe('OptaInfoPopupComponent', () => {
  let component: OptaInfoPopupComponent;
  let device, windowRef, changeDetectorRef;
  beforeEach(() => {
    device = {};
    windowRef = {};
    changeDetectorRef = {
      markForCheck: jasmine.createSpy('markForCheck')
    };
    component = new OptaInfoPopupComponent(device, windowRef, changeDetectorRef);
  });
  describe('ngOnInit', () => {
    it('should create component instance', () => {
      expect(component).toBeTruthy();
    });

    it('should open popup', () => {
      spyOn(AbstractDialog.prototype, 'open');
      component.open();

      expect(AbstractDialog.prototype.open).toHaveBeenCalled();
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    });
  });
});
