import { Component, ViewChild, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

import { AbstractDialog } from '@shared/components/oxygenDialogs/abstract-dialog';
import { DeviceService } from '@core/services/device/device.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';


@Component({
  selector: 'opta-info-popup',
  templateUrl: './opta-info-popup.component.html',
  styleUrls: ['./opta-info-popup.component.less'],
  encapsulation : ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptaInfoPopupComponent extends AbstractDialog {

  @ViewChild('optaInfoPopup') dialog;

  constructor(
    device: DeviceService,
    windowRef: WindowRefService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    super(device, windowRef);
  }

  open(): void {
    super.open();
    this.changeDetectorRef.markForCheck();
  }
}
