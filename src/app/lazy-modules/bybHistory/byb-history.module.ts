import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { ModalModule } from 'angular-custom-modal';

import { BybSelectionsComponent } from '@lazy-modules/bybHistory/components/bybSelections/byb-selections.component';
import { BetStatusIndicatorComponent } from '@lazy-modules/bybHistory/components/betStatusIndicator/bet-status-indicator.component';
import { ProgressBarComponent } from '@lazy-modules/bybHistory/components/progressBar/progress-bar.component';
import { SharedModule } from '@sharedModule/shared.module';
import { OptaInfoComponent } from '@lazy-modules/bybHistory/components/optaInfo/opta-info.component';
import { AlignTooltipDirective } from '@bybHistoryModule/directives/align-tooltip.directive';

@NgModule({
  imports: [
    SharedModule,
    ModalModule
  ],
  declarations: [
    BybSelectionsComponent,
    BetStatusIndicatorComponent,
    ProgressBarComponent,
    OptaInfoComponent,
    AlignTooltipDirective
  ],
  entryComponents: [
    BybSelectionsComponent,
    OptaInfoComponent
  ],
  providers: [],
  exports: [
    BybSelectionsComponent,
    OptaInfoComponent,
    AlignTooltipDirective
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class LazyBybHistoryModule {
  static entry = { BybSelectionsComponent, OptaInfoComponent };
}
