import { Component, OnInit, HostListener, ChangeDetectionStrategy } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

import { CasinoGamesService } from '@app/vanillaInit/services/casinoGames/casino-games.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { RPG_CONSTANTS } from './rpg.constants';

@Component({
  selector: 'rpg',
  templateUrl: './rpg.component.html',
  styleUrls: ['./rpg.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RpgComponent implements OnInit {

  rpgUrl: SafeResourceUrl;
  rpgTitle = '';
  seeAllUrl = '';
  frameHeight = 0;
  showContent: boolean = false;

  constructor(
    private casinoGamesService: CasinoGamesService,
    private sanitizer: DomSanitizer,
    private windowRef: WindowRefService
    ) { }

  ngOnInit(): void {
    this.rpgUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.casinoGamesService.recentlyPlayedGamesUrl);
    this.seeAllUrl = this.casinoGamesService.seeAllUrl;
  }


  /**
   * listener for post messages from RPG iframe
   * @param event {MessageEvent} post message from iframe
   */
  @HostListener('window:message', ['$event'])
  onMessage(event: MessageEvent): void {
    this.handleMessage(event);
  }

  /**
   * defines eventName and calls corresponding eventName handler
   * @param event {MessageEvent} contain params for RPG
   */
  handleMessage(event: MessageEvent): void {
    const eventName = event.data.eventName;
    switch (eventName) {
      case RPG_CONSTANTS.EVENT_NAME.LOBBY_LOADED:
        this.openIFrame(event.data.params.displayName, event.data.params.height);
        break;
      case RPG_CONSTANTS.EVENT_NAME.GAME_LAUNCH:
        this.windowRef.nativeWindow.location.href = event.data.params.redirectUrl;
        break;
      case RPG_CONSTANTS.EVENT_NAME.ERROR:
        this.showContent = false;
        break;
    }
  }

  /**
   * If post message LOBBY_LOADED, show content and apply title, height
   * @param title {string} title of RPG container
   * @param height {number} height of iframe
   */
  openIFrame(title: string, height: number): void {
    this.rpgTitle = title;
    this.frameHeight = height;
    this.showContent = true;
  }

}
