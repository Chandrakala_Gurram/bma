export const RPG_CONSTANTS = {
  EVENT_NAME: {
    LOBBY_LOADED: 'LobbyLoaded',
    GAME_LAUNCH: 'GameLaunch',
    ERROR: 'Error',
  }
};
