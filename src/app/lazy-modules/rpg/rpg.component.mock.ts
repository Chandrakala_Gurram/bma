import { IRpgConfig, IGamesResponse } from '@featured/models/rpg.model';

export const rpgResponseMock: IGamesResponse = {
  baseUrl: 'http://src-gaming.coral.co.uk:80/igaming/games-info/',
  games: {
    '1' : { name: 'Game 1', imageURL: 'url-1', gameCode: '1'},
    '2' : { name: 'Game 2', imageURL: 'url-2', gameCode: '2'}
  }
};

export const rpgConfigMock: IRpgConfig = {
  title: 'Recently Played Games',
  seeMoreLink: 'https://gaming.coral.co.uk',
  gamesAmount: 5
};
