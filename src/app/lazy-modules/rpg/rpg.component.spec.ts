import { RpgComponent } from './rpg.component';

describe('RpgComponent', () => {
  let component, casinoGamesService, sanitizer, windowRef;
  const recentlyPlayedGamesUrl = 'recentlyPlayedGamesUrl';
  const seeAllUrl = 'seeAllUrl';

  beforeEach(() => {

    sanitizer = {
      bypassSecurityTrustResourceUrl: jasmine.createSpy('sanitizer.bypassSecurityTrustResourceUrl')
        .and.returnValue(recentlyPlayedGamesUrl)
    };

    casinoGamesService = {
      recentlyPlayedGamesUrl,
      seeAllUrl
    };

    windowRef = {
      nativeWindow: {
        location: {
          href: ''
        }
      }
    };

    component = new RpgComponent(
      casinoGamesService,
      sanitizer,
      windowRef
    );
  });

  it('should create a component', () => {
    expect(component).toBeTruthy();
  });

  it('#ngOnInit', () => {
    component.ngOnInit();

    expect(sanitizer.bypassSecurityTrustResourceUrl).toHaveBeenCalledWith(recentlyPlayedGamesUrl);
    expect(component.rpgUrl).toBe(recentlyPlayedGamesUrl);
    expect(component.seeAllUrl).toBe(seeAllUrl);
  });

  it('#onMessage should call handleMessage method', () => {
    const event = {} as MessageEvent;
    component['handleMessage'] = jasmine.createSpy('component.handleMessage');
    component.onMessage(event);

    expect(component['handleMessage']).toHaveBeenCalledWith(event);
  });

  describe('#handleMessage', () => {
    it('LobbyLoaded event - should call openIFrame with params',  () => {
      const event = {
        data: {
          eventName: 'LobbyLoaded',
          params: {
            height: 100,
            displayName: 'iframe'
          }
        }
      } as MessageEvent;
      component['openIFrame'] = jasmine.createSpy('component.openIFrame');
      component.handleMessage(event);

      expect(component['openIFrame']).toHaveBeenCalledWith(event.data.params.displayName, event.data.params.height);
    });

    it('GameLaunch event - should redirect to url',  () => {
      const event = {
        data: {
          eventName: 'GameLaunch',
          params: {
            promptLogin: false,
            redirectUrl: 'redirectUrl'
          }
        }
      } as MessageEvent;
      component.handleMessage(event);

      expect(windowRef.nativeWindow.location.href).toEqual(event.data.params.redirectUrl);
    });

    it('Error event - should hide iframe',  () => {
      component.showContent = true;
      const event = {
        data: {
          eventName: 'Error',
        }
      } as MessageEvent;
      component.handleMessage(event);

      expect(component.showContent).toBeFalsy();
    });

  });

  it('#openIFrame should apply params and show content',  () => {
    const title = 'iframe',
      height = 100;
    component.openIFrame(title, height);

    expect(component.showContent).toBeTruthy();
    expect(component.rpgTitle).toBe(title);
    expect(component.frameHeight).toBe(height);
  });

});
