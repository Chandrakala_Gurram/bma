import { NextRacesHomeTabComponent } from './next-races-home-tab.component';

describe('NextRacesHomeTabComponent', () => {
  let component: NextRacesHomeTabComponent;

  beforeEach(() => {
    component = new NextRacesHomeTabComponent();
  });
  describe('toggleLoader', () => {
    it('should toggle showLoader property', () => {
      expect(component['showLoader']).toEqual(true);
      component.toggleLoader(true);
      expect(component['showLoader']).toEqual(false);
      component.toggleLoader(false);
      expect(component['showLoader']).toEqual(true);
      component.toggleLoader();
      expect(component['showLoader']).toEqual(true);
    });
  });
});
