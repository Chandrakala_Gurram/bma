import { Component, OnInit, OnDestroy, Input, ViewEncapsulation } from '@angular/core';

import { IMarket } from '@core/models/market.model';
import { FiltersService } from '@core/services/filters/filters.service';
import { SbFiltersService } from '@sb/services/sbFilters/sb-filters.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { ISilkStyleModel } from '@core/services/raceOutcomeDetails/silk-style.model';
import { IOutcome } from '@core/models/outcome.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { IRacingFormEvent } from '@core/models/racing-form-event.model';
import { RaceOutcomeDetailsService } from '@core/services/raceOutcomeDetails/race-outcome-details.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { Router } from '@angular/router';
import { NextRacesHomeService } from '@lazy-modules-module/lazyNextRacesTab/components/nextRacesHome/next-races-home.service';
import { EventService } from '@root/app/sb/services/event/event.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { VirtualSharedService } from '@shared/services/virtual/virtual-shared.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'race-card-home',
  templateUrl: './race-card-home.component.html',
  styleUrls: ['./race-card-home.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class RaceCardHomeComponent implements OnInit, OnDestroy {

  @Input() raceIndex: number;
  @Input() raceOrigin: string;
  @Input() raceMaxSelections: number;
  @Input() showTimer: boolean;
  @Input() raceNewStyleCard: boolean;
  @Input() gtmModuleTitle?: string;
  @Input() isFeaturedRaceCard?: boolean;
  @Input() hideNonRunners?: boolean;
  @Input() hideFavourite?: boolean;
  @Input() set raceData(value: ISportEvent[]) {
    this._raceData = value;
    this.processOutcomes();
    this.raceDataSub.next(this._raceData);
  }

  // for featured module. amount of selections above "see more/show less" button.
  @Input() selectionsLimit?: number;

  disableScroll: boolean;
  showCarouselButtons: boolean;
  eventCategory: string;
  seeAllRaceText: string;

  isShowAllActive: boolean = false;
  allShown: boolean = false;
  limit: number;
  raceData$: Observable<ISportEvent[]>;
  isVirtual: boolean = false;

  private raceDataSub: BehaviorSubject<ISportEvent[]> = new BehaviorSubject(null);
  private _raceData: ISportEvent[];
  private raceMarkets: string[] = [];
  private outcomesLength = 0;

  constructor(
    public raceOutcomeDetails: RaceOutcomeDetailsService,
    public routingHelperService: RoutingHelperService,
    public nextRacesHomeService: NextRacesHomeService,
    public locale: LocaleService,
    public sbFiltersService: SbFiltersService,
    public filtersService: FiltersService,
    public pubSubService: PubSubService,
    public router: Router,
    public eventService: EventService,
    public virtualSharedService: VirtualSharedService,
    public datePipe: DatePipe
  ) {
    this.raceData$ = this.raceDataSub.asObservable();
  }

  toggleShow(): void {
    this.allShown = !this.allShown;

    if (this.allShown) {
      this.limit = undefined;
    } else {
      this.limit = this.selectionsLimit;
    }
  }

  ngOnInit(): void {
    if (this._raceData[0]) {
      this.outcomesLength = this._raceData[0].markets[0].outcomes.length;
      this._raceData.forEach((event: ISportEvent) => {
        if (event.racingFormEvent) {
          event.racingFormEvent.raceType = this.getRaceType(event.racingFormEvent);
        }
        this.isVirtual = this.virtualSharedService.isVirtual(this._raceData[0].categoryId);
      });
    }
    this.raceIndex = this.raceIndex || 0;
    this.raceOrigin = this.raceOrigin ? `?origin=${this.raceOrigin}` : '';

    this.seeAllRaceText = this.locale.getString('sb.seeAll');
    this.eventCategory = 'widget';

    this.processOutcomes();

    // show all button and limit only for featured module
    this.limit = this.isFeaturedRaceCard ? this.selectionsLimit : undefined;
    this.isShowAllActive = this.isFeaturedRaceCard && this.outcomesLength > 0 && this.outcomesLength > this.selectionsLimit;

    // re-sort outcomes on price change event
    this.pubSubService.subscribe('RaceCardComponent', this.pubSubService.API.OUTCOME_UPDATED, (market: IMarket) => {
      if (this.raceMarkets.includes(market.id)) {
        this.processOutcomes(market);
        this.isShowAllActive = this.isFeaturedRaceCard && market.outcomes.length > this.selectionsLimit;
      }
    });
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe('RaceCardComponent');
  }

  trackByEvents(i: number, event: ISportEvent): string {
    return `${i}_${event.id}_${event.name}_${event.categoryId}`;
  }

  trackByMarkets(i: number, market: IMarket): string {
    return `${i}_${market.id}_${market.name}_${market.marketStatusCode}`;
  }

  trackByOutcomes(i: number, outcome: IOutcome): string {
    return `${i}_${outcome.id}_${outcome.name}_${outcome.runnerNumber}`;
  }

  removeLineSymbol(name: string): string {
    return this.filtersService.removeLineSymbol(name);
  }

  getEventName(event: ISportEvent): string {
    if (this.isVirtual) {
      const correctTime = this.datePipe.transform(event.startTime, 'HH:mm');
      return event.name.replace(/\d\d\:\d\d/, correctTime);
    }
    const name = event.localTime ? `${event.localTime} ${event.typeName}` : event.name;
    return event.nameOverride || name;
  }

  isCashoutAvailable(event: ISportEvent): boolean {
    return event.cashoutAvail === 'Y' || event.viewType === 'handicaps';
  }

  formEdpUrl(eventEntity: ISportEvent): string {
    if (this.isVirtual) {
      return this.virtualSharedService.formVirtualEventUrl(eventEntity);
    } else {
      return `${this.routingHelperService.formEdpUrl(eventEntity)}${this.raceOrigin}`;
    }
  }

  trackEvent(entity: ISportEvent): void {
    this.nextRacesHomeService.trackNextRace(entity);

    const link: string = this.formEdpUrl(entity);
    this.router.navigateByUrl(link);
  }

  isItvEvent(event: ISportEvent): boolean {
    return this.nextRacesHomeService.isItvEvent(event);
  }

  getGoing(going: string): string {
    return this.nextRacesHomeService.getGoing(going);
  }

  getDistance(distance: string): string {
    return this.nextRacesHomeService.getDistance(distance);
  }

  showEchWayTerms(market: IMarket): boolean {
    return !!(market.eachWayPlaces && market.eachWayFactorDen && market.eachWayFactorNum);
  }

  isGenericSilk(eventEntity: ISportEvent, outcomeEntity: IOutcome): boolean {
    return this.raceOutcomeDetails.isGenericSilk(eventEntity, outcomeEntity);
  }

  isGreyhoundSilk(eventEntity: ISportEvent, outcomeEntity: IOutcome): boolean {
    return this.raceOutcomeDetails.isGreyhoundSilk(eventEntity, outcomeEntity);
  }

  isNumberNeeded(eventEntity: ISportEvent, outcomeEntity: IOutcome): boolean {
    return this.raceOutcomeDetails.isNumberNeeded(eventEntity, outcomeEntity);
  }

  getRunnerNumber(outcomeEntity: IOutcome): string {
    return outcomeEntity.runnerNumber || outcomeEntity.racingFormOutcome && outcomeEntity.racingFormOutcome.draw;
  }

  getSilkStyle(raceData: ISportEvent[], outcomeEntity: IOutcome): ISilkStyleModel {
    return this.raceOutcomeDetails.getSilkStyle(raceData, outcomeEntity);
  }

  isStreamLabelShown(event: ISportEvent): boolean {
    const liveStreamAvailable: boolean = this.eventService.isLiveStreamAvailable(event).liveStreamAvailable;
    return liveStreamAvailable;
  }

  /**
   * Get race type
   * @returns {*|string}
   */
  private getRaceType(racingFormEvent: IRacingFormEvent): string {
    const KEY_NOT_FOUND = 'KEY_NOT_FOUND';
    let stage = this.locale.getString(`racing.raceType.${racingFormEvent.raceType}`);
    if (stage === KEY_NOT_FOUND) {
      stage = '';
    } else if (stage !== KEY_NOT_FOUND && (racingFormEvent.going || racingFormEvent.distance || this.showTimer)) {
      stage = `${stage} - `;
    }
    return stage;
  }

  /**
   * Sort and filter outcomes of market(s)
   * providing updatedMarket parameter will process only appropriate market, otherwise - all existing
   *
   * @param updatedMarket
   */
  private processOutcomes(updatedMarket?: IMarket): void {
    this._raceData.forEach((event: ISportEvent) => {
      return event.markets.some((market: IMarket) => {
        if (updatedMarket) {
          if (updatedMarket.id === market.id) {
            market.outcomes = this.applyFilters(market);

            return true;
          }
        } else {
          this.raceMarkets.push(market.id);
          market.outcomes = this.applyFilters(market);
        }
      });
    });
  }

  private applyFilters(market: IMarket): IOutcome[] {
    const orderedOutcomes = this.sbFiltersService.orderOutcomeEntities(
      market.outcomes,
      market.isLpAvailable,
      true,
      true,
      this.hideNonRunners,
      this.hideFavourite,
      this._raceData[0].categoryCode === 'GREYHOUNDS'
    );
    return this.raceMaxSelections ? orderedOutcomes.splice(0, this.raceMaxSelections) : orderedOutcomes;
  }
}
