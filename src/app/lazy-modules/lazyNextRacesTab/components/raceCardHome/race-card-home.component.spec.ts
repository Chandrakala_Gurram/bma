import { RaceCardHomeComponent } from './race-card-home.component';
import { fakeAsync } from '@angular/core/testing';

describe('#RaceCardHomeComponent', () => {
  let component: RaceCardHomeComponent;
  let raceOutcomeDetails;
  let routingHelperService;
  let nextRacesHomeService;
  let localeService;
  let sbFiltersService;
  let filtersService;
  let pubSubService;
  let router;
  let eventService;
  let virtualSharedService;
  let datePipe;


  const mockString = 'seeAll';
  let raceDataMock;

  beforeEach(fakeAsync(() => {
    raceDataMock = [
      {
        markets: [
          {
            id: '12345',
            outcomes: []
          }
        ]
      }
    ];
    localeService = {
      getString: jasmine.createSpy('getString').and.returnValue(mockString)
    };
    pubSubService = {
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: {
        OUTCOME_UPDATED: 'OUTCOME_UPDATED'
      }
    };
    filtersService = {
      removeLineSymbol: jasmine.createSpy('removeLineSymbol')
    };
    nextRacesHomeService = {
      trackNextRace: jasmine.createSpy('trackNextRace'),
      isItvEvent: jasmine.createSpy('isItvEvent'),
      getGoing: jasmine.createSpy('getGoing'),
      getDistance: jasmine.createSpy('getDistance')
    };
    routingHelperService = {
      formEdpUrl: jasmine.createSpy('formEdpUrl')
    };
    raceOutcomeDetails = {
      isGenericSilk: jasmine.createSpy('isGenericSilk'),
      isGreyhoundSilk: jasmine.createSpy('isGreyhoundSilk'),
      isNumberNeeded: jasmine.createSpy('isNumberNeeded'),
      getSilkStyle: jasmine.createSpy('getSilkStyle')
    };
    sbFiltersService = {
      orderOutcomeEntities: jasmine.createSpy('orderOutcomeEntities').and.returnValue([{
        id: 'outcome1'
      },
        {
          id: 'outcome2'
        }])
    };
    router = {
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    };
    eventService = {
      isLiveStreamAvailable: jasmine.createSpy('isLiveStreamAvailable').and.returnValue({
        liveStreamAvailable: true
      })
    };
    virtualSharedService = {
      isVirtual: jasmine.createSpy('isVirtual'),
      formVirtualEventUrl: jasmine.createSpy('formVirtualEventUrl')
    };
    datePipe = {
      transform: () => '20:15'
    } as any;

    component = new RaceCardHomeComponent(raceOutcomeDetails, routingHelperService, nextRacesHomeService, localeService,
      sbFiltersService, filtersService, pubSubService, router, eventService, virtualSharedService, datePipe);
    (component['_raceData'] as any) = raceDataMock;
  }));

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  it('should call proper setter methods on raceData assign', () => {
    component['raceDataSub'].next = jasmine.createSpy();
    component['processOutcomes'] = jasmine.createSpy();
    component.raceData = raceDataMock;

    expect(component['_raceData'][0].markets[0].id).toEqual('12345');
    expect(component['processOutcomes']).toHaveBeenCalled();
    expect(component['raceDataSub'].next).toHaveBeenCalledWith(raceDataMock);
    expect(component.raceData$).toBeDefined();
  });

  describe('ngOnInit', () => {
    it('should init component', () => {
      component['processOutcomes'] = jasmine.createSpy();
      component.ngOnInit();

      expect(component.raceIndex).toEqual(0);
      expect(component.raceOrigin).toEqual('');
      expect(component.seeAllRaceText).toEqual('seeAll');
      expect(component.eventCategory).toEqual('widget');
      expect(component.limit).toEqual(undefined);
      expect(component.isShowAllActive).toBeFalsy();

      expect(localeService.getString).toHaveBeenCalledWith('sb.seeAll');
      expect(pubSubService.subscribe).toHaveBeenCalledWith('RaceCardComponent', 'OUTCOME_UPDATED', jasmine.any(Function));
    });

    it('should set limits for featured module', () => {
      component.isFeaturedRaceCard = true;
      component.selectionsLimit = 10;
      component.ngOnInit();

      expect(component.limit).toEqual(10);
    });

    it('should add race types to cards', () => {
      component['_raceData'][0].racingFormEvent = {
        raceType: 'raceType',
        distance: 'distance',
        going: 'going'
      };
      component.ngOnInit();

      expect(localeService.getString).toHaveBeenCalledWith('racing.raceType.raceType');
    });

    it('when no race data', () => {
      component['processOutcomes'] = jasmine.createSpy('processOutcomes');
      component.isFeaturedRaceCard = true;
      component['_raceData'][0] = undefined;
      component.ngOnInit();

      expect(component.isShowAllActive).toEqual(false);
    });

    it('should set isShowAllActive for featured module', () => {
      const arrayOfOutcomesMock: any = [{}, {}, {}, {}, {}];
      component['_raceData'][0].markets[0].outcomes = component['_raceData'][0].markets[0].outcomes.concat(arrayOfOutcomesMock);

      component.isFeaturedRaceCard = true;
      component.selectionsLimit = 3;
      component.ngOnInit();

      expect(component.limit).toEqual(3);
      expect(component.isShowAllActive).toBeTruthy();
    });

    it('should process outcomes on OUTCOME_UPDATED', () => {
      spyOn(component as any, 'processOutcomes').and.callThrough();
      component.raceOrigin = 'uk';
      component['raceMarkets'] = ['1'];
      component.isFeaturedRaceCard = true;
      component.selectionsLimit = 1;
      const market = { id: '1', outcomes: [{}, {}] };
      pubSubService.subscribe.and.callFake((name, channel, cb) => cb(market));

      component.ngOnInit();

      expect(component['processOutcomes']).toHaveBeenCalledWith(market);
    });

    it('should not process outcomes on OUTCOME_UPDATED', () => {
      spyOn(component as any, 'processOutcomes').and.callThrough();
      component['raceMarkets'] = [];
      pubSubService.subscribe.and.callFake((name, channel, cb) => cb({ id: '1' }));

      component.ngOnInit();

      expect(component['processOutcomes']).toHaveBeenCalled();
    });
  });

  it('#toggleShow test component state', () => {
    component.allShown = false;
    component.isFeaturedRaceCard = true;
    component.selectionsLimit = 10;

    component.toggleShow();
    expect(component.allShown).toEqual(true);
    expect(component.limit).toEqual(undefined);

    component.toggleShow();
    expect(component.allShown).toEqual(false);
    expect(component.limit).toEqual(component.selectionsLimit);
  });

  it('#ngOnDestroy', () => {
    component.ngOnDestroy();

    expect(pubSubService.unsubscribe).toHaveBeenCalledWith('RaceCardComponent');
  });

  it('#trackByEvents', () => {
    const result = component.trackByEvents(1, ({
      id: '12345',
      name: 'name',
      categoryId: '12'
    } as any));

    expect(result).toEqual('1_12345_name_12');
  });

  it('#trackByMarkets', () => {
    const result = component.trackByMarkets(1, ({
      id: '12345',
      name: 'name',
      marketStatusCode: 'A'
    } as any));

    expect(result).toEqual('1_12345_name_A');
  });

  it('#trackByOutcomes', () => {
    const result = component.trackByOutcomes(1, ({
      id: '12345',
      name: 'name',
      runnerNumber: '2'
    } as any));

    expect(result).toEqual('1_12345_name_2');
  });

  it('#removeLineSymbol', () => {
    component.removeLineSymbol('removeLineSymbol');

    expect(filtersService.removeLineSymbol).toHaveBeenCalledWith('removeLineSymbol');
  });

  describe('#getEventName', () => {
    it('should return name if event has nameOverride', () => {
      component.isVirtual = false;
      const result = component.getEventName(({
        typeName: 'London',
        name: 'London2',
        nameOverride: 'London1'
      }) as any);

      expect(result).toEqual('London1');
    });

    it('should return name if event without nameOverride', () => {
      component.isVirtual = false;
      const result = component.getEventName(({
        localTime: '2',
        typeName: 'London',
        name: 'London2'
      }) as any);

      expect(result).toEqual('2 London');
    });

    it('should return correct virtual name', () => {
      component.isVirtual = true;
      const event = { id: 1, categoryId: 39, name: '18:15 Laddies Leap\'s Lane' } as any;
      expect(component.getEventName(event)).toBe('20:15 Laddies Leap\'s Lane');
    });
  });

  describe('formEdpUrl', () => {
    it('should create EDP url', () => {
      component.formEdpUrl(({id: '1'} as any));

      expect(routingHelperService.formEdpUrl).toHaveBeenCalledWith({ id: '1' });
    });

    it('should create Virtual url', () => {
      component.isVirtual = true;
      component.formEdpUrl(({id: '2'} as any));

      expect(virtualSharedService.formVirtualEventUrl).toHaveBeenCalledWith({ id: '2' });
    });
  });

  it('#getRunnerNumber return runner number', () => {
    expect(component.getRunnerNumber({} as any)).toBe(undefined);
    expect(component.getRunnerNumber({runnerNumber: '3'} as any)).toBe('3');
    expect(component.getRunnerNumber({racingFormOutcome: {}} as any)).toBe(undefined);
    expect(component.getRunnerNumber({racingFormOutcome: { draw: '6' }} as any)).toBe('6');
  });

  describe('#isCashoutAvailable', () => {
    it('should return isCashoutAvailable', () => {
      const result = component.isCashoutAvailable(({
        cashoutAvail: 'Y',
        viewType: '',
      }) as any);

      expect(result).toEqual(true);
    });

    it('should return isCashoutAvailable when view type handicaps', () => {
      const result = component.isCashoutAvailable(({
        cashoutAvail: 'N',
        viewType: 'handicaps',
      }) as any);

      expect(result).toEqual(true);
    });

    it('should return isCashoutAvailable when no cashout', () => {
      const result = component.isCashoutAvailable(({
        cashoutAvail: 'N',
        viewType: '',
      }) as any);

      expect(result).toEqual(false);
    });
  });

  it('#formEdpUrl', () => {
    component.formEdpUrl(({id: '12312312'} as any));

    expect(routingHelperService.formEdpUrl).toHaveBeenCalledWith({ id: '12312312' });
  });

  it('#trackEvent', () => {
    spyOn(component, 'formEdpUrl').and.returnValue('/event/1212');
    component.trackEvent(({ id: '12312312' } as any));

    expect(nextRacesHomeService.trackNextRace).toHaveBeenCalled();
    expect(router.navigateByUrl).toHaveBeenCalledWith('/event/1212');
  });

  it('#isItvEvent', () => {
    component.isItvEvent(({id: '12312312'} as any));

    expect(nextRacesHomeService.isItvEvent).toHaveBeenCalledWith({ id: '12312312' });
  });

  it('#getGoing', () => {
    component.getGoing('G');

    expect(nextRacesHomeService.getGoing).toHaveBeenCalledWith('G');
  });

  it('#getDistance', () => {
    component.getDistance('Distance');

    expect(nextRacesHomeService.getDistance).toHaveBeenCalledWith('Distance');
  });

  describe('#showEchWayTerms', () => {
    it('#showEchWayTerms if market has eachWayPlaces', () => {
      const result = component.showEchWayTerms(({
        eachWayPlaces: true,
        eachWayFactorDen: '1',
        eachWayFactorNum: '7'
      } as any));

      expect(result).toEqual(true);
    });

    it('#showEchWayTerms does not have eachWayPlaces', () => {
      const result = component.showEchWayTerms(({
        eachWayPlaces: false,
        eachWayFactorDen: '',
        eachWayFactorNum: ''
      } as any));

      expect(result).toEqual(false);
    });
  });

  it('#isGenericSilk', () => {
    component.isGenericSilk(({name: 'event'} as any), ({anme: 'outcome'} as any));

    expect(raceOutcomeDetails.isGenericSilk).toHaveBeenCalledWith({name: 'event'}, {anme: 'outcome'});
  });

  it('#isGreyhoundSilk', () => {
    component.isGreyhoundSilk(({name: 'event'} as any), ({anme: 'outcome'} as any));

    expect(raceOutcomeDetails.isGreyhoundSilk).toHaveBeenCalledWith({name: 'event'}, {anme: 'outcome'});
  });

  it('#isNumberNeeded', () => {
    component.isNumberNeeded(({name: 'event'} as any), ({anme: 'outcome'} as any));

    expect(raceOutcomeDetails.isNumberNeeded).toHaveBeenCalledWith({name: 'event'}, {anme: 'outcome'});
  });

  it('#getSilkStyle', () => {
    component.getSilkStyle(([{name: 'event'}] as any), ({anme: 'outcome'} as any));

    expect(raceOutcomeDetails.getSilkStyle).toHaveBeenCalledWith([{name: 'event'}], {anme: 'outcome'});
  });

  it('#isStreamLabelShown', () => {
    const result = component.isStreamLabelShown(({ name: 'event' } as any));

    expect(eventService.isLiveStreamAvailable).toHaveBeenCalledWith({ name: 'event' });
    expect(result).toEqual(true);
  });

  describe('#processOutcomes', () => {
    beforeEach(() => {
      component['applyFilters'] = jasmine.createSpy('applyFilters').and.returnValue([]);
      (component['_raceData'] as any) = [
        {
          markets: [
            { id: '98765', outcomes: [] },
            { id: '12345', outcomes: [] }
          ]
        }
      ];
    });
    it('should Sort and filter outcomes of market(s)', () => {
      component['processOutcomes']();

      expect(component['applyFilters']).toHaveBeenCalledWith({
        id: '12345',
        outcomes: []
      });
      expect(component['raceMarkets']).toEqual(['98765', '12345']);
    });

    it('should Sort and filter outcomes of market(s) for updatedMarket', () => {
      component['processOutcomes'](({
        id: '12345',
        outcomes: []
      } as any));

      expect(component['applyFilters']).toHaveBeenCalledWith({
        id: '12345',
        outcomes: []
      });
      expect(component['raceMarkets']).toEqual([]);
    });
  });

  describe('#getRaceType', () => {
    it('should return race type with going', () => {
      localeService.getString.and.returnValue('Flat Turf');
      const result = component['getRaceType']({
        raceType: 'raceType',
        going: 'going',
        distance: 'distance'
      });

      expect(result).toEqual('Flat Turf - ');
    });

    it('should return race type without going', () => {
      localeService.getString.and.returnValue('Flat Turf');
      const result = component['getRaceType']({
        raceType: 'raceType',
        distance: 'distance'
      } as any);

      expect(result).toEqual('Flat Turf - ');
    });

    it('should return race type without going and distance', () => {
      localeService.getString.and.returnValue('Flat Turf');
      component.showTimer = true;
      const result = component['getRaceType']({
        raceType: 'raceType'
      } as any);

      expect(result).toEqual('Flat Turf - ');
    });

    it('should return race type without going and distance and without timer', () => {
      localeService.getString.and.returnValue('Flat Turf');
      component.showTimer = false;
      const result = component['getRaceType']({
        raceType: 'raceType'
      } as any);

      expect(result).toEqual('Flat Turf');
    });

    it('should return epty string when key not found', () => {
      localeService.getString.and.returnValue('KEY_NOT_FOUND');
      const result = component['getRaceType']({
        raceType: 'raceType',
        distance: 'distance'
      } as any);

      expect(result).toEqual('');
    });
  });

  describe('#applyFilters', () => {
    it('should filter outcomes of market', () => {
      component.hideNonRunners = false;
      component.hideFavourite = false;
      const result = component['applyFilters'](({
        outcomes: [],
        isLpAvailable: 'Yes',
      } as any));
      expect(sbFiltersService.orderOutcomeEntities).toHaveBeenCalledWith([], 'Yes', true, true, false, false, false);
      expect(result).toEqual(([{
        id: 'outcome1'
      },
        {
          id: 'outcome2'
        }] as any));
    });

    it('should filter outcomes of market and splice for max selections on card', () => {
      component.raceMaxSelections = 1;
      const result = component['applyFilters'](({
        outcomes: [],
        isLpAvailable: 'Yes',
      } as any));
      expect(sbFiltersService.orderOutcomeEntities).toHaveBeenCalledWith([], 'Yes', true, true, undefined, undefined, false);
      expect(result).toEqual(([{
        id: 'outcome1'
      }] as any));
    });
  });
});
