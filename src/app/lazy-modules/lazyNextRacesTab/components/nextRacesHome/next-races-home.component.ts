import { Component, Input, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import * as _ from 'underscore';
import { Subscription, from } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { NextRacesHomeService } from '@lazy-modules-module/lazyNextRacesTab/components/nextRacesHome/next-races-home.service';
import { EventService } from '@sb/services/event/event.service';
import { ICombinedRacingConfig, ISystemConfig } from '@core/services/cms/models/system-config';
import { ISportEvent } from '@core/models/sport-event.model';
import { RacingPostService } from '@core/services/racing/racingPost/racing-post.service';
import { UpdateEventService } from '@core/services/updateEvent/update-event.service';

@Component({
  selector: 'next-races-home',
  templateUrl: 'next-races-home.component.html'
})
export class NextRacesHomeComponent implements OnInit, OnDestroy {
  @Input() moduleType: string;
  @Input() moduleAllLink: boolean;
  @Input() maxSelections?: number;
  @Output() readonly dataLoaded: EventEmitter<boolean> = new EventEmitter<boolean>();
  showLoader: boolean =  true;

  /** @member {boolean} */
  isExpanded: boolean = true;

  /** @member {object} config return from nextRacesfactory */
  nextRacesModule;

  /** @member {number} number to show amount of selection in one row */
  numberOfSelections: number = 3; // default value

  /** @member {String} Module title */
  moduleTitle: string = '';

  /** that represent connect callback for page reload on SS Error */
  raceModule: string;

  /** @member {boolean} */
  ssDown: boolean = false;

  /** @member {boolean} */
  className: string;

  /** @member {boolean} */
  showTimer: boolean;

  // Show More Link
  raceEvent: string;

  leftTitleText: string;

  headerClass: string;

  private subscribedChannelsId: string = '';
  private eventsSubscription: Subscription;

  constructor(
    public pubSubService: PubSubService,
    public cmsService: CmsService,
    public nextRacesHomeService: NextRacesHomeService,
    public eventService: EventService,
    public racingPostService: RacingPostService,
    // tslint:disable-next-line
    protected updateEventService: UpdateEventService // for events subscription (done in service init)
  ) {
  }

  // Module Name
  get MODULE_NAME(): string {
    return `MODULE_${this.raceModule}`;
  }

  /**
   * Init function for(callbacks, watchers, scope destroying)
   * @private
   */
  ngOnInit(): void {
    this.raceModule = 'NEXT_RACE';
    this.cmsService.triggerSystemConfigUpdate();
    this.registerEvents();
    this.getCmsConfigs();
    this.className = `next-races-${this.moduleType}`;
    this.showTimer = this.moduleType === 'horseracing';
    this.raceEvent = this.moduleType === 'horseracing' ? 'Horse Racing' : 'Greyhounds';
  }

  ngOnDestroy(): void {
    // unSubscribe LS Updates via PUSH
    this.nextRacesHomeService.unSubscribeForUpdates(this.subscribedChannelsId);
    this.pubSubService.unsubscribe(this.MODULE_NAME);
    this.eventsSubscription && this.eventsSubscription.unsubscribe();
  }

  /**
   * Get data from Cms config
   */
  getCmsConfigs(): void {
    let storedConfig: ICombinedRacingConfig;

    this.pubSubService.subscribe(this.MODULE_NAME, this.pubSubService.API.SYSTEM_CONFIG_UPDATED, (data: ISystemConfig) => {
      const updatedConfig: ICombinedRacingConfig = {
        RacingDataHub: data.RacingDataHub,
        NextRaces: data.NextRaces,
        GreyhoundNextRaces: data.GreyhoundNextRaces
      };

      if (updatedConfig.NextRaces && !_.isEqual(updatedConfig, storedConfig)) {
        storedConfig = updatedConfig;
        this.moduleTitle = updatedConfig.NextRaces.title;
        // All nextRacesModule configuration
        this.nextRacesModule = this.nextRacesHomeService.getNextRacesModuleConfig(this.moduleType, updatedConfig);
        this.leftTitleText = this.moduleTitle;
        this.getNextEvents();
      }
    });
  }

  /**
   * tracking for vial link on home page
   */
  sendToGTM(): void {
    this.nextRacesHomeService.sendGTM('view all', 'home');
  }

  /**
   * Get Next Races Events
   */
  getNextEvents(): void {
    this.showLoader = true;
    // unSubscribe LS Updates via PUSH
    this.nextRacesHomeService.unSubscribeForUpdates(this.subscribedChannelsId);

    this.eventsSubscription = from(this.eventService.getNextEvents(this.nextRacesModule, 'nextRacesHome'))
      .pipe(
        mergeMap((eventsData: ISportEvent[]) => {
          return this.racingPostService.updateRacingEventsList(eventsData, this.moduleType === 'horseracing');
        })
      ).subscribe((eventsData: ISportEvent[]) => {
        Object.assign(this.nextRacesModule, { storedEvents: this.nextRacesHomeService.getUpdatedEvents(eventsData, this.moduleType) });
        // subscribe LS Updates via PUSH
        this.subscribedChannelsId = this.nextRacesHomeService.subscribeForUpdates(this.nextRacesModule.storedEvents);
        this.dataLoaded.emit(true);
        this.showLoader = false;
      }, () => {
        this.dataLoaded.emit(true);
        this.showLoader = false;
        console.warn('Error while getting next events');
      });
  }

  /**
   * Sync events
   */
  registerEvents(): void {
    this.pubSubService.subscribe(this.MODULE_NAME, this.pubSubService.API.RELOAD_COMPONENTS, () => this.getCmsConfigs());
  }
}
