import { DateAgoPipe } from '@lazy-modules/timeline/pipes/dateAgo/date-ago.pipe';

describe('ReplacePipe', () => {
  let pipe;

  beforeEach(() => {
    pipe = new DateAgoPipe();
    jasmine.clock().mockDate(new Date('2020-01-01T12:00:00.976Z'));
  });

  it('should return value if it\'s falsy', () => {
    const result = pipe.transform('');

    expect(result).toBe('');
  });

  it('should return \'just now\'', () => {
    const result = pipe.transform('2020-01-01T12:00:29.976Z');

    expect(result).toBe('Just now');
  });

  it('should return \'1 min\'', () => {
    const result = pipe.transform('2020-01-01T11:59:00.976Z');

    expect(result).toBe('1 min');
  });

  it('should return \'2 mins\'', () => {
    const result = pipe.transform('2020-01-01T11:58:00.976Z');

    expect(result).toBe('2 mins');
  });

  it('should return \'1 hour\'', () => {
    const result = pipe.transform('2020-01-01T11:00:00.976Z');

    expect(result).toBe('1 hour');
  });

  it('should return \'2 hours\'', () => {
    const result = pipe.transform('2020-01-01T10:00:00.976Z');

    expect(result).toBe('2 hours');
  });

  it('should return \'1 day\'', () => {
    const result = pipe.transform('2019-12-31T11:00:00.976Z');

    expect(result).toBe('1 day');
  });

  it('should return \'2 days\'', () => {
    const result = pipe.transform('2019-12-30T11:00:00.976Z');

    expect(result).toBe('2 days');
  });

  it('should return \'1 week\'', () => {
    const result = pipe.transform('2019-12-25T11:00:00.976Z');

    expect(result).toBe('1 week');
  });

  it('should return \'2 weeks\'', () => {
    const result = pipe.transform('2019-12-18T11:00:00.976Z');

    expect(result).toBe('2 weeks');
  });

  it('should return \'1 month\'', () => {
    const result = pipe.transform('2019-12-01T11:00:00.976Z');

    expect(result).toBe('1 month');
  });

  it('should return \'2 months\'', () => {
    const result = pipe.transform('2019-11-01T11:00:00.976Z');

    expect(result).toBe('2 months');
  });

  it('should return \'1 year\'', () => {
    const result = pipe.transform('2018-12-01T11:00:00.976Z');

    expect(result).toBe('1 year');
  });

  it('should return \'2 years\'', () => {
    const result = pipe.transform('2017-11-01T11:00:00.976Z');

    expect(result).toBe('2 years');
  });

});
