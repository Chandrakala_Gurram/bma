import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateAgo',
  pure: true
})
export class DateAgoPipe implements PipeTransform {
  private intervals = {
    'year': 31536000,
    'month': 2592000,
    'week': 604800,
    'day': 86400,
    'hour': 3600,
    'min': 60,
    's': 1
  };
  transform(value: string): string {
    if (value) {
      const seconds = Math.floor((Date.now() - new Date(value).getTime()) / 1000);
      if (seconds < 30) {
        return 'Just now';
      }

      let counter;
      // tslint:disable-next-line: forin
      for (const i in this.intervals) {
        counter = Math.floor(seconds / this.intervals[i]);
        if (counter > 0) {
          return `${counter} ${i}${counter === 1 ? '' : 's'}`;
        }
      }
    }
    return value;
  }
}
