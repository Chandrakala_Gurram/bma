import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { filter, mergeMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { WsConnector } from '@core/services/wsConnector/ws-connector';
import { TimelineService } from '@lazy-modules/timeline/services/timeline.service';
import { CmsService } from '@core/services/cms/cms.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { AsyncScriptLoaderService } from '@root/app/core/services/asyncScriptLoader/async-script-loader.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { RendererService } from '@shared/services/renderer/renderer.service';
import { UserService } from '@core/services/user/user.service';

import { ITimelineSettings } from '@core/services/cms/models/timeline-settings.model';
import { IPost } from '@lazy-modules/timeline/models/timeline-post.model';
import { SPRITE_PATH } from '@root/app/bma/constants/image-manager.constant';
import { TIMELINE_EVENTS } from '@lazy-modules/timeline/constants/timeline.constant';
import { IOutcomePrice } from '@core/models/outcome-price.model';

@Component({
  selector: 'timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimelineComponent implements OnInit, OnDestroy {
  public showSkeleton: boolean = false;
  public isReconectedFailedMsg: boolean = false;
  public posts: IPost[] = [];
  public lastPost: IPost;
  public socket: WsConnector;
  public isTimelineAvailable: boolean = false;
  public isNewPostIconDisplayed: boolean = false;
  public timelineOpened: boolean = false;
  public timelineSettings: ITimelineSettings;
  public timelineIcons: string;
  public bybShown: boolean = false;
  public allPostsLoaded: boolean = false;
  public tutorialReady: boolean = false;
  public priceButtonClasses: { [key: string]: string } = {};
  private timelineSettingSub: Subscription;
  private timelineServiceSub: Subscription;
  private createSocketSub: Subscription;
  private routeChangeSub: Subscription;
  private BODY_CLASS: string = 'timeline-opened';
  private readonly title: string = 'timeline';
  private readonly removePriceUpdateClassTime = 2000;
  private socketStates = ['reconnect_attempt', 'reconnect', 'connect_error'];

  constructor(
    protected timelineService: TimelineService,
    protected cms: CmsService,
    protected router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private asyncScriptLoaderService: AsyncScriptLoaderService,
    private pubSubService: PubSubService,
    private windowRefService: WindowRefService,
    private rendererService: RendererService,
    private userService: UserService
  ) {
    this.handleTimeline = this.handleTimeline.bind(this);
    this.loadTimelineIcons();
  }

  ngOnInit(): void {
    this.timelineSettingSub = this.cms.getTimelineSetting()
      .subscribe((settings: ITimelineSettings) => {
        this.timelineSettings = settings;
      });

    this.pubSubService.subscribe(this.title, this.pubSubService.API.LOGIN_POPUPS_END, () => {
      this.tutorialReady = true;
      this.changeDetectorRef.detectChanges();
    });

    if (this.userService.status) {
      this.tutorialReady = true;
    }

    this.pubSubService.subscribe(this.title, [
        this.pubSubService.API.TIMELINE_SETTINGS_CHANGE,
        this.pubSubService.API.SESSION_LOGOUT,
        this.pubSubService.API.SUCCESSFUL_LOGIN
      ], this.handleTimeline
    );

    this.pubSubService.subscribe(this.title, this.pubSubService.API.BYB_SHOWN, (bybShown: boolean) => {
      this.bybShown = bybShown;
      this.changeDetectorRef.markForCheck();
    });

    this.subscribeToRouteEvents();
    this.handleTimeline();
    this.changeDetectorRef.markForCheck();
  }

  ngOnDestroy(): void {
    this.closeTimeline();
    this.routeChangeSub && this.routeChangeSub.unsubscribe();
    this.timelineSettingSub && this.timelineSettingSub.unsubscribe();
    this.timelineServiceSub && this.timelineServiceSub.unsubscribe();
    this.createSocketSub && this.createSocketSub.unsubscribe();
    this.pubSubService.unsubscribe(this.title);
  }

  handleTimeline(): void {
    if (this.timelineSettings && this.timelineSettings.enabled) {
      const endDate = new Date(this.timelineSettings.liveCampaignDisplayTo).getTime();
      const availableRoutes = this.timelineSettings.pageUrls && this.timelineSettings.pageUrls.split(',') || [''];
      this.isTimelineAvailable = this.isUrlAvailable(availableRoutes, this.router.url) &&
        this.userService.status && this.userService.timeline;
      this.timelineOpened = false;

      this.pubSubService.publish(this.pubSubService.API.TIMELINE_SHOWN, this.isTimelineAvailable);

      if (this.isTimelineAvailable && (endDate > Date.now())) {
        if (!this.timelineService.socket || !this.timelineService.socket.isConnected()) {
          this.tutorialReady && this.pubSubService.publish(this.pubSubService.API.SHOW_TIMELINE_TUTORIAL);
          this.openTimeline();
        }
      } else {
        this.closeTimeline();
      }

      this.changeDetectorRef.markForCheck();
    }
  }

  /**
   * Change state of timeline
   *
   * @param state - state
   */
  onStateChange(state: boolean): void {
    this.timelineOpened = state;
    this.timelineService.gtm(`${state ? 'open': 'close'}`, {
      eventLabel: this.timelineSettings.liveCampaignName
    });
    this.toggleBodyScroll(state);

    if (state) {
      this.isNewPostIconDisplayed = false;
    }

    this.changeDetectorRef.detectChanges();
  }

  onTimelineReload(state: boolean): void {
    if (state) {
      this.closeTimeline();
      this.openTimeline();
      this.changeDetectorRef.detectChanges();
    }
  }

  openTimeline(): void {
    this.subscribeToTimelineUpdates();
    this.pubSubService.subscribe(this.title, this.pubSubService.API.RELOAD_COMPONENTS, () => {
      this.subscribeToTimelineUpdates(true);
    });
  }

  closeTimeline(): void {
    if (this.timelineService) {
      this.timelineService.removeListener(TIMELINE_EVENTS.CAMPAIGN_CLOSED);
      this.timelineService.removeListener(TIMELINE_EVENTS.POST_CHANGED);
      this.timelineService.removeListener(TIMELINE_EVENTS.POST_REMOVED);
      this.timelineService.removeListener(TIMELINE_EVENTS.POST_PAGE);
      this.timelineService.removeListener(TIMELINE_EVENTS.POST);

      this.timelineServiceSub && this.timelineServiceSub.unsubscribe();
      this.createSocketSub && this.createSocketSub.unsubscribe();
      this.timelineService.disconnect();
    }

    this.posts = [];
    this.changeDetectorRef.detectChanges();
  }

  loadMore(): void {
    this.showSkeleton = true;
    this.timelineService.emit(TIMELINE_EVENTS.LOAD_POST_PAGE, {
      from: {
        id: this.lastPost.id,
        timestamp: this.lastPost.createdDate
      }
    });
    this.changeDetectorRef.detectChanges();
  }

  trackSocketState(state: string): void {
    if (state && this.socketStates.includes(state)) {
      this.showSkeleton = true;
      this.isReconectedFailedMsg = false;
    } else if (state === 'reconnect_failed') {
      this.showSkeleton = false;
      this.isReconectedFailedMsg = true;
    } else {
      this.showSkeleton = false;
      this.isReconectedFailedMsg = false;
    }
    this.changeDetectorRef.detectChanges();
  }

  private subscribeToTimelineUpdates(resetTimelinePosts: boolean = false): void {
    let resetPosts = resetTimelinePosts;
    this.createSocketSub = this.timelineService.createSocket()
      .pipe(mergeMap(webSocket => webSocket.state$))
      .subscribe((state) => {
        this.showSkeleton = true;
        this.trackSocketState(state);
        this.changeDetectorRef.detectChanges();
      });

    this.timelineServiceSub = this.timelineService.connect().subscribe(() => {
      this.timelineService.addListener(TIMELINE_EVENTS.POST_PAGE, ((postsPage: any) => {

        const posts = postsPage && postsPage.page;
        const count = postsPage && postsPage.count;

        if (posts && posts.length) {
          this.lastPost = posts[posts.length - 1];
          this.posts = resetPosts ? [...posts] : [...this.posts, ...posts];
          this.allPostsLoaded = count === this.posts.length;
          resetPosts = false;
        }
        this.showSkeleton = false;
        this.changeDetectorRef.detectChanges();
      }));

      this.timelineService.addListener(TIMELINE_EVENTS.POST, (post: IPost) => {
        this.posts = [post, ...this.posts];

        if (!this.timelineOpened) {
          this.isNewPostIconDisplayed = true;
        }

        this.changeDetectorRef.detectChanges();
      });

      this.timelineService.addListener(TIMELINE_EVENTS.POST_CHANGED, (newPosts) => {
        const post = newPosts.data || newPosts[0];
        const postToUpdate = this.posts.find((p: IPost) => post.id === p.id);
        this.handlePriceChange(post, postToUpdate);
        this.posts = this.posts.reduce((posts: IPost[], currentPost: IPost) => {
          return currentPost.id === post.id ? [...posts, post] : [...posts, currentPost];
        }, []);

        this.changeDetectorRef.detectChanges();
      });

      this.timelineService.addListener(TIMELINE_EVENTS.POST_REMOVED, (action) => {
        this.posts = this.posts.filter((post: IPost) => post.id !== action.affectedMessageId);
        this.changeDetectorRef.detectChanges();
      });

      this.timelineService.addListener(TIMELINE_EVENTS.CAMPAIGN_CLOSED, () => {
        this.posts = [];

        this.changeDetectorRef.detectChanges();
      });
    });
  }

  private handlePriceChange(post: IPost, updatedPost: IPost): void {
    if (post.selectionEvent) {
      const prices = post.selectionEvent.obEvent.markets[0].outcomes[0].prices[0];
      const updatedPrices = updatedPost.selectionEvent.obEvent.markets[0].outcomes[0].prices[0];
      const buttonClass = this.getClassForPriceUpdate(prices, updatedPrices);
      this.updateButtonClass(post.id, buttonClass);
    }
  }

  private getClassForPriceUpdate(currentPrices: IOutcomePrice, updatedPrices: IOutcomePrice): string {
    if (!currentPrices || !updatedPrices) {
      return '';
    }
    let buttonClass = '';
    if ((currentPrices.priceNum / currentPrices.priceDen) > (updatedPrices.priceNum / updatedPrices.priceDen)) {
      buttonClass = 'bet-up';
    } else if ((currentPrices.priceNum / currentPrices.priceDen) < (updatedPrices.priceNum / updatedPrices.priceDen)) {
      buttonClass = 'bet-down';
    }
    return buttonClass;
  }

  private updateButtonClass(postId: string, buttonClass: string): void {
    if (buttonClass) {
      this.priceButtonClasses = {...this.priceButtonClasses, [postId]: buttonClass };
      setTimeout(() => {
        this.priceButtonClasses = {...this.priceButtonClasses, [postId]: '' };
        this.changeDetectorRef.markForCheck();
      }, this.removePriceUpdateClassTime);
    }
  }

  /**
   * Check if timeline is available for current url
   * @param availableRoutes {Array} - urls where popup is enabled
   * @param currentUrl {string} - current url
   *
   * Examples of page url rules:
   *  /, /horse-racing/, /live-stream - only for root page, /horse-racing/ page and /live-stream page
   *  /horse-racing/* - category horse-racing and all its child (for example /horse-racing/live-stream)
   */
  private isUrlAvailable(availableRoutes: string[], currentUrl: string): boolean {
    return availableRoutes.some((url: string): boolean => {
      url = url.trim();
      let isEnabled = url === currentUrl;

      if (url[url.length - 1] === '*') {
        const result = currentUrl.match(url);
        isEnabled = result && (result.index === 0);
      }

      return isEnabled;
    });
  }

  /**
   * Toggles class to body element to enable/disable scrolling depending on passed state.
   * @param {boolean} state
   * @private
   */
  private toggleBodyScroll(state: boolean): void {
    const body = this.windowRefService.document.body;
    if (state) {
      this.rendererService.renderer.addClass(body, this.BODY_CLASS);
    } else {
      this.rendererService.renderer.removeClass(body, this.BODY_CLASS);
    }
  }

  private loadTimelineIcons(): void {
    this.asyncScriptLoaderService.getSvgSprite(SPRITE_PATH.timeline).subscribe((icons: string) => this.timelineIcons = icons);
  }

  private subscribeToRouteEvents(): void {
    this.routeChangeSub = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        this.timelineService.gtm('rendered', {
          eventLabel: this.timelineSettings.liveCampaignName
        });
        this.handleTimeline();
      });

    this.changeDetectorRef.markForCheck();
  }
}
