import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef
} from '@angular/core';
import { IPost } from '@lazy-modules/timeline/models/timeline-post.model';
import { IScrollEvent } from '@lazy-modules/timeline/components/sliderPanel/slider-panel.model';
import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { fromEvent, from, merge, Observable, Subscription } from 'rxjs';
import { debounceTime, map, filter, mergeMap, delay, mapTo } from 'rxjs/operators';

@Component({
  selector: 'slider-panel',
  templateUrl: './slider-panel.component.html',
  styleUrls: ['./slider-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SliderPanelComponent extends AbstractOutletComponent implements OnInit, OnDestroy {
  @ViewChild('sliderPanel') sliderPanel: ElementRef;
  @Input() isReconectedFailedMsg: boolean = false;
  @Input() showSkeleton: boolean = false;
  @Input() displayedPosts: IPost[];
  @Input() allPostsLoaded: boolean = false;
  @Input() visible: boolean;
  @Input() priceButtonClasses: { [key: string]: string };
  @Output() readonly stateChange: EventEmitter<boolean> = new EventEmitter();
  @Output() readonly reloadTimeline: EventEmitter<boolean> = new EventEmitter();
  @Output() readonly loadMore: EventEmitter<void> = new EventEmitter();
  bounce$: Observable<boolean>;
  private subscription: Subscription;

  constructor(
    private changeDetectorRef: ChangeDetectorRef
  ) {
    super()/* istanbul ignore next */;
  }

  ngOnInit(): void {
    const scrollEnd$ = this.getScrollObservable().pipe(
      map(({ target }) => target.scrollHeight === target.scrollTop + target.clientHeight)
    );
    this.subscription = scrollEnd$.pipe(
      debounceTime(400),
      filter(scrollEnd => scrollEnd && !this.allPostsLoaded)
    ).subscribe(() => {
      this.loadMore.emit();
    });
    this.bounce$ = this.getBounceObservable(scrollEnd$);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  show($event: Event, visible: boolean): void {
    $event.stopPropagation();

    this.visible = visible;
    this.stateChange.emit(visible);
    this.changeDetectorRef.markForCheck();
  }

  callToReload($event: Event): void {
    $event.stopPropagation();
    this.reloadTimeline.emit(true);
    this.changeDetectorRef.markForCheck();
  }

  trackByPost(index: number, post: IPost): string {
    return post.id;
  }

  private getScrollObservable(): Observable<IScrollEvent> {
    const events = ['scroll', 'touchmove',];
    return from(events).pipe(mergeMap(event => fromEvent(this.sliderPanel.nativeElement, event)));
  }

  private isScrollEnd(scrollEnd: boolean): boolean {
    return scrollEnd && this.allPostsLoaded;
  }

  private getBounceObservable(scrollEnd$: Observable<boolean>): Observable<boolean> {
    const scrollEndAndPostsLoaded$ = scrollEnd$.pipe(
      filter(scrollEnd => this.isScrollEnd(scrollEnd))
    );
    const addBounceAnimation$ = scrollEndAndPostsLoaded$.pipe(
      mapTo(true)
    );
    const removeBounceAnimation$ = scrollEndAndPostsLoaded$.pipe(
      mapTo(false),
      delay(1200)
    );
    return merge(addBounceAnimation$, removeBounceAnimation$);
  }
}
