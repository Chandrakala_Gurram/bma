import { SliderPanelComponent } from '@lazy-modules/timeline/components/sliderPanel/slider-panel.component';
import { of, Observable, BehaviorSubject } from 'rxjs';

describe('SliderPanelComponent', () => {
  let sliderComponent;
  let changeDetectorRef;
  const event = {
    stopPropagation: jasmine.createSpy('stopPropagation')
  };

  changeDetectorRef = {
    markForCheck: jasmine.createSpy('markForCheck')
  };

  beforeEach(() => {
    sliderComponent = new SliderPanelComponent(changeDetectorRef);
    sliderComponent.stateChange.emit = jasmine.createSpy('stateChangeEmit');
    sliderComponent.reloadTimeline.emit = jasmine.createSpy('reloadTimelineEmit');
    sliderComponent.loadMore.emit = jasmine.createSpy('loadMoreEmit');
  });

  it('#ngOnInit scroll end', () => {
    const scrollEvent = {
      target: {
        scrollHeight: 250,
        scrollTop: 100,
        clientHeight: 150
      }
    };
    sliderComponent.allPostsLoaded = true;
    sliderComponent['getScrollObservable'] = jasmine.createSpy('getScrollEnd').and.returnValue(of(scrollEvent));

    sliderComponent.ngOnInit();

    expect(sliderComponent.loadMore.emit).not.toHaveBeenCalled();
    expect(sliderComponent.bounce$).toEqual(jasmine.any(Observable));
  });

  it('#ngOnInit not all posts are loaded', () => {
    const scrollEvent = {
      target: {
        scrollHeight: 250,
        scrollTop: 100,
        clientHeight: 80
      }
    };
    sliderComponent.allPostsLoaded = false;
    sliderComponent['getScrollObservable'] = jasmine.createSpy('getScrollEnd').and.returnValue(of(scrollEvent));

    sliderComponent.ngOnInit();

    expect(sliderComponent.loadMore.emit).not.toHaveBeenCalled();
    expect(sliderComponent.bounce$).toEqual(jasmine.any(Observable));
  });

  it('#ngOnInit scroll end and not all posts are loaded', () => {
    const scrollEvent = {
      target: {
        scrollHeight: 250,
        scrollTop: 100,
        clientHeight: 150
      }
    };
    sliderComponent.allPostsLoaded = false;
    sliderComponent['getScrollObservable'] = jasmine.createSpy('getScrollEnd').and.returnValue(of(scrollEvent));

    sliderComponent.ngOnInit();

    expect(sliderComponent.loadMore.emit).toHaveBeenCalled();
    expect(sliderComponent.bounce$).toEqual(jasmine.any(Observable));
  });

  it('#ngOnDestroy', () => {
    sliderComponent['subscription'] = {
      unsubscribe: jasmine.createSpy('unsubscribe')
    };
    sliderComponent.ngOnDestroy();
    expect(sliderComponent['subscription']['unsubscribe']).toHaveBeenCalled();
  });

  it('#getScrollObservable', () => {
    sliderComponent.sliderPanel = {
      nativeElement: {}
    };
    const scrollEnd = sliderComponent['getScrollObservable']();
    expect(scrollEnd).toEqual(jasmine.any(Observable));
  });

  it('#isScrollEnd scroll is not ended', () => {
    sliderComponent.allPostsLoaded = true;
    expect(sliderComponent['isScrollEnd'](false)).toBeFalsy();
  });

  it('#isScrollEnd not all posts are loaded', () => {
    sliderComponent.allPostsLoaded = false;
    expect(sliderComponent['isScrollEnd'](true)).toBeFalsy();
  });

  it('#isScrollEnd scroll is ended all posts are loaded', () => {
    sliderComponent.allPostsLoaded = true;
    expect(sliderComponent['isScrollEnd'](true)).toBeTruthy();
  });

  it('#getBounceObservable', () => {
    const scrollEnd$ = new BehaviorSubject<boolean>(true);
    const scrollEndObservable$ = scrollEnd$.asObservable();
    sliderComponent['isScrollEnd'] = jasmine.createSpy('isScrollEnd').and.returnValue(true);
    sliderComponent['getBounceObservable'](scrollEndObservable$).subscribe();
    expect(sliderComponent['isScrollEnd']).toHaveBeenCalledWith(true);
  });

  it('should hide panel', () => {
    sliderComponent.show(event, false);

    expect(sliderComponent.visible).toBeFalsy();
    expect(event.stopPropagation).toHaveBeenCalled();
    expect(sliderComponent.stateChange.emit).toHaveBeenCalledWith(false);
    expect(sliderComponent.changeDetectorRef.markForCheck).toHaveBeenCalled();
  });

  it('should show panel', () => {
    sliderComponent.show(event, true);

    expect(sliderComponent.visible).toBeTruthy();
    expect(event.stopPropagation).toHaveBeenCalled();
    expect(sliderComponent.stateChange.emit).toHaveBeenCalledWith(true);
    expect(sliderComponent.changeDetectorRef.markForCheck).toHaveBeenCalled();
  });

  it('should test callToReload()', () => {
    sliderComponent.callToReload(event);

    expect(event.stopPropagation).toHaveBeenCalled();
    expect(sliderComponent.reloadTimeline.emit).toHaveBeenCalledWith(true);
    expect(sliderComponent.changeDetectorRef.markForCheck).toHaveBeenCalled();
  });

  it('should trackByPost', () => {
    expect(sliderComponent.trackByPost(1, { id: '2' })).toEqual('2');
  });
});
