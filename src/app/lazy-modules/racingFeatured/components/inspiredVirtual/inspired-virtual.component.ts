import { Component, Input, OnDestroy, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { Router } from '@angular/router';

import { InspiredVirtualService } from './inspired-virtual.service';
import { ISportEvent } from '@core/models/sport-event.model';
import { TempStorageService } from '@core/services/storage/temp-storage.service';
import { VirtualSharedService } from '@shared/services/virtual/virtual-shared.service';
import { from, Subscription } from 'rxjs';

@Component({
  selector: 'inspired-virtual-module',
  templateUrl: 'inspired-virtual.component.html'
})
export class InspiredVirtualComponent implements OnInit, OnDestroy, OnChanges {
  @Input() widget?: boolean;
  @Input() virtualsTitle: string;
  @Input() eventsData: Partial<ISportEvent>[];
  @Input() sportName: string;

  eventsArray: ISportEvent[];
  isHovered = false;
  carouselName: string = 'inspired-virtual';
  isFirstTimeCollapsed: boolean = false;
  isExpanded: boolean;

  protected SECTION_FLAG: string = 'VRC';
  private loadDataSubscription: Subscription;

  constructor(
    protected inspiredVirtualService: InspiredVirtualService,
    protected router: Router,
    protected storage: TempStorageService,
    protected virtualSharedService: VirtualSharedService
  ) {
    this.carouselName = 'inspired-virtual';
    this.isFirstTimeCollapsed = false;
  }

  ngOnInit(): void {
    const expandedState = this.storage.get(this.SECTION_FLAG);
    this.isExpanded = expandedState !== undefined ? this.storage.get(this.SECTION_FLAG) : true;

    if (this.eventsData[0] && this.eventsData[0].name === 'VRC') {
      this.loadDataSubscription = from(this.inspiredVirtualService.getEvents()).subscribe(data => {
        this.eventsArray = data;
      });
    } else {
      this.eventsArray = this.inspiredVirtualService.setupEvents(this.eventsData);
    }
  }

  ngOnChanges(changes: SimpleChanges): void  {
    const eventsData = changes.eventsData && changes.eventsData.currentValue;
    if (eventsData) {
      this.eventsArray = this.inspiredVirtualService.setupEvents(this.eventsData);
    }
  }

  /**
   * ngFor trackBy function
   * @param {number} index
   * @param {ISportEvent} event
   * @return {string}
   */
  trackById(index: number, event: ISportEvent): string {
    return `${index}${event.id}`;
  }

  /**
   * Send GA on first collapse
   */
  sendCollapseGTM(): void {
    this.storage.set(this.SECTION_FLAG, !this.isExpanded);
    if (!this.isFirstTimeCollapsed) {
      this.inspiredVirtualService.sendGTMOnFirstTimeCollapse(this.sportName);
      this.isFirstTimeCollapsed = true;
    }
  }

  /**
   * Go to all HR Virtuals
   */
  viewAllVirtual(event: ISportEvent): void {
    const url = event ? this.virtualSharedService.formVirtualTypeUrl(event.classId) : 'virtual-sports';
    this.router.navigateByUrl(url);
  }

  /**
   * Go to Live Virtual sport event
   */
  goToLiveEvent(event: ISportEvent): void {
    this.inspiredVirtualService.sendGTMOnGoToLiveEvent(this.sportName);
    const url =  event ? this.virtualSharedService.formVirtualEventUrl(event) : 'virtual-sports';
    this.router.navigateByUrl(url);
  }

  /**
   * Get data format in hh:mm
   * @params {number} startTime(timestamp)
   */
  getStartTime(startTime: number): string {
    return this.inspiredVirtualService.getStartTime(startTime);
  }

  /**
   * onDestroy component
   */
  ngOnDestroy(): void {
    this.inspiredVirtualService.destroyTimers();
    if (this.loadDataSubscription) {
      this.loadDataSubscription.unsubscribe();
    }
  }
}
