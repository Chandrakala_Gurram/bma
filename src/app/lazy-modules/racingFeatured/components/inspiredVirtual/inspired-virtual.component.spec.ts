import { fakeAsync, tick } from '@angular/core/testing';
import { InspiredVirtualComponent } from './inspired-virtual.component';
import { ISportEvent } from '@core/models/sport-event.model';

describe('InspiredVirtualComponent', () => {
  let component: InspiredVirtualComponent,
    inspiredVirtualService,
    router,
    storage,
    virtualSharedService;

  beforeEach(() => {
    inspiredVirtualService = {
      config: {
        slidesLimit: 5
      },
      getEvents: jasmine.createSpy().and.returnValue(Promise.resolve([])),
      setupEvents: jasmine.createSpy().and.returnValue([]),
      sendGTMOnFirstTimeCollapse: jasmine.createSpy(),
      sendGTMOnGoToLiveEvent: jasmine.createSpy(),
      getStartTime: jasmine.createSpy().and.returnValue('testStartTime'),
      destroyTimers: jasmine.createSpy()
    };

    router = {
      navigate: jasmine.createSpy(),
      navigateByUrl: jasmine.createSpy()
    };
    storage = {
      get: jasmine.createSpy('get'),
      set: jasmine.createSpy('set'),
    };

    virtualSharedService = {
      formVirtualTypeUrl: () => {},
      formVirtualEventUrl: () => {}
    };

    component = new InspiredVirtualComponent(inspiredVirtualService, router, storage, virtualSharedService);
  });

  it('should create component instance', () => {
    component.eventsData = [{}];
    component.ngOnInit();

    expect(component).toBeTruthy();
    expect(component.carouselName).toEqual('inspired-virtual');
    expect(component.isFirstTimeCollapsed).toBeFalsy();
  });

  it('should test ngOnInit (setupEvents)', fakeAsync(() => {
    component.eventsData = [{}];
    storage.get.and.returnValue(true);
    component.ngOnInit();
    tick();
    expect(inspiredVirtualService.setupEvents).toHaveBeenCalled();
    expect(component.eventsArray).toEqual([] as any);
    expect(component.isExpanded).toBeTruthy();
  }));

  it('should test ngOnInit (getEvents)', fakeAsync(() => {
    component.eventsData = [{ name: 'VRC' }];
    storage.get.and.returnValue(true);
    component.ngOnInit();
    tick();
    expect(inspiredVirtualService.getEvents).toHaveBeenCalled();
  }));

  it('should test trackById', () => {
    expect(component.trackById(1, { id : 1 } as any)).toEqual('11');
  });

  it('should test sendCollapseGTM positive', () => {
    component.sportName = 'horseracing';
    component.sendCollapseGTM();
    expect(inspiredVirtualService.sendGTMOnFirstTimeCollapse).toHaveBeenCalled();
    expect(component.isFirstTimeCollapsed).toBeTruthy();
  });

  it('should test sendCollapseGTM negative', () => {
    component.sportName = 'horseracing';
    component.isFirstTimeCollapsed = true;
    component.sendCollapseGTM();
    expect(inspiredVirtualService.sendGTMOnFirstTimeCollapse).not.toHaveBeenCalled();
    expect(component.isFirstTimeCollapsed).toBeTruthy();
  });

  describe('viewAllVirtual', () => {
    it('should test viewAllVirtual', () => {
      const event = { classId: '123'} as ISportEvent;
      component.viewAllVirtual(event);
      expect(router.navigateByUrl).toHaveBeenCalled();
    });

    it('should test viewAllVirtual when event is undefined', () => {
      const event = undefined;
      component.viewAllVirtual(event);
      expect(router.navigateByUrl).toHaveBeenCalledWith('virtual-sports');
    });
  });

  it('should test getStartTime', () => {
    expect(component.getStartTime(111)).toEqual('testStartTime');
    expect(inspiredVirtualService.getStartTime).toHaveBeenCalledWith(111);
  });

  describe('goToLiveEvent', () => {
    it('should test goToLiveEvent', () => {
      const event = { classId: '123'} as ISportEvent;
      component.goToLiveEvent(event);
      expect(inspiredVirtualService.sendGTMOnGoToLiveEvent).toHaveBeenCalled();
      expect(router.navigateByUrl).toHaveBeenCalled();
    });

    it('should test goToLiveEvent when classId is undefined', () => {
      component.goToLiveEvent( undefined);
      expect(inspiredVirtualService.sendGTMOnGoToLiveEvent).toHaveBeenCalled();
      expect(router.navigateByUrl).toHaveBeenCalledWith('virtual-sports');
    });
  });

  describe('ngOnDestroy', () => {
    it('should test ngOnDestroy', () => {
      component['loadDataSubscription'] = null;
      component.ngOnDestroy();

      expect(inspiredVirtualService.destroyTimers).toHaveBeenCalled();
    });

    it('should unsubscribe from data load subscription', () => {
      const loadDataSubscription = jasmine.createSpyObj('loadDataSubscription', ['unsubscribe']);

      component['loadDataSubscription'] = loadDataSubscription;
      component.ngOnDestroy();

      expect(loadDataSubscription.unsubscribe).toHaveBeenCalled();
    });
  });

  describe('@ngOnChanges', () => {
    it('should sort events and parse names when eventsData were changed', () => {
      component.eventsArray = undefined;
      inspiredVirtualService.setupEvents.and.returnValue([{}] as any);
      const changes = {
        eventsData: {
          currentValue: [],
          previousValue: []
        }
      } as any;

      component.ngOnChanges(changes);
      expect(inspiredVirtualService.setupEvents).toHaveBeenCalled();
      expect(component.eventsArray).toEqual([{}] as any);
    });

    it('should not sort events and parse names when eventsData not changed', () => {
      component.eventsArray = [] as any;
      inspiredVirtualService.setupEvents.and.returnValue([{}]);
      const changes = { } as any;

      component.ngOnChanges(changes);
      expect(inspiredVirtualService.setupEvents).not.toHaveBeenCalled();
      expect(component.eventsArray).toEqual([]);
    });
  });
});
