import { Input, Output, EventEmitter, Component, OnChanges } from '@angular/core';

import { LocaleService } from '@core/services/locale/locale.service';
import { TempStorageService } from '@core/services/storage/temp-storage.service';

import { IRaceGridMeeting } from '@core/models/race-grid-meeting.model';

@Component({
  selector: 'racing-events',
  templateUrl: './racing-events.component.html'
})
export class RacingEventsComponent implements OnChanges {
  @Input() sportName: string;
  @Input() racing: IRaceGridMeeting;
  @Input() byTimeEvents;
  @Input() eventsOrder: string[];
  @Input() filter?: string;
  @Input() sectionTitle: Object;
  @Input() moduleTitle: string;
  @Input() display?: string;

  @Output() readonly gaTracking = new EventEmitter();

  titleMap: {[key: string]: string} = {};
  accordionsState: {[key: string]: boolean} = {};

  constructor(private locale: LocaleService, private storage: TempStorageService) {}

  ngOnChanges(change): void {
    if (change['moduleTitle']) {
      const moduleTitle = change['moduleTitle'].currentValue;

      this.racing && this.racing.groupedRacing.forEach((group) => {

        const isInStorage = this.storage.get(group.flag) !== undefined;

        this.accordionsState[group.flag] = isInStorage ? this.storage.get(group.flag) : true;

        if (group.flag === 'UK' || group.flag === 'INT' || group.flag === 'VR') {
          this.titleMap[group.flag] = moduleTitle;
        } else {
          this.titleMap[group.flag] = this.locale.getString(this.sectionTitle[group.flag]);
        }
      });
    }
  }

  trackModule(flag, moduleName) {
    const currentAccState = !this.accordionsState[flag];

    this.storage.set(flag, currentAccState);
    this.gaTracking.emit([this.sectionTitle[flag], moduleName]);
  }
}
