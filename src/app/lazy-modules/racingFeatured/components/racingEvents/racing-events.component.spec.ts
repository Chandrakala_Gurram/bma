
import { RacingEventsComponent } from './racing-events.component';

describe('RacingEventsComponent', () => {
  let component;
  let locale;
  let storage;


  beforeEach(() => {
    locale = {
      getString: jasmine.createSpy('locale.getString').and.returnValue('France translated')
    };
    storage = {
      get: jasmine.createSpy('get'),
      set: jasmine.createSpy('set'),
    };

    component = new RacingEventsComponent(locale, storage);
  });

  it('ngOnChanges set editable title', () => {
    storage.get.and.returnValue(true);
    component.racing = {
      groupedRacing: [{
        flag: 'UK',
        data: []
      }]
    };

    component.ngOnChanges({moduleTitle: {currentValue: 'UK / Ireland'}});

    expect(component.accordionsState['UK']).toBeTruthy();
    expect(component.titleMap['UK']).toBe('UK / Ireland');
  });

  it('ngOnChanges set predefined title', () => {
    component.sectionTitle = {'FR' : 'France'};
    component.moduleTitle = 'UK / Ireland';
    component.racing = {
      groupedRacing: [{
        flag: 'FR',
        data: []
      }]
    };
    component.ngOnChanges({moduleTitle: {currentValue: 'UK / Ireland'}});

    expect(component.titleMap['FR']).toBe('France translated');

    component.ngOnChanges({});
    expect(component.titleMap['FR']).toBe('France translated');
  });

  it('trackModule', () => {
    component.sectionTitle = {'UK': 'sb.UKIRRaces'};
    component.accordionsState['UK'] = true;
    component.gaTracking.emit = jasmine.createSpy('gaTrakcing.emit');
    component.trackModule('UK', 'horseracing');

    expect(storage.set).toHaveBeenCalledWith('UK', false);
    expect(component.gaTracking.emit).toHaveBeenCalledWith(['sb.UKIRRaces', 'horseracing']);
  });
});
