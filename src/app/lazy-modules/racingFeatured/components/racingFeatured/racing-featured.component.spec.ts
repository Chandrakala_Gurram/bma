import { of as observableOf } from 'rxjs';

import { FeaturedModuleComponent } from '@app/featured/components/featured-module/featured-module.component';
import { RacingFeaturedComponent } from './racing-featured.component';

import { ISystemConfig } from '@core/services/cms/models/system-config';
import environment from '@environment/oxygenEnvConfig';


import { fakeAsync, tick } from '@angular/core/testing';

const racingMock = {
  events: [],
  classesTypeNames: {},
  groupedRacing: [
    {
      flag: 'UK',
      data: [{id: '1'}]
    },
    {
      flag: 'INT',
      data: []
    },
    {
      flag: 'FR',
      data: []
    },
    {
      flag: 'VR',
      data: []
    }
  ],
  selectedTab: 'horseracing',
  modules: {}
};

const featuredModulesMock = {
  modules: [
    {
      '@type': 'RacingEventsModule',
      data: [
        {
          '@type': 'RacingEventsModule'
        }
      ]
    },
    {
      '@type': 'VirtualRaceModule',
      data: [
        {
          '@type': 'test'
        }
      ]
    }
  ]
};

describe('AppRacingFeaturedComponent', () => {
  let component;
  let locale;
  let filtersService;
  let windowRef;
  let pubsub;
  let featuredModuleService;
  let templateService;
  let commentsService;
  let wsUpdateEventService;
  let sportEventHelper;
  let cmsService;
  let promotionsService;
  let changeDetectorRef;
  let router;
  let gtmService;
  let routingHelperService;
  let newRelicService;
  let user;
  let eventService;
  let virtualSharedService;
  let racingGaService;
  let storage;
  let horseRacingService;
  let greyhoundService;
  let routingState;
  let buildUtilityService;
  let timeService;

  const sysConfig: ISystemConfig = {
    InternationalTotePool: {
      Enable_International_Totepools: true
    },
    NextRacesToggle: {
      nextRacesComponentEnabled: true,
      nextRacesTabEnabled: true
    },
    VirtualSports: {
      'virtual-horse-racing': true
    },
    defaultAntepostTab: {
    }
  };


  beforeEach(() => {
    locale = {};
    filtersService = {};
    windowRef = {
      nativeWindow: {
        setInterval:  jasmine.createSpy('setInterval').and.callFake(cb => cb()),
        view: 'Mobile'
      }
    };
    pubsub = {};
    featuredModuleService = {
      cacheEvents: jasmine.createSpy('cacheEvents'),
    };
    templateService = {};
    commentsService = {};
    wsUpdateEventService = {};
    sportEventHelper = {};
    cmsService = {
      getSystemConfig: jasmine.createSpy().and.returnValue(observableOf(sysConfig))
    };
    router = {};
    gtmService = {};
    routingHelperService = {};
    promotionsService = {};
    changeDetectorRef = {
      detach: jasmine.createSpy('cdr.detach'),
      detectChanges: jasmine.createSpy('cdr.detectChanges')
    };
    newRelicService = {};
    user = {};
    eventService = {};
    virtualSharedService = {};
    racingGaService = {
      trackModule: jasmine.createSpy(),
      reset: jasmine.createSpy(),
      todayEventsByClasses: jasmine.createSpy('todayEventsByClasses')
    };

    storage = {
      set: jasmine.createSpy('storage.set')
    };

    horseRacingService = {
      todayEventsByClasses: jasmine.createSpy('todayEventsByClasses').and.returnValue(Promise.resolve(racingMock)),
      getConfig: jasmine.createSpy('getConfig').and.returnValue({
        request: {
          date: 'today'
        }
      }),
      addLpAvailableProp: jasmine.createSpy('addLpAvailableProp'),
      addPersistentInCacheProp: jasmine.createSpy('addPersistentInCacheProp'),
      groupByFlagCodesAndClassesTypeNames: jasmine.createSpy('groupByFlagCodesAndClassesTypeNames').and.returnValue(racingMock),
      sortRaceGroup: jasmine.createSpy('sortRaceGroup')
    };
    greyhoundService = {};
    routingState = {
      getCurrentSegment: jasmine.createSpy('getCurrentSegment').and.returnValue('horseracing')
    };
    buildUtilityService = {
      msEventBuilder: jasmine.createSpy('msEventBuilder')
    };
    timeService = {
      getDayI18nValue: jasmine.createSpy(' timeService.getDayI18nValue').and.returnValue('sb.today')
    };

    component = new RacingFeaturedComponent(
      locale, filtersService, windowRef, pubsub, featuredModuleService, templateService, commentsService,
      wsUpdateEventService, sportEventHelper, cmsService, promotionsService, changeDetectorRef, routingHelperService,
      router, gtmService, newRelicService, user, eventService, virtualSharedService, racingGaService, storage,
      horseRacingService, greyhoundService, routingState, buildUtilityService, timeService
    );
  });

  it('ngOnInit', () => {
    spyOn(FeaturedModuleComponent.prototype, 'ngOnInit');
    spyOn(FeaturedModuleComponent.prototype, 'init');
    component.featuredModuleData = {
      modules: []
    };
    component.sportId = Number(environment.CATEGORIES_DATA.racing.horseracing.id);
    component.racing = racingMock;
    component.setIntTotePool = jasmine.createSpy('setIntTotePool').and.returnValue(true);
    component.ngOnInit();
    component.init();
    expect(component.setIntTotePool).toHaveBeenCalledWith(sysConfig);
    expect(component.isTotePoolsAvailable).toEqual(true);
    expect(component.isHorseracingVirtualsEnabled).toBeTruthy();
    component.sportId = Number(environment.CATEGORIES_DATA.racing.greyhound.id);
    component.ngOnInit();
    component.init();
    expect(component.isTotePoolsAvailable).toEqual(false);
  });

  describe('', () => {
    beforeEach(() => {
      spyOn(FeaturedModuleComponent.prototype, 'ngOnInit');
      spyOn(FeaturedModuleComponent.prototype, 'init');
      component.featuredModuleData = featuredModulesMock;
      component.racing = racingMock;
      component.setIntTotePool = jasmine.createSpy('setIntTotePool').and.returnValue(true);
    });

    it('init', () => {
      component.ngOnInit();
      component.init();
      expect(component.featuredModuleData.modules[1]['@type']).toEqual('VirtualRaceModule');
    });

    it('init with racingEventsModules', () => {
      component.featuredModuleData.modules.push({
        '@type': 'RacingEventsModule',
        data: [
          {
            '@type': 'RacingEventsModule'
          }
        ]
      });
      buildUtilityService.msEventBuilder.and.returnValue({ '@type': 'RacingEventsModule', id: '1', correctedDay: 'sb.tooday'});
      component.ngOnInit();
      component.init();
      expect(racingGaService.todayEventsByClasses).not.toHaveBeenCalled();
    });
  });

  it('ngOnDestroy', () => {
    spyOn(FeaturedModuleComponent.prototype, 'ngOnDestroy');
    component.sysConfigSubscription = { unsubscribe: jasmine.createSpy('sysConfigSubsciption.unsubsctibe') };
    component.ngOnDestroy();

    expect(racingGaService.reset).toHaveBeenCalled();
    expect(component.sysConfigSubscription.unsubscribe).toHaveBeenCalled();
  });

  describe('init', () => {
    it('init', fakeAsync(() => {
      component.featuredLoaded = {
        emit: jasmine.createSpy('featuredLoaded.emit')
      };
      component.racing = racingMock;
      component.featuredModuleData = {
        modules: [
          {
            '@type': 'RacingModule',
            data: [{ name: 'UIR'}]
          },
          {
            '@type': 'RacingModule',
            data: [{ name: 'LVR'}]
          },
          {
            '@type': 'RacingEventsModule',
            data: [{ name: 'IR'}]
          }
        ]
      };
      spyOn(FeaturedModuleComponent.prototype, 'init');
      component.display = 'today';
      component.sportName = 'greyhound';
      component.filter = 'by-time';
      buildUtilityService.msEventBuilder.and.returnValue({ id: '1', correctedDay: 'sb.tooday'});
      component.init();
      tick();
      expect(component.featuredLoaded.emit).toHaveBeenCalled();
      expect(buildUtilityService.msEventBuilder).toHaveBeenCalled();
    }));
  });


  it('setIntTotePool: check whether result returns value from CMS config', () => {
    const result = component.setIntTotePool(sysConfig);
    const resultFalse = component.setIntTotePool({});
    expect(result).toEqual(true);
    expect(resultFalse).toEqual(false);
  });

  it('setHorseracingVirtualSportsSwitcher: check whether result returns value from CMS config', () => {
    const result = component.setHorseracingVirtualSportsSwitcher(sysConfig);
    const resultFalse = component.setHorseracingVirtualSportsSwitcher({});
    expect(result).toEqual(true);
    expect(resultFalse).toEqual(false);
  });

  it('trackModule', () => {
    component.trackModule('horseracing', 'sb.UKRacing');
    expect(racingGaService.trackModule).toHaveBeenCalledWith('horseracing', 'sb.UKRacing');
  });

  it('reloadComponent', () => {
    component.ngOnInit = jasmine.createSpy('ngOnInit');
    component.ngOnDestroy = jasmine.createSpy('ngOnDestroy');
    component.reloadComponent();

    expect(component.ngOnInit).toHaveBeenCalled();
    expect(component.ngOnDestroy).toHaveBeenCalled();
  });

  describe('loadDefaultModules', () => {
    beforeEach(() => {
      component.featuredModuleData = {
        modules: []
      };
      component['groupEvents'] = jasmine.createSpy();
    });

    it('with data', fakeAsync(() => {
      component['loadDefaultModules']();
      tick();
      expect(component.featuredModuleData.modules.find(m => m._id === '1').data).toEqual([{id: '1'}]);
      expect(buildUtilityService.msEventBuilder).not.toHaveBeenCalled();
    }));

    it('withoout data', fakeAsync(() => {
      const mock = {
        events: [],
        classesTypeNames: {},
        groupedRacing: [],
        selectedTab: 'horseracing',
        modules: {}
      };
      horseRacingService.todayEventsByClasses.and.returnValue(Promise.resolve(mock));
      component['loadDefaultModules']();
      tick();
      expect(component.featuredModuleData.modules.find(m => m._id === '1').data).toEqual([]);
    }));
  });

  it('handleInvalidNameSpace', () => {
    spyOn(FeaturedModuleComponent.prototype, 'handleInvalidNameSpace');
    component['loadDefaultModules'] = jasmine.createSpy('loadDefaultModules');
    component.handleInvalidNameSpace();

    expect(component['loadDefaultModules']).toHaveBeenCalled();
  });

  describe('ngOnChanges', () => {
    it('ngOnChanges (groupEvents)', () => {
      const changes = <any>{
        display: {}
      };

      spyOn(component, 'groupEvents');
      component.ngOnChanges(changes);
      expect(component['groupEvents']).toHaveBeenCalled();
    });

    it('ngOnChanges (!groupEvents)', () => {
      const changes = <any>{
        display: { firstChange: true }
      };

      spyOn(component, 'groupEvents');
      component.ngOnChanges(changes);
      expect(component['groupEvents']).not.toHaveBeenCalled();
    });
  });

  it('onModuleUpdate', () => {
    component.featuredModuleData = {
      modules: featuredModulesMock.modules
    } as any;
    component.racing = {
      events: []
    } as any;
    component.badges = {
      'dsge322': {}
    } as any;

    const data = {
      '@type': 'RacingEventsModule',
      _id: 'dsge322',
      data: [
        {
          '@type': 'RacingEventsModule'
        }
      ]
    } as any;
    component.onModuleUpdate(data);
    expect(featuredModuleService.cacheEvents).toHaveBeenCalled();
    expect(buildUtilityService.msEventBuilder).toHaveBeenCalled();
    expect(storage.set).toHaveBeenCalled();
  });

  describe('isSimpleModule', () => {
    it('not simple module', () => {
      const module = {
        '@type': 'lorem'
      } as any;
      expect(component.isSimpleModule(module)).toBe(false);
    });

    it('not racing module true', () => {
      const module = {
        '@type': 'RecentlyPlayedGameModule'
      } as any;
      expect(component.isSimpleModule(module)).toBe(true);
    });

    it('RacingEventsModule module true', () => {
      const module = {
        '@type': 'RacingEventsModule'
      } as any;
      expect(component.isSimpleModule(module)).toBe(true);
    });

    it('InternationalToteRaceModule module true', () => {
      const module = {
        '@type': 'InternationalToteRaceModule'
      } as any;
      expect(component.isSimpleModule(module)).toBe(true);
    });

    it('VirtualRaceModule module true', () => {
      const module = {
        '@type': 'VirtualRaceModule'
      } as any;
      expect(component.isSimpleModule(module)).toBe(true);
    });
  });
});
