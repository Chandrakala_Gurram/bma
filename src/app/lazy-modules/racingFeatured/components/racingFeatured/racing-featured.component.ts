import { Input, Component, ChangeDetectorRef, OnInit, OnDestroy, Output, EventEmitter, OnChanges } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';

import { FeaturedModuleComponent } from '@app/featured/components/featured-module/featured-module.component';

import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { SportEventHelperService } from '@core/services/sportEventHelper/sport-event-helper.service';
import { TemplateService } from '@shared/services/template/template.service';
import { FeaturedModuleService } from '@featured/services/featuredModule/featured-module.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { CommentsService } from '@core/services/comments/comments.service';
import { WsUpdateEventService } from '@core/services/wsUpdateEvent/ws-update-event.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { PromotionsService } from '@promotions/services/promotions/promotions.service';
import { GtmService } from '@core/services/gtm/gtm.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { NewRelicService } from '@core/services/newRelic/new-relic.service';
import { UserService } from '@core/services/user/user.service';
import { EventService } from '@app/sb/services/event/event.service';
import { VirtualSharedService } from '@shared/services/virtual/virtual-shared.service';
import { RacingGaService } from '@racing/services/racing-ga.service';
import { StorageService } from '@core/services/storage/storage.service';
import { ISportEvent } from '@core/models/sport-event.model';
import { IRaceGridMeeting } from '@core/models/race-grid-meeting.model';
import { ISystemConfig } from '@core/services/cms/models';
import { IFeaturedModel } from '@featured/models/featured.model';

import { defaultModules, FLAGS } from './constant';
import { HorseracingService } from '@coreModule/services/racing/horseracing/horseracing.service';
import { GreyhoundService } from '@coreModule/services/racing/greyhound/greyhound.service';
import { RoutingState } from '@shared/services/routingState/routing-state.service';
import { BuildUtilityService } from '@core/services/buildUtility/build-utility.service';
import environment from '@environment/oxygenEnvConfig';
import { TimeService } from '@core/services/time/time.service';
import { IOutputModule } from '@featured/models/output-module.model';

@Component({
  selector: 'racing-featured',
  templateUrl: './racing-featured.component.html'
})
export class RacingFeaturedComponent extends FeaturedModuleComponent implements OnInit, OnDestroy, OnChanges {
  @Input() racing: IRaceGridMeeting;
  @Input() sectionTitle: Object;
  @Input() responseError?;
  @Input() sportName: string;
  @Input() eventsOrder: string[];
  @Input() filter: string;
  @Input() displayNextRaces: boolean;
  @Input() sportModule: string;
  @Input() isExtraPlaceAvailable: boolean;
  @Input() offersAndFeaturedRacesTitle: string;
  @Input() display?: string;
  @Output() readonly featuredLoaded: EventEmitter<IRaceGridMeeting> = new EventEmitter();
  @Output() readonly nextRacesLoaded: EventEmitter<void> = new EventEmitter();

  isTotePoolsAvailable: boolean = false;
  isHorseracingVirtualsEnabled: boolean = false;
  races: {[key: string]: Partial<IRaceGridMeeting> & {racingType: string}};
  sysConfigSubscription: Subscription;
  noEvents: boolean;
  allEvents: ISportEvent[] = [];
  reloadEventsModule: boolean;

  constructor(
    protected locale: LocaleService,
    protected filtersService: FiltersService,
    protected windowRef: WindowRefService,
    protected pubsub: PubSubService,
    protected featuredModuleService: FeaturedModuleService,
    protected templateService: TemplateService,
    protected commentsService: CommentsService,
    protected wsUpdateEventService: WsUpdateEventService,
    protected sportEventHelper: SportEventHelperService,
    protected cmsService: CmsService,
    protected promotionsService: PromotionsService,
    protected changeDetectorRef: ChangeDetectorRef,
    protected routingHelperService: RoutingHelperService,
    public router: Router,
    public gtmService: GtmService,
    protected newRelicService: NewRelicService,
    public user: UserService,
    public eventService: EventService,
    protected virtualSharedService: VirtualSharedService,
    private racingGaService: RacingGaService,
    private storage: StorageService,
    private horseRacingService: HorseracingService,
    private greyhoundService: GreyhoundService,
    private routingState: RoutingState,
    private buildUtilityService: BuildUtilityService,
    private timeService: TimeService,
  ) {
    super(
      locale, filtersService, windowRef, pubsub, featuredModuleService, templateService, commentsService,
      wsUpdateEventService, sportEventHelper, cmsService, promotionsService, changeDetectorRef, routingHelperService,
      router, gtmService, newRelicService, user, eventService, virtualSharedService
    );
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.sysConfigSubscription = this.cmsService.getSystemConfig()
    .subscribe((config: ISystemConfig) => {
      this.isHorseracingVirtualsEnabled = this.setHorseracingVirtualSportsSwitcher(config);
      this.isTotePoolsAvailable = this.setIntTotePool(config)
        && Number(this.sportId) === Number(environment.CATEGORIES_DATA.racing.horseracing.id);
    });
  }

  ngOnChanges(changes): void {
    if (changes.display && !changes['display'].firstChange) {
      this.reloadEventsModule = true;
      this.changeDetectorRef.detectChanges();
      this.groupEvents();
    }
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.racingGaService.reset();
    this.sysConfigSubscription.unsubscribe();
  }

  setIntTotePool(config: ISystemConfig): boolean {
    if (config.InternationalTotePool && config.InternationalTotePool.Enable_International_Totepools) {
      return config.InternationalTotePool.Enable_International_Totepools;
    } else {
      return false;
    }
  }

  /**
   * Set horseracing Virtual Sports Switcher
   * @params {Object} config
   */
  setHorseracingVirtualSportsSwitcher(config: ISystemConfig): boolean {
    if (config.VirtualSports && config.VirtualSports['virtual-horse-racing']) {
      return config.VirtualSports['virtual-horse-racing'];
    } else {
      return false;
    }
  }

  trackModule(moduleName: string, sport: string): void {
    this.racingGaService.trackModule(moduleName, sport);
  }

  reloadComponent(): void {
    this.ngOnDestroy();
    this.ngOnInit();
  }

  init(featured: IFeaturedModel): void {
    super.init(featured);
    this.noEvents = true;

    const racingEventsModules = this.featuredModuleData.modules.filter((module) => module['@type'] === 'RacingEventsModule');
    if(!racingEventsModules.length) {
      this.loadDefaultModules();  // ss-request
    } else {
      this.groupEvents(true);
    }
  }

  handleNextRacesLoaded(): void {
    this.nextRacesLoaded.emit();
  }

  handleInvalidNameSpace(): void {
    super.handleInvalidNameSpace();
    this.loadDefaultModules();
  }

  /**
   * Operations on module update receiving
   * @param {Object} data
   */
  onModuleUpdate(data: IOutputModule): void {
    super.onModuleUpdate(data);
    this.groupEvents(true);
  }

  protected isSimpleModule(module): boolean {
    return super.isSimpleModule(module) || module['@type'] === 'RacingEventsModule' ||
      module['@type'] === 'InternationalToteRaceModule' || module['@type'] === 'VirtualRaceModule';
  }

  private get racingService(): HorseracingService | GreyhoundService {
    const segment = this.routingState.getCurrentSegment();

    return segment.indexOf('horseracing') >= 0 ? this.horseRacingService : this.greyhoundService;
  }

  private groupEvents(isMS: boolean = false): void {
    const racingEventsModules = this.featuredModuleData.modules.filter((module) => module['@type'] === 'RacingEventsModule');

    this.races = {};
    this.allEvents = [];
    racingEventsModules.forEach((module) => {
      this.noEvents = this.noEvents && !module.data.length;
      const config = this.racingService.getConfig().request;
      if (isMS) {
        module.data = module.data.map((event) => this.buildUtilityService.msEventBuilder(event));
      }
      const filteredData = module.data.filter(event => this.display ? event.correctedDay === `sb.${this.display}` : event);
      this.allEvents.push(...filteredData);
      this.racingService.addLpAvailableProp(filteredData);
      this.racingService.addPersistentInCacheProp(filteredData, config.date);
      this.races[module._id] = this.racingService.groupByFlagCodesAndClassesTypeNames(filteredData) as any;
      this.races[module._id].racingType = module.racingType;
      ['groupedRacing', 'classesTypeNames'].forEach(key => this.racing[key] = this.races[module._id][key]);
    });

    // GH only
    // On greyhounds, when filter is by-time, we display all events.
    // This part is just to get any available racing module with events from FEATURED MS
    // and display all events as a single racing-events component

    if(this.filter === 'by-time') {
      let racingModulesCounter = 0;
      this.featuredModuleData.modules = this.featuredModuleData.modules.reduce((acc, val) => {
        if(val['@type'] === 'RacingEventsModule' && racingModulesCounter < 1 ) {
          racingModulesCounter += 1;
          acc.push(val);
         } else if(val['@type'] !== 'RacingEventsModule') {
          acc.push(val);
         }

        return acc;
      }, []);
    }

    this.racing.events = this.allEvents;


    // Save titles and available modules to display them properly on quick-navigation (EDP)

    const titlesMap: {[key: string]: string} = {};
    this.featuredModuleData.modules.filter(module => module['@type'] === 'RacingEventsModule')
      .map((module) => titlesMap[module.racingType] = module.title);
    this.storage.set('racingFeatured', titlesMap);
    this.sortIntRaces();
    this.featuredLoaded.emit(this.racing);
    this.reloadEventsModule = false;
  }

  private loadDefaultModules(): void {
    this.racingService.todayEventsByClasses(true).then((data) => {
      this.featuredModuleData.modules = defaultModules;

      this.featuredModuleData.modules.find(module => module._id === '1').data =
      data.groupedRacing.find(race => race.flag === FLAGS.UK) && data.groupedRacing.find(race => race.flag === FLAGS.UK).data || [];

      this.featuredModuleData.modules.find(module => module._id === '2').data =
      data.groupedRacing.find(race => race.flag !== FLAGS.UK && race.flag !== FLAGS.VR) &&
      data.groupedRacing.filter(race => race.flag !== FLAGS.UK && race.flag !== FLAGS.VR)
                        .map((race) => race.data ).reduce((acc, val) => acc.concat(val), []) || [];

      this.featuredModuleData.modules.find(module => module._id === '3').data =
      data.groupedRacing.find(race => race.flag === FLAGS.VR) && data.groupedRacing.find(race => race.flag === FLAGS.VR).data || [];

      this.groupEvents();
    });
  }
  // Sort Int races (BMA-49338)
  private sortIntRaces(): void {
    const currentDay = this.timeService.getDayI18nValue(new Date().toString()).split('.');
    const intRaces =  Object.values(this.races).find(meeting => meeting.racingType === 'IR') as IRaceGridMeeting;
    this.racingService.sortRaceGroup(intRaces, `racing.${currentDay[1]}`);
  }
}
