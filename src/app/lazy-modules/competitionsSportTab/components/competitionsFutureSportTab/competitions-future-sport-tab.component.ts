import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

import * as _ from 'underscore';

import { ISportEvent } from '@root/app/core/models/sport-event.model';
import { SportTabsService } from '@root/app/sb/services/sportTabs/sport-tabs.service';
import { ITypeSegment } from '@root/app/inPlay/models/type-segment.model';
import { ActivatedRoute } from '@angular/router';
import { RoutingHelperService } from '@root/app/core/services/routingHelper/routing-helper.service';
import { ILoadingState } from '../competitionsSportTab/competitions.model';
import { GamingService } from '@root/app/core/services/sport/gaming.service';
import { DeviceService } from '@root/app/core/services/device/device.service';
import { from, Observable, Subscription } from 'rxjs';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { concatMap } from 'rxjs/operators';
import { EventMethods } from '@app/core/services/cms/models/sport-config-event-methods.model';
import { CmsService } from '@app/core/services/cms/cms.service';
import { ISystemConfig } from '@app/core/services/cms/models';
import { MarketSortService } from '@sb/services/marketSort/market-sort.service';
import { ILazyComponentOutput } from '@root/app/shared/components/lazy-component/lazy-component.model';

@Component({
  selector: 'competitions-future-sport-tab',
  templateUrl: 'competitions-future-sport-tab.component.html',
  styleUrls: ['./competitions-future-sport-tab.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class CompetitionsFutureSportTabComponent implements OnInit, OnDestroy {
  @Input() sport: GamingService;

  @Output() readonly updateLoadingState: EventEmitter<ILoadingState> = new EventEmitter();

  isResponseError: boolean = false;
  isMarketSwitcherConfigured: boolean = false;
  isLoaded: boolean = false;
  eventsBySections: ITypeSegment[];
  categoryId: string;
  limitedSections: { [key: number]: boolean }[] = [];
  private matches: ISportEvent[];
  private outrights: ISportEvent[];
  private readonly componentName: string = 'CompetitionsFutureSportTabComponent';
  private loadEventsSubscription: Subscription;
  private marketSwitcherConfigSubscription: Subscription;
  private eventsLimit: number = 3;
  protected activeMarketFilter: string;

  constructor(
    private sportTabsService: SportTabsService,
    private routingHelperService: RoutingHelperService,
    private pubSubService: PubSubService,
    private activatedRoute: ActivatedRoute,
    private deviceService: DeviceService,
    private cmsService: CmsService,
    private marketSortService: MarketSortService
  ) { }

  ngOnInit(): void {
    this.categoryId = this.sport.config.request.categoryId;
    this.marketSwitcherConfigSubscription = this.cmsService.getMarketSwitcherFlagValue(this.sport.config.name)
    .subscribe((flag: boolean) => { this.isMarketSwitcherConfigured = flag; });
    const eventsDataLoader$: Observable<ISystemConfig> = from(this.sport.getByTab(EventMethods.antepost))
    .pipe(
      concatMap(((matchesEvents: ISportEvent[]) => {
        this.matches = matchesEvents;
        return from(this.sport.getByTab(EventMethods.outrights));
      })),
      concatMap(((outrightEvents: ISportEvent[]) => {
        this.outrights = outrightEvents;
        return this.cmsService.getSystemConfig();
      })));
    this.loadEventsSubscription = eventsDataLoader$.subscribe((systemConfig: ISystemConfig) => {
      if (systemConfig && systemConfig.SportCompetitionsTab && systemConfig.SportCompetitionsTab.eventsLimit) {
        this.eventsLimit = systemConfig.SportCompetitionsTab.eventsLimit;
      }

      let preparedEvents = _.uniq([...this.matches, ...this.outrights], (event => event.id));
      // not to show see all on desktop
      if (!this.deviceService.isDesktop) {
        preparedEvents = this.prepareEvents(preparedEvents);
      }
      this.eventsBySections = preparedEvents && preparedEvents.length ?
        this.prepareAccordions(this.sportTabsService.eventsBySections(preparedEvents, this.sport)) : [];
      this.isLoaded = true;
      this.isResponseError = false;
      this.updateLoadingState.emit({
        isLoaded: this.isLoaded,
        isResponseError: this.isResponseError,
        eventsBySectionsLength: this.eventsBySections.length
      });
      // // Subscribe LS Updates via PUSH updates! unSubscribe will be automatically invoked on next subscribe!
      this.sport.subscribeLPForUpdates(preparedEvents);
      this.pubSubService.subscribe(this.componentName, this.pubSubService.API.DELETE_EVENT_FROM_CACHE, (eventId: string) => {
        this.sportTabsService.deleteEvent(eventId, this.eventsBySections);
        this.updateLoadingState.emit({
          isLoaded: this.isLoaded,
          isResponseError: this.isResponseError,
          eventsBySectionsLength: this.eventsBySections.length
        });
      });
    }, (error => {
      this.isLoaded = true;
      this.isResponseError = true;
      this.updateLoadingState.emit({
        isLoaded: this.isLoaded,
        isResponseError: this.isResponseError,
        eventsBySectionsLength: 0
      });
      console.warn(`Competitions Data:`, error && error.error || error);
    }));
  }

  ngOnDestroy(): void {
    this.sport.unSubscribeLPForUpdates();
    this.pubSubService.unsubscribe(this.componentName);
    this.loadEventsSubscription && this.loadEventsSubscription.unsubscribe();
    this.marketSwitcherConfigSubscription && this.marketSwitcherConfigSubscription.unsubscribe();
  }

  trackByTypeId(index: number, sportSection: ITypeSegment): string {
    return sportSection.typeId;
  }

  goToCompetition(competitionSection: ITypeSegment): string {
    const competitionPageUrl: string = this.routingHelperService.formCompetitionUrl({
      sport: this.activatedRoute.snapshot.paramMap.get('sport'),
      typeName: competitionSection.typeName,
      className: competitionSection.className
    });

    return competitionPageUrl;
  }

  updateState(state: boolean, section: ITypeSegment): void {
    section.isExpanded = state;
  }

  filterEvents(marketFilter: ILazyComponentOutput): void {
    if (!this.activeMarketFilter || this.activeMarketFilter !== marketFilter.value) {
      this.initMarketSelector(marketFilter.value);
    }
  }
  protected initMarketSelector(marketFilter: string): void {
    this.activeMarketFilter = marketFilter;
    this.marketSortService.setMarketFilterForMultipleSections(this.eventsBySections, marketFilter);
    this.eventsBySections = [...this.prepareAccordions(this.eventsBySections)];
  }

  private prepareEvents(events: ISportEvent[]): ISportEvent[] {
    const eventsBySection = this.sport.arrangeEventsBySection(events, true);
    return this.getEventsFromSections(this.limitSections(eventsBySection));
  }

  private getEventsFromSections(sections: ITypeSegment[]): ISportEvent[] {
    return _.reduce(sections, (events: ISportEvent[], section: ITypeSegment): ISportEvent[] => _.union(events, section.events), []);
  }

  private limitSections(sections: ITypeSegment[]): ITypeSegment[] {
    _.each(sections, (section: ITypeSegment) => {
      if (section.events.length > this.eventsLimit) {
        section.events.splice(this.eventsLimit);
        this.limitedSections[section.typeId] = true;
      }
    });
    return sections;
  }

  private prepareAccordions(sections: ITypeSegment[]): ITypeSegment[] {
    let j: number = 0;
    _.each(sections, (section: ITypeSegment) => {
      if (j < 3) {
        section.isExpanded = true;
        j++;
      } else {
        section.isExpanded = false;
      }
    });
    return sections;
  }
}
