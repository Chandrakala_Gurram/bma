import {
  CompetitionsMatchesTabComponent
} from '@lazy-modules/competitionsSportTab/components/competitionsPage/competitionsPageTabs/competitions-matches-tab.component';
import environment from '@environment/oxygenEnvConfig';
import { of as observableOf } from 'rxjs';


describe('#CompetitionsMatchesTabComponent', () => {
  let component: CompetitionsMatchesTabComponent;
  let marketSortService;
  let gamingService;
  let filterService;
  let cmsService;

  let eventsByCategory;
  const orderedEvents = [1, 2, 3];

  beforeEach(() => {
    marketSortService = {
      setMarketFilterForOneSection: jasmine.createSpy('setMarketFilterForOneSection')
    };
    gamingService = {
      sportConfig: {
        config: {
          name: 'cricket',
          request: { marketTemplateMarketNameIntersects: '' },
          tier: 1
        }
      }
    };
    filterService = {
      orderBy: jasmine.createSpy('orderBy').and.returnValue(orderedEvents)
    };

    cmsService = {
      getMarketSwitcherFlagValue: jasmine.createSpy('getMarketSwitcherFlagValue').and.returnValue(observableOf(Boolean))
    };

    eventsByCategory = {
      events: [{
        markets: [{
          name: '',
          templateMarketName: 'Match Betting Head/Head'
        }]
      }]
    } as any;

    component = new CompetitionsMatchesTabComponent(marketSortService, filterService, cmsService);
    component.sport = gamingService as any;
  });

  describe('ngOnChanges', () => {
    beforeEach(() => {
      component.checkSelectedMarkets = jasmine.createSpy('checkSelectedMarkets');
    });

    it('should call checkSelectedMarkets and filteredMatches', () => {
      component.eventsByCategory = {
        groupedByDate: {
          1: {
            deactivated: false,
            events: [10, 20, 30]
          }
        }
      } as any;
      component.filteredMatches = [];

      const changes = {
        eventsByCategory: {
          currentValue: 'test'
        }
      };

      component.ngOnChanges(changes as any);

      expect(component.checkSelectedMarkets).toHaveBeenCalledWith(changes.eventsByCategory.currentValue);
      expect(component.filteredMatches).toEqual([{
        deactivated: false,
        events: orderedEvents
      }] as any);
    });

    it('should not call checkSelectedMarkets ', () => {
      const changes = {};

      component.ngOnChanges(changes as any);

      expect(component.checkSelectedMarkets).not.toHaveBeenCalled();
    });
  });

  describe('ngOnDestroy', () => {
    it('should unsubscribe from marketSwitcherConfig',  () => {
      component['marketSwitcherConfigSubscription'] = {
        unsubscribe: jasmine.createSpy('unsubscribe')
      } as any;
      component.ngOnDestroy();

      expect(component['marketSwitcherConfigSubscription'].unsubscribe).toHaveBeenCalled();
    });
  });

  describe('selectedMarket', () => {
    it('should return selected market name if it previously defined defined', () => {
      component.defaultSelectedMarket = 'market name';

      const result = component.selectedMarket({} as any);

      expect(result).toEqual('market name');
    });

    it('should return `Match Result`', () => {
      component.sportId = environment.CATEGORIES_DATA.footballId;
      const result = component.selectedMarket(eventsByCategory);

      expect(result).toEqual('Match Result');
    });

    it('should return `Default`', () => {
      component.sportId = environment.CATEGORIES_DATA.footballId;
      eventsByCategory = {
        events: [{
          markets: [{
            name: '',
            templateMarketName: 'Match Result'
          }]
        }],
        defaultValue: 'Default'
      };
      const result = component.selectedMarket(eventsByCategory);

      expect(result).toEqual('Default');
    });

    it('should return default selected market (no default selected, not football)', () => {
      component.defaultSelectedMarket = null;
      component.sportId = null;
      expect(component.selectedMarket({} as any)).toEqual(null);
    });
  });

  describe('filterEvents', () => {
    it('should filter Events', () => {
      (component.eventsByCategory as any) = {};

      component.filterEvents({output: '', value: 'MR'});

      expect(marketSortService.setMarketFilterForOneSection).toHaveBeenCalledWith({}, 'MR');
      expect(component.filteredMatches).toEqual([]);
    });
  });

  describe('checkSelectedMarkets', () => {
    it('should return true', () => {
      spyOn<any>(component, 'selectedMarket').and.callFake(() => 'Match Betting Head/Head');
      component.sportId = environment.CATEGORIES_DATA.footballId;
      eventsByCategory.events[0].markets[0].name = 'Match Betting';

      const result = component.checkSelectedMarkets(eventsByCategory);

      expect(result).toBeTruthy();
    });

    it('should return false', () => {
      spyOn<any>(component, 'selectedMarket').and.callFake(() => 'Match Betting');
      component.sportId = environment.CATEGORIES_DATA.footballId;
      eventsByCategory.events[0].markets[0].name = 'Match Result';

      const result = component.checkSelectedMarkets(eventsByCategory);

      expect(result).toBeFalsy();
    });
  });

  describe('trackByTypeId', () => {
    it('should trackBy TypeId', () => {
      const result = component.trackByTypeId({
        typeId: 'typeId'
      } as any);

      expect(result).toEqual('typeId');
    });
  });

  describe('getFilteredMatches', () => {
    it('should get Filtered Matches', () => {
      component.eventsByCategory = {
        groupedByDate: {
          0: {
            deactivated: true
          },
          1: {
            deactivated: false,
            events: [10, 20, 30]
          }
        }
      } as any;

      const result = component.getFilteredMatches();

      expect(result).toEqual([component.eventsByCategory.groupedByDate[1]] as any);
      expect(filterService.orderBy).toHaveBeenCalledWith([10, 20, 30], ['startTime', 'displayOrder', 'name']);
      expect(filterService.orderBy).toHaveBeenCalledTimes(1);
      expect(component.eventsByCategory.groupedByDate['1'].events).toEqual(orderedEvents);
    });

    it('should return empty array if no eventsByCategory', () => {
      component.eventsByCategory = undefined;

      expect(component.getFilteredMatches()).toEqual([]);
    });

    it('should return empty array if no groupedByDate', () => {
      component.eventsByCategory = {
        groupedByDate: undefined
      } as any;

      expect(component.getFilteredMatches()).toEqual([]);
    });
  });

  describe('#reinitHeader', () => {
    it('should assign changed Market', () => {
      const changedMarket = {
        id: '1',
        cashoutAvail: 'cashoutAvail',
        correctPriceTypeCode: 'correctPriceTypeCode',
        dispSortName: 'dispSortName',
      };

      component.reinitHeader(changedMarket as any);

      expect(component.undisplayedMarket).toEqual(changedMarket as any);
    });
  });

  describe('#ngOninit', () => {
    it('check for Tier1', () => {
      component.sportId = environment.CATEGORIES_DATA.footballId;
      component.ngOnInit();
      expect(component.isMarketSelectorAvailable).toBe(true);
    });
  });

  describe('check for isMarketSwitcherConfigured', () => {
    it('should set isMarketSwitcherConfigured to true if cmsService getMarketSwitcherFlagValue return true', () => {
      cmsService.getMarketSwitcherFlagValue.subscribe = jasmine.createSpy('cmsService.getMarketSwitcherFlagValue')
        .and.callFake((flag) => {
          expect(cmsService.getMarketSwitcherFlagValue).toHaveBeenCalled();
          flag = true;
          expect(component.isMarketSwitcherConfigured).toBe(true);
        });
    });
    it('should set isMarketSwitcherConfigured to false if cmsService getMarketSwitcherFlagValue return false', () => {
      cmsService.getMarketSwitcherFlagValue.subscribe = jasmine.createSpy('cmsService.getMarketSwitcherFlagValue')
        .and.callFake((flag) => {
          expect(cmsService.getMarketSwitcherFlagValue).toHaveBeenCalled();
          flag = false;
          expect(component.isMarketSwitcherConfigured).toBe(false);
        });
    });
  });
});
