import { Component, Input, OnChanges, SimpleChanges,OnInit,OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';
import { MarketSortService } from '@sb/services/marketSort/market-sort.service';
import { ITypeSegment, IGroupedByDateObj, IGroupedByDateItem } from '@app/inPlay/models/type-segment.model';
import { IMarket } from '@core/models/market.model';
import { ISportEvent } from '@core/models/sport-event.model';
import environment from '@environment/oxygenEnvConfig';
import { CmsService } from '@core/services/cms/cms.service';
import { GamingService } from '@core/services/sport/gaming.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { ILazyComponentOutput } from '@app/shared/components/lazy-component/lazy-component.model';

@Component({
  selector: 'competitions-matches-tab',
  templateUrl: 'competitions-matches-tab.component.html'
})
export class CompetitionsMatchesTabComponent implements OnInit,OnChanges,OnDestroy {
  @Input() eventsByCategory: ITypeSegment;
  @Input() sportId: string;
  @Input() sport: GamingService;
  @Input() isLoaded: boolean;
  @Input() competitionPage: boolean = false;
  @Input() inner: boolean = true;

  public isMarketSelected: boolean = false;
  public defaultSelectedMarket: string;
  public isMarketSelectorSticky: boolean = true;
  undisplayedMarket: IMarket;
  filteredMatches: IGroupedByDateItem[] = [];
  isMarketSelectorAvailable: boolean = true;
  isMarketSwitcherConfigured: boolean = false;
  private marketSwitcherConfigSubscription: Subscription;

  constructor(
    private marketSortService: MarketSortService,
    private filterService: FiltersService,
    private cmsService: CmsService
  ) { }

  ngOnInit() {
    this.isMarketSelectorAvailable = environment.CATEGORIES_DATA.categoryIds.includes(this.sportId);
    this.marketSwitcherConfigSubscription = this.cmsService.getMarketSwitcherFlagValue(this.sport.sportConfig.config.name)
    .subscribe((flag: boolean) => { this.isMarketSwitcherConfigured = flag; });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.eventsByCategory && changes.eventsByCategory.currentValue) {
      this.isMarketSelected = this.checkSelectedMarkets(changes.eventsByCategory.currentValue);
      this.filteredMatches = this.getFilteredMatches();
    }
  }

  ngOnDestroy(): void {
    this.marketSwitcherConfigSubscription && this.marketSwitcherConfigSubscription.unsubscribe();
  }

  selectedMarket(eventsByCategory: ITypeSegment): string {
    if (this.defaultSelectedMarket) {
      return this.defaultSelectedMarket;
    }

    if (environment.CATEGORIES_DATA.categoryIds.includes(this.sportId)) {
      return eventsByCategory.defaultValue || 'Match Result';
    }

    return this.defaultSelectedMarket;
  }

  /**
   * Filter Events by Markets selector
   * @param {string} marketFilter
   */
  filterEvents(marketFilter: ILazyComponentOutput): void {
    this.marketSortService.setMarketFilterForOneSection(this.eventsByCategory, marketFilter.value);
    this.filteredMatches = this.getFilteredMatches();
  }

  /**
   * Check event markets are selected
   * @param {ITypeSegment} eventsByCategory
   * @returns {boolean}
   */
  checkSelectedMarkets(eventsByCategory: ITypeSegment): boolean {
    return eventsByCategory.events.some((event: ISportEvent) => {
      return event.markets.some((market: IMarket) => {
        market.name = market.name === 'Match Betting'
        && this.sportId === environment.CATEGORIES_DATA.footballId ? 'Match Result' : market.name;
        return market.templateMarketName === this.selectedMarket(eventsByCategory) || market.name === this.selectedMarket(eventsByCategory);
      });
    });
  }

  /**
   * Track Events by typeId
   * @param {ITypeSegment} event
   * @returns {string}
   */
  trackByTypeId(event: ITypeSegment): string {
    return event.typeId;
  }

  /**
   * Fet Filtered Matches
   * @param {IGroupedByDateObj} matches
   * @returns {IGroupedByDateItem[]}
   */
  getFilteredMatches(): IGroupedByDateItem[] {
    const groupedByDate: IGroupedByDateObj = this.eventsByCategory && this.eventsByCategory.groupedByDate;

    if (!groupedByDate) {
      return [];
    }

    const groups = [];

    Object.keys(groupedByDate).forEach((date: string) => {
      if (!groupedByDate[date].deactivated) {
        groupedByDate[date].events = this.filterService.orderBy(groupedByDate[date].events, ['startTime', 'displayOrder', 'name']);
        groups.push(groupedByDate[date]);
      }
    });

    return groups;
  }

  reinitHeader(changedMarket: IMarket): void {
    this.undisplayedMarket = changedMarket;
  }
}
