export const timeline = {
  tlt: {
    closeTutorial: 'OK, THANKS!',
    topLeftArr: 'Breaking news and betting opportunities',
    bottomLeftArr: 'Prices and commentary on live events',
    bottomRightArr: 'Fantastic new offers & promotions'
  },
  title: 'Ladbrokes lounge',
  minimise: 'Minimise'
};
