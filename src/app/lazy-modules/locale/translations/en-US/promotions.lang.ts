export const promotions = {
  promotions: 'Promotions',
  more: 'More',
  ok: 'Ok',
  moreInformation: 'More information',
  noPromotionsFound: 'No active Promotions at the moment',
  noPromo: 'Promotion is expired or unavailable',
  tabsAll: 'All',
  tabsRetail: 'Shop Exclusive',
  termsAndConditionsLabel: 'Terms and Conditions',
  footerText: {
    part1: 'Click here',
    part2: 'for more information about this offer'
  },
  footerMore: 'More Info',
  betNow: 'bet now',
  Promotions: 'Promotions',
  unassignedGroup: 'More from Coral'
};
