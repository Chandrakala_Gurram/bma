export const oddsboost = {
  tokensInfoDialog: {
    title: 'Odds Boost',
    available1: 'You have ',
    available2: ' odds %1 available',
    boost: 'boost',
    boosts: 'boosts',
    terms: '18+. Terms Apply',
    showMore: 'Show more',
    okThanks: 'Ok, thanks',
    dontShowThisAgain: 'Don\'t show this again'
  },
  infoDialog: {
    title: 'Odds Boost',
    text: 'Hit Boost to increase the odds of the bets in your betslip! You can boost up to %2%1 total stake.',
    oddsBoostUnavailable: 'Odds Boost is unavailable for this selection'
  },
  betslipHeader: {
    title: 'Odds Boost',
    subtitle: 'Tap to boost your betslip.',
  },
  boostButton: {
    enabled: 'BOOSTED',
    disabled: 'BOOST',
    reboost: 'RE-BOOST'
  },
  page: {
    title: 'Odds Boost',
    todayOddsBoosts: 'Today\'s Odds Boosts',
    availableNow: 'Available now',
    upcomingBoosts: 'Upcoming boosts',
    availableNowSectionTitle: 'Boosts available now',
    termsConditionsNowSectionTitle: 'Terms & conditions',
    boostUpTo: 'Boost Up To:',
    boostUseBy: 'Use By:',
    boostValidFrom: 'Valid From:',
    noBoostsMessage: 'Look out for boosts appearing here for upcoming events!',
    nextBoostAvailable: 'Next boost available:'
  },
  spDialog: {
    title: 'Odds Boost Unavailable',
    text: 'Odds Boost is unavailable for SP selections',
    ok: 'Ok'
  },
  betslipDialog: {
    noThanks: 'No, thanks',
    yesPlease: 'Yes, please',
    okThanks: 'Ok, thanks',
    continueWithFreeBet: 'Continue with Free Bet?',
    cancelBoostPriceMessage: 'Selecting a Free Bet will cancel your boosted price. Are you sure you want to continue?',
    cantBoostMessage: 'Unfortunately you can\'t boost your odds while using a Free Bet. Please de-select your Free Bet to boost your odds.'
  },
  maxStakeExceeded: {
    title: 'Max Stake Exceeded',
    text: 'The current total stake exceeds the Odds Boost max stake. Please adjust your total stake.' +
         '<br/>You can boost up to <strong>%2%1</strong> total stake.'
  }
};
