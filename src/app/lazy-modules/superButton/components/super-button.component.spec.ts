import { of as observableOf } from 'rxjs';
import { SuperButtonComponent } from './super-button.component';

describe('SuperButtonComponent', () => {
  let component: SuperButtonComponent;
  let cmsService;
  let gtmService;
  let navigationService;
  let changeDetectorRef;
  let deviceService;

  beforeEach(() => {
    spyOn(Date, 'now').and.returnValue(1548323458917); // 2019-01-24
    cmsService = {
      getNavigationPoints: jasmine.createSpy('getNavigationPoints').and.returnValue(observableOf([{
        categoryId: [1, 2, 4],
        competitionId: ['123', '234'],
        homeTabs: ['featured', 'inplay'],
        enabled: true,
        targetUri: '/sport/football',
        title: 'Football',
        description: 'werthjk',
        validityPeriodEnd: '2019-02-24T09:48:20.917Z',
        validityPeriodStart: '2018-12-24T09:48:20.917Z'
      }]))
    };
    gtmService = {
      push: jasmine.createSpy('push')
    };
    navigationService = {
      isInternalUri: jasmine.createSpy('isInternalUri').and.returnValue(true),
      openUrl: jasmine.createSpy('openUrl')
    };
    changeDetectorRef = {
      markForCheck: jasmine.createSpy('markForCheck')
    };
    deviceService = {
      isAndroid: false,
      isWrapper: false
    };

    component = new SuperButtonComponent(cmsService, gtmService, navigationService, changeDetectorRef, deviceService);
    component.type = 'sport';
    component.categoryId = 1;
  });

  it('should create', () => {
    component.showNavPoint = jasmine.createSpy('showNavPoint');

    component.ngOnInit();
    expect(component).toBeTruthy();
    expect(cmsService.getNavigationPoints).toHaveBeenCalledTimes(1);
    expect(component['data'].length).toEqual(1);
    expect(component.showNavPoint).toHaveBeenCalled();
  });

  describe('ngOnChanges', () => {
    it('homeTabs include homeTabUrl', () => {
      component.showNavPoint = jasmine.createSpy('showNavPoint');
      const changes = {
        homeTabUrl: {
          currentValue: 'featured'
        }
      };

      component.ngOnChanges(changes as any);
      expect(component.showNavPoint).toHaveBeenCalled();
    });

    it('homeTabs no include homeTabUrl', () => {
      component.showNavPoint = jasmine.createSpy('showNavPoint');
      const changes = {
        homeTabUrl: {
          currentValue: ''
        }
      };

      component.ngOnChanges(changes as any);
      expect(component.showNavPoint).not.toHaveBeenCalled();
    });
  });


  it('showNavPoint', () => {
    component.ngOnInit();
    expect(component.isShowNavPoint).toBeTruthy();

    component.type = 'bigCompetition';
    component.ngOnInit();
    expect(component.isShowNavPoint).toBeFalsy();

    component.type = 'homeTabs';
    component.ngOnInit();
    expect(component.isShowNavPoint).toBeFalsy();

    expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
  });

  it('setExternal Url link for android Web', () => {
    deviceService.isAndroid = true;
    component.ngOnInit();
    expect(component.isAndroidExternalUrl).toBeFalsy();
  });

  it('setExternal Url link for android wrapper for Internal link', () => {
    deviceService.isAndroid = true;
    deviceService.isWrapper = true;

    component.ngOnInit();
    expect(component.isAndroidExternalUrl).toBeFalsy();
  });

  it('setExternal Url link for android wrapper external link', () => {
    deviceService.isAndroid = true;
    deviceService.isWrapper = true;

    navigationService.isInternalUri.and.returnValue(false);

    component.ngOnInit();
    expect(component.isAndroidExternalUrl).toBeTruthy();
  });

  it('goToUrl', () => {
    component.ngOnInit();
    component.goToUrl();

    expect(gtmService.push).toHaveBeenCalledWith(
      'trackEvent', {
        eventCategory: 'cta',
        eventAction: 'Football'
      }
    );
    expect(navigationService.openUrl).toHaveBeenCalledWith('/sport/football');
  });


  it('goToUrl call redirect if it is in-shop user', () => {
    component.ngOnInit();
    component.goToUrl();

    expect(gtmService.push).toHaveBeenCalledWith(
      'trackEvent', {
        eventCategory: 'cta',
        eventAction: 'Football'
      }
    );
    expect(navigationService.openUrl).toHaveBeenCalledWith('/sport/football');
  });
});
