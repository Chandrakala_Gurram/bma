import {
  Component,
  OnInit,
  OnChanges,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { GtmService } from '@core/services/gtm/gtm.service';
import { INavigationPoint } from '@core/services/cms/models';
import { NavigationService } from '@core/services/navigation/navigation.service';
import { DeviceService } from '@core/services/device/device.service';

@Component({
  selector: 'super-button',
  templateUrl: './super-button.component.html',
  styleUrls: ['./super-button.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SuperButtonComponent implements OnInit, OnChanges {
  @Input() type: string;
  @Input() homeTabUrl: string;
  @Input() competitionId: string;
  @Input() categoryId: number;

  public navPoint: INavigationPoint;
  public isShowNavPoint: boolean;
  public isAndroidExternalUrl: boolean;

  private data: INavigationPoint[];

  constructor(
    private cmsService: CmsService,
    private gtmService: GtmService,
    private navigationService: NavigationService,
    private changeDetectorRef: ChangeDetectorRef,
    private deviceService: DeviceService
  ) { }

  ngOnInit(): void {
    this.cmsService.getNavigationPoints().subscribe((item: INavigationPoint[]) => {
      this.data = this.validatePeriod(item);
      this.showNavPoint();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.homeTabUrl && changes.homeTabUrl.currentValue) {
      this.showNavPoint();
    }
  }

  showNavPoint(): void {
    this.navPoint = this.pickNavigationPointFn()();
    this.isShowNavPoint = !!this.navPoint;

    if (this.isShowNavPoint) {
      const isAndroidWrapper = this.deviceService.isAndroid && this.deviceService.isWrapper;

      // Due to issues with window.open, for android wrappers and for external links we will use A link with target blank
      this.isAndroidExternalUrl = isAndroidWrapper && !this.navigationService.isInternalUri(this.navPoint.targetUri);
    }

    this.changeDetectorRef.markForCheck();
  }

  goToUrl(): void {
    this.gtmService.push('trackEvent', {
      eventCategory: 'cta',
      eventAction: this.navPoint.title
    });
    this.navigationService.openUrl(this.navPoint.targetUri);
  }

  /*
   * Return function for component type attribute
   * @return {Function}
   * @private
   */
  private pickNavigationPointFn(): Function {
    return {
      bigCompetition: this.getCompetitionNavigationPoint.bind(this),
      homeTabs: this.getHomeTabsNavigationPoint.bind(this),
      sport: this.getSportNavigationPoint.bind(this)
    }[this.type];
  }

  /*
   * Filter data from expired elements
   * @param {Array} arr
   * @return {Array}
   * @private
   */
  private validatePeriod(arr: INavigationPoint[]): INavigationPoint[] {
    return arr.filter((point: INavigationPoint) => {
      return Date.now() < Date.parse(point.validityPeriodEnd) &&
        Date.now() > Date.parse(point.validityPeriodStart);
    });
  }

  /*
   * Find navigation point by url id for home tabs
   * @return {Object}
   * @private
   */
  private getHomeTabsNavigationPoint(): INavigationPoint {
    return this.data.find((point: INavigationPoint) => point.homeTabs.includes(this.homeTabUrl));
  }

  /*
   * Find navigation point by competition id for big competitions
   * @return {Object}
   * @private
   */
  private getCompetitionNavigationPoint(): INavigationPoint {
    return this.data.find((point: INavigationPoint) => point.competitionId.includes(this.competitionId));
  }

  /*
   * Find navigation point by category id for sport
   * @return {Object}
   * @private
   */
  private getSportNavigationPoint(): INavigationPoint {
    return this.data.find((point: INavigationPoint) => point.categoryId.includes(+this.categoryId));
  }
}
