import { Component, OnInit, HostListener, HostBinding, ChangeDetectionStrategy, Input, OnDestroy } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { GAMING_CONSTANTS } from '@lazy-modules/gamingOverlay/components/gaming-overlay.constants';

@Component({
  selector: 'gaming-overlay',
  templateUrl: './gaming-overlay.component.html',
  styleUrls: ['./gaming-overlay.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GamingOverlayComponent implements OnInit, OnDestroy {
  @HostBinding('class.isActive') isActive: boolean = true;
  @Input() url: string;

  overlayUrl: SafeResourceUrl;
  isLoaded: boolean = false;
  height: number = 0;
  width: number = 0;

  private readonly componentName: string = 'GamingOverlay';

  constructor(
    private sanitizer: DomSanitizer,
    private windowRef: WindowRefService,
    private pubSubService: PubSubService
    ) { }

  ngOnInit(): void {
    this.overlayUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    this.pubSubService.subscribe(this.componentName, this.pubSubService.API.GAMING_OVERLAY_OPEN, () => {
      this.isActive = true;
    });
  }

  ngOnDestroy() {
    this.pubSubService.unsubscribe(this.componentName);
  }

  /**
   * listener for post messages from Gaming Overlay iframe
   * @param event {MessageEvent} post message from iframe
   */
  @HostListener('window:message', ['$event'])
  onMessage(event: MessageEvent): void {
    switch (event.data.eventName) {
      case GAMING_CONSTANTS.LOADED:
        this.isLoaded = true;
        break;
      case GAMING_CONSTANTS.ACTION:
        this.windowRef.nativeWindow.location.href = event.data.params.redirectUrl;
        break;
      case GAMING_CONSTANTS.LAUNCH:
        this.windowRef.nativeWindow.location.href = event.data.params.redirectUrl;
        break;
      case GAMING_CONSTANTS.CLICK:
        this.isActive = false;
        break;
    }
  }
}
