import { GamingOverlayComponent } from '@lazy-modules/gamingOverlay/components/gaming-overlay.component';

describe('GamingOverlayComponent', () => {
  let component, sanitizer, windowRef, pubSubService;
  const overlayUrl = 'https://gaming.ladbrokes.com/';

  beforeEach(() => {

    sanitizer = {
      bypassSecurityTrustResourceUrl: jasmine.createSpy('sanitizer.bypassSecurityTrustResourceUrl')
        .and.returnValue(overlayUrl)
    };

    pubSubService = {
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: {
        GAMING_OVERLAY_OPEN: 'GAMING_OVERLAY_OPEN'
      }
    };

    windowRef = {
      nativeWindow: {
        location: {
          href: ''
        }
      }
    };

    component = new GamingOverlayComponent(
      sanitizer,
      windowRef,
      pubSubService
    );
  });

  it('should create a component', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    pubSubService.subscribe.and.callFake((name, channel, callBack) => {
      if (channel === 'GAMING_OVERLAY_OPEN') {
        callBack();
      }
    });
    component.url = overlayUrl;
    component.ngOnInit();

    expect(sanitizer.bypassSecurityTrustResourceUrl).toHaveBeenCalledWith(overlayUrl);
    expect(component.overlayUrl).toBe(overlayUrl);
    expect(component.isActive).toBe(true);
  });

  it('ngOnDestroy', () => {
    component.ngOnDestroy();

    expect(pubSubService.unsubscribe).toHaveBeenCalled();
  });

  describe('onMessage', () => {
    it('should set LOADED state', () => {
      const event = { data: { eventName: 'loadingCompleted' }} as MessageEvent;
      component.onMessage(event);
      expect(component.isLoaded).toBe(true);
    });

    it('should set ACTION state', () => {
      const event = { data: { eventName: 'UserAction', params: { redirectUrl: 'https://gaming.ladbrokes.com/' } }} as MessageEvent;
      component.onMessage(event);
      expect(windowRef.nativeWindow.location.href).toBe('https://gaming.ladbrokes.com/');
    });

    it('should set LAUNCH state', () => {
      const event = { data: { eventName: 'GameLaunch', params: { redirectUrl: 'https://gaming.ladbrokes.com/' } }} as MessageEvent;
      component.onMessage(event);
      expect(windowRef.nativeWindow.location.href).toBe('https://gaming.ladbrokes.com/');
    });

    it('should set CLICK state', () => {
      const event = { data: { eventName: 'sportsoverlaycloseclick' }} as MessageEvent;
      component.onMessage(event);
      expect(component.isActive).toBe(false);
    });
  });
});
