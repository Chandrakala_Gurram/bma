export const FORECAST_CONFIG = {
  forecastMarketPath: 'forecast',
  tricastMarketPath: 'tricast',
  poolTypesMap: {
    FC: { name: 'Forecast', path: 'forecast' },
    TC: { name: 'Tricast', path: 'tricast' }
  }
};
