import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { CouponsModule } from '@sb/coupons/coupons.module';
import { LazyCouponsListHomeTabRoutingModule } from './coupons-list-home-tab-routing.module';

@NgModule({
  imports: [
    CouponsModule,

    LazyCouponsListHomeTabRoutingModule
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  exports: [],
  schemas: [NO_ERRORS_SCHEMA]
})
export class LazyCouponsListHomeTabModule { }
