import { Component, OnDestroy, OnInit } from '@angular/core';
import { timeout } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { QuestionEngineService } from '@app/questionEngine/services/question-engine/question-engine.service';
import { DeviceService } from '@core/services/device/device.service';
import { NewRelicService } from '@core/services/newRelic/new-relic.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { LocaleService } from '@core/services/locale/locale.service';

import { QuestionEngineModel } from '@app/questionEngine/models/questionEngineModel.model';
import { IQuizHistoryModel } from '@app/questionEngine/models/quizHistory.model';
import {
  BACKEND_RESPONSE_TIMEOUT_LIMIT,
  QE_INIT_DATA_FAILURE,
} from '@app/questionEngine/constants/question-engine.constant';

@Component({
  selector: 'question-engine-main',
  templateUrl: './question-engine-main.component.html',
  styleUrls: ['./question-engine-main.component.less'],
})
export class QuestionEngineMainComponent implements OnInit, OnDestroy {
  isMobile: boolean = true;
  dataLoadingError: string = this.localeService.getString('qe.dataLoadingError');
  dataLoadingErrorBtn: string = this.localeService.getString('qe.dataLoadingErrorBtn');
  public qeData: QuestionEngineModel;
  public errorMessage: string;
  protected routeChangeListener: Subscription;
  protected quizHistoryListener: Subscription;
  private readonly tag = 'QeMainComponent';

  constructor(
    protected pubSubService: PubSubService,
    protected deviceService: DeviceService,
    protected questionEngineService: QuestionEngineService,
    protected newRelicService: NewRelicService,
    protected localeService: LocaleService,
    protected router: Router,
  ) {}

  ngOnInit(): void {
    this.isMobile = this.deviceService.isMobile;
    this.isMobile && this.pubSubService.publish(this.pubSubService.API.TOGGLE_MOBILE_HEADER_FOOTER, false);
    this.pubSubService.subscribe(this.tag, this.pubSubService.API.QE_FATAL_ERROR,
      (errorMessage, error) => {
        this.errorMessage = errorMessage;
        error && this.newRelicService.noticeError(error);
        console.error(errorMessage);
        this.newRelicService.addPageAction(`Question Engine fatal error`, {error: errorMessage});
      });

    this.initComponentData();
    this.pubSubService.subscribe(this.tag, this.pubSubService.API.SUCCESSFUL_LOGIN, () => {
      this.initComponentData();
    });
  }

  ngOnDestroy(): void {
    this.isMobile && this.pubSubService.publish(this.pubSubService.API.TOGGLE_MOBILE_HEADER_FOOTER, true);
    this.questionEngineService.resetCheckForAnonymousDataValue();
    this.pubSubService.unsubscribe(this.tag);
    this.routeChangeListener && this.routeChangeListener.unsubscribe();
    this.quizHistoryListener && this.quizHistoryListener.unsubscribe();
  }

  private initComponentData(initial: boolean = false): void {
    this.quizHistoryListener = this.questionEngineService.getQuizHistory(initial)
      .pipe(timeout(BACKEND_RESPONSE_TIMEOUT_LIMIT))
      .subscribe((data: IQuizHistoryModel) => {

        // if no quiz live or previous game data from BE - perform default data fetch
        if (this.questionEngineService.checkGameData(data, this.initComponentData.bind(this))) {
          return;
        }

        this.qeData = this.questionEngineService.mapResponseOnComponentModel(data);
        this.questionEngineService.setQEDataUptodateStatus(true);
        this.pubSubService.publish(this.pubSubService.API.QE_HISTORY_DATA_RECEIVED);
      },
      (error) => {
        this.pubSubService.publish(this.pubSubService.API.QE_FATAL_ERROR, [QE_INIT_DATA_FAILURE, error]);
      });
  }

}
