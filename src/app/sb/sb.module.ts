import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'angular-custom-modal';

import { LocaleService } from '@core/services/locale/locale.service';
import * as sbLangData from '@localeModule/translations/en-US/sb.lang';

import { SharedModule } from '@sharedModule/shared.module';
import { OutcomeTemplateHelperService } from '@sb/services/outcomeTemplateHelper/outcome-template-helper.service';
import { BuildYourBetTabComponent } from '@sb/components/buildYourBetTab/build-your-bet-tab.component';
import { BuildYourBetHomeComponent } from '@sb/components/buildYourBetHome/build-your-bet-home.component';
import { SmartBoostsService } from '@sb/services/smartBoosts/smart-boosts.service';
import { LuckyDipDialogComponent } from '@sb/components/jackpotSportTab/luckyDipDialog/lucky-dip-dialog.component';
import { JackpotReceiptPageComponent } from '@sb/components/jackpotReceiptPage/jackpot-receipt-page.component';
import { HowToPlayDialogComponent } from '@sb/components/jackpotSportTab/howToPlayDialog/how-to-play-dialog.component';
import { OutrightsSportTabComponent } from '@sb/components/outrightsSportTab/outrights-sport-tab.component';
import { JackpotSportTabComponent } from '@sb/components/jackpotSportTab/jackpot-sport-tab.component';
import { EnhancedMultiplesTabComponent } from '@sbModule/components/enhancedMultiplesModule/enhanced-multiples-tab.component';
import { PrivateMarketsTabComponent } from '@sb/components/privateMarketsTab/private-markets-tab.component';
import { PrivateMarketsTermsAndConditionsComponent } from '@sb/components/privateMarketsTab/private-markets-terms-and-conditions.component';
import { SpecialsSportTabComponent } from '@sb/components/specialsSportTab/specials-sport-tab.component';
import { SbFiltersService } from '@sb/services/sbFilters/sb-filters.service';
import { BannersModule } from '@banners/banners.module';
import { SportTabsService } from '@sb/services/sportTabs/sport-tabs.service';
import { JackpotReceiptPageService } from '@sb/components/jackpotReceiptPage/jackpot-receipt-page.service';
import { JackpotSportTabService } from '@sb/components/jackpotSportTab/jackpot-sport-tab.service';
import { CurrentMatchesService } from '@sb/services/currentMatches/current-matches.service';
import { DividendsService } from '@sb/services/dividents/dividends.service';
import { EnhancedMultiplesService } from '@sb/services/enhancedMultiples/enhanced-multiples.service';
import { EventService } from '@sb/services/event/event.service';
import { EventFiltersService } from '@sb/services/eventFilters/event-filters.service';
import { EventsByClassesService } from '@sb/services/eventsByClasses/events-by-classes.service';
import { IsPropertyAvailableService } from '@sb/services/isPropertyAvailable/is-property-available.service';
import { LiveStreamService } from '@sb/services/liveStream/live-stream.service';
import { MarketSortService } from '@sb/services/marketSort/market-sort.service';
import { StreamTrackingService } from '@sb/services/streamTracking/stream-tracking.service';
import { OlympicsService } from '@sb/services/olympics/olympics.service';

import { MatchResultsSportTabComponent } from '@app/sb/components/matchResultsSportTab/match-results-sport-tab.component';

@NgModule({
  imports: [
    HttpClientModule,
    FormsModule,
    ModalModule,
    SharedModule,
    BannersModule
  ],
  providers: [
    JackpotSportTabService,
    JackpotReceiptPageService,
    EventFiltersService,
    EnhancedMultiplesService,
    IsPropertyAvailableService,
    DividendsService,
    EventsByClassesService,
    LiveStreamService,
    StreamTrackingService,
    MarketSortService,
    SportTabsService,
    OutcomeTemplateHelperService,
    EventService,
    CurrentMatchesService,
    SbFiltersService,
    DatePipe,
    SmartBoostsService,
    OlympicsService
  ],
  declarations: [
    JackpotReceiptPageComponent,
    OutrightsSportTabComponent,
    LuckyDipDialogComponent,
    HowToPlayDialogComponent,
    JackpotSportTabComponent,
    SpecialsSportTabComponent,
    EnhancedMultiplesTabComponent,
    BuildYourBetTabComponent,
    PrivateMarketsTabComponent,
    PrivateMarketsTermsAndConditionsComponent,
    MatchResultsSportTabComponent,
    BuildYourBetHomeComponent
  ],
  entryComponents: [
    LuckyDipDialogComponent,
    JackpotReceiptPageComponent,
    HowToPlayDialogComponent,
    JackpotSportTabComponent,
    OutrightsSportTabComponent,
    SpecialsSportTabComponent,
    EnhancedMultiplesTabComponent,
    BuildYourBetTabComponent,
    PrivateMarketsTabComponent,
    PrivateMarketsTermsAndConditionsComponent,
    BuildYourBetHomeComponent
  ],
  exports: [
    JackpotReceiptPageComponent,
    EnhancedMultiplesTabComponent,
    BuildYourBetTabComponent,
    PrivateMarketsTabComponent,
    PrivateMarketsTermsAndConditionsComponent,
    MatchResultsSportTabComponent,
    BuildYourBetHomeComponent,
    SpecialsSportTabComponent,
    JackpotSportTabComponent,
    OutrightsSportTabComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SbModule {
  constructor(private localeService: LocaleService) {
    this.localeService.setLangData(sbLangData);
  }
}


