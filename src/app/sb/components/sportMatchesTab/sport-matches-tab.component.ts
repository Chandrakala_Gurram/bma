import {
  ChangeDetectorRef,
  Component,
  Input,
  Output,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  EventEmitter
} from '@angular/core';
import { from, Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'underscore';
import { CmsService } from '@core/services/cms/cms.service';
import { SportTabsService } from '@sb/services/sportTabs/sport-tabs.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { EnhancedMultiplesService } from '@sb/services/enhancedMultiples/enhanced-multiples.service';
import { StorageService } from '@core/services/storage/storage.service';
import { ISportEvent } from '@core/models/sport-event.model';
import { MarketSortService } from '@sb/services/marketSort/market-sort.service';
import { ITypeSegment, IGroupedByDateItem } from '@app/inPlay/models/type-segment.model';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { GamingService } from '@core/services/sport/gaming.service';
import { Location } from '@angular/common';
import { GtmService } from '@core/services/gtm/gtm.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { FavouritesService } from '@root/app/favourites/services/favourites.service';
import { IMarket } from '@root/app/core/models/market.model';
import { ILazyComponentOutput } from '@shared/components/lazy-component/lazy-component.model';

@Component({
  selector: 'sport-matches-tab',
  templateUrl: 'sport-matches-tab.component.html'
})
export class SportMatchesTabComponent implements OnInit, OnDestroy, OnChanges {
  @Input() sport: GamingService;
  @Input() tab: string = '';
  @Input() featuredEventsCount: number;

  @Output() readonly isLoadedEvent = new EventEmitter<ILazyComponentOutput>();

  eventsBySections: ITypeSegment[] = [];
  eventsCache: ISportEvent[] = [];

  showEventsBySections: boolean;
  enhancedEvents: ISportEvent[] = [];
  isLoaded: boolean = false;
  isResponseError: boolean = false;
  isExpandedEnhanced: boolean = true;
  isLoadedEnhanced: boolean = false;
  sportName: string;
  sportId: number;
  isDisplayTutorial: boolean;
  showAccordionBody: boolean;
  detectListener;
  groupedByDateEnhancedEvents: ITypeSegment;
  locationPath: string;
  isMarketSelectorActive: boolean;
  readonly tag = 'MatchesSportTabComponent';
  undisplayedMarket: IMarket;
  isMarketSwitcherConfigured: boolean = false;

  protected activeMarketFilter: string;
  private loadDataSubscription: Subscription;
  private enhancedMultiplesSubscription: Subscription;
  private marketSwitcherConfigSubscription: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private cmsService: CmsService,
    private sportTabsService: SportTabsService,
    private marketSortService: MarketSortService,
    private enhancedMultiplesService: EnhancedMultiplesService,
    private storageService: StorageService,
    private pubSubService: PubSubService,
    private windowRef: WindowRefService,
    private changeDetectorRef: ChangeDetectorRef,
    private location: Location,
    private gtmService: GtmService,
    private routingHelperService: RoutingHelperService,
    private favouritesService: FavouritesService,
    private router: Router
  ) {
    this.changeDetectorRef.detach();
    this.locationPath = this.location.path();
  }

  ngOnInit(): void {
    this.detectListener = this.windowRef.nativeWindow.setInterval(() => {
      this.changeDetectorRef.detectChanges();
    }, 500);

    this.sportName = this.activatedRoute.snapshot.paramMap.get('sport');
    this.sportId = Number(this.sport.sportConfig.config.request.categoryId);
    this.isDisplayTutorial = this.sportName === 'football' &&
      !this.storageService.get('footballTutorial') &&
      this.favouritesService.isFavouritesEnabled;

    this.loadMatchesData();

    this.pubSubService.subscribe(this.tag, this.pubSubService.API.DELETE_MARKET_FROM_CACHE, () => {
      if (this.eventsCache) {
        this.sport.setMarketsAvailability(this.eventsCache, this.eventsBySections);
      }
      this.initMarketSelector(this.activeMarketFilter);
    });

    this.pubSubService.subscribe(this.tag, this.pubSubService.API.DELETE_EVENT_FROM_CACHE, (eventId: string) => {
      this.sportTabsService.deleteEvent(eventId, this.eventsBySections);
    });

    this.marketSwitcherConfigSubscription = this.cmsService.getMarketSwitcherFlagValue(this.sport.sportConfig.config.name)
      .subscribe((flag: boolean) => { this.isMarketSwitcherConfigured = flag; });
  }

  ngOnDestroy(): void {
    if (this.loadDataSubscription) {
      this.loadDataSubscription.unsubscribe();
    }

    this.unsubscribeFromLiveUpdates();
    clearInterval(this.detectListener);
    this.pubSubService.unsubscribe(this.tag);
    this.enhancedMultiplesSubscription && this.enhancedMultiplesSubscription.unsubscribe();
    this.marketSwitcherConfigSubscription && this.marketSwitcherConfigSubscription.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.tab && changes.tab.currentValue && changes.tab.previousValue) {
      this.activeMarketFilter = undefined;
      this.loadMatchesData();
    }

    if (changes.sport && changes.sport.currentValue !== changes.sport.previousValue) {
      this.sport = changes.sport.currentValue;
    }

    if (changes.featuredEventsCount && changes.featuredEventsCount.currentValue !== changes.featuredEventsCount.previousValue) {
      this.featuredEventsCount = changes.featuredEventsCount.currentValue;
    }
  }

  updateDynamicProperties(): void {
    this.isMarketSelectorActive = this.eventsBySections.length > 0;
    this.showEventsBySections = this.eventsBySections.length && (!this.isMarketSelectorActive || !!this.eventsBySections[0].defaultValue);
  }

  selectedMarket(eventsBySection: ITypeSegment): string {
    const primaryMarketName = eventsBySection.defaultValue; // check football
    return primaryMarketName;
  }

  trackByTypeId(index: number, sportSection: ITypeSegment): string {
    return `${sportSection.typeId}_${sportSection.deactivated}`;
  }

  trackById(index: number, sportEvent: ISportEvent): number {
    return sportEvent.id;
  }

  filterEvents(marketFilter: string): void {
    if (!this.activeMarketFilter || this.activeMarketFilter !== marketFilter) {
      this.initMarketSelector(marketFilter);
    }
  }

  hideEnhancedSection(): void {
    this.isExpandedEnhanced = false;
  }

  trackEvent(eventEntity: ISportEvent): void {
    this.gtmService.push('trackEvent', {
      event: 'trackEvent',
      eventCategory: 'upcoming module',
      eventAction: this.locationPath,
      eventName: eventEntity.name,
      eventLabel: 'view event',
      eventID: eventEntity.id
    });
  }

  trackByDate(index: number, dateGroup: IGroupedByDateItem) {
    return `${dateGroup.startTime}_${dateGroup.title.replace(' ', '_')}_${index}`;
  }

  isPrimaryMarket(eventsBySection: ITypeSegment): boolean {
    // non Football case
    if (!eventsBySection.defaultValue) {
      return true;
    }
    // Football Primary market case
    return eventsBySection.defaultValue.toLowerCase() === 'match result';
  }

  updateState(state: boolean, type, section?: ITypeSegment): void {
    if (type === 'enhanced') {
      this.isExpandedEnhanced = state;
    }

    if (type === 'event' && section) {
      // this.activeMarketFilter
      if (state) {
        // If section was not - subscribe for live updates
        this.subscribeForSectionUpdates(section);
      } else if (!state && section.isExpanded) {
        // If section was expanded - unsubscribe from live updates
        this.unsubscribeFromSectionUpdates(section);
      }

      section.isExpanded = state;
    }
  }

  goToCompetition(competitionSection: ITypeSegment): void {
    const competitionPageUrl = this.routingHelperService.formCompetitionUrl({
      sport: this.activatedRoute.snapshot.paramMap.get('sport'),
      typeName: competitionSection.typeName,
      className: competitionSection.className
    });

    this.gtmService.push('trackEvent', {
      event: 'trackEvent',
      eventCategory: 'upcoming module',
      eventAction: this.locationPath,
      eventLabel: 'see all',
      competitionName: competitionSection.sectionTitle
    });

    this.router.navigateByUrl(competitionPageUrl);
  }

  handleOutput(output: ILazyComponentOutput) {
    if (output.output === 'filterChange') {
      this.filterEvents(output.value);
    }
    if (output.output === 'hideEnhancedSection') {
      this.hideEnhancedSection();
    }
  }

  reinitHeader(changedMarket: IMarket): void {
    this.undisplayedMarket = changedMarket;
  }

  protected prepeareAccordions(sections: ITypeSegment[]): ITypeSegment[] {
    let j: number = 0;

    _.each(sections, (section: ITypeSegment) => {
      if (!section.deactivated && j < 3) {
        this.updateState(true, 'event', section);
        j++;
      } else if (!section.isExpanded) {
        this.updateState(false, 'event', section);
      }
    });
    return sections;
  }

  protected initMarketSelector(marketFilter: string): void {
    this.activeMarketFilter = marketFilter;
    this.showAccordionBody = false;
    this.marketSortService.setMarketFilterForMultipleSections(this.eventsBySections, marketFilter);

    this.windowRef.nativeWindow.setTimeout(() => {
      this.eventsBySections = [...this.prepeareAccordions(this.eventsBySections)];
      this.updateDynamicProperties();
      this.showAccordionBody = true;
    });
  }

  private isInPlayEvent(event: ISportEvent): boolean {
    return Boolean(event.isLiveNowEvent) === true || Boolean(event.isStarted) === true;
  }

  private filterOutInplayEvents(events: ISportEvent[]) {
    return _.filter(events, event => !this.isInPlayEvent(event));
  }

  private filterUpcomingEvents(events: ISportEvent[]): ISportEvent[] {
    let upcomingEvents = events;
    upcomingEvents = this.sport.filterOutFutureEvents(events);
    return this.filterOutInplayEvents(upcomingEvents);
  }

  /**
   * Load Sport Matches Data
   */
  private loadMatchesData(): void {
    this.isLoaded = false;
    this.isLoadedEvent.emit({ output: 'isLoadedEvent', value: this.isLoaded });
    this.isResponseError = false;
    const tabName = this.tab || 'upcoming';

    this.unsubscribeFromLiveUpdates();
    this.eventsBySections = [];
    this.changeDetectorRef.detectChanges();
    if (this.loadDataSubscription) {
      this.loadDataSubscription.unsubscribe();
    }

    this.loadDataSubscription = from(this.sport.getByTab(tabName))
      .subscribe((events: ISportEvent[]) => {
        this.eventsCache = events;
        this.eventsBySections = events && events.length ?
          this.sportTabsService.eventsBySections(events, this.sport) : [];
        this.isResponseError = false;
        this.eventsBySections = this.prepeareAccordions(this.eventsBySections);
        this.updateDynamicProperties();
        this.hideLoading();

        this.activeMarketFilter &&
          this.marketSortService.setMarketFilterForMultipleSections(this.eventsBySections, this.activeMarketFilter);
      }, (error) => {
        this.isResponseError = true;
        console.warn(`Matches ${tabName} Data:`, error && error.error || error);
        this.hideLoading();
      });

    this.enhancedMultiplesSubscription = this.enhancedMultiplesService.getEnhancedMultiplesEvents(this.sportName, this.tab)
      .subscribe((result: ISportEvent[]) => {
        this.enhancedEvents = this.sortSportEventsData(result);
        const upcomingEvents = tabName === 'upcoming' ? this.filterUpcomingEvents(this.enhancedEvents)
          : this.enhancedEvents;
        if (upcomingEvents && upcomingEvents.length) {
          this.groupedByDateEnhancedEvents = this.sport.arrangeEventsBySection(upcomingEvents, true)[0];
        }
        this.isLoadedEnhanced = true;
        // display overlay
        setTimeout(() => {
          this.pubSubService.publish(this.pubSubService.API.SHOW_FOOTBALL_TUTORIAL);
        });
      }, (error => {
        this.isLoadedEnhanced = true;
        console.warn('Enhanced Data:', error && error.error || error);
      }));
  }

  /**
   * Sort results data
   * @param {ISportEvent[]} data
   * @returns {ISportEvent[]}
   */
  private sortSportEventsData(data: ISportEvent[]): ISportEvent[] {
    return _(data).chain().sortBy((event: ISportEvent) => {
      return event.name.toLowerCase();
    }).sortBy((event: ISportEvent) => {
      return event.startTime;
    }).value();
  }

  private hideLoading(): void {
    this.isLoaded = true;
    this.changeDetectorRef.detectChanges();
    this.windowRef.nativeWindow.setTimeout(() => this.isLoadedEvent.emit({ output: 'isLoadedEvent', value: this.isLoaded }));
  }

  /**
   * Subscribe for sEVENT, sEVMKT, sSELCN updates of all events in given section.
   * @param {ITypeSegment} section
   */
  private subscribeForSectionUpdates(section: ITypeSegment): void {
    if (section && !section.subscriptionKey) {
      section.subscriptionKey = this.sport.subscribeEventChildsUpdates(section.events, Number(section.typeId));
    }
  }

  /**
   * Ububscribve from sEVENT, sEVMKT, sSELCN updates of all events in given section by given section key.
   * @param {ITypeSegment} section
   */
  private unsubscribeFromSectionUpdates(section: ITypeSegment): void {
    if (section && section.subscriptionKey) {
      this.sport.unsubscribeEventChildsUpdates(section.subscriptionKey);
      section.subscriptionKey = null;
    }
  }

  /**
   * Ububscribve from sEVENT, sEVMKT, sSELCN updates of all events in all sections.
   */
  private unsubscribeFromLiveUpdates(): void {
    // unSubscribe LS Updates via WS
    _.each(this.eventsBySections, (section: ITypeSegment) => {
      this.unsubscribeFromSectionUpdates(section);
    });
  }
}
