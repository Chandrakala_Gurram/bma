import { of as observableOf, throwError, Subject, of } from 'rxjs';
import { SportMainComponent } from '@sb/components/sportMain/sport-main.component';
import { NavigationEnd } from '@angular/router';

describe('SportMainComponent', () => {
  let component: SportMainComponent;
  let getRouteSegmentCase = 1;
  let cmsService;
  let timeService;
  let getSportInstanceService;
  let routingState;
  let pubSubService;
  let location;
  let storage;
  let user;
  let router;
  let route;
  let device;
  let sportTabsService;
  let sportConfig;
  let sportConfigContainer;
  let routeSnapshot;
  let coreToolsService;
  let slpSpinnerStateService;
  let routeChangeListener;
  let unsavedEmaHandler;
  let navigationService;

  beforeEach(() => {
    sportConfig = {
      config: {
        defaultTab: 'matches',
        request: {
          categoryId: '15'
        },
        tier: 1,
        name: 'football',
      },
      filters: {
        VIEW_BY_FILTERS: {},
        LIVE_VIEW_BY_FILTERS: {}
      },
      order: {
        BY_LEAGUE_ORDER: {},
        BY_LEAGUE_EVENTS_ORDER: {},
        BY_TIME_ORDER: {}
      },
      tabs: [{
        id: 'tab-live',
        label: 'sb.tabsNameInPlay',
        url: '/sport/football/live',
        hidden: true,
        name: 'live',
        displayInConnect: true
      }, {
        id: 'tab-matches',
        label: 'sb.tabsNameInPlay',
        url: '/sport/football/matches',
        hidden: false,
        name: 'matches',
        displayInConnect: true
      }, {
        id: 'tab-competitions',
        label: 'sb.tabsNameCompetitions',
        url: '/sport/football/competitions',
        hidden: false,
        name: 'competitions',
        displayInConnect: true
      }]
    };

    sportConfigContainer = {
      sportConfig: sportConfig,
      getConfig: jasmine.createSpy('getConfig'),
      config: {
        tier: 1
      }
    };

    cmsService = {
      getLeagues: jasmine.createSpy('getLeagues').and.returnValue({
        pipe: jasmine.createSpy('pipe').and.returnValue(observableOf({}))
      }),
      getToggleStatus: jasmine.createSpy('getToggleStatus').and.returnValue(of(true))
    } as any;
    timeService = {} as any;
    getSportInstanceService = {
      getSport: jasmine.createSpy('getSport').and.returnValue(observableOf(sportConfigContainer))
    } as any;
    routingState = {
      getRouteParam: jasmine.createSpy('getRouteParam'),
      getRouteSegment: jasmine.createSpy('getRouteSegment').and.callFake(() => {
        switch (getRouteSegmentCase) {
          case 1:
            return 'sport';
          case 2:
            return 'olympicsSport';
          case 3:
            return 'edp';
        }
      }),
    } as any;
    pubSubService = {
      subscribe: jasmine.createSpy('subscribe').and.callFake((subscriber, method, handler) => {
        if (method === 'EMA_UNSAVED_ON_EDP') {
          unsavedEmaHandler = handler;
        }
      }),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      publish: jasmine.createSpy('publish'),
      API: {
        SPORT_DEFAULT_PAGE: 'SPORT_DEFAULT_PAGE',
        EMA_OPEN_CANCEL_DIALOG: 'EMA_OPEN_CANCEL_DIALOG',
        EMA_UNSAVED_ON_EDP: 'EMA_UNSAVED_ON_EDP',
        SESSION_LOGIN: 'SESSION_LOGIN'
      }
    } as any;
    location = {
      path: jasmine.createSpy('path').and.returnValue('https://test.url')
    } as any;
    storage = {
      remove: jasmine.createSpy('remove'),
      get: jasmine.createSpy('get'),
      set: jasmine.createSpy('set')
    } as any;
    user = {} as any;
    router = {
      events: {
        subscribe: jasmine.createSpy('subscribe').and.callFake((cb) => {
          routeChangeListener = cb;
        })
      },
      navigate: jasmine.createSpy('navigate'),
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    } as any;
    routeSnapshot = {
      data: {},
      url: [{ path: 'sport' }],
      paramMap: {
        get: jasmine.createSpy('get').and.returnValue('football')
      },
      params: {
        sport: 'football'
      }
    } as any;
    route = {
      params: observableOf({
        id: '1',
        sport: 'tennis'
      }),
      snapshot: routeSnapshot
    } as any;
    sportTabsService = {
      storeSportTabs: jasmine.createSpy('storeSportTabs')
    } as any;
    device = {
      isDesktop: false
    } as any;

    coreToolsService = {
      deepClone: jasmine.createSpy('deepClone').and.returnValue(sportConfig.tabs)
    };

    slpSpinnerStateService = {
      clearSpinnerState: jasmine.createSpy('clearSpinnerState'),
      createSpinnerStream: jasmine.createSpy('createSpinnerStream')
    };
    navigationService = jasmine.createSpyObj('navigationService', ['handleHomeRedirect']);

    component = new SportMainComponent(cmsService, timeService,
      getSportInstanceService, routingState, pubSubService, location, storage,
      user, router, route, device, sportTabsService, coreToolsService, slpSpinnerStateService,
      navigationService);

    component.goToDefaultPage = jasmine.createSpy().and.callFake(cb => {
      cb();
    });

    component['slpSpinnerStateService'].slpSpinnerStateObservable$ = new Subject();
  });

  it('constructor', () => {
    expect(component).toBeDefined();
  });

  it('should redirect if incorrect event id', () => {
    route.params = observableOf({
      id: '/test',
      sport: 'tennis'
    });
    component.ngOnInit();
    expect(navigationService.handleHomeRedirect).toHaveBeenCalledWith('slp');
  });

  describe('applySportConfiguration', () => {
    it('check sport name for aem baenners when applying sport configuration', () => {
      component.sportBanner = 'prevName';

      component['applySportConfiguration'](sportConfigContainer);

      expect(component.sportBanner).toEqual('football');
    });

    it('check sport name for aem baenners when applying sport configuration', () => {
      component.sportBanner = 'prevName';
      sportConfigContainer.sportConfig.config.name = null;

      component['applySportConfiguration'](sportConfigContainer);

      expect(component.sportBanner).toEqual('prevName');
    });

    it('should call filterTabs', () => {
      component['filterTabs'] = jasmine.createSpy('filterTabs');

      component['applySportConfiguration'](sportConfigContainer);

      expect(component['filterTabs']).toHaveBeenCalledWith(sportConfigContainer.sportConfig.tabs);
    });

    it('should call filterTabs', () => {
      sportConfigContainer.sportConfig.config.defaultTab = 'test';
      component['filterTabs'] = jasmine.createSpy('filterTabs');
      component['applySportConfiguration'](sportConfigContainer);
      component['goToDefaultPage']();
      expect(pubSubService.publish).toHaveBeenCalledWith('SPORT_DEFAULT_PAGE');
    });
  });


  describe('#getIsEnhancedMultiplesEnabled', () => {
    it('isEnhancedMultiplesEnabled true', () => {
      component['getIsEnhancedMultiplesEnabled']().subscribe((value) => {
        expect(value).toBeTruthy();
      });
      expect(cmsService.getToggleStatus).toHaveBeenCalledWith('EnhancedMultiples');
    });

    it('isEnhancedMultiplesEnabled false', () => {
      cmsService.getToggleStatus = jasmine.createSpy().and.returnValue(of(false));
      component['getIsEnhancedMultiplesEnabled']().subscribe((value) => {
        expect(value).toBeFalsy();
      });
      expect(cmsService.getToggleStatus).toHaveBeenCalledWith('EnhancedMultiples');
    });
  });

  describe('#selectTabSport', () => {
    beforeEach(() => {
      component.sportTabs = [{
        id: 'tab-matches',
        label: 'new matches',
        url: '/sport/football/matches',
        name: 'matches',
        displayInConnect: true,
        enabled: true,
        index: 0
      }];
    });
    it('should call selectTabSport method for default tab', () => {
      pubSubService.subscribe.and.callFake((file, methods, callback) => {
        callback();
      });
      component['defaultTab'] = 'matches';
      component['isDefaultUrl'] = jasmine.createSpy('isDefaultUrl').and.returnValue(true);
      component['setSportTab'] = jasmine.createSpy('setSportTab');
      component['selectTabSport']();

      expect(component['setSportTab']).toHaveBeenCalledWith('matches');
    });

    it('should call selectTabSport method for dispay', () => {
      pubSubService.subscribe.and.callFake((file, methods, callback) => {
        callback();
      });
      component['isDefaultUrl'] = jasmine.createSpy('isDefaultUrl').and.returnValue(false);
      component['setSportTab'] = jasmine.createSpy('setSportTab');
      routingState.getRouteParam.and.returnValue('matches');
      component['selectTabSport']();

      expect(component['setSportTab']).toHaveBeenCalledWith('matches');
    });

    it('should call selectTabSport method when no display and default tab', () => {
      pubSubService.subscribe.and.callFake((file, methods, callback) => {
        callback();
      });

      routingState.getRouteParam.and.returnValue(null);
      component['isDefaultUrl'] = jasmine.createSpy('isDefaultUrl').and.returnValue(false);
      component['setSportTab'] = jasmine.createSpy('setSportTab');

      component['selectTabSport']();

      expect(component['setSportTab']).not.toHaveBeenCalled();
    });

    it('should call selectTabSport method when tab is not active', () => {
      component['processUrl'] = jasmine.createSpy('processUrl');
      routingState.getRouteParam.and.returnValue('competitions');
      component['selectTabSport']();

      expect(component['processUrl']).not.toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith(['matches'], { relativeTo: jasmine.any(Object) });
    });

    it('should call selectTabSport method when tab is active', () => {
      component['processUrl'] = jasmine.createSpy('processUrl');
      routingState.getRouteParam.and.returnValue('matches');
      component['selectTabSport']();

      expect(component['processUrl']).toHaveBeenCalledWith('matches');
    });
  });

  describe('@processUrl', () => {
    it('should navigate if tab is exist', () => {
      routingState.getRouteSegment.and.returnValue('sport');
      component['processUrl']('matches');

      expect(component.sportActiveTab).toEqual({ id: 'tab-matches' });
      expect(router.navigate).toHaveBeenCalledWith(['matches'], { relativeTo: route });
    });

    it('should NOT navigate if tab is NOT exist', () => {
      routingState.getRouteSegment.and.returnValue('sport');
      component['defaultTab'] = undefined;
      component['processUrl']('matches');

      expect(component.sportActiveTab).toEqual(undefined);
      expect(router.navigate).not.toHaveBeenCalled();
    });
  });

  describe('isHomeUrl', () => {
    it('should return true for sport segment', () => {
      getRouteSegmentCase = 1;
      const isHomeUrl = component.isHomeUrl();
      expect(routingState.getRouteSegment).toHaveBeenCalledWith('segment', routeSnapshot);
      expect(isHomeUrl).toEqual(true);
    });
    it('should return true for olympic sport segment', () => {
      getRouteSegmentCase = 2;
      const isHomeUrl = component.isHomeUrl();
      expect(routingState.getRouteSegment).toHaveBeenCalledWith('segment', routeSnapshot);
      expect(isHomeUrl).toEqual(true);
    });
    it('should return true for not sport and olympic sport segment', () => {
      getRouteSegmentCase = 3;
      const isHomeUrl = component.isHomeUrl();
      expect(routingState.getRouteSegment).toHaveBeenCalledWith('segment', routeSnapshot);
      expect(isHomeUrl).toEqual(false);
    });
  });

  describe('shouldNavigatedToTab', () => {
    it('should return true for home page and not vertical', () => {
      getRouteSegmentCase = 1;
      expect(component['shouldNavigatedToTab']()).toEqual(true);
    });
  });

  describe('#filterTabs', () => {
    it('should filter tabs when matches available', () => {
      const result = component['filterTabs']([{
        id: 'tab-matches',
        label: 'matches',
        url: '/sport/football/matches',
        hidden: false,
        name: 'matches',
        displayInConnect: true
      }]);

      expect(result).toEqual([{
        id: 'tab-matches',
        label: 'matches',
        hidden: false,
        url: '/sport/football/matches',
        name: 'matches',
        displayInConnect: true
      }]);
      expect(component['defaultTab']).toEqual('matches');
    });

    it('should filter tabs when matches is not available', () => {
      const result = component['filterTabs']([{
        id: 'tab-competitions',
        label: 'competitions',
        url: '/sport/football/competitions',
        name: 'competitions',
        displayInConnect: true
      }]);

      expect(result).toEqual([{
        id: 'tab-competitions',
        label: 'competitions',
        url: '/sport/football/competitions',
        name: 'competitions',
        displayInConnect: true
      }]);
      expect(component['defaultTab']).toEqual('competitions');
    });

    it('should filter tabs when no tabs from cms', () => {
      component['filterTabs']([]);

      expect(navigationService.handleHomeRedirect).toHaveBeenCalledWith('slp');
      expect(component['defaultTab']).not.toBeDefined();
    });

    it('should add live tab to tabs for desktop', () => {
      device.isDesktop = true;

      const result = component['filterTabs']([{
          id: 'tab-matches',
          label: 'sb.tabsNameMatches',
          url: '/sport/football/matches',
          name: 'matches',
          displayInConnect: true
        },
        {
          id: 'tab-live',
          label: 'sb.tabsNameInPlay',
          url: '/sport/football/live',
          name: 'live'
        }]);

      expect(result.length).toEqual(2);
    });
  });

  describe('redirect to /', () => {
    it('should redirect to / if incorrect sport name', () => {
      getSportInstanceService.getSport.and.returnValue(throwError(''));
      component.ngOnInit();
      expect(navigationService.handleHomeRedirect).toHaveBeenCalledWith('slp');
    });

    it('should redirect to / if incorrect sport name', () => {
      getSportInstanceService.getSport.and.returnValue(observableOf({}));
      component.ngOnInit();
      expect(navigationService.handleHomeRedirect).toHaveBeenCalledWith('slp');
    });
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      route.params = new Subject();
    });

    it('should not reload component if sport name and event id is the same as stored sport name', () => {
      const sportName = 'tennis';
      const eventId = '123';

      routingState.getRouteParam.and.callFake(key => key === 'sport' ? sportName : eventId);
      component.ngOnInit();

      route.params.next({ sport: sportName, id: eventId });

      expect(getSportInstanceService.getSport).toHaveBeenCalledTimes(1);
    });

    it('should not reload component if sport name is the different as stored sport name', () => {
      const sportName = 'tennis';
      const eventId = '123';

      routingState.getRouteParam.and.callFake(key => key === 'sport' ? 'football' : eventId);
      component.ngOnInit();

      route.params.next({ sport: sportName, id: eventId });

      expect(getSportInstanceService.getSport).toHaveBeenCalledTimes(1);
    });

    it('should not reload component if event id is the different as stored event id', () => {
      const sportName = 'tennis';
      const eventId = '123';

      routingState.getRouteParam.and.callFake(key => key === 'sport' ? sportName : '124');
      component.ngOnInit();

      route.params.next({ sport: sportName, id: eventId });

      expect(getSportInstanceService.getSport).toHaveBeenCalledTimes(1);
    });

    it('should not reload component if event id is not present', () => {
      const sportName = 'tennis';

      routingState.getRouteParam.and.callFake(key => key === 'sport' ? sportName : 'tennis');

      component = new SportMainComponent(cmsService, timeService,
        getSportInstanceService, routingState, pubSubService, location, storage,
        user, router, route, device, sportTabsService, coreToolsService, slpSpinnerStateService,
        navigationService);

      component.ngOnInit();

      route.params.next({ sport: sportName, id: null });

      expect(getSportInstanceService.getSport).toHaveBeenCalledTimes(1);
    });

    it('should handle EMA_UNSAVED_ON_EDP event', () => {
      component.ngOnInit();
      expect(pubSubService.subscribe).toHaveBeenCalledWith('sportMain', pubSubService.API.EMA_UNSAVED_ON_EDP, jasmine.any(Function));
    });

    it('routeChangeListener', () => {
      component.ngOnInit();
      routeChangeListener(new NavigationEnd(0, 'test', 'test'));
      expect(routingState.getRouteParam).toHaveBeenCalledWith('display', component['route'].snapshot);
      expect(routingState.getRouteParam).toHaveBeenCalledWith('display', component['route'].snapshot);
    });

    it('routeChangeListener sportPath', () => {
      component.ngOnInit();
      routingState.getRouteParam.and.returnValue('test');
      component['sportPath'] = 'test';
      component['processUrl'] = jasmine.createSpy('processUrl');
      routeChangeListener(new NavigationEnd(0, 'test', 'test'));
      expect(component['processUrl']).toHaveBeenCalled();
    });

    it('routeChangeListener !NavigationEnd', () => {
      component.ngOnInit();
      routeChangeListener({});
      expect(routingState.getRouteParam).not.toHaveBeenCalledTimes(4);
    });

    it('unsavedEmaHandler', () => {
      component.ngOnInit();
      unsavedEmaHandler(true);
      expect(component['editMyAccaUnsavedOnEdp']).toEqual(true);
    });
  });

  describe('#ngOnDestroy', () => {
    it('should ngOnDestroy', () => {
      component.ngOnDestroy();
      expect(pubSubService.unsubscribe).toHaveBeenCalledWith('sportMain');
      expect(slpSpinnerStateService.clearSpinnerState).toHaveBeenCalled();
    });

    it('should ngOnDestroy and usubscribe', () => {
      component['routeParamsListener'] = {
        unsubscribe: jasmine.createSpy('unsubscribe')
      } as any;
      component['routeChangeListener'] = {
        unsubscribe: jasmine.createSpy('unsubscribe')
      } as any;
      component['sportsConfigSubscription'] = {
        unsubscribe: jasmine.createSpy('unsubscribe')
      } as any;

      component.ngOnDestroy();

      expect(component['routeParamsListener'].unsubscribe).toHaveBeenCalled();
      expect(component['routeChangeListener'].unsubscribe).toHaveBeenCalled();
      expect(component['sportsConfigSubscription'].unsubscribe).toHaveBeenCalled();
      expect(pubSubService.unsubscribe).toHaveBeenCalledWith('sportMain');
      expect(slpSpinnerStateService.clearSpinnerState).toHaveBeenCalled();
    });
  });

  describe('#getSportUri', () => {
    it('should return sport uri for mobile', () => {
      const sportUrl = 'sport/';
      const actualResult = component['getSportUri'](sportUrl);

      expect(actualResult).toEqual(`${sportUrl}football`);
    });

    it('should return sport uri for desktop', () => {
      device.isDesktop = true;
      const sportUrl = 'sport/';
      const actualResult = component['getSportUri'](sportUrl);

      expect(actualResult).toEqual(`${sportUrl}football/matches/today`);
    });
  });

  describe('#checkTabs', () => {
    it('should redirect to home page, when no tabs', () => {
      component['checkTabs']([]);
      expect(navigationService.handleHomeRedirect).toHaveBeenCalledWith('slp');
    });
    it('should redirect to home page, when all tabs are hidden', () => {
      component['checkTabs']([{ label: 'matches', hidden: true }, { label: 'competitions', hidden: true }]);
      expect(navigationService.handleHomeRedirect).toHaveBeenCalledWith('slp');
    });
  });

  describe('loadSportData', () => {
    beforeEach(() => {
      component['initModel'] = jasmine.createSpy('initModel');
      component['applySportConfiguration'] = jasmine.createSpy('applySportConfiguration');
      component['selectTabSport'] = jasmine.createSpy('selectTabSport');
      component['hideSpinner'] = jasmine.createSpy('hideSpinner');
      component['showSpinner'] = jasmine.createSpy('showSpinner');
    });

    it('should get sport config instance', () => {
      component['loadSportData']();

      expect(getSportInstanceService.getSport).toHaveBeenCalledWith('football', false);
      expect(component['initModel']).toHaveBeenCalled();
      expect(component['applySportConfiguration']).toHaveBeenCalledWith(sportConfigContainer);
      expect(component['selectTabSport']).toHaveBeenCalled();
      expect(component.hideSpinner).toHaveBeenCalled();
      expect(component.showSpinner).not.toHaveBeenCalled();
    });

    it('should redirect to / if incorrect sport name', () => {
      getSportInstanceService.getSport.and.returnValue(throwError(''));
      component['loadSportData']();
      expect(navigationService.handleHomeRedirect).toHaveBeenCalledWith('slp');
    });

    it('should redirect to / if incorrect sport name', () => {
      getSportInstanceService.getSport.and.returnValue(observableOf({}));
      component['loadSportData']();
      expect(navigationService.handleHomeRedirect).toHaveBeenCalledWith('slp');
    });
  });

  describe('initLazyHandler', () => {
    it(`should set True for isLazyComponentLoaded`, () => {
      component.isLazyComponentLoaded = false;

      component.initLazyHandler();
      expect(component.isLazyComponentLoaded).toBeTruthy();
    });
  });

  describe('canChangeRoute', () => {
    it('should not when editMyAccaUnsavedOnEdp is true', () => {
      component['editMyAccaUnsavedOnEdp'] = false;
      expect(component.canChangeRoute()).toEqual(true);
    });

    it('should when editMyAccaUnsavedOnEdp is false', () => {
      component['editMyAccaUnsavedOnEdp'] = true;
      expect(component.canChangeRoute()).toEqual(false);
    });
  });

  it('onChangeRoute', () => {
    component.onChangeRoute();
    expect(pubSubService.publish).toHaveBeenCalledWith(pubSubService.API.EMA_OPEN_CANCEL_DIALOG);
  });

  describe('setSportTab', () => {
    it('setSportTab', () => {
      user.status = true;
      user.username = 'qa';
      component['shouldSaveTab'] = true;
      component['baseUrl'] = 'test';
      component['setSportTab']('tab1');
      expect(storage.set).toHaveBeenCalledWith('test-tab-qa', 'tab1');
    });

    it('setSportTab', () => {
      user.username = 'qa';
      component['setSportTab']('tab1');
      expect(storage.set).not.toHaveBeenCalled();
    });
  });

  it('processUrl', () => {
    const event = <any>{
      url: '/sport'
    };

    device.isDesktop = true;
    component['sportName'] = 'football';
    component['defaultTab'] = 'test';
    component['processUrl'](null, event);
    expect(router.navigate).toHaveBeenCalledWith([component['defaultTab']], jasmine.any(Object));
  });

  it('processUrl (!shouldNavigatedToTab)', () => {
    component['sportName'] = 'football';
    component['defaultTab'] = 'test';
    getRouteSegmentCase = 3;
    component['processUrl']('test');
    expect(component['sportActiveTab']).toEqual({ id: 'tab-test' });
  });

  it('getSportTab', () => {
    storage.get.and.returnValue('tab2');
    component['shouldSaveTab'] = true;
    user.status = true;
    user.username = 'qa';
    component['baseUrl'] = 'test';
    component.sportTabs = <any>[
      {
        name: 'tab1',
        hidden: true
      },
      {
        name: 'tab2',
        hidden: false
      }
    ];

    expect(component['getSportTab']()).toEqual('tab2');
  });
});
