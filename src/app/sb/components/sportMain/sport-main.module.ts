// (no proper coverage - no file movement) TODO re-folder this module

import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from '@sharedModule/shared.module';
import { BannersModule } from '@banners/banners.module';
import { SportMainComponent } from './sport-main.component';

@NgModule({
  imports: [
    SharedModule,
    BannersModule
  ],
  declarations: [
    SportMainComponent
  ],
  entryComponents: [
    SportMainComponent
  ],
  providers: [],
  exports: [
    SportMainComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SportMainModule { }
