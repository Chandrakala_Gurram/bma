import { switchMap } from 'rxjs/operators';
import { Router, Event, NavigationEnd, ActivatedRoute } from '@angular/router';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { Subscription, from } from 'rxjs';
import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ChangeDetectorRef,
  AfterViewInit,
} from '@angular/core';
import * as _ from 'underscore';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { DeviceService } from '@core/services/device/device.service';
import { CouponsDetailsService } from '@sb/components/couponsDetails/coupons-details.service';
import { MarketSortService } from '@sb/services/marketSort/market-sort.service';
import { DomToolsService } from '@coreModule/services/domTools/dom.tools.service';
import { UpdateEventService } from '@core/services/updateEvent/update-event.service';

import { COUPON_NAMES_CONFIG } from '@sb/components/couponsDetails/coupons-details.constant';

import { ISportEvent } from '@core/models/sport-event.model';
import { ICouponMarketSelector } from '@shared/components/marketSelector/market-selector.model';
import { ITypeSegment, IGroupedByDateItem } from '@app/inPlay/models/type-segment.model';
import { ICoupon } from '@core/models/coupon.model';
import { CacheEventsService } from '@core/services/cacheEvents/cache-events.service';
import { SportsConfigService } from '@sb/services/sportsConfig/sports-config.service';
import { GamingService } from '@app/core/services/sport/gaming.service';
import { RoutingState } from '@shared/services/routingState/routing-state.service';
import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { ILazyComponentOutput } from '@shared/components/lazy-component/lazy-component.model';

@Component({
  selector: 'coupons-details',
  templateUrl: './coupons-details.component.html'
})
export class CouponsDetailsComponent extends AbstractOutletComponent implements OnInit, OnDestroy, AfterViewInit {
  couponsList: ICoupon[];
  marketOptions: ICouponMarketSelector[] = [];
  couponNamesConfig;
  couponEvents = [];
  savedCouponEvents = [];
  applyingParams = true;
  applyingList = true;
  isEmptyEvents = false;
  isExpanded = [true, true, true];
  showCoupons = false;
  couponName: string;
  isBetFilterEnable: boolean = false;
  isEventsUnavailable: boolean = false;
  oddsHeader: string[];
  couponId: string;
  marketFilter: string = '';
  footballService: GamingService;
  eventIdFromEDP: number;
  pageRenderTime: number = 1000;

  private detectListener: number;
  private routeChangeSuccessHandler: Subscription;
  private couponsDetailsSubscription: Subscription;
  private couponsListSubscription: Subscription;
  private sportsConfigSubscription: Subscription;
  private readonly COUPON_NAMES_CONFIG = COUPON_NAMES_CONFIG;
 constructor(
    private activatedRoute: ActivatedRoute,
    private sportConfigService: SportsConfigService,
    private deviceService: DeviceService,
    private pubSubService: PubSubService,
    private marketSortService: MarketSortService,
    protected elementRef: ElementRef,
    private router: Router,
    protected domToolsService: DomToolsService,
    protected windowRefService: WindowRefService,
    private changeDetectorRef: ChangeDetectorRef,
    private couponsDetailsService: CouponsDetailsService,
    private cacheEventsService: CacheEventsService,
    private routingState: RoutingState,
    // tslint:disable-next-line
    private updateEventService: UpdateEventService // DO NOT REMOVE: needed for events subscription on init of this dependency
  ) {
    super()/* istanbul ignore next */;
    this.couponNamesConfig = this.COUPON_NAMES_CONFIG;
  }

  ngOnInit() {
    this.changeDetectorRef.detach();
    this.detectListener = this.windowRefService.nativeWindow.setInterval(() => {
      this.changeDetectorRef.detectChanges();
    }, 500);

    const couponName: string = this.activatedRoute.snapshot.paramMap.get('couponName');
    this.couponId = this.activatedRoute.snapshot.paramMap.get('couponId');

    this.sportsConfigSubscription = this.sportConfigService.getSport('football').subscribe((sportService: GamingService) => {
      this.footballService = sportService;
      this.footballService.extendRequestConfig('coupons');
      this.setCouponsData(couponName);
    });

    if (this.deviceService.isTablet || this.deviceService.isTabletLandscape) {
      this.setTabletTopTitleWidth();

      this.pubSubService.subscribe(
        'CouponsDetailsCtrl',
        this.pubSubService.API.SHOW_HIDE_WIDGETS,
        () => this.setTabletTopTitleWidth()
      );
    }

    this.routeChangeSuccessHandler = this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        const currentCouponId: string = this.activatedRoute.snapshot.paramMap.get('couponId');
        const currentCouponName: string = this.activatedRoute.snapshot.paramMap.get('couponName');
        const isRouteSegment: boolean = this.activatedRoute.snapshot.data['segment'] === 'couponsDetails';

        if (this.couponId !== currentCouponId && isRouteSegment) {
          this.marketFilter = '';
          this.couponId = currentCouponId;
          this.applyingParams = true;
          this.setCouponsData(currentCouponName);
        }
      }
    });

    // Block quick bet on coupon details page if configured in CMS
    this.couponsDetailsService.isQuickBetBlocked().subscribe((isQuickBetBlocked: boolean) => {
      if (!isQuickBetBlocked) { return; }

      this.pubSubService.subscribe('CouponsDetailsCtrl', this.pubSubService.API.BETSLIP_LOADED, () => {
        this.blockQuickBet();
      });
      this.blockQuickBet();
    });

    this.pubSubService.subscribe('CouponsDetailsCtrl', this.pubSubService.API.DELETE_EVENT_FROM_CACHE, eventId => {
      this.deleteEvent(eventId);
    });
  }

  ngAfterViewInit(): void {
    this.changeDetectorRef.detectChanges();
  }

  ngOnDestroy() {
    this.cacheEventsService.clearByName('coupons');
    this.windowRefService.nativeWindow.clearInterval(this.detectListener);
    // unSubscribe LS Updates via liveServe PUSH updates (iFrame)!
    this.footballService.unSubscribeCouponsForUpdates('football-coupons');
    this.pubSubService.unsubscribe('CouponsDetailsCtrl');
    this.routeChangeSuccessHandler && this.routeChangeSuccessHandler.unsubscribe();
    this.couponsDetailsSubscription && this.couponsDetailsSubscription.unsubscribe();
    this.couponsListSubscription && this.couponsListSubscription.unsubscribe();
    this.sportsConfigSubscription && this.sportsConfigSubscription.unsubscribe();
    this.blockQuickBet(false);
  }

  trackById(index: number, element: ISportEvent): string {
    return element.id ? `${index}${element.id}` : index.toString();
  }

  trackByTypeId(index: number, element: ITypeSegment): string {
    return `${index}${element.typeId}${element.deactivated}`;
  }

  changeAccordionState(i: number, value: boolean): void {
    this.isExpanded[i] = value;
  }

  /**
   * filterEvents()
   * @param {string} marketFilter
   */
  filterEvents(marketFilter: string): void {
    this.marketFilter = marketFilter;
    this.marketSortService.setMarketFilterForMultipleSections(this.savedCouponEvents, marketFilter);
    this.isEventsUnavailable = this.getEventsIsUnavailable;
    this.oddsHeader = this.couponsDetailsService.setOddsHeader(this.marketOptions, marketFilter);

    // filter events and keep reference to the array
    this.couponEvents.length = 0;
    this.couponEvents.push(...this.filteredEvents());
  }
  /**
   * showCouponsList()
   */
  showCouponsList(): void {
    this.showCoupons = !this.showCoupons;
    this.scrollToTop();
  }

  /**
   * goToPage()
   * @param {string} path
   * @returns {void|boolean}
   */
  goToPage(path: string): Promise<boolean>| boolean {
    return path ? this.router.navigateByUrl(path) : false;
  }

  filteredEvents(): ITypeSegment[] {
    return this.savedCouponEvents.filter(event => !event.deactivated);
  }

  handleMatchesMarketSelectorEvent(event: ILazyComponentOutput): void {
    if (event.output === 'filterChange') {
      this.filterEvents(event.value);
    }
  }

  protected getStickyElementsHeight(): number {
    const couponsContainer = this.windowRefService.document.querySelector('.coupons-container-js');
    return !couponsContainer ? 0 : this.domToolsService.getOffset(couponsContainer).top;
  }

  private updatePreviousStateInfo(): void {
    const previousSegment = this.routingState.getPreviousSegment();
    if (previousSegment !== 'eventMain') {
      return;
    }
    const regexpResult = /\/([0-9]*)\//.exec(this.routingState.getPreviousUrl()),
      eventIdFromEDP = regexpResult && +regexpResult[1];
    if (!eventIdFromEDP) {
      return;
    }
    const allEvents = this.couponEvents.map(coupon => coupon.events)
      .reduce((firstArray, secondArray) => firstArray.concat(secondArray), []);
    const eventIndex = allEvents.findIndex(event => event.id === eventIdFromEDP);
    if (eventIndex === -1) {
      return;
    }
    const typeId = allEvents[eventIndex].typeId,
      typeIndex = this.couponEvents.findIndex(type => type.typeId === typeId);
    this.isExpanded[typeIndex] = true;
    this.eventIdFromEDP = eventIdFromEDP;
  }

  /**
   * If redirecting back from coupons EDP, scroll to previous position on coupons page
   */
  private scrollToPreviousState(): void {
    const document: HTMLDocument = this.windowRefService.document,
      eventElement = document.querySelector(`.coupon-event-${this.eventIdFromEDP}`);
    if (!eventElement) {
      return;
    }
    const eventElementOffset = this.domToolsService.getOffset(eventElement).top;
    setTimeout(() => {
      this.windowRefService.nativeWindow.scrollTo(0, eventElementOffset - this.getStickyElementsHeight());
    });
  }

  private setCouponsData(couponName: string): void {
    const couponTitle = this.getCouponName(couponName);
    this.marketOptions = [];
    this.couponsDetailsService.isGoalscorerCoupon = couponTitle.toLowerCase() ===
      this.couponNamesConfig.goalscorerCouponName.toLowerCase();
    this.footballService.unSubscribeCouponsForUpdates('football-coupons');
    this.getCouponsData(couponTitle);
    this.getCouponsListData();
  }

  /**
   * eventsIsUnavailable()
   * @returns {boolean}
   */
  private get getEventsIsUnavailable(): boolean {
    if (this.isEmptyEvents) {
      return true;
    }

    return this.couponsDetailsService.isCustomCoupon ? false : !_.some(this.couponEvents, (couponEvent: ITypeSegment) => {
      return _.some(couponEvent.groupedByDate, (group: IGroupedByDateItem) => {
        return group.deactivated === false || group.deactivated === undefined;
      });
    });
  }

  /**
   * getCouponsListData()
   */
  private getCouponsListData(): void {
    this.couponsListSubscription = from(this.footballService.coupons())
      .pipe(switchMap((data: ICoupon[]) => {
        this.applyingList = false;
        this.couponsList = data;
        const currentCoupon = this.couponsList.find((coupon: ICoupon) => coupon.id === this.couponId);
        this.couponName = this.getCouponName(currentCoupon.name);
        return this.couponsDetailsService.isBetFilterEnable(currentCoupon);
      })).subscribe((isEnable: boolean) => {
      this.isBetFilterEnable = isEnable;
      this.hideSpinner();
    }, (error) => {
        this.applyingList = false;
        this.isBetFilterEnable = false;
        this.couponsList = [];
        console.warn('Error:', error);
        this.showError();
    });
  }

  /**
   * getCouponsData()
   * @param {number} couponId
   * @param {string} couponName
   */
  private getCouponsData(couponName: string): void {
    this.couponsDetailsSubscription = this.couponsDetailsService.getCouponEvents(this.couponId, couponName, this.footballService)
    .subscribe(({ coupons, options }) => {
      this.loadCouponEvents(coupons, options);
      }, ({ coupons, options }) => {
      if (coupons && options) {
        this.loadCouponEvents(coupons, options);
      } else  {
        this.showError();
      }
    });
  }

  /**
   * Load Coupons Events
   * @param {ISportEvent[]} coupons
   * @param {ICouponMarketSelector[]} options
   */
  private loadCouponEvents(coupons: ISportEvent[], options: ICouponMarketSelector[]): void {
    this.savedCouponEvents = this.couponsDetailsService.groupCouponEvents(coupons, this.footballService);
    this.couponEvents = this.savedCouponEvents.slice();
    this.isEmptyEvents = _.isEmpty(this.couponEvents);
    this.isEventsUnavailable = this.getEventsIsUnavailable;
    this.applyingParams = false;

    if (!this.isEmptyEvents) {
      this.footballService.subscribeCouponsForUpdates(coupons, 'football-coupons');
      this.marketOptions = options;
    }
    this.updatePreviousStateInfo();
    if (this.eventIdFromEDP) {
      this.windowRefService.nativeWindow.setTimeout(() => {
        this.scrollToPreviousState();
      }, this.pageRenderTime);
    }
  }

  private scrollToTop(): void {
    const couponsListElem = this.elementRef.nativeElement.querySelector('.coupons-list');
    if (this.showCoupons && couponsListElem) {
      this.windowRefService.document.body.scrollTop = 0; // For Safari
      this.windowRefService.document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      couponsListElem.scrollTop = 0; // Element scrollTop
    }
  }

  /**
   * setTabletTopTitleWidth()
   */
  private setTabletTopTitleWidth(): void {
    const couponsTopTitle = this.elementRef.nativeElement.querySelector('.coupons-top-title');
    if (!couponsTopTitle) {
      return;
    }

    const pageContent = this.windowRefService.document.getElementById('content');

    const elmWidth = this.domToolsService.getWidth(pageContent);

    this.domToolsService.css(couponsTopTitle, 'width', `${elmWidth}px`);
    this.domToolsService.css(couponsTopTitle, 'transition', 'initial');
  }

  /**
   * deleteEvent()
   * @param {number} eventId
   */
  private deleteEvent(eventId: number): void {
    this.savedCouponEvents.forEach((section: ITypeSegment, i: number) => {
      if (section) {
        this.deleteIndex(section, eventId, i);
        section.groupedByDate.forEach((groupedSection: IGroupedByDateItem, j: number) => {
          this.deleteIndex(groupedSection, eventId, j);
        });
      }
    });
  }

  private deleteIndex(section: ITypeSegment | IGroupedByDateItem, eventId: number, index: number): void {
    const eventIndex = section.events.findIndex((event: ISportEvent) => Number(event.id) === Number(eventId));
    if (eventIndex !== -1) {
      section.events.splice(eventIndex, 1);
      if (!section.events.length) {
        section.events.splice(index, 1);
      }
    }
  }

  /**
   * getCouponName()
   * @param {string} couponName
   * @returns {string}
   */
  private getCouponName(couponName: string): string {
    const couponTitle = couponName.replace(/-/g, ' ');
    const overUnderCoupon = this.couponNamesConfig.overUnderOriginalName.toLowerCase() === couponTitle.toLowerCase()
      && this.couponNamesConfig.overUnderOriginalName;
    const ukCoupon = this.couponNamesConfig.ukCoupon.toLowerCase() === couponTitle.toLowerCase()
      && this.couponNamesConfig.ukCoupon;

    return overUnderCoupon || ukCoupon || couponTitle;
  }

  private blockQuickBet(block: boolean = true): void {
    this.pubSubService.publish(this.pubSubService.API.BLOCK_QUICK_BET, block);
  }
}
