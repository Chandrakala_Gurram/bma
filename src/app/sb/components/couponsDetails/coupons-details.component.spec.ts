import { Subscription, Observable, of, throwError } from 'rxjs';
import { fakeAsync, tick, flush } from '@angular/core/testing';
import { CouponsDetailsComponent } from './coupons-details.component';
import { ITypeSegment } from '@app/inPlay/models/type-segment.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { NavigationEnd } from '@angular/router';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('CouponsDetailsComponent', () => {
  let component: CouponsDetailsComponent;
  let couponsDetailsService;
  let pubSubService;
  let deviceService;
  let marketSortService;
  let updateEventService;
  let footballService;
  let domToolsService;
  let router;
  let activatedRoute;
  let windowRefService;
  let changeDetectorRef;
  let cacheEventsService;
  let couponsList;
  let elementParamsMap;
  let elementRef;
  let getSportInstanceService;
  let routingState;

  const marketOptions = [
    {
      title: 'Match Result',
      templateMarketName: 'Match Betting',
      header: ['Home', 'Draw', 'Away'],
    },
    {
      title: 'Both Teams to Score',
      templateMarketName: 'Both Teams to Score',
      header: ['Yes', 'No'],
    }
  ];

  const couponEvents = [{
    typeId: 971,
    events: [{
      id: 5112681,
      typeId: 971
    }, {
      id: 3567838,
      typeId: 971
    }],
    groupedByDate: [{
      events: [{
        id: 5112681
      }, {
        id: 3567838
      }]
    }]
  }];

  beforeEach(() => {
    couponsDetailsService = {
      setOddsHeader: jasmine.createSpy('setOddsHeader'),
      isBetFilterEnable: jasmine.createSpy('isBetFilterEnable'),
      getCouponEvents: jasmine.createSpy('getCouponEvents'),
      groupCouponEvents: jasmine.createSpy('groupCouponEvents'),
      initMarketSelector: jasmine.createSpy('initMarketSelector'),
      isQuickBetBlocked: jasmine.createSpy('isQuickBetBlocked'),
      isCustomCoupon: false
    };

    updateEventService = {};

    pubSubService = {
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy(),
      publish: jasmine.createSpy(),
      API: pubSubApi
    };

    deviceService = {
      isTablet: true
    };

    cacheEventsService = {
      store: jasmine.createSpy('store'),
      clearByName: jasmine.createSpy('clearByName')
    };

    marketSortService = {
      setMarketFilterForMultipleSections : jasmine.createSpy(),
    };

    footballService = {
      unSubscribeCouponsForUpdates : jasmine.createSpy('unSubscribeCouponsForUpdates'),
      coupons: jasmine.createSpy('coupons'),
      extendRequestConfig: jasmine.createSpy('extendRequestConfig'),
      subscribeCouponsForUpdates: jasmine.createSpy('subscribeCouponsForUpdates')
    };
    elementRef = { nativeElement: { tagName: 'parent' } };
    elementParamsMap = {
      header: { bottom: 50 },
      parent: { top: 40 },
      topBar: { bottom: 70 }
    };

    domToolsService = {
      HeaderEl: { tagName: 'header' },
      getElementTopPosition: jasmine.createSpy('getElementTopPosition').and.callFake(
        el => elementParamsMap[el.tagName] ? elementParamsMap[el.tagName].top : 0
      ),
      getElementBottomPosition: jasmine.createSpy('getElementBottomPosition').and.callFake(
        el => elementParamsMap[el.tagName] ? elementParamsMap[el.tagName].bottom : 0
      ),
      getWidth: jasmine.createSpy().and.returnValue(50),
      css: jasmine.createSpy(),
      getOffset: jasmine.createSpy('getOffset').and.returnValue({ top: 120 })
    };

    router = {
      events: Observable.create((observer) => {
        const event = new NavigationEnd(559, 'coupons/football/coupon-1809-weekend/', 'coupons/football/coupon-1809-weekend/');
        setTimeout(() => {
          observer.next(event);
        }, 50);
      }),
      navigateByUrl : jasmine.createSpy()
    };

    activatedRoute = {
      snapshot: {
        data: {
          segment: 'couponsDetails'
        },
        paramMap: {
          get: jasmine.createSpy().and.callFake((() => {
            let i = 0;
            return () => `test${i++}`;
          })())
        }
      }
    };

    windowRefService = {
      nativeWindow: {
        clearInterval: jasmine.createSpy('clearInterval'),
        setInterval: jasmine.createSpy('setInterval').and.callFake((cb) => {
          cb();
          return 2;
        }),
        setTimeout: jasmine.createSpy('setTimeout').and.callFake((cb) => {
          cb();
        }),
        scrollTo: jasmine.createSpy('scrollTo')
      },
      document: {
        getElementById: jasmine.createSpy().and.returnValue({ clientHeight: 0 }),
        body: {
          scrollTop: 1
        },
        documentElement: {
          scrollTop: 1
        },
        querySelector: jasmine.createSpy('querySelector')
      }
    };

    elementRef = {
      nativeElement : {
        querySelector: jasmine.createSpy().and.returnValue({ clientHeight: 0, scrollTop: 1 })
      }
    };

    changeDetectorRef = {
      detach: jasmine.createSpy('detach'),
      detectChanges: jasmine.createSpy('detectChanges')
    };

    getSportInstanceService = {
      getSport: jasmine.createSpy('getSport').and.returnValue(of(footballService))
    };

    routingState = {
      getPreviousSegment: jasmine.createSpy('getPreviousSegment').and.returnValue(''),
      getPreviousUrl: jasmine.createSpy('getPreviousUrl').and.returnValue('')
    };

    component = new CouponsDetailsComponent(
      activatedRoute,
      getSportInstanceService,
      deviceService,
      pubSubService,
      marketSortService,
      elementRef,
      router,
      domToolsService,
      windowRefService,
      changeDetectorRef,
      couponsDetailsService,
      cacheEventsService,
      routingState,
      updateEventService
    );

    component['footballService'] = footballService;
  });

  it('should create a component', () => {
    expect(component).toBeTruthy();
  });

  describe('@ngOnInit', () => {
    beforeEach(() => {
      component['deleteEvent'] = jasmine.createSpy();
      component['setCouponsData'] = jasmine.createSpy();
      component['setTabletTopTitleWidth'] = jasmine.createSpy();
      couponsDetailsService.isQuickBetBlocked.and.returnValue(of(true));
    });

    it('should init component', fakeAsync(() => {
      component.marketFilter = 'Match Betting';
      component['pubSubService'].subscribe = jasmine.createSpy().and.callFake((sb, method, callback) => {
        if (method === pubSubService.API.BETSLIP_LOADED || method === pubSubService.API.SHOW_HIDE_WIDGETS) {
          callback();
        } else if (method === pubSubService.API.DELETE_EVENT_FROM_CACHE) {
          callback('123');
        }
      });
      component.ngOnInit();

      tick(100);

      expect(footballService.extendRequestConfig).toHaveBeenCalledWith('coupons');
      expect(couponsDetailsService.isQuickBetBlocked).toHaveBeenCalled();
      expect(component.marketFilter).toEqual('');
      expect(component['setTabletTopTitleWidth']).toHaveBeenCalledTimes(2);
      expect(component['deleteEvent']).toHaveBeenCalledWith('123');
      expect(pubSubService.subscribe).toHaveBeenCalled();
      expect(pubSubService.publish).toHaveBeenCalledWith(pubSubService.API.BLOCK_QUICK_BET, true);
      expect(pubSubService.subscribe.calls.allArgs()[2]).toEqual(
        ['CouponsDetailsCtrl', pubSubService.API.DELETE_EVENT_FROM_CACHE, jasmine.any(Function)]
      );
      tick();
      expect(getSportInstanceService.getSport).toHaveBeenCalledWith('football');
    }));

    it('should block quick-bet', fakeAsync(() => {
      couponsDetailsService.isQuickBetBlocked.and.returnValue(of(false));
      component.ngOnInit();
      tick(100);
      expect(pubSubService.subscribe).not.toHaveBeenCalledWith('CouponsDetailsCtrl', pubSubService.API.BETSLIP_LOADED);
      expect(pubSubService.publish).not.toHaveBeenCalledWith('CouponsDetailsCtrl', pubSubService.API.BLOCK_QUICK_BET, true);
    }));

    it('should block quick-bet', fakeAsync(() => {
      component.ngOnInit();
      tick(100);
      expect(pubSubService.subscribe).not.toHaveBeenCalledWith('CouponsDetailsCtrl', pubSubService.API.BETSLIP_LOADED);
      expect(pubSubService.publish).not.toHaveBeenCalledWith('CouponsDetailsCtrl', pubSubService.API.BLOCK_QUICK_BET, true);
    }));

    it('should not setTabletTopTitleWidth if device is not a tablet', () => {
      deviceService.isTablet = false;
      deviceService.isTabletLandscape = false;

      component.ngOnInit();

      expect(component['setTabletTopTitleWidth']).not.toHaveBeenCalledTimes(2);
      expect(pubSubService.subscribe)
        .not.toHaveBeenCalledWith('CouponsDetailsCtrl', pubSubService.API.SHOW_HIDE_WIDGETS, jasmine.any(Function));
    });

    it('should subscribe for router events', fakeAsync(() => {
      component.ngOnInit();

      tick(100);

      expect(activatedRoute.snapshot.paramMap.get).toHaveBeenCalledTimes(4);
      expect(activatedRoute.snapshot.paramMap.get).toHaveBeenCalledWith('couponId');
      expect(activatedRoute.snapshot.paramMap.get).toHaveBeenCalledWith('couponName');
      expect(component.marketFilter).toEqual('');
      expect(component.applyingParams).toBeTruthy();
      expect(component['setCouponsData']).toHaveBeenCalledTimes(2);
      expect(component['setCouponsData']).toHaveBeenCalledWith('test0');
      expect(component['setCouponsData']).toHaveBeenCalledWith('test3');
    }));

    it('should no set setCouponsData on router events', fakeAsync(() => {
      activatedRoute.snapshot.data['segment'] = undefined;
      component.ngOnInit();

      tick(100);

      expect(component['setCouponsData']).toHaveBeenCalledTimes(1);
      expect(component['setCouponsData']).toHaveBeenCalledWith('test0');
    }));

    it('should not proceed if router event is not of NavigationEnd type', fakeAsync(() => {
      router.events.subscribe = jasmine.createSpy('router.events.subscribe').and.returnValue(of({}));
      component.ngOnInit();

      tick(100);

      expect(activatedRoute.snapshot.paramMap.get).toHaveBeenCalledTimes(2);
      expect(component['setCouponsData']).toHaveBeenCalledTimes(1);
      expect(component['setCouponsData']).toHaveBeenCalledWith('test0');
    }));
  });

  describe('ngAfterViewInit', () => {
    it(`should detectChanges`, () => {
      component.ngAfterViewInit();

      expect(changeDetectorRef.detectChanges).toHaveBeenCalled();
    });
  });

  describe('@ngOnDestroy', () => {
    it('base flow', () => {
      const subscriptions = [
        'routeChangeSuccessHandler', 'couponsDetailsSubscription', 'couponsListSubscription', 'sportsConfigSubscription'
      ].map((subscription: string) => {
        component[subscription] = new Subscription();
        component[subscription].unsubscribe = jasmine.createSpy(`${ subscription }.unsubscribe`);

        return component[subscription];
      });

      component.ngOnDestroy();
      expect(cacheEventsService.clearByName).toHaveBeenCalledWith('coupons');
      expect(windowRefService.nativeWindow.clearInterval).toHaveBeenCalled();
      expect(footballService.unSubscribeCouponsForUpdates).toHaveBeenCalledWith('football-coupons');
      subscriptions.forEach((subscription: Subscription) => expect(subscription.unsubscribe).toHaveBeenCalled());
      expect(pubSubService.unsubscribe).toHaveBeenCalledWith('CouponsDetailsCtrl');
      expect(pubSubService.publish).toHaveBeenCalledWith(pubSubService.API.BLOCK_QUICK_BET, false);
    });
    it('if subscription is not defined', () => {
      component['routeChangeSuccessHandler'] = null;
      component['couponsDetailsSubscription'] = null;
      component['couponsListSubscription'] = null;
      expect(() => {
        component.ngOnDestroy();
      }).not.toThrow();
    });
  });

  describe('@trackById', () => {
    it('trackById should return a string', () => {
      const mockLeg = {
        id: 1
      } as ISportEvent;
      const result = component.trackById(1, mockLeg);

      expect(result).toBe('11');
    });

    it('trackByIndex should return a string', () => {
      const result = component.trackById(1, {} as any);

      expect(result).toBe('1');
    });
  });

  describe('trackByTypeId', () => {
    it('trackByTypeId should track by typeId', () => {
      const type = {
        typeId: '1',
        deactivated: true
      } as ITypeSegment;
      const result = component.trackByTypeId(1, type);

      expect(result).toBe('11true');
    });
  });

  it('@changeAccordionState', () => {
    component.changeAccordionState(1, false);

    expect(component.isExpanded).toEqual([true, false, true]);
  });

  it('@filterEvents', () => {
    component.couponEvents = couponEvents.slice();
    component.marketOptions = marketOptions;
    component.filterEvents('Match Betting');

    expect(component.marketFilter).toEqual('Match Betting');
    expect(marketSortService.setMarketFilterForMultipleSections).toHaveBeenCalledWith(component.couponEvents, 'Match Betting');
    expect(component.isEventsUnavailable).toBe(false);
    expect(couponsDetailsService.setOddsHeader).toHaveBeenCalledWith(component.marketOptions, 'Match Betting');
  });

  describe('@showCouponsList', () => {
    it('showCouponsList if it is CLOSED', () => {
      component.showCoupons = true;
      component.showCouponsList();

      expect(component.showCoupons).toBe(false);
      expect(windowRefService.document.body.scrollTop).toBe(1);
      expect(windowRefService.document.documentElement.scrollTop).toBe(1);
    });

    it('showCouponsList if it is OPENED', () => {
      component.showCoupons = false;
      component.showCouponsList();

      expect(component.showCoupons).toBe(true);
      expect(windowRefService.document.body.scrollTop).toBe(0);
      expect(windowRefService.document.documentElement.scrollTop).toBe(0);
    });
  });

  describe('goTopPage', () => {
    it('should return a promise with true', () => {
      router.navigateByUrl.and.returnValue(Promise.resolve(true));

      const result = component.goToPage('test_path');

      (result as Promise<boolean>).then((response: boolean) => {
        expect(response).toBeTruthy();
      });
      expect(router.navigateByUrl).toHaveBeenCalledWith('test_path');
    });

    it('should return false', () => {
      expect(component.goToPage('')).toBeFalsy();
      expect(router.navigateByUrl).not.toHaveBeenCalled();
    });
  });

  describe('filteredEvents', () => {
    it('it should filter coupons events', () => {
      component.savedCouponEvents = [
        { typeName: 'type 1', deactivated: false },
        { typeName: 'type 2', deactivated: true }
      ] as ITypeSegment[];
      const result = component.filteredEvents();

      expect(result).toEqual([{ typeName: 'type 1', deactivated: false }] as ITypeSegment[]);
    });
  });

  describe('@setCouponsData', () => {
    beforeEach(() => {
      component['getCouponsData'] = jasmine.createSpy();
      component['getCouponsListData'] = jasmine.createSpy();
    });

    it('it should set coupon data for Goalscorer coupon', () => {
      component.couponId = '10';
      component['setCouponsData']('Goalscorer-Coupon');

      expect(component.marketOptions).toEqual([]);
      expect(couponsDetailsService.isGoalscorerCoupon).toBe(true);
      expect(footballService.unSubscribeCouponsForUpdates).toHaveBeenCalledWith('football-coupons');
      expect(component['getCouponsData']).toHaveBeenCalledWith('Goalscorer Coupon');
      expect(component['getCouponsListData']).toHaveBeenCalledWith();
    });

    it('it should set coupon data for Over/Under Coupon', () => {
      component.couponId = '10';
      component['setCouponsData']('Over / Under Total Goals');

      expect(couponsDetailsService.isGoalscorerCoupon).toBe(false);
    });

    it('it should set coupon data for UK Coupon', () => {
      component.couponId = '10';
      component['setCouponsData']('UK Coupon');

      expect(couponsDetailsService.isGoalscorerCoupon).toBe(false);
    });
  });

  describe('@getEventsIsUnavailable', () => {
    it('eventsIdsUnavailable should return true -> isEmptyEvents ', () => {
      component.isEmptyEvents = true;
      expect(component['getEventsIsUnavailable']).toBe(true);
    });

    it('eventsIdsUnavailable should return false -> isCustomCoupon', () => {
      couponsDetailsService.isCustomCoupon = true;
      expect(component['getEventsIsUnavailable']).toBe(false);
    });

    it('eventsIdsUnavailable should return true', () => {
      component.couponEvents = [
        {
          groupedByDate: [
            { deactivated : true },
            { deactivated : true }
          ]
        }
      ];
      expect(component['getEventsIsUnavailable']).toBe(true);
    });

    it('eventsIdsUnavailable should return false', () => {
      component.couponEvents = [
        { groupedByDate: [
            { deactivated : true },
            { deactivated : false }
          ]
        }
      ];
      expect(component['getEventsIsUnavailable']).toBe(false);
    });
  });

  describe('@setTabletTopTitleWidth', () => {
    it('setTabletTopTitleWidth', () => {
      component['setTabletTopTitleWidth']();

      expect(elementRef.nativeElement.querySelector).toHaveBeenCalled();
      expect(windowRefService.document.getElementById).toHaveBeenCalled();
      expect(domToolsService.getWidth).toHaveBeenCalled();
      expect(domToolsService.css).toHaveBeenCalledTimes(2);
    });

    it(`should return if Not couponsTopTitle`, () => {
      elementRef.nativeElement.querySelector.and.returnValue(null);

      component['setTabletTopTitleWidth']();

      expect(domToolsService.css).not.toHaveBeenCalled();
    });
  });

  describe('deleteEvent: should delete proper event', () => {
    it('deleteEvent: should delete event by id', () => {
      component.savedCouponEvents = couponEvents;
      component['deleteEvent'](5112681);
      expect(component.savedCouponEvents).toEqual([{
        typeId: 971,
        events: [{
          id: 3567838,
          typeId: 971,
        }],
        groupedByDate: [{
          events: [{
            id: 3567838
          }]
        }]
      }]);
    });

    it('deleteEvent: should delete full section', () => {
      component.savedCouponEvents = [{
        typeId: 971,
        events: [{
          id: 3567838
        }],
        groupedByDate: [{
          events: [{
            id: 3567838
          }]
        }]
      }];
      component['deleteEvent'](3567838);
      expect(component.savedCouponEvents).toEqual([{
        typeId: 971,
        events: [],
        groupedByDate: [{
          events: []
        }]
      }]);
    });

    it('deleteEvent: should not delete is section is empty', () => {
      component.couponEvents = [undefined];
      component['deleteEvent'](3567838);
      expect(component.couponEvents).toEqual([undefined]);
    });

    it('deleteEvent: should not delete is event is not exist', () => {
      component.couponEvents = [{
        typeId: 971,
        events: [{
          id: 3567838
        }],
        groupedByDate: [{
          events: [{
            id: 3567838
          }]
        }]
      }];
      component['deleteEvent'](2564839);
      expect(component.couponEvents).toEqual(component.couponEvents);
    });
  });

  describe('@getCouponName', () => {
    it('should get coupon name', () => {
      const result = component['getCouponName']('Test Coupon');

      expect(result).toBe('Test Coupon');
    });
  });

  describe('@getCouponsListData', () => {
    beforeEach(() => {
      couponsList = [
        { id: '1', couponSortCode: 'MR', name: 'UK coupon' } as any,
        { id: '2', couponSortCode: 'HH', name: 'Test-coupon' } as any
      ];
      footballService.coupons.and.returnValue(of(couponsList));
    });
    it('should get coupon list and check if BetFilter is enable', fakeAsync(() => {
      couponsDetailsService.isBetFilterEnable.and.returnValue(of(true));
      component.couponId = '2';
      component['getCouponsListData']();
      tick();
      expect(component.applyingList).toBeFalsy();
      expect(component.couponName).toEqual('Test coupon');
      expect(component.isBetFilterEnable).toBeTruthy();
      expect(component.couponsList).toEqual(couponsList);
    }));

    it('should get coupon list and check if BetFilter is disable', fakeAsync(() => {
      component.couponId = '1';
      couponsDetailsService.isBetFilterEnable.and.returnValue(of(false));
      component['getCouponsListData']();
      tick();
      expect(component.applyingList).toBeFalsy();
      expect(component.isBetFilterEnable).toBeFalsy();
      expect(component.couponsList).toEqual(couponsList);
    }));

    it('should get coupon list if error', fakeAsync(() => {
      footballService.coupons.and.returnValue(throwError(null));
      couponsDetailsService.isBetFilterEnable.and.returnValue(of(false));
      component.couponId = '1';
      component['getCouponsListData']();
      tick();
      expect(component.applyingList).toBeFalsy();
      expect(component.isBetFilterEnable).toBeFalsy();
      expect(component.couponsList).toEqual([]);
    }));

    it('should get coupon list if error', fakeAsync(() => {
      footballService.coupons.and.returnValue(throwError(null));
      couponsDetailsService.isBetFilterEnable.and.returnValue(of(false));
      component.couponId = '1';
      component['getCouponsListData']();
      tick();
      expect(component.applyingList).toBeFalsy();
      expect(component.isBetFilterEnable).toBeFalsy();
      expect(component.couponsList).toEqual([]);
    }));
  });

  describe('@getCouponsData', () => {
    it('should get coupon data if it is exist', fakeAsync(() => {
      const coupons = [{}] as any;
      component['loadCouponEvents'] = jasmine.createSpy('loadCouponEvents');
      couponsDetailsService.getCouponEvents.and.returnValue(of({ coupons, options: []}));
      component.couponId = '1';
      component['getCouponsData']('test');
      tick();
      expect(couponsDetailsService.getCouponEvents).toHaveBeenCalledWith('1', 'test', footballService);
      expect(component['loadCouponEvents']).toHaveBeenCalledWith(coupons, []);
    }));

    it('should get coupon data if ERROR', fakeAsync(() => {
      const coupons = [{}] as any;
      component['loadCouponEvents'] = jasmine.createSpy('loadCouponEvents');
      couponsDetailsService.getCouponEvents.and.returnValue(throwError({ coupons, options: []}));
      component.couponId = '1';
      component['getCouponsData']('test');
      tick();
      expect(couponsDetailsService.getCouponEvents).toHaveBeenCalledWith('1', 'test', footballService);
      expect(component['loadCouponEvents']).toHaveBeenCalledWith(coupons, []);
    }));

    it('should call showError message in case of connection error', fakeAsync(() => {
      component['loadCouponEvents'] = jasmine.createSpy('loadCouponEvents');
      couponsDetailsService.getCouponEvents.and.returnValue(throwError({
        name: 'HttpErrorResponse'
      } as any));
      component.couponId = '1';
      spyOn(component, 'showError').and.callThrough();
      component['getCouponsData']('test');
      tick();
      expect(component.showError).toHaveBeenCalled();
    }));
  });

  describe('@loadCouponEvents', () => {
    it('should get coupon data if it is exist and init MarketSelector', fakeAsync(() => {
      const events = [{  markets: [] }] as any;
      couponsDetailsService.groupCouponEvents.and.returnValue(couponEvents);
      component['loadCouponEvents'](events, marketOptions);
      tick();
      expect(component.couponEvents).toEqual(couponEvents);
      expect(component.isEmptyEvents).toBeFalsy();
      expect(component.isEventsUnavailable).toBeFalsy();
      expect(component.applyingParams).toBeFalsy();
      expect(component.marketOptions).toEqual(marketOptions);
      expect(couponsDetailsService.groupCouponEvents).toHaveBeenCalledWith(events, footballService);
      expect(footballService.subscribeCouponsForUpdates).toHaveBeenCalledWith(events, 'football-coupons');
    }));

    it('should get coupon data if it is NOT exist', fakeAsync(() => {
      const events = [] as any;
      couponsDetailsService.groupCouponEvents.and.returnValue([]);
      component['loadCouponEvents'](events, marketOptions);
      tick();
      expect(component.couponEvents).toEqual([]);
      expect(component.isEmptyEvents).toBeTruthy();
      expect(component.isEventsUnavailable).toBeTruthy();
      expect(component.applyingParams).toBeFalsy();
      expect(component.marketOptions).toEqual([]);
      expect(couponsDetailsService.groupCouponEvents).toHaveBeenCalledWith(events, footballService);
      expect(footballService.subscribeCouponsForUpdates).not.toHaveBeenCalled();
    }));
    it('should call scrollToPreviousState in case eventIdFromEDP defined', fakeAsync(() => {
      const events = [] as any;
      couponsDetailsService.groupCouponEvents.and.returnValue([]);
      component.eventIdFromEDP = 189;
      spyOn(component as any, 'scrollToPreviousState').and.callThrough();
      component['loadCouponEvents'](events, marketOptions);
      tick();
      expect(component['scrollToPreviousState']).toHaveBeenCalled();
    }));
  });
  describe('getStickyElementsHeight', () => {
    it('should return 0 if no element found', () => {
      expect(component['getStickyElementsHeight']()).toEqual(0);
    });
    it('should return sticky elements height', () => {
      windowRefService.document.querySelector = jasmine.createSpy('querySelector').and.returnValue({});
      expect(component['getStickyElementsHeight']()).toEqual(120);
    });
  });
  describe('scrollToPreviousState', () => {
    it('should not call scrollTo in case if no element found', fakeAsync(() => {
      component['scrollToPreviousState']();
      flush();
      expect(windowRefService.nativeWindow.scrollTo).not.toHaveBeenCalled();
    }));
    it('should scrollTo with previous state position', fakeAsync(() => {
      windowRefService.document.querySelector = jasmine.createSpy('querySelector').and.returnValue({});
      component['scrollToPreviousState']();
      flush();
      expect(windowRefService.nativeWindow.scrollTo).toHaveBeenCalledWith(0, 0);
    }));
  });
  describe('updatePreviousStateInfo', () => {
    beforeEach(() => {
      component.couponEvents = couponEvents;
      component.isExpanded[0] = false;
    });
    it('should set eventIdFromEDP if previous URL is EDP', () => {
      const edpUrl = '/event/football/football-england/championship/event-v-test/3567838/all-markets';
      routingState.getPreviousSegment = jasmine.createSpy('getPreviousSegment').and.returnValue('eventMain');
      routingState.getPreviousUrl = jasmine.createSpy('getPreviousUrl').and.returnValue(edpUrl);
      component['updatePreviousStateInfo']();
      expect(component.eventIdFromEDP).toEqual(3567838);
      expect(component.isExpanded[0]).toBeTruthy();
    });
    it('should no set eventIdFromEDP if event not found', () => {
      const edpUrl = '/event/football/football-england/championship/event-v-test/134213/all-markets';
      routingState.getPreviousSegment = jasmine.createSpy('getPreviousSegment').and.returnValue('eventMain');
      routingState.getPreviousUrl = jasmine.createSpy('getPreviousUrl').and.returnValue(edpUrl);
      component['updatePreviousStateInfo']();
      expect(component.eventIdFromEDP).not.toBeDefined();
      expect(component.isExpanded[0]).toBeFalsy();
    });
    it('should no set eventIdFromEDP if link is wrong', () => {
      const edpUrl = '/event/football/football-england/championship/event-v-test/';
      routingState.getPreviousSegment = jasmine.createSpy('getPreviousSegment').and.returnValue('eventMain');
      routingState.getPreviousUrl = jasmine.createSpy('getPreviousUrl').and.returnValue(edpUrl);
      component['updatePreviousStateInfo']();
      expect(component.eventIdFromEDP).not.toBeDefined();
      expect(component.isExpanded[0]).toBeFalsy();
    });
    it('should no set eventIdFromEDP if page not EDP', () => {
      const edpUrl = '/event/football/football-england/championship/event-v-test/';
      routingState.getPreviousSegment = jasmine.createSpy('getPreviousSegment').and.returnValue('HR_LP');
      routingState.getPreviousUrl = jasmine.createSpy('getPreviousUrl').and.returnValue(edpUrl);
      component['updatePreviousStateInfo']();
      expect(component.eventIdFromEDP).not.toBeDefined();
      expect(component.isExpanded[0]).toBeFalsy();
    });
  });

  describe('handleMatchesMarketSelectorEvent', () => {
    beforeEach(() => {
      spyOn(component, 'filterEvents').and.callThrough();
    });

    it('should filter events', () => {
      component.handleMatchesMarketSelectorEvent({ output: 'filterChange', value: {} });
      expect(component.filterEvents).toHaveBeenCalled();
    });

    it('should not filter events', () => {
      component.handleMatchesMarketSelectorEvent({ output: 'test', value: {} });
      expect(component.filterEvents).not.toHaveBeenCalled();
    });
  });
});
