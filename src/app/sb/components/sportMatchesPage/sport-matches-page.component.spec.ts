import { of as observableOf, throwError } from 'rxjs';

import { SportMatchesPageComponent } from '@sb/components/sportMatchesPage/sport-matches-page.component';

describe('SportMatchesPageComponent', () => {
  let component: SportMatchesPageComponent;

  const tabName = 'TestTabName';
  const matchesTabs = {
    sportConfig: {
      tabs: [{ subTabs: { subTabs: [{ onClick: jasmine.createSpy() }] } }]
    },
    config: { tier: 2 }
  };

  let activatedRouteStub;
  let getSportInstanceServiceStub;
  let locationStub;
  let routingStateStub;
  let router;
  let windowRefService;
  const updateEventService = {} as any;

  beforeEach(() => {
    locationStub = {
      go: jasmine.createSpy(),
      path: () => ''
    };
    routingStateStub = {
      setCurrentUrl: jasmine.createSpy()
    };
    getSportInstanceServiceStub = {
      getSport: jasmine.createSpy('getSport').and.returnValue(observableOf(matchesTabs))
    };
    router = {
      navigateByUrl: jasmine.createSpy()
    };
    windowRefService = {
      nativeWindow: {
        setTimeout: jasmine.createSpy('setTimeout')
      }
    };
    activatedRouteStub = {
      snapshot: {
        paramMap: { get: jasmine.createSpy() },
        params: { tab: tabName },
        parent: { data: {} }
      },
      children: {},
      parent: { snapshot: { url: [{ path: '' }] } },
    };

    component = new SportMatchesPageComponent(
        locationStub,
        getSportInstanceServiceStub,
        routingStateStub,
        activatedRouteStub,
        router,
        windowRefService,
        updateEventService);

    component['featuredModuleRef'] = {
      destroy: jasmine.createSpy()
    } as any;
    component['featuredModuleView'] = {
      parentInjector: {},
      createComponent: jasmine.createSpy().and.returnValue({
        instance: {
          sportId: 0,
          sportName: 'sportName'
        },
        destroy: jasmine.createSpy()
      })
    } as any;
    component.SportMatchesTab = {
      ngOnInit: jasmine.createSpy('ngOnInit'),
      ngOnDestroy: jasmine.createSpy('ngOnDestroy')
    };
  });

  describe('ngOnInit', () => {
    describe('should define sportId', () => {
      it('as number 0', () => {
        component.sport = null;
        component.ngOnInit();

        expect(component.sportId).toEqual(0);

        component.sport = {
          readonlyRequestConfig: undefined
        } as any;
        component.ngOnInit();

        expect(component.sportId).toEqual(0);

        component.sport = {
          readonlyRequestConfig: {
            categoryId: undefined
          }
        } as any;
        component.ngOnInit();

        expect(component.sportId).toEqual(0);
      });

      it('from RequestConfig', () => {
        getSportInstanceServiceStub.getSport.and.returnValue(observableOf(
          Object.assign({}, matchesTabs, { readonlyRequestConfig: { categoryId: '333' } })
        ));
        component.ngOnInit();

        expect(component.sportId).toEqual('333');
      });
    });

    describe('#reloadPage', () => {
      it('should reload component', () => {
        component.showSpinner = jasmine.createSpy('showSpinner');
        component.reloadPage();

        expect(component.showSpinner).toHaveBeenCalled();
        expect(component.SportMatchesTab.ngOnInit).toHaveBeenCalled();
        expect(component.SportMatchesTab.ngOnDestroy).toHaveBeenCalled();
      });
    });

    describe(`getSport' return error`, () => {
      it(`should hide spinner`, () => {
        getSportInstanceServiceStub.getSport.and.returnValue(throwError('adads'));
        spyOn(component, 'hideSpinner');

        component.ngOnInit();

        expect(component['hideSpinner']).toHaveBeenCalled();
      });
    });
  });

  describe('loadSport', () => {
    beforeEach(() => {
      component.sportName = 'sportName';
    });

    it(`should generate new 'olympics' sportPath if path segment is equal 'olympicsSport'`, () => {
      component['route'].snapshot.parent.data['segment'] = 'olympicsSport';

      component['loadSport']();

      expect(component['location'].go).toHaveBeenCalledWith('olympics/sportName/matches');
    });

    it(`should generate new 'sport' sportPath if path segment is Not equal 'olympicsSport'`, () => {
      component['route'].snapshot.parent.data['segment'] = 'someStr';

      component['loadSport']();

      expect(component['location'].go).toHaveBeenCalledWith('sport/sportName/matches');
    });

    it(`should generate 'newURL' with 'tabName' if it is defined`, () => {
      component['loadSport']('today');

      expect(component['location'].go).toHaveBeenCalledWith('sport/sportName/matches/today');
    });

    it(`should generate 'newURL' without 'tabName' if it is Not defined`, () => {
      component['loadSport']();

      expect(component['location'].go).toHaveBeenCalledWith('sport/sportName/matches');
    });

    it(`should Not change path if current path is the same`, () => {
      component['location'].path = () => '/sport/sportName/matches';

      component['loadSport']();

      expect(component['location'].go).not.toHaveBeenCalled();
    });

    it(`should 'setCurrentUrl' if current path is Not the same`, () => {
      component['location'].path = () => '/pathname';

      component['loadSport']();

      expect(component['routingState'].setCurrentUrl).toHaveBeenCalledWith('/pathname');
    });

    it(`should Not 'setCurrentUrl' if current path is the same`, () => {
      component['location'].path = () => '/sport/sportName/matches';

      component['loadSport']();

      expect(component['routingState'].setCurrentUrl).not.toHaveBeenCalled();
    });
  });

  describe(`updateLoadStatus`, () => {

    beforeEach(() => {
      windowRefService.nativeWindow.setTimeout.and.callFake(cb => cb && cb());
      spyOn(component, 'hideSpinner');
    });

    it(`should update featuredEventsCount and not update isNotAllModulesLoaded`, () => {
      const output = {
        output: 'featuredEventsCount',
        value: 2
      };
      component.updateLoadStatus(0, output);

      expect(component.featuredEventsCount).toEqual(output.value);
      expect(windowRefService.nativeWindow.setTimeout).toHaveBeenCalled();
      expect(component['modulesLoadStatus']).toEqual([]);
      expect(component.isNotAllModulesLoaded).toBeTruthy();
      expect(component.hideSpinner).not.toHaveBeenCalled();
    });

    it(`should update isNotAllModulesLoaded, not update featuredEventsCount, not call hideSpinner`, () => {
      const output = {
        output: 'isLoadedEvent',
        value: false
      };
      component.updateLoadStatus(0, output);

      expect(component.featuredEventsCount).toEqual(0);
      expect(windowRefService.nativeWindow.setTimeout).toHaveBeenCalled();
      expect(component['modulesLoadStatus'][0]).toEqual(output.value);
      expect(component.isNotAllModulesLoaded).toBeTruthy();
      expect(component.hideSpinner).not.toHaveBeenCalled();
    });

    it(`should update isNotAllModulesLoaded and not update featuredEventsCount`, () => {
      const output = {
        output: 'isLoadedEvent',
        value: true
      };
      component.updateLoadStatus(undefined, output);

      expect(component.featuredEventsCount).toEqual(0);
      expect(windowRefService.nativeWindow.setTimeout).toHaveBeenCalled();
      expect(component['modulesLoadStatus'][0]).toEqual(output.value);
      expect(component.isNotAllModulesLoaded).toBeFalsy();
      expect(component.hideSpinner).toHaveBeenCalled();
    });
  });

  describe(`@ngOnDestroy`, () => {
    it(`unsubscribe sportsConfigSubscription`, () => {
      component['sportsConfigSubscription'] = {
        unsubscribe: jasmine.createSpy('unsubscribe')
      } as any;

      component.ngOnDestroy();
      expect(component['sportsConfigSubscription'].unsubscribe).toHaveBeenCalled();
    });
  });
});
