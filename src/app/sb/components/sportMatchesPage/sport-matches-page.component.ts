import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { GamingService } from '@core/services/sport/gaming.service';
import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { RoutingState } from '@sharedModule/services/routingState/routing-state.service';
import { SportsConfigService } from '@sb/services/sportsConfig/sports-config.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { UpdateEventService } from '@core/services/updateEvent/update-event.service';
import { Subscription } from 'rxjs';
import { ILazyComponentOutput } from '@app/shared/components/lazy-component/lazy-component.model';

@Component({
  selector: 'sport-matches-page',
  templateUrl: 'sport-matches-page.component.html'
})
export class SportMatchesPageComponent extends AbstractOutletComponent implements OnInit, OnDestroy {

  @ViewChild('SportMatchesTab') SportMatchesTab;

  sport: GamingService;
  sportId: string | number;
  sportName: string;
  isNoModulesLoaded: boolean = true;

  isNotAllModulesLoaded: boolean = true;
  featuredEventsCount: number = 0;

  private modulesLoadStatus: boolean[] = [];
  protected sportsConfigSubscription: Subscription;

  constructor(private location: Location,
              protected sportsConfigService: SportsConfigService,
              private routingState: RoutingState,
              protected route: ActivatedRoute,
              protected router: Router,
              protected windowRefService: WindowRefService,
              // tslint:disable-next-line
              protected updateEventService: UpdateEventService // for events subscription (done in service init)
              ) {
    super();
  }

  ngOnInit(): void {
    this.showSpinner();
    this.sportName = this.route.snapshot.paramMap.get('sport');

    this.sportsConfigSubscription = this.sportsConfigService.getSport(this.sportName)
      .subscribe((sport: GamingService) => {
        this.sport = sport;
        this.sportId = (this.sport && this.sport.readonlyRequestConfig && this.sport.readonlyRequestConfig.categoryId) || 0;

        this.loadSport();
      }, error => {
        this.showError();
        console.warn('MatchesPage', error.error || error);
      });
  }

  ngOnDestroy(): void {
    this.sportsConfigSubscription && this.sportsConfigSubscription.unsubscribe();
  }

  /**
   * Update global and module-personal (by index) load statuses
   * index should correspond to specific module
   *
   * setTimeout is to avoid ExpressionChangedAfterItHasBeenCheckedError (and not to use ChangeDetectorRef)
   *
   * @param i
   * @param isLoaded
   */
  updateLoadStatus(i: number = 0, output: ILazyComponentOutput): void {
    if (output.output === 'featuredEventsCount') {
      this.featuredEventsCount = output.value;
    }

    this.windowRefService.nativeWindow.setTimeout(() => {
      if (output.output === 'isLoadedEvent') {
        this.modulesLoadStatus[i] = output.value;
        this.isNotAllModulesLoaded = !this.modulesLoadStatus.every(loaded => loaded);
        if (!this.isNotAllModulesLoaded) {
          this.hideSpinner();
        }
      }
    });
  }

  reloadPage(): void {
    this.showSpinner();
    this.SportMatchesTab.ngOnDestroy();
    this.SportMatchesTab.ngOnInit();
  }

  /**
   * Load Sport Page (ex: Today, Tomorrow, Future)
   */
  protected loadSport(tabName: string = ''): void {
    const sportPath = this.route.snapshot.parent.data['segment'] === 'olympicsSport'
      ? `olympics/${this.sportName}`
      : `sport/${this.sportName}`;

    const newURL: string = `${sportPath}/matches${tabName && `/${tabName}`}`;
    const pathName: string = this.location.path();

    if (`/${newURL}` === pathName) { return; }

    this.location.go(newURL);
    // Needs for back button functionality as location.go doesn't trigger router events
    this.routingState.setCurrentUrl(pathName);
  }
}
