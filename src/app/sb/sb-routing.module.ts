import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
  PrivateMarketsTermsAndConditionsComponent
} from '@app/sb/components/privateMarketsTab/private-markets-terms-and-conditions.component';
import { JackpotReceiptPageComponent } from '@app/sb/components/jackpotReceiptPage/jackpot-receipt-page.component';

export const routes: Routes = [
  {
    path: 'competitions/:sport',
    pathMatch: 'full',
    redirectTo: '/sport/:sport/competitions'
  },
  {
    path: 'competitions/:sport/:className',
    pathMatch: 'full',
    redirectTo: '/sport/:sport/competitions'
  },
  {
    path: 'private-markets',
    children: [
      {
        path: 'terms-conditions',
        component: PrivateMarketsTermsAndConditionsComponent,
        data: {
          segment: 'privateMarketsTeamsAndConditions'
        }
      }]
  },
  {
    path: 'football-jackpot-receipt',
    component: JackpotReceiptPageComponent,
    data: {
      segment: 'footballJackpotReceipt'
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class SbRoutingModule { }
