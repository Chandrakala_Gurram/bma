import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from '@sharedModule/shared.module';
import { SbModule } from '@sbModule/sb.module';
import { SportMainModule } from '@sbModule/components/sportMain/sport-main.module';
import { CouponsModule } from '@sb/coupons/coupons.module';
import { LazySportRoutingModule } from './sport-routing.module';
import { SportMatchesPageComponent } from '@sb/components/sportMatchesPage/sport-matches-page.component';
import { SportTabsPageComponent } from '@sb/components/sportTabsPage/sport-tabs-page.component';
import { FootballTutorialOverlayComponent } from '@sbModule/components/footballTutorialOverlay/football-tutorial-overlay.component';
import { SportMatchesTabComponent } from '@sbModule/components/sportMatchesTab/sport-matches-tab.component';

@NgModule({
  imports: [
    SharedModule,
    SbModule, // TODO do not import when SbModule will be sliced
    SportMainModule,
    CouponsModule,

    LazySportRoutingModule
  ],
  declarations: [
    SportMatchesPageComponent,
    SportTabsPageComponent,
    FootballTutorialOverlayComponent,
    SportMatchesTabComponent
  ],
  entryComponents: [
    SportMatchesPageComponent,
    SportTabsPageComponent,
    FootballTutorialOverlayComponent,
    SportMatchesTabComponent
  ],
  providers: [],
  exports: [
    SportMatchesPageComponent,
    SportTabsPageComponent,
    FootballTutorialOverlayComponent,
    SportMatchesTabComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class LazySportModule { }
