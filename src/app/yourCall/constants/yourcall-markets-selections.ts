/**
 * DS Markets statistics values
 */
export const YOURCALL_MARKETS_SELECTIONS =  {
  OUTCOME: {
    HOME: 1,
    DRAW: 2,
    VISIT: 3
  },
  BINARY: {
    NO: 0,
    YES: 1
  },
  OVER_UNDER: {
    UNDER: 1,
    OVER: 3
  }
};
