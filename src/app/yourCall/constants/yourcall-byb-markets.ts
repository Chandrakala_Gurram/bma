export const YOURCALL_BYB_MARKETS = {
  OUTCOME: 'Outcome',
  CARDS: 'Cards',
  GOALS: 'Goals',
  PLAYER_BETS: 'Player Bets',
  BOTH_SCORE: 'Both score',
  TOTAL_GOALS: 'Total goals',
  TOTAL_CORNERS: 'Total corners',
  TOTAL_BOOKING_POINTS: 'Total booking points'
};
