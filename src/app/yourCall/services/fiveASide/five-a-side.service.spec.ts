import { FiveASideService } from './five-a-side.service';
import { playersMock, playersWithoutStats, playersWithStats, optaMock } from './five-a-side.mock';
import { fakeAsync, tick } from '@angular/core/testing';
import { CoreToolsService } from '@root/app/core/services/coreTools/core-tools.service';
import { of, throwError } from 'rxjs';

describe('#FiveASideService', () => {
  let service;
  let cmsService;
  let yourcallProviderService;
  let yourcallMarketsService;
  let coreToolsService;
  let domSanitizer;

  beforeEach(() => {
    coreToolsService = new CoreToolsService();

    cmsService = {
      getFiveASideFormations: jasmine.createSpy('getFiveASideFormations'),
      getTeamsColors: jasmine.createSpy('getTeamsColors').and.returnValue(of([])),
      getFiveASideStaticBlocks: jasmine.createSpy('getFiveASideStaticBlocks')
    };
    yourcallProviderService = {
      getPlayers: jasmine.createSpy('getPlayers').and.returnValue(Promise.resolve(playersMock))
    };
    yourcallMarketsService = {
      game: {
        homeTeam: {
          title: 'Liverpool'
        },
        visitingTeam: {
          title: 'Arsenal'
        }
      },
      getStatisticsForPlayer: jasmine.createSpy('getStatisticsForPlayer').and.returnValue(of([]))
    };
    domSanitizer = {
      bypassSecurityTrustHtml: jasmine.createSpy('bypassSecurityTrustHtml')
    };

    service = new FiveASideService(
      cmsService,
      yourcallProviderService,
      yourcallMarketsService,
      coreToolsService,
      domSanitizer
    );
  });

  it('should create service instance', () => {
    expect(service).toBeTruthy();
  });

  describe('#getFormations', () => {
    it('should call getFormations method', () => {
      service.getFormations();

      expect(cmsService.getFiveASideFormations).toHaveBeenCalled();
      expect(service.game).toEqual({
        homeTeam: {
          title: 'Liverpool'
        },
        visitingTeam: {
          title: 'Arsenal'
        }
      });
    });
  });

  describe('#getPlayerList', () => {
    beforeEach(() => {
      service.game = yourcallMarketsService.game;
    });

    it('should call getPlayerList method and retrun players without stats', fakeAsync(() => {
      service.getPlayerList(12345).subscribe();
      tick(200);

      expect(service.playerList).toEqual(playersWithoutStats);
    }));

    it('should call getPlayerList method and retrun players without stats if no OPTA playes', fakeAsync(() => {
      service._localStorage.getItem = jasmine.createSpy('getItem').and.returnValue(JSON.stringify({
        data: {
          players: {
            home: [],
            away: []
          }
        }
      }));
      service.getPlayerList(12345).subscribe();
      tick(200);

      expect(service.playerList).toEqual(playersWithoutStats);
    }));

    it('should call getPlayerList method and retrun players when no opta at storage', fakeAsync(() => {
      service._localStorage.getItem = jasmine.createSpy('getItem').and.returnValue(JSON.stringify({data: {
        noPlayers: {}
      }}));

      service.getPlayerList(12345).subscribe();
      tick(200);

      expect(service.playerList).toEqual(playersWithoutStats);
    }));

    it('should call getPlayerList method and retrun players with stats', fakeAsync(() => {
      service._localStorage.getItem = jasmine.createSpy('getItem').and.returnValue(JSON.stringify(optaMock));
      service.teamsColors = {
        Liverpool: { primaryColour: '#fff', secondaryColour: undefined },
        Arsenal: { primaryColour: '#fff', secondaryColour: undefined }
      };
      service.getPlayerList(12345).subscribe();
      tick(200);

      expect(service.playerList).toEqual(playersWithStats);
    }));
  });

  describe('#sortPlayers', () => {
    beforeEach(() => {
      service.game = yourcallMarketsService.game;
    });
    it('should call sortPlayers method', fakeAsync(() => {
      service._localStorage.getItem = jasmine.createSpy('getItem').and.returnValue(JSON.stringify(optaMock));
      service.getPlayerList(12345).subscribe();
      tick(200);
      service.sortPlayers('Passes');

      expect(service.playerList.allPlayers[0].passes).toEqual(60);
      expect(service.playerList.allPlayers[1].passes).toEqual(50);

      service.sortPlayers('Passes', 'home');

      expect(service.playerList.allPlayers[0].passes).toEqual(60);
      expect(service.playerList.allPlayers[1].passes).toEqual(50);
    }));
  });

  describe('#parsePlayer', () => {
    let optaPlayer;
    let banachplayer;

    beforeEach(() => {
      optaPlayer = {
        attendance: {
          appearances: 0
        },
        goalKeeper: {
          conceeded: 1,
          totalSaves: 1,
          penaltySaves: 1,
        },
      } as any;
      banachplayer = {
        id: '1',
        name: 'Oleh',
        team: {
          title: 'Team'
        }
      };
    });

    it('should parsePlayer when appearance 0', () => {
      const result = service['parsePlayer'](optaPlayer, banachplayer);
      expect(result).toEqual({
        id: '1',
        name: 'Oleh',
        teamName: 'Team',
        teamColors: undefined,
        appearances: 0,
        cleanSheets: undefined,
        tackles: undefined,
        passes: undefined,
        crosses: undefined,
        assists: undefined,
        shots: undefined,
        shotsOnTarget: undefined,
        shotsOutsideTheBox: undefined,
        goals: undefined,
        goalsInsideTheBox: undefined,
        goalsOutsideTheBox: undefined,
        cards: undefined,
        cardsRed: undefined,
        cardsYellow: undefined,
        position: { long: undefined, short: undefined },
        penaltySaves: 1,
        conceeded: 0,
        saves: 0,
        isGK: false,
      });
    });

    it('should opta statistics be available',() => {
      service['parsePlayer'](optaPlayer, banachplayer);

      expect(service.optaStatisticsAvailable).toBeFalsy();
    });

    it('should opta statistics not be available ',() => {
      optaPlayer.attendance.appearances = 1;
      service['parsePlayer'](optaPlayer, banachplayer);

      expect(service.optaStatisticsAvailable).toBeTruthy();
    });
  });

  describe('#sortByProperties', () => {
    it('should sort case#1', () => {
      const items = [{
        id: 2,
        name: 'a'
      }, {
        id: 2,
        name: 'c'
      }, {
        id: 2,
        name: 'b'
      }];

      const result = service.sortByProperties(items, ['id', 'name']);

      expect(result).toEqual(
        [{
          id: 2,
          name: 'a'
        }, {
          id: 2,
          name: 'b'
        }, {
          id: 2,
          name: 'c'
        }]
      );
    });

    it('should sort case#2', () => {
      const items = [{
        id: 3,
        name: 'a'
      }, {
        id: 1,
        name: 'c'
      }, {
        id: 5,
        name: 'b'
      }];

      const result = service.sortByProperties(items, ['id', 'name']);

      expect(result).toEqual(
        [{
          id: 5,
          name: 'b'
        }, {
          id: 3,
          name: 'a'
        }, {
          id: 1,
          name: 'c'
        }]
      );
    });

    it('should sort case#3', () => {
      const items = [{
        id: 3,
        name: 'a'
      }, {
        id: 1,
        name: 'c'
      }, {
      id: 5,
      name: 'b'
      }, {
        id: 5,
        name: 'b'
      }];

      const result = service.sortByProperties(items, ['id', 'name']);

      expect(result).toEqual(
        [{
          id: 5,
          name: 'b'
        },
         {
          id: 5,
          name: 'b'
        }, {
          id: 3,
          name: 'a'
        }, {
          id: 1,
          name: 'c'
        }]
      );
    });

    it('should sort case#4', () => {
      const items = [{
        id: 3,
        name: 'a'
      }, {
        id: 1,
        name: 'c'
      }];

      const result = service.sortByProperties(items, []);

      expect(result).toEqual(
        [{
          id: 1,
          name: 'c'
        },
        {
          id: 3,
          name: 'a'
        }]
      );
    });

    it('should sort case#5', () => {
      coreToolsService.getOwnDeepProperty = jasmine.createSpy('getOwnDeepProperty').and.returnValue(undefined);
      const items = [{
        id: 3,
        name: 'a'
      }, {
        id: 1,
        name: 'c'
      }];

      const result = service.sortByProperties(items, ['id', 'name']);

      expect(result).toEqual(
        [{
          id: 1,
          name: 'c'
        },
        {
          id: 3,
          name: 'a'
        }]
      );
    });
  });

  describe('#initTeamsColors', () => {
    beforeEach(() => {
      service.game = yourcallMarketsService.game;
    });
    it('should call initTeamsColors method (same ob ID)', () => {
      service.teamsColors = {
        primaryColour: 'string',
        secondaryColour: 'string'
      };
      service.activeObEventId = 123;
      service.initTeamsColors('16', 123).subscribe();

      expect(cmsService.getTeamsColors).not.toHaveBeenCalled();
    });

    it('should not call getTeamsColors if teamsColorObservable exists', () => {
      service.teamsColorObservable = of({});
      service.activeObEventId = 124;
      service.initTeamsColors('16', 124).subscribe();

      expect(cmsService.getTeamsColors).not.toHaveBeenCalled();
    });

    it('should call initTeamsColors method (same ob ID)', () => {
      service.teamsColors = {
        primaryColour: 'string',
        secondaryColour: 'string'
      };
      service.activeObEventId = 1234;
      service.initTeamsColors('16', 123).subscribe();

      expect(cmsService.getTeamsColors).toHaveBeenCalled();
    });

    it('should call initTeamsColors method (same ob ID)', () => {
      cmsService.getTeamsColors.and.returnValue(throwError('error'));
      service.teamsColors = {
        primaryColour: 'string',
        secondaryColour: 'string'
      };
      service.activeObEventId = 1234;
      service.initTeamsColors('16', 123).subscribe();

      expect(cmsService.getTeamsColors).toHaveBeenCalledWith(['Liverpool', 'Arsenal'], '16');
      expect(service.teamsColors).toEqual({
        primaryColour: 'string',
        secondaryColour: 'string'
      });
    });

    it('should call initTeamsColors method new ob ID', () => {
      cmsService.getTeamsColors.and.returnValue(of([{
        teamName: 'Liverpool',
        primaryColour: '#fff',
        id: 'string',
        sportId: '16',
        secondaryNames: ['string1', 'string2']
      }, {
        teamName: 'Arsenal',
        primaryColour: '#fff',
        id: 'string',
        sportId: '16',
        secondaryNames: ['string1', 'string2']
      }
      ]));
      service.activeObEventId = 1234;
      service.initTeamsColors('16', 123).subscribe();

      expect(cmsService.getTeamsColors).toHaveBeenCalledWith(['Liverpool', 'Arsenal'], '16');
      expect(service.teamsColors).toEqual({
        Liverpool: { primaryColour: '#fff', secondaryColour: '#675d5d' },
        Arsenal: { primaryColour: '#fff', secondaryColour: '#675d5d' }
      });
    });
  });

  describe('showView', () => {
    it('showViewHandler should set view activeItem and player props', () => {
      service.showView({
        view: 'view',
        item: 'item',
        player: 'player'
      } as any);
      expect(service.activeItem).toEqual('item' as any);
      expect(service.view).toEqual('view' as any);
      expect(service.player).toEqual('player' as any);
    });

    it(`should not update player if Not player data`, () => {
      service['player'] = 'player';

      service.showView({
        view: 'view'
      } as any);

      expect(service['player']).toEqual('player');
    });

    it('should set Edit Mode', () => {
      service['player'] = 'player';

      service.showView({
        view: 'view'
      } as any, true);

      expect(service['player']).toEqual('player');
      expect(service.isEditMode).toBe(true);
    });

    it(`should not update item if Not item data`, () => {
      service['item'] = 'qwerty';

      service.showView({
        view: 'item'
      } as any);

      expect(service['item']).toEqual('qwerty');
    });
  });

  describe('hideView', () => {
    it('hideView should reset view prop', () => {
      service['view'] = 'qwerty';
      service['player'] = 'player';
      service['item'] = 'item';
      service.hideView();
      expect(service['view']).toEqual('');
      expect(service['item']).toBeUndefined();
      expect(service['player']).toBeUndefined();
    });
  });

  describe('activeView', () => {
    it('should return service.view', () => {
      service['view'] = 'qwerty';
      expect(service.activeView).toEqual('qwerty');
    });
  });

  describe('activeFormation', () => {
    it('should return service.formation', () => {
      service['formation'] = 'Traditional';
      expect(service.formation).toEqual('Traditional');
    });

    it('should set value service.formation', () => {
      expect(service.formation).toEqual(undefined);
      service.activeFormation = 'Traditional';

      expect(service.activeFormation).toEqual('Traditional');
    });
  });

  describe('activeItem', () => {
    it('should return service.item', () => {
      service['item'] = 'qwerty' as any;
      expect(service.activeItem).toEqual('qwerty');
    });
  });

  describe('activePlayer', () => {
    it('should return service.player', () => {
      service['player'] = 'qwerty' as any;
      expect(service.activePlayer).toEqual('qwerty');
    });
  });

  describe('#activeMatrixFormation', () => {
    it('should get velue', () => {
      const matrix = [{ roleId: '123' }];
      service['matrixFormation'] = matrix as any;
      expect(service.activeMatrixFormation).toEqual(matrix);
    });

    it('should set velue', () => {
      const matrix = [{ roleId: '123' }];
      service.activeMatrixFormation = matrix as any;
      expect(service['matrixFormation']).toEqual(matrix);
    });
  });

  describe('#checkHexColor', () => {
    it('should return color', () => {
      const color = service.checkHexColor('#4da2f9', '123456');

      expect(color).toEqual('#4da2f9');
    });
    it('should return default color', () => {
      const color = service.checkHexColor('123456', '#675d5d');

      expect(color).toEqual('#675d5d');
    });
  });

  describe('isGK', () => {
    it('should return true', () => {
      expect(
        service['isGK']({ position: { title: 'Goalkeeper'} }, {})
      ).toBeTruthy();
    });

    it('should return true', () => {
      expect(
        service['isGK']({}, { position: 'Goalkeeper' })
      ).toBeTruthy();
    });

    it('should return false', () => {
      expect(
        service['isGK']({}, {})
      ).toBeFalsy();
    });
  });

  describe('applyStatEdit', () => {
    beforeEach(() => {
      service.item = { stat: 'Goals', statId: 1 };
    });

    it('should edit stats', () => {
      const changedItem = { stat: 'Passes', statId: 2 };
      service.applyStatEdit(changedItem);
      expect(service.item).toEqual(changedItem);
    });

    it('should not edit stats', () => {
      service.applyStatEdit(service.item);
      expect(service.item).toEqual(service.item);
    });
  });

  describe('saveDefaultStat', () => {
    beforeEach(() => {
      service.item = { stat: 'Goals', statId: 1 };
    });

    it('should save default stat', () => {
      service.saveDefaultStat();
      expect(service.item.defaultStat).toEqual({ stat: 'Goals', statId: 1 });
    });

    it('should not save default stat', () => {
      service.item.defaultStat = {};
      service.saveDefaultStat();
      expect(service.item.defaultStat).toBe(service.item.defaultStat);
    });
  });

  describe('restoreDefaultStat', () => {
    beforeEach(() => {
      service.item = { stat: 'Goals', statId: 1 };
    });

    it('should restore default stat', () => {
      service.item.defaultStat = { stat: 'To Be Carded', statId: 3 };
      service.restoreDefaultStat();
      expect(service.item).toEqual({ stat: 'To Be Carded', statId: 3, defaultStat: null });
    });

    it('should not restore default stat', () => {
      service.restoreDefaultStat();
      expect(service.item).toEqual(service.item);
    });
  });

  describe('loadPlayerStats', () => {
    it('should get players from playersStats', () => {
      service.playerStats[`111-22`] = [1, 2];
      service.loadPlayerStats(111, 22).subscribe(res => {
        expect(res).toBe(service.playerStats[`111-22`]);
      });
    });

    it('should get players from service', () => {
      yourcallMarketsService.getStatisticsForPlayer.and.returnValue(of({
        allData: [
          { id: 11, title: 'Woodwork' },
          { id: 13, title: 'Cards' },
          { id: 21, title: 'Goals' },
          { id: 22, title: 'Shoots' },
          { id: 23, title: 'Goals' }
        ]
      }));
      service.loadPlayerStats(333, 44).subscribe(res => {
        expect(res).toEqual([
          { id: 21, title: 'Goals' },
          { id: 23, title: 'Goals' },
          { id: 22, title: 'Shoots' },
          { id: 13, title: 'To Be Carded' }
        ]);
      });
    });

    it('should throw error', () => {
      yourcallMarketsService.getStatisticsForPlayer.and.returnValue(of(null));
      const errorSpy = jasmine.createSpy('errorSpy');
      service.loadPlayerStats(333, 44).subscribe(null, errorSpy);
      expect(errorSpy).toHaveBeenCalled();
    });
  });

  it('getPlayerStats', () => {
    service.playerStats[`111-22`] = [1, 2];
    expect(service.getPlayerStats(111, 22)).toBe(service.playerStats[`111-22`]);
  });

  describe('getJourneyStaticBlocks', () => {
    const staticBlockArray = [
      {
        htmlMarkup: `<h2 style="text-align: center;">
                        <span style="color: #252835;">5-A-Side</span>
                     </h2>↵
                     <p style="text-align: center;">
                        Welcome to our new feature 5-A-Side. You have a free &pound;1 bet to use, simply build your team and odds to place!
                     </p>`,
        title: 'five-a-side-free-bet'
      }
    ];
    const staticBlockObject = {
      'five-a-side-free-bet': {
        htmlMarkup: `<h2 style="text-align: center;">
                        <span style="color: #252835;">5-A-Side</span>
                     </h2>↵
                     <p style="text-align: center;">
                        Welcome to our new feature 5-A-Side. You have a free &pound;1 bet to use, simply build your team and odds to place!
                     </p>`,
        title: 'five-a-side-free-bet'
      }
    };

    it('should get static blocks', fakeAsync(() => {
      domSanitizer.bypassSecurityTrustHtml.and.returnValue(staticBlockObject['five-a-side-free-bet']);
      cmsService.getFiveASideStaticBlocks.and.returnValue(of(staticBlockArray));

      service.getJourneyStaticBlocks().subscribe();
      tick();

      expect(service['journeyItems']).toEqual(jasmine.any(Object)); // in fact it'ss equal staticBlockObject
    }));

    it('should return static blocks',() => {
      service['journeyItems'] = staticBlockObject;

      service.getJourneyStaticBlocks().subscribe(data => {
        expect(data).toEqual(service['journeyItems']);
      });
    });
  });
});
