import { Injectable } from '@angular/core';
import { catchError, map, shareReplay } from 'rxjs/operators';
import { Observable, from as fromPromise, of, forkJoin, from } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';

import { CmsService } from '@root/app/core/services/cms/cms.service';
import { IFormation } from '@root/app/core/services/cms/models';
import { YourcallProviderService } from '../yourcallProvider/yourcall-provider.service';
import { IPlayerBets, IPlayerBet } from '../../models/yourcall-api-response.model';
import { IFiveASidePlayers, IFiveASidePlayer, ITeamColors, ITeamColorsData } from './five-a-side.model';
import environment from '@environment/oxygenEnvConfig';
import { IConstant } from '@root/app/core/services/models/constant.model';
import { YourcallMarketsService } from '../yourCallMarketsService/yourcall-markets.service';
import { YourCallEvent } from '../../models/yourcall-event';
import { POSITIONS, MARKETS, PLAYER_STATS_NAMES, PLAYER_STATS_EXCLUDE } from '../../constants/five-a-side.constant';
import { CoreToolsService } from '@root/app/core/services/coreTools/core-tools.service';
import { IMatrixFormation, IShowView } from '@yourcall/models/five-a-side.model';
import { DEFAULT_TEAM_COLOURS } from '@yourcall/constants/five-a-side.constant';
import { IYourcallStatisticResponse, IYourcallStatisticItem } from '@yourcall/models/yourcall-api-response.model';
import { IJourneyItems, IJourneyStaticBlock } from '@core/services/cms/models/five-a-side-journey.model';

@Injectable({ providedIn: 'root' })
export class FiveASideService {
  formations: IFormation[];
  players: IPlayerBets;
  playerList: IFiveASidePlayers;
  game: YourCallEvent;
  teamsColors = {} as ITeamColors;
  activeObEventId: number;
  isEditMode: boolean = false;
  optaStatisticsAvailable: boolean = false;

  private readonly positionsMap: IConstant = POSITIONS;
  private readonly marketsMap: IConstant = MARKETS;
  private readonly OPTA_ENV: string = environment.OPTA_SCOREBOARD.ENV;
  private view: string;
  private item: IMatrixFormation;
  private player: IFiveASidePlayer;
  private teamsColorObservable: Observable<ITeamColors>;
  private formation: string;
  private matrixFormation: IMatrixFormation[];
  private _localStorage: Storage;
  private journeyItems: IJourneyItems = {};

  private playerStats: {[key: string]: IYourcallStatisticItem[]} = {};

  constructor(
    private cmsService: CmsService,
    private yourcallProviderService: YourcallProviderService,
    private yourcallMarketsService: YourcallMarketsService,
    private coreToolsService: CoreToolsService,
    private domSanitizer: DomSanitizer
  ) {
    this._localStorage = localStorage;
  }

  get activeView(): string {
    return this.view;
  }

  get activeFormation(): string {
    return this.formation;
  }

  set activeFormation(value: string) {
    this.formation = value;
  }

  get activeItem(): IMatrixFormation {
    return this.item;
  }

  get activePlayer(): IFiveASidePlayer {
    return this.player;
  }

  set activeMatrixFormation(value: IMatrixFormation[]) {
    this.matrixFormation = value;
  }

  get activeMatrixFormation(): IMatrixFormation[] {
    return this.matrixFormation;
  }

  initTeamsColors(sportId: string, obEventId: number): Observable<ITeamColors> {
    if (this.activeObEventId === obEventId && Object.keys(this.teamsColors).length) {
      return of(this.teamsColors);
    }

    this.activeObEventId = obEventId;
    const teamNames = [this.game.homeTeam.title, this.game.visitingTeam.title];
    if (!this.teamsColorObservable) {
      this.teamsColorObservable = this.cmsService.getTeamsColors(teamNames, sportId).pipe(
        shareReplay(1),
        map((cmsTeams: ITeamColorsData[]) => {
          teamNames.forEach((teamName: string) => {
            const team = cmsTeams.find(el => el.teamName === teamName || el.secondaryNames && el.secondaryNames.includes(teamName));

            const primaryColour = team && team.primaryColour;
            const secondaryColour = team && team.secondaryColour;
            this.teamsColors[teamName] = {
              primaryColour: this.checkHexColor(primaryColour, DEFAULT_TEAM_COLOURS.primary),
              secondaryColour: this.checkHexColor(secondaryColour, DEFAULT_TEAM_COLOURS.secondary)
            };
          });
          return this.teamsColors;
        }),
        catchError((err) => {
          console.warn('Error', err);
          return of(this.teamsColors);
        })
      );
    }
    return this.teamsColorObservable;
  }

  getFormations(): Observable<IFormation[]> {
    this.game = this.yourcallMarketsService.game;
    return this.cmsService.getFiveASideFormations();
  }

  getPlayerList(obEventId: number, sportId: string): Observable<IFiveASidePlayers> {
    return forkJoin(this.getPlayers(obEventId), this.initTeamsColors(sportId, obEventId))
      .pipe(
        map((data: [IPlayerBets, ITeamColors]) => this.preapereOPTAInfo(data[0], obEventId)),
      );
  }

  sortPlayers(stat: string, team: string = 'allPlayers'): IFiveASidePlayer[] {
    return this.sortByProperties(this.playerList[team], [this.marketsMap[stat], 'name']);
  }

  showView(showView: IShowView, isEditMode: boolean = false): void {
    this.view = showView.view;
    this.isEditMode = isEditMode;

    if (showView.item) {
      this.item = showView.item;
    }

    if (showView.player) {
      this.player = showView.player;
    }
  }

  hideView(): void {
    this.view = '';
    this.player = undefined;
    this.item = undefined;
    this.isEditMode = false;
  }

  applyStatEdit(item: IMatrixFormation): void {
    if (this.item !== item) {
      this.item.stat = item.stat;
      this.item.statId = item.statId;
    }
  }

  saveDefaultStat(): void {
    if (!this.item.defaultStat) {
      this.item.defaultStat = { stat: this.item.stat, statId: this.item.statId };
    }
  }

  restoreDefaultStat(): void {
    const defaultStat = this.item.defaultStat;
    if (defaultStat) {
      this.item.stat = defaultStat.stat;
      this.item.statId = defaultStat.statId;
      this.item.defaultStat = null;
    }
  }

  loadPlayerStats(obEventId: number, playerId: number): Observable<IYourcallStatisticItem[]> {
    const key = `${obEventId}-${playerId}`;

    if (this.playerStats[key]) {
      return of(this.playerStats[key]);
    }

    return from(this.yourcallMarketsService.getStatisticsForPlayer({ obEventId: `${obEventId}`, playerId })).pipe(
      map((res: IYourcallStatisticResponse) => {
        const allData = res && res.allData;

        if (!allData) {
          throw new Error('No data');
        }

        const stats = allData
          .filter((item: IYourcallStatisticItem) => {
            return !PLAYER_STATS_EXCLUDE.includes(item.id);
          })
          .map((item: IYourcallStatisticItem) => {
            return { ...item, title: PLAYER_STATS_NAMES[item.id] || item.title };
          });

        stats.sort((a: IYourcallStatisticItem, b: IYourcallStatisticItem) => {
          if (a.title < b.title) { return -1; }
          if (a.title > b.title) { return 1; }
          return 0;
        });

        this.playerStats[key] = stats;

        return stats;
      })
    );
  }

  getPlayerStats(obEventId: number, playerId: number): IYourcallStatisticItem[] {
    return this.playerStats[`${obEventId}-${playerId}`];
  }

  /**
   * Return Observable of cached static blocks if or requests for journey static blocks
   */
  getJourneyStaticBlocks(): Observable<IJourneyItems> {
    return Object.keys(this.journeyItems).length ? of(this.journeyItems) : this.getFiveASideStaticBlocks();
  }

  /**
   * Requests journey static blocks and prepares
   */
  private getFiveASideStaticBlocks(): Observable<IJourneyItems> {
    return this.cmsService.getFiveASideStaticBlocks()
      .pipe(
        map((data: IJourneyStaticBlock[]) => {
          data.forEach((item: IJourneyStaticBlock) => {
            this.journeyItems[item.title] = {
              title: item.title,
              htmlMarkup: this.domSanitizer.bypassSecurityTrustHtml(item.htmlMarkup)
            };
          });
          return this.journeyItems;
        })
      );
  }

  private getPlayers(obEventId: number): Observable<IPlayerBets> {
    return fromPromise(this.yourcallProviderService.getPlayers(obEventId));
  }

  private preapereOPTAInfo(players: IPlayerBets, obEventId: number): IFiveASidePlayers {
    const banachPlayers: IPlayerBet[] = players.data;
    const opta = JSON.parse(this._localStorage.getItem(`scoreBoards_${this.OPTA_ENV}_prematch_${obEventId}`));

    const optaPlayers = opta && opta.data && opta.data.players;
    this.playerList = {
      home: [],
      away: [],
      allPlayers: []
    };

    banachPlayers.forEach((banachplayer: IPlayerBet) => {
      if (optaPlayers) {
        let homeOptaPlayer = optaPlayers.home.find((optaplayer: IConstant) => optaplayer.name.matchName === banachplayer.name);

        if (homeOptaPlayer) {
          homeOptaPlayer = this.parsePlayer(homeOptaPlayer, banachplayer);
          this.playerList.home.push(homeOptaPlayer);
          this.playerList.allPlayers.push(homeOptaPlayer);
          return;
        }

        let awayOptaPlayer = optaPlayers.away.find((optaplayer: IConstant) => optaplayer.name.matchName === banachplayer.name);

        if (awayOptaPlayer) {
          awayOptaPlayer = this.parsePlayer(awayOptaPlayer, banachplayer);
          this.playerList.away.push(awayOptaPlayer);
          this.playerList.allPlayers.push(awayOptaPlayer);
          return;
        }

        this.parsePlayerWithoutStats(banachplayer);
      } else {
        this.parsePlayerWithoutStats(banachplayer);
      }
    });

    return this.playerList;
  }

  private parsePlayerWithoutStats(banachplayer: IPlayerBet): void {
    const playerWithoutStats = this.parsePlayer({}, banachplayer);
    if (banachplayer.team.title === this.game.homeTeam.title) {
      this.playerList.home.push(playerWithoutStats);
    } else {
      this.playerList.away.push(playerWithoutStats);
    }
    this.playerList.allPlayers.push(playerWithoutStats);
  }

  private parsePlayer(optaPlayer: IConstant, banachplayer: IPlayerBet): IFiveASidePlayer {
    const goalKeeper = optaPlayer.goalKeeper;
    const passes = optaPlayer.passes;
    const attendance = optaPlayer.attendance;
    const isAppeared = attendance && attendance.appearances === 0;

    if (!this.optaStatisticsAvailable) {
      this.optaStatisticsAvailable = !!(attendance && attendance.appearances);
    }

    return {
      id: banachplayer.id,
      name: banachplayer.name,
      teamName: banachplayer.team.title,
      teamColors: this.teamsColors && this.teamsColors[banachplayer.team.title],
      appearances: attendance && attendance.appearances,
      cleanSheets: attendance && attendance.cleanSheets,
      tackles: optaPlayer.tackles && optaPlayer.tackles.avg,
      passes: passes && passes.avg,
      crosses: passes && passes.avg,
      assists: optaPlayer.assists && optaPlayer.assists.goal,
      ...this.getPlayerShots(optaPlayer) as IFiveASidePlayer,
      goals: optaPlayer.goals && optaPlayer.goals.scored,
      goalsInsideTheBox: optaPlayer.goals && optaPlayer.goals.scored,
      goalsOutsideTheBox: optaPlayer.goals && optaPlayer.goals.scored,
      ...this.getPlayerCards(optaPlayer) as IFiveASidePlayer,
      position: {
        long: optaPlayer.position,
        short: this.positionsMap[optaPlayer.position]
      },
      penaltySaves: goalKeeper && goalKeeper.penaltySaves,
      conceeded: goalKeeper && (isAppeared ? 0 : Math.round(goalKeeper.conceeded / attendance.appearances * 10) / 10),
      saves: goalKeeper && (isAppeared ? 0 : Math.round(goalKeeper.totalSaves / attendance.appearances * 10) / 10),
      isGK: this.isGK(banachplayer, optaPlayer)
    };
  }

  private getPlayerShots(optaPlayer: IConstant): IConstant {
    const shots = optaPlayer.shots;
    return {
      shots: shots && shots.avg,
      shotsOnTarget: shots && shots.avgOnTarget,
      shotsOutsideTheBox: shots && shots.avg,
    };
  }

  private getPlayerCards(optaPlayer: IConstant): IConstant {
    const cards = optaPlayer.cards;
    return {
      cards: cards && cards.red + cards.yellow,
      cardsRed: cards && cards.red,
      cardsYellow: cards && cards.yellow,
    };
  }

  /**
   * Sorts array of objects sequentially comparing object properties
   * Sorting works: first iteration descending, second: ascending, next: descending
   * @param items      {array}  array to sort
   * @param properties {array}  properties to compare
   * @return {array}
   */
  private sortByProperties(items: IFiveASidePlayer[], properties: string[]): IFiveASidePlayer[] {
    items.sort((a, b) => {
      for (let i = 0; i < properties.length; ++i) {
        const prop = properties[i];
        let aVal = this.coreToolsService.getOwnDeepProperty(a, prop);
        let bVal = this.coreToolsService.getOwnDeepProperty(b, prop);

        if (aVal === undefined) {
          aVal = -1;
        }

        if (bVal === undefined) {
          bVal = -1;
        }

        if (i === 0) {
          if (aVal < bVal) { return 1; }
          if (aVal > bVal) { return -1; }
        } else {
          if (aVal > bVal) { return 1; }
          if (aVal < bVal) { return -1; }
        }
      }
      return -1;
    });
    return items;
  }

  private checkHexColor(color: string, defaultColor: string): string {
    const pattern = new RegExp('^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$');
    return pattern.test(color) ? color : defaultColor;
  }

  private isGK(banachPlayer: IPlayerBet, optaPlayer: IConstant): boolean {
    return Boolean(
      (banachPlayer.position && banachPlayer.position.title === 'Goalkeeper') ||
      (optaPlayer.position === 'Goalkeeper')
    );
  }
}
