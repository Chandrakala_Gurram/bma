export interface IDsEventConfig {
  gameId: number;
  sportId: number;
  leagueId: number;
  status: number;
  homeTeam: { id: number; };
  visitingTeam: { id: number; };
}
