export interface IYourcallDashboardCustomEvent {
  type: number;
  conditionType: string;
  conditionValue: string;
  statisticId: number;
  game1Id: number;
  player1Id?: number;
}
