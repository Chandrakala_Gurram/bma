export interface IYourcallNormalizedSelection {
  betType: string;
  conditionType: string;
  conditionValue: string;
  statisticId: string;
  game1Id: string;
  player1Id: string;
}
