export interface IYourcallDashboardError {
  code: number;
  value: number;
  field: string;
  message: string;
}
