import { YourCallLeague } from '@yourcall/models/yourcall-league';

export interface IYourcallByBLeague {
  obTypeId: number;
  title: string;
  status?: number;
}

export interface IYourcallDsLeague {
  acronym: string;
  activeGames: number;
  id: number;
  isActive: number;
  isDefault: number;
  obTypeId: number;
  sportId: number;
  title: string;
}

export interface IYourcallLeaguesMap {
  [key: number]: number;
}

export interface ILeagueResponse {
  data: YourCallLeague[];
}
