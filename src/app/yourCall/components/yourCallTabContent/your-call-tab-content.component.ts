import { IYourcallPlayer } from './../../models/yourcall-api-response.model';
import { YourCallEvent } from '@yourcall/models/yourcall-event';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import * as _ from 'underscore';

import { YourcallDashboardService } from '@yourcall/services/yourcallDashboard/yourcall-dashboard.service';
import { ISportEvent } from '@core/models/sport-event.model';
import { YourcallMarketsService } from './../../services/yourCallMarketsService/yourcall-markets.service';
import { YourcallService } from '@yourCallModule/services/yourcallService/yourcall.service';
import { IYourCallGameTeam } from '@yourcall/models/game-data.model';

@Component({
  selector: 'yourcall-tab-content',
  templateUrl: './your-call-tab-content.component.html'
})
export class YourCallTabContentComponent implements OnInit, OnDestroy {

  @Input() eventEntity: ISportEvent;

  staticType: string;
  showIcon: boolean;
  isLoaded: boolean;
  isMarkets: boolean;
  switchers: {
    name: string;
    onClick: Function;
    provider: string;
    viewByFilters: number;
    icon?: boolean;
  }[];
  filter: number;
  showMarkets: boolean;
  expandCollapseMap: { [key: number]: boolean; } = {};

  constructor(
    private yourCallService: YourcallService,
    private yourCallMarketsService: YourcallMarketsService,
    private yourCallDashboardService: YourcallDashboardService
  ) { }

  ngOnInit(): void {
    this.staticType = 'yourcall-tab';
    this.showIcon = false;
    this.isLoaded = false;
    this.switchers = [];
    this.filter = null;

    this.yourCallDashboardService.clear();

    this.getMarkets();
  }

  /**
   * onDestroy controller function
   */
  ngOnDestroy(): void {
    this.yourCallMarketsService.clear(true);
  }

  trackByMarket(index: number, market): string {
    return `${index}${market.title}`;
  }

  /**
   * Expand markets by list
   * @param marketNamesArray
   */
  setExpandedMarkets(marketNamesArray: string[]): void {
    this.markets.forEach((market, index) => {
      this.expandCollapseMap[index] = marketNamesArray.indexOf(market.key) !== -1;
    });
    const marketCount = this.markets ? this.markets.length : 0;
    this.yourCallService.accordionsStateInit(marketCount);
  }

  /**
   * Expand markets by default value
   */
  setExpandedByDefaultMarkets(): void {
    let availableMarketIndex = 0;

    _.each(this.markets, (market, index) => {
      if (market.available) {
        availableMarketIndex++;
      }
      this.expandCollapseMap[index] = availableMarketIndex <= 2;
    });
    const marketCount = this.markets ? this.markets.length : 0;
    this.yourCallService.accordionsStateInit(marketCount);
  }

  /**
   * Get current Game/Event
   * @returns {*|string}
   */
  get game(): YourCallEvent {
    return this.yourCallMarketsService.game;
  }

  /**
   * Get teams
   * @returns {Array}
   */
  get teams(): IYourCallGameTeam[] {
    return [this.game.homeTeam, this.game.visitingTeam];
  }

  /**
   * Get markets
   * @returns {Array}
   */
  get markets(): any[] {
    return this.yourCallMarketsService.markets;
  }

  /**
   * Get players
   * @returns {Array}
   */
  get players(): IYourcallPlayer[] {
    return this.yourCallMarketsService.players;
  }

  /**
   * Set markets
   * @param markets {Array}
   */
  set markets(markets: any[]) {
    this.yourCallMarketsService.markets = markets;
  }

  expandCollapse(market: any, leagueIndex: number): void {
    this.expandCollapseMap[leagueIndex] = !this.expandCollapseMap[leagueIndex];
    this.yourCallService.sendToggleGTM(market, leagueIndex, this.expandCollapseMap[leagueIndex]);
  }

  private getMarkets(): void {
    this.isLoaded = false;
    this.yourCallMarketsService.getAgregatedMarketsData().then(() => {
      this.showMarkets = this.yourCallMarketsService.showMarkets();
      this.isLoaded = true;
      this.yourCallDashboardService.eventObj = {
        id: this.eventEntity.id,
        name: this.eventEntity.name
      };
      if (this.eventEntity && this.yourCallMarketsService.isAnyStoredBets(this.eventEntity.id)) {
        this.setExpandedMarkets(this.yourCallMarketsService.getStoredBetsMarketsNames(this.eventEntity.id));
      } else {
        this.setExpandedByDefaultMarkets();
      }
    })
    .catch(error => {
      console.warn(error);
    });
  }
}
