import { YourCallTabContentComponent } from '@yourcall/components/yourCallTabContent/your-call-tab-content.component';
import { fakeAsync, tick } from '@angular/core/testing';

describe('YourCallTabContentComponent', () => {
  let yourCallService;
  let yourCallMarketsService;
  let yourCallDashboardService;
  let component: YourCallTabContentComponent;

  beforeEach(() => {
    yourCallService = {
      sendToggleGTM: jasmine.createSpy('sendToggleGTM'),
      accordionsStateInit: jasmine.createSpy('accordionsStateInit')
    };

    yourCallMarketsService = {
      getAgregatedMarketsData: jasmine.createSpy('getAgregatedMarketsData').and.returnValue(Promise.resolve()),
      clear: jasmine.createSpy('clear'),
      showMarkets: jasmine.createSpy('showMarkets'),
      isAnyStoredBets: jasmine.createSpy('isAnyStoredBets').and.returnValue(true),
      getStoredBetsMarketsNames: jasmine.createSpy('getStoredBetsMarketsNames').and.returnValue(['market1']),
      game: {
        homeTeam: 'homeTeam',
        visitingTeam: 'visitingTeam'
      },
      players: [{}],
      markets: [{
        key: 'market1'
      }, {
        key: 'market3'
      }]
    };

    yourCallDashboardService = {
      clear: jasmine.createSpy('clear'),
      eventObj: {}
    };

    component = new YourCallTabContentComponent(
      yourCallService,
      yourCallMarketsService,
      yourCallDashboardService
    );

    component.eventEntity = {} as any;
  });

  it('#ngOnInit', () => {
    component.ngOnInit();

    expect(component.staticType).toEqual('yourcall-tab');
    expect(component.showIcon).toEqual(false);
    expect(component.isLoaded).toEqual(false);
    expect(component.switchers).toEqual([]);
    expect(component.filter).toEqual(null);
    expect(yourCallDashboardService.clear).toHaveBeenCalledTimes(1);
  });

  describe('#expandCollapse', () => {
    it('should set proper value for expandMap', () => {
      component.expandCollapse({}, 5);
      expect(component.expandCollapseMap[5]).toEqual(true);
      component.expandCollapse({}, 5);
      expect(component.expandCollapseMap[5]).toEqual(false);
    });

    it('should send toggle to GTM', () => {
      const market = {};
      component.expandCollapseMap[0] = false;
      component.expandCollapse(market, 0);

      expect(yourCallService.sendToggleGTM).toHaveBeenCalledWith(market, 0, true);
    });
  });

  describe('#ngOnDestroy', () => {
    it('should call ngOnDestroy method', () => {
      component.ngOnDestroy();

      expect(yourCallMarketsService.clear).toHaveBeenCalledWith(true);
    });
  });

  describe('#trackByMarket', () => {
    it('should call trackByMarket method', () => {
      const result = component.trackByMarket(1, {title: 'title'} as any);

      expect(result).toEqual('1title');
    });
  });

  describe('#setExpandedMarkets', () => {
    it('should call setExpandedMarkets method', () => {
      component.setExpandedMarkets(['market1', 'market2']);

      expect(yourCallService.accordionsStateInit).toHaveBeenCalledWith(2);
    });

    it('should call setExpandedMarkets method when no markets', () => {
      yourCallMarketsService.markets = [];
      component.setExpandedMarkets(['market1', 'market2']);

      expect(yourCallService.accordionsStateInit).toHaveBeenCalledWith(0);
    });
  });

  describe('#setExpandedByDefaultMarkets', () => {
    it('should call setExpandedByDefaultMarkets method', () => {
      component.setExpandedByDefaultMarkets();

      expect(yourCallService.accordionsStateInit).toHaveBeenCalledWith(2);
    });

    it('should call setExpandedByDefaultMarkets method with available market', () => {
      yourCallMarketsService.markets = [{
        available: true
      }] as any;
      component.setExpandedByDefaultMarkets();

      expect(yourCallService.accordionsStateInit).toHaveBeenCalledWith(1);
    });

    it('should call setExpandedByDefaultMarkets method when no markets', () => {
      yourCallMarketsService.markets = [];
      component.setExpandedByDefaultMarkets();

      expect(yourCallService.accordionsStateInit).toHaveBeenCalledWith(0);
    });
  });

  describe('#game', () => {
    it('should get game', () => {
      expect(component.game).toEqual({
        homeTeam: 'homeTeam',
        visitingTeam: 'visitingTeam'
      } as any);
    });
  });

  describe('#teams', () => {
    it('should get teams', () => {
      expect(component.teams).toEqual(['homeTeam', 'visitingTeam'] as any);
    });
  });

  describe('#players', () => {
    it('should get players', () => {
      expect(component.players).toEqual([{}] as any);
    });
  });

  describe('#markets', () => {
    it('should set markets', () => {
      expect(yourCallMarketsService.markets).toEqual([{
        key: 'market1'
      }, {
        key: 'market3'
      }]);

      component.markets = [{
        key: 'market1'
      }, {
        key: 'market4'
      }] as any;

      expect(yourCallMarketsService.markets).toEqual([{
        key: 'market1'
      }, {
        key: 'market4'
      }]);
    });
  });

  describe('#expandCollapse', () => {
    it('should call expandCollapse method', () => {
      component.expandCollapse({}, 2);

      expect(yourCallService.sendToggleGTM).toHaveBeenCalledWith({}, 2, true);
      expect(component.expandCollapseMap).toEqual({
        2: true
      });
    });
  });

  describe('#getMarkets', () => {
    it('should call getMarkets method', fakeAsync(() => {
      component.eventEntity = {id: '12345', name: 'Event Name'} as any;
      component['getMarkets']();

      tick();

      expect(yourCallMarketsService.getAgregatedMarketsData).toHaveBeenCalled();
      expect(yourCallMarketsService.showMarkets).toHaveBeenCalled();
      expect(yourCallMarketsService.isAnyStoredBets).toHaveBeenCalledWith('12345');
      expect(yourCallMarketsService.getStoredBetsMarketsNames).toHaveBeenCalledWith('12345');
      expect(yourCallDashboardService.eventObj).toEqual({ id: '12345', name: 'Event Name' });
    }));

    it('should call getMarkets method when no evententity', fakeAsync(() => {
      component.eventEntity = { id: '12345', name: 'Event Name' } as any;
      yourCallMarketsService.isAnyStoredBets.and.returnValue(false);
      component['getMarkets']();

      tick();

      expect(yourCallMarketsService.getAgregatedMarketsData).toHaveBeenCalled();
      expect(yourCallMarketsService.showMarkets).toHaveBeenCalled();
      expect(yourCallMarketsService.isAnyStoredBets).toHaveBeenCalledWith('12345');
      expect(yourCallMarketsService.getStoredBetsMarketsNames).not.toHaveBeenCalled();
      expect(yourCallDashboardService.eventObj).toEqual({ id: '12345', name: 'Event Name' });
    }));

    it('should call getMarkets method with error', fakeAsync(() => {
      yourCallMarketsService.getAgregatedMarketsData.and.returnValue(Promise.reject());
      component['getMarkets']();

      tick();

      expect(yourCallMarketsService.getAgregatedMarketsData).toHaveBeenCalled();
      expect(yourCallMarketsService.showMarkets).not.toHaveBeenCalled();
      expect(yourCallMarketsService.isAnyStoredBets).not.toHaveBeenCalled();
      expect(yourCallMarketsService.getStoredBetsMarketsNames).not.toHaveBeenCalled();
      expect(yourCallDashboardService.eventObj).toEqual({});
    }));
  });
});
