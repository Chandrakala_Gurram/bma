import { Component, Input, OnInit } from '@angular/core';

import { IFiveASidePlayer } from '@yourcall/services/fiveASide/five-a-side.model';
import { MARKETS, STATS_TITLES } from '@yourcall/constants/five-a-side.constant';
import { IMatrixFormation } from '@yourcall/models/five-a-side.model';
import { FiveASideService } from '@yourcall/services/fiveASide/five-a-side.service';
import { InfoDialogService } from '@coreModule/services/infoDialogService/info-dialog.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { ISportEvent } from '@core/models/sport-event.model';
import { IYourcallStatisticItem } from '@yourcall/models/yourcall-api-response.model';

@Component({
  selector: 'player-card',
  templateUrl: './player-card.component.html',
  styleUrls: ['./player-card.component.less']
})
export class PlayerCardComponent implements OnInit {
  @Input() player: IFiveASidePlayer;
  @Input() eventEntity: ISportEvent;

  item: IMatrixFormation;
  markets = MARKETS;
  statsTitles = STATS_TITLES;
  primaryColour: string;
  secondaryColour: string;
  statValue: number | string;
  unavailable: boolean;

  private value: number;

  constructor(
    private fiveASideService: FiveASideService,
    private infoDialogService: InfoDialogService,
    private localeService: LocaleService
  ) {}

  ngOnInit(): void {
    this.item = this.fiveASideService.activeItem;
    this.value = this.player[this.markets[this.item.stat]];
    this.statValue = (this.value === undefined || this.value === null) ? 'N/A' : this.value;
    this.primaryColour = this.player.teamColors.primaryColour;
    this.secondaryColour = this.player.teamColors.secondaryColour;

    this.setUnavailable();
  }

  showPlayerPage(): void {
    const { player, item } = this;
    this.fiveASideService.loadPlayerStats(this.eventEntity.id, player.id).subscribe((stats: IYourcallStatisticItem[]) => {
      if (stats.find((stat: IYourcallStatisticItem) => stat.id === item.statId)) {
        this.fiveASideService.showView({ view: 'player-page', player, item });
        return;
      }

      this.infoDialogService.openInfoDialog(
        this.localeService.getString('yourCall.cantSelectPlayer.caption'),
        this.localeService.getString('yourCall.cantSelectPlayer.text'),
        '', undefined, () => {
          this.unavailable = true;
        }, [{ caption: this.localeService.getString('yourCall.cantSelectPlayer.okBtn') }]
      );
    }, () => {
      this.infoDialogService.openInfoDialog(
        this.localeService.getString('yourCall.error'),
        this.localeService.getString('yourCall.serverError')
      );
    });
  }

  private setUnavailable(): void {
    const stats = this.fiveASideService.getPlayerStats(this.eventEntity.id, this.player.id);
    this.unavailable = stats && !stats.find((stat: IYourcallStatisticItem) => stat.id === this.item.statId);
  }
}
