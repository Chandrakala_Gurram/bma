import { of, throwError } from 'rxjs';

import { PlayerCardComponent } from './player-card.component';

describe('PlayerCardComponent', () => {
  let component: PlayerCardComponent;
  let fiveASideService;
  let infoDialogService;
  let localeService;
  let item;
  let player;
  let eventEntity;

  beforeEach(() => {
    item = {
      rowIndex: 1,
      collIndex: 1,
      position: 'Position',
      stat: 'Passes',
      statId: 1,
      roleId: '1'
    };

    player = {
      name: 'Player name',
      passes: 10,
      teamColors: {
        primaryColour: 'primaryColour',
        secondaryColour: 'secondaryColour'
      }
    };

    eventEntity = {
      id: 101
    };

    fiveASideService = {
      showView: jasmine.createSpy('showView'),
      activeItem : {
        rowIndex: 1,
        collIndex: 1,
        position: 'Position',
        stat: 'Passes',
        statId: 1,
        roleId: '1'
      },
      loadPlayerStats: jasmine.createSpy('loadPlayerStats').and.returnValue(of([])),
      getPlayerStats: jasmine.createSpy('getPlayerStats')
    };
    infoDialogService = {
      openInfoDialog: jasmine.createSpy('openInfoDialog')
    };
    localeService = {
      getString: jasmine.createSpy('getString')
    };

    component = new PlayerCardComponent(fiveASideService, infoDialogService, localeService);
    component.player = player;
    component.eventEntity = eventEntity;
  });

  it('should create PlayerCardComponent', () => {
    expect(component).toBeTruthy();
  });

  describe('#ngOnInit', () => {
    it('#ngOnInit', () => {
      component.ngOnInit();

      expect(component.item).toEqual(item);
      expect(component.statValue).toEqual(10);
      expect(component['value']).toEqual(10);
      expect(component.primaryColour).toEqual('primaryColour');
      expect(component.secondaryColour).toEqual('secondaryColour');
    });

    it('#ngOnInit stat value undefinded', () => {
      player.passes = undefined;
      component.ngOnInit();

      expect(component.item).toEqual(item);
      expect(component.statValue).toEqual('N/A');
      expect(component['value']).toEqual(undefined);
      expect(component.primaryColour).toEqual('primaryColour');
      expect(component.secondaryColour).toEqual('secondaryColour');
    });

    it('#ngOnInit stat value null', () => {
      player.passes = null;
      component.ngOnInit();

      expect(component.statValue).toEqual('N/A');
      expect(component['value']).toEqual(null);
    });
  });

  describe('showPlayerPage', () => {
    beforeEach(() => {
      component.item = item;
    });

    it('should show player page', () => {
      fiveASideService.loadPlayerStats.and.returnValue(of([{ id: item.statId }]));
      component.showPlayerPage();
      expect(fiveASideService.loadPlayerStats).toHaveBeenCalledWith(eventEntity.id, player.id);
      expect(fiveASideService.showView).toHaveBeenCalledWith({ view: 'player-page', player, item });
    });

    it('should not show player page', () => {
      infoDialogService.openInfoDialog.and.callFake((...params) => params[4]());
      component.showPlayerPage();
      expect(fiveASideService.loadPlayerStats).toHaveBeenCalledWith(eventEntity.id, player.id);
      expect(fiveASideService.showView).not.toHaveBeenCalled();
      expect(infoDialogService.openInfoDialog).toHaveBeenCalled();
      expect(localeService.getString).toHaveBeenCalledWith('yourCall.cantSelectPlayer.caption');
      expect(localeService.getString).toHaveBeenCalledWith('yourCall.cantSelectPlayer.text');
      expect(localeService.getString).toHaveBeenCalledWith('yourCall.cantSelectPlayer.okBtn');
    });

    it('should handle error', () => {
      fiveASideService.loadPlayerStats.and.returnValue(throwError('error'));
      component.showPlayerPage();
      expect(infoDialogService.openInfoDialog).toHaveBeenCalled();
      expect(localeService.getString).toHaveBeenCalledWith('yourCall.error');
      expect(localeService.getString).toHaveBeenCalledWith('yourCall.serverError');
    });
  });

  describe('setUnavailable', () => {
    beforeEach(() => {
      component.item = item;
    });

    it('should set false', () => {
      component['setUnavailable']();
      expect(component.unavailable).toBeFalsy();
    });

    it('should set true', () => {
      fiveASideService.getPlayerStats.and.returnValue([99]);
      component['setUnavailable']();
      expect(component.unavailable).toBeTruthy();
    });
  });
});
