import { FiveASidePlayerListComponent } from '@yourcall/components/fiveASidePlayerList/five-a-side-player-list.component';

describe('FiveASidePlayerListComponent', () => {
  let component: FiveASidePlayerListComponent;

  let fiveASideService: any;

  beforeEach(() => {
    fiveASideService = {
      sortPlayers: jasmine.createSpy('sortPlayers').and.returnValue([]),
      hideView: jasmine.createSpy(),
      activeItem:  {
        stat: 'goals'
      }
    };

    component = new FiveASidePlayerListComponent(fiveASideService);
    component.item = {
      stat: 'goals'
    } as any;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('trackById should return unique identifier for player', () => {
    expect(component.trackById(0, { id: 159 } as any)).toEqual('0_159');
  });

  describe('#ngOnInit', () => {
    it('should call ngOnInit', () => {
      component.item = undefined;
      component.ngOnInit();

      expect(component.playersList).toEqual([]);
      expect(fiveASideService.sortPlayers).toHaveBeenCalledWith('goals');
    });

    it('should call ngOnInit for goalkeeper markets', () => {
      fiveASideService.activeItem.stat = 'To Keep A Clean Sheet';
      fiveASideService.sortPlayers.and.returnValue([{ isGK: false }, { isGK: true }]);
      component.item = undefined;
      component.ngOnInit();

      expect(component.playersList).toEqual([{ isGK: true }] as any);
      expect(fiveASideService.sortPlayers).toHaveBeenCalledWith('To Keep A Clean Sheet');
    });
  });

  describe('#handleTabClick', () => {
    it('should call handleTabClick', () => {
      component.handleTabClick('home');

      expect(fiveASideService.sortPlayers).toHaveBeenCalledWith('goals', 'home');
      expect(component.filter).toEqual('home');
    });
  });

  describe('#hide', () => {
    it('should call hide', () => {
      component.hide();

      expect(fiveASideService.hideView).toHaveBeenCalled();
    });
  });

  describe('#formSwitchers', () => {
    it('should call formSwitchers', () => {
      component['formSwitchers']();
      component.switchers.forEach(switcher => {
        switcher.onClick();
      });

      expect(fiveASideService.sortPlayers).toHaveBeenCalledTimes(3);
    });
  });
});
