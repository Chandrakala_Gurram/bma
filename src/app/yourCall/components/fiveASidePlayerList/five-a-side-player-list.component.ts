import { Component, Input, OnInit } from '@angular/core';
import { ISportEvent } from '@core/models/sport-event.model';

import { IFiveASidePlayer } from '@yourcall/services/fiveASide/five-a-side.model';
import { FiveASideService } from '@yourcall/services/fiveASide/five-a-side.service';
import { IMatrixFormation } from '@yourcall/models/five-a-side.model';
import { ISwitcherConfig } from '@core/models/switcher-config.model';
import { GOALKEEPER_MARKETS } from '../../constants/five-a-side.constant';

@Component({
  selector: 'five-a-side-player-list',
  templateUrl: './five-a-side-player-list.component.html',
  styleUrls: ['./five-a-side-player-list.component.less']
})
export class FiveASidePlayerListComponent implements OnInit {
  @Input() eventEntity: ISportEvent;

  switchers: ISwitcherConfig[];
  item: IMatrixFormation;
  filter = 'allPlayers';
  playersList: IFiveASidePlayer[];
  isGKmarket: boolean;
  optaStatisticsAvailable: boolean;

  constructor(private fiveASideService: FiveASideService) {}

  ngOnInit(): void {
    this.optaStatisticsAvailable = this.fiveASideService.optaStatisticsAvailable;
    this.item = this.fiveASideService.activeItem;
    this.isGKmarket = GOALKEEPER_MARKETS.includes(this.item.stat);
    this.playersList = this.fiveASideService.sortPlayers(this.item.stat);
    if (this.isGKmarket) {
      this.playersList = this.playersList.filter((player: IFiveASidePlayer) => player.isGK);
    } else {
      this.formSwitchers();
    }
  }

  trackById(index: number, player: IFiveASidePlayer): string {
    return `${index}_${player.id}`;
  }

  hide(): void {
    this.fiveASideService.hideView();
  }

  handleTabClick(filterBy: string): void {
    this.filter = filterBy;
    this.playersList = this.fiveASideService.sortPlayers(this.item.stat, filterBy);
  }

  private formSwitchers(): void {
    this.switchers = [{
      name: 'All Players',
      viewByFilters: 'allPlayers',
      onClick: (filterBy: string) => this.handleTabClick(filterBy)
    }, {
      name: 'Home',
      viewByFilters: 'home',
      onClick: (filterBy: string) => this.handleTabClick(filterBy)
    }, {
      name: 'Away',
      viewByFilters: 'away',
      onClick: (filterBy: string) => this.handleTabClick(filterBy)
    }];
  }
}
