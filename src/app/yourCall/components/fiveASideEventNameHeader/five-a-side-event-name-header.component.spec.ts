import { FiveASideEventNameHeaderComponent } from './five-a-side-event-name-header.component';
import { of } from 'rxjs';

describe('FiveASideEventNameHeaderComponent', () => {
  let component: FiveASideEventNameHeaderComponent;
  let fiveASideService;
  let changeDetectorRef;

  const teams = {
    ARSENAL: {
      primaryColour: '#EF0107',
      secondaryColour: '#E8E8E8'
    },
    LIVERPOOL: {
      primaryColour: '#C8102E',
      secondaryColour: '#A60D26'
    }
  };

  beforeEach(() => {
      fiveASideService = {
        initTeamsColors: jasmine.createSpy('initTeamsColors').and.returnValue(of(teams)),
        teamsColors: teams
      }
      ;
      changeDetectorRef = {
        markForCheck: jasmine.createSpy('markForCheck')
      };
      component = new FiveASideEventNameHeaderComponent(
        fiveASideService,
        changeDetectorRef
      );
    }
  );

  it('should create FiveASideEventNameHeaderComponent', () => {
    expect(component).toBeTruthy();
  });

  describe('#ngOnInit', () => {
    const expectedResult = [
      {teamName: 'ARSENAL' , colors: {primaryColour: '#EF0107', secondaryColour: '#E8E8E8'}},
      {teamName: 'LIVERPOOL', colors: {primaryColour: '#C8102E', secondaryColour: '#A60D26'}}];
    it('should get team names and colors', () => {
      component.ngOnInit();
      expect(component.teamsColors).toEqual(expectedResult);
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    });
  });
});
