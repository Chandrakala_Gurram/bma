import { Component, Input, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FiveASideService } from '@yourcall/services/fiveASide/five-a-side.service';
import { ITeamColours } from '@yourcall/services/fiveASide/five-a-side.model';


@Component({
  selector: 'five-a-side-event-name-header',
  templateUrl: './five-a-side-event-name-header.component.html',
  styleUrls: ['./five-a-side-event-name-header.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FiveASideEventNameHeaderComponent implements OnInit {
  @Input() eventId: number;
  @Input() sportId: string;

  teamsColors: ITeamColours[] = [];
  noEntityName: string = 'No entity name';

  constructor(
    private fiveASideService: FiveASideService,
    private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.fiveASideService.initTeamsColors(this.sportId, this.eventId).subscribe(() => {
      const colors = Object.entries(this.fiveASideService.teamsColors);
      colors.forEach(([key, value]) => {
        this.teamsColors.push({teamName: key, colors: value});
      });
      this.changeDetectorRef.markForCheck();
    });
  }
}
