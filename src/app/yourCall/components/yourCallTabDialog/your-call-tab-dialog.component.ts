import { Component, ViewChild } from '@angular/core';

import { DeviceService } from '@core/services/device/device.service';
import { AbstractDialog } from '@shared/components/oxygenDialogs/abstract-dialog';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

@Component({
  selector: 'your-call-dialog',
  templateUrl: './your-call-tab-dialog.component.html'
})
export class YourCallTabDialogComponent extends AbstractDialog {

  @ViewChild('yourCallTabDialog') dialog;

  constructor(
    device: DeviceService, windowRef: WindowRefService
  ) {
    super(device, windowRef);
  }
}
