import {
  Component,
  EventEmitter,
  Input,
  Output,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';

import { ISportEvent } from '@core/models/sport-event.model';
import { IMatrixFormation } from '@yourcall/models/five-a-side.model';
import { IYourcallStatisticItem } from '@yourcall/models/yourcall-api-response.model';
import { IFiveASidePlayer } from '@yourcall/services/fiveASide/five-a-side.model';
import { FiveASideService } from '@yourcall/services/fiveASide/five-a-side.service';

@Component({
  selector: 'stats-drop-down',
  templateUrl: './stats-drop-down.component.html',
  styleUrls: ['./stats-drop-down.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class StatsDropDownComponent implements OnInit {
  @Input() eventEntity: ISportEvent;
  @Input() formation: IMatrixFormation;
  @Input() player: IFiveASidePlayer;

  @Output() readonly statChange: EventEmitter<IYourcallStatisticItem> = new EventEmitter();

  showMenu: boolean = false;
  stats: IYourcallStatisticItem[] = [];

  constructor(private fiveASideService: FiveASideService) {

  }

  ngOnInit(): void {
    this.stats = this.fiveASideService.getPlayerStats(this.eventEntity.id, this.player.id);
  }

  trackByFn(index: number, item: IYourcallStatisticItem): number {
    return item.id;
  }

  clickItem(item: IYourcallStatisticItem): void {
    if (item.id !== this.formation.statId) {
      this.statChange.emit(item);
    }

    this.menuToggle();
  }

  menuToggle(show?: boolean): void {
    this.showMenu = show !== undefined ? show : !this.showMenu;
  }

  clickHandler(event: Event): void {
    if (!event.cancelable) { return; }

    event.preventDefault();
    this.menuToggle(false);
  }
}
