import { StatsDropDownComponent } from './stats-drop-down.component';

describe('StatDropDownComponent', () => {
  let component: StatsDropDownComponent;
  let fiveASideService;

  beforeEach(() => {
    fiveASideService = {
      getPlayerStats: jasmine.createSpy('getPlayerStats').and.returnValue([])
    };

    component = new StatsDropDownComponent(fiveASideService);
    component.eventEntity = { id: 1 } as any;
    component.player = { id: 2 } as any;
    component.formation = { statId: 3, stat: 'Stat3' } as any;
  });

  it('ngOnInit', () => {
    component.ngOnInit();
    expect(fiveASideService.getPlayerStats)
      .toHaveBeenCalledWith(component.eventEntity.id, component.player.id);
  });

  it('trackByFn', () => {
    expect(component.trackByFn(0, { id: 1 } as any)).toBe(1);
  });

  describe('clickItem', () => {
    beforeEach(() => {
      spyOn(component.statChange, 'emit');
      spyOn(component, 'menuToggle');
    });

    it('should emit item', () => {
      component.clickItem({ id: 4 } as any);
      expect(component.statChange.emit).toHaveBeenCalledWith({ id: 4 });
    });

    it('should not emit item', () => {
      component.clickItem({ id: 3 } as any);
      expect(component.statChange.emit).not.toHaveBeenCalled();
    });
  });

  describe('menuToggle', () => {
    it('should show menu', () => {
      component.menuToggle();
      expect(component.showMenu).toBeTruthy();
    });

    it('should hide menu', () => {
      component.menuToggle(false);
      expect(component.showMenu).toBeFalsy();
    });
  });

  describe('clickHandler', () => {
    let clickEvent;
    beforeEach(() => {
      clickEvent = { preventDefault: jasmine.createSpy('preventDefault') };
    });

    it('should not prevent default', () => {
      component.clickHandler(clickEvent);
      expect(clickEvent.preventDefault).not.toHaveBeenCalled();
    });

    it('should prevent event and close menu', () => {
      spyOn(component, 'menuToggle');
      clickEvent.cancelable = true;
      component.clickHandler(clickEvent);
      expect(clickEvent.preventDefault).toHaveBeenCalled();
      expect(component.menuToggle).toHaveBeenCalledWith(false);
    });
  });
});
