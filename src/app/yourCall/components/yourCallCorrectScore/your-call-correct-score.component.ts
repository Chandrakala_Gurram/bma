import { IYourcallSelection } from './../../models/selection.model';
import { Component, OnChanges, Input } from '@angular/core';
import * as _ from 'underscore';

import { YourcallMarketsService } from '../../services/yourCallMarketsService/yourcall-markets.service';
import { YourCallMarketGroupItem } from '@yourcall/models/markets/yourcall-market-group-item';
import { IYourcallBYBEventResponse } from '@yourcall/models/byb-events-response.model';

@Component({
  selector: 'yourcall-correct-score',
  templateUrl: './your-call-correct-score.component.html'
})
export class YourCallCorrectScoreComponent implements OnChanges {

  @Input() market: YourCallMarketGroupItem;
  @Input() game: IYourcallBYBEventResponse;

  groupedMarkets: IYourcallSelection[][];
  filterValueType: string;
  allShown: boolean;
  selectedValueHome: number;
  selectedValueAway: number;
  scoreHome: string[];
  scoreAway: string[];

  constructor(
    private yourCallMarketsService: YourcallMarketsService
  ) { }

  ngOnChanges(): void {
    // set default values
    this.groupMarkets();
    this.getSelectionValues('scoreHome', 'bettingValue1', 0, 'selectedValueHome');
    this.getSelectionValues('scoreAway', 'bettingValue2', 2, 'selectedValueAway');
    this.onScoreChange();
    if (this.yourCallMarketsService.isRestoredNeeded(this.market.key)) {
      this.yourCallMarketsService.restoreBet(this.market);
    }
  }

  /**
   * Toggle showing all outcomes or selects only
   */
  toggleShow(): void {
    if (this.filterValueType === 'all') {
      this.allShown = false;
      this.filterValueType = 'main';
    } else {
      this.allShown = true;
      this.filterValueType = 'all';
    }
  }

  /**
   * Group Markets , filter out Ininity value
   * @private
   */
  groupMarkets(): void {
    const data = _.groupBy(_.reject((this.market.selections as IYourcallSelection[]), (sel: IYourcallSelection) => {
      return sel.odds === 'Infinity';
    }), 'relatedTeamType');
    this.groupedMarkets = [data[1], data[0], data[2]];
  }

  /**
   * Get possible scores for the match
   * @param item
   * @returns {string}
   */
  getScores(item: IYourcallSelection): string {
    return (item.relatedTeamType === 1) ? `${Math.floor(Number(item.bettingValue1))} - ${Math.floor(Number(item.bettingValue2))}`
      : `${Math.floor(Number(item.bettingValue2))} - ${Math.floor(Number(item.bettingValue1))}`;
  }

  /**
   * Action performed on button click
   * @param value {object}
   */
  selectValue(value: IYourcallSelection): void {
    this.yourCallMarketsService.selectValue(this.market, value);
    this.resetDropdown();
  }

  /**
   * Check if value is selected
   * @param value {object}
   * @returns {boolean}
   */
  isSelected(value: IYourcallSelection): boolean {
    return this.yourCallMarketsService.isSelected(this.market, value);
  }

  /**
   * React on score change
   */
  onScoreChange(): void {
    this.getSelectedObj() || this.checkForNull();
  }

  /**
   * Add selection to dashboard
   */
  addToDashboard(): void {
    const selected = this.getSelectedObj() || this.checkForNull();

    if (selected) {
      this.selectValue(selected);
    }
    this.resetDropdown();
  }

  trackByMarket(index: number, market: IYourcallSelection[]): string {
    return `${index}${market.map((s: IYourcallSelection) => `${s.odds}${s.id}${s.title}`).join('-')}`;
  }

  trackBySelection(index: number, market: IYourcallSelection): string {
    return `${index}${market.title}${market.id}${market.displayOrder}`;
  }

  /**
   * Set dropdowns to default values
   * @private
   */
  private resetDropdown(): void {
    this.selectedValueHome = 0;
    this.selectedValueAway = 0;
  }

  /**
   * Find selection
   * @private
   */
  private getSelectedObj(): IYourcallSelection {
    return _.find((this.market.selections as IYourcallSelection[]), (el: IYourcallSelection) => {
      return (Number(el.bettingValue1) === Number(this.selectedValueHome)) &&
        (Number(el.bettingValue2) === Number(this.selectedValueAway));
    });
  }

  /**
   * Null is returned for 90min period in case of Draw 0-0
   * @returns {object|undefined}
   * @private
   */
  private checkForNull(): IYourcallSelection {
    return (!this.selectedValueHome && !this.selectedValueAway)
      ? _.findWhere((this.market.selections as IYourcallSelection[]), { bettingValue2: null }) : undefined;
  }

  /**
   * Get values for team for selections
   * @param team
   * @param type
   * @param index
   * @private
   */
  private getSelectionValues(team: string, type: string, index: number, selectedValue: string) {
    this[team] = _.sortBy(_.uniq(_.map(_.pluck(this.groupedMarkets[index], type), Number)));
    this[team].unshift(0);

    this[selectedValue] = this[team][0];
  }
}
