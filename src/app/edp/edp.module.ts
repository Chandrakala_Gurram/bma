import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from '@sharedModule/shared.module';

// Components
import { BetGeniusScoreboardComponent } from './components/betGeniusScoreboard/bet-genius-scoreboard.component';
import { EventTitleBarComponent } from './components/eventTitleBar/event-title-bar.component';
import { FallbackScoreboardComponent } from '@edp/components/fallbackScoreboard/fallback-scoreboard.component';
import { OptaScoreboardComponent } from '@app/edp/directives/opta-scoreboard.component';
import { ScoreboardComponent } from './components/scoreboard/scoreboard.component';
import { SportEventMainComponent } from '@app/edp/components/sportEventMain/sport-event-main.component';
import { SportEventPageComponent } from '@app/edp/components/sportEventPage/sport-event-page.component';
import { MarketsGroupComponent } from '@edp/components/marketsGroup/markets-group.component';
import { CorrectScoreComponent } from '@edp/components/markets/correctScore/correct-score.component';
import { ScorecastComponent } from '@edp/components/markets/scorecast/scorecast.component';
import { YourCallPlayerStatsComponent } from '@edp/components/markets/playerStats/your-call-player-stats.component';
import { EdpSurfaceBetsCarouselComponent } from '@edp/components/surfaceBetsCarousel/surface-bets-carousel.component';

// Services
import { SportEventPageProviderService } from '@app/edp/components/sportEventPage/sport-event-page-provider.service';
import { SportEventMainProviderService } from '@app/edp/components/sportEventMain/sport-event-main-provider.service';
import { FootballExtensionService } from './services/footballExtension/football-extension.service';
import { TennisExtensionService } from './services/tennisExtension/tennis-extension.service';
import { CorrectScoreService } from '@edp/components/markets/correctScore/correct-score.service';
import { YourCallPlayerStatsGTMService } from '@edp/components/markets/playerStats/your-call-player-stats-grm.service';
import { ScorecastService } from '@edp/components/markets/scorecast/scorecast.service';
import { MarketsGroupService } from '@edp/services/marketsGroup/markets-group.service';
import { ScoreMarketService } from '@edp/services/scoreMarket/score-market.service';
import { EventMarketsComponent } from '@edp/components/eventMarkets/event-markets.component';

@NgModule({
  imports: [
    SharedModule
  ],
  providers: [
    SportEventPageProviderService,
    SportEventMainProviderService,
    FootballExtensionService,
    TennisExtensionService,
    CorrectScoreService,
    YourCallPlayerStatsGTMService,
    ScorecastService,
    ScoreMarketService,
    MarketsGroupService
  ],
  declarations: [
    EventMarketsComponent,
    BetGeniusScoreboardComponent,
    EventTitleBarComponent,
    FallbackScoreboardComponent,
    OptaScoreboardComponent,
    ScoreboardComponent,
    SportEventMainComponent,
    SportEventPageComponent,
    MarketsGroupComponent,
    CorrectScoreComponent,
    ScorecastComponent,
    EdpSurfaceBetsCarouselComponent,
    YourCallPlayerStatsComponent
  ],
  entryComponents: [
    EventMarketsComponent,
    BetGeniusScoreboardComponent,
    EventTitleBarComponent,
    FallbackScoreboardComponent,
    OptaScoreboardComponent,
    ScoreboardComponent,
    SportEventMainComponent,
    SportEventPageComponent,
    MarketsGroupComponent,
    CorrectScoreComponent,
    ScorecastComponent,
    EdpSurfaceBetsCarouselComponent,
    YourCallPlayerStatsComponent
  ],
  exports: [
    EventMarketsComponent,
    BetGeniusScoreboardComponent,
    EventTitleBarComponent,
    FallbackScoreboardComponent,
    OptaScoreboardComponent,
    ScoreboardComponent,
    SportEventMainComponent,
    EdpSurfaceBetsCarouselComponent,
    SportEventPageComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class EdpModule {
  static entry = SportEventMainComponent;
}
