import { Component, Input, OnInit } from '@angular/core';
import * as _ from 'underscore';

import { ITeams, ITeamsScores } from '@core/models/team.model';
import { IMarket } from '@core/models/market.model';
import { CorrectScoreService } from './correct-score.service';
import { IOutcome } from '@core/models/outcome.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { FiltersService } from '@core/services/filters/filters.service';

@Component({
  selector: 'correct-score',
  templateUrl: 'correct-score.component.html'
})
export class CorrectScoreComponent implements OnInit {
  @Input() market: IMarket;
  @Input() eventEntity: ISportEvent;
  @Input() isExpanded: boolean;
  @Input() memoryId: number;
  @Input() memoryLocation: string;
  @Input() marketGroup: IMarket[];
  combinedOutcome: {outcome: IOutcome};
  allShown: boolean;
  filterValueType: string;
  teamsObg: {teams: ITeams};
  teamHScores: number[];
  teamAScores: number[];
  groupedOutcomes: IOutcome[][];

  constructor(private correctScoreService: CorrectScoreService, private filterService: FiltersService) {}

  /**
   * Initial operations
   */
  ngOnInit(): void {
    this.filterValueType = 'main';
    this.allShown = false;
    this.market.outcomes = this.market.outcomes.filter((outcome: IOutcome) => outcome.outcomeMeaningScores);
    this.teamsObg = { teams: this.correctScoreService.getTeams(this.marketGroup) };
    this.teamsObg.teams.teamH.score = this.getMaxValues().teamH[0];
    this.teamsObg.teams.teamA.score = this.getMaxValues().teamA[0];

    this.onScoreChange();
    this.teamHScores = this.getMaxValues().teamH;
    this.teamAScores = this.getMaxValues().teamA;
    this.groupedOutcomes = _.reject(_.map(this.filterService.groupBy(this.market.outcomes, 'outcomeMeaningMinorCode'),
      (value: IOutcome[]) => value), (col: IOutcome[]) => !col.length);
  }

  /**
   * ngFor trackBy function
   * @param {number} index
   * @return {number}
   */
  trackByIndex(index: number): number {
    return index;
  }

  filteredName(name: string): string {
    return this.filterService.getScoreFromName(name);
  }

  /**
   * Get options for score select
   * @return {object} team scores
   */
  getMaxValues(): ITeamsScores {
    return this.correctScoreService.getMaxScoreValues(this.market.outcomes);
  }
  /**
   * Assign outcome, based on scores
   */
  onScoreChange(value?: number, team?: string): void {
    if (value && team) {
      this.teamsObg.teams[team].score = value;
    }

    this.combinedOutcome = {
      outcome: this.correctScoreService.getCombinedOutcome(this.teamsObg.teams, this.market.outcomes, this.eventEntity, this.market)
    };
  }

  public changeAccordionState(accordionState: boolean): void {
    this.isExpanded = accordionState;
  }
  /**
   * Toggle showing all outcomes or selects only
   */
  toggleShow(): void {
    if (this.filterValueType === 'all') {
      this.allShown = false;
      this.filterValueType = 'main';
    } else {
      this.allShown = true;
      this.filterValueType = 'all';
    }
  }

  /**
   * Check if bet button should be disabled
   * @returns {Boolean}
   */
  isButtonDisabled(): boolean {
    const outcome = this.combinedOutcome.outcome;
    return !(_.has(outcome, 'prices') && outcome.prices.length) ||
      outcome.outcomeStatusCode === 'S' || this.isMarketOrEventSuspended();
  }

  /**
   * Check if event or market is suspended
   * @returns {Boolean}
   */
  isMarketOrEventSuspended(): boolean {
    return this.eventEntity.eventStatusCode === 'S' || this.market.marketStatusCode === 'S';
  }
}
