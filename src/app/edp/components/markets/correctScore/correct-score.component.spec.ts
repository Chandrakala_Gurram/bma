import { CorrectScoreComponent } from '@edp/components/markets/correctScore/correct-score.component';

describe('CorrectScoreComponent', () => {
  let component: CorrectScoreComponent;

  let correctScoreService;
  let filterService;

  const outcome = {
    id: '123456',
    name: 'Outcome name'
  };
  const teamsMock = {
    teamA: {
      name: 'teamA name',
      score: 3
    },
    teamH: {
      name: 'teamH name',
      score: 5
    }
  };

  beforeEach(() => {
    correctScoreService = {
      getCombinedOutcome: jasmine.createSpy('getCombinedOutcome').and.returnValue(outcome),
      getMaxScoreValues: jasmine.createSpy('getMaxScoreValues'),
      getTeams: jasmine.createSpy('getTeams').and.returnValue(teamsMock),
    };
    filterService = {
      getScoreFromName: jasmine.createSpy('getScoreFromName'),
      groupBy: jasmine.createSpy('groupBy').and.returnValue([])
    };

    component = new CorrectScoreComponent(
      correctScoreService,
      filterService
    );
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  describe('isMarketOrEventSuspended', () => {
    beforeEach(() => {
      component.eventEntity = {
        eventStatusCode: ''
      } as any;
      component.market = {
        marketStatusCode: ''
      } as any;
    });

    it('to be true for not S status code', () => {
      expect(component.isMarketOrEventSuspended()).toEqual(false);
    });

    it('to be true for eventEntity eventStatusCode S', () => {
      component.eventEntity.eventStatusCode = 'S';
      expect(component.isMarketOrEventSuspended()).toEqual(true);
    });

    it('to be true for market marketStatusCode S', () => {
      component.market.marketStatusCode = 'S';
      expect(component.isMarketOrEventSuspended()).toEqual(true);
    });
  });

  describe('isButtonDisabled', () => {
    beforeEach(() => {
      component.isMarketOrEventSuspended = jasmine.createSpy().and.returnValue(false);
      component.combinedOutcome = {
        outcome: {
          outcomeStatusCode: 'any',
          prices: [{}]
        }
      } as any;
    });

    it('to be falsy', () => {
      expect(component.isButtonDisabled()).toBeFalsy();
    });

    it('to be truthy if isMarketOrEventSuspended', () => {
      component.isMarketOrEventSuspended = jasmine.createSpy().and.returnValue(true);
      expect(component.isButtonDisabled()).toBeTruthy();
    });

    it('to be truthy if for outcomeStatusCode S', () => {
      component.combinedOutcome.outcome.outcomeStatusCode = 'S';
      expect(component.isButtonDisabled()).toBeTruthy();
    });

    it('to be truthy if no outcome.prices', () => {
      delete (component.combinedOutcome.outcome.prices);
      expect(component.isButtonDisabled()).toBeTruthy();
    });

    it('to be truthy if outcome.prices empty', () => {
      component.combinedOutcome.outcome.prices = [];
      expect(component.isButtonDisabled()).toBeTruthy();
    });
  });

  describe('toggleShow', () => {
    beforeEach(() => {
      component.allShown = undefined;
    });

    it('should toggle allShown to false', () => {
      component.filterValueType = 'all';
      component.toggleShow();

      expect(component.filterValueType).toEqual('main');
      expect(component.allShown).toEqual(false);
    });

    it('should toggle allShown to true', () => {
      component.filterValueType = 'main';
      component.toggleShow();

      expect(component.filterValueType).toEqual('all');
      expect(component.allShown).toEqual(true);
    });
  });

  describe('onScoreChange', () => {
    let teamsObg;
    let market;
    let eventEntity;

    beforeEach(() => {
      teamsObg = {
        teams: {
          teamA: {}
        }
      };
      market = {
        outcomes: [{
          id: '123'
        }]
      };
      eventEntity = {
        id: 56789
      };

      component.teamsObg = teamsObg as any;
      component.market = market as any;
      component.eventEntity = eventEntity as any;
    });

    it('should set score for team', () => {
      component.onScoreChange(5, 'teamA');

      expect(component.teamsObg.teams['teamA'].score).toEqual(5);
    });

    it('should set combinedOutcome from service', () => {
      component.onScoreChange();

      expect(correctScoreService.getCombinedOutcome).toHaveBeenCalledWith(teamsObg.teams, market.outcomes, eventEntity, market);
      expect(component.combinedOutcome.outcome).toEqual(outcome as any);
    });
  });

  it('getMaxValues', () => {
    component.market = {
      outcomes: [{
        id: '123'
      }]
    } as any;
    component.getMaxValues();

    expect(correctScoreService.getMaxScoreValues).toHaveBeenCalledWith(component.market.outcomes);
  });

  it('filteredName', () => {
    const name = 'some name';
    component.filteredName(name);

    expect(filterService.getScoreFromName).toHaveBeenCalledWith(name);
  });

  it('trackByIndex', () => {
    const index = 1;
    const result = component.trackByIndex(index);
    expect(result).toBe(index);
  });

  it('ngOnInit', () => {
    component.marketGroup = {} as any;
    component.market = { outcomes: [{
        outcomeMeaningScores: '2,0'
      }, {
        outcomeMeaningScores: '0,2'
      }, {}] } as any;
    filterService.groupBy.and.returnValue({
      1: [{ id: 1, outcomeMeaningMinorCode: 1 }],
      2: [{ id: 2, outcomeMeaningMinorCode: 2 }],
      3: [{ id: 3, outcomeMeaningMinorCode: 3 }]
    });
    const result = { outcomes: [{
        outcomeMeaningScores: '2,0'
      }, {
        outcomeMeaningScores: '0,2'
      }] } as any;

    spyOn(component, 'onScoreChange');
    spyOn(component, 'getMaxValues').and.returnValue({
      teamH: [{}],
      teamA: [{}]
    });
    component.ngOnInit();

    // @ts-ignore
    expect(component.groupedOutcomes).toEqual([
      [{ id: 1, outcomeMeaningMinorCode: 1 }],
      [{ id: 2, outcomeMeaningMinorCode: 2 }],
      [{ id: 3, outcomeMeaningMinorCode: 3 }]
    ]);
    expect(component.market).toEqual(result);
    expect(component.filterValueType).toEqual('main');
    expect(correctScoreService.getTeams).toHaveBeenCalledWith(component.marketGroup);
    expect(component.getMaxValues).toHaveBeenCalledTimes(4);
    expect(filterService.groupBy).toHaveBeenCalledWith(component.market.outcomes, 'outcomeMeaningMinorCode');
  });

  it('should test changeAccordionState state changings', () => {
    component.changeAccordionState(true);
    expect(component.isExpanded).toBeTruthy();

    component.changeAccordionState(false);
    expect(component.isExpanded).toBeFalsy();
  });
});
