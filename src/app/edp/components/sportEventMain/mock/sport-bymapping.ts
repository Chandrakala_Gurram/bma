export const SPORTBYMAPPING = {
    'id': '95a952d7-bcfb-4c48-872c-966e5ddc49fd',
    'system' : 'test',
    'name': 'OLEG OBRAZKOV /vs/ TARAS BONISHCHUK',
    'startTime': '2020-06-02T05:30:00Z',
    'sport': 'FOOTBALL',
    'competition': '30047',
    'mappings': [
        {
            'id': '230445626',
            'system': 'LADBROKES'
        },
        {
            'id': '14780963',
            'system': 'CORAL'
        }
    ],
    'feedMappings': [
        {
            'provider': 'OPTA',
            'id': '22170257',
            'booking': {
                'booked': true,
                'bookingRequestTime': '2020-06-02T04:15:46Z',
                'bookingTime': '2020-06-02T04:15:46Z',
                'bookingError': null
            }
        }
    ],
    'participants': {
        'AWAY': {
            'name': 'TARAS BONISHCHUK',
            'providerId': '65127',
            'role': 'AWAY'
        },
        'HOME': {
            'name': 'OLEG OBRAZKOV',
            'providerId': '65130',
            'role': 'HOME'
        }
    }
  };
  export const SPORTBYMAPPINGDISABLED = {
     'id': '95a952d7-bcfb-4c48-872c-966e5ddc49fd',
     'name': 'OLEG OBRAZKOV /vs/ TARAS BONISHCHUK',
     'startTime': '2020-06-02T05:30:00Z',
     'sport': 'FOOTBALL',
     'competition': '30047',
     'mappings': [
         {
             'id': '230445626',
             'system': 'LADBROKES'
         },
         {
             'id': '14780963',
             'system': 'CORAL'
         }
     ],
     'feedMappings': [
         {
             'provider': 'BETRADAR',
             'id': '22170257',
             'booking': {
                 'booked': true,
                 'bookingRequestTime': '2020-06-02T04:15:46Z',
                 'bookingTime': '2020-06-02T04:15:46Z',
                 'bookingError': null
             }
         }
     ],
     'participants': {
         'AWAY': {
             'name': 'TARAS BONISHCHUK',
             'providerId': '65127',
             'role': 'AWAY'
         },
         'HOME': {
             'name': 'OLEG OBRAZKOV',
             'providerId': '65130',
             'role': 'HOME'
         }
     }
   };
  export const BETRADARBYMAPPING = [
      {
         'BetRadarScoreBoard':{
            'desktop':true,
            'mobile':true,
            'tablet':true
         },
         'BetRadarScoreBoardsSports':{
            '36':true,
            '17':true,
            '59':true,
            '22':true,
            '20':true
         }
      },
      {
         '1':true,
         '6':true,
         '10':true,
         '16':true,
         '31':true,
         '34':true
      },
      {
         'id':'03bd7be2-61aa-4748-a3b7-89570f689e0e',
         'name':'IURII BONDARENKO /vs/ OLEKSANDR IVCHUK',
         'startTime':'2020-07-03T13:15:00Z',
         'sport':'TABLE_TENNIS',
         'competition':'30045',
         'mappings':[
            {
               'id':'230626109',
               'system':'LADBROKES'
            },
            {
               'id':'14976525',
               'system':'CORAL'
            }
         ],
         'feedMappings':[
            {
               'provider':'BETRADAR',
               'id':'22537897',
               'booking':{
                  'booked':true,
                  'bookingRequestTime':'2020-07-02T19:58:04Z',
                  'bookingTime':'2020-07-02T19:58:04Z',
                  'bookingError':null
               }
            }
         ],
         'participants':{
            'AWAY':{
               'name':'OLEKSANDR IVCHUK',
               'providerId':'66345',
               'role':'AWAY'
            },
            'HOME':{
               'name':'IURII BONDARENKO',
               'providerId':'66074',
               'role':'HOME'
            }
         }
      }
   ];
   export const BETRADAREVENTMAPPING = {
      'id':'03bd7be2-61aa-4748-a3b7-89570f689e0e',
      'name':'IURII BONDARENKO /vs/ OLEKSANDR IVCHUK',
      'startTime':'2020-07-03T13:15:00Z',
      'sport':'TABLE_TENNIS',
      'competition':'30045',
      'mappings':[
         {
            'id':'230626109',
            'system':'LADBROKES'
         },
         {
            'id':'14976525',
            'system':'CORAL'
         }
      ],
      'feedMappings':[
         {
            'provider':'BETRADAR',
            'id':'22537897',
            'booking':{
               'booked':true,
               'bookingRequestTime':'2020-07-02T19:58:04Z',
               'bookingTime':'2020-07-02T19:58:04Z',
               'bookingError':null
            }
         }
      ],
      'participants':{
         'AWAY':{
            'name':'OLEKSANDR IVCHUK',
            'providerId':'66345',
            'role':'AWAY'
         },
         'HOME':{
            'name':'IURII BONDARENKO',
            'providerId':'66074',
            'role':'HOME'
         }
      }
   };
   export const BETRADARDEVICEMAPPING = [
     {
        'BetRadarScoreBoard':{
           'desktop':true,
           'mobile':false,
           'tablet':true
        },
        'BetRadarScoreBoardsSports':{
           '36':true,
           '17':true,
           '59':true,
           '22':true,
           '20':true
        }
     },
     {
        '1':true,
        '6':true,
        '10':true,
        '16':true,
        '31':true,
        '34':true
     },
     {
        'id':'03bd7be2-61aa-4748-a3b7-89570f689e0e',
        'name':'IURII BONDARENKO /vs/ OLEKSANDR IVCHUK',
        'startTime':'2020-07-03T13:15:00Z',
        'sport':'TABLE_TENNIS',
        'competition':'30045',
        'mappings':[
           {
              'id':'230626109',
              'system':'LADBROKES'
           },
           {
              'id':'14976525',
              'system':'CORAL'
           }
        ],
        'feedMappings':[
           {
              'provider':'BETRADAR',
              'id':'22537897',
              'booking':{
                 'booked':true,
                 'bookingRequestTime':'2020-07-02T19:58:04Z',
                 'bookingTime':'2020-07-02T19:58:04Z',
                 'bookingError':null
              }
           }
        ],
        'participants':{
           'AWAY':{
              'name':'OLEKSANDR IVCHUK',
              'providerId':'66345',
              'role':'AWAY'
           },
           'HOME':{
              'name':'IURII BONDARENKO',
              'providerId':'66074',
              'role':'HOME'
           }
        }
     }
  ];
  export const BETRADARNOMAPPING = [
     {
        'BetRadarScoreBoard':{
           'desktop':true,
           'mobile':false,
           'tablet':true
        },
        'BetRadarScoreBoardsSports':{
           '36':true,
           '17':true,
           '59':true,
           '22':true,
           '20':true
        }
     },
     {
        '1':true,
        '6':true,
        '10':true,
        '16':true,
        '31':true,
        '34':true
     },
     {
        'id':'03bd7be2-61aa-4748-a3b7-89570f689e0e',
        'name':'IURII BONDARENKO /vs/ OLEKSANDR IVCHUK',
        'startTime':'2020-07-03T13:15:00Z',
        'sport':'TABLE_TENNIS',
        'competition':'30045',
        'mappings':[
           {
              'id':'230626109',
              'system':'LADBROKES'
           },
           {
              'id':'14976525',
              'system':'CORAL'
           }
        ],
        'feedMappings':[
           {
              'provider':'OPTA',
              'id':'22537897',
              'booking':{
                 'booked':true,
                 'bookingRequestTime':'2020-07-02T19:58:04Z',
                 'bookingTime':'2020-07-02T19:58:04Z',
                 'bookingError':null
              }
           }
        ],
        'participants':{
           'AWAY':{
              'name':'OLEKSANDR IVCHUK',
              'providerId':'66345',
              'role':'AWAY'
           },
           'HOME':{
              'name':'IURII BONDARENKO',
              'providerId':'66074',
              'role':'HOME'
           }
        }
     }
  ];
  export const CONFIGMAPPINGS = {
     'apiKey':'COMc368624411e44b6e80e83c5a7f7c03c8',
     'endpoints':{
        'prematch':'https://com-prd1.api.datafabric.prod.aws.ladbrokescoral.com/sdm/stats/prematch/CORAL',
        'bymapping':'https://com-prd1.api.datafabric.prod.aws.ladbrokescoral.com/sdm/events/bymapping/CORAL'
     }
  };
