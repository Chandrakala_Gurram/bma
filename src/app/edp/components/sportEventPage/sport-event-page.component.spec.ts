import { fakeAsync, tick } from '@angular/core/testing';
import { NavigationEnd, NavigationStart } from '@angular/router';
import { BehaviorSubject, Subscription, throwError, of } from 'rxjs';

import { SportEventPageComponent } from '@edp/components/sportEventPage/sport-event-page.component';
import { IMarket } from '@core/models/market.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('SportEventPageComponent', () => {
  let component;
  let activatedRoute;
  let sportEventPageProviderService;
  let templateService;
  let router;
  let footballExtensionService;
  let tennisExtensionService;
  let pubSubService;
  let filtersService;
  let routingHelperService;
  let location;
  let smartBoostsService;
  let sportConfigService;
  let changeDetectorRef;
  let windowRefService;
  let deleteMarketHandler;

  const testStr = 'TestString';
  const wasPriceStub = 'TestWasPrice';

  beforeEach(() => {

    activatedRoute = {
      snapshot: {
        paramMap: {
          get: jasmine.createSpy('get').and.callFake(str => str)
        }
      }
    } as any;

    sportEventPageProviderService = {
      sportData: {
        subscribe: jasmine.createSpy('subscribe').and.returnValue(new Subscription()),
        unsubscribe: jasmine.createSpy('unsubscribe')
      }
    } as any;

    templateService = {
      sortOutcomesByPrice: jasmine.createSpy('sortOutcomesByPrice').and.returnValue([{
        prices: [{
          priceDec: 0.34
        }],
      }, {
        prices: [{
          priceDec: 2.4
        }]
      }])
    };

    footballExtensionService = {
      eventMarkets: jasmine.createSpy('eventMarkets')
    };

    tennisExtensionService = {
      eventMarkets: jasmine.createSpy('eventMarkets')
    };

    router = {
      events: {
        subscribe: jasmine.createSpy('subscribe').and.returnValue(new Subscription()),
        unsubscribe: jasmine.createSpy('unsubscribe')
      },
      navigateByUrl: jasmine.createSpy()
    };

    pubSubService = {
      API: pubSubApi,
      subscribe: jasmine.createSpy('subscribe').and.callFake((file, method, handler) => {
        if (method === 'DELETE_MARKET_FROM_CACHE') {
          deleteMarketHandler = handler;
        }
      }),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      publish: jasmine.createSpy('publish')
    };

    filtersService = {
      groupBy: jasmine.createSpy(),
      filterAlphabetsOnly: jasmine.createSpy('filterAlphabetsOnly').and.returnValue('filterAlphabetsOnly'),
      filterNumbersOnly: jasmine.createSpy('filterNumbersOnly').and.returnValue('filterNumbersOnly')
    };

    smartBoostsService = {
      isSmartBoosts: jasmine.createSpy().and.returnValue(true),
      parseName: jasmine.createSpy().and.returnValue({ name: testStr, wasPrice: wasPriceStub })
    };

    routingHelperService = {
      formEdpUrl: jasmine.createSpy('formEdpUrl'),
    };

    location = {
      go: jasmine.createSpy('go')
    };

    sportConfigService = {
      getSport: jasmine.createSpy('getSport').and.returnValue(of({}))
    };

    changeDetectorRef = {
      detectChanges: jasmine.createSpy('changeDetectorRef')
    };

    windowRefService = {
      document: {
        querySelector: jasmine.createSpy('querySelector')
      }
    };

    component = new SportEventPageComponent(
      router,
      activatedRoute,
      sportEventPageProviderService,
      templateService,
      footballExtensionService,
      tennisExtensionService,
      routingHelperService,
      pubSubService,
      location,
      filtersService,
      smartBoostsService,
      sportConfigService,
      changeDetectorRef,
      windowRefService
    );
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  describe('showLimit', () => {
    it('should not define market showLimit', () => {
      const marketEntity = {
        isAllShown: true,
        showLimit: 1
      };

      const result = component.showLimit(marketEntity as any);
      expect(result).toBeUndefined();
    });

    it('shloud return showLimit', () => {
      const marketEntity = {
        isAllShown: false,
        showLimit: 1
      };

      const result = component.showLimit(marketEntity as any);
      expect(result).toBe(marketEntity.showLimit);
    });
  });

  describe('showYourCallMarket', () => {
    let market;

    beforeEach(() => {
      market = {
        localeName: 'yourCall'
      };
    });

    it('should not show YourCall markets for unavailable CallMarket option', () => {
      component['yourCallMarketAvailable'] = false;

      const result = component.showYourCallMarket(market as any);
      expect(result).toBeFalsy();
    });

    it('should not show YourCall markets for non YourCallMarket', () => {
      market.localeName = 'NotYourCallMarket';
      component['yourCallMarketAvailable'] = true;

      const result = component.showYourCallMarket(market as any);
      expect(result).toBeFalsy();
    });

    it('should show YourCall markets', () => {
      component['yourCallMarketAvailable'] = true;

      const result = component.showYourCallMarket(market as any);
      expect(result).toBeTruthy();
    });
  });

  describe('hasScorecastMarket', () => {
    let market;
    let marketName;

    beforeEach(() => {
      market = {
        name: 'Correct Score'
      };
      marketName = 'Active Scorecast Market';

      component['isFootball'] = true;
      component['eventEntity'] = {
        isStarted: false
      } as any;
      component['isScorecastMarketsAvailable'] = true;
      component['scorecastInTabs'] = [marketName];
      component['activeTab'] = {
        marketName: marketName
      };
    });

    it('should successfully check the existence of Scorecast Market', () => {
      const result = component.hasScorecastMarket(market);
      expect(result).toBeTruthy();
    });

    it('should successfully check the existence of Scorecast Market', () => {
      const result = component.hasScorecastMarket(<any>{ name: 'correct score' });
      expect(result).toBeTruthy();
    });

    it('should check the  of Scorecast Market for started event', () => {
      component['eventEntity']['isStarted'] = true;

      const result = component.hasScorecastMarket(market);
      expect(result).toBeFalsy();
    });

    it('should check the absence of Scorecast Market for not football event', () => {
      component['isFootball'] = false;

      const result = component.hasScorecastMarket(market);
      expect(result).toBeFalsy();
    });

    it('should check the absence of Scorecast Market for unavailable ScorecastMarket option', () => {
      component['isScorecastMarketsAvailable'] = false;

      const result = component.hasScorecastMarket(market);
      expect(result).toBeFalsy();
    });

    it('should check the absence of Scorecast Market if not Market in tabs', () => {
      component['scorecastInTabs'] = ['Any', 'others', 'tabs'];

      const result = component.hasScorecastMarket(market);
      expect(result).toBeFalsy();
    });

  });

  it('toggleShowYourCallMarket should toggle market isAllShown', () => {
    const isAllShown = false;
    const market = {
      isAllShown: isAllShown
    };

    component.toggleShowYourCallMarket(market as any);
    expect(market.isAllShown).toBe(!isAllShown);
  });

  describe('isHeaderHidden', () => {
    let market;

    beforeEach(() => {
      market = {
        marketsGroup: false,
        viewType: 'Any view type'
      };
    });

    it('should check false', () => {
      const result = component.isHeaderHidden(market);
      expect(result).toBeFalsy();
    });

    it('should check true for markets group', () => {
      market.marketsGroup = true;
      const result = component.isHeaderHidden(market);
      expect(result).toBeTruthy();
    });

    it('should check true for correctScore viewType', () => {
      market.viewType = 'correctScore';
      const result = component.isHeaderHidden(market);
      expect(result).toBeTruthy();
    });
  });

  describe('isExpanded', () => {
    beforeEach(() => {
      component['openMarketTabs'] = [true, false];
    });

    it('should be falsy for missing index', () => {
      const result = component.isExpanded(100);
      expect(result).toBeFalsy();
    });

    it('should be falsy for falsy openMarketTab and not market', () => {
      const market = {};
      spyOn<any>(component, 'isHeaderHidden').and.returnValue(false);

      const result = component.isExpanded(1, market as any);
      expect(result).toBeFalsy();
    });

    it('should be truthy for truthy openMarketTab', () => {
      const result = component.isExpanded(0);
      expect(result).toBeTruthy();
    });

    it('should be truthy for falsy openMarketTab and market', () => {
      const market = {};
      spyOn<any>(component, 'isHeaderHidden').and.returnValue(true);

      const result = component.isExpanded(100, market as any);
      expect(result).toBeTruthy();
    });

    it('should test changeAccordionState state changings', () => {
      component.changeAccordionState(100, true);
      expect(changeDetectorRef.detectChanges).toHaveBeenCalled();

      const result = component.isExpanded(100);
      expect(result).toBeTruthy();
    });
  });

  describe('getTrackByValue', () => {
    it('should track by market.id', () => {
      const market = {
        id: 111
      };
      const index = 5;
      component['isFootball'] = false;
      component['activeTab'] = null;

      const result = component.getTrackByValue(index, market as any);
      expect(result).toBe(`${market.id}-`);
    });

    it('should track by index', () => {
      const market = {
        id: 111
      };
      const index = 5;
      component['isFootball'] = true;
      component['activeTab'] = {
        id: 0
      };

      const result = component.getTrackByValue(index, market as any);
      expect(result).toBe(`${index}-`);
    });

    it('should track by market.id and by activeTab id', () => {
      const market = {
        id: 111
      };
      const index = 5;
      component['isFootball'] = false;
      component['activeTab'] = {
        id: 5
      };

      const result = component.getTrackByValue(index, market as any);
      expect(result).toBe(`${market.id}-${component['activeTab']['id']}`);
    });

    it('should track by index and by activeTab id', () => {
      const market = {
        id: 111
      };
      const index = 5;
      component['isFootball'] = true;
      component['activeTab'] = {
        id: 5
      };

      const result = component.getTrackByValue(index, market as any);
      expect(result).toBe(`${index}-${component['activeTab']['id']}`);
    });
  });

  it('getTrackById should track by entity.id', () => {
    const entity = {
      id: 111
    };

    const result = component.getTrackById(0, entity);
    expect(result).toBe('111_0');
  });

  describe('isEnabledOnCms', () => {
    it('should be falsy for missing yourCallPlayerStatsName', () => {
      component['sysConfig'] = {};

      const result = component.isEnabledOnCms();
      expect(result).toEqual(false);
    });

    it('should be falsy for disabled option', () => {
      component['sysConfig'] = {
        yourCallPlayerStatsName: {
          enabled: false
        }
      };
      const result = component.isEnabledOnCms();
      expect(result).toEqual(false);
    });

    it('should be truthy for enabled option', () => {
      component['sysConfig'] = {
        yourCallPlayerStatsName: {
          enabled: true
        }
      };
      const result = component.isEnabledOnCms();
      expect(result).toEqual(true);
    });
  });

  describe('ngOnDestroy', () => {
    it('should unsubscribe listeners', () => {
      component['yourCallTabContentComponent'] = {} as any;
      component['sportDataSubscription'] = {
        unsubscribe: jasmine.createSpy('unsubscribe')
      } as any;
      component['routeChangeListener'] = {
        unsubscribe: jasmine.createSpy('unsubscribe')
      } as any;
      component['sportsConfigSubscription'] = {
        unsubscribe: jasmine.createSpy('unsubscribe')
      } as any;

      component.ngOnDestroy();

      expect(pubSubService.unsubscribe).toHaveBeenCalledWith('sportEventPageCtrl');
      expect(component['sportDataSubscription'].unsubscribe).toHaveBeenCalled();
      expect(component['routeChangeListener'].unsubscribe).toHaveBeenCalled();
    });

    it('should not throw error', () => {
      component.sportDataSubscription = component.routeChangeListener = null;
      expect(() => component.ngOnDestroy()).not.toThrowError();
    });
  });

  describe('ngOnInit', () => {
    it('should transform Markets', () => {
      spyOn(component as any, 'generateYourCallMarkets');
      component.filteredMarketGroup = [{ outcomes: [{ name: '' }] }] as IMarket[];
      component.marketGroup = [{ name: '', outcomes: [{ name: '' }] }] as IMarket[];
      component.eventEntity = {} as ISportEvent;
      component['sport'].isMarketTabCorrect = () => {};
      component['marketsByCollection'] = [];

      component.init();

      expect(component.filteredMarketGroup[0].outcomes[0].wasPrice).toEqual(wasPriceStub);
    });

    it('should subscribe sportData and router events', () => {
      spyOn(component, 'init');
      component.ngOnInit();

      expect(sportEventPageProviderService.sportData.subscribe)
        .toHaveBeenCalledWith(component['sportDataHandler'], jasmine.any(Function));
      expect(router.events.subscribe).toHaveBeenCalledWith(jasmine.any(Function));
    });

    it('should properly set default tab in case if tab was renamed', () => {
      const tabs1 = [
        {id: 'tab-all-markets'},
        {id: 'tab-main-markets'}
      ] as any;
      const tabs2 = [
        {id: 'tab-all-markets'},
        {id: 'tab-main'}
      ] as any;
      component.filteredMarketGroup = [{ outcomes: [{ name: '' }] }] as IMarket[];
      component.marketGroup = [{ name: '', outcomes: [{ name: '' }] }] as IMarket[];
      component['sport'].isMarketTabCorrect = () => true;
      component['eventTabs'] = tabs1;
      component['marketName'] = 'main-markets';
      component.eventEntity = {} as ISportEvent;
      component['marketsByCollection'] = [];
      component['generateYourCallMarkets'] = () => {};
      component.init();
      expect(component['activeTab'].id).toEqual('tab-main-markets');
      component['eventTabs'] = tabs2;
      component.init();
      expect(component['activeTab'].id).toEqual('tab-main');
    });

    it('should properly set redirect tab', () => {
     component['sport'].isMarketTabCorrect = () => false;
      routingHelperService.formEdpUrl.and.returnValue('baseURL');
      component['marketName'] = 'fakeName';
      component['defaultMarketName'] = 'default';
      component.eventEntity = {} as ISportEvent;
      component['marketsByCollection'] = [];
      component['generateYourCallMarkets'] = () => {};
      component.init();
      expect(router.navigateByUrl).toHaveBeenCalledWith('baseURL/default');
    });

    it('should trigger init for NavigationEnd event', fakeAsync(() => {
      const eventEnd = new NavigationEnd(0, '', '');
      router.events.subscribe.and.callFake((fn) => fn(eventEnd));
      spyOn(component, 'init');
      component.ngOnInit();
      tick();

      expect(activatedRoute.snapshot.paramMap.get).toHaveBeenCalledWith('market');
      expect(component['marketName']).toEqual('market');
      expect(component.init).toHaveBeenCalled();
    }));

    it('should not trigger init for non NavigationEnd event', fakeAsync(() => {
      const eventStart = new NavigationStart(0, '');
      router.events.subscribe.and.callFake((fn) => fn(eventStart));
      spyOn(component, 'init');
      component.ngOnInit();
      tick();

      expect(component.init).not.toHaveBeenCalled();
    }));

    it ('should call error handler', () => {
      sportEventPageProviderService.sportData = new BehaviorSubject<any>(null);
      sportEventPageProviderService.sportData.error(throwError('err'));
      component.ngOnInit();

      expect(component.state.loading).toBe(false);
      expect(component.state.error).toBe(true);
    });
  });

  describe('init', () => {
    beforeEach(() => {
      component.sport = { isMarketTabCorrect: jasmine.createSpy('isMarketTabCorrect') };
      component.eventEntity = {};
      component.marketsByCollection = [];
      component.sysConfig = { YourCallMarket: {} };
    });

    it('should not init component', () => {
      component.eventEntity = null;
      component.init();
      expect(component.sport.isMarketTabCorrect).not.toHaveBeenCalled();
      expect(component.showSurfaceBets).toBe(false);
    });

    it('market name same as default market name', () => {
      component.marketName = component.defaultMarketName = 'mkt';
      component.init();
      expect(location.go).toHaveBeenCalledTimes(1);
    });

    it('should set selected market group', () => {
      component.sport.isMarketTabCorrect.and.returnValue(true);
      component.eventTabs = [{ id: 'tab-mkt', label: 'mkt' }];
      component.marketName = 'mkt';
      component.marketsByCollection = [{ markets: [], name: 'mkt' }];
      component.init();
      expect(component.marketGroup).toEqual(component.marketsByCollection[0].markets);
    });

    it('should create player stats market group', () => {
      spyOn(component, 'createPlayerStatsMarketsGroup');
      component.sport.isMarketTabCorrect.and.returnValue(true);
      component.eventTabs = [{ id: 'tab-mkt', label: 'mkt' }];
      component.marketName = 'mkt';
      component.marketsByCollection = [{
        name: 'mkt', markets: [{ templateMarketName: 'Player_Stats_' , name: '' }]
      }];
      component.init();
      expect(component.createPlayerStatsMarketsGroup).toHaveBeenCalledTimes(1);
    });

    it('init opened markets tabs', () => {
      component.sport.isMarketTabCorrect.and.returnValue(true);
      component.eventTabs = [{ id: 'tab-mkt', label: 'mkt' }];
      component.marketName = 'mkt';
      component.marketsByCollection = [{
        name: 'mkt', markets: [
          { name: '1', outcomes: [{}] },
          { name: '2', outcomes: [{}] },
          { name: '3', outcomes: [{}] }
        ]
      }];
      component.init();
      expect(component.openMarketTabs).toEqual([true, true, false]);
    });

    it('should insert collapsed state', () => {
      spyOn(component, 'insertCollapsedState');
      component.sport.isMarketTabCorrect.and.returnValue(true);
      component.eventTabs = [{ id: 'tab-mkt', label: 'mkt' }];
      component.marketName = 'mkt';
      component.marketsByCollection = [{ name: 'mkt', markets: [] }];
      component.openMarketTabs = [true];
      component.init();
      expect(component.insertCollapsedState).toHaveBeenCalledTimes(1);
    });

    it('should not show SurfaceBets on BYB and 5-A-Side tabs', () => {
      component.eventTabs = [{ id: 'tab-mkt' },
                        { id: 'tab-build-your-bet' },
                        { id: 'tab-bet-builder' },
                        { id: 'tab-5-a-side' }];
      component.eventEntity = {} as any;
      component.defaultMarketName = 'mkt';
      component.init();
      expect(component.showSurfaceBets).toBe(false);

      component.eventEntity = { id: 12 } as any;
      component.init();
      expect(component.showSurfaceBets).toBe(true);

      component.defaultMarketName = 'build-your-bet';
      component.init();
      expect(component.showSurfaceBets).toBe(false);

      component.defaultMarketName = 'bet-builder';
      component.init();
      expect(component.showSurfaceBets).toBe(false);

      component.defaultMarketName = '5-a-side';
      component.init();
      expect(component.showSurfaceBets).toBe(false);
    });

  });

  describe('#generateYourCallMarkets', () => {
    const marketGroup_01 = [{
      id: '1',
      displayOrder: 2,
      templateMarketName: 'Over/Under Second Half Home Team Total Goals',
      name: '2.5',
      outcomes: [{
        prices: [{
          priceDec: 0.34
        }]
      }, {
        prices: [{
          priceDec: 2.4
        }]
      }]
    }, {
      id: '2',
      displayOrder: 1,
      templateMarketName: 'YourCall InPlay and Cashout',
      name: '#YourCall - River Plate'
    }] as any;

    const marketGroup_02 = [{
      id: '1',
      displayOrder: 1,
      templateMarketName: 'YourCall InPlay Market',
      name: '#YourCall - Palestino'
    }] as any;

    const marketGroup_03 = [{
      id: '2',
      displayOrder: 2,
      templateMarketName: 'Match Betting',
      name: 'Match Result'
    }] as any;

    const marketGroup_04 = [{
      id: '1',
      displayOrder: 2,
      templateMarketName: 'Over/Under Second Half Home Team Total Goals',
      name: '2.5',
      outcomes: [{
        prices: [{
          priceDec: 2.4
        }]
      }, {
        prices: [{
          priceDec: 0.34
        }]
      }]
    }, {
      id: '2',
      displayOrder: 1,
      templateMarketName: 'YourCall InPlay and Cashout',
      name: '#YourCall - River Plate',
      outcomes: [{
        prices: [{
          priceDec: 2.4
        }]
      }, {
        prices: [{
          priceDec: 0.34
        }]
      }]
    }] as any;

    const marketGroup_05 = [{
      id: '1',
      displayOrder: 2,
      templateMarketName: 'Over/Under Second Half Home Team Total Goals',
      name: '2.5'
    }, {
      id: '2',
      displayOrder: 1,
      templateMarketName: 'YourCall InPlay and Cashout',
      name: '#YourCall - River Plate'
    }] as any;

    const eventEntity_01 = {
      id: 12558682,
      name: 'Palestino v River Plate',
      markets: marketGroup_01
    } as any;

    const eventEntity_02 = {
      id: 23432423,
      name: 'Palestino v River Plate',
      markets: marketGroup_02
    } as any;

    const eventEntity_03 = {
      id: 24234223,
      name: 'Palestino v River Plate',
      markets: marketGroup_03
    } as any;

    const eventEntity_04 = {
      id: 12558682,
      name: 'Palestino v River Plate',
      markets: marketGroup_04
    } as any;

    const eventEntity_05 = {
      id: 12558682,
      name: 'Palestino v River Plate',
      markets: marketGroup_05
    } as any;


    const result_01 = [{
      id: '1',
      displayOrder: 2,
      templateMarketName: 'Over/Under Second Half Home Team Total Goals',
      name: '2.5',
      outcomes: [{
        prices: [{
          priceDec: 0.34
        }],
      }, {
        prices: [{
          priceDec: 2.4
        }]
      }]
    }, {
      id: '2',
      displayOrder: 1,
      isAllShown: false,
      showLimit: 5,
      templateMarketName: 'YourCall InPlay and Cashout',
      name: '#YourCall - River Plate'
    }] as any;

    const result_02 = [{
      id: '1',
      isAllShown: false,
      showLimit: 5,
      displayOrder: 1,
      templateMarketName: 'YourCall InPlay Market',
      name: '#YourCall - Palestino'
    }] as any;

    const result_03 = [{
      id: '1',
      displayOrder: 2,
      templateMarketName: 'Over/Under Second Half Home Team Total Goals',
      name: '2.5',
      outcomes: [{
        prices: [{
          priceDec: 2.4
        }]
      }, {
        prices: [{
          priceDec: 0.34
        }]
      }]
    }, {
      id: '2',
      displayOrder: 1,
      isAllShown: false,
      showLimit: 5,
      templateMarketName: 'YourCall InPlay and Cashout',
      name: '#YourCall - River Plate',
      outcomes: [{
        prices: [{
          priceDec: 0.34
        }]
      }, {
        prices: [{
          priceDec: 2.4
        }]
      }]
    }] as any;

    const result_05 = [{
      id: '1',
      displayOrder: 2,
      templateMarketName: 'Over/Under Second Half Home Team Total Goals',
      name: '2.5'
    }, {
      id: '2',
      displayOrder: 1,
      isAllShown: false,
      showLimit: 5,
      templateMarketName: 'YourCall InPlay and Cashout',
      name: '#YourCall - River Plate'
    }] as any;

    it('it should generate YourCall markets it is exist', () => {
      component['sysConfig'] = {
        YourCallMarket: {
          football: '#YourCall',
          basketball: '#YourCall'
        }
      };
      component.marketGroup = marketGroup_01;
      component.eventEntity = eventEntity_01;
      component['sportName'] = 'football';

      component['generateYourCallMarkets']();
      expect(component.marketGroup).toEqual(result_01);
    });

    it('it should generate YourCall markets it is exist and should sort outcomes', () => {
      component['sysConfig'] = {
        YourCallMarket: {
          football: '#YourCall',
          basketball: '#YourCall'
        }
      };
      component.marketGroup = marketGroup_04;
      component.eventEntity = eventEntity_04;
      component['sportName'] = 'football';

      component['generateYourCallMarkets']();
      expect(templateService.sortOutcomesByPrice).toHaveBeenCalled();
      expect(component.marketGroup).toEqual(result_03);
    });

    it('it should generate YourCall markets it is exist but should NOT sort outcomes', () => {
      component['sysConfig'] = {
        YourCallMarket: {
          football: '#YourCall',
          basketball: '#YourCall'
        }
      };
      component.marketGroup = marketGroup_05;
      component.eventEntity = eventEntity_05;
      component['sportName'] = 'football';

      component['generateYourCallMarkets']();
      expect(templateService.sortOutcomesByPrice).not.toHaveBeenCalled();
      expect(component.marketGroup).toEqual(result_05);
    });

    it('it should NOT generate YourCall markets it is already exist', () => {
      component['sysConfig'] = {
        YourCallMarket: {}
      };
      component.marketGroup = marketGroup_01;
      component.eventEntity = eventEntity_01;
      component['sportName'] = 'football';

      expect(component.marketGroup).toEqual(result_01);
      component['generateYourCallMarkets']();
      expect(component.marketGroup).toEqual(result_01);
    });

    it('it should generate YourCall markets it is exist and set default market name', () => {
      component['sysConfig'] = {
        YourCallMarket: {}
      };
      component.marketGroup = marketGroup_02;
      component.eventEntity = eventEntity_02;
      component['sportName'] = 'tennis';

      component['generateYourCallMarkets']();
      expect(component.marketGroup).toEqual(result_02);
    });

    it('it should NOT generate YourCall markets it is not exist', () => {
      component['sysConfig'] = {
        YourCallMarket: {}
      };
      component.marketGroup = marketGroup_03;
      component.eventEntity = eventEntity_03;
      component['sportName'] = 'basketball';

      component['generateYourCallMarkets']();
      expect(component.marketGroup).toEqual(marketGroup_03);
    });
  });

  describe('recalculateExpandedMarkets', () => {
    it('should be recalculate openMarketTabs', () => {
      component['openMarketTabs'] = [] as any;
      component['openedMarketTabsCountByDefault'] = 1;
      component.filteredMarketGroup = [{ outcomes: [{ name: '' }] }, { outcomes: [] }] as IMarket[];

      component.recalculateExpandedMarkets();
      expect(component.openMarketTabs).toEqual([true, false]);
    });


    it('should be check openMarketTabs length', () => {
      const filterMarkets = [true, true];
      component['openMarketTabs'] = [] as any;
      component['openedMarketTabsCountByDefault'] = 2;
      component.filteredMarketGroup = [{ outcomes: [{ name: '' }] }] as IMarket[];

      component.recalculateExpandedMarkets();
      expect(component.openMarketTabs).toEqual(filterMarkets);
    });
  });

  describe('transformMarkets', () => {
    it(`isSmartBoosts property should equal true if market is SmartBoosts`, () => {
      const markets = [{ outcomes: [{ name: '' }] }] as IMarket[];

      component['transformMarkets'](markets);
      expect(markets[0].isSmartBoosts).toBeTruthy();
    });

    it(`should change outcomes 'name' if market is SmartBoosts`, () => {
      const markets = [{ outcomes: [{ name: '' }] }] as IMarket[];

      component['transformMarkets'](markets);
      expect(markets[0].outcomes[0].name).toEqual(testStr);
    });

    it(`should set outcomes 'wasPrice' if market is SmartBoosts`, () => {
      const markets = [{ outcomes: [{ name: '' }] }] as IMarket[];

      component['transformMarkets'](markets);
      expect(markets[0].outcomes[0].wasPrice).toEqual(wasPriceStub);
    });

    it(`should Not change outcomes 'name' if market is Not SmartBoosts`, () => {
      const markets = [{ outcomes: [{ name: '' }] }] as IMarket[];
      component['smartBoostsService'].isSmartBoosts = jasmine.createSpy().and.returnValue(false);

      component['transformMarkets'](markets);
      expect(markets[0].outcomes[0].name).toEqual('');
    });

    it(`should Not set outcomes 'wasPrice' if market is Not SmartBoosts`, () => {
      const markets = [{ outcomes: [{ name: '' }] }] as IMarket[];
      component['smartBoostsService'].isSmartBoosts = jasmine.createSpy().and.returnValue(false);

      component['transformMarkets'](markets);
      expect(markets[0].outcomes[0].wasPrice).toBeUndefined();
    });

    it(`should Not set outcomes 'wasPrice' if parsedName has Not 'wasPrice'`, () => {
      const markets = [{ outcomes: [{ name: '' }] }] as IMarket[];
      component['smartBoostsService'].parseName = jasmine.createSpy().and.returnValue({ name: '' });

      component['transformMarkets'](markets);
      expect(markets[0].outcomes[0].wasPrice).toBeUndefined();
    });

    it(`should set outcome alphabetName && numbersName`, () => {
      const markets = [{ viewType: 'columns-3', outcomes: [{ name: '' }] }] as IMarket[];
      component['transformMarkets'](markets);
      expect(markets[0].outcomes[0].alphabetName).toEqual('filterAlphabetsOnly');
      expect(markets[0].outcomes[0].numbersName).toEqual('filterNumbersOnly');
    });
  });

  describe('invokeSportExtension', () => {
    it('football', () => {
      component['isFootball'] = true;
      component['invokeSportExtension']();
      expect(footballExtensionService.eventMarkets).toHaveBeenCalledTimes(1);
    });

    it('tennis', fakeAsync(() => {
      component['sportName'] = 'tennis';
      component['invokeSportExtension']();
      tick();
      expect(sportConfigService.getSport).toHaveBeenCalledWith('tennis');
      expect(tennisExtensionService.eventMarkets).toHaveBeenCalledTimes(1);
    }));
  });

  describe('DELETE_MARKET_FROM_CACHE', () => {
    it('should init after DELETE_MARKET_FROM_CACHE', () => {
      spyOn(component, 'init');
      component['marketsByCollection'] = <any>[{ markets: [{id: 1}] }];
      deleteMarketHandler(1);
      expect(component.init).toHaveBeenCalledTimes(1);
    });

    it('should not init after DELETE_MARKET_FROM_CACHE', () => {
      spyOn(component, 'init');
      component['marketsByCollection'] = <any>[{ markets: [{id: 1}] }];
      deleteMarketHandler(2);
      expect(component.init).not.toHaveBeenCalled();
    });
  });

  it('childComponentLoaded should set initialized to true', () => {
    component.childComponentLoaded();
    expect(component.initialized).toBeTruthy();
  });

  describe('recalculateExpandedMarkets', () => {
    it('should`t reset initialized prop if resetInit param is false', () => {
      component.filteredMarketGroup = [{ outcomes: [{ name: '' }] }] as IMarket[];
      component.initialized = true;
      component.recalculateExpandedMarkets(false);
      expect(component.initialized).toBeTruthy();
    });
    it('should reset initialized prop if resetInit param is true', () => {
      component.filteredMarketGroup = [{ outcomes: [{ name: '' }] }] as IMarket[];
      component.initialized = true;
      component.recalculateExpandedMarkets();
      expect(component.initialized).toBeFalsy();
    });
  });

  describe('sportDataHandler', () => {
    beforeEach(() => {
      spyOn(component, 'init');
    });

    it('data present', () => {
      component.sportDataHandler({ eventData: { event: [{}] } });
      expect(component.init).toHaveBeenCalledTimes(1);
    });

    it('no data', () => {
      component.sportDataHandler(null);
      expect(component.init).not.toHaveBeenCalled();
    });
  });

  describe('createPlayerStatsMarketsGroup', () => {
    beforeEach(() => {
      component.sysConfig = { yourCallPlayerStatsName: { name: 'player' } };
      component.eventEntity = {
        markets: [{ templateMarketName: 'Player_Stats_' }, {}]
      };
      component.marketGroup = [];
    });

    it('should add market group', () => {
      component.createPlayerStatsMarketsGroup();
      expect(component.marketGroup.length).toBe(1);
    });

    it('should not add market group', () => {
      component.sysConfig.yourCallPlayerStatsName = null;
      component.marketGroup = [{ localeName: 'playerStats' }];
      component.createPlayerStatsMarketsGroup();
      expect(component.marketGroup.length).toBe(1);
    });
  });

  it('insertCollapsedState', () => {
    component.openedMarketTabsMap = { '1': {} };
    component.filteredMarketGroup = [{ id: '1' }, { id: '2' }, { id: '3' }];
    component.insertCollapsedState();
    expect(component.openedMarketTabsMap['2']).toBeDefined();
  });

  describe('subscribeToEvents', () => {
    it('should detect changes (OUTCOME_UPDATED)', () => {
      pubSubService.subscribe.and.callFake((name, channel, cb) => channel === 'OUTCOME_UPDATED' && cb());
      component['subscribeToEvents']();
      expect(changeDetectorRef.detectChanges).toHaveBeenCalled();
    });

    it('should check byb tabs (MOVE_EVENT_TO_INPLAY)', () => {
      pubSubService.subscribe.and.callFake((name, channel, cb) => channel === 'MOVE_EVENT_TO_INPLAY' && cb({ id: 1 }));
      spyOn(component, 'checkBybTabs');
      component.eventEntity = { id: 1 };
      component['subscribeToEvents']();
      expect(component.checkBybTabs).toHaveBeenCalled();
    });

    it('should not check byb tabs (MOVE_EVENT_TO_INPLAY)', () => {
      pubSubService.subscribe.and.callFake((name, channel, cb) => channel === 'MOVE_EVENT_TO_INPLAY' && cb({ id: 2 }));
      spyOn(component, 'checkBybTabs');
      component.eventEntity = { id: 1 };
      component['subscribeToEvents']();
      expect(component.checkBybTabs).not.toHaveBeenCalled();
    });

    it('should remove byb tabs (REMOVE_EDP_BYB_TABS)', () => {
      pubSubService.subscribe.and.callFake((name, channel, cb) => channel === 'REMOVE_EDP_BYB_TABS' && cb());
      spyOn(component, 'removeBybTabs');
      component['subscribeToEvents']();
      expect(component.removeBybTabs).toHaveBeenCalled();
    });
  });

  describe('checkBybTabs', () => {
    beforeEach(() => {
      component.eventEntity = { id: 1,  eventIsLive: false };
      component.eventTabs = [{ id: 'tab-all-markets', url: 'all-market-url' }, { id: 'tab-build-your-bet' }];
      spyOn(component, 'removeBybTabs').and.callThrough();
    });

    it('should not remove byb tabs', () => {
      component.checkBybTabs();
      expect(component.removeBybTabs).not.toHaveBeenCalled();
    });

    it('should remove byb tabs', () => {
      component.eventEntity.eventIsLive = true;
      component.checkBybTabs();
      expect(component.removeBybTabs).toHaveBeenCalled();
      expect(component.eventTabs).toEqual([{ id: 'tab-all-markets', url: 'all-market-url' }]);
    });

    it('should remove byb tabs and redirect to all markets', () => {
      component.eventEntity.eventIsLive = true;
      component.activeTab = { id: 'tab-build-your-bet' };
      component.checkBybTabs();
      expect(component.removeBybTabs).toHaveBeenCalled();
      expect(component.eventTabs).toEqual([{ id: 'tab-all-markets', url: 'all-market-url' }]);
      expect(router.navigateByUrl).toHaveBeenCalledWith('all-market-url');
      expect(pubSubService.publish).toHaveBeenCalledWith('REMOVE_BYB_STORED_EVENT', component.eventEntity.id);
    });
  });
});
