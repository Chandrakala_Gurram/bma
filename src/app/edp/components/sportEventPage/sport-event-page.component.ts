import { Component, OnDestroy, OnInit, ChangeDetectorRef } from '@angular/core';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { Router, Event, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { SportEventPageProviderService } from './sport-event-page-provider.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { ISystemConfig } from '@core/services/cms/models';
import * as _ from 'underscore';
import { IMarketCollection } from '@core/models/market-collection.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { IMarket } from '@core/models/market.model';
import { ITab } from '@core/models/tab.model';
import { IConstant } from '@core/services/models/constant.model';
import { FootballExtensionService } from '@app/edp/services/footballExtension/football-extension.service';
import { TennisExtensionService } from '@app/edp/services/tennisExtension/tennis-extension.service';
import { AbstractOutletComponent } from '@shared/components/abstractOutlet/abstract-outlet.component';
import { IMarketsGroup } from '@edp/services/marketsGroup/markets-group.model';
import { Subscription } from 'rxjs';
import { IOutcome } from '@core/models/outcome.model';
import { SmartBoostsService } from '@sb/services/smartBoosts/smart-boosts.service';
import { TemplateService } from '@shared/services/template/template.service';
import { SportsConfigService } from '@sb/services/sportsConfig/sports-config.service';
import { IReference } from '@core/models/live-serve-update.model';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

@Component({
  selector: 'sport-event-page',
  styleUrls: ['sport-event-page.component.less'],
  templateUrl: './sport-event-page.component.html'
})
export class SportEventPageComponent extends AbstractOutletComponent implements OnInit, OnDestroy {

  public showYourCallContent: boolean = false;
  public showFiveASideContent: boolean = false;
  public liveMaketTemplateMarketName: string;
  public eventEntity: ISportEvent;
  public marketGroup: IMarket[];
  public filteredMarketGroup: IMarket[];
  public scorecastInTabs: string[];
  public isScorecastMarketsAvailable: boolean;
  public eventTabs: ITab[];
  public isSpecialEvent: boolean = false;
  public activeTab: IMarketCollection;
  public typeId: string;
  public marketConfig: IMarket[];
  public marketAvailable: IConstant;
  public openedMarketTabsCountByDefault: number = 2;
  public initialized: boolean = false;
  public showSurfaceBets: boolean = false;

  protected openMarketTabs: boolean[] = [];
  protected marketName: string = '';
  protected routeChangeListener: Subscription;
  protected sportDataSubscription: Subscription;
  protected yourCallMarketAvailable: boolean = false;

  private defaultMarketName: string = 'main-markets';
  private openedMarketTabsMap: IConstant = {};
  private sportName: string = '';
  private isFootball: boolean = false;
  private sport: any = {};
  private marketsByCollection: IMarketCollection[];
  private sysConfig: ISystemConfig;
  private sportsConfigSubscription: Subscription;

  private bybTabsRe = /^tab-(build-your-bet|bet-builder|5-a-side)$/;

  constructor(
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    protected sportEventPageProviderService: SportEventPageProviderService,
    protected templateService: TemplateService,
    private footballExtension: FootballExtensionService,
    private tennisExtension: TennisExtensionService,
    private routingHelperService: RoutingHelperService,
    protected pubSubService: PubSubService,
    private location: Location,
    private filtersService: FiltersService,
    private smartBoostsService: SmartBoostsService,
    private sportConfigService: SportsConfigService,
    private changeDetectorRef: ChangeDetectorRef,
    private windowRefService: WindowRefService
  ) {
    super()/* istanbul ignore next */;
    this.marketName = this.activatedRoute.snapshot.paramMap.get('market') || this.defaultMarketName;
    this.sportName = this.activatedRoute.snapshot.paramMap.get('sport');
    this.isFootball = this.sportName === 'football';
    this.getTrackByValue = this.getTrackByValue.bind(this);

    // handle market deletion when market is undisplayed or all selections are undisplayed
    this.pubSubService.subscribe('sportEventPageCtrl', pubSubService.API.DELETE_MARKET_FROM_CACHE, marketId => {
      this.marketsByCollection.forEach((colection: IMarketCollection) => {
        const marketIndex = _.findIndex(colection.markets, {
          id: marketId
        });

        if (marketIndex >= 0) {
          colection.markets.splice(marketIndex, 1);
          this.init();
        }
      });
    });
    this.sportDataHandler = this.sportDataHandler.bind(this);

    this.subscribeToEvents();
  }

  public showLimit(marketEntity: IMarket): number {
    return !marketEntity.isAllShown ? marketEntity.showLimit : undefined;
  }

  /**
   * Check for showing YourCall markets
   * @param {object} market
   * @return {boolean}
   */
  public showYourCallMarket(market: IMarketsGroup): boolean {
    return this.yourCallMarketAvailable && market.localeName === 'yourCall';
  }

  /**
   * Check if event has scorecast market
   * @param market
   * return {boolean}
   */
  public hasScorecastMarket(market: IMarket): boolean {
    const correctScorePattern = new RegExp('^correct\\sscore$', 'i');
    const matchCorrectScorePattern = market.name.toString().match(correctScorePattern) !== null;

    return this.isFootball && matchCorrectScorePattern && this.isScorecastMarketsAvailable &&
      !this.eventEntity.isStarted && (this.scorecastInTabs.indexOf(this.activeTab.marketName) > -1);
  }

  /**
   * Toggle method to show hide selections on YourCall markets
   * @param marketEntity
   */
  public toggleShowYourCallMarket(marketEntity: IMarket) {
    marketEntity.isAllShown = !marketEntity.isAllShown;
  }

  /**
   * Hide Accordion header
   * @param {Object} market
   * @returns {Boolean}
   */
  public isHeaderHidden(market: IMarket): boolean {
    return market.marketsGroup || market.viewType === 'correctScore';
  }

  /**
   * Accordion expand status
   * @param {Number} index
   * @param {Object} market
   */
  public isExpanded(index: number, market?: IMarket): boolean {
    const isMarket = market ? this.isHeaderHidden(market) : false;
    return this.openMarketTabs[index] || isMarket;
  }

  public changeAccordionState(index: number, value: boolean) {
    this.openMarketTabs[index] = value;
    this.changeDetectorRef.detectChanges();
  }

  /**
   * Get trackBy Value
   * @param {number} index
   * @param {object} market
   * @return {string}
   */
  public getTrackByValue(index: number, market: IMarket): string {
    return `${this.isFootball ? index : market.id}-${this.activeTab && this.activeTab.id ? this.activeTab.id : ''}`;
  }


  public getTrackById(index: number, entity: any) {
    return `${entity.id}_${index}`;
  }

  public childComponentLoaded(): void {
    this.initialized = true;
  }

  /**
   * Group if markets are turn on in cms
   */
  public isEnabledOnCms(): boolean {
    return !!(this.sysConfig.yourCallPlayerStatsName && this.sysConfig.yourCallPlayerStatsName.enabled);
  }
  recalculateExpandedMarkets(resetInit: boolean = true) {
    this.openMarketTabs = [];
    if (resetInit) {
      this.initialized = false;
    }
    this.filteredMarketGroup.forEach((market, index) => {
      this.openedMarketTabsMap[market.id] = index;
      this.openMarketTabs.push(index < this.openedMarketTabsCountByDefault || false);
    });
    if (this.openMarketTabs.length < this.openedMarketTabsCountByDefault) {
      this.openMarketTabs = new Array(this.openedMarketTabsCountByDefault).fill(true);
    }
    // Recalculate opened markets tabs states on markets count change
    if (this.filteredMarketGroup.length !== this.openMarketTabs.length) {
      this.insertCollapsedState();
    }
  }
  /**
   * initialize controller
   */
  public init(): void {
    let localMarketName = this.marketName;

    if (this.eventEntity && !this.isSpecialEvent) {
      // redirecting to 404 if wrong market.
      if (!this.sport.isMarketTabCorrect(this.eventTabs, localMarketName)) {
        const edpUrl = this.routingHelperService.formEdpUrl(this.eventEntity);
        if (localMarketName === this.defaultMarketName) {
          localMarketName = 'all-markets';
          this.location.go(`${edpUrl}/${localMarketName}`);
        } else {
          localMarketName = this.defaultMarketName;
          this.router.navigateByUrl(`${edpUrl}/${localMarketName}`);
        }
      }

      this.checkBybTabs();

      // Select active tab.
      const renamedMainTab = _.findWhere(this.eventTabs, { id: `tab-main` });
      localMarketName = renamedMainTab && localMarketName === 'main-markets' ? 'main' : localMarketName;
      this.activeTab = _.findWhere(this.eventTabs, { id: `tab-${localMarketName}` });
      this.showYourCallContent = this.activeTab && (this.activeTab.id === 'tab-build-your-bet' || this.activeTab.id === 'tab-bet-builder');
      this.showFiveASideContent = this.activeTab && this.activeTab.id === 'tab-5-a-side';

      this.showSurfaceBets = !!this.eventEntity.id && !this.showYourCallContent && !this.showFiveASideContent;

      // Get selected market group.
      const selectedMarketGroup = _.find(this.marketsByCollection.filter(coll => coll.markets), mar => this.activeTab &&
        mar.name === this.activeTab.label);
      if (selectedMarketGroup) {
        this.marketGroup = selectedMarketGroup.markets;
      }

      this.invokeSportExtension();
      this.generateYourCallMarkets();

      if (this.isPlayerStatsMarketsPresent()) {
        this.createPlayerStatsMarketsGroup();
      }

      this.marketGroup = _.chain(this.marketGroup)
        .sortBy(data => {
          return data.name.toLowerCase();
        })
        .sortBy('displayOrder')
        .value();
      this.filteredMarketGroup = _.filter(this.marketGroup, market => {
        return market.viewType !== 'marketsGroup' &&
          !market.hidden && (market.marketsGroup || (market.outcomes && market.outcomes.length > 0));
      });

      // Init opened markets tabs
      if (!this.openMarketTabs.length) {
        this.filteredMarketGroup.forEach((market, index) => {
          this.openedMarketTabsMap[market.id] = index;
          this.openMarketTabs.push(index < this.openedMarketTabsCountByDefault || false);
        });
      }

      // Recalculate opened markets tabs states on markets count change
      if (this.filteredMarketGroup.length !== this.openMarketTabs.length) {
        this.insertCollapsedState();
      }

      this.transformMarkets(this.filteredMarketGroup);
    }
  }

  ngOnInit(): void {
    this.sportDataSubscription = this.sportEventPageProviderService.sportData
      .subscribe(this.sportDataHandler, () => this.showError());
    this.routeChangeListener = this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.marketName = this.activatedRoute.snapshot.paramMap.get('market');
        this.init();
      }
    });
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe('sportEventPageCtrl');

    if (this.sportDataSubscription) {
      this.sportDataSubscription.unsubscribe();
    }

    if (this.routeChangeListener) {
      this.routeChangeListener.unsubscribe();
    }

    this.sportsConfigSubscription && this.sportsConfigSubscription.unsubscribe();
  }

  isYourCallMarket(market): boolean {
    const yourCallMarketPattern = /(YourCall)/i;
    return !!(market.templateMarketName && market.templateMarketName.match(yourCallMarketPattern));
  }

  protected sportDataHandler(data: {
    sport: IConstant,
    eventData: { event: ISportEvent },
    marketsByCollection: IMarketCollection[],
    eventTabs: ITab[],
    sysConfig: ISystemConfig,
    isSpecialEvent: boolean,
    liveMaketTemplateMarketName: string,
  }): void {
    if (data) {
      this.sport = data.sport;
      this.eventEntity = data.eventData.event[0];
      this.marketsByCollection = data.marketsByCollection;
      this.eventTabs = data.eventTabs;
      this.sysConfig = data.sysConfig;
      this.typeId = this.eventEntity.typeId;
      this.isSpecialEvent = data.isSpecialEvent;
      this.liveMaketTemplateMarketName = data.liveMaketTemplateMarketName;
      this.init();
    }

    this.hideSpinner();
  }

  /**
   * Generate your call markets without grouping
   */
  protected generateYourCallMarkets(): void {
    this.yourCallMarketAvailable = this.marketGroup && this.marketGroup.some(market => this.isYourCallMarket(market));

    if (this.yourCallMarketAvailable) {
      _.each(this.eventEntity.markets, marketEntity => {
        if (this.isYourCallMarket(marketEntity)) {
          marketEntity.showLimit = 5;
          // isAllShown - indicates whether use limit or not
          marketEntity.isAllShown = false;
          if (marketEntity.outcomes) {
            marketEntity.outcomes = this.templateService.sortOutcomesByPrice(marketEntity.outcomes);
          }
        }
      });
    }
  }

  /**
   * Transform markets data
   * @param {IMarket[]} markets
   */
  private transformMarkets(markets: IMarket[]): void {
    _.each(markets, (market: IMarket) => {
      market.isSmartBoosts = this.smartBoostsService.isSmartBoosts(market);

      _.each(market.outcomes, (outcome: IOutcome) => {
        if (market.viewType === 'columns-2-3' || market.viewType === 'columns-3') {
          outcome.alphabetName = this.filtersService.filterAlphabetsOnly(outcome.name);
          outcome.numbersName = this.filtersService.filterNumbersOnly(outcome.name);
        }

        if (!market.isSmartBoosts) { return; }

        const parsedName = this.smartBoostsService.parseName(outcome.name);
        if (!parsedName.wasPrice) { return; }

        outcome.name = parsedName.name;
        outcome.wasPrice = parsedName.wasPrice;
      });

      market.groupedOutcomes = _.toArray(this.filtersService.groupBy(market.outcomes, 'outcomeMeaningMinorCode'));
      market.groupedOutcomes = _.filter(market.groupedOutcomes, outcomes => !_.isEmpty(outcomes));
    });
  }

  /**
   * Group your call markets to sub categories
   */
  private createPlayerStatsMarketsGroup(): void {
    const marketsGroup: any = {
      name: (this.sysConfig.yourCallPlayerStatsName && this.sysConfig.yourCallPlayerStatsName.name) || '',
      localeName: 'playerStats',
      marketsGroup: true,
      displayOrder: 0,
      markets: []
    };

    _.each(this.eventEntity.markets, marketEntity => {
      if (marketEntity.templateMarketName && marketEntity.templateMarketName.match(/(Player_Stats_)/i)) {
        marketsGroup.markets.push(marketEntity);
        marketEntity.hidden = true;
      }
    });

    marketsGroup.markets = _.sortBy(marketsGroup.markets, 'displayOrder');
    marketsGroup.displayOrder = marketsGroup.markets[0].displayOrder;

    if (!_.find(this.marketGroup, { localeName: 'playerStats' })) {
      this.marketGroup.push(marketsGroup);
    }
  }

  /**
   * Check for showing Player Stats markets
   * @return {boolean}
   */
  private isPlayerStatsMarketsPresent(): boolean {
    const playerStatsMarketPattern = /(Player_Stats_)/i;

    return this.marketGroup && this.marketGroup.some(market =>
      !!(market.templateMarketName && market.templateMarketName.match(playerStatsMarketPattern)));
  }

  /**
   * Invoke sport extension
   * @private
   */
  private invokeSportExtension(): void {
    if (this.isFootball) {
      this.footballExtension.eventMarkets(this);
    } else if (this.sportName === 'tennis') {
      this.sportsConfigSubscription = this.sportConfigService.getSport('tennis').subscribe((tennisInstance) => {
        this.tennisExtension.eventMarkets(this, tennisInstance.sportConfig);
      });
    }
  }

  private insertCollapsedState() {
    this.filteredMarketGroup.forEach((m, i) => {
      if (this.openedMarketTabsMap[m.id] === undefined) {
        this.openedMarketTabsMap[m.id] = i;
        // insert collapsed state for new market
        this.openMarketTabs.splice(i, 0, i < this.openedMarketTabsCountByDefault || false);
      }
    });
  }

  private subscribeToEvents(): void {
    this.pubSubService.subscribe('sportEventPageCtrl', this.pubSubService.API.OUTCOME_UPDATED, () => {
      this.changeDetectorRef.detectChanges();
    });

    this.pubSubService.subscribe('sportEventPageCtrl', this.pubSubService.API.MOVE_EVENT_TO_INPLAY, (ref: IReference) => {
      if (this.eventEntity && this.eventEntity.id === ref.id) {
        this.checkBybTabs();
      }
    });

    this.pubSubService.subscribe('sportEventPageCtrl', this.pubSubService.API.REMOVE_EDP_BYB_TABS, () => {
      this.removeBybTabs();
    });
  }

  private checkBybTabs(): void {
    const isLive = this.eventEntity.eventIsLive;
    const hasBybTabs = this.eventTabs && this.eventTabs.some((tab: ITab) => this.bybTabsRe.test(tab.id));
    const isQBShown = !!this.windowRefService.document.querySelector('quickbet-yourcall-wrapper');

    if (isLive && hasBybTabs && !isQBShown) {
      this.removeBybTabs();
    }
  }

  private removeBybTabs(): void {
    // redirect to all markets tab if active tab is byb or 5aside
    if (this.activeTab && this.bybTabsRe.test(this.activeTab.id)) {
      const redirectTab = this.eventTabs.find((tab: ITab) => tab.id === 'tab-all-markets');
      this.router.navigateByUrl(redirectTab.url);
      this.pubSubService.publish(this.pubSubService.API.REMOVE_BYB_STORED_EVENT, this.eventEntity.id);
    }

    // remove tabs
    this.eventTabs = this.eventTabs.filter((tab: ITab) => !this.bybTabsRe.test(tab.id));
  }
}
