import { EventMarketsComponent } from '@edp/components/eventMarkets/event-markets.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('EventMarketsComponent', () => {
  let component: EventMarketsComponent,
    templateService,
    pubSubService;

  let eventEntity;

  beforeEach(() => {
    templateService = {
      genTerms: jasmine.createSpy('genTerms')
    };
    pubSubService = {
      subscribe: jasmine.createSpy('subscribe').and.callFake((a, b, cb) => { cb(); }),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: pubSubApi
    };
    component = new EventMarketsComponent(
      templateService,
      pubSubService
    );

    eventEntity = {
      id: 1,
      name: 'Event Name',
      eventStatusCode: 'A',
      isActive: true,
      markets: [
        {
          id: '1',
          name: 'Market Name',
          isLpAvailable: true,
          cashoutAvail: 'Y',
          outcomes: [
            {
              id: '1',
              isActive: true,
              isAvailable: true,
              marketId: '1',
              name: 'Outcome Name 1',
              outcomeStatusCode: 'A',
              prices: [
                {
                  id: '1',
                  displayOrder: '1',
                  isActive: true,
                  priceDec: 1.4,
                  priceDen: 10,
                  priceNum: 1,
                  priceType: 'LP'
                }
              ]
            },
            {
              id: '2',
              isActive: true,
              isAvailable: true,
              marketId: '1',
              name: 'Outcome Name 2',
              outcomeStatusCode: 'A',
              prices: [
                {
                  id: '1',
                  displayOrder: '1',
                  isActive: true,
                  priceDec: 1,
                  priceDen: 2,
                  priceNum: 1,
                  priceType: 'LP'
                }
              ]
            }
          ]
        }
      ]
    };

    component.eventEntity = eventEntity as any;
    component.panelType = 'outright';
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnInit methods', () => {
    component.panelType = 'enhancedMultiple';
    component.eventEntity = {
      markets: [{
        displayOrder: 3,
        outcomes: [{
          displayOrder: 2,
          prices: [{
            priceDec: 54
          }]
        }, {
          displayOrder: 1,
          prices: [{
            priceDec: 21
          }]
        }, {
          displayOrder: 3,
          prices: [{
            priceDec: 99
          }]
        }, {
          displayOrder: 3,
          prices: [{
            priceDec: 23
          }]
        }, {
          displayOrder: 4,
          name: 'b',
          prices: [{
            priceDec: 11
          }]
        }, {
          displayOrder: 4,
          name: 'a',
          prices: [{
            priceDec: 11
          }]
        }]
      }, {
        displayOrder: 2,
        outcomes: [{
          prices: []
        }]
      }, {
        displayOrder: 5,
        outcomes: [{}]
      }]
    } as any;

    const result = {
      markets: [{
        displayOrder: 2,
        outcomes: [{
          prices: []
        }]
      }, {
        displayOrder: 3,
        outcomes: [{
          displayOrder: 1,
          prices: [{
            priceDec: 21
          }]
        }, {
          displayOrder: 2,
          prices: [{
            priceDec: 54
          }]
        }, {
          displayOrder: 3,
          prices: [{
            priceDec: 23
          }]
        }, {
          displayOrder: 3,
          prices: [{
            priceDec: 99
          }]
        }, {
          displayOrder: 4,
          name: 'a',
          prices: [{
            priceDec: 11
          }]
        }, {
          displayOrder: 4,
          name: 'b',
          prices: [{
            priceDec: 11
          }]
        }]
      }, {
        displayOrder: 5,
        outcomes: [{}]
      }]
    } as any;
    component.ngOnInit();

    expect(component.showTerms).toEqual(false);
    expect(component.limit).toEqual(undefined);
    expect(component.isShowAllActive).toBeFalsy();
    expect(component.eventEntity).toEqual(result);
  });

  it('should call ngOnDestroy methods', () => {
    component.ngOnDestroy();
    expect(pubSubService.unsubscribe).toHaveBeenCalledWith('EventMarketsComponent');
  });

  it('showAll Button should be visible on featured when more than default amount of markets available', () => {
    component.isFeaturedMarkets = true;
    component.selectionsLimit = 5;
    component.eventEntity = {
      markets: [{
        outcomes: [{}, {}, {}, {}, {}, {}, {}]
      }]
    } as any;
    component.ngOnInit();
    expect(component.limit).toEqual(component.selectionsLimit);
    expect(component.isShowAllActive).toBeTruthy();
  });


  it('should call genTerms for each event market', () => {
    component.ngOnInit();
    expect(templateService.genTerms).toHaveBeenCalledTimes(eventEntity.markets.length);
  });

  it('should not call genTerms', () => {
    component.panelType = '';
    component.ngOnInit();
    expect(component.showTerms).toBeFalsy();
  });

  it('trackByIndex', () => {
    const index = 1;
    const result = component.trackByIndex(index);
    expect(result).toBe(index);
  });

  it('getTerms', () => {
    component['getTerms'](eventEntity as any);
    expect(templateService.genTerms).toHaveBeenCalledTimes(eventEntity.markets.length);
  });

  it('test toggleShow function', () => {
    component.allShown = false;

    component.toggleShow();
    expect(component.allShown).toEqual(true);
    expect(component.limit).toEqual(undefined);

    component.toggleShow();
    expect(component.allShown).toEqual(false);
    expect(component.limit).toEqual(component.selectionsLimit);
  });
});
