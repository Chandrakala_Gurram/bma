import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import * as _ from 'underscore';

import { ISportEvent } from '@core/models/sport-event.model';
import { IMarket } from '@core/models/market.model';
import { IOutcome } from '@core/models/outcome.model';

import { TemplateService } from '@shared/services/template/template.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'event-markets',
  templateUrl: './event-markets.html',
  styleUrls: ['./event-markets.less']
})
export class EventMarketsComponent implements OnInit, OnDestroy {
  @Input() eventEntity: ISportEvent;
  @Input() panelType: string;
  @Input() isFeaturedMarkets?: boolean;

  // for featured module. amount of selections above "see more/show less" button.
  @Input() selectionsLimit?: number = -1;

  // Open first 2 market tabs(0,1)
  openMarketTabs: number = 1;
  showTerms: boolean = false;
  isEachWayAvailable: boolean;

  isShowAllActive: boolean = false;
  allShown: boolean = false;
  limit: number;
  private readonly tagName: string = 'EventMarketsComponent';

  constructor(
    private templateService: TemplateService,
    private pubSubService: PubSubService
  ) { }

  ngOnInit(): void {
    const outcomesLength = this.eventEntity.markets[0].outcomes.length;
    this.sortMarkets();

    // show all button and limit only for featured module
    this.limit = this.isFeaturedMarkets ? this.selectionsLimit : undefined;
    this.isShowAllActive = this.isFeaturedMarkets && outcomesLength > 0 && outcomesLength > this.selectionsLimit;

    if (this.panelType === 'outright') {
      this.getTerms(this.eventEntity);
      this.showTerms = true;
    }

    this.pubSubService.subscribe(this.tagName, this.pubSubService.API.OUTCOME_UPDATED, () => {
      this.sortMarkets();
    });
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe(this.tagName);
  }

  toggleShow(): void {
    this.allShown = !this.allShown;

    if (this.allShown) {
      this.limit = undefined;
    } else {
      this.limit = this.selectionsLimit;
    }
  }

  trackByIndex(index): number {
    return index;
  }

  /**
   * Sort markets by displayOrder,
   *      outcomes by displayOrder or price and name
   */
  private sortMarkets(): void {
    this.eventEntity.markets
        .sort((a: IMarket, b: IMarket) => (a.displayOrder - b.displayOrder))
        .forEach((market: IMarket) => {
          market.outcomes = market.outcomes.sort((a: IOutcome, b: IOutcome) => {
            if (a.displayOrder > b.displayOrder) { return 1; }

            if (a.displayOrder === b.displayOrder) {
              const
                aPrice = a.prices && a.prices[0] && a.prices[0].priceDec,
                bPrice = b.prices && b.prices[0] && b.prices[0].priceDec;

              if (!aPrice) {
                return -1;
              }
              if (aPrice > bPrice || (aPrice === bPrice && a.name > b.name)) {
                return 1;
              }
            }

            return -1;
          });
        });
  }

  /**
   * Generates "Each Way" text string with info about odds and win places.
   * @param {Object} event
   */
  private getTerms(event: ISportEvent): void {
    _.each(event.markets, market => {
      market.terms = this.templateService.genTerms(market);
    });
  }

}
