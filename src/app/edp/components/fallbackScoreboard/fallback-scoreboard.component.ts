import { Component, Input, OnInit } from '@angular/core';

import { ISportEvent } from '@core/models/sport-event.model';
import { IScoreType, IScoreData } from '@core/services/scoreParser/models/score-data.model';

@Component({
  selector: 'fallback-scoreboard',
  templateUrl: './fallback-scoreboard.html',
  styleUrls: ['./fallback-scoreboard.less']
})

export class FallbackScoreboardComponent implements OnInit {
  isBoxScore: boolean;
  boxScoreType: string[] = ['SetsGamesPoints', 'SetsPoints', 'GamesPoints', 'BoxScore'];

  @Input() event: ISportEvent;
  @Input() score: IScoreData;
  @Input() scoreType: IScoreType;

  constructor() {}

  ngOnInit(): void {
    this.isBoxScore = this.boxScoreType.includes(this.scoreType);
  }
}
