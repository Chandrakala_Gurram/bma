import { OptaScoreboardComponent } from './opta-scoreboard.component';
import { fakeAsync } from '@angular/core/testing';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('OptaScoreboardComponent', () => {
  let component: OptaScoreboardComponent;
  let elementRef;
  let gtmService;
  let pubSubService;
  let ngZone;
  let windowRefService;
  let rendererService;
  let coreboardOverlayWrapper;

  beforeEach(fakeAsync(() => {
    coreboardOverlayWrapper = {
      classList: {
        contains: jasmine.createSpy('contains'),
        remove: jasmine.createSpy('remove'),
        add: jasmine.createSpy('add')
      }
    } as any;
    elementRef = {
      nativeElement: {
        append: jasmine.createSpy('nativeElement'),
        appendChild: jasmine.createSpy('appendChild')
      }
    };
    gtmService = {
      push: jasmine.createSpy()
    };
    pubSubService = {
      publish: jasmine.createSpy('publish'),
      API: pubSubApi,
      subscribe: jasmine.createSpy('publish')
    };
    ngZone = {
      runOutsideAngular: jasmine.createSpy('runOutsideAngular')
    };
    windowRefService = {
      document: {
        querySelector: jasmine.createSpy(),
        createElement: jasmine.createSpy().and.callFake(param => {
          switch (param) {
            case 'scoreboard-container':
              return document.createElement('scoreboard-container');
            case 'scoreboard-overlay':
              return document.createElement('scoreboard-overlay');
            case 'div':
              return document.createElement('div');
          }
        }),
        body: {
          appendChild: jasmine.createSpy('appendChild'),
          classList: {
            add: jasmine.createSpy('add'),
            remove: jasmine.createSpy('remove')
          }
        }
      },
      nativeWindow: {
        setTimeout: jasmine.createSpy('setTimeout'),
        clearTimeout: jasmine.createSpy('clearTimeout')
      }
    };
    rendererService = {
      renderer: {
        listen: jasmine.createSpy().and.callFake(() => jasmine.createSpy())
      }
    };

    component = new OptaScoreboardComponent(
      elementRef,
      gtmService,
      pubSubService,
      ngZone,
      windowRefService,
      rendererService
    );
    component.event = {} as any;
  }));

  afterEach(() => {
    spyOn(component, 'ngOnDestroy').and.callFake(() => { });
  });

  describe('@hideOptaScoreboardHandlerFn', () => {
    it('should publish event to hide scoreboard component', () => {

      component['hideOptaScoreboardHandlerFn']();
      expect(pubSubService.publish).toHaveBeenCalledWith(pubSubService.API.HIDE_OPTA_SCOREBOARD);
    });
  });

  describe('@gtmHandlerFn', () => {
    const event = { detail: '' };
    beforeEach(() => {
      spyOn(component.isLoaded, 'emit');
    });

    it('should track event', () => {
      component['gtmHandlerFn'](event);
      expect(component['gtmService'].push).toHaveBeenCalledWith('trackEvent', event.detail);
    });

    it(`should emit 'isLoaded' if 'isLoadedValue' is Falthy`, () => {
      component['isLoadedValue'] = false;

      component['gtmHandlerFn'](event);

      expect(component.isLoaded.emit).toHaveBeenCalledWith(true);
      expect(component['isLoadedValue']).toBeTruthy();
    });

    it(`should Not emit 'isLoaded' if 'isLoadedValue' is Truthy`, () => {
      component['isLoadedValue'] = true;

      component['gtmHandlerFn'](event);

      expect(component.isLoaded.emit).not.toHaveBeenCalled();
    });
  });

  describe('@getParameters', () => {
    it('if no element has sb-data attribute , should return empty object', () => {
      const div = document.createElement('div');

      const result = component['getParameters'](div);
      expect(result).toEqual({});
    });

    it('if element has JSON data in sb-data attribute, should return it contents parsed', () => {
      const div = document.createElement('div');
      div.setAttribute('sb-data', '{"foo":"bar"}');

      const result = component['getParameters'](div);
      expect(result).toEqual({ foo: 'bar' });
    });

    it('if element has non-JSON string in sb-data attribute, should return empty object', () => {
      const div = document.createElement('div');
      div.setAttribute('sb-data', 'foobar');

      const result = component['getParameters'](div);
      expect(result).toEqual({});
    });

    it('if element has non-object JSON data in sb-data attribute, should return empty object', () => {
      const div = document.createElement('div');
      div.setAttribute('sb-data', '"foobar"');

      const result = component['getParameters'](div);
      expect(result).toEqual({});
    });
  });

  describe('@initializeScoreboards', () => {
    it('should initialize scoreboard container and overlay', () => {
      spyOn<any>(component, 'setParameters');
      spyOn<any>(component, 'getParameters').and.returnValue({matchId: 12});

      component['initializeScoreboards']();
      expect(component['setParameters']).toHaveBeenCalled();
    });
  });

  it('scoreboardHandlerFn', () => {
    spyOn<any>(component, 'setParameters');

    const customEvent = {
      detail: {
        scoreboardKey: 'key'
      }
    };

    component['scoreboardOverlayWrapper'] = coreboardOverlayWrapper;
    component['scoreboardHandlerFn'](customEvent);

    expect(windowRefService.document.body.classList.add).toHaveBeenCalledWith('opta-scoreboard-overlay-shown');
  });

  it('overlayHandlerFn', () => {
    component['scoreboardOverlayWrapper'] = coreboardOverlayWrapper;
    component['overlayHandlerFn']();

    expect(coreboardOverlayWrapper.classList.remove).toHaveBeenCalledWith('visible');
    expect(component['windowRefService'].document.body.classList.remove).toHaveBeenCalledWith('opta-scoreboard-overlay-shown');
  });

  describe('ngOnDestroy', () => {
    let scoreboardOverlay;
    let scoreboardOverlayWrapper;

    beforeEach(() => {
      scoreboardOverlay = {
        removeEventListener: jasmine.createSpy('removeEventListener')
      } as any;

      scoreboardOverlayWrapper = {
        classList: {
          contains: jasmine.createSpy('contains'),
          remove: jasmine.createSpy('remove')
        }
      } as any;
    });

    it('should remove element, listener but not class', () => {
      component['scoreboardOverlayWrapper'] = scoreboardOverlayWrapper;
      component['scoreboardOverlay'] = scoreboardOverlay;

      component.ngOnDestroy();
      expect(scoreboardOverlayWrapper.classList.contains).toHaveBeenCalledWith('visible');
      expect(scoreboardOverlayWrapper.classList.remove).not.toHaveBeenCalled();
    });

    it('should not remove listener', () => {
      component['scoreboardOverlayWrapper'] = null;
      component['scoreboardOverlay'] = scoreboardOverlay;

      component.ngOnDestroy();

      expect(scoreboardOverlay.removeEventListener).not.toHaveBeenCalled();
    });

    it('should clear update carousel timeout', () => {
      const timer = 100;


      component['updateCarouselTimer'] = timer;
      component.ngOnDestroy();

      expect(windowRefService.nativeWindow.clearTimeout).toHaveBeenCalledWith(timer);
    });
  });

  describe('ngOnChanges', () => {
    it('should not set timeout for update carousel when previous value is false', () => {
      const changes = {
        toggleScoreboard: {
          previousValue: false,
          currentValue: true
        }
      } as any;


      component.ngOnChanges(changes);

      expect(windowRefService.nativeWindow.setTimeout).not.toHaveBeenCalled();
    });

    it('should not do anything if changes object do not contain toggleScoreboard item', () => {
      const changes = {} as any;


      component.ngOnChanges(changes);

      expect(windowRefService.nativeWindow.setTimeout).not.toHaveBeenCalled();
    });

    it('should not set timeout for update carousel when current value is true', () => {
      const changes = {
        toggleScoreboard: {
          previousValue: true,
          currentValue: true
        }
      } as any;


      component.ngOnChanges(changes);

      expect(windowRefService.nativeWindow.setTimeout).not.toHaveBeenCalled();
    });

    it('should set timeout for update carousel when values are met', () => {
      const changes = {
        toggleScoreboard: {
          previousValue: true,
          currentValue: false
        }
      } as any;


      component.ngOnChanges(changes);

      expect(windowRefService.nativeWindow.setTimeout).toHaveBeenCalledWith(jasmine.any(Function), 500);
    });

    it('should call function in callback', () => {
      const changes = {
        toggleScoreboard: {
          previousValue: true,
          currentValue: false
        }
      } as any;
      const scoreboardContainer = {
        carousel: {
          updateCarousel: jasmine.createSpy('updateCarousel')
        }
      } as any;
      let callback;

      windowRefService.nativeWindow.setTimeout.and.callFake(c => callback = c);

      component['scoreboardContainer'] = scoreboardContainer;
      component.ngOnChanges(changes);

      callback();

      expect(windowRefService.nativeWindow.setTimeout).toHaveBeenCalledWith(jasmine.any(Function), 500);
      expect(scoreboardContainer.carousel.updateCarousel).toHaveBeenCalled();
    });
  });

  describe('ngAfterViewInit', () => {
    it(`should publish SCOREBOARD_VISUALIZATION_LOADED event`, () => {
      component.ngAfterViewInit();

      expect(pubSubService.publish).toHaveBeenCalled();
    });
  });

  it('ngOnInit', () => {
    ngZone.runOutsideAngular.and.callFake(cb => cb());
    component.ngOnInit();
    expect(rendererService.renderer.listen).toHaveBeenCalledTimes(1);
  });

  it('ngOnDestroy', () => {
    component['windowOrientationChangeListener'] = jasmine.createSpy('windowOrientationChangeListener');
    component['scoreboardContainer'] = { remove: jasmine.createSpy('remove') } as any;
    component['scoreboardOverlayWrapper'] = {
      classList: { contains: () => true, remove: jasmine.createSpy('remove') }
    } as any;
    component['scoreboardOverlay'] = {
      removeEventListener: jasmine.createSpy('removeEventListener')
    } as any;

    component.ngOnDestroy();

    expect(component['windowOrientationChangeListener']).toHaveBeenCalledTimes(1);
    expect(component['scoreboardContainer'].remove).toHaveBeenCalledTimes(1);
    expect(component['scoreboardOverlayWrapper'].classList.remove).toHaveBeenCalledTimes(1);
    expect(windowRefService.document.body.classList.remove).toHaveBeenCalledTimes(1);
    expect(component['scoreboardOverlay'].removeEventListener).toHaveBeenCalledTimes(1);
  });
});
