/* --- Default--- */
// const config = ${config};

const oxygenEnvConfig: {
  brand: string;
  version: string;
  production: boolean;
  ENVIRONMENT: string;
  CURRENT_PLATFORM: string;
  DOMAIN: string;
  EVENT_CATEGORIES_WITH_WATCH_RULES: string[],
  PIROZHOK_API_ENDPOINT: string;
  SITESERVER_VERSION: string;
  SITESERVER_DNS: string;
  SITESERVER_BASE_URL: string;
  SITESERVER_ENDPOINT: string;
  SITESERVER_COMMON_ENDPOINT: string;
  SITESERVER_LOTTERY_ENDPOINT: string;
  SITESERVER_HISTORIC_ENDPOINT: string;
  SITESERVER_COMMENTARY_ENDPOINT: string;
  IMAGES_RACE_ENDPOINT: string;
  SURFACE_BETS_URL: string;
  IMAGES_ENDPOINT: string;
  INT_TOTE_IMAGES_ENDPOINT: string;
  CMS_ENDPOINT: string;
  CMS_ROOT_URI: string;
  CMS_LINK: string;
  BP_URI: string;
  PERFORM_GROUP_END_POINT: string;
  PERFORM_GROUP_END_POINT_DESKTOP: string;
  IMG_END_POINT: string;
  ATR_END_POINT: string;
  QUANTUMLEAP_SCRIPT_ENDPOINT: string;
  QUANTUMLEAP_BOOKMAKER: string;
  VISUALIZATION_ENDPOINT: string;
  VISUALIZATION_TENNIS_ENDPOINT: string;
  VISUALIZATION_IFRAME_URL: string;
  VISUALIZATION_PREMATCH_URL: string;
  DIGITAL_SPORTS_IFRAME_URL: string;
  DIGITAL_SPORTS_SYSTEM_ID: number;
  STATS_CENTRE_ENDPOINT: string;
  OPT_IN_ENDPOINT: string;
  ONE_TWO_FREE_ENDPOINT: string;
  IG_MEDIA_TOTE_ENDPOINT: string;
  IG_MEDIA_ENDPOINT: string;
  IGM_STREAM_SERVICE_ENDPOINT: string;
  APOLLO: {
    API_ENDPOINT: string;

    CWA_ROUTE: string;
    UCMS_ROUTE: string;
  },
  BET_FINDER_ENDPOINT: string;
  BET_FILTER_ENDPOINT: string;
  BET_TRACKER_ENDPOINT: string;
  SHOP_LOCATOR_ENDPOINT: string;
  NOTIFICATION_CENTER_ENDPOINT: string;
  LIVESERV: {
    DOMAIN: string;
    PUSH_COMMON_SUBDOMAIN: string;
    PUSH_LOCATION_HANDLER: string;
    PUSH_URL: string;
    PUSH_INSTANCE_URL: string;
    PUSH_INSTANCE_PORT: string;
    PUSH_INSTANCE_TYPE: string;
    PUSH_IFRAME_ID: string;
    PUSH_VERSION: string;
  },
  BR_TYPE_ID: string[];
  CATEGORIES_DATA: any;
  LOTTERIES_CONFIG: any;
  TOTE_CLASSES: {
    greyhounds: number;
    horseracing: number;
    trotting: number;
  },
  OPTA_SCOREBOARD: {
    CDN: string;
    ENV: string;
  },
  mobileWidth: number;
  landTabletWidth: number;
  desktopWidth: number;
  FEATURED_SPORTS: string;
  INPLAYMS: string;
  CASHOUT_MS: string;
  LIVESERVEMS: string;
  REMOTEBETSLIPMS: string;
  EDPMS: string;
  BIG_COMPETITION_MS: string;
  BET_GENIUS_SCOREBOARD: {
    scorecentre: string;
    api: string;
  },
  AEM_CONFIG: {
    server: string;
    at_property: string;
    betslip_at_property: string;
  },
  HELP_SUPPORT_URL: string;
  HELP_LOTTO_RULES: string;
  RULE_FOUR_URL: string;
  TIME_ENDPOINT: string;
  TIMEFORM_ENDPOINT: string;
  RACING_POST_API_ENDPOINT: string;
  RACING_POST_API_KEY: string;
  GAMING_URL: string[];
  VS_QUOLITY_MAP: {
    desktop: string;
    tablet: string;
    mobile: string;
  },
  BYB_CONFIG: {
    HR_YC_EVENT_TYPE_ID: number;
    uri: string;
  },
  HORSE_RACING_CATEGORY_ID: string;
  TOTE_CATEGORY_ID: string;
  EXTERNAL_URL: {
    gaming: string;
    help: string;
    'accountone-stg': string;
    'accountone-test': string;
  },
  MINI_GAMES_IFRAME_URL: string;
  VIP_USERS_EXTERNAL_URL: string;
  DESKTOP_VIP_USERS_EXTERNAL_URL: string;
  STORAGE_SERVICE_PREFIX: string;
  LIVE_COMMENTARY_URL: string;
  QUESTION_ENGINE_ENDPOINT: string;
  googleTagManagerID: string[];
  QUBIT_SCRIPT: string;
  TIMELINE_MS: string;
  BETRADAR_WIDGETLOADERURL: string;
} = (window as any).oxygenEnvConfig || (config as any);

export default oxygenEnvConfig;
