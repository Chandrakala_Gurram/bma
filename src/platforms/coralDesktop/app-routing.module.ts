import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AzSportsPageComponent } from '@sharedModule/components/azSportPage/az-sports-page.component';
import { LoggedInGuard } from '@coreModule/guards/logged-in-guard.service';
import { ContactUsComponent } from '@bmaModule/components/contactUs/contact-us.component';
import { NotFoundComponent } from '@bmaModule/components/404/404.component';
import { MaintenanceComponent } from '@sharedModule/components/maintenance/maintenance.component';
import { IRetailConfig } from '@coreModule/services/cms/models/system-config';
import { IRouteData } from '@coreModule/models/route-data.model';
import { StaticComponent } from '@bmaModule/components/static/static.component';
import { LazyRouteGuard } from '@coreModule/guards/lazy-route-guard.service';

import { SiteRootGuard } from '@vanilla/core/core';
import { VANILLA_ROUTES } from '@vanilla/core/features';
import { LABELHOST_FEATURES_ROUTES, routeData } from '@labelhost/core/features';
import { RootComponent } from '@coralDesktop/app.component';
import { ROOT_APP_ROUTES } from '@vanillaInitModule/root-routes.definitions';
import { CustomPreloadingStrategy } from '@vanillaInitModule/custom.preload.strategy';

import { routes as SbRoutes } from '@sbModule/sb-routing.module';
import { routes as RacingRoutes } from '@coralDesktop/racing/racing-routing.module';
import { routes as OlympicsRoutes } from '@olympicsModule/olympics-route.module';
import { routes as BigCompetitionsRoutes } from '@bigCompetitionsModule/big-competitions-routing.module';
import { routes as BetFinderRoutes } from '@betFinderModule/betfinder-routing.module';

import { LogoutResolver } from '@vanillaInitModule/services/logout/logout.service';
import { DesktopHomeComponent } from '@coralDesktop/bma/components/home/home.component';
import { ProductSwitchResolver } from '@app/host-app/product-switch.resolver';
import { DepositRedirectGuard } from '@coreModule/guards/deposit-redirect.guard';
import { SignUpRouteGuard } from '@core/guards/sign-up-guard.service';
import { MaintenanceGuard } from '@core/guards/maintenance-guard.service';
import { DesktopLiveStreamWrapperComponent } from '@coralDesktop/bma/components/liveStream/live-stream-wrapper.component';
import { NotFoundPageGuard } from '@core/guards/not-found-page-guard.service';

const appRoutes: Routes = [
  {
    path: '',
    component: RootComponent,
    data: {
      ...routeData({ allowAnonymous: true }),
      product: 'host',
      segment: 'main'
    },
    runGuardsAndResolvers: 'always',
    resolve: {
      __p: ProductSwitchResolver
    },
    canActivate: [SiteRootGuard],
    children: [
      {
        path: 'en',
        data: {
          segment: 'vanilla'
        },
        children: [
          ...VANILLA_ROUTES,
          ...LABELHOST_FEATURES_ROUTES,
          ...ROOT_APP_ROUTES,
          {
            path: '**',
            redirectTo: '/'
          }
        ]
      },
      ...SbRoutes,
      ...RacingRoutes,
      ...OlympicsRoutes,
      ...BigCompetitionsRoutes,
      ...BetFinderRoutes,
      {
        path: '',
        component: DesktopHomeComponent,
        data: {
          segment: 'home'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            loadChildren: '@featuredModule/featured.module#FeaturedModule',
            data: {
              segment: 'home'
            }
          }
        ]
      }, {
        path: 'live-stream',
        component: DesktopLiveStreamWrapperComponent,
        data: {
          segment: 'liveStream'
        }
      }, {
        path: 'az-sports',
        component: AzSportsPageComponent,
        data: {
          segment: 'azSports'
        }
      }, {
        path: 'logout',
        component: DesktopHomeComponent,
        resolve: {
          logout: LogoutResolver
        },
        data: {
          segment: 'logout'
        }
      }, {
        path: 'settings',
        loadChildren: '@bma/components/userSettings/user-settings.module#UserSettingsModule',
        canActivate: [LoggedInGuard],
        runGuardsAndResolvers: 'always',
        data: {
          segment: 'settings'
        }
      }, {
        path: 'contact-us',
        component: ContactUsComponent,
        data: {
          segment: 'contactUs'
        }
      }, {
        path: 'static/:static-block',
        component: StaticComponent,
        data: {
          segment: 'static'
        }
      }, {
        path: 'freebets',
        loadChildren: '@freebetsModule/freebets.module#FreebetsModule',
        canActivate: [LoggedInGuard],
        data: {
          segment: 'freebets'
        }
      }, {
        path: '404',
        component: NotFoundComponent,
        data: {
          segment: '404'
        }
      }, {
        path: 'under-maintenance',
        component: MaintenanceComponent,
        resolve: { data: MaintenanceGuard },
        canDeactivate: [MaintenanceGuard]
      }, {
        path: 'deposit',
        component: DesktopHomeComponent,
        canActivate: [DepositRedirectGuard]
      },
      // Modules lazy loaded by routes
      // Yourcall
      {
        path: 'yourcall-lazy-load',
        loadChildren: '@yourCallModule/your-call.module#YourCallModule'
      },
      {
        path: 'tote-information',
        redirectTo: 'tote/information'
      }, {
        path: 'tote',
        loadChildren: '@coralDesktop/tote/tote.module#ToteModule'
      }, {
        path: 'virtual-sports',
        loadChildren: '@coralDesktop/vsbr/vsbr.module#VsbrModule'
      }, {
        path: 'in-play',
        loadChildren: '@inplayModule/inplay.module#InplayModule',
        data: {
          preload: true
        }
      }, {
        path: 'byb-module',
        loadChildren: '@bybHistoryModule/byb-history.module#LazyBybHistoryModule',
        canActivate: [ LazyRouteGuard ]
      }, {
        path: 'betslip/add/:outcomeId',
        loadChildren: '@betslipModule/betslip.module#BetslipModule'
      }, {
        path: 'betslip/unavailable',
        loadChildren: '@betslipModule/betslip.module#BetslipModule'
      },
      {
        path: 'competitions/:sport/:className/:typeName',
        data: {
          segment: 'competitionTypeEvents'
        },
        loadChildren: '@lazy-modules-module/competitionsSportTab/competitionsSportTab.module#CompetitionsTabModule'
      },
      // Odds Boost module lazy loading
      {
        path: 'oddsboost',
        loadChildren: '@oddsBoostModule/odds-boost.module#OddsBoostModule'
      },
      // Bet History module lazy loading
      {
        path: 'cashout',
        loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule'
      },
      {
        path: 'forecast-tricast-lazy-load',
        loadChildren: '@lazy-modules-module/forecastTricast/forecastTricast.module#ForecastTricastModule',
        canActivate: [ LazyRouteGuard ]
      },
      {
        path: 'event-video-stream-lazy-load',
        loadChildren: '@lazy-modules-module/eventVideoStream/event-video-stream.module#LazyEventVideoStreamModule',
        canActivate: [ LazyRouteGuard ]
      },
      // Bet radar lazy loading
      {
        path: 'bet-radar-lazy-load',
        loadChildren: '@lazy-modules-module/betRadarProvider/bet-radar.module#BetRadarCoralModule',
        canActivate: [ LazyRouteGuard ]
      },
      // Receipt header lazy loading
      {
        path: 'receipt-header-lazy-load',
        loadChildren: '@lazy-modules-module/receiptHeader/receipt-header.module#ReceiptHeaderModule',
        canActivate: [ LazyRouteGuard ]
      },
      // Runner-spotlight lazy loading
      {
        path: 'runner-spotlight-lazy-load',
        loadChildren: '@lazy-modules-module/runnerSpotlight/runner-spotlight.module#RunnerSpotlightModule',
        canActivate: [LazyRouteGuard]
      },
      {
        path: 'open-bets',
        loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule'
      }, {
        path: 'bet-history',
        loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule'
      }, {
        path: 'in-shop-bets',
        loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule'
      },
      // Betslip module lazy loading
      {
        path: 'voucher-code',
        loadChildren: '@betslipModule/betslip.module#BetslipModule'
        },
      // Lotto module lazy loading
      {
        path: 'lotto-results',
        redirectTo: 'lotto/results'
      }, {
        path: 'lotto',
        loadChildren: '@lottoModule/lotto.module#LottoModule'
      },
      // Retail module lazy loading
      {
        path: 'retail',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'menu'
        } as IRouteData<IRetailConfig>
      },
      {
        path: 'retail-upgrade',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'upgrade'
        } as IRouteData<IRetailConfig>
      },
      {
        path: 'bet-filter',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'footballFilter'
        } as IRouteData<IRetailConfig>
      },
      {
        path: 'bet-filter/:child',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'footballFilter'
        } as IRouteData<IRetailConfig>
      },
      {
        path: 'bet-filter/:child/:child',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'footballFilter'
        } as IRouteData<IRetailConfig>
      },
      {
        path: 'bet-tracker',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'shopBetTracker'
        } as IRouteData<IRetailConfig>
      },
      {
        path: 'shop-locator',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'shopLocator'
        } as IRouteData<IRetailConfig>
      },
      // Quickbet
      {
        path: 'quickbet-lazy-load',
        loadChildren: '@quickbetModule/quickbet.module#QuickbetModule',
        canActivate: [LazyRouteGuard]
      },
      // Quick Deposit
      {
        path: 'quick-deposit-lazy-load',
        loadChildren: '@quickDepositModule/quick-deposit.module#QuickDepositModule',
        canActivate: [ LazyRouteGuard ]
      },
      {
        path: 'promotions',
        loadChildren: '@promotionsModule/promotions.module#PromotionsModule',
        data: {
          path: 'promotions/retail',
          feature: 'promotions'
        }
      },
      // EDP
      {
        path: 'edp-lazy-load',
        loadChildren: '@edpModule/edp.module#EdpModule',
        canActivate: [LazyRouteGuard]
      },
      {
        path: 'signup',
        canActivate: [SignUpRouteGuard],
        children: []
      },
      // Correct4
      {
        path: 'correct4',
        loadChildren: '@questionEngine/question-engine.module#QuestionEngineModule',
        data: {
          segment: 'question-engine'
        }
      },
      // Other Question Engine versions
      {
        path: 'qe/:sourceId',
        loadChildren: '@questionEngine/question-engine.module#QuestionEngineModule',
        data: {
          segment: 'question-engine'
        }
      },
      {
        path: '**',
        component: NotFoundComponent,
        canActivate: [ NotFoundPageGuard ]
      }
    ]
  }];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        useHash: false,
        paramsInheritanceStrategy: 'always',
        onSameUrlNavigation: 'reload',
        preloadingStrategy: CustomPreloadingStrategy
        // enableTracing: true  // <-- debugging purposes only
      }
    )
  ],
  exports: [
    RouterModule
  ],
  declarations: [],
  providers: [
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    CustomPreloadingStrategy
  ]
})
export class AppRoutingModule { }
