import { Component, ViewEncapsulation } from '@angular/core';
import { AccordionComponent as AppAccordionComponent } from '@shared/components/accordion/accordion.component';

@Component({
  selector: 'accordion',
  templateUrl: 'accordion.component.html',
  styleUrls: ['./accordion.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class AccordionComponent extends AppAccordionComponent {
}

