import { Component, OnInit } from '@angular/core';
import { CmsService } from '@core/services/cms/cms.service';
import { IDesktopQuickLink } from '@core/services/cms/models';

@Component({
  selector: 'quick-links',
  templateUrl: './quick-links.component.html',
  styleUrls: ['./quick-links.component.less']
})
export class QuickLinksComponent implements OnInit {
  quickLinks: IDesktopQuickLink[] = [];

  constructor(
    private cmsService: CmsService
  ) { }

  ngOnInit() {
    // TODO reduce amounts of HTTP calls on pages.
    this.cmsService.getDesktopQuickLinks()
      .subscribe((data: IDesktopQuickLink[]) => {
        this.quickLinks = data.map(el => {
          el.target = `/${el.target.replace(/^\/+/, '')}`;
          return el;
        });
      });
  }

  trackByQuickLink(index: number, link: IDesktopQuickLink): string {
    return `${index}${link.target}`;
  }

}
