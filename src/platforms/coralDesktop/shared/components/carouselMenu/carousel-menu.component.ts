import {
  Component,
  ViewChild,
  ElementRef,
  ChangeDetectionStrategy
} from '@angular/core';
import { CarouselMenuComponent as AppCarouselMenuComponent } from '@shared/components/carouselMenu/carousel-menu.component';
@Component({
  selector: 'carousel-menu',
  styleUrls: ['../../../../../app/shared/components/carouselMenu/carousel-menu.component.less'],
  templateUrl: './carousel-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarouselMenuComponent extends AppCarouselMenuComponent {

  @ViewChild('menuContainer') menuContainer: ElementRef;

  isShowLeftArrow: boolean = false;
  isShowRightArrow: boolean = false;

  private readonly SCROLL_STEP: number = 50;

  /**
   * On mouseover action
   */
  mouseOver(): void {
    this.isShowLeftArrow = this.isScrollLeftAvailable();
    this.isShowRightArrow = this.isScrollRightAvailable();
  }

  /**
   * On mouseleave action
   */
  mouseLeave(): void {
    this.isShowLeftArrow = false;
    this.isShowRightArrow = false;
  }

  /**
   * Scroll menuContainer left
   * @param event
   */
  scrollLeft(event: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }
    this.menuContainer.nativeElement.scrollLeft = this.domTools.getScrollLeft(this.menuContainer.nativeElement) - this.SCROLL_STEP;
  }

  /**
   * Scroll menuContainer right
   * @param event
   */
  scrollRight(event: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }

    this.menuContainer.nativeElement.scrollLeft = this.domTools.getScrollLeft(this.menuContainer.nativeElement) + this.SCROLL_STEP;
  }


  /**
   * Get width of scroll inner element
   * @returns {number}
   * @private
   */
  private getInnerWidth(): number {
    if (this.menuContainer) {
      return this.menuContainer.nativeElement.firstChild.scrollWidth;
    }
    return 0;
  }

  /**
   * Check if scroll available
   * @returns {*|boolean}
   * @private
   */
  private isScrollAvailable(): boolean {
    return this.menuContainer && this.domTools.getWidth(this.menuContainer.nativeElement) < this.getInnerWidth();
  }

  /**
   * Check if left scroll arrow available
   * @returns {*|boolean}
   */
  private isScrollLeftAvailable(): boolean {
    return this.isScrollAvailable() && this.domTools.getScrollLeft(this.menuContainer.nativeElement) > 0;
  }

  /**
   * Check if right scroll arrow available
   * @returns {*|boolean}
   */
  private isScrollRightAvailable(): boolean {
    return this.isScrollAvailable() &&
      this.domTools.getScrollLeft(this.menuContainer.nativeElement) <
      this.getInnerWidth() - this.domTools.getWidth(this.menuContainer.nativeElement);
  }

}
