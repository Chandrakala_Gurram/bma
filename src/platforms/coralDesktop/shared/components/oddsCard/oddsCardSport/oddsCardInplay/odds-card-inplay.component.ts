import { Component, Input } from '@angular/core';
import { OddsCardSportComponent } from '@coralDesktop/shared/components/oddsCard/oddsCardSport/odds-card-sport.component';

@Component({
  selector: 'odds-card-inplay',
  templateUrl: 'odds-card-inplay.component.html'
})

export class OddsCardInplayComponent {
  @Input() gtmModuleTitle?: string;
  @Input() oddsCard: OddsCardSportComponent;
}
