import { Component } from '@angular/core';
import { OddsCardHeaderComponent as AppOddsCardHeaderComponent } from '@shared/components/oddsCardHeader/odds-card-header.component';

@Component({
  selector: 'odds-card-header',
  templateUrl: 'odds-card-header.component.html'
})

export class OddsCardHeaderComponent extends AppOddsCardHeaderComponent {}
