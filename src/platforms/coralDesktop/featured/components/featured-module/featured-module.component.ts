import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import * as _ from 'underscore';
import { Router } from '@angular/router';

import { IOutputModule } from '@featured/models/output-module.model';
import { IFeaturedModel } from '@featured/models/featured.model';
import { ISportEvent } from '@core/models/sport-event.model';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { SportEventHelperService } from '@core/services/sportEventHelper/sport-event-helper.service';
import { TemplateService } from '@shared/services/template/template.service';
import { FeaturedModuleService } from '@featured/services/featuredModule/featured-module.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { CommentsService } from '@core/services/comments/comments.service';
import { WsUpdateEventService } from '@core/services/wsUpdateEvent/ws-update-event.service';
import { CmsService } from '@core/services/cms/cms.service';
import { GtmService } from '@core/services/gtm/gtm.service';
import { FeaturedModuleComponent } from '@featured/components/featured-module/featured-module.component';
import { PromotionsService } from '@promotions/services/promotions/promotions.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { UserService } from '@core/services/user/user.service';
import { NewRelicService } from '@core/services/newRelic/new-relic.service';
import { EventService } from '@sb/services/event/event.service';
import { VirtualSharedService } from '@shared/services/virtual/virtual-shared.service';

@Component({
  selector: 'featured-module',
  templateUrl: './featured-module.component.html',
  styleUrls: ['./featured-module.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DesktopFeaturedModuleComponent extends FeaturedModuleComponent {
  gtmDataLayer: {} = {};

  constructor(
    locale: LocaleService,
    filtersService: FiltersService,
    windowRef: WindowRefService,
    pubsub: PubSubService,
    featuredModuleService: FeaturedModuleService,
    templateService: TemplateService,
    commentsService: CommentsService,
    wsUpdateEventService: WsUpdateEventService,
    sportEventHelper: SportEventHelperService,
    cmsService: CmsService,
    promotionsService: PromotionsService,
    changeDetectorRef: ChangeDetectorRef,
    routingHelperService: RoutingHelperService,
    router: Router,
    gtmService: GtmService,
    newRelicService: NewRelicService,
    user: UserService,
    eventService: EventService,
    virtualSharedService: VirtualSharedService
) {
    super(locale, filtersService, windowRef, pubsub, featuredModuleService, templateService, commentsService,
      wsUpdateEventService, sportEventHelper, cmsService, promotionsService, changeDetectorRef, routingHelperService, router, gtmService,
      newRelicService, user, eventService, virtualSharedService);
    }

  /**
   * Function is responsible for initialization of featuredModule
   *
   * @param {IFeaturedModel} featured
   */
  init(featured: IFeaturedModel): void {
    if (featured === null) { return; }
    featured.modules = this.filterModules(featured.modules);
    this.gtmDataLayer = {
      eventAction: 'featured events',
      eventLabel: 'additional markets'
    };
    super.init(featured);
    this.isModuleAvailable = this.isFeaturedModuleAvailable;
  }

  /**
   * Check if accordion header should be hidden
   * @param module
   * @returns {boolean}
   */
  isHeaderHidden(module: IOutputModule): boolean {
    return this.isFeaturedOffer(module);
  }

  /**
   * Check if accordion should be expanded
   * @param module
   * @returns {Boolean}
   */
  isExpanded(module: IOutputModule): boolean {
    const isExpanded = module.showExpanded || this.isFeaturedOffer(module);
    if (isExpanded) {
      this.manageSocketSubscription(module, isExpanded);
    }
    return isExpanded;
  }

  /**
   * Send tracking data on show more click
   */
  sendToGTM(): void {
    this.gtmService.push('trackEvent', {
      eventCategory: 'home',
      eventAction: 'featured events',
      eventLabel: 'view all'
    });
  }

  /**
   * Operations on module update receiving
   * @param {Object} module
   */
  onModuleUpdate(module: IOutputModule): void {
    (module.data as ISportEvent[]) = this.removeInPlayEvents(module);
    super.onModuleUpdate(module);
  }

  /**
   * Check if event is featured offer
   * @param module
   * @returns {boolean|string}
   */
  private isFeaturedOffer(module: IOutputModule): boolean {
    return module.isSpecial || module.isEnhanced;
  }

  /**
   * Filtering InPlay events
   * @param module
   * @return {Array}
   */
  private removeInPlayEvents(module: IOutputModule): ISportEvent[] {
    return _.filter(module.data, event => !event.eventIsLive);
  }

  /**
   * Excluded from desktop version modules
   * @param module
   */
  private isExcludedModule(module: IOutputModule) {
    const isMarketModule = (module.dataSelection && module.dataSelection.selectionType === 'Market');
    const isEventIdModule = (module.dataSelection && module.dataSelection.selectionType === 'Event');

    return isMarketModule || isEventIdModule;
  }

  /**
   * Filtering modules with InPlay events
   * @param modules
   * @return {Object} filtered modules
   */
  private filterModules(modules: IOutputModule[]): IOutputModule[] {
    return _.filter(modules, (module: IOutputModule) => {
      (module.data as ISportEvent[]) = this.removeInPlayEvents(module);
      return this.isEventsModule(module) && module.hasNoLiveEvents && !this.isExcludedModule(module);
    });
  }
  /**
   * Check if module type is - EventsModule
   */
  private isEventsModule(module: IOutputModule): boolean {
    return module['@type'] === 'EventsModule';
  }
}
