import { Component } from '@angular/core';

import { CouponsDetailsComponent as CoreCouponsDetailsComponent } from '@sb/components/couponsDetails/coupons-details.component';

@Component({
  selector: 'coupons-details',
  templateUrl: './coupons-details.component.html'
})
export class CouponsDetailsComponent extends CoreCouponsDetailsComponent {
  protected getStickyElementsHeight(): number {
    const headerElement = this.windowRefService.document.querySelector('header');
    return !headerElement ? 0 : this.domToolsService.getHeight(headerElement);
  }
}
