import { Component } from '@angular/core';
import { SportMainComponent as CoreSportMainComponent } from '@app/sb/components/sportMain/sport-main.component';
import { ISportConfigTab } from '@root/app/core/services/cms/models/sport-config-tab.model';

@Component({
  selector: 'sport-main-component',
  styleUrls: ['sport-main.component.less'],
  templateUrl: 'sport-main.component.html'
})
export class SportMainComponent extends CoreSportMainComponent {

  get sportDetailPage(): string {
    return this.isSportDetailPage ? 'eventDetailsPage' : '';
  }

  protected shouldNavigatedToTab() {
    return this.isHomeUrl();
  }

  protected filterTabs(sportTabs: ISportConfigTab[]): ISportConfigTab[] {
    sportTabs.forEach((tab: ISportConfigTab, index: number) => {
      if (tab.id === 'tab-matches' || tab.id === 'tab-live') {
        tab.hidden = false;

      }
      this.defaultTab = 'matches';
    });

    if (!sportTabs.find(sportTab => sportTab.id === 'tab-live')) {
      sportTabs.unshift({
        id: 'tab-live',
        name: 'live',
        label: 'sb.tabsNameInPlay',
        url: `/sport/${this.sportPath}/live`,
        hidden: false,
        sortOrder: 1
      });
    }

    return sportTabs;
  }
}
