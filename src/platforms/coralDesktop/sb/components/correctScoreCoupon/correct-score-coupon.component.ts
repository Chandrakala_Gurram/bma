import { Component } from '@angular/core';

import {
  CorrectScoreCouponComponent as CoreCorrectScoreCouponComponent
} from '@sb/components/correctScoreCoupon/correct-score-coupon.component';

@Component({
  selector: 'correct-score-coupon',
  templateUrl: 'correct-score-coupon.component.html',
  styleUrls: ['correct-score-coupon.component.less']
})
export class CorrectScoreCouponComponent extends CoreCorrectScoreCouponComponent { }
