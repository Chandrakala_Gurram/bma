import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { VirtualSportClassesComponent } from '@app/vsbr/components/virtualSportClasses/virtual-sport-classes.component';

// Overridden
import { DesktopVirtualSportsPageComponent } from '@coralDesktop/vsbr/components/virtualSportsPage/virtual-sports-page.component';

const routes: Routes = [
  {
    path: '',
    component: DesktopVirtualSportsPageComponent,
    data: {
      segment: 'virtual-sports'
    },
    children: [
      {
        path: ':category',
        component: VirtualSportClassesComponent,
        data: {
          segment: 'virtual-sports.category'
        }
      },
      {
        path: ':category/:alias',
        component: VirtualSportClassesComponent,
        data: {
          segment: 'virtual-sports.class'
        },
      },
      {
        path: ':category/:alias/:eventId',
        component: VirtualSportClassesComponent,
        data: {
          segment: 'virtual-sports.vhrEvent'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class VsbrRoutingModule {}
