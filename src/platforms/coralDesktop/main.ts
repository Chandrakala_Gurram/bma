import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';
import environment from '@environment/oxygenEnvConfig';

if (environment.production) {
  enableProdMode();
}

const cmsInitUrl = `${ environment.CMS_ENDPOINT }/${ environment.brand }/initial-data/${ environment.CURRENT_PLATFORM }`,
  cmsInitPromise = fetch(cmsInitUrl).then(data => data.json());

const cmsConfigProvider = {
  provide: 'CMS_CONFIG',
  useValue: cmsInitPromise
};

platformBrowserDynamic([cmsConfigProvider])
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));
