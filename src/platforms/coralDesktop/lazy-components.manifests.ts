import {
  IDynamicComponentManifest,
  DYNAMIC_SLIDE_OUT_BETSLIP,
  DYNAMIC_BET_HISTORY_PAGE,
  DYNAMIC_CASH_OUT_PAGE,
  DYNAMIC_OPEN_BETS,
  DYNAMIC_IN_SHOP_BETS_PAGE,
  DYNAMIC_MY_BETS,
  DYNAMIC_RETAIL_OVERLAY,
  DYNAMIC_BET_TRACKER,
  DYNAMIC_ODDS_BOOST_BS_HEADER,
  DYNAMIC_ODDS_BOOST_PRICE,
  DYNAMIC_ODDS_BOOST_BUTTON,
  DYNAMIC_ODDS_BOOST_INFO_ICON,
  DYNAMIC_ODDS_BOOST_NOTIFICATION,
  DYNAMIC_RACING_EVENT,
  DYNAMIC_YC_ICON
} from '@app/dynamicLoader/dynamic-loader-manifest';

export const lazyComponentsManifests: IDynamicComponentManifest[] = [
  {
    path: 'SlideOutBetslip',
    loadChildren: '@betslipModule/betslip.module#BetslipModule',
    componentToken: DYNAMIC_SLIDE_OUT_BETSLIP,
    componentName: 'SlideOutBetslipComponent'
  },
  {
    path: 'BetHistoryPage',
    loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule',
    componentToken: DYNAMIC_BET_HISTORY_PAGE,
    componentName: 'BetHistoryPageComponent'
  },
  {
    path: 'CashOutPage',
    loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule',
    componentToken: DYNAMIC_CASH_OUT_PAGE,
    componentName: 'CashOutPageComponent'
  },
  {
    path: 'OpenBets',
    loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule',
    componentToken: DYNAMIC_OPEN_BETS,
    componentName: 'OpenBetsComponent'
  },
  {
    path: 'InShopBetsPage',
    loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule',
    componentToken: DYNAMIC_IN_SHOP_BETS_PAGE,
    componentName: 'InShopBetsPageComponent'
  },
  {
    path: 'MyBets',
    loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule',
    componentToken: DYNAMIC_MY_BETS,
    componentName: 'MyBetsComponent'
  },
  {
    path: 'RetailOverlay',
    loadChildren: 'app/retail/retail.module#RetailModule',
    componentToken: DYNAMIC_RETAIL_OVERLAY,
    componentName: 'RetailOverlayComponent'
  },
  {
    path: 'BetTracker',
    loadChildren: 'app/retail/retail.module#RetailModule',
    componentToken: DYNAMIC_BET_TRACKER,
    componentName: 'BetTrackerComponent'
  },
  {
    path: 'OddsBoostBetslipHeader',
    loadChildren: '@oddsBoostModule/odds-boost.module#OddsBoostModule',
    componentToken: DYNAMIC_ODDS_BOOST_BS_HEADER,
    componentName: 'OddsBoostBetslipHeaderComponent'
  },
  {
    path: 'OddsBoostPrice',
    loadChildren: '@oddsBoostModule/odds-boost.module#OddsBoostModule',
    componentToken: DYNAMIC_ODDS_BOOST_PRICE,
    componentName: 'OddsBoostPriceComponent'
  },
  {
    path: 'OddsBoostButton',
    loadChildren: '@oddsBoostModule/odds-boost.module#OddsBoostModule',
    componentToken: DYNAMIC_ODDS_BOOST_BUTTON,
    componentName: 'OddsBoostButtonComponent'
  },
  {
    path: 'OddsBoostInfoIcon',
    loadChildren: '@oddsBoostModule/odds-boost.module#OddsBoostModule',
    componentToken: DYNAMIC_ODDS_BOOST_INFO_ICON,
    componentName: 'OddsBoostInfoIconComponent'
  },
  {
    path: 'OddsBoostCountNotification',
    loadChildren: '@oddsBoostModule/odds-boost.module#OddsBoostModule',
    componentToken: DYNAMIC_ODDS_BOOST_NOTIFICATION,
    componentName: 'OddsBoostCountNotificationComponent'
  },
  {
    path: 'RacingEventMain',
    loadChildren: '@racingModule/racing.module#RacingModule',
    componentToken: DYNAMIC_RACING_EVENT,
    componentName: 'RacingEventMainComponent'
  },
  {
    path: 'YourCallIcon',
    loadChildren: '@yourCallModule/your-call.module#YourCallModule',
    componentToken: DYNAMIC_YC_ICON,
    componentName: 'YourCallIconComponent'
  }
];
