import { HttpClientModule } from '@angular/common/http';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { RacingRoutingModule } from './racing-routing.module';
import { QuantumLeapComponent } from '@racing/components/quantumLeap/quantum-leap.component';
import { RacingTabYourcallComponent } from '@racing/components/racingTabYourcall/racing-tab-yourcall.component';
import { TimeFormSummaryComponent } from '@racing/components/timeformSummary/time-form-summary.component';
import { SortByOptionsService } from '@racing/services/sortByOptions/sort-by-options.service';
import { SharedModule } from '@sharedModule/shared.module';
import { RacingGaService } from '@racing/services/racing-ga.service';
import { RoutesDataSharingService } from '@racing/services/routesDataSharing/routes-data-sharing.service';
import { DesktopRacingMainComponent } from './components/racingMain/racing-main.component';
import { RacingTabsMainComponent } from '@racing/components/racingTabsMain/racing-tabs-main.component';
import { RacingEventMainComponent } from '@racing/components/racingEventMain/racing-event-main.component';
import { AboutWatchFreeComponent } from '@racing/components/aboutWatchFreeWidget/about-watch-free.component';
import { DailyRacingModuleComponent } from '@racing/components/dailyRacing/daily-racing.component';
import { RacingEnhancedMultiplesService } from '@racing/components/racingEnhancedMultiples/racing-enhanced-multiples.service';
import { RacingEnhancedMultiplesComponent
} from '@coralDesktop/racing/components/racingEnhancedMultiples/racing-enhanced-multiples.component';
import { SbModule } from '@sbModule/sb.module';
import { RaceMarketComponent } from '@coralDesktop/racing/components/raceMarket/race-market.component';
import { RacingRunService } from '@racing/services/racingRunService/racing-run.service';
import { RacingSpecialsCarouselComponent } from '@racing/components/racingSpecialsCarousel/racing-specials-carousel.component';
import { RacingPostWidgetComponent } from '@racing/components/racingPostWidget/racing-post-widget.component';
import { DesktopHorseracingTabsComponent } from '@coralDesktop/racing/components/horseracingTabs/horseracing-tabs.component';
import { DesktopGreyhoundsTabsComponent } from '@coralDesktop/racing/components/greyhoundsTabs/greyhounds-tabs.component';
import { DesktopModule } from '@desktopModule/desktop.module';
import { BuildRaceCardComponent } from '@coralDesktop/racing/components/buildRaceCard/build-race-card.component';
import { DesktopRacingAntepostTabComponent } from '@coralDesktop/racing/components/racingAntepostTab/racing-antepost-tab.component';
import { DesktopRacingEventComponent } from '@coralDesktop/racing/components/racingEventComponent/racing-event.component';
import { DesktopRacingEventModelComponent } from '@coralDesktop/racing/components/racingEventModel/racing-event-model.component';
import { BannersModule } from '@banners/banners.module';
import { QuickNavigationComponent } from '@racing/components/quickNavigation/quick-navigation.component';
import { RacingResultsService } from '@core/services/sport/racing-results.service';
import { ForecastTricastGuard } from '@racing/guards/forecast-tricast-guard.service';

// Platform app components / services
import { BuildYourRaceCardPageComponent } from '@coralDesktop/racing/components/buildYourRaceCardPage/build-your-race-card-page.component';
import { BuildYourRaceCardPageService } from '@coralDesktop/racing/components/buildYourRaceCardPage/build-your-race-card-page.service';
import { StickyBuildCardDirective } from '@coralDesktop/racing/directives/sticky-build-card.directive';
import {
  DesktopRacingYourcallSpecialsComponent
} from '@coralDesktop/racing/components/racingYourcallSpecials/racing-yourcall-specials.component';
import {
  DesktopRacingSpecialsTabComponent
} from '@coralDesktop/racing/components/racingSpecialsTab/racing-specials-tab.component';
import { OffersAndFeaturedRacesComponent
} from '@coralDesktop/racing/components/offersAndFeaturedRaces/offers-and-featured-races.component';
import { GreyhoundFutureTabComponent } from '@racing/components/greyhound/greyhoundFutureTab/greyhound-future-tab.component';
import { RacingOutcomeResultedCardComponent } from '@racing/components/racingOutcomeResultedCard/racing-outcome-resulted-card.component';
import {
  DesktopRacingEventResultedComponent
} from '@coralDesktop/racing/components/racingEventResultedComponent/racing-event-resulted.component';
import { RaceCardsControlsComponent } from '@coralDesktop/racing/components/race-cards-controls/race-cards-controls.component';
@NgModule({
  declarations: [
    // Overridden app components
    DesktopHorseracingTabsComponent,
    DesktopGreyhoundsTabsComponent,
    DesktopRacingMainComponent,
    DesktopRacingAntepostTabComponent,
    DesktopRacingEventComponent,
    DesktopRacingEventModelComponent,
    // Platform app components
    BuildYourRaceCardPageComponent,
    DesktopRacingYourcallSpecialsComponent,
    DesktopRacingSpecialsTabComponent,
    // Main app components
    TimeFormSummaryComponent,
    RacingSpecialsCarouselComponent,
    AboutWatchFreeComponent,
    DailyRacingModuleComponent,
    RacingEnhancedMultiplesComponent,
    DesktopHorseracingTabsComponent,
    DesktopGreyhoundsTabsComponent,
    RaceMarketComponent,
    QuantumLeapComponent,
    RacingTabYourcallComponent,
    RacingTabsMainComponent,
    RacingEventMainComponent,
    RacingOutcomeResultedCardComponent,
    DesktopRacingEventResultedComponent,
    RacingPostWidgetComponent,
    QuickNavigationComponent,
    BuildRaceCardComponent,
    StickyBuildCardDirective,
    OffersAndFeaturedRacesComponent,
    GreyhoundFutureTabComponent,
    RaceCardsControlsComponent
  ],
  imports: [
    HttpClientModule,
    SharedModule,
    SbModule,
    RacingRoutingModule,
    DesktopModule,
    BannersModule
  ],
  exports: [
    // Overridden app components
    DesktopHorseracingTabsComponent,
    DesktopGreyhoundsTabsComponent,
    DesktopRacingMainComponent,
    DesktopRacingAntepostTabComponent,
    DesktopRacingEventComponent,
    DesktopRacingEventModelComponent,
    // Platform app components
    BuildYourRaceCardPageComponent,
    DesktopRacingYourcallSpecialsComponent,
    DesktopRacingSpecialsTabComponent,
    // Main app components
    TimeFormSummaryComponent,
    RacingSpecialsCarouselComponent,
    AboutWatchFreeComponent,
    DailyRacingModuleComponent,
    RacingEnhancedMultiplesComponent,
    DesktopHorseracingTabsComponent,
    QuantumLeapComponent,
    RacingTabYourcallComponent,
    RaceMarketComponent,
    RacingTabsMainComponent,
    RacingEventMainComponent,
    RacingOutcomeResultedCardComponent,
    DesktopRacingEventResultedComponent,
    RacingPostWidgetComponent,
    QuickNavigationComponent,
    StickyBuildCardDirective,
    OffersAndFeaturedRacesComponent
  ],
  providers: [
    RacingGaService,
    RoutesDataSharingService,
    RacingEnhancedMultiplesService,
    RacingRunService,
    SortByOptionsService,
    ForecastTricastGuard,
    // Platform app services
    BuildYourRaceCardPageService,
    RacingResultsService
  ],
  entryComponents: [
    // Overridden app components
    DesktopHorseracingTabsComponent,
    DesktopGreyhoundsTabsComponent,
    DesktopRacingMainComponent,
    DesktopRacingAntepostTabComponent,
    DesktopRacingEventComponent,
    DesktopRacingEventModelComponent,
    // Platform app components
    BuildYourRaceCardPageComponent,
    DesktopRacingYourcallSpecialsComponent,
    DesktopRacingSpecialsTabComponent,
    // Main app components
    TimeFormSummaryComponent,
    RacingSpecialsCarouselComponent,
    AboutWatchFreeComponent,
    DailyRacingModuleComponent,
    RacingEnhancedMultiplesComponent,
    QuantumLeapComponent,
    RacingTabYourcallComponent,
    BuildRaceCardComponent,
    RaceMarketComponent,
    RacingTabsMainComponent,
    RacingEventMainComponent,
    RacingOutcomeResultedCardComponent,
    DesktopRacingEventResultedComponent,
    RacingPostWidgetComponent,
    QuickNavigationComponent,
    OffersAndFeaturedRacesComponent,
    GreyhoundFutureTabComponent
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class RacingModule {
  constructor(racingRunService: RacingRunService) {
    racingRunService.run();
  }
}
