import { DesktopHorseracingTabsComponent  } from './horseracing-tabs.component';

describe('DesktopHorseracingTabsComponent ', () => {
  let component;
  let cmsService;
  let router;
  let routingHelperService;
  let eventService;

  beforeEach(() => {
    cmsService = {};
    router = {};
    routingHelperService = {};
    eventService = {};

    component = new DesktopHorseracingTabsComponent(router, routingHelperService, eventService, cmsService);
  });

  describe('onFeaturedEvents', () => {
    it('fetchCardId', () => {
      component.fetchCardId = jasmine.createSpy();
      component.onFeaturedEvents({output: 'fetchCardId', value: {id: '1'}});
      expect(component.fetchCardId).toHaveBeenCalled();
    });

    it('featuredLoaded', () => {
      component.handleFeaturedLoaded = jasmine.createSpy();
      component.onFeaturedEvents({output: 'featuredLoaded', value: 5});
      expect(component.handleFeaturedLoaded).toHaveBeenCalledWith(5);
    });
  });
});
