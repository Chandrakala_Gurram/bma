import { Component } from '@angular/core';
import {
  RaceMarketComponent as MobileRaceMarketComponent
} from '@racing/components/raceMarket/race-market.component';

@Component({
  selector: 'race-market-component',
  templateUrl: '../../../../../app/racing/components/raceMarket/race-market.component.html',
  styleUrls: ['race-market.less']
})
export class RaceMarketComponent extends MobileRaceMarketComponent {}
