import { Component } from '@angular/core';
import { RacingEventResultedComponent } from '@racing/components/racingEventResultedComponent/racing-event-resulted.component';

@Component({
  selector: 'racing-event-resulted',
  templateUrl: '../../../../../app/racing/components/racingEventResultedComponent/racing-event-resulted.component.html',
  styleUrls: ['./racing-event-resulted.component.less']
})
export class DesktopRacingEventResultedComponent extends RacingEventResultedComponent {}
