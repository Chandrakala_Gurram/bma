import { Component } from '@angular/core';
import {
  RacingOutcomeCardComponent
} from '@racing/components/racingOutcomeCard/racing-outcome-card.component';
@Component({
  selector: 'racing-outcome-card',
  templateUrl: 'racing-outcome-card.component.html'
})
export class DesktopRacingOutcomeCardComponent extends RacingOutcomeCardComponent {
}
