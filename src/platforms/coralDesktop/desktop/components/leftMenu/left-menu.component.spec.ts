import { of as observableOf } from 'rxjs';
import { LeftMenuComponent } from '@desktop/components/leftMenu/left-menu.component';
import { category } from './category.mock';

describe('LeftMenuComponentCoral', () => {
  let component;
  let navigationService;
  let leftMenuService;
  let changeDetectorRef;

  beforeEach(() => {
    navigationService = jasmine.createSpyObj(['openUrl']);
    leftMenuService = {
      getMenuItems: jasmine.createSpy('getMenuItems').and.returnValue(observableOf([category]))
    };
    changeDetectorRef = {
      markForCheck: jasmine.createSpy('markForCheck')
    };
    component = new LeftMenuComponent(navigationService, leftMenuService, changeDetectorRef);
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  it('should use OnPush strategy', () => {
    expect(LeftMenuComponent['__annotations__'][0].changeDetection).toBe(0);
  });

  it('#ngOnInit', () => {
    component['setTargetUriParts'] = jasmine.createSpy().and.callFake((item) => item);
    component.ngOnInit();
    expect(leftMenuService.getMenuItems).toHaveBeenCalled();
    expect(component['setTargetUriParts']).toHaveBeenCalledWith(category);
    expect(component.menuItems).toEqual([category]);
    expect(component['changeDetectorRef'].markForCheck).toHaveBeenCalled();
  });

  it('#trackById, should return correct result', () => {
    const menuItem = { id: '15' } as any;
    const index = 2;
    expect(component.trackById(index, menuItem)).toBe('15');
  });

  it('#setTargetUriParts, should extend category with targetUriParts property', () => {
    const result = component['setTargetUriParts'](category);
    expect(result.targetUriParts).toEqual(jasmine.arrayContaining(['/', 'sport', 'american-football']));
  });
});
