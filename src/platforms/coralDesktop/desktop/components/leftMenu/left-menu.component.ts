import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { map } from 'rxjs/operators';

import { ISportCategory } from '@core/services/cms/models/sport-category.model';
import { LeftMenuService } from './left-menu.service';
import { NavigationService } from '@core/services/navigation/navigation.service';

@Component({
  selector: 'left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.less'],
  providers: [
    LeftMenuService
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LeftMenuComponent implements OnInit {

  menuItems: ISportCategory[];

  constructor(
    public navigationService: NavigationService,
    private leftMenuService: LeftMenuService,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.leftMenuService.getMenuItems().pipe(
      map((items: ISportCategory[]) => items.map(item => this.setTargetUriParts(item)))
    ).subscribe((menuItems: ISportCategory[]) => {
      this.menuItems = menuItems;
      this.changeDetectorRef.markForCheck();
    });
  }

  /**
   * ngFor trackBy function
   * @param {number} index
   * @param {ISportCategory} menuItem
   * @return {number}
   */
  trackById(index: number, menuItem: ISportCategory): string {
    return menuItem.id;
  }

  /**
   * extends category object with targetUriParts property
   * @param category {ISportCategory} sport category for modify
   * @return {ISportCategory} sport category with targetUriParts
   */
  private setTargetUriParts(category: ISportCategory): ISportCategory {
    return {
      ...category,
      targetUriParts: [
        '/',
        ...category.targetUri.split('/').filter(param => param)
      ]
    };
  }
}
