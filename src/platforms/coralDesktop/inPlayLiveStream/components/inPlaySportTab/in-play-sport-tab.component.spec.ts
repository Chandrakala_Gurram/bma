import { InPlaySportTabComponent } from './in-play-sport-tab.component';

describe('#InPlaySportTabComponent', () => {
  let component: InPlaySportTabComponent;
  let inplayHelperService,
  inplayMainService,
  userService;

  beforeEach(() => {
    inplayHelperService = {
      subscribeForLiveUpdates: () => {},
      unsubscribeForLiveUpdates: () => {}
    };
    inplayMainService = {
      isCashoutAvailable: () => {}
    };
    userService = {};

    component = new InPlaySportTabComponent(inplayHelperService, inplayMainService, userService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('gtmDataLayer.eventAction should be equal live stream', () => {
    component.liveStreamTab = true;
    component.ngOnInit();

    expect(component.gtmDataLayer.eventAction).toBe('live stream');
  });

  it('gtmDataLayer.eventAction should be equal in-play', () => {
    component.liveStreamTab = false;
    component.ngOnInit();

    expect(component.gtmDataLayer.eventAction).toBe('in-play');
  });
});
