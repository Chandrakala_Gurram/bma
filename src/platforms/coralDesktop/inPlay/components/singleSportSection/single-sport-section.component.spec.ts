import { of as observableOf } from 'rxjs';
import { delay } from 'rxjs/operators';
import { fakeAsync, tick, discardPeriodicTasks } from '@angular/core/testing';
import { SingleSportSectionComponent } from '@coralDesktop/inPlay/components/singleSportSection/single-sport-section.component';
import { EVENT_TYPES } from '@app/inPlay/constants/event-types.constant';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('#DesktopSingleSportSectionComponent', () => {
  let component: SingleSportSectionComponent;
  let pubSubService;
  let inPlayMainService;
  let coreToolService;
  let sportsConfigService;
  let windowRef;
  let changeDetectorRef;
  let setIntervalHandler;
  let cmsService;
  const competitionEvents = [{
    cashoutAvail: 'cashoutAvail',
    categoryCode: 'categoryCode',
    categoryId: 'categoryId',
    categoryName: 'categoryName',
    displayOrder: 'displayOrder',
    drilldownTagNames: 'drilldownTagNames',
    eventIsLive: 'eventIsLive',
    eventSortCode: 'eventSortCode',
    eventStatusCode: 'eventStatusCode',
    id: 123
  }];
  const expectedSportConfig = {
    path: 'football',
    id: '16',
    specialsTypeIds: [2297, 2562],
    dispSortName: 'MR',
    primaryMarkets: '|Match Betting|',
    viewByFilters: ['byLeaguesCompetitions', 'byTime'],
    oddsCardHeaderType: 'homeDrawAwayType',
    isMultiTemplateSport: false
  };

  beforeEach(() => {
    inPlayMainService = {
      unsubscribeForSportCompetitionUpdates: jasmine.createSpy(),
      unsubscribeForEventsUpdates: jasmine.createSpy(),
      _getCompetitionData: jasmine.createSpy().and.returnValue(observableOf(competitionEvents)),
      getTopLevelTypeParameter: jasmine.createSpy().and.returnValue(EVENT_TYPES.LIVE_EVENT),
      subscribeForUpdates: jasmine.createSpy(),
      isCashoutAvailable: jasmine.createSpy().and.returnValue(true),
      clearDeletedEventFromType: jasmine.createSpy('clearDeletedEventFromType'),
      getSportName: jasmine.createSpy('getSportName').and.returnValue('football')
    };
    pubSubService = {
      subscribe: jasmine.createSpy('ssubscribeync').and.callFake((a, b, cb) => {
        switch (b) {
          case 'INPLAY_COMPETITION_REMOVED:34:LIVE_EVENT':
            cb();
            break;
          case 'INPLAY_COMPETITION_ADDED:34:LIVE_EVENT':
            cb([]);
            break;
          default:
            break;
        }
      }),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: pubSubApi
    };

    windowRef = {
      nativeWindow: {
        setInterval: jasmine.createSpy('setInterval').and.callFake((cb, interval) => {
          setIntervalHandler = cb;
        }),
        clearInterval: jasmine.createSpy('clearInterval'),
      }
    };
    changeDetectorRef = {
      markForCheck: jasmine.createSpy('markForCheck'),
      detach: jasmine.createSpy('detach'),
      detectChanges: jasmine.createSpy('detectChanges')
    };

    coreToolService = {
      uuid: jasmine.createSpy('uuid').and.returnValue('UUID')
    };

    sportsConfigService = {
      getSport: jasmine.createSpy('getSport').and.returnValue(observableOf(expectedSportConfig))
    };

    cmsService = {
      getMarketSwitcherFlagValue: jasmine.createSpy('getMarketSwitcherFlagValue').and.returnValue(observableOf(Boolean))
    };

    component = new SingleSportSectionComponent(
      inPlayMainService,
      pubSubService,
      windowRef,
      changeDetectorRef,
      coreToolService,
      sportsConfigService,
      cmsService
    );

    component.eventsBySports = {
      categoryId: '34',
      eventsByTypeName: [],
      marketSelector: 'To Qualify',
      marketSelectorOptions: ['Main markets', 'Match Betting']
    } as any;

    component.reloadData.emit = jasmine.createSpy('reloadData.emit');

    component['CATEGORIES_DATA'] = {
      gaming: {
        football: { id: '1' }
      },
      tierOne: ['16', '34', '6']
    };
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  it('should use OnPush strategy', () => {
    expect(SingleSportSectionComponent['__annotations__'][0].changeDetection).toBe(0);
  });

  describe('#ngOnInit', () => {
    it(`should define categoryId and topLevelType`, () => {
      component.ngOnInit();

      expect(component.categoryId).toEqual('34');
      expect(component.topLevelType).toEqual(EVENT_TYPES.LIVE_EVENT);
    });

    it('ngOnInit', () => {
      component.setExpandedFlags = jasmine.createSpy();
      component.ngOnInit();

      expect(pubSubService.subscribe).toHaveBeenCalledTimes(3);
      expect(component.setExpandedFlags).toHaveBeenCalled();
    });

    it('should subscribe for updates and set addedCompetition isExpanded to true', () => {
      let inplayCompetitionHandler;
      const eventsByTypeName = [{
        showCashoutIcon: false,
        typeId: 1
      }
      ] as any;
      const addedCompetitions = {typeId: 1, events: [], isExpanded: false};

      pubSubService.subscribe.and.callFake((name, method, cb) => {
        if (method === 'INPLAY_COMPETITION_ADDED:34:LIVE_EVENT') {
          inplayCompetitionHandler = cb;
        }
      });
      component.ngOnInit();
      component.eventsBySports.eventsByTypeName = eventsByTypeName;
      component.expandedLeaguesCount = undefined;

      inplayCompetitionHandler(addedCompetitions);

      expect(inPlayMainService.subscribeForUpdates).toHaveBeenCalledWith([]);
      expect(addedCompetitions.isExpanded).toBeTruthy();
      expect(eventsByTypeName[0].showCashoutIcon).toBeTruthy();
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    });

    it('ngOnInit case INPLAY_COMPETITION_REMOVED with market selector', () => {
      component.isMarketSelectorVisible = jasmine.createSpy('isMarketSelectorVisible').and.returnValue(true);
      component.ngOnInit();

      expect(pubSubService.subscribe).toHaveBeenCalledTimes(3);
      expect(component.reloadData.emit).toHaveBeenCalledWith({
        useCache: false,
        additionalParams: {
          marketSelector: 'Main markets'
        }
      });
      expect(changeDetectorRef.detectChanges).toHaveBeenCalled();
    });

    it('ngOnInit case INPLAY_COMPETITION_REMOVED without market selector', () => {
      component.isMarketSelectorVisible = jasmine.createSpy('isMarketSelectorVisible').and.returnValue(false);
      component.ngOnInit();

      expect(pubSubService.subscribe).toHaveBeenCalledTimes(3);
      expect(changeDetectorRef.detectChanges).toHaveBeenCalled();
      expect(component.reloadData.emit).not.toHaveBeenCalled();
    });

    it('should filter "Enhanced Multiples" type if eventsBySports exist', () => {
      component.eventsBySports = {} as any;
      component.ngOnInit();
      expect(component.eventsBySports).toEqual({} as any);

      component.eventsBySports.eventsByTypeName = [];
      component.ngOnInit();
      expect(component.eventsBySports.eventsByTypeName).toEqual([]);

      component.eventsBySports.eventsByTypeName = [{
        typeName: 'English Football',
      }, {
        typeName: 'Enhanced Multiples'
      }] as any;
      component.ngOnInit();

      expect(component.eventsBySports.eventsByTypeName).toEqual([{
        typeName: 'English Football',
        showCashoutIcon: true
      }] as any);
    });

    it('should delete event from data structure', () => {
      component.eventsBySports = {
        categoryId: '16',
        marketSelectorOptions: ['1', '2'],
        eventsByTypeName: []
      } as any;
      pubSubService.subscribe.and.callFake((a, b, cb) => {
        if (b === 'DELETE_EVENT_FROM_CACHE') {
          cb(1);
        }
      });

      component.ngOnInit();

      expect(pubSubService.subscribe).toHaveBeenCalledWith(
        'inplay-single-sport_UUID',
        pubSubService.API.DELETE_EVENT_FROM_CACHE,
        jasmine.any(Function)
      );
      expect(inPlayMainService.clearDeletedEventFromType).toHaveBeenCalledWith({
        categoryId: '16',
        marketSelectorOptions: ['1', '2'],
        eventsByTypeName: []
      }, 1);
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    });

    it('should define sport', fakeAsync(() => {
      component.ngOnInit();
      tick();

      expect(inPlayMainService.getSportName).toHaveBeenCalled();
      expect(sportsConfigService.getSport).toHaveBeenCalledWith('football');
    }));

    it('should invoke change detector', fakeAsync(() => {
      component.ngOnInit();

      expect(windowRef.nativeWindow.setInterval).toHaveBeenCalledWith(jasmine.any(Function), 500);

      setIntervalHandler();
      expect(changeDetectorRef.detectChanges).toHaveBeenCalled();
    }));
  });

  it('reloadSportData', () => {
    component.expandedFlags = {};
    const options = {
      useCache: true,
      additionalParams: {
        topLevelType: 'type',
        categoryId: 'id'
      }
    } as any;
    component.reloadSportData(options);

    expect(component.reloadData.emit).toHaveBeenCalledWith(options);
  });

  describe('#setExpandedFlags', () => {
    it('should update expandedFlags object & set true for showCashoutIcon field of eventsByTypeName', () => {
      component.expandedLeaguesCount = 4;
      component.expandedFlags = {
        1: undefined,
        2: undefined,
        3: undefined
      };
      component.eventsBySports = {
        eventsByTypeName: [
          {
            showCashoutIcon: false,
            typeId: 1
          },
          {
            showCashoutIcon: false,
            typeId: 2
          },
          {
            showCashoutIcon: false,
            typeId: 3
          }
        ]
      } as any;
      const expandedFlagsResult = {
        1: true,
        2: true,
        3: true
      };
      component.setExpandedFlags();

      component.eventsBySports.eventsByTypeName.forEach((competitionSection) => expect(competitionSection.showCashoutIcon).toBeTruthy);
      expect(component.expandedFlags).toEqual(expandedFlagsResult);
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    });

    it('should update expandedFlags', () => {
      component.expandedFlags = {
        1: undefined,
        2: undefined,
        3: undefined
      };
      const expandedFlagsResult = {
        1: undefined,
        2: undefined,
        3: undefined
      };
      component.setExpandedFlags();

      expect(component.expandedFlags).toEqual(expandedFlagsResult);
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    });

    it('should not update expandedFlags', () => {
      const expandedFlagsResult = {
        1: true,
        2: true,
        3: true
      };

      component.expandedLeaguesCount = 4;
      component.expandedFlags = {
        1: true,
        2: true,
        3: true
      };
      component.eventsBySports = {
        eventsByTypeName: [
          {
            showCashoutIcon: false,
            typeId: 1
          },
          {
            showCashoutIcon: false,
            typeId: 2
          },
          {
            showCashoutIcon: false,
            typeId: 3
          }
        ]
      } as any;

      component.setExpandedFlags();

      expect(component.expandedFlags).toEqual(expandedFlagsResult);
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    });
  });

  describe('#toggleCompetitionSection', () => {
    it('should test toggling of expand/collapse state of league section', fakeAsync(() => {
      const competitionSection = {
        typeId: '01',
        marketSelector: 'marketSelector',
        events: [
          {
            cashoutAvail: 'cashoutAvail',
            categoryCode: 'categoryCode',
            categoryId: 'categoryId'
          }
        ]
      } as any;
      const sectionsArray = [
        {
          typeId: '01',
          marketSelector: 'marketSelector',
          events: [
            {
              cashoutAvail: 'cashoutAvail',
              categoryCode: 'categoryCode',
              categoryId: 'categoryId'
            }
          ]
        },
        {
          typeId: '02',
          marketSelector: 'marketSelector',
          events: [
            {
              cashoutAvail: 'cashoutAvail',
              categoryCode: 'categoryCode',
              categoryId: 'categoryId'
            }
          ]
        }
      ] as any[];
      component.expandedFlags = {
        '01': false,
        '02': false
      };
      component.competitionRequestInProcessFlags = {
        '01': false,
        '02': false
      };
      component.eventsBySports = {
        categoryCode: '01',
        categoryId: '02'
      } as any;
      component.filter = 'live';
      const requestParams = {
        categoryId: component.eventsBySports.categoryId,
        isLiveNowType: true,
        topLevelType: 'LIVE_EVENT',
        typeId: competitionSection.typeId,
        modifyMainMarkets: true
      };

      component.toggleCompetitionSection(competitionSection, sectionsArray);

      expect(component.competitionRequestInProcessFlags['01']).toBeFalsy();
      inPlayMainService._getCompetitionData(requestParams, component.eventsBySports.categoryCode)
        .subscribe((competition) => {
          expect(component.competitionRequestInProcessFlags[competitionSection.typeId]).toBeFalsy();
          expect(competitionSection.events).toEqual(competition);
          expect(inPlayMainService.subscribeForUpdates).toHaveBeenCalledWith(competitionSection.events);
          expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
        });
      tick();
    }));

    it('should not toggle competition section if isRequestInProcess', () => {
      const competitionSection = {typeId: 1} as any;

      component.competitionRequestInProcessFlags = {1: true};

      const actualResult = component.toggleCompetitionSection(competitionSection, []);

      expect(inPlayMainService._getCompetitionData).not.toHaveBeenCalled();
      expect(actualResult).toBeFalsy();
    });

    it('should unsubscribe from event updates', () => {
      const competitionSection = {typeId: 1} as any;

      component.expandedFlags = {1: true};
      component.competitionRequestInProcessFlags = {1: false};
      component.toggleCompetitionSection(competitionSection, []);

      expect(inPlayMainService.unsubscribeForEventsUpdates).toHaveBeenCalledWith(competitionSection);
    });

    it('should splice sectionArray', () => {
      inPlayMainService._getCompetitionData.and.returnValue(observableOf([]));
      const competitionSection = {typeId: 1, marketSelector: true} as any;
      const sectionsArray = [];

      component.expandedFlags = {1: false};
      component.competitionRequestInProcessFlags = {1: false};
      component.toggleCompetitionSection(competitionSection, sectionsArray);

      expect(sectionsArray).toEqual([]);
    });

    it('should not add marketSelector to requestParams', () => {
      const competitionSection = {typeId: 1, marketSelector: false} as any;
      const sectionsArray = [];
      const requestParams = {
        categoryId: undefined,
        isLiveNowType: false,
        topLevelType: 'LIVE_EVENT',
        typeId: 1,
        marketSelector: undefined
      };

      component.expandedFlags = {1: false};
      component.competitionRequestInProcessFlags = {1: false};
      component.toggleCompetitionSection(competitionSection, sectionsArray);

      expect(requestParams['marketSelector']).toBeUndefined();
    });

    it('should not subscribe for updates', fakeAsync(() => {
      inPlayMainService._getCompetitionData.and.returnValue(observableOf(competitionEvents).pipe(delay(5000)));
      const competitionSection = {typeId: 1, marketSelector: true} as any;
      const sectionsArray = [];

      component.expandedFlags = {1: false};
      component.competitionRequestInProcessFlags = {1: false};
      component.toggleCompetitionSection(competitionSection, sectionsArray);

      tick(5000);
      component.expandedFlags = {1: false};
      expect(inPlayMainService.subscribeForUpdates).not.toHaveBeenCalled();
      discardPeriodicTasks();
    }));
  });

  it('#isMarketSelectorVisible ', () => {
    component.eventsBySports = {
      categoryId: '16',
      marketSelectorOptions: ['1', '2']
    } as any;
    component.categoryId = '16';
    component.inner = false;
    component.filter = 'livenow';

    const actualResult = component.isMarketSelectorVisible();

    expect(actualResult).toBeTruthy();
  });

  it('#isCashoutAvailable', () => {
    const sportEvents = [
      {
        cashoutAvail: 'cashoutAvail'
      }
    ] as any[];

    const actualResult = component.isCashoutAvailable(sportEvents, true);

    expect(actualResult).toBeTruthy();
  });

  describe('#getSectionTitle', () => {
    const competitionSectionData = {
      typeSectionTitleAllSports: 'all sports title',
      typeSectionTitleOneSport: 'one sport title'
    } as any;

    it('should return: all sports title', () => {
      component.inner = true;

      const actualResult = component.getSectionTitle(competitionSectionData);

      expect(actualResult).toEqual(competitionSectionData.typeSectionTitleAllSports);
    });

    it('should return one sport title', () => {
      component.inner = false;

      const actualResult = component.getSectionTitle(competitionSectionData);

      expect(actualResult).toEqual(competitionSectionData.typeSectionTitleOneSport);
    });
  });

  it('ngOnDestroy: should remove listeners', function () {
    component.eventsBySports = {} as any;
    component.ngOnDestroy();

    expect(inPlayMainService.unsubscribeForSportCompetitionUpdates).toHaveBeenCalledWith(component.eventsBySports);
    expect(inPlayMainService.unsubscribeForEventsUpdates).toHaveBeenCalledWith(component.eventsBySports);
    expect(pubSubService.unsubscribe).toHaveBeenCalledWith('inplay-single-sport_UUID');
    expect(component.eventsBySports.eventsByTypeName).toEqual([]);
  });

  it('ngOnDestroy: should unsubscribe from sports config', () => {
    component['sportsConfigSubscription'] = {
      unsubscribe: jasmine.createSpy('unsubscribe')
    } as any;
    component.ngOnDestroy();

    expect(component['sportsConfigSubscription'].unsubscribe).toHaveBeenCalled();
  });

  it('ngOnDestroy: should unsubscribe from marketSwitcherConfig',  () => {
    component['marketSwitcherConfigSubscription'] = {
      unsubscribe: jasmine.createSpy('unsubscribe')
    } as any;
    component.ngOnDestroy();

    expect(component['marketSwitcherConfigSubscription'].unsubscribe).toHaveBeenCalled();
  });

  it('ngOnDestroy: should not unsubscribe from sports config', () => {
    component['sportsConfigSubscription'] = undefined;
    component.ngOnDestroy();

    expect(component['sportsConfigSubscription']).not.toBeDefined();
  });

  describe('ngOnChanges', () => {
    it('should reset expanded flags', () => {
      component.setExpandedFlags = jasmine.createSpy('setExpandedFlags');
      component.ngOnChanges({eventsBySports: {}} as any);

      expect(component.setExpandedFlags).toHaveBeenCalled();
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    });

    it('should not reset expanded flags', () => {
      component.setExpandedFlags = jasmine.createSpy('setExpandedFlags');
      component.ngOnChanges({});

      expect(component.setExpandedFlags).not.toHaveBeenCalled();
      expect(changeDetectorRef.markForCheck).not.toHaveBeenCalled();
    });
  });

  it('should return item typeId', () => {
    const actualResult = component.trackByTypeId(1, {typeId: '2'});

    expect(actualResult).toEqual('2');
  });

  it('should return item id', () => {
    const actualResult = component.trackByEventId(1, {id: '2'});

    expect(actualResult).toEqual('2');
  });

  describe('check for isMarketSwitcherConfigured', () => {
    it('should set isMarketSwitcherConfigured to true if cmsService getMarketSwitcherFlagValue return true', () => {
      cmsService.getMarketSwitcherFlagValue.subscribe = jasmine.createSpy('cmsService.getMarketSwitcherFlagValue')
        .and.callFake((flag) => {
          expect(cmsService.getMarketSwitcherFlagValue).toHaveBeenCalled();
          flag = true;
          expect(component.isMarketSwitcherConfigured).toBe(true);
        });
    });
    it('should set isMarketSwitcherConfigured to false if cmsService getMarketSwitcherFlagValue return false', () => {
      cmsService.getMarketSwitcherFlagValue.subscribe = jasmine.createSpy('cmsService.getMarketSwitcherFlagValue')
        .and.callFake((flag) => {
          expect(cmsService.getMarketSwitcherFlagValue).toHaveBeenCalled();
          flag = false;
          expect(component.isMarketSwitcherConfigured).toBe(false);
        });
    });
  });
});
