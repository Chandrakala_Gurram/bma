import { Component } from '@angular/core';

import { SplashPageComponent } from '@app/questionEngine/components/splashPage/splash-page.component';

@Component({
  selector: 'splash-page',
  templateUrl: './splash-page.component.html',
  styleUrls: ['./splash-page.component.less'],
})

export class DesktopSplashPageComponent extends SplashPageComponent {

}
