import { Component } from '@angular/core';
import { PreviousTabComponent } from '@app/questionEngine/components/resultsPage/tabs/previousTab/previous-tab.component';

@Component({
  selector: 'previous-tab',
  templateUrl: './previous-tab.component.html',
  styleUrls: ['./previous-tab.component.less'],
})

export class DesktopPreviousTabComponent extends PreviousTabComponent {

}
