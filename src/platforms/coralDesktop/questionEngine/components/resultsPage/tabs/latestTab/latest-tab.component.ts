import { Component } from '@angular/core';
import { LatestTabComponent } from '@app/questionEngine/components/resultsPage/tabs/latestTab/latest-tab.component';

@Component({
  selector: 'latest-tab',
  templateUrl: '../../../../../../../app/questionEngine/components/resultsPage/tabs/latestTab/latest-tab.component.html',
  styleUrls: ['../../../../../../../app/questionEngine/components/resultsPage/tabs/latestTab/latest-tab.component.less'],
})

export class DesktopLatestTabComponent extends LatestTabComponent {
}
