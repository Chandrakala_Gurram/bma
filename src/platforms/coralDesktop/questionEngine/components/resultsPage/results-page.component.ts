import { Component } from '@angular/core';

import { ResultsPageComponent } from '@app/questionEngine/components/resultsPage/results-page.component';

@Component({
  selector: 'results-page',
  templateUrl: './results-page.component.html',
  styleUrls: ['./results-page.component.less'],
})
export class DesktopResultsPageComponent extends ResultsPageComponent {

}
