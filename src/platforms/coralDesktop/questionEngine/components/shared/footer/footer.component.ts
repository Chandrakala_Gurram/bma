import { Component } from '@angular/core';

import { FooterComponent } from '@app/questionEngine/components/shared/footer/footer.component';

@Component({
  selector: 'qe-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})

export class DesktopFooterComponent extends FooterComponent {

}
