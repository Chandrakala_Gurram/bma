import { Component } from '@angular/core';
import { AnswersSummaryComponent } from '@app/questionEngine/components/shared/answersSummary/answers-summary.component';


@Component({
  selector: 'answers-summary',
  templateUrl: './answers-summary.component.html',
  styleUrls: ['./answers-summary.component.less'],
})

export class DesktopAnswersSummaryComponent extends AnswersSummaryComponent {
}
