import { Router } from '@angular/router';
import { of } from 'rxjs';

import { DesktopQuestionEngineMainComponent } from './question-engine-main.component';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { DeviceService } from '@core/services/device/device.service';
import { QuestionEngineService } from '@app/questionEngine/services/question-engine/question-engine.service';
import { NewRelicService } from '@core/services/newRelic/new-relic.service';
import { LocaleService } from '@core/services/locale/locale.service';

describe('Desktop QuestionEngine Main Component', () => {
  let component: DesktopQuestionEngineMainComponent;
  let pubSubService;
  let deviceService;
  let questionEngineService;
  let newRelicService;
  let localeService;
  let router;

  beforeEach(() => {
    const data = null;

    router = {
      url: '/correct4/splash',
      events: of({})
    };

    newRelicService = {
      API: {},
      addPageAction: jasmine.createSpy('addPageAction'),
      noticeError: jasmine.createSpy('noticeError')
    };

    pubSubService = {
      API: {
        TOGGLE_MOBILE_HEADER_FOOTER: 'TOGGLE_MOBILE_HEADER_FOOTER',
        QE_FATAL_ERROR: 'QE_FATAL_ERROR'
      },
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy(),
      publish: jasmine.createSpy()
    };

    questionEngineService = {
      qeData: null,
      getQuizHistory: jasmine.createSpy().and.returnValue(of(data)),
      mapResponseOnComponentModel: jasmine.createSpy().and.returnValue(of(data)),
      checkGameData: jasmine.createSpy(),
      setQEDataUptodateStatus: jasmine.createSpy(),
      pipe: jasmine.createSpy(),
      error: jasmine.createSpy().and.callThrough(),
    };

    deviceService = {
      isMobile: true
    };

    localeService = {
      getString: jasmine.createSpy(),
    };

    component = new DesktopQuestionEngineMainComponent(
      pubSubService as PubSubService,
      deviceService as DeviceService,
      questionEngineService as QuestionEngineService,
      newRelicService as NewRelicService,
      localeService as LocaleService,
      router as Router
    );
  });

  it('should create component', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();

    component.getQuizName();
    expect(component.quizName).toEqual('correct4');

    router = {
      url: '/',
      events: of({})
    };
    component = new DesktopQuestionEngineMainComponent(
      pubSubService as PubSubService,
      deviceService as DeviceService,
      questionEngineService as QuestionEngineService,
      newRelicService as NewRelicService,
      localeService as LocaleService,
      router as Router
    );
    component.getQuizName();
    expect(component.quizName).toEqual('');

    router = {
      url: '/qe/smart-money',
      events: of({})
    };
    component = new DesktopQuestionEngineMainComponent(
      pubSubService as PubSubService,
      deviceService as DeviceService,
      questionEngineService as QuestionEngineService,
      newRelicService as NewRelicService,
      localeService as LocaleService,
      router as Router
    );
    component.getQuizName();
    expect(component.quizName).toEqual('smart-money');
  });
});
