import { Component } from '@angular/core';

import { QuestionsInfoComponent } from '@app/questionEngine/components/questionsPage/questions-info/questions-info.component';

  @Component({
    selector: 'questions-info',
    templateUrl: './questions-info.component.html',
    styleUrls: ['./questions-info.component.less'],
  })

  export class DesktopQuestionsInfoComponent extends QuestionsInfoComponent {
}
