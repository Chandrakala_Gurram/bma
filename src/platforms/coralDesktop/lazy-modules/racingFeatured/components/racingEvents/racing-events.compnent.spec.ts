import { RacingEventsComponent } from './racing-events.component';

describe('RacingEventsComponent', () => {
  let component;
  let locale;
  let storage;

  beforeEach(() => {
    locale = {};
    storage = {};

    component = new RacingEventsComponent(locale, storage);
  });

  it('emitFetchCardId', () => {
    component.fetchCardId.emit = jasmine.createSpy('emitFetchCardId.emit');
    component.emitFetchCardId({id: '1'});
    expect(component.fetchCardId.emit).toHaveBeenCalledWith({id: '1'});
  });
});
