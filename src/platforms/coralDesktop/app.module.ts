import { HttpClientModule } from '@angular/common/http';
import {
  Injector,
  NgModule,
  NgModuleFactory,
  NgModuleFactoryLoader,
  NO_ERRORS_SCHEMA
} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { STORE_PREFIX, runOnAppInit, VanillaCoreModule } from '@vanilla/core/core';
import { VanillaCommonModule } from '@vanilla/core/common';
import { VanillaFeaturesModule, AppComponent } from '@vanilla/core/features';
import { LabelHostFeaturesModule, LH_HTTP_ERROR_HANDLERS } from '@labelhost/core/features';

import { BmaModule } from '@bmaModule/bma.module';
import { CoreModule } from '@coreModule/core.module';
import { DynamicLoaderModule } from '@app/dynamicLoader/dynamic-loader.module';
import { DesktopModule } from '@desktopModule/desktop.module';
import { SharedModule } from '@sharedModule/shared.module';

import { VanillaInitModule } from '@vanillaInitModule/vanilla-init.module';
import { LabelHostHttpErrorHandler } from '@vanillaInitModule/services/labelHostErrorHandler/labelHostHttpErrorHandler.service';
import { AppRoutingModule } from '@coralDesktop/app-routing.module';
import { RootComponent } from '@coralDesktop/app.component';
import { lazyComponentsManifests } from '@coralDesktop/lazy-components.manifests';
import { LocaleService } from '@coreModule/services/locale/locale.service';
import * as sbDesktopLangData from '@localeModule/translations/en-US/sbdesktop.lang';
import { RoutingState } from '@sharedModule/services/routingState/routing-state.service';
import { HostAppBootstrapper } from '@app/host-app/host-app-bootstrapper.service';
import { CoralSportsClientConfigModule } from '@app/client-config/client-config.module';

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    VanillaCoreModule.forRoot(),
    VanillaCommonModule.forRoot(),
    VanillaFeaturesModule.forRoot(),
    LabelHostFeaturesModule.forRoot(),
    CoreModule,
    VanillaInitModule,
    SharedModule.forRoot(),
    BmaModule,
    DesktopModule,
    DynamicLoaderModule.forRoot(lazyComponentsManifests),
    CoralSportsClientConfigModule.forRoot()
  ],
  declarations: [
    RootComponent
  ],
  providers: [
    { provide: STORE_PREFIX, useValue: 'coralsports.' },
    { provide: LH_HTTP_ERROR_HANDLERS, useClass: LabelHostHttpErrorHandler, multi: true },
    runOnAppInit(HostAppBootstrapper)
  ],
  bootstrap: [
    AppComponent
  ],
  entryComponents: [
    RootComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {
  constructor(
    private loader: NgModuleFactoryLoader,
    private injector: Injector,
    private localeService: LocaleService,
    private routingState: RoutingState
  ) {
    this.routingState.loadRouting();
    const paths = ['src/app/lazy-modules/locale/translation.module#TranslationModule',
      'src/app/lazy-modules/performanceMark/performanceMark.module#PerformanceMarkModule'];
    paths.forEach((path) => {
      this.loader
        .load(path)
        .then((moduleFactory: NgModuleFactory<any>) => moduleFactory.create(this.injector));
    });
    this.localeService.setLangData(sbDesktopLangData);
  }
}
