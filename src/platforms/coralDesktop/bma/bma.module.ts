import { FormsModule } from '@angular/forms';
import { APP_INITIALIZER, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ModalModule } from 'angular-custom-modal';

import { DesktopModule } from '@coralDesktop/desktop/desktop.module';
import { SharedModule } from '@sharedModule/shared.module';
import { FavouritesModule } from '@favouritesModule/favourites.module';

import { ContactUsComponent } from '@bmaModule/components/contactUs/contact-us.component';
import { NotFoundComponent } from '@bmaModule/components/404/404.component';
import {
  RightColumnWidgetWrapperComponent
} from '@bmaModule/components/rightColumn/rightColumnWidgetWrapper/right-column-widget-wrapper.component';
import { WBetslipComponent } from '@bmaModule/components/rightColumn/wBetslip/w-betslip.component';
import { WMatchCentreComponent } from '@bmaModule/components/rightColumn/wMatchCentre/w-match-centre.component';
import { InplayLiveStreamModule } from '@inPlayLiveStream/inplay-live-stream.module';
import { BmaRunService } from '@bmaModule/services/bmaRunService/bma-run.service';
import { BmaInit } from '@bmaModule/services/bmaRunService/bma-init';
import { BannersModule } from '@banners/banners.module';
import { InplayHomeTabComponent } from '@bmaModule/components/inlayHomeTab/inplay-home-tab.component';
import { LocaleService } from '@coreModule/services/locale/locale.service';
import { StaticComponent } from '@bmaModule/components/static/static.component';
import * as criticalLangData from '@localeModule/translations/en-US/critical-lang-data';
import {
  RightColumnWidgetItemComponent
} from '@coralDesktop/bma/components/rightColumn/rightColumnWidgetItem/right-column-widget-item.component';
import { WMiniGamesIframeComponent } from '@bma/components/rightColumn/wMiniGames/w-mini-games-iframe.component';
import { RightColumnWidgetComponent } from '@coralDesktop/bma/components/rightColumn/rightColumnWidget/right-column-widget.component';
import { DesktopLiveStreamWrapperComponent } from '@coralDesktop/bma/components/liveStream/live-stream-wrapper.component';

// Vanilla desktop overrides
import { DesktopBmaMainComponent } from '@coralDesktop/bma/components/bmaMain/bma-main.component';
import { DesktopHomeComponent } from '@coralDesktop/bma/components/home/home.component';

@NgModule({
  imports: [
    SharedModule,
    ModalModule,
    FormsModule,
    FavouritesModule,
    DesktopModule,
    BannersModule,
    InplayLiveStreamModule
  ],
  providers: [
    BmaRunService,
    {
      provide: APP_INITIALIZER,
      useFactory: BmaInit,
      deps: [BmaRunService],
      multi: true
    },
  ],
  declarations: [
    ContactUsComponent,
    DesktopHomeComponent,
    NotFoundComponent,
    RightColumnWidgetWrapperComponent,
    RightColumnWidgetComponent,
    RightColumnWidgetItemComponent,
    WBetslipComponent,
    WMatchCentreComponent,
    InplayHomeTabComponent,
    StaticComponent,
    DesktopBmaMainComponent,
    WMiniGamesIframeComponent,
    DesktopLiveStreamWrapperComponent,
  ],
  entryComponents: [
    ContactUsComponent,
    DesktopHomeComponent,
    NotFoundComponent,
    RightColumnWidgetWrapperComponent,
    RightColumnWidgetComponent,
    RightColumnWidgetItemComponent,
    WBetslipComponent,
    WMatchCentreComponent,
    InplayHomeTabComponent,
    StaticComponent,
    DesktopBmaMainComponent,
    WMiniGamesIframeComponent,
    DesktopLiveStreamWrapperComponent,
  ],
  exports: [
    ContactUsComponent,
    NotFoundComponent,
    RightColumnWidgetWrapperComponent,
    RightColumnWidgetComponent,
    RightColumnWidgetItemComponent,
    WBetslipComponent,
    WMatchCentreComponent,
    InplayHomeTabComponent,
    StaticComponent,
    DesktopBmaMainComponent,
    WMiniGamesIframeComponent,
    DesktopLiveStreamWrapperComponent,
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class BmaModule {
  constructor(private localeService: LocaleService) {
    this.localeService.setLangData(criticalLangData);
  }
}
