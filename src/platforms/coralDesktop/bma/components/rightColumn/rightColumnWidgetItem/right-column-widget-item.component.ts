import { Component } from '@angular/core';
import {
  RightColumnWidgetItemComponent as CoralRightColumnWidgetItemComponent
} from '@bma/components/rightColumn/rightColumnWidgetItem/right-column-widget-item.component';

@Component({
  selector: 'right-column-widget-item',
  templateUrl: 'right-column-widget-item.component.html'
})
export class RightColumnWidgetItemComponent extends CoralRightColumnWidgetItemComponent {}
