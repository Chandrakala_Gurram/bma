import { Component, OnInit,  } from '@angular/core';
import {
  RightColumnWidgetComponent as AppRightColumnWidgetItemComponent
} from '@bma/components/rightColumn/rightColumnWidget/right-column-widget.component';

@Component({
  selector: 'right-column-widget',
  styleUrls: ['right-column-widget.less'],
  templateUrl: '../../../../../../app/bma/components/rightColumn/rightColumnWidget/right-column-widget.component.html'
})
export class RightColumnWidgetComponent extends AppRightColumnWidgetItemComponent implements OnInit {}
