import { Component } from '@angular/core';

import { CmsService } from '@core/services/cms/cms.service';

import * as _ from 'underscore';
import { HomeComponent } from '@bma/components/home/home.component';
import { DynamicLoaderService } from '@root/app/dynamicLoader/dynamic-loader.service';

@Component({
  selector: 'home',
  templateUrl: 'home.component.html'
})

export class DesktopHomeComponent extends HomeComponent {
  constructor(
    protected cmsService: CmsService,
    protected dynamicComponentLoader: DynamicLoaderService
  ) {
    super(cmsService, dynamicComponentLoader);
  }

  /**
   * Says whether to show ribbon module or not
   * @return {boolean}
   */
  showRibbon(): boolean {
    const id: string = 'tab-build-your-bet';
    const ribbon = _.findWhere(this.ribbon, { id });

    return ribbon ? ribbon.visible && (ribbon.showTabOn === 'both' || ribbon.showTabOn === 'desktop') : false;
  }
}
