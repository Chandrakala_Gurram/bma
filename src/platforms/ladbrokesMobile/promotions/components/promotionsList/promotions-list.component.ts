import { Component } from '@angular/core';
import { PromotionsListComponent } from '@app/promotions/components/promotionsList/promotions-list.component';

@Component({
  selector: 'promotions-list',
  templateUrl: './promotions-list.component.html',
  styleUrls: ['promotions-list.component.less']
})
export class LadbrokesPromotionsListComponent extends PromotionsListComponent { }
