import { Component } from '@angular/core';
import { PromoLabelsComponent } from '@promotions/components/promoLabels/promo-labels.component';
@Component({
  selector: 'promo-labels',
  styleUrls: ['./promo-labels.component.less'],
  templateUrl: 'promo-labels.component.html'
})

export class LadbrokesPromoLabelsComponent extends PromoLabelsComponent {}
