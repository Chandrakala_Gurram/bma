import { HttpClientModule } from '@angular/common/http';
import {
  Injector,
  NgModule,
  NgModuleFactory,
  NgModuleFactoryLoader,
  NO_ERRORS_SCHEMA
} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BmaModule } from '@bmaModule/bma.module';
import { lazyComponentsManifests } from '@ladbrokesMobile/lazy-components.manifests';
import { CoreModule } from '@coreModule/core.module';
import { DynamicLoaderModule } from '@app/dynamicLoader/dynamic-loader.module';
import { SharedModule } from '@ladbrokesMobile/shared/shared.module';
import { AppRoutingModule } from '@ladbrokesMobile/app-routing.module';
import { RootComponent } from '@ladbrokesMobile/app.component';
import { runOnAppInit, STORE_PREFIX, VanillaCoreModule } from '@vanilla/core/core';
import { VanillaCommonModule } from '@vanilla/core/common';
import { AppComponent, VanillaFeaturesModule } from '@vanilla/core/features';
import { LabelHostFeaturesModule, LH_HTTP_ERROR_HANDLERS } from '@labelhost/core/features';
import { VanillaInitModule } from '@vanillaInitModule/vanilla-init.module';
import { CoralSportsClientConfigModule } from '@app/client-config/client-config.module';
import { LabelHostHttpErrorHandler } from '@app/vanillaInit/services/labelHostErrorHandler/labelHostHttpErrorHandler.service';
import { HostAppBootstrapper } from '@app/host-app/host-app-bootstrapper.service';
import { RoutingState } from '@shared/services/routingState/routing-state.service';
import { NativeBridgeBootstrapperService } from '@app/vanillaInit/services/NativeBridgeBootstrapper/native-bridge-bootstrapper.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    VanillaCoreModule.forRoot(),
    VanillaCommonModule.forRoot(),
    VanillaFeaturesModule.forRoot(),
    LabelHostFeaturesModule.forRoot(),
    CoreModule,
    VanillaInitModule,
    AppRoutingModule,
    SharedModule.forRoot(),
    BmaModule,
    DynamicLoaderModule.forRoot(lazyComponentsManifests),
    CoralSportsClientConfigModule.forRoot()
  ],
  declarations: [RootComponent],
  providers: [
    { provide: STORE_PREFIX, useValue: 'coralsports.' },
    { provide: LH_HTTP_ERROR_HANDLERS, useClass: LabelHostHttpErrorHandler, multi: true },
    runOnAppInit(HostAppBootstrapper)
  ],
  bootstrap: [AppComponent],
  entryComponents: [RootComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {
  constructor(private loader: NgModuleFactoryLoader, private injector: Injector,
    private routingState: RoutingState,
    private nativeBridgeBootstrapper: NativeBridgeBootstrapperService) {
    this.routingState.loadRouting();
    const paths = ['src/app/lazy-modules/locale/translation.module#TranslationModule',
      'src/app/lazy-modules/performanceMark/performanceMark.module#PerformanceMarkModule'];
    paths.forEach((path) => {
      this.loader
        .load(path)
        .then((moduleFactory: NgModuleFactory<any>) => moduleFactory.create(this.injector));
    });
    // Attaches Native Bridge Adapter and Portal Native Event Notifier.
    this.nativeBridgeBootstrapper.init();
  }
}
