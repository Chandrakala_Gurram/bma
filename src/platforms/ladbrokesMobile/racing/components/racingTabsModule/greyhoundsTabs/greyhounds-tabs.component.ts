import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ISystemConfig } from '@core/services/cms/models';
import {
  GreyhoundsTabsComponent as CoralGreyhoundsTabsComponent
} from '@racing/components/racingTabsModule/greyhoundsTabs/greyhounds-tabs.component';
import { Router } from '@angular/router';
import { RacingGaService } from '@racing/services/racing-ga.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { EventService } from '@sb/services/event/event.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'greyhounds-tabs',
  templateUrl: 'greyhounds-tabs.component.html',
  styleUrls: ['greyhounds-tabs.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class LadbrokesGreyhoundsTabsComponent extends CoralGreyhoundsTabsComponent implements OnInit, OnDestroy {
  nextRacesComponentEnabled: boolean = false;

  private cmsDataSubscription: Subscription;

  constructor(
    public router: Router,
    public filterService: FiltersService,
    public racingGaService: RacingGaService,
    public routingHelperService: RoutingHelperService,
    public eventService: EventService,
    public pubSubService: PubSubService,
    public cmsService: CmsService,
  ) {
    super(router,
      filterService,
      racingGaService,
      routingHelperService,
      eventService,
      pubSubService,
      cmsService);
  }

  /**
   * Check if next races component should be shown
   */
  get displayNextRaces(): boolean {
    return !this.responseError && this.display === 'today' && this.nextRacesComponentEnabled;
  }

  ngOnInit(): void {
    this.cmsDataSubscription = this.cmsService.getSystemConfig()
      .subscribe((config: ISystemConfig) => {
        this.nextRacesComponentEnabled = config && config.GreyhoundNextRacesToggle
          && config.GreyhoundNextRacesToggle.nextRacesComponentEnabled === true;
        super.ngOnInit();
      });
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();

    if (this.cmsDataSubscription) {
      this.cmsDataSubscription.unsubscribe();
    }
  }
}
