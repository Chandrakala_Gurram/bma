import { Component } from '@angular/core';
import { QuickNavigationComponent } from '@racing/components/quickNavigation/quick-navigation.component';

@Component({
  templateUrl: './quick-navigation.component.html',
  selector: 'quick-navigation',
  styleUrls: ['./quick-navigation.component.less']
})
export class LadbrokesMobileQuickNavigationComponent extends QuickNavigationComponent {}
