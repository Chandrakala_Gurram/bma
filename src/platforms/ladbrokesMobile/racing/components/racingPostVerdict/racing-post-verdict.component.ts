import { Component, Input, OnInit } from '@angular/core';
import { IRacingPostVerdict, IStarRating } from '@racing/models/racing-post-verdict.model';


@Component({
  selector: 'racing-post-verdict',
  templateUrl: 'racing-post-verdict.component.html',
  styleUrls: ['racing-post-verdict.component.less']
})

export class RacingPostVerdictComponent implements OnInit {
  @Input() data: IRacingPostVerdict;
  @Input() showMap: boolean;
  resultLimit = 3;

  constructor() {
  }

  ngOnInit(): void {
    this.data.starRatings.sort(this.compareByStarsAndName);
    if (this.data.starRatings.length > this.resultLimit) {
      this.data.starRatings.splice(this.resultLimit, this.data.starRatings.length);
    }
  }

  trackByIndex(index: number): number {
    return index;
  }

  private compareByStarsAndName(a: IStarRating, b: IStarRating): number {
    if (Number(a.rating) === Number(b.rating)) {
      return a.name > b.name ? 1 : -1;
    } else if (Number(a.rating) > Number(b.rating)) {
      return -1;
    } else {
      return 1;
    }
  }
}

