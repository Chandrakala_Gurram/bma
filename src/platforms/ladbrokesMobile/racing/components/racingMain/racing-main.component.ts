import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';

import {
  RacingMainComponent as CoralRacingMainComponent
} from '@racing/components/racingMain/racing-main.component';
import { ActivatedRoute, Router } from '@angular/router';
import { TemplateService } from '@shared/services/template/template.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { RoutesDataSharingService } from '@racing/services/routesDataSharing/routes-data-sharing.service';
import { HorseracingService } from '@coreModule/services/racing/horseracing/horseracing.service';
import { GreyhoundService } from '@coreModule/services/racing/greyhound/greyhound.service';
import { RoutingState } from '@sharedModule/services/routingState/routing-state.service';
import { CmsService } from '@ladbrokesMobile/core/services/cms/cms.service';
import { forkJoin as observableForkJoin, of as observableOf, Subscription } from 'rxjs';
import { ISportCategory, ISystemConfig } from '@core/services/cms/models';
import { IInitialSportConfig } from '@core/services/sport/config/initial-sport-config.model';
import { WindowRefService } from '@root/app/core/services/windowRef/window-ref.service';
import { map, switchMap, mergeMap } from 'rxjs/operators';
import { GtmService } from '@core/services/gtm/gtm.service';
import { IRacingHeader } from '@root/app/shared/models/racing-header.model';
import { PubSubService } from '@root/app/core/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'racing-main-component',
  templateUrl: './racing-main.component.html'
})
export class RacingMainComponent extends CoralRacingMainComponent implements OnInit, OnDestroy {
  isNextRacesEnabled: boolean;
  isNextRacesTabEnabled: boolean;
  private routeListener: Subscription;

  constructor(
    public route: ActivatedRoute,
    public templateService: TemplateService,
    public routingHelperService: RoutingHelperService,
    public routesDataSharingService: RoutesDataSharingService,
    public router: Router,
    public horseRacingService: HorseracingService,
    public greyhoundService: GreyhoundService,
    public routingState: RoutingState,
    public cms: CmsService,
    public changeDetRef: ChangeDetectorRef,
    public windowRefService: WindowRefService,
    protected gtmService: GtmService,
    protected pubSubService: PubSubService
  ) {
    super(
      route,
      templateService,
      routingHelperService,
      routesDataSharingService,
      router,
      horseRacingService,
      greyhoundService,
      routingState,
      cms,
      changeDetRef,
      windowRefService,
      gtmService,
      pubSubService
    );
  }

  ngOnInit(): void {
    this.isHRDetailPage = this.isHorseRacingDetailPage();
    this.pubSubService.subscribe(this.RACING_MAIN_COMPONENT, 'TOP_BAR_DATA', (topBar: IRacingHeader) => {
      this.breadcrumbsItems = topBar.breadCrumbs;
      this.quickNavigationItems = topBar.quickNavigationItems;
      this.eventEntity = topBar.eventEntity;
      this.meetingsTitle = topBar.meetingsTitle;
      const breadCrumbsLength = this.breadcrumbsItems.length;
      if (breadCrumbsLength && this.isHRDetailPage) {
        const displayName = this.breadcrumbsItems[breadCrumbsLength - 1].name;
        this.breadcrumbsItems[breadCrumbsLength - 1].name = displayName.length > 7 ?
          `${this.breadcrumbsItems[breadCrumbsLength - 1].name.substring(0, 7)}...` : displayName;
      }
    });
    this.addChangeDetection();
    this.racingMainSubscription = observableForkJoin([
      observableOf(this.racingService.getSport()),
      this.racingService.isSpecialsAvailable(this.router.url),
      this.cms.getSystemConfig(false)
    ]).pipe(
      mergeMap((racingData: [(HorseracingService | GreyhoundService), boolean, ISystemConfig]) => {
        this.racingData = racingData;
        this.initModel();

        let nextRacesToggle;
        if (this.racingName === 'greyhound') {
          nextRacesToggle = 'GreyhoundNextRacesToggle';
        } else if (this.racingName === 'horseracing') {
          nextRacesToggle = 'NextRacesToggle';
        }
        this.isNextRacesTabEnabled = this.racingData && this.racingData[2]
          && this.racingData[2][nextRacesToggle]
          && this.racingData[2][nextRacesToggle].nextRacesTabEnabled === true;
        this.isNextRacesEnabled = this.racingData && this.racingData[2]
          && this.racingData[2][nextRacesToggle]
          && this.racingData[2][nextRacesToggle].nextRacesComponentEnabled === true;

        // Workaround, related to SEO Static Blocks
        // Needed if 'sb' module is initialized and user types /:static path into address bar
        return this.racingInstance ? observableOf(null) : observableOf();
      }),
      map(() => {
        this.defaultTab = (this.racingName === 'greyhound') ? 'races' : 'featured';
        this.applyRacingConfiguration(this.racingInstance);
        this.selectTabRacing();
      }),
      switchMap(() => this.routesDataSharingService.activeTabId),
      map(id => {
        if (id) {
          this.activeTab = { id };
        }
      }),
      switchMap(() => this.routesDataSharingService.hasSubHeader),
      map(hasSubHeader => this.hasSubHeader = hasSubHeader),
      switchMap(() => this.racingId ? this.templateService.getIconSport(this.racingId) : observableOf(null)),
    ).subscribe((icon: ISportCategory) => {
      if (icon) {
        this.racingIconId = icon.svgId;
        this.racingIconSvg = icon.svg;
      }
      this.hideSpinner();
      this.hideError();
    }, () => {
      this.showError();
    });
    this.routeListener = this.router.events.subscribe(() => {
      this.isHRDetailPage = this.isHorseRacingDetailPage();
    });

    super.getSystemConfig();
  }

  /**
   * Set racing configuration to model from racing config constant, for example: 'HORSERACING_CONFIG'
   * @param racingInstance
   */
  applyRacingConfiguration(racingInstance: any) {
    const racingConfiguration: IInitialSportConfig = racingInstance.getGeneralConfig();

    this.routingHelperService.formSportUrl(this.racingName,
      this.racingName === 'horseracing' || this.racingName === 'greyhound' ? 'races/next' : 'today')
      .subscribe((url: string) => {
        this.racingDefaultPath = url;
      });

    this.racingId = racingConfiguration.config.request.categoryId;

    // Racing tabs information
    this.racingTabs = racingInstance
      .configureTabs(this.racingName, racingConfiguration.tabs, this.isSpecialsPresent, this.isNextRacesTabEnabled);

    this.routesDataSharingService.setRacingTabs(this.racingName, this.racingTabs);

    this.activeTab = {
      id: racingInstance.getGeneralConfig().tabs[0].id
    };
  }

  ngOnDestroy() {
    this.pubSubService.unsubscribe(this.RACING_MAIN_COMPONENT);
    this.routeListener && this.routeListener.unsubscribe();
  }
}
