import { Observable, of } from 'rxjs/index';
import { fakeAsync, tick } from '@angular/core/testing';
import { NavigationEnd } from '@angular/router';
import { RacingMainComponent } from './racing-main.component';

describe('RacingMainComponent', () => {
  let component: RacingMainComponent;
  let routingState,
      templateService,
      activatedRoute,
      routingHelperService,
      routesDataSharingService,
      router,
      horseracingService,
      greyhoundService,
      cmsService,
      changeDetector,
      windowRefService,
      gtmService,
      pubSubService;

  const sConfig = {
    BetFilterHorseRacing: {
      enabled: true
    }
  };

  beforeEach(() => {
    activatedRoute = {} as any;
    templateService = {
      getIconSport: jasmine.createSpy('getIconSport').and.returnValue(of({}))
    };
    routingHelperService = {
      formSportUrl: jasmine.createSpy('formSportUrl').and.returnValue(of(''))
    } as any;
    routesDataSharingService = {
      setRacingTabs: jasmine.createSpy('setRacingTabs')
    } as any;
    router = {
      url: '/horse-racing/featured',
      events: new Observable(observer => {
        const event = new NavigationEnd(0, 'test', 'test');

        observer.next(event);
        observer.complete();
      }),
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    } as any;
    horseracingService = {
      getSport: jasmine.createSpy('getSport').and.returnValue(of([])),
      isSpecialsAvailable: jasmine.createSpy('isSpecialsAvailable').and.returnValue(of([]))
    } as any;
    greyhoundService = {
      getSport: jasmine.createSpy('getSport').and.returnValue(of([])),
      isSpecialsAvailable: jasmine.createSpy('isSpecialsAvailable').and.returnValue(of([]))
    } as any;
    routingState = {
      getCurrentSegment: jasmine.createSpy('getCurrentSegment').and.returnValue('horceracing')
    };
    cmsService = {
      getToggleStatus: jasmine.createSpy('getToggleStatus').and.returnValue(of(true)),
      getSystemConfig: jasmine.createSpy('getSystemConfig').and.returnValue(of(sConfig))
    } as any;
    changeDetector = {
      detach: jasmine.createSpy('detach'),
      detectChanges: jasmine.createSpy()
    } as any;
    windowRefService = {
      nativeWindow: {
        setInterval: jasmine.createSpy('setInterval'),
        clearInterval: jasmine.createSpy('clearInterval')
      }
    } as any;

    routingState = jasmine.createSpyObj(['getCurrentSegment']);
    gtmService = {
      push: jasmine.createSpy('push')
    };
    pubSubService = {
      publish: jasmine.createSpy(),
      subscribe: jasmine.createSpy().and.callFake((file, method, callback) => {
        if (method === 'TOP_BAR_DATA') {
          callback({
            breadCrumbs: [],
            quickNavigationItems: [],
            eventEntity: null,
            meetingsTitle: null
          });
        }
      }),
      unsubscribe: jasmine.createSpy()
    };


    component = new RacingMainComponent(
      activatedRoute,
      templateService,
      routingHelperService,
      routesDataSharingService,
      router,
      horseracingService,
      greyhoundService,
      routingState,
      cmsService,
      changeDetector,
      windowRefService,
      gtmService,
      pubSubService
    );
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    component.addChangeDetection = jasmine.createSpy();
    component['routingState'].getCurrentSegment = jasmine.createSpy().and.returnValue('horseracing.eventMain');
    component['routesDataSharingService'].activeTabId = of('races');
    component['selectTabRacing'] = jasmine.createSpy('selectTabRacing');

    component.ngOnInit();
    expect(component.addChangeDetection).toHaveBeenCalled();
    expect(component.breadcrumbsItems.length).toBe(0);
  });

  describe('#ngoninit breadcrumbs data', () => {
    it('breadcrumbs name is within the length', () => {
      pubSubService = {
        publish: jasmine.createSpy(),
        subscribe: jasmine.createSpy().and.callFake((file, method, callback) => {
          if (method === 'TOP_BAR_DATA') {
            callback({
              breadCrumbs: [{
                name: 'Prairie'
              }],
              quickNavigationItems: [],
              eventEntity: null,
              meetingsTitle: null
            } as any);
          }
        }),
        unsubscribe: jasmine.createSpy()
      };
      component = new RacingMainComponent(activatedRoute, templateService, routingHelperService, routesDataSharingService,
        router, horseracingService, greyhoundService, routingState, cmsService, changeDetector,
        windowRefService, gtmService, pubSubService);
      component.addChangeDetection = jasmine.createSpy();
      component['routingState'].getCurrentSegment = jasmine.createSpy().and.returnValue('horseracing.eventMain');
      component['routesDataSharingService'].activeTabId = of('races');
      component['selectTabRacing'] = jasmine.createSpy('selectTabRacing');

      component.ngOnInit();
      expect(component.breadcrumbsItems.length).not.toBe(0);
      expect(component.breadcrumbsItems[0].name).toBe('Prairie');
    });
    it('breadcrumbs name exceeds the length', () => {
      pubSubService = {
        publish: jasmine.createSpy(),
        subscribe: jasmine.createSpy().and.callFake((file, method, callback) => {
          if (method === 'TOP_BAR_DATA') {
            callback({
              breadCrumbs: [{
                name: 'Indiana Grand Race Course'
              }],
              quickNavigationItems: [],
              eventEntity: null,
              meetingsTitle: null
            } as any);
          }
        }),
        unsubscribe: jasmine.createSpy()
      };
      component = new RacingMainComponent(activatedRoute, templateService, routingHelperService, routesDataSharingService,
        router, horseracingService, greyhoundService, routingState, cmsService, changeDetector,
        windowRefService, gtmService, pubSubService);
      component.addChangeDetection = jasmine.createSpy();
      component['routingState'].getCurrentSegment = jasmine.createSpy().and.returnValue('horseracing.eventMain');
      component['routesDataSharingService'].activeTabId = of('races');
      component['selectTabRacing'] = jasmine.createSpy('selectTabRacing');

      component.ngOnInit();
      expect(component.breadcrumbsItems[0].name).toBe('Indiana...');
    });
  });

  describe('isDetailPage', () => {
    it(`should return Truthy if currentSegment == 'greyhound.eventMain.market'`, () => {
      component['routingState'].getCurrentSegment = jasmine.createSpy().and.returnValue('greyhound.eventMain.market');

      expect(component.isDetailPage).toBeTruthy();
    });
  });

  it('#ngOnDestroy', () => {
    component['routeListener'] = {
      unsubscribe: jasmine.createSpy()
    } as any;
    component.ngOnDestroy();
    expect(pubSubService.unsubscribe).toHaveBeenCalled();
    expect(component['routeListener'].unsubscribe).toHaveBeenCalled();
  });

  it('applyRacingConfiguration', fakeAsync(() => {
    const racingInstance = {
      getGeneralConfig: jasmine.createSpy('getGeneralConfig').and.returnValue({
        config: {
          request: {}
        },
        tabs: [{}]
      }),
      configureTabs: jasmine.createSpy('configureTabs')
    };

    component.applyRacingConfiguration(racingInstance);
    tick();

    expect(racingInstance.getGeneralConfig).toHaveBeenCalled();
    expect(racingInstance.configureTabs).toHaveBeenCalled();
    expect(routesDataSharingService.setRacingTabs).toHaveBeenCalled();
    expect(component['racingDefaultPath']).toBeDefined();
  }));
});
