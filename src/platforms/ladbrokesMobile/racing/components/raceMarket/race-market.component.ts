/**
 * @class Race market controller To Finish, Top Finish, Place Insurance
 */

import { Component, Input } from '@angular/core';

import { RaceMarketComponent } from '@racing/components/raceMarket/race-market.component';
import { IOutcome } from '@core/models/outcome.model';
import { RaceOutcomeDetailsService } from '@core/services/raceOutcomeDetails/race-outcome-details.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { SbFiltersService } from '@sb/services/sbFilters/sb-filters.service';
import { RacingService } from '@coreModule/services/sport/racing.service';
import { GtmService } from '@core/services/gtm/gtm.service';

@Component({
  selector: 'race-market-component',
  templateUrl: 'race-market.component.html',
  styleUrls: ['race-market.less'],
})
export class LadbrokesRaceMarketComponent extends RaceMarketComponent {
  @Input() isGreyhoundEdp?: boolean;
  @Input() sortOptionsEnabled: boolean;
  @Input() mIndex: number;
  @Input() sortOptionsEnabledFn: Function;

  constructor(
    protected raceOutcomeDetailsService: RaceOutcomeDetailsService,
    protected filterService: FiltersService,
    protected locale: LocaleService,
    protected pubsubService: PubSubService,
    protected sbFiltersService: SbFiltersService,
    protected racingService: RacingService,
    protected gtmService: GtmService,
  ) {
    super(raceOutcomeDetailsService, filterService, locale, pubsubService, sbFiltersService, racingService);
  }

  isShowMore(outcomeEntity: IOutcome): boolean {
    return (outcomeEntity.racingFormOutcome && !outcomeEntity.isFavourite &&
      (!this.isGreyhoundEdp || outcomeEntity.racingFormOutcome.overview)) || outcomeEntity.timeformData;
  }

  addTrackEvent(): void {
    this.gtmService.push('trackEvent', {
      eventCategory: 'horse racing',
      eventAction: 'race card',
      eventLabel: 'details'
    });
  }

  toggleShowOptions(expandedSummary: Array<Array<boolean>>, showOption: boolean): void {
    expandedSummary.forEach((el) => el[0] = showOption);
  }

  onExpand(expandedSummary: boolean[][], oIndex: number): void {
    expandedSummary[oIndex][0] = !expandedSummary[oIndex][0];
  }
}
