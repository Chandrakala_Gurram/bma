import { Component } from '@angular/core';
import { GreyhoundFutureTabComponent } from '@racing/components/greyhound/greyhoundFutureTab/greyhound-future-tab.component';
import { EventTimeService } from '@ladbrokesMobile/racing/services/event-time.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { EventService } from '@sb/services/event/event.service';
import { RoutingHelperService } from '@core/services/routingHelper/routing-helper.service';
import { ISportEvent } from '@core/models/sport-event.model';

@Component({
  selector: 'greyhound-future-tab',
  templateUrl: 'greyhound-future-tab.component.html'
})

export class LadbrokesGreyhoundFutureTabComponent extends GreyhoundFutureTabComponent {
  constructor(
    public filterService: FiltersService,
    public eventService: EventService,
    public routingHelperService: RoutingHelperService,
    private eventTimeService: EventTimeService
  ) {
    super(filterService, eventService, routingHelperService);
  }

  public getRaceTimeView(event: ISportEvent): string {
    return this.eventTimeService.getDate(event);
  }
}
