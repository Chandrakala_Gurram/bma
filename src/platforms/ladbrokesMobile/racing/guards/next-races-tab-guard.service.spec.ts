import { of } from 'rxjs';
import { NextRacesTabGuard } from '@ladbrokesMobile/racing/guards/next-races-tab-guard.service';

describe('NextRacesTabGuard', () => {
  let service;
  let router;
  let cmsService;
  let configMock;
  beforeEach(() => {
    configMock = {
      NextRacesToggle:
        {
          nextRacesTabEnabled: true
        }
    };
    router = {
      navigate: jasmine.createSpy('navigate')
    };
    cmsService = {
      getSystemConfig: jasmine.createSpy('getSystemConfig').and.returnValue(of(configMock))
    };

    service = new NextRacesTabGuard(cmsService, router);
  });

  it('constructor', () => {
    expect(service).toBeDefined(true);
  });

  describe('canActivate', () => {
    it('should allow activate route if nextRacesTabEnable enabled', () => {
      service.canActivate().subscribe(canActivate => {
        expect(canActivate).toEqual(true);
      });
    });

    it('should not allow activate route if nextRacesTabEnable disabled', () => {
      configMock = {
        NextRacesToggle:
          {
            nextRacesTabEnabled: false
          }
      };
      service['cmsService'].getSystemConfig = jasmine.createSpy('getSystemConfig').and.returnValue(of(configMock));
      service.canActivate().subscribe(canActivate => {
        expect(canActivate).toEqual(false);
      });
      expect(router.navigate).toHaveBeenCalledWith([ '/horse-racing/' ]);
    });
  });
});
