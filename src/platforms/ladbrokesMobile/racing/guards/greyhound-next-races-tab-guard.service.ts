import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CmsService } from '@coreModule/services/cms/cms.service';

@Injectable()
export class GreyhoundNextRacesTabGuard implements CanActivate {
  constructor(
    private cmsService: CmsService,
    private router: Router
  ) { }

  canActivate(): Observable<boolean> {
    return this.cmsService.getSystemConfig().pipe(map(config => {
        if (config.GreyhoundNextRacesToggle && config.GreyhoundNextRacesToggle.nextRacesTabEnabled === true) {
          return true;
        } else {
          this.router.navigate(['/greyhound-racing/today']);
          return false;
        }
      })
    );
  }
}
