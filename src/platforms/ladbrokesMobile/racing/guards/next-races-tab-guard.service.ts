import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class NextRacesTabGuard implements CanActivate {
  constructor(
    private cmsService: CmsService,
    private router: Router
  ) {
  }

  canActivate(): Observable<boolean> {
    return this.cmsService.getSystemConfig().pipe(map(config => {
        if (config.NextRacesToggle && config.NextRacesToggle.nextRacesTabEnabled === true) {
          return true;
        } else {
          this.router.navigate(['/horse-racing/']);
          return false;
        }
      })
    );
  }
}
