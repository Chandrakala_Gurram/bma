import { HttpClientModule } from '@angular/common/http';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { RacingTabYourcallComponent } from '@racing/components/racingTabYourcall/racing-tab-yourcall.component';
import { TimeFormSummaryComponent } from '@racing/components/timeformSummary/time-form-summary.component';
import { SharedModule } from '@sharedModule/shared.module';
import { RacingGaService } from '@racing/services/racing-ga.service';
import { RoutesDataSharingService } from '@racing/services/routesDataSharing/routes-data-sharing.service';
import { RacingTabsMainComponent } from '@racingModule/components/racingTabsMain/racing-tabs-main.component';
import { QuantumLeapComponent } from '@racing/components/quantumLeap/quantum-leap.component';

import { RacingYourcallSpecialsComponent } from '@racing/components/racingYourcallSpecials/racing-yourcall-specials.component';

import { RacingSpecialsCarouselService } from '@racing/components/racingSpecialsCarousel/racing-specials-carousel.service';
import { RacingEnhancedMultiplesService } from '@racing/components/racingEnhancedMultiples/racing-enhanced-multiples.service';
import { RacingEnhancedMultiplesComponent } from '@racing/components/racingEnhancedMultiples/racing-enhanced-multiples.component';

import { SbModule } from '@sbModule/sb.module';

import { SortByOptionsService } from '@racing/services/sortByOptions/sort-by-options.service';
import { RacingRunService } from '@racing/services/racingRunService/racing-run.service';
import { BannersModule } from '@banners/banners.module';
import { RacingPostVerdictComponent } from '@ladbrokesMobile/racing/components/racingPostVerdict/racing-post-verdict.component';
import { RacingPostVerdictLabelComponent } from '@ladbrokesMobile/racing/components/racingPostVerdict/racing-post-verdict-label.component';
import { LadbrokesRacingPostWidgetComponent } from '@ladbrokesMobile/racing/components/racingPostWidget/racing-post-widget.component';
import { RacingResultsService } from '@core/services/sport/racing-results.service';

import {
  LadbrokesRacingSpecialsCarouselComponent
} from '@ladbrokesMobile/racing/components/racingSpecialsCarousel/racing-specials-carousel.component';
import {
  HorseracingTabsComponent
} from '@ladbrokesMobile/racing/components/racingTabsModule/horseracingTabs/horseracing-tabs.component';
// tslint:disable: max-line-length
import { LadbrokesRacingEventComponent } from '@ladbrokesMobile/racing/components/racingEventComponent/racing-event.component';
import { LadbrokesRacingAntepostTabComponent } from '@ladbrokesMobile/racing/components/racingAntepostTab/racing-antepost-tab.component';
import { LadbrokesGreyhoundFutureTabComponent } from '@ladbrokesMobile/racing/components/greyhound/greyhoundFutureTab/greyhound-future-tab.component';
import { LadbrokesGreyhoundsTabsComponent } from '@ladbrokesMobile/racing/components/racingTabsModule/greyhoundsTabs/greyhounds-tabs.component';
import { EventTimeService } from '@ladbrokesMobile/racing/services/event-time.service';
import { LadbrokesRacingEventResultedComponent } from '@ladbrokesMobile/racing/components/racingEventResultedComponent/racing-event-resulted.component';
import { LadbrokesRacingEventMainComponent } from '@ladbrokesMobile/racing/components/racingEventMain/racing-event-main.component';
import { LadbrokesRaceCardsControlsComponent } from '@ladbrokesMobile/racing/components/race-cards-controls/race-cards-controls.component';
import { LadbrokesMobileRacingSpecialsTabComponent } from '@ladbrokesMobile/racing/components/racingSpecialsTab/racing-specials-tab.component';
import { LadbrokesRacingOutcomeResultedCardComponent } from '@ladbrokesMobile/racing/components/racingOutcomeResultedCard/racing-outcome-resulted-card.component';
import { LadbrokesMobileQuickNavigationComponent } from '@ladbrokesMobile/racing/components/quickNavigation/quick-navigation.component';
import { RacingMainComponent } from '@ladbrokesMobile/racing/components/racingMain/racing-main.component';
import { NextRacesTabGuard } from '@ladbrokesMobile/racing/guards/next-races-tab-guard.service';
import { LadbrokesRaceMarketComponent } from '@ladbrokesMobile/racing/components/raceMarket/race-market.component';
import { GreyhoundNextRacesTabGuard } from '@ladbrokesMobile/racing/guards/greyhound-next-races-tab-guard.service';
import { RacingPostPickComponent } from '@racing/components/racingPostPick/racing-post-pick.component';
import { ForecastTricastGuard } from '@racing/guards/forecast-tricast-guard.service';

@NgModule({
  declarations: [
    // Overridden Component
    LadbrokesRacingSpecialsCarouselComponent,
    LadbrokesMobileRacingSpecialsTabComponent,
    LadbrokesGreyhoundsTabsComponent,
    LadbrokesMobileQuickNavigationComponent,

    TimeFormSummaryComponent,
    RacingYourcallSpecialsComponent,
    RacingEnhancedMultiplesComponent,
    LadbrokesRacingAntepostTabComponent,
    LadbrokesRacingEventComponent,
    LadbrokesRacingOutcomeResultedCardComponent,
    LadbrokesRacingEventResultedComponent,
    LadbrokesRaceMarketComponent,
    QuantumLeapComponent,
    RacingTabYourcallComponent,
    RacingMainComponent,
    RacingTabsMainComponent,
    RacingPostVerdictComponent,
    RacingPostVerdictLabelComponent,
    LadbrokesRacingPostWidgetComponent,
    LadbrokesGreyhoundFutureTabComponent,
    HorseracingTabsComponent,
    LadbrokesRacingEventMainComponent,
    LadbrokesRaceCardsControlsComponent,
    RacingPostPickComponent
  ],
  imports: [
    HttpClientModule,
    SharedModule,
    SbModule,
    BannersModule
  ],
  exports: [
    // Overridden Component
    LadbrokesRacingSpecialsCarouselComponent,
    TimeFormSummaryComponent,
    RacingYourcallSpecialsComponent,
    RacingEnhancedMultiplesComponent,
    LadbrokesRacingAntepostTabComponent,
    QuantumLeapComponent,
    LadbrokesGreyhoundsTabsComponent,
    RacingTabYourcallComponent,
    LadbrokesRaceMarketComponent,
    RacingMainComponent,
    RacingTabsMainComponent,
    RacingPostVerdictComponent,
    RacingPostVerdictLabelComponent,
    LadbrokesRacingPostWidgetComponent,
    HorseracingTabsComponent,
    LadbrokesRacingEventMainComponent,
    LadbrokesRaceCardsControlsComponent,
    LadbrokesMobileRacingSpecialsTabComponent,
    RacingPostPickComponent,
    LadbrokesMobileQuickNavigationComponent
  ],
  providers: [
    RacingGaService,
    RoutesDataSharingService,
    RacingSpecialsCarouselService,
    RacingEnhancedMultiplesService,
    RacingRunService,
    SortByOptionsService,
    EventTimeService,
    NextRacesTabGuard,
    GreyhoundNextRacesTabGuard,
    RacingResultsService,
    ForecastTricastGuard
  ],
  entryComponents: [
    // Overridden Component
    LadbrokesRacingSpecialsCarouselComponent,
    LadbrokesMobileRacingSpecialsTabComponent,
    LadbrokesMobileQuickNavigationComponent,

    TimeFormSummaryComponent,
    RacingYourcallSpecialsComponent,
    RacingEnhancedMultiplesComponent,
    LadbrokesRacingAntepostTabComponent,
    QuantumLeapComponent,
    LadbrokesGreyhoundsTabsComponent,
    RacingTabYourcallComponent,
    HorseracingTabsComponent,
    LadbrokesRacingEventComponent,
    LadbrokesRaceMarketComponent,
    RacingMainComponent,
    RacingTabsMainComponent,
    RacingPostVerdictComponent,
    RacingPostVerdictLabelComponent,
    LadbrokesRacingPostWidgetComponent,
    LadbrokesGreyhoundFutureTabComponent,
    LadbrokesRacingEventMainComponent,
    LadbrokesRaceCardsControlsComponent,
    RacingPostPickComponent
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class RacingModule {
  constructor(racingRunService: RacingRunService) {
    racingRunService.run();
  }
}
