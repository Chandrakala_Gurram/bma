export const SPORT_MATCHES = [
  {
    SPORT_ID: 16,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result',
      'To Qualify',
      'Match Result and Both Teams To Score',
      'Over/Under Total Goals 2.5',
      'Win & Both Teams To Score',
      'Both Teams to Score',
      'Draw No Bet',
      'First-Half Result'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Both Teams to Score': 'Both Teams to Score',
      'Match Result and Both Teams To Score': 'Match Result & Both Teams To Score',
      'Over/Under Total Goals 2.5': 'Total Goals Over/Under 2.5',
      'Win & Both Teams To Score': 'Win & Both Teams To Score',
      'Draw No Bet': 'Draw No Bet',
      'First-Half Result': '1st Half Result',
      'To Qualify': 'To Qualify'
    }
  },
  {
    SPORT_ID: 34,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result', 'Games Total', 'Games Handicap', 'Current Set Winner'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Games Total': 'Games Total',
      'Games Handicap': 'Games Handicap',
      'Current Set Winner': '1st Set Winner'
    }
  },
  {
    SPORT_ID: 6,
    DEFAULT_SELECTED_OPTION: 'Money Line',
    MARKETS_NAME_ORDER: [
      'Money Line', 'Total Points', 'Handicap', 'Home team total points', 'Away team total points'
    ],
    MARKETS_NAMES: {
      'Money Line': 'Money Line',
      'Total Points': 'Total Points',
      'Handicap': 'Handicap',
      'Home team total points': 'Home Team Total points',
      'Away team total points': 'Away Team Total Points'
    }
  },
  {
    SPORT_ID: 10,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result', 'Total Sixes'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Total Sixes': 'Total Sixes'
    }
  },
  {
    SPORT_ID: 13,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result', 'Most 180s', 'Leg Handicap', 'Match Handicap', 'Total 180s Over/Under'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Most 180s': 'Most 180s',
      'Leg Handicap': 'Handicap',
      'Match Handicap': 'Handicap',
      'Total 180s Over/Under': 'Total 180s'
    }
  },
  {
    SPORT_ID: 22,
    DEFAULT_SELECTED_OPTION: 'Money Line',
    MARKETS_NAME_ORDER: [
      'Money Line', '60 Minutes Betting', 'Puck Line', 'Total Goals 2-way'
    ],
    MARKETS_NAMES: {
      'Money Line': 'Money Line',
      '60 Minutes Betting': '60 Minutes Betting',
      'Puck Line': 'Puck Line',
      'Total Goals 2-way': 'Total Goals 2-way'
    }
  },
  {
    SPORT_ID: 18,
    DEFAULT_SELECTED_OPTION: '2 Ball Betting',
    MARKETS_NAME_ORDER: [
      '3 Ball Betting', '2 Ball Betting'
    ],
    MARKETS_NAMES: {
      '3 Ball Betting': '3 Ball Betting',
      '2 Ball Betting': '2 Ball Betting'
    }
  },
  {
    SPORT_ID: 1,
    DEFAULT_SELECTED_OPTION: 'Money Line',
    MARKETS_NAME_ORDER: [
      'Money Line', 'Handicap 2-way', '60 Minute Betting', 'Total Points'
    ],
    MARKETS_NAMES: {
      'Money Line': 'Money Line',
      'Handicap 2-way': 'Handicap',
      '60 Minute Betting': '60 Minute Betting',
      'Total Points': 'Total Points'
    }
  },
  {
    SPORT_ID: 9,
    DEFAULT_SELECTED_OPTION: 'Fight Betting',
    MARKETS_NAME_ORDER: [
      'Fight Betting'
    ],
    MARKETS_NAMES: {
      'Fight Betting': 'Fight Betting'
    }
  },
  {
    SPORT_ID: 32,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result', 'Match Handicap', 'Total Frames Over/Under'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Match Handicap': 'Handicap',
      'Total Frames Over/Under': 'Total Frames'
    }
  },
  {
    SPORT_ID: 5,
    DEFAULT_SELECTED_OPTION: 'Money Line',
    MARKETS_NAME_ORDER: [
      'Money Line', 'Total Runs', 'Run Line', 'Home Total Runs', 'Away Total Runs'
    ],
    MARKETS_NAMES: {
      'Money Line': 'Money Line',
      'Total Runs': 'Total Runs',
      'Run Line': 'Run Line',
      'Home Total Runs': 'Home Total Runs',
      'Away Total Runs': 'Away Total Runs'
    }
  },
  {
    SPORT_ID: 31,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result', 'Handicap 2-way', 'Total Match Points'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Handicap 2-way': 'Handicap 2-way',
      'Total Match Points': 'Total Points'
    }
  },
  {
    SPORT_ID: 30,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result', 'Handicap 2-way', 'Total Match Points'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Handicap 2-way': 'Handicap 2-way',
      'Total Match Points': 'Total Points'
    }
  },
  {
    SPORT_ID: 36,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result', 'Set Handicap', 'Total Match Points'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Set Handicap': 'Set Handicap',
      'Total Match Points': 'Total Points'
    }
  }
];

export const SPORT_INPLAY = [
  {
    SPORT_ID: 16,
    DEFAULT_SELECTED_OPTION: 'Match Betting',
    MARKETS_NAME_ORDER: [
      'Main Market',
      'Penalty Shoot-Out Winner',
      'Match Betting',
      'Match Result',
      'Next Team to Score',
      'Match Result and Both Teams To Score',
      'Draw No Bet',
      'First-Half Result',
      'Total Goals Over/Under 1.5',
      'Total Goals Over/Under 3.5',
      'Total Goals Over/Under 4.5',
      'To Qualify',
      'Extra-Time Result',
      'Over/Under Total Goals 2.5',
      'Both Teams to Score',
    ],
    MARKETS_NAMES: {
      'Main Market': 'Main Markets',
      'Penalty Shoot-Out Winner': 'Penalty Shoot-Out Winner',
      'Match Betting': 'Match Result',
      'Match Result': 'Match Result',
      'To Qualify': 'To Qualify',
      'Next Team to Score': 'Next Team to Score',
      'Extra-Time Result': 'Extra Time Result',
      'Match Result and Both Teams To Score': 'Match Result & Both Teams To Score',
      'Both Teams to Score': 'Both Teams to Score',
      'Draw No Bet': 'Draw No Bet',
      'First-Half Result': '1st Half Result',
      'Over/Under Total Goals 1.5': 'Total Goals Over/Under 1.5',
      'Over/Under Total Goals 2.5': 'Total Goals Over/Under 2.5',
      'Over/Under Total Goals 3.5': 'Total Goals Over/Under 3.5',
      'Over/Under Total Goals 4.5': 'Total Goals Over/Under 4.5'
    }
  },
  {
    SPORT_ID: 34,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Main Market', 'Match Result', 'Match Betting', 'Current Set Winner', 'Games Total', 'Next Set Winner',
      'Next Game Winner', 'Current Set Games Total', 'Current Game Winner'
    ],
    MARKETS_NAMES: {
      'Main Market': 'Main Markets',
      'Match Result': 'Match Result',
      'Match Betting': 'Match Result',
      'Current Set Winner': 'Current Set Winner',
      'Games Total': 'Games Total',
      'Next Set Winner': 'Next Set Winner',
      'Next Game Winner': 'Next Game Winner',
      'Current Set Games Total': 'Current Set Games Total',
      'Current Game Winner': 'Current Game Winner'
    }
  },
  {
    SPORT_ID: 6,
    DEFAULT_SELECTED_OPTION: 'Money Line',
    MARKETS_NAME_ORDER: [
      'Money Line', 'Total Points', 'Handicap', 'Half Total Points', 'Quarter Total Points'
    ],
    MARKETS_NAMES: {
      'Main Market': 'Main Markets',
      'Money Line': 'Money Line',
      'Total Points': 'Total Points',
      'Handicap': 'Handicap',
      'Half Total Points': 'Current Half Total Points',
      'Quarter Total Points': 'Current Quarter Total Points'
    }
  }
];

export const SPORT_COMPETITIONS = [
  {
    SPORT_ID: 16,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result',
      'To Qualify',
      'Next Team to Score',
      'Extra-Time Result',
      'Over/Under Total Goals 2.5',
      'Both Teams to Score',
      'Match Result and Both Teams To Score',
      'Draw No Bet',
      'First-Half Result',
      'Win & Both Teams To Score'
    ],
    MARKETS_NAMES: {
      'Next Team to Score': 'Next Team to Score',
      'Extra-Time Result': 'Extra Time Result',
      'Match Result': 'Match Result',
      'Both Teams to Score': 'Both Teams to Score',
      'Match Result and Both Teams To Score': 'Match Result & Both Teams To Score',
      'Over/Under Total Goals 2.5': 'Total Goals Over/Under 2.5',
      'Win & Both Teams To Score': 'Win & Both Teams To Score',
      'Draw No Bet': 'Draw No Bet',
      'First-Half Result': '1st Half Result',
      'To Qualify': 'To Qualify'
    }
  },
  {
    SPORT_ID: 34,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result',
      'Current Set Winner',
      'Games Total',
      'Next Set Winner',
      'Next Game Winner',
      'Current Set Games Total',
      'Current Game Winner',
      'Games Handicap'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Current Set Winner': '1st Set Winner',
      'Games Total': 'Games Total',
      'Next Set Winner': 'Next Set Winner',
      'Next Game Winner': 'Next Game Winner',
      'Current Set Games Total': 'Current Set Games Total',
      'Current Game Winner': 'Current Game Winner',
      'Games Handicap': 'Games Handicap'
    }
  },
  {
    SPORT_ID: 6,
    DEFAULT_SELECTED_OPTION: 'Money Line',
    MARKETS_NAME_ORDER: [
      'Money Line', 'Total Points', 'Handicap', 'Home team total points', 'Away team total points',
      'Half Total Points', 'Quarter Total Points'
    ],
    MARKETS_NAMES: {
      'Money Line': 'Money Line',
      'Total Points': 'Total Points',
      'Handicap': 'Handicap',
      'Home team total points': 'Home Team Total Points',
      'Away team total points': 'Away Team Total Points',
      'Half Total Points': 'Current Half Total Points',
      'Quarter Total Points': 'Current Quarter Total Points'
    }
  },
  {
    SPORT_ID: 10,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result', 'Total Sixes', 'Next Over Runs (Main)', 'Team Runs (Main)', 'Runs At Fall Of Next Wicket'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Total Sixes': 'Total Sixes',
      'Next Over Runs (Main)': 'Next Over Runs',
      'Team Runs (Main)': 'Team Runs',
      'Runs At Fall Of Next Wicket': 'Runs at Fall of Next Wicket'
    }
  },
  {
    SPORT_ID: 13,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result', 'Most 180s', 'Leg Winner', 'Total 180s Over/Under', 'Leg Handicap', 'Match Handicap'

    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Most 180s': 'Most 180s',
      'Leg Winner': 'Next Leg Winner',
      'Total 180s Over/Under': 'Total 180s',
      'Leg Handicap': 'Handicap',
      'Match Handicap': 'Handicap'
    }
  },
  {
    SPORT_ID: 22,
    DEFAULT_SELECTED_OPTION: 'Money Line',
    MARKETS_NAME_ORDER: [
      'Money Line', '60 Minutes Betting', 'Puck Line', 'Total Goals 2-way'
    ],
    MARKETS_NAMES: {
      'Money Line': 'Money Line',
      '60 Minutes Betting': '60 Minutes Betting',
      'Puck Line': 'Puck Line',
      'Total Goals 2-way': 'Total Goals 2-way'
    }
  },
  {
    SPORT_ID: 18,
    DEFAULT_SELECTED_OPTION: '2 Ball Betting',
    MARKETS_NAME_ORDER: [
      '3 Ball Betting', '2 Ball Betting'
    ],
    MARKETS_NAMES: {
      '3 Ball Betting': '3 Ball Betting',
      '2 Ball Betting': '2 Ball Betting'
    }
  },
  {
    SPORT_ID: 1,
    DEFAULT_SELECTED_OPTION: 'Money Line',
    MARKETS_NAME_ORDER: [
      'Money Line', 'Handicap 2-way', '60 Minute Betting', 'Total Points'
    ],
    MARKETS_NAMES: {
      'Money Line': 'Money Line',
      'Handicap 2-way': 'Handicap',
      '60 Minute Betting': '60 Minute Betting',
      'Total Points': 'Total Points'
    }
  },
  {
    SPORT_ID: 9,
    DEFAULT_SELECTED_OPTION: 'Fight Betting',
    MARKETS_NAME_ORDER: [
      'Fight Betting'
    ],
    MARKETS_NAMES: {
      'Fight Betting': 'Fight Betting'
    }
  },
  {
    SPORT_ID: 32,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result', 'Match Handicap', 'Total Frames Over/Under', '1st Frame Winner'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Match Handicap': 'Handicap',
      'Total Frames Over/Under': 'Total Frames',
      '1st Frame Winner': 'Next Frame Winner'
    }
  },
  {
    SPORT_ID: 5,
    DEFAULT_SELECTED_OPTION: 'Money Line',
    MARKETS_NAME_ORDER: [
      'Money Line', 'Total Runs', 'Run Line', 'Home Total Runs', 'Away Total Runs'
    ],
    MARKETS_NAMES: {
      'Money Line': 'Money Line',
      'Total Runs': 'Total Runs',
      'Run Line': 'Run Line',
      'Home Total Runs': 'Home Total Runs',
      'Away Total Runs': 'Away Total Runs'
    }
  },
  {
    SPORT_ID: 31,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result', 'Handicap 2-way', 'Total Match Points'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Handicap 2-way': 'Handicap 2-way',
      'Total Match Points': 'Total Points'
    }
  },
  {
    SPORT_ID: 30,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result', 'Handicap 2-way', 'Total Match Points'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Handicap 2-way': 'Handicap 2-way',
      'Total Match Points': 'Total Points'
    }
  },
  {
    SPORT_ID: 36,
    DEFAULT_SELECTED_OPTION: 'Match Result',
    MARKETS_NAME_ORDER: [
      'Match Result', 'Set Handicap', 'Total Match Points', 'Set X Winner'
    ],
    MARKETS_NAMES: {
      'Match Result': 'Match Result',
      'Set Handicap': 'Set Handicap',
      'Total Match Points': 'Total Points',
      'Set X Winner': 'Current Set Winner'
    }
  }
];
