import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { LadbrokesMatchesMarketSelectorComponent } from './matchesMarketSelector/matches-market-selector.component';
import { LadbrokesInplayMarketSelectorComponent } from './inplayMarketSelector/inplay-market-selector.component';
import { MarketSelectorConfigService } from '@app/shared/components/marketSelector/market-selector-config.service';
import { SharedModule } from '@sharedModule/shared.module';

@NgModule({
    imports:[SharedModule],
    providers: [MarketSelectorConfigService],
    declarations: [LadbrokesMatchesMarketSelectorComponent,LadbrokesInplayMarketSelectorComponent],
    entryComponents: [
        LadbrokesMatchesMarketSelectorComponent,LadbrokesInplayMarketSelectorComponent
    ],
    exports:[LadbrokesMatchesMarketSelectorComponent,LadbrokesInplayMarketSelectorComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class MarketSelectorModule {
    static entry = LadbrokesMatchesMarketSelectorComponent;
}
