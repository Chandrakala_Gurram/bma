import { Component } from '@angular/core';
import { MatchesMarketSelectorComponent } from '@shared/components/marketSelector/matchesMarketSelector/matches-market-selector.component';

@Component({
  selector: 'matches-market-selector',
  styleUrls: ['./matches-market-selector.component.less'],
  templateUrl: './matches-market-selector.component.html'
})
export class LadbrokesMatchesMarketSelectorComponent extends  MatchesMarketSelectorComponent {
  setTitle(marketFilter: string): string {
    const activeOption = this.selectOptions.find((option) => option.templateMarketName === marketFilter);
    const title = activeOption ? activeOption.title : marketFilter;
    return title;
  }
}
