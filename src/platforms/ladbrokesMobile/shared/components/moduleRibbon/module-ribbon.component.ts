import { Component, OnInit } from '@angular/core';
import * as _ from 'underscore';

import { Router } from '@angular/router';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { UserService } from '@core/services/user/user.service';
import { ModuleRibbonComponent } from '@root/app/shared/components/moduleRibbon/module-ribbon.component';
import { ModuleRibbonService } from '@root/app/core/services/moduleRibbon/module-ribbon.service';
import { SessionService } from '@authModule/services/session/session.service';
import { Location } from '@angular/common';
import { GermanSupportService } from '@app/core/services/germanSupport/german-support.service';

  @Component({
    selector: 'module-ribbon',
    templateUrl: 'module-ribbon.component.html'
  })

  export class LadbrokesModuleRibbonComponent extends ModuleRibbonComponent implements OnInit {
    constructor(
      protected location: Location,
      protected ribbonService: ModuleRibbonService,
      protected user: UserService,
      protected pubSubService: PubSubService,
      protected router: Router,
      protected sessionService: SessionService,
      private germanSupportService: GermanSupportService
    ) {
      super(location, ribbonService, user, pubSubService, router, sessionService);
    }

    ngOnInit(): void {
      super.ngOnInit();

      if (!this.user.status) {
        this.filterNextRaces();
      }
    }

    protected addPrivateMarketTab(): void {
      if (this.user && this.user.status && !this.isOnPrivateMarketTab() && !this.privateMarketTabCreated) {
        this.privateMarketTabCreated = true;
        this.ribbonService.getPrivateMarketTab(_.clone(this.ribbon)).subscribe(result => {
          this.moduleList = [...result];
          if (!this.isRedirectNeeded()) {
            this.filterNextRaces();
          }
          this.setLocation();
        }, () => this.privateMarketTabCreated = false);
      }
    }

    private isRedirectNeeded() {
      return this.canRedirectToHomePage() || this.canRedirectToPrivateMarketsTab();
    }

    /**
     * Hide Next Races Tab on ribbon for German Users
     */
    private filterNextRaces(): void {
      this.moduleList = this.germanSupportService.toggleItemsList(this.moduleList, 'filterNextRaces');
    }
  }
