import { Component } from '@angular/core';
import { ToggleSwitchComponent } from '@shared/components/toggleSwitch/toggle-switch.component';

@Component({
  selector: 'toggle-switch',
  templateUrl: 'toggle-switch.component.html',
  styleUrls: ['./toggle-switch.component.less']

})
export class LadbrokesToggleSwitchComponent extends ToggleSwitchComponent {
}
