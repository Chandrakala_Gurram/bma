import { Component } from '@angular/core';
import { RaceCardComponent as OxygenRaceCardComponent } from '@app/shared/components/raceCard/race-card.component';

@Component({
  selector: 'race-card',
  templateUrl: './race-card.component.html',
  styleUrls: ['../../../../../app/shared/components/raceCard/next-races.less',
    '../../../../../app/shared/components/raceCard/race-card.component.less',
    './race-card.component.less']
})
export class RaceCardComponent extends OxygenRaceCardComponent {}
