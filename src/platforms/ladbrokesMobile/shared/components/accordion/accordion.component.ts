import { Component, ViewEncapsulation, HostBinding } from '@angular/core';

import { AccordionComponent } from '@shared/components/accordion/accordion.component';

@Component({
  selector: 'accordion',
  templateUrl: 'accordion.component.html',
  styleUrls: ['accordion.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class LadbrokesAccordionComponent extends AccordionComponent {

  @HostBinding('class.is-expanded') isExpanded: boolean;

  toggled(event: MouseEvent) {
    super.toggled(event);
    this.headerClasses = this.setHeaderClass();
  }

  /**
   * Set Header CSS Class
   * @returns { [key: string]: boolean }
   */
  setHeaderClass(): { [key: string]: boolean } {
    const classes = {
      'inner-header': this.isChevronToLeft || this.inner
    };
    if (this.headerClass) {
      classes[this.headerClass] = this.headerClass;
    }
    return classes;
  }
}

