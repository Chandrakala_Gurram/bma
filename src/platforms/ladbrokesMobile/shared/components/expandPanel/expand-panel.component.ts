import { Component } from '@angular/core';
import { ExpandPanelComponent as AppExpandPanelComponent } from '@app/shared/components/expandPanel/expand-panel.component';

@Component({
  selector: 'expand-panel',
  templateUrl: 'expand-panel.component.html',
  styleUrls: ['expand-panel.component.less']
})

export class ExpandPanelComponent extends AppExpandPanelComponent {}
