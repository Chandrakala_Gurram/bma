import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'star-rating',
  templateUrl: 'star-rating.component.html',
  styleUrls: ['star-rating.component.less']
})

export class StarRatingComponent implements OnInit {
  @Input() rating: number;
  startRating: boolean[];
  constructor() {}

  ngOnInit(): void {
    this.startRating = new Array(5).fill(false).map((item, index) => index < +this.rating);
  }

  trackByIndex(index: number): number {
    return index;
  }
}
