import { Component } from '@angular/core';
import { TabsPanelComponent } from '@shared/components/tabsPanel/tabs-panel.component';

@Component({
  selector: 'tabs-panel',
  styleUrls: ['tabs-panel.component.less'],
  templateUrl: 'tabs-panel.component.tpl.html'
})
export class LadbrokesTabsPanelComponent extends TabsPanelComponent {

}
