import { Component } from '@angular/core';
import { OxygenDialogComponent } from '@shared/components/oxygenDialogs/oxygen-dialog.component';

@Component({
  selector: 'oxygen-dialog',
  templateUrl: './oxygen-dialog.component.html',
  styleUrls: ['./oxygen-dialog.component.less']
})
export class LadbrokesOxygenDialogComponent extends OxygenDialogComponent {
}
