import { Component } from '@angular/core';
import { FreeBetLabelComponent as AppFreeBetLabelComponent } from '@app/shared/components/freeBetLabel/free-bet-label.component';

@Component({
  selector: 'free-bet-label',
  templateUrl: '../../../../../app/shared/components/freeBetLabel/free-bet-label.component.html',
  styleUrls: ['../../../../../app/shared/components/freeBetLabel/free-bet-label.less', './free-bet-label.less']
})

export class FreeBetLabelComponent extends AppFreeBetLabelComponent {}
