import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FooterMenuComponent } from '@shared/components/footerMenu/footer-menu.component';

@Component({
  selector: 'footer-menu',
  templateUrl: '../../../../../app/shared/components/footerMenu/footer-menu.component.html',
  styleUrls: ['footer-menu.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class LadbrokesFooterMenuComponent extends FooterMenuComponent {}
