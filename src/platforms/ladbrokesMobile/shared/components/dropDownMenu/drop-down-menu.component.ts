import { Component } from '@angular/core';

import { DropDownMenuComponent } from '@shared/components/dropDownMenu/drop-down-menu.component';

@Component({
  selector: 'drop-down-menu',
  templateUrl: './drop-down-menu.component.html',
  styleUrls: ['./drop-down-menu.component.less']
})
export class LadbrokesDropDownMenuComponent extends DropDownMenuComponent {}
