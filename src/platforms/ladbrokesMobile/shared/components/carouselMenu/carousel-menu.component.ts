import { Component, ChangeDetectionStrategy } from '@angular/core';

import { CarouselMenuComponent as AppCarouselMenuComponent } from '@shared/components/carouselMenu/carousel-menu.component';

@Component({
  selector: 'carousel-menu',
  styleUrls: ['./carousel-menu.component.less'],
  templateUrl: 'carousel-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarouselMenuComponent extends AppCarouselMenuComponent {}
