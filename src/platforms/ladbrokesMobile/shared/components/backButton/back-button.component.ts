import { Component, HostListener } from '@angular/core';
import { BackButtonService } from '@core/services/backButton/back-button.service';

@Component({
    selector: 'back-button',
    template: '<a data-crlat="btnBack" [i18n]="\'bma.back\'"></a>',
    styleUrls: ['back-button.component.less']
})

export class BackButtonComponent {
  constructor(
    private backButtonService: BackButtonService
  ) {}

  @HostListener('click') redirect() {
    this.backButtonService.redirectToPreviousPage();
  }
}
