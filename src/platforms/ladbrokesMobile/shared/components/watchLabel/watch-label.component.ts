import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

import { WatchLabelComponent } from '@shared/components/watchLabel/watch-label.component';

@Component({
  selector: 'watch-label',
  template: '<span [i18n]="\'app.watch\'" data-crlat="watchLive"></span>',
  styleUrls: ['./watch-label.component.less'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LadbrokesWatchLabelComponent extends WatchLabelComponent {}
