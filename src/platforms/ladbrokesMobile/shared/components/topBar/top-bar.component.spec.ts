import { TopBarComponent } from '@ladbrokesMobile/shared/components/topBar/top-bar.component';

describe('TopBarComponent', () => {
  let component: TopBarComponent;
  let localeService;
  let changeDetectorRef;
  let pubSubService;
  let domToolsService;
  let elementRef;

  beforeEach(() => {
    localeService = {};
    changeDetectorRef = {};
    pubSubService = {};
    domToolsService = {};
    elementRef = {};
    component = new TopBarComponent(localeService, changeDetectorRef, pubSubService, domToolsService, elementRef);
  });

  it('should create extended component', () => {
    expect(component).toBeDefined();
  });
});
