import { Component } from '@angular/core';
import { RaceTimerComponent as AppRaceTimerComponent } from '@shared/components/raceTimer/race-timer.component';

@Component({
  selector: 'race-timer',
  styleUrls: ['./race-timer.component.less'],
  templateUrl: '../../../../../app/shared/components/raceTimer/race-timer.component.html'
})
export class RaceTimerComponent extends AppRaceTimerComponent {}
