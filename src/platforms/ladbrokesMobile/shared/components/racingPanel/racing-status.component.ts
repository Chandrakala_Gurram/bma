import { Component } from '@angular/core';
import {
  RacingStatusComponent as CoralRacingStatusComponent
} from '@shared/components/racingPanel/racing-status.component';

@Component({
  selector: 'racing-status',
  templateUrl: 'racing-status.component.html',
  styleUrls: ['./racing-status.component.less']
})
export class RacingStatusComponent extends CoralRacingStatusComponent {}

