import { RacingPanelComponent } from './racing-panel.component';
import {
  RacingPanelComponent as CoralRacingPanelComponent
} from '@shared/components/racingPanel/racing-panel.component';

describe('RacingPanelComponent', () => {
  let component: RacingPanelComponent;
  let localeService;
  let router;
  let routingHelperService;

  beforeEach(() => {
    localeService = {
      getString: jasmine.createSpy().and.returnValue('Test string')
    };
    router = {
      navigate: jasmine.createSpy('navigate')
    };
    routingHelperService = {
      formEdpUrl: jasmine.createSpy().and.returnValue('EDPpath')
    };
    component = new RacingPanelComponent(
      localeService,
      router,
      routingHelperService
    );
  });

  describe('instance', () => {
    it('should be created', () => {
      expect(component).toBeTruthy();
    });

    it('should extend CoralRacingPanelComponent', () => {
      expect(component instanceof CoralRacingPanelComponent).toBeTruthy();
    });
  });
});
