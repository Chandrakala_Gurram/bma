import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from '@bma/components/home/home.component';
import { LadbrokesAzSportsPageComponent } from '@ladbrokesMobile/shared/components/azSportPage/az-sports-page.component';
import { LoggedInGuard } from '@core/guards/logged-in-guard.service';
import { BuildYourBetHomeComponent } from '@sb/components/buildYourBetHome/build-your-bet-home.component';
import { EnhancedMultiplesTabComponent } from '@sbModule/components/enhancedMultiplesModule/enhanced-multiples-tab.component';
import { PrivateMarketsTabComponent } from '@ladbrokesMobile/sb/components/privateMarketsTab/private-markets-tab.component';
import { NotFoundComponent } from '@bma/components/404/404.component';
import { MaintenanceComponent } from '@shared/components/maintenance/maintenance.component';
import { InplayHomeTabComponent } from '@bma/components/inlayHomeTab/inplay-home-tab.component';
import { IRouteData } from '@app/core/models/route-data.model';
import { ILadbrokesRetailConfig } from '@ladbrokesMobile/core/services/cms/models/system-config';
import { FeaturedTabGuard } from '@core/guards/featured-tab-guard.service';
import { PrivateMarketsGuard } from '@core/guards/private-markets-guard.service';
import { ContactUsComponent } from '@bma/components/contactUs/contact-us.component';
import { SignUpRouteGuard } from '@core/guards/sign-up-guard.service';
import { LogoutResolver } from '@app/vanillaInit/services/logout/logout.service';

import { GermanSupportGuard } from '@app/core/guards/german-support-guard.service';
import { StaticComponent } from '@bma/components/static/static.component';
import { LazyRouteGuard } from '@core/guards/lazy-route-guard.service';
import { EventhubTabGuard } from '@core/guards/eventhub-tab-guard.service';
import { RootComponent } from '@ladbrokesMobile/app.component';
import { LABELHOST_FEATURES_ROUTES, routeData } from '@labelhost/core/features';
import { ProductSwitchResolver } from '@app/host-app/product-switch.resolver';
import { SiteRootGuard } from '@vanilla/core/core';
import { VANILLA_ROUTES } from '@vanilla/core/features';
import { ROOT_APP_ROUTES } from '@app/vanillaInit/root-routes.definitions';
import { CustomPreloadingStrategy } from '@app/vanillaInit/custom.preload.strategy';
import { routes as SbRoutes } from '@ladbrokesMobile/sb/sb-routing.module';
import { routes as FavouritesRoutes } from '@app/favourites/favourites-routing.module';
import { MaintenanceGuard } from '@core/guards/maintenance-guard.service';
import { LiveStreamWrapperComponent } from '@bma/components/liveStream/live-stream-wrapper.component';
import { DepositRedirectGuard } from '@coreModule/guards/deposit-redirect.guard';
import { NotFoundPageGuard } from '@core/guards/not-found-page-guard.service';
import { IRetailConfig } from '@core/services/cms/models/system-config';

const appRoutes: Routes = [
  {
    path: '',
    component: RootComponent,
    data: {
      ...routeData({ allowAnonymous: true }),
      product: 'host',
      segment: 'main'
    },
    runGuardsAndResolvers: 'always',
    resolve: {
      __p: ProductSwitchResolver
    },
    canActivate: [SiteRootGuard],
    children: [
      {
        path: 'en',
        data: {
          segment: 'vanilla'
        },
        children: [
          ...VANILLA_ROUTES,
          ...LABELHOST_FEATURES_ROUTES,
          ...ROOT_APP_ROUTES,
          {
            path: '**',
            redirectTo: '/'
          }
        ]
      },
      ...SbRoutes,
      ...FavouritesRoutes,
      {
        path: '',
        component: HomeComponent,
        data: {
          segment: 'home'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            loadChildren: '@featuredModule/featured.module#FeaturedModule',
            canActivate: [FeaturedTabGuard]
          }
        ]
      },
      {
        path: 'home',
        component: HomeComponent,
        data: {
          segment: 'home'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'featured'
          },
          {
            path: 'featured',
            loadChildren: '@featuredModule/featured.module#FeaturedModule',
            data: {
              segment: 'featured'
            }
          },
          {
            path: 'eventhub/:hubIndex',
            loadChildren: 'platforms/ladbrokesMobile/featured/featured.module#FeaturedModule',
            canActivate: [EventhubTabGuard],
            data: {
              segment: 'featured'
            }
          },
          {
            path: 'betbuilder',
            component: BuildYourBetHomeComponent,
            data: {
              segment: 'buildYourBet'
            }
          },
          {
            path: 'coupons',
            loadChildren: '@lazy-modules/couponsListHomeTab/coupons-list-home-tab.module#LazyCouponsListHomeTabModule'
          },
          {
            path: 'in-play',
            component: InplayHomeTabComponent,
            data: {
              segment: 'inPlay'
            }
          }, {
            path: 'byb-module',
            loadChildren: '@bybHistoryModule/byb-history.module#LazyBybHistoryModule',
            canActivate: [ LazyRouteGuard ]
          }, {
            path: 'multiples',
            component: EnhancedMultiplesTabComponent,
            data: {
              segment: 'multiples'
            }
          },
          {
            path: 'live-stream',
            component: LiveStreamWrapperComponent,
            data: {
              segment: 'liveStream'
            }
          },
          {
            path: 'private-markets',
            component: PrivateMarketsTabComponent,
            canActivate: [LoggedInGuard, PrivateMarketsGuard],
            data: {
              segment: 'privateMarkets'
            }
          },
          {
            path: 'next-races',
            canActivate: [GermanSupportGuard],
            data: {
              segment: 'nextRaces'
            },
            loadChildren: '@ladbrokesMobile/lazy-modules/lazyNextRacesTab/lazyNextRacesTab.module#LazyNextRacesTabModule'
          }
        ]
      }, {
        path: 'live-stream',
        component: LiveStreamWrapperComponent,
        data: {
          segment: 'liveStream'
        }
      }, {
        path: 'az-sports',
        component: LadbrokesAzSportsPageComponent,
        data: {
          segment: 'azSports'
        }
      }, {
        path: 'logout',
        component: HomeComponent,
        resolve: {
          logout: LogoutResolver
        },
        data: {
          segment: 'logout'
        }
      }, {
        path: 'settings',
        loadChildren: '@ladbrokesMobile/bma/components/userSettings/user-settings.module#LadbrokesUserSettingsModule',
        canActivate: [LoggedInGuard],
        runGuardsAndResolvers: 'always',
        data: {
          segment: 'settings'
        }
      }, {
        path: 'contact-us',
        component: ContactUsComponent,
        data: {
          segment: 'contactUs'
        }
      }, {
        path: 'static/:static-block',
        component: StaticComponent,
        data: {
          segment: 'static'
        }
      }, {
        path: 'freebets',
        loadChildren: '@freebetsModule/freebets.module#FreebetsModule',
        canActivate: [LoggedInGuard],
        data: {
          segment: 'freebets'
        }
      }, {
        path: '404',
        component: NotFoundComponent,
        data: {
          segment: '404'
        }
      }, {
        path: 'under-maintenance',
        component: MaintenanceComponent,
        resolve: { data: MaintenanceGuard },
        canDeactivate: [MaintenanceGuard]
      }, {
        path: 'deposit',
        component: HomeComponent,
        canActivate: [DepositRedirectGuard]
      },
      // Modules lazy loaded by routes
      {
        path: 'tote-information',
        canActivate: [GermanSupportGuard],
        redirectTo: 'tote/information'
      }, {
        path: 'tote',
        canActivate: [GermanSupportGuard],
        loadChildren: '@ladbrokesMobile/tote/tote.module#ToteModule'
      }, {
        path: 'bet-finder',
        canActivate: [GermanSupportGuard],
        loadChildren: '@betFinderModule/betfinder.module#BetFinderModule',
        data: {
          feature: 'raceBetFinder'
        } as IRouteData<ILadbrokesRetailConfig>
      }, {
        path: 'virtual-sports',
        loadChildren: '@ladbrokesMobile/vsbr/vsbr.module#VsbrModule'
      }, {
        path: 'in-play',
        loadChildren: '@inplayModule/inplay.module#InplayModule',
        data: {
          preload: true
        }
      }, {
        path: 'byb-module',
        loadChildren: '@bybHistoryModule/byb-history.module#LazyBybHistoryModule',
        canActivate: [ LazyRouteGuard ]
      }, {
        path: 'betslip/add/:outcomeId',
        loadChildren: '@betslipModule/betslip.module#BetslipModule'
      }, {
        path: 'betslip/unavailable',
        loadChildren: '@betslipModule/betslip.module#BetslipModule'
      },

      // Odds Boost module lazy loading
      {
        path: 'oddsboost',
        loadChildren: '@oddsBoostModule/odds-boost.module#OddsBoostModule'
      },
      {
        path: 'competitions/:sport/:className/:typeName',
        data: {
          segment: 'competitionTypeEvents'
        },
        loadChildren: '@lazy-modules-module/competitionsSportTab/competitionsSportTab.module#CompetitionsTabModule'
      },
      {
        path: 'signup',
        canActivate: [ SignUpRouteGuard ],
        children: []
      }, {
        path: 'cashout',
        loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule'
      }, {
        path: 'open-bets',
        loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule'
      }, {
        path: 'bet-history',
        loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule'
      }, {
        path: 'in-shop-bets',
        loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule'
      }, {
        path: 'lotto-results',
        canActivate: [GermanSupportGuard],
        redirectTo: 'lotto/results'
      }, {
        path: 'lotto',
        canActivate: [GermanSupportGuard],
        loadChildren: '@lottoModule/lotto.module#LottoModule'
      }, {
        path: 'big-competition',
        loadChildren: 'app/bigCompetitions/big-competitions.module#BigCompetitionsModule'
      },
      // Retail module lazy loading
      {
        path: 'retail',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'menu'
        } as IRouteData<IRetailConfig>
      },
      {
        path: 'retail-upgrade',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'upgrade'
        } as IRouteData<IRetailConfig>
      },
      {
        path: 'bet-filter',
        loadChildren: '@ladbrokesMobile/retail/retail.module#RetailModule',
        data: {
          feature: 'footballFilter'
        } as IRouteData<ILadbrokesRetailConfig>
      },
      {
        path: 'bet-filter/:child',
        loadChildren: '@ladbrokesMobile/retail/retail.module#RetailModule',
        data: {
          feature: 'footballFilter'
        } as IRouteData<ILadbrokesRetailConfig>
      },
      {
        path: 'bet-filter/:child/:child',
        loadChildren: '@ladbrokesMobile/retail/retail.module#RetailModule',
        data: {
          feature: 'footballFilter'
        } as IRouteData<ILadbrokesRetailConfig>
      },
      {
        path: 'bet-filter',
        loadChildren: '@ladbrokesMobile/retail/retail.module#RetailModule',
        data: {
          feature: 'digitalCoupons'
        } as IRouteData<ILadbrokesRetailConfig>
      },
      {
        path: 'bet-tracker',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'shopBetTracker'
        } as IRouteData<IRetailConfig>
      },
      {
        path: 'shop-locator',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'shopLocator'
        } as IRouteData<IRetailConfig>
      },
      // Racing lazy loading
      {
        path: 'horse-racing',
        loadChildren: '@racingModule/horseracing.module#HorseracingModule',
        canActivate: [GermanSupportGuard]
      },
      {
        path: 'greyhound-racing',
        loadChildren: '@racingModule/greyhound.module#GreyhoundModule',
        canActivate: [GermanSupportGuard]
      },

      // EDP
      {
        path: 'edp-lazy-load',
        loadChildren: '@edpModule/edp.module#EdpModule',
        canActivate: [ LazyRouteGuard ]
      },
      // Quickbet
      {
        path: 'quickbet-lazy-load',
        loadChildren: '@quickbetModule/quickbet.module#QuickbetModule',
        canActivate: [ LazyRouteGuard ]
      },
      // Yourcall
      {
        path: 'yourcall-lazy-load',
        loadChildren: '@yourCallModule/your-call.module#YourCallModule',
        canActivate: [ LazyRouteGuard ]
      },
      // Quick Deposit
      {
        path: 'quick-deposit-lazy-load',
        loadChildren: '@quickDepositModule/quick-deposit.module#QuickDepositModule',
        canActivate: [ LazyRouteGuard ]
      },

      // Sport (SB) lazy loading
      {
        path: 'sport',
        loadChildren: '@sbModule/sport/sport.module#LazySportModule'
      },

      // Event (SB) lazy loading
      {
        path: 'event',
        loadChildren: '@sb/event/event.module#LazyEventModule'
      },

      // Coupons lazy loading
      {
        path: 'coupons',
        loadChildren: '@lazy-modules/couponsPage/coupons-page.module#LazyCouponsPageModule'
      },

      // Olympics lazy loading
      {
        path: 'olympics',
        loadChildren: '@olympicsModule/olympics.module#OlympicsModule'
      },
      // Bet radar lazy loading
      {
        path: 'bet-radar-lazy-load',
        loadChildren: '@lazy-modules-module/betRadarProvider/bet-radar.module#BetRadarLadbrokesModule',
        canActivate: [ LazyRouteGuard ]
      },
      // Receipt header lazy loading
      {
        path: 'receipt-header-lazy-load',
        loadChildren: '@lazy-modules-module/receiptHeader/receipt-header.module#ReceiptHeaderModule',
        canActivate: [ LazyRouteGuard ]
      },
      // Runner-spotlight lazy loading
      {
        path: 'runner-spotlight-lazy-load',
        loadChildren: '@lazy-modules-module/runnerSpotlight/runner-spotlight.module#RunnerSpotlightModule',
        canActivate: [LazyRouteGuard]
      },
      // Market Selector
      {
        path:'market-selector',
        loadChildren:'@sharedModule/components/marketSelector/market-selector.module#MarketSelectorModule',
        canActivate: [LazyRouteGuard]
      },
      {
        path: 'forecast-tricast-lazy-load',
        loadChildren: '@lazy-modules-module/forecastTricast/forecastTricast.module#ForecastTricastModule',
        canActivate: [ LazyRouteGuard ]
      },
      {
        path: 'event-video-stream-lazy-load',
        loadChildren: '@lazy-modules-module/eventVideoStream/event-video-stream.module#LazyEventVideoStreamModule',
        canActivate: [ LazyRouteGuard ]
      },
      // Freebets lazy loading
      {
        path: 'freebets-lazy-load',
        loadChildren: '@freebetsModule/freebets.module#FreebetsModule',
        canActivate: [ LazyRouteGuard ]
      },
      {
        path: 'promotions',
        loadChildren: '@promotionsModule/promotions.module#PromotionsModule',
        data: {
          path: 'promotions/retail',
          feature: 'promotions'
        }
      },
      {
        path: '1-2-free',
        pathMatch: 'full',
        loadChildren: 'app/oneTwoFree/one-two-free.module#OneTwoFreeModule',
        data: {
          segment: '1-2-free'
        }
      },
      {
        path: 'qe/:sourceId',
        loadChildren: '@ladbrokesMobile/questionEngine/question-engine.module#QuestionEngineModule',
        data: {
          segment: 'question-engine'
        }
      },
      // Timeline
      {
        path: 'timeline-lazy-load',
        loadChildren: '@lazy-modules/timeline/timeline.module#TimelineModule',
        canActivate: [ LazyRouteGuard ]
      },
      {
        path: '**',
        component: NotFoundComponent,
        canActivate: [ NotFoundPageGuard ]
      }
    ]
  }
];


@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        useHash: false,
        paramsInheritanceStrategy: 'always',
        onSameUrlNavigation: 'reload',
        preloadingStrategy: CustomPreloadingStrategy
        // enableTracing: true  // <-- debugging purposes only
      }
    )
  ],
  exports: [
    RouterModule
  ],
  declarations: [],
  providers: [
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    CustomPreloadingStrategy
  ]
})
export class AppRoutingModule { }
