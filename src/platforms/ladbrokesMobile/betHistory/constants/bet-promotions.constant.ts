export const BET_PROMO_CONFIG = [{
  name: 'boosted',
  label: 'bs.boostedMsg',
  svgId: 'odds-boost-icon-dark'
}, {
  name: 'money-back',
  label: 'bs.moneyBackMsg',
  svgId: 'icon-money-back'
}, {
  name: 'acca-insurance',
  label: 'bs.accaInsurance',
  svgId: 'icon-acca-insurance'
}];
