import { Component } from '@angular/core';
import { BetHistoryPageComponent } from '@app/betHistory/components/betHistoryPage/bet-history-page.component';

@Component({
  selector: 'bet-history-page',
  templateUrl: 'bet-history-page.component.html',
  styleUrls: ['./bet-history-page.less', '../../../../../app/betHistory/components/betHistoryPage/bet-history-error-template.less']
})
export class LadbrokesBetHistoryPageComponent extends BetHistoryPageComponent {
  createFilters(): void {
    super.createFilters();
    // remove Player bets on ladbrokes
    this.betTypes = this.betTypes.filter((type) => type.viewByFilters !== 'digitalSportBet');
  }
}
