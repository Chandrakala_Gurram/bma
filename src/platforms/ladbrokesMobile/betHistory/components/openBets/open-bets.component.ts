import { Component, OnInit } from '@angular/core';
import { OpenBetsComponent as BaseOpenBetsComponent } from '@app/betHistory/components/openBets/open-bets.component';

@Component({
  selector: 'open-bets',
  templateUrl: 'open-bets.component.html'
})
export class OpenBetsComponent extends BaseOpenBetsComponent implements OnInit {
  // remove Player bets on ladbrokes
  protected readonly TYPES: string[] = ['regularType', 'lottoType', 'poolType'];
  ngOnInit(): void {
    super.ngOnInit();
    // remove Player bets on ladbrokes
    this.betTypes = this.betTypes.filter((type) => type.viewByFilters !== 'digitalSportBet');
  }
}
