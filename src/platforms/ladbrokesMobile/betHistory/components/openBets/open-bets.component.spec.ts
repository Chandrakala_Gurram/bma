import { of as observableOf } from 'rxjs';
import { OpenBetsComponent } from '@ladbrokesMobile/betHistory/components/openBets/open-bets.component';

describe('OpenBetsComponent', () => {
  let component: OpenBetsComponent;
  let sessionServiceStub;
  let liveServConnectionServiceStub;
  let pubSubServiceStub;
  let localeServiceStub;
  let maintenanceServiceStub;
  let betHistoryMainServiceStub;
  let userServiceStub;
  let betsLazyLoadingServiceStub;
  let cashoutBetsStreamServiceStub;
  let editMyAccaService;

  beforeEach(() => {
    sessionServiceStub = {
      whenProxySession: jasmine.createSpy().and.returnValue(
        Promise.resolve()
      )
    };

    liveServConnectionServiceStub = {
      connect: jasmine.createSpy().and.returnValue(observableOf({}))
    };

    pubSubServiceStub = {
      subscribe: jasmine.createSpy(),
      publish: jasmine.createSpy('publish'),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: {
        RELOAD_COMPONENTS: 'reload'
      }
    };

    localeServiceStub = {
      getString: jasmine.createSpy().and.returnValue('locale')
    };

    maintenanceServiceStub = {
      siteServerHealthCheck: jasmine.createSpy().and.returnValue(observableOf('health status'))
    };

    betHistoryMainServiceStub = {
      getBetStatus: jasmine.createSpy().and.returnValue('open'),
      getHistoryForYear: jasmine.createSpy('getHistoryForYear').and.returnValue(observableOf({ bets: [{}] } as any)),
      getEditMyAccaHistory: jasmine.createSpy('getEditMyAccaHistory').and.returnValue(observableOf({ bets: [{}] } as any)),
      makeSafeCall: jasmine.createSpy().and.callFake(x => x),
      buildSwitchers: jasmine.createSpy().and.returnValue([{}]),
      getSwitcher: jasmine.createSpy(),
      extendCashoutBets: jasmine.createSpy('extendCashoutBets')
    };

    userServiceStub = {
      status: true,
      isInShopUser: jasmine.createSpy().and.returnValue(false)
    };

    betsLazyLoadingServiceStub = {
      reset: jasmine.createSpy(),
      initialize: jasmine.createSpy()
    };

    cashoutBetsStreamServiceStub = {
      openBetsStream: jasmine.createSpy().and.returnValue(observableOf([])),
      closeBetsStream: jasmine.createSpy()
    };

    editMyAccaService = {
      isUnsavedInWidget: jasmine.createSpy('isUnsavedInWidget'),
      showEditCancelMessage: jasmine.createSpy('isUnsavedInWidget'),
    };

    component = new OpenBetsComponent(
      cashoutBetsStreamServiceStub,
      sessionServiceStub,
      liveServConnectionServiceStub,
      pubSubServiceStub,
      localeServiceStub,
      maintenanceServiceStub,
      betHistoryMainServiceStub,
      userServiceStub,
      betsLazyLoadingServiceStub,
      editMyAccaService
    );
  });

  describe('ngOnInit', () => {
    it('should remove digitalSportBet tab', () => {
      component.ngOnInit();
      const digitalSportBet = component['betTypes'].filter((type) => type.viewByFilters === 'digitalSportBet');
      expect(component['betTypes']).toBeDefined();
      expect(digitalSportBet.length).toBe(0);
    });
  });
});
