import { Component } from '@angular/core';
import {
  BetHistoryPromptComponent as CoralBetHistoryPromptComponent
} from '@app/betHistory/components/betHistoryPrompt/bet-history-prompt.component';

@Component({
  selector: 'bet-history-prompt',
  templateUrl: '../../../../../app/betHistory/components/betHistoryPrompt/bet-history-prompt.component.html',
  styleUrls: ['../../../../../app/betHistory/components/betHistoryPrompt/bet-history-prompt.component.less',
    './bet-history-prompt.component.less']
})
export class BetHistoryPromptComponent extends CoralBetHistoryPromptComponent {}
