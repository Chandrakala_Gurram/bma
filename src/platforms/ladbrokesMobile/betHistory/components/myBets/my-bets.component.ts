import { Component } from '@angular/core';
import { MyBetsComponent as AppMyBetsComponent } from '@app/betHistory/components/myBets/my-bets.component';

@Component({
  selector: 'my-bets',
  templateUrl: '../cashOutBets/cash-out-bets.component.html',
  styleUrls: ['../../../../../app/betHistory/components/myBets/my-bets.component.less']
})
export class MyBetsComponent extends AppMyBetsComponent {
}
