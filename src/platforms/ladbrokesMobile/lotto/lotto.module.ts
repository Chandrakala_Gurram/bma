import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'angular-custom-modal';

import { SharedModule } from '@sharedModule/shared.module';

import { SimpleFiltersService } from '@ss/services/simple-filters.service';
import { LottoRoutingModule } from '@app/lotto/lotto-routing.module';
import { LottoResultsComponent } from '@app/lotto/components/lottoResults/lotto-results.component';
import { LottoMainComponent } from '@app/lotto/components/lottoMain/lotto-main.component';
import { LottoReceiptComponent } from '@app/lotto/components/lottoReceipt/lotto-receipt.component';
import { LottoResultsPageComponent } from '@app/lotto/components/lottoResultsPage/lotto-results-page.component';
import { LottoResultsFilterPageComponent } from '@app/lotto/components/lottoResulsFilterPage/lotto-results-filter-page.component';
import { LottoSegmentPageComponent } from '@app/lotto/components/lottoSegmentPage/lotto-segment-page.component';
import { NumberSelectorComponent } from '@app/lotto/components/numberSelector/number-selector.component';
import { LottoNumberSelectorComponent } from '@lottoModule/components/lottoNumberSelectorDialog/lotto-number-selector-dialog.component';
import { SiteServerLottoService } from '@app/lotto/services/siteServerLotto/site-server-lotto.service';
import { BuildLotteriesService } from '@app/lotto/services/buildLotteries/build-lotteries.service';
import { LottoReceiptService } from '@app/lotto/services/lottoReceipt/lotto-receipt.service';
import { LottoBetService } from '@app/lotto/services/lottoBet/lotto-bet.service';
import { LottoResultsService } from '@app/lotto/services/lottoResults/lotto-results.service';
import { MainLottoService } from '@app/lotto/services/mainLotto/main-lotto.service';
import { SegmentDataUpdateService } from '@app/lotto/services/segmentDataUpdate/segment-data-update.service';
import { BannersModule } from '@banners/banners.module';

@NgModule({
  declarations: [
    LottoMainComponent,
    LottoResultsComponent,
    LottoReceiptComponent,
    LottoResultsPageComponent,
    LottoResultsFilterPageComponent,
    LottoSegmentPageComponent,
    NumberSelectorComponent,
    LottoNumberSelectorComponent
  ],
  imports: [
    ModalModule,
    SharedModule,
    FormsModule,
    LottoRoutingModule,
    BannersModule
  ],
  exports: [
    LottoMainComponent,
    LottoResultsComponent,
    LottoReceiptComponent,
    LottoResultsPageComponent,
    LottoResultsFilterPageComponent,
    LottoSegmentPageComponent,
    NumberSelectorComponent,
    LottoNumberSelectorComponent
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  entryComponents: [
    LottoMainComponent,
    LottoResultsComponent,
    LottoReceiptComponent,
    LottoResultsPageComponent,
    LottoResultsFilterPageComponent,
    LottoSegmentPageComponent,
    NumberSelectorComponent,
    LottoNumberSelectorComponent
  ],
  providers: [
    BuildLotteriesService,
    SiteServerLottoService,
    MainLottoService,
    LottoReceiptService,
    LottoBetService,
    LottoResultsService,
    SegmentDataUpdateService,
    SimpleFiltersService
  ],
})
export class LottoModule {}
