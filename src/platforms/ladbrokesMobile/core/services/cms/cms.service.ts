import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

import { CmsService as AppCmsService } from '@core/services/cms/cms.service';

import { PubSubService } from '@coreModule/services/communication/pubsub/pubsub.service';
import { CmsToolsService } from '@coreModule/services/cms/cms.tools';
import { DeviceService } from '@coreModule/services/device/device.service';
import { StorageService } from '@coreModule/services/storage/storage.service';
import { CoreToolsService } from '@coreModule/services/coreTools/core-tools.service';
import { NativeBridgeService } from '@coreModule/services/nativeBridge/native-bridge.service';
import { IInitialData } from '@coreModule/services/cms/models';
import { UserService } from '@coreModule/services/user/user.service';

import { CasinoLinkService } from '@coreModule/services/casinoLink/casino-link.service';

@Injectable()
export class CmsService extends AppCmsService {
  constructor(
    protected pubSubService: PubSubService,
    protected cmsToolsService: CmsToolsService,
    protected deviceService: DeviceService,
    protected httpClient: HttpClient,
    protected coreToolsService: CoreToolsService,
    protected storageService: StorageService,
    protected casinoLinkService: CasinoLinkService,
    protected nativeBridgeService: NativeBridgeService,
    protected userService: UserService,
    @Inject('CMS_CONFIG') protected cmsInitConfig: Promise<IInitialData>
  ) {
    super(
      pubSubService,
      cmsToolsService,
      deviceService,
      httpClient,
      coreToolsService,
      storageService,
      casinoLinkService,
      nativeBridgeService,
      userService,
      cmsInitConfig
    );
  }
}
