import { InplaySingleSportPageComponent } from './inplay-single-sport-page.component';

describe('LMInplaySingleSportPageComponent', () => {
  let component: InplaySingleSportPageComponent,
    inplayMainService,
    inplaySubscriptionManagerService,
    pubsubService,
    inPlayConnectionService,
    route,
    cms,
    router,
    deviceService,
    newRelicService,
    changeDetectorRef;

  beforeEach(() => {
    changeDetectorRef = {
      markForCheck: jasmine.createSpy('markForCheck')
    };
    route = {};
    router = {};
    deviceService = {};
    inPlayConnectionService = {};
    inplayMainService = {
      isNewUserFromOtherCountry: jasmine.createSpy().and.returnValue(true),
    };
    cms = {};
    pubsubService = {
      subscribe: jasmine.createSpy('subscribe').and.callFake((arg: string, p: string, fn: Function) => {
        if (p === 'SESSION_LOGIN') {
          fn();
        }
      }),
      unsubscribe: jasmine.createSpy('subscribe'),
      API: {
        SESSION_LOGIN: 'SESSION_LOGIN'
      }
    };
    inplaySubscriptionManagerService = {};
    newRelicService = {
      addPageAction: jasmine.createSpy()
    };

    component = new InplaySingleSportPageComponent(
      inplayMainService,
      inplaySubscriptionManagerService,
      pubsubService,
      inPlayConnectionService,
      route,
      cms,
      router,
      deviceService,
      newRelicService,
      changeDetectorRef
    );
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  it('should use OnPush strategy', () => {
    expect(InplaySingleSportPageComponent['__annotations__'][0].changeDetection).toBe(0);
  });

  describe('@showContent', () => {
    it('should subscribe and reloadComponent on event', () => {
      spyOn(component, 'reloadComponent');

      component['showContent']();
      expect(pubsubService.subscribe).toHaveBeenCalledWith('inplaySingleSportPage', 'SESSION_LOGIN', jasmine.any(Function));
      expect(component.reloadComponent).toHaveBeenCalled();
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
      expect(component.state.loading).toBe(false);
    });

    it('should not reloadComponent on event', () => {
      spyOn(component, 'reloadComponent');
      inplayMainService.isNewUserFromOtherCountry.and.returnValue(false);

      component['showContent']();
      expect(pubsubService.subscribe).toHaveBeenCalled();
      expect(component.reloadComponent).not.toHaveBeenCalled();
      expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    });
  });

});
