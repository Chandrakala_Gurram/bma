import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { InplaySingleSportPageComponent
    as AppInplaySingleSportPageComponent } from '@app/inPlay/components/inplaySingleSportPage/inplay-single-sport-page.component';
import { InplayMainService } from '@ladbrokesMobile/inPlay/services/inplayMain/inplay-main.service';
import { InplaySubscriptionManagerService } from '@app/inPlay/services/InplaySubscriptionManager/inplay-subscription-manager.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { InplayConnectionService } from '@app/inPlay/services/inplayConnection/inplay-connection.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { DeviceService } from '@core/services/device/device.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NewRelicService } from '@core/services/newRelic/new-relic.service';

@Component({
  selector: 'inplay-single-sport-page',
  templateUrl: '../../../../../app/inPlay/components/inplaySingleSportPage/inplay-single-sport-page.component.html',
  styleUrls: ['../../../../../app/inPlay/components/inplaySingleSportPage/inplay-single-sport-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class InplaySingleSportPageComponent extends AppInplaySingleSportPageComponent {
  constructor(
    protected inplayMainService: InplayMainService,
    inplaySubscriptionManagerService: InplaySubscriptionManagerService,
    pubsubService: PubSubService,
    inPlayConnectionService: InplayConnectionService,
    route: ActivatedRoute,
    cms: CmsService,
    router: Router,
    deviceService: DeviceService,
    newRelicService: NewRelicService,
    changeDetectorRef: ChangeDetectorRef
  ) {
    super(
      inplayMainService,
      inplaySubscriptionManagerService,
      pubsubService,
      inPlayConnectionService,
      route,
      cms,
      router,
      deviceService,
      newRelicService,
      changeDetectorRef
    );
  }

  protected showContent(): void {
    /* tslint:disable-next-line:pubsub-connect */
    this.pubsubService.subscribe(this.cSyncName, this.pubsubService.API.SESSION_LOGIN, () => {
      if (this.inplayMainService.isNewUserFromOtherCountry()) {
        this.reloadComponent();
        this.changeDetectorRef.markForCheck();
      }
    });
    super.showContent();
    this.changeDetectorRef.markForCheck();
  }
}
