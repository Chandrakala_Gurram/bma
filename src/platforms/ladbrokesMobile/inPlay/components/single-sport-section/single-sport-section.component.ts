import { Component, ChangeDetectionStrategy } from '@angular/core';
import { SingleSportSectionComponent } from '@app/inPlay/components/singleSportSection/single-sport-section.component';

@Component({
  selector: 'single-sport-section',
  templateUrl: 'single-sport-section.component.html',
  styleUrls: [
    '../../../../../app/inPlay/components/singleSportSection/single-sport-section.component.less',
    './single-sport-section.component.less'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LadbrokesSingleSportSectionComponent extends SingleSportSectionComponent { }

