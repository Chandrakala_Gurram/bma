import { Component } from '@angular/core';

import { OddsBoostButtonComponent } from '@oddsBoost/components/oddsBoostButton/odds-boost-button.component';

@Component({
  selector: 'odds-boost-button',
  templateUrl: './odds-boost-button.component.html',
  styleUrls: ['./odds-boost-button.component.less']
})
export class LadbrokesOddsBoostButtonComponent extends OddsBoostButtonComponent {

}
