import { async } from '@angular/core/testing';
import { RootComponent } from './app.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('LMAppComponent', () => {
  let component: RootComponent;
  let vanillaPortalModuleLoadNotifier;
  let appRef;
  let ngZone;
  let pubSubService;
  let router;

  beforeEach(async(() => {
    router = {
      events: {
        subscribe: jasmine.createSpy('subscribe')
      }
    };

    pubSubService = {
      subscribe: jasmine.createSpy('subscribe'),
      API: pubSubApi
    };
    vanillaPortalModuleLoadNotifier = {
      loadPortalModule: jasmine.createSpy()
    };
    appRef = {};
    ngZone = {};

    component = new RootComponent(
      vanillaPortalModuleLoadNotifier,
      appRef,
      ngZone,
      pubSubService,
      router
    );
  }));

  it('should call loadPortalModule', () => {
    component.ngAfterViewInit();
    expect(vanillaPortalModuleLoadNotifier.loadPortalModule).toHaveBeenCalled();
  });
});
