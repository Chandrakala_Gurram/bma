import { Injectable } from '@angular/core';
import { VanillaFreebetsBadgeDynamicLoaderService as AppVanillaFreebetsBadgeDynamicLoaderService
} from '@app/vanillaInit/services/vanillaFreeBets/vanilla-fb-badges-loader.service';
import { MenuCountersService, MenuSection } from '@vanilla/core';
import { FreeBetsBadgeService } from '@app/vanillaInit/services/vanillaFreeBets/vanilla-freebets-badge.service';
import { IFreeBetsBadgeModel } from '@app/vanillaInit/models/free-bets.interface';
import { IFreebetToken } from '@bpp/services/bppProviders/bpp-providers.model';

@Injectable({
    providedIn: 'root'
})
export class VanillaFreebetsBadgeDynamicLoaderService extends AppVanillaFreebetsBadgeDynamicLoaderService {
  private headerPromoBadge: IFreeBetsBadgeModel = {
    section: MenuSection.Header,
    item: 'promo',
    count: null
  };
  private readonly badgeCount: string = 'FB';

  constructor(menuCountersService: MenuCountersService,
              freebetsBadgeService: FreeBetsBadgeService) {
    super(menuCountersService, freebetsBadgeService);
  }

  sportsFreebetsCount(freebetArr: IFreebetToken[]): string {
    const count = super.sportsFreebetsCount(freebetArr);
    return count ? this.badgeCount : null;
  }

  /**
   * Check if fbStatus equal false set null to headerBadge.count and execute addCounter method
   * @param fbStatus
   */
  updateBadge(fbStatus: boolean): void {
    this.headerBadge.count = fbStatus ? this.badgeCount : null;
    this.menuOffersBadge.count = fbStatus ? this.badgeCount : null;
    this.headerPromoBadge.count = fbStatus ? this.badgeCount : null;
    this.addCounter(this.headerBadge);
    this.addCounter(this.menuOffersBadge);
    this.addCounter(this.headerPromoBadge);
    this.update();
  }
}
