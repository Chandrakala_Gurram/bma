import { VanillaFreebetsBadgeDynamicLoaderService } from './vanilla-fb-badges-loader.service';

describe('Ladbrokes VanillaFreebetsBadgeDynamicLoaderService', () => {
  let fbBadgeLoaderService;
  let menuCountersService;
  let freebetsBadgeService;

  beforeEach(() => {
    menuCountersService = {
      update: jasmine.createSpy()
    };

    freebetsBadgeService = {
      freeBetCounters: []
    };

    fbBadgeLoaderService = new VanillaFreebetsBadgeDynamicLoaderService(menuCountersService, freebetsBadgeService);
  });

  describe('sportsFreebetsCount', () => {

    it('user has FreeBets" ', () => {
      const freeBetArr = [{ freebetTokenType: 'SPORTS' } as any];
      expect(fbBadgeLoaderService.sportsFreebetsCount(freeBetArr)).toBe('FB');
    });

    it('user has no FreeBets', () => {
      const freeBetArr = [];
      expect(fbBadgeLoaderService.sportsFreebetsCount(freeBetArr)).toBe(null);
    });

  });

});
