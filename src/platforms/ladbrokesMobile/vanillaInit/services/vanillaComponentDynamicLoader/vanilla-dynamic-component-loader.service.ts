import { Injectable } from '@angular/core';
import { VanillaDynamicComponentLoaderService as AppVanillaDynamicComponentLoaderService
} from '@app/vanillaInit/services/vanillaComponentDynamicLoader/vanilla-dynamic-component-loader.service';
import { BackButtonComponent } from '@ladbrokesMobile/shared/components/backButton/back-button.component';
import { BetslipHeaderIconComponent } from '@ladbrokesMobile/shared/components/betslipHeaderIcon/betslip-header-icon.component';

@Injectable({
  providedIn: 'root'
})
export class VanillaDynamicComponentLoaderService extends AppVanillaDynamicComponentLoaderService {
  protected VANILLA_HEADER_SLOTS: { [key: string]: string } = {
    betSlip: 'betslip',
    backBtn: 'logo-back'
  };

  protected addComponentsToVanillaHeader(): void {
    // Add betslip icon to Vanilla header dynamically
    this.headerService.setHeaderComponent(this.VANILLA_HEADER_SLOTS.betSlip, BetslipHeaderIconComponent);
    this.headerService.setHeaderComponent(this.VANILLA_HEADER_SLOTS.backBtn, BackButtonComponent);
  }
}
