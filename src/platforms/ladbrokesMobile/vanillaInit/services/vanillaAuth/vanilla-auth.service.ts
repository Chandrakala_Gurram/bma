import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';

import { VanillaAuthService as AppVanillaAuthService  } from '@app/vanillaInit/services/vanillaAuth/vanilla-auth.service';
import {
  BalanceService,
  UserService as VanillaUserService
} from '@vanilla/core/core';
import { AuthService as VanillaAuth } from '@vanilla/core/common';
import { ClaimsService } from '@vanilla/core';
import {
  LoginDialogService,
  RememberMeStatusService,
  NavigationService
} from '@labelhost/core/features';
import { UserService } from '@coreModule/services/user/user.service.ts';
import { NativeBridgeService } from '@coreModule/services/nativeBridge/native-bridge.service';
import { NewRelicService } from '@coreModule/services/newRelic/new-relic.service';
import { PubSubService } from '@coreModule/services/communication/pubsub/pubsub.service';
import { CoreToolsService } from '@coreModule/services/coreTools/core-tools.service';
import { FiltersService } from '@coreModule/services/filters/filters.service';
import { AfterLoginNotificationsService } from '@coreModule/services/afterLoginNotifications/after-login-notifications.service';
import { AuthService } from '@authModule/services/auth/auth.service';
import { VanillaFreebetsBadgeDynamicLoaderService } from '@platform/vanillaInit/services/vanillaFreeBets/vanilla-fb-badges-loader.service';
import { AccountUpgradeLinkService } from '@vanillaInitModule/services/accountUpgradeLink/account-upgrade-link.service';
import { NativeBridgeAdapter } from '@vanillaInitModule/services/NativeBridgeAdapter/nativebridge.adapter';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { StorageService } from '@core/services/storage/storage.service';
import { SessionService } from '@authModule/services/session/session.service';
import { ProxyHeadersService } from '@bpp/services/proxyHeaders/proxy-headers.service';
import { DeviceService } from '@core/services/device/device.service';
import { GermanSupportService } from '@core/services/germanSupport/german-support.service';

@Injectable({
  providedIn: 'root'
})
export class VanillaAuthService extends AppVanillaAuthService {

  static ngInjectableDef = undefined;

  constructor(
     vanillaUser: VanillaUserService,
     user: UserService,
     nativeBridgeService: NativeBridgeService,
     newRelicService: NewRelicService,
     pubsub: PubSubService,
     coreToolsService: CoreToolsService,
     filtersService: FiltersService,
     vanillaLoginDialogService: LoginDialogService,
     balanceService: BalanceService,
     afterLoginNotifications: AfterLoginNotificationsService,
     authService: AuthService,
     vanillaAuth: VanillaAuth,
     claimsService: ClaimsService,
     freeBetsBadgeLoader: VanillaFreebetsBadgeDynamicLoaderService,
     accountUpgradeLinkService: AccountUpgradeLinkService,
     nativeBridgeAdapter: NativeBridgeAdapter,
     storage: StorageService,
     windowRef: WindowRefService,
     router: Router,
     sessionService: SessionService,
     proxyHeadersService: ProxyHeadersService,
     device: DeviceService,
     rememberMeStatusService: RememberMeStatusService,
     navigationService: NavigationService,
     private germanSupportService: GermanSupportService,
     protected ngZone: NgZone
  ) {
    super(
      vanillaUser,
      user,
      nativeBridgeService,
      newRelicService,
      pubsub,
      coreToolsService,
      filtersService,
      vanillaLoginDialogService,
      balanceService,
      afterLoginNotifications,
      authService,
      vanillaAuth,
      claimsService,
      freeBetsBadgeLoader,
      accountUpgradeLinkService,
      nativeBridgeAdapter,
      storage,
      windowRef,
      router,
      sessionService,
      proxyHeadersService,
      device,
      rememberMeStatusService,
      navigationService,
      ngZone
    );
  }

  protected mapUserData(): void {
    super.mapUserData();
    this.storage.set('countryCode', this.vanillaUser.country);
  }

  protected login(): void {
    this.user.login(this.vanillaUser.ssoToken);
    this.user.resolveOpenApiAuth();
    this.authService.innerSessionLoggedIn.next(null);
    // redirect german user to main page
    this.germanSupportService.redirectToMainPageOnLogin();
    this.pubsub.publish(this.pubsub.API.LOGIN_PENDING, true);
    this.pubsub.publish(this.pubsub.API.SESSION_LOGIN, [{ User: this.user, options: {} }]);
  }
}
