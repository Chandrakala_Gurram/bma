import { NgModule } from '@angular/core';

import { SharedModule } from '@sharedModule/shared.module';

import { ApiServiceFactory } from '@vanilla/core';
import { MENU_COUNTERS_PROVIDER } from '@vanilla/core/common';
import { VanillaApiService } from '@vanillaInitModule/services/vanillaApi/vanilla-api.service';
import { VanillaAuthService } from '@vanillaInitModule/services/vanillaAuth/vanilla-auth.service';
import {
  VanillaSmartBannerHandlerService
} from '@vanillaInitModule/services/vanillaSmartBannerHandler/vanilla-smart-banner-handler.service';
// tslint:disable-next-line
import {
  VanillaDynamicComponentLoaderService
} from '@ladbrokesMobile/vanillaInit/services/vanillaComponentDynamicLoader/vanilla-dynamic-component-loader.service';
import {
  VanillaPortalModuleLoadNotifier
} from '@vanillaInitModule/services/vanillaPortalModuleLoadNotifier/vanilla-portal-module-load-notifier.service';
import { NativeBridgeAdapter } from '@vanillaInitModule/services/NativeBridgeAdapter/nativebridge.adapter';
import { PortalNativeEventNotifier } from '@vanillaInitModule/services/PortalNativeEventNotifier/portal-nativeEvent-notifier';
import { NativeBridgeBootstrapperService } from '@vanillaInitModule/services/NativeBridgeBootstrapper/native-bridge-bootstrapper.service';
import { VanillaFreebetsBadgeDynamicLoaderService
} from '@ladbrokesMobile/vanillaInit/services/vanillaFreeBets/vanilla-fb-badges-loader.service';
import { FreeBetsBadgeService } from '@app/vanillaInit/services/vanillaFreeBets/vanilla-freebets-badge.service';

export function apiServiceFactory(service: ApiServiceFactory) {
  return service.create(VanillaApiService, 'coralsports');
}

@NgModule({
  imports: [
    SharedModule
  ],
  providers: [
    VanillaPortalModuleLoadNotifier,
    NativeBridgeAdapter,
    PortalNativeEventNotifier,
    VanillaDynamicComponentLoaderService,
    VanillaFreebetsBadgeDynamicLoaderService,
    NativeBridgeBootstrapperService,
    { provide: MENU_COUNTERS_PROVIDER, useExisting: FreeBetsBadgeService, multi: true },
    { provide: VanillaApiService, deps: [ApiServiceFactory], useFactory: apiServiceFactory }
  ],
})
export class VanillaInitModule {
  constructor(
    private vanillaDynamicComponentLoaderService: VanillaDynamicComponentLoaderService,
    private vanillaAuthService: VanillaAuthService,
    private vanillaSmartBannerHandlerService: VanillaSmartBannerHandlerService
  ) {
    this.vanillaAuthService.init();
    this.vanillaDynamicComponentLoaderService.init();
    this.vanillaSmartBannerHandlerService.init();
  }
}
