import { Component } from '@angular/core';
import { BetslipMultiplesReceiptComponent } from '@root/app/betslip/components/betslipMultiplesReceipt/betslip-multiples-receipt.component';

@Component({
  selector: 'betslip-multiples-receipt',
  templateUrl: './betslip-multiples-receipt.component.html',
  styleUrls: ['../../../../../app/betslip/assets/styles/modules/receipt.less',
    '../../../../../app/betslip/components/betslipMultiplesReceipt/betslip-multiples-receipt.component.less',
    '../../assets/styles/modules/receipt.less']
})
export class LadbrokesBetslipMultiplesReceiptComponent extends BetslipMultiplesReceiptComponent {}
