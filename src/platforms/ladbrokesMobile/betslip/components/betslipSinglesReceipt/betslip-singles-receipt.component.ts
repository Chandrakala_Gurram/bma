import { Component } from '@angular/core';
import { BetslipSinglesReceiptComponent } from '@root/app/betslip/components/betslipSinglesReceipt/betslip-singles-receipt.component';

@Component({
  selector: 'betslip-singles-receipt',
  templateUrl: './betslip-singles-receipt.component.html',
  styleUrls: [
    '../../../../../app/betslip/assets/styles/modules/receipt.less',
    '../../assets/styles/modules/receipt.less', './betslip-singles-receipt.component.less'
  ]
})
export class LadbrokesBetslipSinglesReceiptComponent extends BetslipSinglesReceiptComponent {}
