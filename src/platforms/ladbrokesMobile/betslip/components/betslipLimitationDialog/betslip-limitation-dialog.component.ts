import { Component } from '@angular/core';
// tslint:disable-next-line:max-line-length
import { BetslipLimitationDialogComponent as AppBetslipLimitationDialogComponent } from '@betslip/components/betslipLimitationDialog/betslip-limitation-dialog.component';

@Component({
  selector: 'betslip-limitation-dialog',
  templateUrl: 'betslip-limitation-dialog.component.html'
})
export class BetslipLimitationDialogComponent extends AppBetslipLimitationDialogComponent {}
