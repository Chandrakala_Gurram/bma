import { Component } from '@angular/core';
import { ToteBetReceiptComponent as AppToteBetReceiptComponent } from '@app/betslip/components/toteBetReceipt/tote-bet-receipt.component';
@Component({
  selector: 'tote-bet-receipt',
  templateUrl: './tote-bet-receipt.component.html'
})
export class ToteBetReceiptComponent extends AppToteBetReceiptComponent {}
