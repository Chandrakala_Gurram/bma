import { LadbrokesBetslipReceiptComponent } from '@ladbrokesMobile/betslip/components/betslipReceipt/betslip-receipt.component';
import { of as observableOf } from 'rxjs';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('LadbrokesBetslipReceiptComponent', () => {
  let component: LadbrokesBetslipReceiptComponent;

  let user,
    betReceiptService,
    sessionService,
    storageService,
    betSlipBannerService,
    betInfoDialogService,
    locationStub,
    gtmService,
    router,
    commandService,
    device,
    nativeBridgeService,
    windowRefService,
    bannersService,
    germanSupportService,
    bodyScrollLockService,
    gtmTrackingService,
    overAskService,
    localeService,
    pubSubService;

  beforeEach(() => {
    user = {
      oddsFormat: 'frac',
      receiptViewsCounter: 0,
      winAlertsToggled: false,
      set: jasmine.createSpy()
    };
    betReceiptService = {
      getActiveSportsEvents: jasmine.createSpy('getActiveSportsEvents'),
      getActiveFootballEvents: jasmine.createSpy('getActiveFootballEvents'),
      getGtmObject: jasmine.createSpy().and.returnValue({}),
      reuse: jasmine.createSpy('reuse'),
      done: jasmine.createSpy('done'),
      clearMessage: jasmine.createSpy('clearMessage'),
      getBetReceipts: jasmine.createSpy('getBetReceipts').and.returnValue(observableOf({}))
    };
    sessionService = {
      whenProxySession: jasmine.createSpy('whenProxySession').and.returnValue(Promise.resolve({}))
    };
    storageService = {
      remove: jasmine.createSpy('remove'),
      get: jasmine.createSpy('get')
    };
    betSlipBannerService = {
      setIsBannerAvailable: jasmine.createSpy(),
      setBanner: jasmine.createSpy()
    };
    betInfoDialogService = {
      multiple: jasmine.createSpy()
    };
    locationStub = {
      path: () => ''
    };
    gtmService = {
      push: jasmine.createSpy()
    };
    gtmTrackingService = {};
    router = {
      navigate: jasmine.createSpy()
    };
    commandService = {
      executeAsync: () => Promise.resolve({
        streamActive: true,
        streamID: '111'
      }),
      API: {
        GET_LIVE_STREAM_STATUS: ''
      }
    };
    device = {};
    nativeBridgeService = {};
    windowRefService = {
      nativeWindow: {
        pushNotificationsEnabled: true
      }
    } as any;
    germanSupportService = {
      isGermanUser: jasmine.createSpy('isGermanUser').and.returnValue(true)
    };
    bannersService = {
      fetchBetslipOffersFromAEM: jasmine.createSpy('fetchBetslipOffersFromAEM').and.returnValue(
        observableOf({ offers: [] })
      )
    };
    bodyScrollLockService = {
      enableBodyScroll: jasmine.createSpy('enableBodyScroll')
    };
    overAskService = {};
    localeService = {
      getString: jasmine.createSpy('getString')
    };
    pubSubService = {
      API: pubSubApi,
      publish: jasmine.createSpy('publish')
    };
  });

  function createComponent() {
    component = new LadbrokesBetslipReceiptComponent(
      user,
      betReceiptService,
      sessionService,
      storageService,
      betSlipBannerService,
      betInfoDialogService,
      bannersService,
      locationStub,
      gtmService,
      router,
      commandService,
      device,
      gtmTrackingService,
      bodyScrollLockService,
      nativeBridgeService,
      windowRefService,
      overAskService,
      localeService,
      pubSubService,
      germanSupportService);
  }

  afterEach(() => {
    component = null;
  });

  it('should create component instance', () => {
    createComponent();
    expect(component).toBeTruthy();
  });

  describe('#ngOnInit', () => {
    it('#ngOnInit', () => {
      createComponent();
      component.ngOnInit();

      expect(component.isGermanUser).toBeTruthy();
    });
  });
});
