import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { UserService } from '@core/services/user/user.service';
import { BetReceiptService } from '@betslip/services/betReceipt/bet-receipt.service';
import { DeviceService } from '@core/services/device/device.service';
import { SessionService } from '@authModule/services/session/session.service';
import { StorageService } from '@core/services/storage/storage.service';
import { BetSlipBannerService } from '@betslip/components/betslipBanner/betslip-banner.service';
import { BetInfoDialogService } from '@betslip/services/betInfoDialog/bet-info-dialog.service';
import { GtmService } from '@core/services/gtm/gtm.service';
import { CommandService } from '@core/services/communication/command/command.service';
import { NativeBridgeService } from '@core/services/nativeBridge/native-bridge.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { BetslipReceiptComponent } from '@betslip/components/betslipReceipt/betslip-receipt.component';
import { GermanSupportService } from '@core/services/germanSupport/german-support.service';
import { BannersService } from '@core/services/aemBanners/banners.service';
import { GtmTrackingService } from '@core/services/gtmTracking/gtm-tracking.service';
import { BodyScrollLockService } from '@betslip/services/bodyScrollLock/betslip-body-scroll-lock.service';
import { OverAskService } from '@betslip/services/overAsk/over-ask.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'betslip-receipt',
  templateUrl: 'betslip-receipt.component.html',
  styleUrls: ['./betslip-receipt.component.less']
})
export class LadbrokesBetslipReceiptComponent extends BetslipReceiptComponent implements OnInit {
  isGermanUser: boolean;
  constructor(user: UserService,
              betReceiptService: BetReceiptService,
              sessionService: SessionService,
              storageService: StorageService,
              betSlipBannerService: BetSlipBannerService,
              betInfoDialogService: BetInfoDialogService,
              aemBannersService: BannersService,
              location: Location,
              gtmService: GtmService,
              router: Router,
              comandService: CommandService,
              device: DeviceService,
              gtmTrackingService: GtmTrackingService,
              bodyScrollLockService: BodyScrollLockService,
              nativeBridge: NativeBridgeService,
              window: WindowRefService,
              overAskService: OverAskService,
              localeService: LocaleService,
              protected pubSubService: PubSubService,
              private germanSupportService: GermanSupportService) {
    super(user, betReceiptService, sessionService, storageService, betSlipBannerService, betInfoDialogService,
      aemBannersService, location, gtmService, router, comandService, device,
      gtmTrackingService,
      bodyScrollLockService, nativeBridge, window, overAskService, localeService, pubSubService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.isGermanUser = this.germanSupportService.isGermanUser();
  }
}

