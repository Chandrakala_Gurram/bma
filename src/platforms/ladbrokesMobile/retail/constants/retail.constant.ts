import * as oxygenRetailConstants from '@app/retail/constants/retail.constant';

export const UPGRADE_ACCOUNT_DIALOG = {
  ...oxygenRetailConstants.UPGRADE_ACCOUNT_DIALOG,
  inshopUpgrade: {
    ...oxygenRetailConstants.UPGRADE_ACCOUNT_DIALOG.inshopUpgrade,
    dialogHeader: 'inshop-upgrade-dialog-header',
    dialogButton: 'inshop-upgrade-dialog-button'
  },
  onlineUpgrade: {
    dialogHeader: 'online-upgrade-dialog-header',
    dialogBody: 'online-upgrade-dialog-body',
    dialogButton: 'online-upgrade-dialog-button'
  }
};
