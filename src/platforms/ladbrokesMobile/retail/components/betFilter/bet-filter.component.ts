import { Component } from '@angular/core';

import { BetFilterComponent as OxygenBetFilterComponent } from '@app/retail/components/betFilter/bet-filter.component';

@Component({
  selector: 'bet-filter',
  templateUrl: 'bet-filter.component.html'
})

export class BetFilterComponent extends OxygenBetFilterComponent {
}
