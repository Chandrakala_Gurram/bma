import { Component, OnInit, OnDestroy } from '@angular/core';
import { RetailPageComponent as OxygenRetailPageComponent } from '@app/retail/components/retailPage/retail-page.component';
import { UserService } from '@core/services/user/user.service';
import { LocaleService } from '@app/core/services/locale/locale.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { IStaticBlock, ISystemConfig } from '@app/core/services/cms/models';
import { PubSubService } from '@app/core/services/communication/pubsub/pubsub.service';
import { UPGRADE_ACCOUNT_DIALOG } from '@ladbrokesMobile/retail/constants/retail.constant';

@Component({
  selector: 'retail-page',
  templateUrl: 'retail-page.component.html'
})

export class RetailPageComponent extends OxygenRetailPageComponent implements OnInit, OnDestroy {
  upgradeButtonContent: SafeHtml;
  showUpgradeButton: boolean;
  isUpgradeEnable: boolean;

  constructor(
    private userService: UserService,
    private localeService: LocaleService,
    private cmsService: CmsService,
    private domSanitizer: DomSanitizer,
    private pubSubService: PubSubService
  ) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.configUpgradeButton();
    this.pubSubService.subscribe('userLoginOnHub',
    [this.pubSubService.API.SUCCESSFUL_LOGIN, this.pubSubService.API.SESSION_LOGOUT], () => {
      this.configUpgradeButton();
    });
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe('userLoginOnHub');
  }

  private configUpgradeButton(): void {
    this.cmsService.getSystemConfig().subscribe((data: ISystemConfig) => {
      this.isUpgradeEnable = data.Connect && data.Connect.upgrade; // TODO: rename to retail after changes in cms.
      this.showUpgradeButton = (this.userService.status && !this.userService.isMultiChannelUser()) && this.isUpgradeEnable;

      if (this.userService.isInShopUser() && this.showUpgradeButton) {
        this.getUpgradeButtonStaticBlock(UPGRADE_ACCOUNT_DIALOG.inshopUpgrade.dialogButton);
      } else if (!this.userService.isRetailUser() && this.showUpgradeButton) {
        this.getUpgradeButtonStaticBlock(UPGRADE_ACCOUNT_DIALOG.onlineUpgrade.dialogButton);
      }
    });
  }

  private getUpgradeButtonStaticBlock(blockName: string): void {
    this.cmsService.getStaticBlock(blockName, this.localeService.getLocale().toLowerCase())
      .subscribe((cmsContent: IStaticBlock) => {
        this.upgradeButtonContent = this.domSanitizer.bypassSecurityTrustHtml(cmsContent.htmlMarkup);
      }, err => {
        console.warn('An error occured when loading button content from CMS', err);
    });
  }
}
