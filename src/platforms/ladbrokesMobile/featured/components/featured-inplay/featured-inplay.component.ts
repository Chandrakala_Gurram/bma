import { Component, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { FeaturedInplayComponent } from '@featured/components/featured-inplay/featured-inplay.component';

@Component({
  selector: 'featured-inplay',
  styleUrls: ['../../../../../app/featured/components/featured-inplay/featured-inplay.component.less', './featured-inplay.component.less'],
  templateUrl: '../../../../../app/featured/components/featured-inplay/featured-inplay.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation : ViewEncapsulation.None
})
export class LadbrokesFeaturedInplayComponent extends FeaturedInplayComponent {}
