import { Component, ChangeDetectionStrategy } from '@angular/core';
import { FeaturedQuickLinksComponent } from '@featured/components/featured-quick-links/featured-quick-links.component';

@Component({
  selector: 'featured-quick-links',
  templateUrl: '../../../../../app/featured/components/featured-quick-links/featured-quick-links.component.html',
  styleUrls: ['../../../../../app/featured/components/featured-quick-links/featured-quick-links.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LadbrokesFeaturedQuickLinksComponent extends FeaturedQuickLinksComponent {}
