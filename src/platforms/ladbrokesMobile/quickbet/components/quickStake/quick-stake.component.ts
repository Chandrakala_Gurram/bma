import { Component, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';

import { QuickStakeComponent } from '@app/quickbet/components/quickStake/quick-stake.component';

@Component({
  selector: 'quick-stake',
  templateUrl: '../../../../../app/quickbet/components/quickStake/quick-stake.component.html',
  styleUrls: ['../../../../../app/quickbet/components/quickStake/quick-stake.component.less', 'quick-stake.component.less'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class LadbrokesQuickStakeComponent extends QuickStakeComponent {}
