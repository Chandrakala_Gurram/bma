import { Component } from '@angular/core';

import { QuickbetInfoPanelComponent } from '@app/quickbet/components/quickbetInfoPanel/quickbet-info-panel.component';

@Component({
  selector: 'quickbet-info-panel',
  templateUrl: '../../../../../app/quickbet/components/quickbetInfoPanel/quickbet-info-panel.component.html',
  styleUrls: ['quickbet-info-panel.component.less']
})
export class LadbrokesQuickbetInfoPanelComponent extends QuickbetInfoPanelComponent {}
