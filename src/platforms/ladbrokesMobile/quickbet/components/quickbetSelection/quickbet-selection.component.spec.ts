import { LadbrokesQuickbetSelectionComponent } from '@ladbrokesMobile/quickbet/components/quickbetSelection/quickbet-selection.component';

describe('LadbrokesQuickbetSelectionComponent', () => {
  let pubSubService;
  let userService;
  let localeService;
  let filtersService;
  let quickbetDepositService;
  let quickbetService;
  let quickbetUpdateService;
  let freeBetsService;
  let quickbetNotificationService;
  let commandService;
  let cmsService;
  let component: LadbrokesQuickbetSelectionComponent;
  let gtmService;
  let windowRef;
  let cdr;

  beforeEach(() => {
    pubSubService = {};
    userService = {};
    localeService = {
      getString: jasmine.createSpy().and.returnValue('test_str')
    };
    filtersService = {};
    quickbetDepositService = {};
    quickbetService = {};
    quickbetUpdateService = {};
    freeBetsService = {};
    quickbetNotificationService = {};
    commandService = {};
    cmsService = {};
    gtmService = {};
    cdr = {};
    windowRef = {
      nativeWindow: {
        addEventListener: jasmine.createSpy('addEventListener'),
        removeEventListener: jasmine.createSpy('removeEventListener'),
        setTimeout: jasmine.createSpy('setTimeout')
      }
    };
    windowRef = {
      nativeWindow: {
        addEventListener: jasmine.createSpy('addEventListener'),
        removeEventListener: jasmine.createSpy('removeEventListener'),
        setTimeout: jasmine.createSpy('setTimeout')
      }
    };

    component = new LadbrokesQuickbetSelectionComponent(pubSubService, userService, localeService, filtersService,
      quickbetDepositService, quickbetService, quickbetUpdateService, freeBetsService, quickbetNotificationService, commandService,
      cmsService, gtmService, cdr, windowRef);
  });

  describe('@showMessage', () => {
    it('should show deposit message', () => {
      component.infoPanel = {
        msg: 'err',
        location: 'quick-deposit',
        type: 'error'
      };
      expect(component.showMessage()).toBe(true);
    });

    it('should show boosted price change message', () => {
      component.isBoostEnabled = true;
      component.selection = {
        oddsBoost: {},
        price: { isPriceChanged: true },
        isBoostActive: true
      } as any;
      expect(component.showMessage()).toBe(true);
    });

    it('should not show message', () => {
      expect(component.showMessage()).toBe(false);
    });
  });
});
