import { Component, OnInit } from '@angular/core';

import { QuickbetSelectionComponent } from '@app/quickbet/components/quickbetSelection/quickbet-selection.component';
import { IQuickbetNotificationModel } from '@app/quickbet/models/quickbet-notification.model';

@Component({
  selector: 'quickbet-selection',
  templateUrl: './quickbet-selection.component.html',
  styleUrls: ['quickbet-selection.component.less']
})

export class LadbrokesQuickbetSelectionComponent extends QuickbetSelectionComponent implements OnInit {
  infoMessages: Array<string> = [];
  infoPanel: IQuickbetNotificationModel;

  ngOnInit(): void {
    super.ngOnInit();

    this.updatePanelMessage = this.updatePanelMessage.bind(this);
    this.updatePanelMessage(this.quickbetNotificationService.config);
    this.pubsub.subscribe(this.QB_SELECTION_NAME, this.pubsub.API.QUICKBET_INFO_PANEL, this.updatePanelMessage);
  }

  showMessage(): boolean {
    const isDepositMessage = this.infoPanel && this.infoPanel.msg && this.infoPanel.location === 'quick-deposit';
    const isPriceBoostMessage = this.canBoostSelection && this.selection.price.isPriceChanged && this.selection.isBoostActive;
    const messages = [];

    if (isDepositMessage) {
      messages.push(this.infoPanel.msg);
    }

    if (isPriceBoostMessage) {
      messages.push(this.locale.getString('quickbet.reboostPriceChanged'));
    }

    this.infoMessages = messages;

    return !!messages.length;
  }

  private updatePanelMessage(infoPanelObj: IQuickbetNotificationModel): void {
    const isDepositMessage = !infoPanelObj.location || infoPanelObj.location === 'quick-deposit';

    if (isDepositMessage) {
      this.infoPanel = Object.assign({}, infoPanelObj);
    }
  }
}
