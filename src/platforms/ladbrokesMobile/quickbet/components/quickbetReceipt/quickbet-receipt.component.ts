import { Component, OnInit } from '@angular/core';

import { UserService } from '@core/services/user/user.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { QuickbetService } from '@app/quickbet/services/quickbetService/quickbet.service';
import { NativeBridgeService } from '@core/services/nativeBridge/native-bridge.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';

import { QuickbetReceiptComponent } from '@app/quickbet/components/quickbetReceipt/quickbet-receipt.component';
import { GermanSupportService } from '@core/services/germanSupport/german-support.service';
import { StorageService } from '@core/services/storage/storage.service';

@Component({
  selector: 'quickbet-receipt',
  templateUrl: 'quickbet-receipt.component.html',
  styleUrls: ['./quickbet-receipt.component.less']
})

export class LadbrokesQuickbetReceiptComponent extends QuickbetReceiptComponent implements OnInit {
  isGermanUser: boolean;
  constructor(user: UserService,
              filtersService: FiltersService,
              quickbetService: QuickbetService,
              nativeBridge: NativeBridgeService,
              window: WindowRefService,
              storageService: StorageService,
              private germanSupportService: GermanSupportService) {
    super(user, filtersService, quickbetService, nativeBridge, window, storageService);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.isGermanUser = this.germanSupportService.isGermanUser();
  }
}
