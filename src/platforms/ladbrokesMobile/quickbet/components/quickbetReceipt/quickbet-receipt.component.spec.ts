import { LadbrokesQuickbetReceiptComponent } from '@ladbrokesMobile/quickbet/components/quickbetReceipt/quickbet-receipt.component';
import { IQuickbetReceiptDetailsModel } from '@root/app/quickbet/models/quickbet-receipt.model';
import { IQuickbetSelectionModel } from '@core/models/quickbet-selection.model';

describe('#LadbrokesQuickbetReceiptComponent', () => {
  let  component: LadbrokesQuickbetReceiptComponent,
       user,
       filtersService,
       quickbetService,
       nativeBridge,
       window,
       germanSupportService,
       storageService;

  beforeEach(() => {
    user = {
      currencySymbol: '$',
      receiptViewsCounter: 0,
      winAlertsToggled: false,
      set: jasmine.createSpy()
    };

    filtersService = {
      setCurrency: jasmine.createSpy(),
      filterPlayerName: jasmine.createSpy('filterPlayerName').and.returnValue(''),
      filterAddScore: jasmine.createSpy('filterAddScore').and.returnValue('')
    };

    quickbetService = {
      getOdds: jasmine.createSpy('getOdds'),
      getEWTerms: jasmine.createSpy('getEWTerms'),
      getLinesPerStake: jasmine.createSpy('getLinesPerStake'),
      isVirtualSport: jasmine.createSpy('isVirtualSport').and.returnValue(true),
      getBybSelectionType: jasmine.createSpy('getBybSelectionType')
    };

    nativeBridge = {
      onActivateWinAlerts: jasmine.createSpy()
    };

    window = {
      nativeWindow : {
        pushNotificationsEnabled: true
      }
    };

    germanSupportService = {
      isGermanUser: jasmine.createSpy('isGermanUser').and.returnValue(true)
    };

    storageService = {
      set: jasmine.createSpy('set'),
      get: jasmine.createSpy('get')
    };

    component = new LadbrokesQuickbetReceiptComponent(
      user,
      filtersService,
      quickbetService,
      nativeBridge,
      window,
      storageService,
      germanSupportService
    );

    component.selection = {
      stake: '1.22'
    } as IQuickbetSelectionModel;

    component.betReceipt = {
      bet: { id: 1 },
      stake: {
        amount: '100',
        freebet: '10',
        stakePerLine: '111',
      },
      legParts: [{
        eventDesc: '',
        marketDesc: '',
        handicap: '',
        outcomeDesc: ''
      }],
      price: {
        priceType: 'test_price'
      },
      payout: { potential: '99' },
      oddsValue: '1/2'
    } as IQuickbetReceiptDetailsModel;
  });

  it('should create LadbrokesQuickbetReceiptComponent instance', () => {
    expect(component).toBeTruthy();
  });

  describe('#ngOnInit', () => {
    it('should set isGermanUser', () => {
      component.ngOnInit();
      expect(component.isGermanUser).toBeTruthy();
    });
  });

  afterEach(() => {
    component = null;
  });
});
