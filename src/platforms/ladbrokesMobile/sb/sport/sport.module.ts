import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from '@sharedModule/shared.module';
import { CouponsModule } from '@sb/coupons/coupons.module';
import { SportMatchesPageComponent } from '@sb/components/sportMatchesPage/sport-matches-page.component';
import { SportTabsPageComponent } from '@sb/components/sportTabsPage/sport-tabs-page.component';
import { SportMatchesTabComponent } from '@sbModule/components/sportMatchesTab/sport-matches-tab.component';
import { LazySportRoutingModule } from '@sb/sport/sport-routing.module';
import { SbModule } from '@ladbrokesMobile/sb/sb.module';
import { SportMainModule } from '@ladbrokesMobile/sb/components/sportMain/sport-main.module';

@NgModule({
  imports: [
    SharedModule,
    SbModule, // TODO do not import when SbModule will be sliced
    SportMainModule,
    CouponsModule,

    LazySportRoutingModule
  ],
  declarations: [
    SportMatchesPageComponent,
    SportTabsPageComponent,
    SportMatchesTabComponent
  ],
  entryComponents: [
    SportMatchesPageComponent,
    SportTabsPageComponent,
    SportMatchesTabComponent
  ],
  providers: [],
  exports: [
    SportMatchesPageComponent,
    SportTabsPageComponent,
    SportMatchesTabComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class LazySportModule { }
