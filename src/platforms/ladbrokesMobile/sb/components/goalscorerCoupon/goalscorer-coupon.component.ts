import { Component } from '@angular/core';
import { GoalscorerCouponComponent as AppGoalscorerCouponComponent } from '@sb/components/goalscorerCoupon/goalscorer-coupon.component';

@Component({
  selector: 'goalscorer-coupon',
  templateUrl: './goalscorer-coupon.component.html',
  styleUrls: [
    '../../../../../app/sb/components/goalscorerCoupon/goalscorer-coupon.component.less',
    './goalscorer-coupon.component.less'
  ]
})
export class GoalscorerCouponComponent extends AppGoalscorerCouponComponent {}
