import { Component } from '@angular/core';

import { CouponsDetailsComponent as CoralCouponsDetailsComponent } from '@sb/components/couponsDetails/coupons-details.component';

@Component({
  selector: 'coupons-details',
  templateUrl: './coupons-details.component.html'
})
export class CouponsDetailsComponent extends CoralCouponsDetailsComponent {}
