import { LadbrokesBmaMainComponent } from './components/bmaMain/bma-main.component';
import { FormsModule } from '@angular/forms';
import { APP_INITIALIZER, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ModalModule } from 'angular-custom-modal';
import { SharedModule } from '@sharedModule/shared.module';
import { FavouritesModule } from '@favouritesModule/favourites.module';
import { BmaInit } from '@app/bma/services/bmaRunService/bma-init';
import { BmaRunService } from '@app/bma/services/bmaRunService/bma-run.service';
import { ContactUsComponent } from '@app/bma/components/contactUs/contact-us.component';
import { LogoutResolver } from '@app/vanillaInit/services/logout/logout.service';

import { HomeComponent } from '@app/bma/components/home/home.component';
import { NotFoundComponent } from '@bma/components/404/404.component';
import {
  RightColumnWidgetWrapperComponent } from '@app/bma/components/rightColumn/rightColumnWidgetWrapper/right-column-widget-wrapper.component';
import { RightColumnWidgetComponent } from '@app/bma/components/rightColumn/rightColumnWidget/right-column-widget.component';
import { RightColumnWidgetItemComponent } from '@app/bma/components/rightColumn/rightColumnWidgetItem/right-column-widget-item.component';
import { WBetslipComponent } from '@app/bma/components/rightColumn/wBetslip/w-betslip.component';
import { WMatchCentreComponent } from '@app/bma/components/rightColumn/wMatchCentre/w-match-centre.component';
import { InplayHomeTabComponent } from '@bma/components/inlayHomeTab/inplay-home-tab.component';
import { BannersModule } from '@banners/banners.module';
import { LocaleService } from '@core/services/locale/locale.service';
import { StaticComponent } from '@bma/components/static/static.component';
import * as criticalLangData from '@ladbrokesMobile/lazy-modules/locale/translations/en-US/critical-lang-data';
import { LiveStreamWrapperComponent } from '@app/bma/components/liveStream/live-stream-wrapper.component';

@NgModule({
  imports: [
    SharedModule,
    ModalModule,
    FormsModule,
    FavouritesModule,
    BannersModule
  ],
  providers: [
    BmaRunService,
    {
      provide: APP_INITIALIZER,
      useFactory: BmaInit,
      deps: [BmaRunService],
      multi: true
    },
    LogoutResolver
  ],
  declarations: [
    LadbrokesBmaMainComponent,
    ContactUsComponent,
    HomeComponent,
    NotFoundComponent,
    RightColumnWidgetWrapperComponent,
    RightColumnWidgetComponent,
    RightColumnWidgetItemComponent,
    WBetslipComponent,
    WMatchCentreComponent,
    InplayHomeTabComponent,
    StaticComponent,
    LiveStreamWrapperComponent
  ],
  entryComponents: [
    LadbrokesBmaMainComponent,
    ContactUsComponent,
    HomeComponent,
    NotFoundComponent,
    RightColumnWidgetWrapperComponent,
    RightColumnWidgetComponent,
    RightColumnWidgetItemComponent,
    WBetslipComponent,
    WMatchCentreComponent,
    InplayHomeTabComponent,
    LiveStreamWrapperComponent
  ],
  exports: [
    LadbrokesBmaMainComponent,
    ContactUsComponent,
    NotFoundComponent,
    RightColumnWidgetWrapperComponent,
    RightColumnWidgetComponent,
    RightColumnWidgetItemComponent,
    WBetslipComponent,
    WMatchCentreComponent,
    InplayHomeTabComponent,
    StaticComponent,
    LiveStreamWrapperComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class BmaModule {
  constructor(private localeService: LocaleService) {
    this.localeService.setLangData(criticalLangData);
  }
}
