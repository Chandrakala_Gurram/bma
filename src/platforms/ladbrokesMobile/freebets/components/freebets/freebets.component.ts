import { finalize, map } from 'rxjs/operators';
import { forkJoin as observableForkJoin, Observable } from 'rxjs';
import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '@core/services/user/user.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { IFreebetToken, ITokenPossibleBet } from '@app/bpp/services/bppProviders/bpp-providers.model';
import { FreeBetsService } from '@coreModule/services/freeBets/free-bets.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { InfoDialogService } from '@coreModule/services/infoDialogService/info-dialog.service';
import { FreebetsComponent as AppFreebetsComponent } from '@freebets/components/freebets/freebets.component';

@Component({
  selector: 'freebets',
  styleUrls: ['freebets.component.less'],
  templateUrl: './freebets.component.html'
})
export class FreebetsComponent extends AppFreebetsComponent implements OnInit {
  @Output() readonly closeOverlay = new EventEmitter();
  freebets: IFreebetToken[];
  totalFreeBetsAmount: string;
  totalFreeBets: string;
  sportBalance: string;
  totalBalance: string;
  freeBetText: string = this.localeService.getString('bma.freeBet');
  private readonly defaultDialogText = `${this.localeService.getString('bma.any')} ${this.localeService.getString('bma.event')}`;


  constructor(
    protected userService: UserService,
    protected filtersService: FiltersService,
    protected freeBetsService: FreeBetsService,
    protected router: Router,
    private localeService: LocaleService,
    private infoDialogService: InfoDialogService,
    public user: UserService,
  ) {
    super(
      userService,
      filtersService,
      freeBetsService,
      router
    );
  }

  trackByTokenId(index: number, item: IFreebetToken): number {
    return +item.freebetTokenId;
  }

  navigateToEvent(freeBet: IFreebetToken): void {
    freeBet.pending = true;
    this.freeBetsService.getFreeBetWithBetNowLink(freeBet).pipe(
      finalize(() => {
        freeBet.pending = false;
        this.closeOverlay.emit();
      })).subscribe((freeBetItem: IFreebetToken ) => {
      this.router.navigateByUrl(freeBetItem.betNowLink);
    }, () => {});
  }

  stopOuterAction($event: Event): void {
    $event.stopPropagation();
  }

  showInfoDialog(freeBet: IFreebetToken): void {
    const tokens = freeBet.tokenPossibleBets && freeBet.tokenPossibleBets.filter(tokenPossibleBet => {
      return tokenPossibleBet.betId && tokenPossibleBet.betLevel;
    });

    if (tokens && tokens.length) {
      const tokenObservables = tokens.map(tokenPossibleBet => {
        return this.freeBetsService.getBetLevelName((tokenPossibleBet.betId).toString(), tokenPossibleBet.betLevel);
      });
      freeBet.pending = true;
      this.buildDialogMessage(tokenObservables, tokens).pipe(
        finalize(() => freeBet.pending = false)
      ).subscribe(
        dialogMessage => this.openDefaultDialog(dialogMessage),
        () => this.openDefaultDialog()
      );
    } else {
      this.openDefaultDialog();
    }
  }

  private openDefaultDialog(dialogMessage?: string): void {
    this.infoDialogService.openInfoDialog(
      this.localeService.getString('bma.freeBet'),
      `${this.localeService.getString('bma.freeBetInfo')} ${dialogMessage || this.defaultDialogText}.`,
      undefined,
      undefined,
      undefined,
      [{
        caption: 'OK',
        cssClass: 'btn-style2 okButton'
      }]
    );
  }

  private buildDialogMessage(arrOfObservables: Observable<any>[], tokens: ITokenPossibleBet[]): Observable<string> {
    return observableForkJoin(arrOfObservables).pipe(
      map((messages) => {
        return messages.map((name, index) => {
          if (!name) {
            return '';
          }
          return `${name} ${FreeBetsService.EVENT_LEVELS.includes(tokens[index].betLevel) ?
            this.localeService.getString('bma.event') :
            this.localeService.getString('bma.events')}`;
        }).filter(name => name).join(', ');
      })
    );
  }

}
