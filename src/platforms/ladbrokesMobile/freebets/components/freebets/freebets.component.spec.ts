import { ComponentFixture, fakeAsync, flush, TestBed } from '@angular/core/testing';
import { of as observableOf, throwError } from 'rxjs';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FreebetsComponent } from './freebets.component';
import { LocaleService } from '@core/services/locale/locale.service';
import { Router } from '@angular/router';
import { FreeBetsService } from '@coreModule/services/freeBets/free-bets.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { InfoDialogService } from '@core/services/infoDialogService/info-dialog.service';
import { UserService } from '@core/services/user/user.service';

describe('LadbrokesFreebetsComponent', () => {
  let fixture: ComponentFixture<FreebetsComponent>,
    component: FreebetsComponent, user, localeService, router, freeBetsService, infoDialogService;

  beforeEach(() => {
    user = {};

    localeService = {
      getString: jasmine.createSpy('getString').and.returnValue('some string')
    };

    router = {
      navigate: jasmine.createSpy('navigateByUrl'),
      navigateByUrl: jasmine.createSpy('navigateByUrl')
    };

    freeBetsService = {
      getFreeBetWithBetNowLink: jasmine.createSpy('getFreeBetWithBetNowLink'),
      getBetLevelName: jasmine.createSpy('getBetLevelName')
    };

    infoDialogService = {
      openInfoDialog: jasmine.createSpy('openInfoDialog')
    };

    TestBed.configureTestingModule({
      declarations: [FreebetsComponent],
      providers: [
        { provide: UserService, useValue: user },
        { provide: LocaleService, useValue: localeService },
        { provide: Router, useValue: router },
        { provide: FreeBetsService, useValue: freeBetsService },
        { provide: InfoDialogService, useValue: infoDialogService },
        { provide: FiltersService },

      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(FreebetsComponent);
    component = fixture.componentInstance;
  });

  it('should create LadbrokesFreebetsComponent instance', () => {
    expect(component).toBeTruthy();
  });

  it('#trackByTokenId', () => {
    const result = component.trackByTokenId(1, { freebetTokenId: '1' } as any);
    expect(result).toBe(1);
  });

  describe('#navigateToEvent', () => {
    beforeEach(() => {
      spyOn(component.closeOverlay, 'emit');
    });
    it('should successfully navigate', () => {
      const freeBet = { betNowLink: 'someLink' } as any;
      freeBetsService.getFreeBetWithBetNowLink.and.returnValue(observableOf(freeBet));
      component.navigateToEvent(freeBet);
      expect(router.navigateByUrl).toHaveBeenCalledWith('someLink');
      expect(freeBet.pending).toBeFalsy();
      expect(component.closeOverlay.emit).toHaveBeenCalledTimes(1);
    });

    it('shouldn`t successfully navigate', () => {
      const freeBet = { betNowLink: '' } as any;
      freeBetsService.getFreeBetWithBetNowLink.and.returnValue(throwError({}));
      component.navigateToEvent(freeBet);
      expect(router.navigateByUrl).not.toHaveBeenCalled();
      expect(freeBet.pending).toBeFalsy();
      expect(component.closeOverlay.emit).toHaveBeenCalledTimes(1);
    });
  });

  it('#stopOuterAction', () => {
    const event = {
      stopPropagation: jasmine.createSpy('stopPropagation')
    } as any;
    component.stopOuterAction(event);
    expect(event.stopPropagation).toHaveBeenCalledTimes(1);
  });

  describe('showInfoDialog', () => {
    beforeEach(() => {
      spyOn(component as any, 'buildDialogMessage').and.callThrough();
    });

    it('should open default dialog', function () {
      component.showInfoDialog({} as any);
      expect(component['buildDialogMessage']).not.toHaveBeenCalled();
      expect(infoDialogService.openInfoDialog).toHaveBeenCalledWith(
        'some string',
        'some string some string some string.',
        undefined,
        undefined,
        undefined,
        jasmine.any(Array)
      );
    });

    it('should open dialog with bet level name', fakeAsync(() => {
      freeBetsService.getBetLevelName.and.returnValue(observableOf('level name'));
      component.showInfoDialog({
        tokenPossibleBets: [
          { betId: '0', betLevel: 'betLevel' },
          { betId: '1', betLevel: 'betLevel' }
        ]
      } as any);
      expect(component['buildDialogMessage']).toHaveBeenCalledTimes(1);
      expect(infoDialogService.openInfoDialog).toHaveBeenCalledWith(
        'some string',
        'some string level name some string, level name some string.',
        undefined,
        undefined,
        undefined,
        [jasmine.any(Object)]
      );
      flush();
    }));

    it('should open default dialog because of no proper response form SideServ', fakeAsync(() => {
      freeBetsService.getBetLevelName.and.returnValue(observableOf(null));
      component.showInfoDialog({
        tokenPossibleBets: [{betId: '0', betLevel: 'betLevel'}]
      } as any);
      expect(component['buildDialogMessage']).toHaveBeenCalledTimes(1);
      expect(infoDialogService.openInfoDialog).toHaveBeenCalledWith(
        'some string',
        'some string some string some string.',
        undefined,
        undefined,
        undefined,
        [jasmine.any(Object)]
      );
      flush();
    }));

  });

  afterEach(() => {
    component = null;
  });
});
