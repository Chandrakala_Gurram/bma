import { Component, ChangeDetectionStrategy } from '@angular/core';

import {
  FreeBetSelectDialogComponent
} from '@freebets/components/freeBetSelectDialog/free-bet-select-dialog.component';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'free-bet-select-dialog',
  templateUrl: 'free-bet-select-dialog.component.html',
  styleUrls: ['free-bet-select-dialog.component.less']
})
export class LadbrokesFreeBetSelectDialogComponent extends FreeBetSelectDialogComponent {}
