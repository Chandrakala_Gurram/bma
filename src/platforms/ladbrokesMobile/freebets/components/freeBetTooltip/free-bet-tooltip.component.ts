import { Component } from '@angular/core';

import { FreeBetTooltipComponent } from '@freebets/components/freeBetTooltip/free-bet-tooltip.component';

@Component({
  selector: 'free-bet-tooltip',
  styleUrls: ['free-bet-tooltip.component.less'],
  templateUrl: 'free-bet-tooltip.component.html'
})
export class LadbrokesFreeBetTooltipComponent extends FreeBetTooltipComponent {

}
