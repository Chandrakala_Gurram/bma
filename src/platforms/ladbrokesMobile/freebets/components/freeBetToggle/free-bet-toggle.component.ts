import { Component } from '@angular/core';

import { FreeBetToggleComponent } from '@freebets/components/freeBetToggle/free-bet-toggle.component';
import { LadbrokesFreeBetSelectDialogComponent } from '../freeBetSelectDialog/free-bet-select-dialog.component';

@Component({
  selector: 'free-bet-toggle',
  templateUrl: './free-bet-toggle.component.html',
  styleUrls: ['./free-bet-toggle.component.less']
})
export class LadbrokesFreeBetToggleComponent extends FreeBetToggleComponent {
  get dialogComponent() {
    return LadbrokesFreeBetSelectDialogComponent;
  }
}
