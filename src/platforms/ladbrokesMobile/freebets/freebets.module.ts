import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ModalModule } from 'angular-custom-modal';

import { SharedModule } from '@sharedModule/shared.module';
import { LadbrokesFreeBetToggleComponent } from '@ladbrokesMobile/freebets/components/freeBetToggle/free-bet-toggle.component';
import {
  LadbrokesFreeBetSelectDialogComponent
} from '@ladbrokesMobile/freebets/components/freeBetSelectDialog/free-bet-select-dialog.component';
import { LadbrokesFreeBetTooltipComponent } from '@ladbrokesMobile/freebets/components/freeBetTooltip/free-bet-tooltip.component';
import { LadbrokesLpSpDropdownComponent } from '@ladbrokesMobile/freebets/components/lpSpDropdown/lp-sp-dropdown.component';
import { FreebetDetailsComponent } from '@freebets/components/freebetDetails/freebet-details.component';
import { FreebetsRoutingModule } from '@freebets/freebets-routing.module';
import { FreebetsComponent } from '@ladbrokesMobile/freebets/components/freebets/freebets.component';

@NgModule({
  imports: [
    SharedModule,
    ModalModule,
    FreebetsRoutingModule
  ],
  declarations: [
    FreebetsComponent,
    FreebetDetailsComponent,
    LadbrokesFreeBetToggleComponent,
    LadbrokesFreeBetSelectDialogComponent,
    LadbrokesFreeBetTooltipComponent,
    LadbrokesLpSpDropdownComponent
  ],
  exports: [],
  entryComponents: [
    FreebetsComponent,
    FreebetDetailsComponent,
    LadbrokesFreeBetToggleComponent,
    LadbrokesFreeBetSelectDialogComponent,
    LadbrokesFreeBetTooltipComponent,
    LadbrokesLpSpDropdownComponent
  ],
  providers: [],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class FreebetsModule {
  static entry = { LadbrokesFreeBetToggleComponent, LadbrokesFreeBetTooltipComponent, LadbrokesLpSpDropdownComponent };
}
