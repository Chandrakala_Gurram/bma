import { Component } from '@angular/core';
import { SportEventPageComponent as AppSportEventPageComponent } from '@edp/components/sportEventPage/sport-event-page.component';

@Component({
  selector: 'sport-event-page',
  styleUrls: [
    '../../../../../app/edp/components/sportEventPage/sport-event-page.component.less',
  ],
  templateUrl: './sport-event-page.component.html'
})
export class SportEventPageComponent extends AppSportEventPageComponent {
}
