import { Injectable } from '@angular/core';
import { ScorecastService as AppScorecastService } from '@edp/components/markets/scorecast/scorecast.service';
import { ScoreMarketService } from '@edp/services/scoreMarket/score-market.service';
import { IsPropertyAvailableService } from '@sb/services/isPropertyAvailable/is-property-available.service';
import { CashOutLabelService } from '@core/services/cashOutLabel/cash-out-label.service';
import { HttpClient } from '@angular/common/http';
import { IScorecast, IScorecastLookupTable, IScorecastLookupTablePrice } from '@edp/components/markets/scorecast/scorecast.model';
import { IOutcome } from '@core/models/outcome.model';
import { IPrice } from '@core/models/price.model';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import * as _ from 'underscore';

@Injectable()
export class ScorecastService extends AppScorecastService {
  private lookupTableData: IScorecastLookupTable;

  constructor(
    scoreMarketService: ScoreMarketService,
    isPropertyAvailableService: IsPropertyAvailableService,
    cashOutLabelService: CashOutLabelService,
    private http: HttpClient) {
    super(scoreMarketService, isPropertyAvailableService, cashOutLabelService);
  }

  getTableOddPrice(
    scorecastMarketScorecasts: IScorecast[],
    goalscorerOutcome: IOutcome,
    correctScoreOutcome: IOutcome,
    priceType: string
  ): Observable<IPrice | null> {
    return this.getLookupTable().pipe(map((data: IScorecastLookupTable) => {
      this.lookupTableData = data;
      const prices = _.filter(data[priceType], (item: IScorecastLookupTablePrice) => {
        return correctScoreOutcome.prices[0].priceDec > item.price_cs_lo
          && correctScoreOutcome.prices[0].priceDec <= item.price_cs_hi
          && goalscorerOutcome.prices[0].priceDec <= item.price_fg_hi
          && goalscorerOutcome.prices[0].priceDec > item.price_fg_lo;
      });

      return prices[0] ? {
        priceType: 'LP',
        priceNum: prices[0].price_num,
        priceDen: prices[0].price_den
      } : null;
    }));
  }

  private getLookupTable(): Observable<IScorecastLookupTable> {
    return this.lookupTableData ? of(this.lookupTableData) : this.http.get<IScorecastLookupTable>('assets/scorecast-lookup-table.json');
  }
}
