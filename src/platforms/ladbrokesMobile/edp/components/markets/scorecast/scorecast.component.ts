import { Component } from '@angular/core';
import { ScorecastComponent as AppScorecastComponent } from '@edp/components/markets/scorecast/scorecast.component';
import { FracToDecService } from '@core/services/fracToDec/frac-to-dec.service';
import { ScorecastService } from '@ladbrokesMobile/edp/components/markets/scorecast/scorecast.service';
import { BetslipSelectionsDataService } from '@core/services/betslipSelectionsData/betslip-selections-data';
import { PriceOddsButtonAnimationService } from '@shared/components/priceOddsButton/price-odds-button.animation.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { LocaleService } from '@core/services/locale/locale.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { IPrice } from '@core/models/price.model';

@Component({
  selector: 'scorecast',
  templateUrl: 'scorecast.component.html'
})
export class ScorecastComponent extends AppScorecastComponent {
  constructor(
    fracToDecFactory: FracToDecService,
    protected scorecastService: ScorecastService,
    betSlipSelectionsData: BetslipSelectionsDataService,
    priceOddsButtonService: PriceOddsButtonAnimationService,
    pubsubService: PubSubService,
    localeService: LocaleService,
    filterService: FiltersService) {
    super(
      fracToDecFactory,
      scorecastService,
      betSlipSelectionsData,
      priceOddsButtonService,
      pubsubService,
      localeService,
      filterService
    );
  }

  buildCumulativeOdd(): void {
    if (this.isTwoOutcomesSelected()) {
      const isTeamHGolscoarer = this.selectedGoalscorerTeam.name === this.teams.teamH.name;
      const teamAScore = Number(this.teams.teamA.score);
      const teamHScore = Number(this.teams.teamH.score);
      let priceType: string;

      if ((isTeamHGolscoarer && !teamHScore) || (!isTeamHGolscoarer && !teamAScore)) {
        this.cumulativeOdd = this.cumulativeOddPriceToShow = null;
        return;
      }

      if (teamHScore > teamAScore) {
        priceType = isTeamHGolscoarer ? 'W' : 'L';
      } else if (teamHScore < teamAScore) {
        priceType = isTeamHGolscoarer ? 'L' : 'W';
      } else {
        priceType = 'D';
      }

      this.scorecastService.getTableOddPrice(
        this.selectedScorecastMarket.scorecasts,
        this.selectedGoalscorerOutcome.outcome,
        this.selectedCorrectScoreOutcome.outcome,
        priceType
      ).subscribe((price: IPrice) => {
        this.cumulativeOdd = price;
        this.cumulativeOddPriceToShow = this.cumulativeOdd &&
          this.fracToDecFactory.getFormattedValue(this.cumulativeOdd.priceNum, this.cumulativeOdd.priceDen);
      });
    } else {
      this.cumulativeOdd = this.cumulativeOddPriceToShow = null;
    }
  }
}
