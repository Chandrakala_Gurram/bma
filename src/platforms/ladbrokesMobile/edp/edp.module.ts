import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from '@sharedModule/shared.module';

// Components
import { FallbackScoreboardComponent } from '@edp/components/fallbackScoreboard/fallback-scoreboard.component';
import { OptaScoreboardComponent } from '@app/edp/directives/opta-scoreboard.component';
import { MarketsGroupComponent } from '@edp/components/marketsGroup/markets-group.component';
import { CorrectScoreComponent } from '@edp/components/markets/correctScore/correct-score.component';
import { YourCallPlayerStatsComponent } from '@edp/components/markets/playerStats/your-call-player-stats.component';
import { EdpSurfaceBetsCarouselComponent } from '@edp/components/surfaceBetsCarousel/surface-bets-carousel.component';

// Services
import { SportEventPageProviderService } from '@app/edp/components/sportEventPage/sport-event-page-provider.service';
import { SportEventMainProviderService } from '@app/edp/components/sportEventMain/sport-event-main-provider.service';
import { CorrectScoreService } from '@edp/components/markets/correctScore/correct-score.service';
import { YourCallPlayerStatsGTMService } from '@edp/components/markets/playerStats/your-call-player-stats-grm.service';
import { MarketsGroupService } from '@edp/services/marketsGroup/markets-group.service';
import { ScoreMarketService } from '@edp/services/scoreMarket/score-market.service';
import { FootballExtensionService } from '@edp/services/footballExtension/football-extension.service';
import { TennisExtensionService } from '@edp/services/tennisExtension/tennis-extension.service';
import { BetGeniusScoreboardComponent } from '@edp/components/betGeniusScoreboard/bet-genius-scoreboard.component';
import { ScoreboardComponent } from '@edp/components/scoreboard/scoreboard.component';

// Overridden
import { ScorecastService } from '@ladbrokesMobile/edp/components/markets/scorecast/scorecast.service';
import { EventTitleBarComponent } from '@ladbrokesMobile/edp/components/eventTitleBar/event-title-bar.component';
import { SportEventMainComponent } from '@ladbrokesMobile/edp/components/sportEventMain/sport-event-main.component';
import { EventMarketsComponent } from '@ladbrokesMobile/edp/components/eventMarkets/event-markets.component';
import { ScorecastComponent } from '@ladbrokesMobile/edp/components/markets/scorecast/scorecast.component';
import { SportEventPageComponent } from '@ladbrokesMobile/edp/components/sportEventPage/sport-event-page.component';

@NgModule({
  imports: [
    SharedModule
  ],
  providers: [
    SportEventPageProviderService,
    SportEventMainProviderService,
    FootballExtensionService,
    TennisExtensionService,
    CorrectScoreService,
    YourCallPlayerStatsGTMService,
    ScorecastService,
    ScoreMarketService,
    MarketsGroupService
  ],
  declarations: [
    EventMarketsComponent,
    BetGeniusScoreboardComponent,
    EventTitleBarComponent,
    FallbackScoreboardComponent,
    OptaScoreboardComponent,
    ScoreboardComponent,
    SportEventMainComponent,
    SportEventPageComponent,
    MarketsGroupComponent,
    CorrectScoreComponent,
    ScorecastComponent,
    EdpSurfaceBetsCarouselComponent,
    YourCallPlayerStatsComponent
  ],
  entryComponents: [
    EventMarketsComponent,
    BetGeniusScoreboardComponent,
    EventTitleBarComponent,
    FallbackScoreboardComponent,
    OptaScoreboardComponent,
    ScoreboardComponent,
    SportEventMainComponent,
    SportEventPageComponent,
    MarketsGroupComponent,
    CorrectScoreComponent,
    ScorecastComponent,
    EdpSurfaceBetsCarouselComponent,
    YourCallPlayerStatsComponent
  ],
  exports: [
    EventMarketsComponent,
    BetGeniusScoreboardComponent,
    EventTitleBarComponent,
    FallbackScoreboardComponent,
    OptaScoreboardComponent,
    ScoreboardComponent,
    SportEventMainComponent,
    EdpSurfaceBetsCarouselComponent,
    SportEventPageComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class EdpModule {
  static entry = SportEventMainComponent;
}
