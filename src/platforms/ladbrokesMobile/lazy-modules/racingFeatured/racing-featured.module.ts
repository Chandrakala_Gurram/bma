import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { RacingFeaturedComponent } from './components/racingFeatured/racing-featured.component';
import { InspiredVirtualComponent } from '@app/lazy-modules/racingFeatured/components/inspiredVirtual/inspired-virtual.component';
import {
  LadbrokesHorseRaceGridComponent as HorseRaceGridComponent
} from './components/horseRaceGrid/horse-race-grid.component';

import { LadbrokesRacingPoolIndicatorComponent } from './components/racingPoolIndicator/racing-pool-indicator.component';

import { RacingEventsComponent } from './components/racingEvents/racing-events.component';
import { DailyRacingModuleComponent } from '@app/racing/components/dailyRacing/daily-racing.component';
// tslint:disable: max-line-length
import { LadbrokesOffersAndFeaturedRacesComponent } from '@ladbrokesMobile/racing/components/offersAndFeaturedRaces/offers-and-featured-races.component';

import { SharedModule } from '@sharedModule/shared.module';

@NgModule({
  imports: [
    SharedModule
  ],
  providers: [],
  exports: [],
  declarations: [
    RacingFeaturedComponent,
    InspiredVirtualComponent,
    HorseRaceGridComponent,
    LadbrokesRacingPoolIndicatorComponent,
    RacingEventsComponent,
    DailyRacingModuleComponent,
    LadbrokesOffersAndFeaturedRacesComponent
  ],
  entryComponents: [
    RacingFeaturedComponent
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class RacingFeaturedModule {
  static entry = RacingFeaturedComponent;
}
