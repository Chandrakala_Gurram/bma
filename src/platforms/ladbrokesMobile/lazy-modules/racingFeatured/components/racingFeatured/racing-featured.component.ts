import { Component } from '@angular/core';

import {
  RacingFeaturedComponent as CoralRacingFeaturedComponent
} from '@root/app/lazy-modules/racingFeatured/components/racingFeatured/racing-featured.component';

@Component({
  selector: 'racing-featured',
  templateUrl: './racing-featured.component.html',
  styleUrls: ['./racing-featured.component.less']
})
export class RacingFeaturedComponent extends CoralRacingFeaturedComponent {}
