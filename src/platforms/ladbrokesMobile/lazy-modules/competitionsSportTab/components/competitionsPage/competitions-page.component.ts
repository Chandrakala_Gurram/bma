import { Component } from '@angular/core';
import { CompetitionsPageComponent } from '@lazy-modules/competitionsSportTab/components/competitionsPage/competitions-page.component';

@Component({
  selector: 'competitions-page',
  templateUrl: 'competitions-page.component.html',
  styleUrls: ['./competitions-page.component.less']
})
export class LadbrokesCompetitionsPageComponent extends CompetitionsPageComponent {}
