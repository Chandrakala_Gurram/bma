import { Component } from '@angular/core';
import {
  CompetitionsStandingsTabComponent
} from '@lazy-modules/competitionsSportTab/components/competitionsPage/competitionsPageTabs/competitions-standings-tab.component';

@Component({
  selector: 'competitions-standings-tab',
  // tslint:disable-next-line
  templateUrl: '../../../../../../../app/lazy-modules/competitionsSportTab/components/competitionsPage/competitionsPageTabs/competitions-standings-tab.component.html',
  styleUrls: [
    // tslint:disable-next-line
    '../../../../../../../app/lazy-modules/competitionsSportTab/components/competitionsPage/competitionsPageTabs/competitions-standings-tab.component.less',
    './competitions-standings-tab.component.less']
})
export class LadbrokesCompetitionsStandingsTabComponent extends CompetitionsStandingsTabComponent {}
