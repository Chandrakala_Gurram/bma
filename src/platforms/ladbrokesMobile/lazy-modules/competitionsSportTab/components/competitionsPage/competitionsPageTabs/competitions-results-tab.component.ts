import { Component } from '@angular/core';
// tslint:disable-next-line
import { CompetitionsResultsTabComponent } from '@lazy-modules/competitionsSportTab/components/competitionsPage/competitionsPageTabs/competitions-results-tab.component';

@Component({
  selector: 'competitions-results-tab',
  // tslint:disable-next-line
  templateUrl: '../../../../../../../app/lazy-modules/competitionsSportTab/components/competitionsPage/competitionsPageTabs/competitions-results-tab.component.html',
  styleUrls: ['competitions-results-tab.component.less']
})
export class LadbrokesCompetitionsResultsTabComponent extends CompetitionsResultsTabComponent {}
