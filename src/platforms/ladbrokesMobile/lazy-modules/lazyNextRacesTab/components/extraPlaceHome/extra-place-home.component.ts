import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ExtraPlaceHomeComponent } from '@lazy-modules/lazyNextRacesTab/components/extraPlaceHome/extra-place-home.component';

@Component({
  selector: 'extra-place-home-module',
  templateUrl: 'extra-place-home.component.html',
  styleUrls: [
    '../../../../../../app/lazy-modules/lazyNextRacesTab/components/extraPlaceHome/extra-place-home.component.less',
    './extra-place-home.component.less'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LadbrokesExtraPlaceHomeComponent extends ExtraPlaceHomeComponent {}
