import * as _ from 'underscore';

import { Component, OnInit } from '@angular/core';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { EventService } from '@sb/services/event/event.service';
import { NextRacesHomeService } from '@ladbrokesMobile/lazy-modules/lazyNextRacesTab/components/nextRacesHome/next-races-home.service';
import { RacingPostService } from '@core/services/racing/racingPost/racing-post.service';
import {
  NextRacesHomeComponent as CoralNextRacesHomeComponent
} from '@lazy-modules/lazyNextRacesTab/components/nextRacesHome/next-races-home.component';
import { ICombinedRacingConfig, ISystemConfig } from '@core/services/cms/models/system-config';
import { UpdateEventService } from '@core/services/updateEvent/update-event.service';

@Component({
  selector: 'next-races-home',
  templateUrl: 'next-races-home.component.html'
})
export class NextRacesHomeComponent extends CoralNextRacesHomeComponent implements  OnInit {
  constructor(
    public pubSubService: PubSubService,
    public cmsService: CmsService,
    public nextRacesHomeService: NextRacesHomeService,
    public eventService: EventService,
    public racingPostService: RacingPostService,
    protected updateEventService: UpdateEventService
  ) {
    super(
      pubSubService,
      cmsService,
      nextRacesHomeService,
      eventService,
      racingPostService,
      updateEventService
      );
  }

  /**
   * Init function for(callbacks, watchers, scope destroying)
   * @private
   */
  ngOnInit(): void {
    super.ngOnInit();
  }

  /**
   * Get data from Cms config
   */
  getCmsConfigs(): void {
    let storedConfig: ICombinedRacingConfig;

    /* tslint:disable-next-line:pubsub-connect */
    this.pubSubService.subscribe(this.MODULE_NAME, this.pubSubService.API.SYSTEM_CONFIG_UPDATED, (data: ISystemConfig) => {
      const updatedConfig: ICombinedRacingConfig = {
        NextRaces: data.NextRaces,
        RacingDataHub: data.RacingDataHub,
        GreyhoundNextRaces: data.GreyhoundNextRaces
      };

      if (updatedConfig.NextRaces && !_.isEqual(updatedConfig, storedConfig)) {
        storedConfig = updatedConfig;
        this.moduleTitle = updatedConfig.NextRaces.title;
        // All nextRacesModule configuration
        this.nextRacesModule = this.nextRacesHomeService.getNextRacesModuleConfig(this.moduleType, updatedConfig);
        this.leftTitleText = this.moduleTitle;
        this.getNextEvents();
      }
    });
  }
}
