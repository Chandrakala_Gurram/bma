import { NextRacesHomeComponent } from './next-races-home.component';
import { fakeAsync, tick } from '@angular/core/testing';
import { of as observableOf } from 'rxjs';

describe('LadbrokesNextRacesHomeComponent', () => {
  let component: NextRacesHomeComponent;
  let nextRacesHomeService;
  let pubSubService;
  let cmsService;
  let eventService;
  let racingPostService;
  let cmsData;
  let updateEventService;

  beforeEach(fakeAsync(() => {
    pubSubService = {
      subscribe: jasmine.createSpy('subscribe').and.callFake(((a, b, cb) => cb && cb(cmsData))),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: {
        SYSTEM_CONFIG_UPDATED: 'SYSTEM_CONFIG_UPDATED',
        RELOAD_COMPONENTS: 'RELOAD_COMPONENTS'
      }
    };
    eventService = {
      getNextEvents: jasmine.createSpy('getNextEvents').and.returnValue(Promise.resolve([{ id: 1 }, { id: 2 }]))
    };
    nextRacesHomeService = {
      unSubscribeForUpdates: jasmine.createSpy('unSubscribeForUpdates'),
      subscribeForUpdates: jasmine.createSpy('subscribeForUpdates'),
      getUpdatedEvents: jasmine.createSpy('getUpdatedEvents'),
      getNextRacesModuleConfig: jasmine.createSpy('getNextRacesModuleConfig').and.returnValue({}),
      sendGTM: jasmine.createSpy('sendGTM')
    };
    cmsService = {
      triggerSystemConfigUpdate: jasmine.createSpy('triggerSystemConfigUpdate')
    };
    racingPostService = {
      updateRacingEventsList: jasmine.createSpy('updateRacingEventsList').and.returnValue(
        observableOf([{ id: 1, racingPostEvent: {} }, { id: 2, racingPostEvent: {} }]))
    };
    cmsData = {
      NextRaces: { title: 'Next races', numberOfSelections: '10' },
      GreyhoundNextRaces: { numberOfSelections: '4' },
      RacingDataHub: { isEnabledForGreyhound: true }
    };
    updateEventService = {};

    component = new NextRacesHomeComponent(
      pubSubService,
      cmsService,
      nextRacesHomeService,
      eventService,
      racingPostService,
      updateEventService
    );

    component.raceModule = 'NEXT_RACE';
    component.moduleType = 'horseracing';
    component.nextRacesModule = {};
  }));

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  it('#ngOnInit', () => {
    component.ngOnInit();

    expect(component.raceModule).toEqual('NEXT_RACE');
    expect(component.className).toEqual('next-races-horseracing');
    expect(component.showTimer).toEqual(true);
    expect(component.raceEvent).toEqual('Horse Racing');
    expect(cmsService.triggerSystemConfigUpdate).toHaveBeenCalled();
    expect(pubSubService.subscribe).toHaveBeenCalledTimes(3);
  });

  it('#ngOnDestroy', () => {
    component.ngOnDestroy();

    expect(nextRacesHomeService.unSubscribeForUpdates).toHaveBeenCalled();
    expect(pubSubService.unsubscribe).toHaveBeenCalledWith('MODULE_NEXT_RACE');
  });

  it('#sendToGTM', () => {
    component.sendToGTM();

    expect(nextRacesHomeService.sendGTM).toHaveBeenCalledWith('view all', 'home');
  });

  describe('getCmsConfigs method', function () {
    let pubSubFn;

    beforeEach(() => {
      spyOn(component, 'getNextEvents');
      pubSubService.subscribe.and.callFake((subscriberName, command, cb) => pubSubFn = cb);
    });

    it('should subscribe to pubsub SYSTEM_CONFIG_UPDATED', () => {
      component.getCmsConfigs();
      expect(pubSubService.subscribe).toHaveBeenCalledWith('MODULE_NEXT_RACE', 'SYSTEM_CONFIG_UPDATED', jasmine.any(Function));
    });

    it('should execute nextRacesHomeService.getNextRacesModuleConfig and getNextEvents methods and update props on subscription', () => {
      component.getCmsConfigs();
      pubSubFn(cmsData);
      expect(component.moduleTitle).toEqual('Next races');
      expect(component.leftTitleText).toEqual('Next races');
      expect(nextRacesHomeService.getNextRacesModuleConfig).toHaveBeenCalledWith('horseracing',
        {
          NextRaces: { title: 'Next races', numberOfSelections: '10' },
          GreyhoundNextRaces: { numberOfSelections: '4' },
          RacingDataHub: { isEnabledForGreyhound: true}
        });
      expect(component.getNextEvents).toHaveBeenCalled();
    });

    it('should execute nextRacesHomeService.getNextRacesModuleConfig and getNextEvents methods and update props on subscription', () => {
      delete cmsData.RacingDataHub;
      component.getCmsConfigs();
      pubSubFn(cmsData);
      expect(nextRacesHomeService.getNextRacesModuleConfig).toHaveBeenCalledWith('horseracing',
      {
        NextRaces: { title: 'Next races', numberOfSelections: '10' },
        GreyhoundNextRaces: { numberOfSelections: '4' },
        RacingDataHub: undefined
      });
      expect(component.getNextEvents).toHaveBeenCalled();
    });

    describe('should not call nextRacesHomeService.getNextRacesModuleConfig and getNextEvents methods', () => {
      it('when cms config does not contain NextRaces entry', () => {
        cmsData = {};
        component.getCmsConfigs();
        pubSubFn(cmsData);
      });

      it('when called more than once with the same config', () => {
        component.getCmsConfigs();
        pubSubFn(cmsData);
        nextRacesHomeService.getNextRacesModuleConfig.calls.reset();
        (component.getNextEvents as any).calls.reset();
        pubSubFn(cmsData);
      });
      afterEach(() => {
        expect(component.getNextEvents).not.toHaveBeenCalled();
        expect(nextRacesHomeService.getNextRacesModuleConfig).not.toHaveBeenCalled();
      });
    });
  });

  describe('getNextEvents method', () => {
    it('should call racingPostService.updateRacingEventsList method', fakeAsync(() => {
      component.getNextEvents();
      tick();
      expect(racingPostService.updateRacingEventsList).toHaveBeenCalledWith([{ id: 1 }, { id: 2 }], true);
      expect(nextRacesHomeService.getUpdatedEvents).toHaveBeenCalledWith(
        [{ id: 1, racingPostEvent: { } }, { id: 2, racingPostEvent: { } }], 'horseracing');
    }));
  });

  it('#registerEvents', () => {
    component.raceModule = 'NEXT_RACE';
    component.registerEvents();

    expect(pubSubService.subscribe).toHaveBeenCalledTimes(2);
    expect(pubSubService.subscribe).toHaveBeenCalledWith('MODULE_NEXT_RACE', 'RELOAD_COMPONENTS', jasmine.any(Function));
  });
});
