import { oddsboost as baseOddsBoost } from '@app/lazy-modules/locale/translations/en-US/oddsBoost.lang';

export const oddsboost = {
  ...baseOddsBoost,
  infoDialog: {
    title: 'Odds Boost',
    text: 'Hit Boost to increase the odds of the bets in your betslip!<br><br>You can boost up to %2%1 total stake.',
    oddsBoostUnavailable: 'Odds Boost is unavailable for this selection'
  },
  tokensInfoDialog: {
    title: 'Odds Boost',
    available1: 'You have ',
    available2: ' odds %1 available',
    boost: 'boost',
    boosts: 'boosts',
    terms: '18+, Terms Available',
    showMore: 'Show more',
    okThanks: 'Ok, thanks',
    dontShowThisAgain: 'Don\'t show this again'
  },
};
