import { racing as appRacing } from '@app/lazy-modules/locale/translations/en-US/racing.lang';

export const racing = {
  ...appRacing,
  racingPostStarRating: 'Racing Post STAR RATING',
  racingPostTips: 'Racing Post Tips',
  internationalToteTitle: 'International Tote',
  extraPlaceTitle: 'Extra Place Offers'
};

