import { quickbet as appQuickbet } from '@app/lazy-modules/locale/translations/en-US/quickbet.lang';

export const quickbet = {
  ...appQuickbet,
  tax5: 'A fee of 5.00 % is applicable on winnings',
  potentialResults: 'Total Potential Returns',
  totalStake: 'Stake for this bet: ',
  potentialReturns: 'Potential Returns',
  potentialReturnsWithColon: 'Potential Returns: ',
  betId: 'Receipt No: ',
  bybType: {
    fiveASide: '5-A-Side',
    byb: 'Bet Builder'
  }
};
