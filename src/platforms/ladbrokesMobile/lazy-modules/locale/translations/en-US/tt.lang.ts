import { tt as appTt } from '@app/lazy-modules/locale/translations/en-US/tt.lang';

export const tt = {
  ...appTt,
  // tslint:disable-next-line: max-line-length
  bettingRulesMsg: 'All bets are accepted with the <a href="https://help.ladbrokes.com/s/"">Ladbrokes Betting Rules</a> as published on this site.'
};
