import { retail as appRetail } from '@app/lazy-modules/locale/translations/en-US/retail.lang';

export const retail = {
  ...appRetail,
  title: 'The Grid',
};
