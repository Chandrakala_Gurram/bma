import { bethistory as appBethistory } from '@app/lazy-modules/locale/translations/en-US/bethistory.lang';

export const bethistory = {
  ...appBethistory,
  startBetting: 'Go betting',
  noCashoutBets: 'You currently have no bets available for cash out.',
  totalReturn: 'Potential Returns:',
  newTotalReturn: 'New Potential Returns: ',
  bybHeader: { byb: 'Bet Builder', fiveASide: '5-A-Side' }
};
