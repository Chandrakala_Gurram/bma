import { Component } from '@angular/core';

import {
  VideoStreamErrorDialogComponent as VideoStreamErrorDialogBaseComponent
} from '@lazy-modules/eventVideoStream/components/videoStreamErrorDialog/video-stream-error-dialog.component';

@Component({
  selector: 'video-stream-error-dialog',
  styleUrls: ['./video-stream-error-dialog.component.less'],
  // tslint:disable-next-line
  templateUrl: '../../../../../../app/lazy-modules/eventVideoStream/components/videoStreamErrorDialog/video-stream-error-dialog.component.html'
})
export class VideoStreamErrorDialogComponent extends VideoStreamErrorDialogBaseComponent {
}
