import { Component, AfterViewInit, ApplicationRef, NgZone } from '@angular/core';
import environment from '@environment/oxygenEnvConfig';
import {
  VanillaPortalModuleLoadNotifier
} from '@vanillaInitModule/services/vanillaPortalModuleLoadNotifier/vanilla-portal-module-load-notifier.service';
import { PubSubService } from '@root/app/core/services/communication/pubsub/pubsub.service';
import decorateTick from '@root/platforms/app-decorator';
import { Router } from '@angular/router';

@Component({
  selector: 'root-app',
  templateUrl: './app.component.html'
})
export class RootComponent implements AfterViewInit {
  isProduction = environment.production;

  constructor(
    private vanillaPortalModuleLoadNotifier: VanillaPortalModuleLoadNotifier,
    protected pubsub: PubSubService,
    private applicationRef: ApplicationRef,
    private ngZone: NgZone,
    private pubSubService: PubSubService,
    private router: Router
  ) {
    decorateTick(this.applicationRef, this.ngZone, this.pubSubService, this.router);
  }

  ngAfterViewInit(): void {
    this.vanillaPortalModuleLoadNotifier.loadPortalModule();
  }
}
