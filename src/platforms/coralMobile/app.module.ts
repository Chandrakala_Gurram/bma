import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule, NgModuleFactory, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { STORE_PREFIX, runOnAppInit, VanillaCoreModule } from '@vanilla/core/core';
import { VanillaCommonModule } from '@vanilla/core/common';
import { VanillaFeaturesModule, AppComponent } from '@vanilla/core/features';
import { LabelHostFeaturesModule, LH_HTTP_ERROR_HANDLERS } from '@labelhost/core/features';

import { AppRoutingModule } from '@coralMobile/app-routing.module';
import { RootComponent } from '@coralMobile/app.component';
import { DynamicLoaderModule } from '@app/dynamicLoader/dynamic-loader.module';
import { lazyComponentsManifests } from '@coralMobile/lazy-components.manifests';
import { VanillaInitModule } from '@vanillaInitModule/vanilla-init.module';
import { LabelHostHttpErrorHandler } from '@vanillaInitModule/services/labelHostErrorHandler/labelHostHttpErrorHandler.service';
import { HostAppBootstrapper } from '@app/host-app/host-app-bootstrapper.service';

import { SharedModule } from '@sharedModule/shared.module';
import { BmaModule } from '@bmaModule/bma.module';
import { CoreModule } from '@coreModule/core.module';
import { SbModule } from '@sbModule/sb.module';
import { RoutingState } from '@sharedModule/services/routingState/routing-state.service';
import { CoralSportsClientConfigModule } from '@app/client-config/client-config.module';
import { NativeBridgeBootstrapperService } from '@vanillaInitModule/services/NativeBridgeBootstrapper/native-bridge-bootstrapper.service';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    VanillaCoreModule.forRoot(),
    VanillaCommonModule.forRoot(),
    VanillaFeaturesModule.forRoot(),
    LabelHostFeaturesModule.forRoot(),
    CoreModule,
    VanillaInitModule,
    AppRoutingModule,
    SharedModule.forRoot(),
    BmaModule,
    SbModule,
    DynamicLoaderModule.forRoot(lazyComponentsManifests),
    CoralSportsClientConfigModule.forRoot()
  ],
  declarations: [
    RootComponent
  ],
  providers: [
    { provide: STORE_PREFIX, useValue: 'coralsports.' },
    { provide: LH_HTTP_ERROR_HANDLERS, useClass: LabelHostHttpErrorHandler, multi: true },
    runOnAppInit(HostAppBootstrapper)
  ],
  bootstrap: [
    AppComponent
  ],
  entryComponents: [
    RootComponent
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {
  constructor(private loader: NgModuleFactoryLoader,
    private injector: Injector,
    private routingState: RoutingState,
    private nativeBridgeBootstrapper: NativeBridgeBootstrapperService
  ) {
    this.routingState.loadRouting();
    const paths = ['src/app/lazy-modules/locale/translation.module#TranslationModule',
      'src/app/lazy-modules/performanceMark/performanceMark.module#PerformanceMarkModule'];
    paths.forEach((path) => {
      this.loader
        .load(path)
        .then((moduleFactory: NgModuleFactory<any>) => moduleFactory.create(this.injector));
    });
    // Attaches Native Bridge Adapter and Portal Native Event Notifier.
    this.nativeBridgeBootstrapper.init();
  }
}
