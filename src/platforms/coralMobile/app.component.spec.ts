import { async } from '@angular/core/testing';
import { RootComponent } from './app.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('AppComponent', () => {
  let component: RootComponent;
  let vanillaPortalModuleLoadNotifier;
  let pubsub;
  let appRef;
  let ngZone;
  let pubSubService;
  let router;

  beforeEach(async(() => {
    router = {
      events: {
        subscribe: jasmine.createSpy('subscribe')
      }
    };

    pubSubService = {
      subscribe: jasmine.createSpy('subscribe'),
      API: pubSubApi
    };

    vanillaPortalModuleLoadNotifier = {
      loadPortalModule: jasmine.createSpy()
    };

    pubsub = {
      publishSync: jasmine.createSpy(),
      API: jasmine.createSpy()
    };

    appRef = {};
    ngZone = {};

    component = new RootComponent(
      vanillaPortalModuleLoadNotifier,
      pubsub,
      appRef,
      ngZone,
      pubSubService,
      router
    );
  }));

  describe('ngAfterViewInit', () => {

    it('should call loadPortalModule', () => {
      component.ngAfterViewInit();
      expect(vanillaPortalModuleLoadNotifier.loadPortalModule).toHaveBeenCalled();
    });
  });
});
