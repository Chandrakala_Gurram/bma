import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { ModalModule } from 'angular-custom-modal';
import { SharedModule } from '@sharedModule/shared.module';

import {
  DYNAMIC_ODDS_BOOST_BS_HEADER,
  DYNAMIC_ODDS_BOOST_PRICE,
  DYNAMIC_ODDS_BOOST_INFO_ICON,
  DYNAMIC_ODDS_BOOST_BUTTON,
  DYNAMIC_ODDS_BOOST_NOTIFICATION
} from '@app/dynamicLoader/dynamic-loader-manifest';

import { OddsBoostService } from './services/odds-boost.service';
import { OddsBoostRunService } from '@oddsBoost/services/odds-boost-run.service';
import { OddsBoostPriceService } from '@oddsBoost/services/odds-boost-price.service';
import { OddsBoostRoutingModule } from '@oddsBoost/odds-boost-routing.module';

import { LadbrokesOddsBoostButtonComponent } from '@ladbrokesMobile/oddsBoost/components/oddsBoostButton/odds-boost-button.component';
import {
  LadbrokesOddsBoostBetslipHeaderComponent
} from '@ladbrokesMobile/oddsBoost/components/oddsBoostBetslipHeader/odds-boost-betslip-header.component';
import { LadbrokesOddsBoostPriceComponent } from '@ladbrokesMobile/oddsBoost/components/oddsBoostPrice/odds-boost-price.component';
import { OddsBoostPageComponent } from '@oddsBoost/components/oddsBoostPage/odds-boost-page.component';
import { OddsBoostListComponent } from '@oddsBoost/components/oddsBoostList/odds-boost-list.component';
import { OddsBoostInfoIconComponent } from '@oddsBoost/components/odds-boost-info-icon/odds-boost-info-icon.component';
import { OddsBoostUpcomingHeaderComponent } from '@oddsBoost/components/oddsBoostUpcomingHeader/odds-boost-upcoming-header.component';
import {
  OddsBoostCountNotificationComponent
} from '@oddsBoost/components/oddsBoostCountNotification/odds-boost-count-notification.component';

@NgModule({
  imports: [
    SharedModule,
    ModalModule,
    OddsBoostRoutingModule
  ],
  exports: [],
  providers: [
    OddsBoostService,
    OddsBoostRunService,
    OddsBoostPriceService,
    { provide: DYNAMIC_ODDS_BOOST_BS_HEADER, useValue: LadbrokesOddsBoostBetslipHeaderComponent },
    { provide: DYNAMIC_ODDS_BOOST_PRICE, useValue: LadbrokesOddsBoostPriceComponent },
    { provide: DYNAMIC_ODDS_BOOST_INFO_ICON, useValue: OddsBoostInfoIconComponent },
    { provide: DYNAMIC_ODDS_BOOST_BUTTON, useValue: LadbrokesOddsBoostButtonComponent },
    { provide: DYNAMIC_ODDS_BOOST_NOTIFICATION, useValue: OddsBoostCountNotificationComponent }
  ],
  declarations: [
    LadbrokesOddsBoostButtonComponent,
    LadbrokesOddsBoostBetslipHeaderComponent,
    LadbrokesOddsBoostPriceComponent,
    OddsBoostPageComponent,
    OddsBoostListComponent,
    OddsBoostInfoIconComponent,
    OddsBoostUpcomingHeaderComponent,
    OddsBoostCountNotificationComponent
  ],
  entryComponents: [
    LadbrokesOddsBoostBetslipHeaderComponent,
    LadbrokesOddsBoostButtonComponent,
    OddsBoostPageComponent,
    LadbrokesOddsBoostPriceComponent,
    OddsBoostListComponent,
    OddsBoostInfoIconComponent,
    OddsBoostUpcomingHeaderComponent,
    OddsBoostCountNotificationComponent
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class OddsBoostModule {
  constructor(private oddsBoostRunService: OddsBoostRunService) {
    this.oddsBoostRunService.run();
  }
}
