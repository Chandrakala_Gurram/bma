import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { DesktopModule } from '@desktop/desktop.module';
import { SharedModule } from '@sharedModule/shared.module';
import { SbModule } from '@sbModule/sb.module';
import { OlympicsRunService } from '@app/olympics/services/olympics-run.service';
import { BannersModule } from '@banners/banners.module';

// Overridden app components
import { OlympicsPageComponent } from '@ladbrokesDesktop/olympics/components/olympicsPage/olympics-page.component';

@NgModule({
  imports: [
    SbModule,
    SharedModule,
    DesktopModule,
    BannersModule
  ],
  declarations: [
    // Overridden app components
    OlympicsPageComponent
  ],
  exports: [
    // Overridden app components
    OlympicsPageComponent
  ],
  entryComponents: [
    // Overridden app components
    OlympicsPageComponent
  ],
  providers: [
    OlympicsRunService
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class OlympicsModule {
  constructor(private olympicsRunService: OlympicsRunService) {
    this.olympicsRunService.run();
  }
}
