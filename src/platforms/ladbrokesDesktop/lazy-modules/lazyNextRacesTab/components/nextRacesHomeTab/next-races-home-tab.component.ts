import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NextRacesHomeTabComponent } from '@lazy-modules/lazyNextRacesTab/components/nextRacesHomeTab/next-races-home-tab.component';

@Component({
  selector: 'next-races-home-tab',
  templateUrl: 'next-races-home-tab.component.html'
})
export class LadbrokesNextRacesHomeTabComponent extends NextRacesHomeTabComponent implements OnInit {
  extraPlaceLimit: number = 1;
  moduleType: string;

  constructor(protected route: ActivatedRoute) {
    super();
  }

  ngOnInit(): void {
    this.setModuleType();
  }

  private setModuleType(): void {
    this.moduleType = this.route.snapshot.data['segment'] && this.route.snapshot.data['segment'] === 'greyhound.nextRaces'
      ? 'greyhound'
      : 'horseracing';
  }
}
