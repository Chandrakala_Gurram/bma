import { Component } from '@angular/core';
import { RunnerSpotlightComponent } from '@lazy-modules/runnerSpotlight/runner-spotlight.component';

@Component({
  selector: 'runner-spotlight',
  templateUrl: './runner-spotlight.html',
  styleUrls: ['runner-spotlight.component.less'],
})
export class DesktopRunnerSpotlightComponent extends RunnerSpotlightComponent {

}
