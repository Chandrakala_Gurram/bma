import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'angular-custom-modal';

import { LocaleService } from '@core/services/locale/locale.service';
import * as sbLangData from '@localeModule/translations/en-US/sb.lang';
import * as sbDesktopLangData from '@localeModule/translations/en-US/sbdesktop.lang';

import { DesktopModule } from '@desktop/desktop.module';
import { SharedModule } from '@sharedModule/shared.module';
import { CouponsListService } from '@sb/components/couponsList/coupons-list.service';
import { CouponsListComponent } from '@app/sb/components/couponsList/coupons-list.component';
import { GoalscorerCouponComponent } from '@sbModule/components/goalscorerCoupon/goalscorer-coupon.component';
import { GoalscorerCouponService } from '@app/sb/components/goalscorerCoupon/goalscorer-coupon.service';
import { CouponsDetailsService } from '@sb/components/couponsDetails/coupons-details.service';
import { OutcomeTemplateHelperService } from '@app/sb/services/outcomeTemplateHelper/outcome-template-helper.service';
import { BuildYourBetTabComponent } from '@app/sb/components/buildYourBetTab/build-your-bet-tab.component';
import { SportEventComponent } from '@app/sb/components/sportEvent/sport-event.component';
import { BuildYourBetHomeComponent } from '@app/sb/components/buildYourBetHome/build-your-bet-home.component';
import { LuckyDipDialogComponent } from '@app/sb/components/jackpotSportTab/luckyDipDialog/lucky-dip-dialog.component';
import { JackpotReceiptPageComponent } from '@app/sb/components/jackpotReceiptPage/jackpot-receipt-page.component';
import { HowToPlayDialogComponent } from '@app/sb/components/jackpotSportTab/howToPlayDialog/how-to-play-dialog.component';

import { JackpotSportTabComponent } from '@app/sb/components/jackpotSportTab/jackpot-sport-tab.component';
import { CouponsContentSportTabComponent } from '@app/sb/components/couponsContentSportTab/coupons-content-sport-tab.component';
import { CouponsListSportTabComponent } from '@app/sb/components/couponsListSportTab/coupons-list-sport-tab.component';
import { EnhancedMultiplesTabComponent } from '@sbModule/components/enhancedMultiplesModule/enhanced-multiples-tab.component';
import { PrivateMarketsTabComponent } from '@ladbrokesMobile/sb/components/privateMarketsTab/private-markets-tab.component';
import {
  PrivateMarketsTermsAndConditionsComponent
} from '@app/sb/components/privateMarketsTab/private-markets-terms-and-conditions.component';
import { SpecialsSportTabComponent } from '@app/sb/components/specialsSportTab/specials-sport-tab.component';
import { SbFiltersService } from '@app/sb/services/sbFilters/sb-filters.service';
import { CorrectScoreCouponService } from '@sb/components/correctScoreCoupon/correct-score-coupon.service';
import { BannersModule } from '@banners/banners.module';
import { SmartBoostsService } from '@sb/services/smartBoosts/smart-boosts.service';

/* tslint:disable */
import { SportTabsService } from '@app/sb/services/sportTabs/sport-tabs.service';
import { JackpotReceiptPageService } from '@app/sb/components/jackpotReceiptPage/jackpot-receipt-page.service';
import { JackpotSportTabService } from '@app/sb/components/jackpotSportTab/jackpot-sport-tab.service';
import { CurrentMatchesService } from '@app/sb/services/currentMatches/current-matches.service';
import { DividendsService } from '@app/sb/services/dividents/dividends.service';
import { EnhancedMultiplesService } from '@app/sb/services/enhancedMultiples/enhanced-multiples.service';
import { EventService } from '@app/sb/services/event/event.service';
import { EventFiltersService } from '@app/sb/services/eventFilters/event-filters.service';
import { EventsByClassesService } from '@app/sb/services/eventsByClasses/events-by-classes.service';
import { IsPropertyAvailableService } from '@app/sb/services/isPropertyAvailable/is-property-available.service';
import { LiveStreamService } from '@app/sb/services/liveStream/live-stream.service';
import { MarketSortService } from '@app/sb/services/marketSort/market-sort.service';
import { StreamTrackingService } from '@app/sb/services/streamTracking/stream-tracking.service';

import { OlympicsService } from '@sb/services/olympics/olympics.service';
import { MatchResultsSportTabComponent } from '@app/sb/components/matchResultsSportTab/match-results-sport-tab.component';
// Overridden app components
import { SportMainComponent } from '@ladbrokesDesktop/sb/components/sportMain/sport-main.component';
import { DesktopSportMatchesPageComponent } from '@ladbrokesDesktop/sb/components/sportMatchesPage/sport-matches-page.component';
import { CouponsDetailsComponent } from '@ladbrokesDesktop/sb/components/couponsDetails/coupons-details.component';
import { DesktopSportMatchesTabComponent } from '@ladbrokesDesktop/sb/components/sportMatchesTab/sport-matches-tab.component';
import { CorrectScoreCouponComponent } from '@ladbrokesDesktop/sb/components/correctScoreCoupon/correct-score-coupon.component';
import { SportTabsPageComponent } from '@sbModule/components/SportTabsPage/sport-tabs-page.component';
import { OutrightsSportTabComponent } from '@ladbrokesDesktop/sb/components/outrightsSportTab/outrights-sport-tab.component';

@NgModule({
  imports: [
    HttpClientModule,
    DesktopModule,
    FormsModule,
    ModalModule,
    SharedModule,
    BannersModule
  ],
  providers: [
    JackpotSportTabService,
    CouponsListService,
    JackpotReceiptPageService,
    EventFiltersService,
    EnhancedMultiplesService,
    IsPropertyAvailableService,
    DividendsService,
    EventsByClassesService,
    LiveStreamService,
    StreamTrackingService,
    MarketSortService,
    SportTabsService,
    OutcomeTemplateHelperService,
    EventService,
    CurrentMatchesService,
    SbFiltersService,
    DatePipe,
    CorrectScoreCouponService,
    CouponsDetailsService,
    GoalscorerCouponService,
    SmartBoostsService,
    OlympicsService
  ],
  declarations: [
    // Overridden app components
    SportMainComponent,
    DesktopSportMatchesPageComponent,
    CouponsDetailsComponent,
    DesktopSportMatchesTabComponent,
    CorrectScoreCouponComponent,

    SportEventComponent,
    SportTabsPageComponent,
    JackpotReceiptPageComponent,
    OutrightsSportTabComponent,
    LuckyDipDialogComponent,
    HowToPlayDialogComponent,
    JackpotSportTabComponent,
    CouponsListSportTabComponent,
    CouponsContentSportTabComponent,
    SpecialsSportTabComponent,
    EnhancedMultiplesTabComponent,
    BuildYourBetTabComponent,
    PrivateMarketsTabComponent,
    PrivateMarketsTermsAndConditionsComponent,
    CouponsListComponent,
    GoalscorerCouponComponent,
    MatchResultsSportTabComponent,
    BuildYourBetHomeComponent
  ],
  entryComponents: [
    // Overridden app components
    SportMainComponent,
    DesktopSportMatchesPageComponent,
    CouponsDetailsComponent,
    DesktopSportMatchesTabComponent,
    CorrectScoreCouponComponent,
    CouponsContentSportTabComponent,
    LuckyDipDialogComponent,
    JackpotReceiptPageComponent,
    HowToPlayDialogComponent,
    JackpotSportTabComponent,
    SportTabsPageComponent,
    CouponsListSportTabComponent,
    OutrightsSportTabComponent,
    SpecialsSportTabComponent,
    EnhancedMultiplesTabComponent,
    BuildYourBetTabComponent,
    PrivateMarketsTabComponent,
    PrivateMarketsTermsAndConditionsComponent,
    CouponsListComponent,
    GoalscorerCouponComponent,
    BuildYourBetHomeComponent
  ],
  exports: [
    // Overridden app components
    SportMainComponent,
    DesktopSportMatchesPageComponent,
    CouponsDetailsComponent,
    DesktopSportMatchesTabComponent,
    SportTabsPageComponent,
    JackpotReceiptPageComponent,
    EnhancedMultiplesTabComponent,
    BuildYourBetTabComponent,
    PrivateMarketsTabComponent,
    PrivateMarketsTermsAndConditionsComponent,
    CouponsListComponent,
    GoalscorerCouponComponent,
    MatchResultsSportTabComponent,
    CouponsListSportTabComponent,
    CouponsContentSportTabComponent,
    SpecialsSportTabComponent,
    OutrightsSportTabComponent,
    JackpotSportTabComponent,
    BuildYourBetHomeComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SbModule {
  constructor(private localeService: LocaleService) {
    this.localeService.setLangData(sbLangData);
    this.localeService.setLangData(sbDesktopLangData);
  }
}
