import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SportEventComponent } from '@sb/components/sportEvent/sport-event.component';
import {
  PrivateMarketsTermsAndConditionsComponent
} from '@app/sb/components/privateMarketsTab/private-markets-terms-and-conditions.component';
import { JackpotReceiptPageComponent } from '@app/sb/components/jackpotReceiptPage/jackpot-receipt-page.component';
import { SportTabsPageComponent } from '@sbModule/components/SportTabsPage/sport-tabs-page.component';

// Overridden app components
import { CouponsDetailsComponent } from '@ladbrokesDesktop/sb/components/couponsDetails/coupons-details.component';
import { DesktopSportMatchesPageComponent } from '@ladbrokesDesktop/sb/components/sportMatchesPage/sport-matches-page.component';
import { SportMainComponent } from '@ladbrokesDesktop/sb/components/sportMain/sport-main.component';
import { GermanSupportSportTabGuard } from '@root/app/core/guards/german-support-sport-tab-guard.service';

export const routes: Routes = [
  {
    path: 'sport/:sport',
    component: SportMainComponent,
    data: {
      segment: 'sport'
    },
    children: [{
      path: 'matches',
      pathMatch: 'full',
      redirectTo: 'matches/today'
    }, {
      path: 'playerbets',
      pathMatch: 'full',
      redirectTo: '/playerbets'
    }, {
      path: ':display',
      component: SportTabsPageComponent,
      canActivate: [GermanSupportSportTabGuard],
      data: {
        segment: 'sport.display'
      }
    }, {
      path: ':display/:tab',
      component: DesktopSportMatchesPageComponent,
      data: {
        segment: 'sport.matches.tab'
      }
    }],
  },
  {
    path: 'event/:sport',
    component: SportEventComponent,
  },
  {
    path: 'event/:sport/:className/:typeName/:eventName/:id',
    pathMatch: 'full',
    redirectTo: 'event/:sport/:className/:typeName/:eventName/:id/all-markets'
  },
  {
    path: 'event/:sport/:className/:typeName/:eventName/:id/:market',
    component: SportMainComponent,
    data: {
      segment: 'eventMain'
    },
    children: [{
      path: ':pitch',
      component: SportMainComponent,
      data: {
        segment: 'eventMain'
      }
    }]
  },
  {
    path: 'event/:sport/:className/:typeName/:eventName/:id/:market/:live',
    component: SportMainComponent,
    data: {
      segment: 'eventMain'
    }
  },
  {
    path: 'competitions/:sport',
    pathMatch: 'full',
    redirectTo: '/sport/:sport/competitions'
  },
  {
    path: 'competitions/:sport/:className',
    pathMatch: 'full',
    redirectTo: '/sport/:sport/competitions'
  },
  {
    path: 'coupons/:sport',
    pathMatch: 'full',
    redirectTo: '/sport/:sport/coupons'
  },
  {
    path: 'coupons/:sport/:couponName',
    pathMatch: 'full',
    redirectTo: '/sport/:sport/coupons'
  },
  {
    path: 'coupons/:sport/:couponName/:couponId',
    component: CouponsDetailsComponent,
    data: {
      segment: 'couponsDetails'
    }
  },
  {
    path: 'private-markets',
    children: [
      {
        path: 'terms-conditions',
        component: PrivateMarketsTermsAndConditionsComponent,
        data: {
          segment: 'privateMarketsTeamsAndConditions'
        }
      }]
  },
  {
    path: 'football-jackpot-receipt',
    canActivate: [GermanSupportSportTabGuard],
    component: JackpotReceiptPageComponent,
    data: {
      segment: 'footballJackpotReceipt'
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class SbRoutingModule { }
