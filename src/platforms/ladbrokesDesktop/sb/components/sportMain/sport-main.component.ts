import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { SportMainComponent as CoralSportMainComponent } from '@app/sb/components/sportMain/sport-main.component';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { TimeService } from '@core/services/time/time.service';
import { RoutingState } from '@sharedModule/services/routingState/routing-state.service';
import { SportsConfigService } from '@sb/services/sportsConfig/sports-config.service';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { StorageService } from '@core/services/storage/storage.service';
import { UserService } from '@core/services/user/user.service';
import { DeviceService } from '@core/services/device/device.service';
import { GermanSupportService } from '@core/services/germanSupport/german-support.service';
import { ISportConfigTab } from '@root/app/core/services/cms/models';
import { SportTabsService } from '@sb/services/sportTabs/sport-tabs.service';
import { CoreToolsService } from '@root/app/core/services/coreTools/core-tools.service';
import { SlpSpinnerStateService } from '@core/services/slpSpinnerState/slpSpinnerState.service';
import { NavigationService } from '@core/services/navigation/navigation.service';

@Component({
  selector: 'sport-main-component',
  styleUrls: ['sport-main.component.less'],
  templateUrl: 'sport-main.component.html'
})

export class SportMainComponent extends CoralSportMainComponent implements OnInit { // TODO extend ladbrokesMobile instead!
  constructor(
    protected cmsService: CmsService,
    protected timeService: TimeService,
    protected sportsConfigService: SportsConfigService,
    protected routingState: RoutingState,
    protected pubSubService: PubSubService,
    protected location: Location,
    protected Storage: StorageService,
    protected User: UserService,
    protected router: Router,
    protected route: ActivatedRoute,
    protected device: DeviceService,
    protected germanSupportService: GermanSupportService,
    protected sportTabsService: SportTabsService,
    protected coreToolsService: CoreToolsService,
    protected slpSpinnerStateService: SlpSpinnerStateService,
    protected navigationService: NavigationService) {
    super(
      cmsService,
      timeService,
      sportsConfigService,
      routingState,
      pubSubService,
      location,
      Storage,
      User,
      router,
      route,
      device,
      sportTabsService,
      coreToolsService,
      slpSpinnerStateService,
      navigationService);
  }

  get sportDetailPage(): string {
    return this.isSportDetailPage ? 'eventDetailsPage' : '';
  }

  ngOnInit(): void {
    super.ngOnInit();

    // tslint:disable-next-line:pubsub-connect
    this.pubSubService.subscribe(this.channelName, [this.pubSubService.API.SUCCESSFUL_LOGIN, this.pubSubService.API.SESSION_LOGIN], () => {
      this.sportTabs = this.filterTabs(this.sportTabs);
    });
  }

  protected shouldNavigatedToTab() {
    return this.isHomeUrl();
  }

  protected filterTabs(sportTabs: ISportConfigTab[]): ISportConfigTab[] {
    sportTabs.forEach((tab: ISportConfigTab, index: number) => {
      if (this.sportName === 'football' && this.germanSupportService.isGermanUser() && tab.id === 'tab-jackpot') {
        tab.hidden = true;
      }

      if (tab.id === 'tab-matches' || tab.id === 'tab-live') {
        tab.hidden = false;
      }
      this.defaultTab = 'matches';
    });


    if (!sportTabs.find(sportTab => sportTab.id === 'tab-live')) {
      sportTabs.unshift({
        id: 'tab-live',
        name: 'live',
        label: 'sb.tabsNameInPlay',
        url: `/sport/${this.sportPath}/live`,
        hidden: false,
        sortOrder: 1
      });
    }

    return sportTabs;
  }
}
