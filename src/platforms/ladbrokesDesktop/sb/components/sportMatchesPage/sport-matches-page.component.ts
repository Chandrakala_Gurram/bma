import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Event, NavigationEnd, Router } from '@angular/router';
import { Location } from '@angular/common';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs/index';
import * as _ from 'underscore';

import { GamingService } from '@core/services/sport/gaming.service';
import { ISportConfigSubTab, ISportConfigTab } from '@app/olympics/models/olympics.model';
import { RoutingState } from '@sharedModule/services/routingState/routing-state.service';
import { SportsConfigService } from '@sb/services/sportsConfig/sports-config.service';
import { SportMatchesPageComponent } from '@app/sb/components/sportMatchesPage/sport-matches-page.component';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { UpdateEventService } from '@core/services/updateEvent/update-event.service';

@Component({
  selector: 'sport-matches-page',
  templateUrl: 'sport-matches-page.component.html'
})

export class DesktopSportMatchesPageComponent extends SportMatchesPageComponent implements OnInit, OnDestroy {
  tab: string;
  matchesTabs: ISportConfigSubTab[];
  switchers: ISportConfigSubTab[];
  isFootball: boolean;
  locationChangeListener: Subscription;
  indexPage: number;

  constructor(location: Location,
              sportsConfigService: SportsConfigService,
              routingState: RoutingState,
              protected route: ActivatedRoute,
              protected router: Router,
              windowRefService: WindowRefService,
              // tslint:disable-next-line
              protected updateEventService: UpdateEventService // for events subscription (done in service init)
              ) {
    super(location, sportsConfigService, routingState, route, router, windowRefService, updateEventService);
  }

  ngOnInit(): void {
    this.sportName = this.route.snapshot.paramMap.get('sport');
    this.isFootball = this.sportName === 'football';
    this.tab = this.route.children && this.route.snapshot.paramMap.get('tab');
    this.showSpinner();
    this.sportsConfigSubscription = this.sportsConfigService.getSport(this.sportName)
      .subscribe((sport: GamingService) => {
        this.sport = sport;

        this.initialzeMatchesTabs();
        this.loadSport(this.switchers[this.indexPage].name);
        this.addLocationChangeHandler();
        this.hideSpinner();
      }, error => {
        this.hideSpinner();
        console.warn('MatchesPage', error.error || error);
      });
  }

  ngOnDestroy(): void {
    this.locationChangeListener && this.locationChangeListener.unsubscribe();
    this.sportsConfigSubscription && this.sportsConfigSubscription.unsubscribe();
  }

  loadSport(tabName: string = ''): void {
    this.tab = tabName;
    super.loadSport(tabName);
  }

  private addLocationChangeHandler(): void {
    this.locationChangeListener = this.router
      .events.pipe(
        filter((event: Event) => event instanceof NavigationEnd))
      .subscribe(() => {
        const { tab } = this.route.snapshot.params;
        if (tab === this.tab) { return; }
        this.selectTab(tab);
      });
  }

  /**
   * Function responsible for selecting tabs
   */
  private selectTab(switcherName: string): void {
    this.indexPage = this.getTabIndex(switcherName);
    this.switchers &&
    this.switchers[this.indexPage] &&
    this.switchers[this.indexPage].name &&
    this.loadSport(this.switchers[this.indexPage].name);
  }

  /**
   * Function return index of selected tab
   */
  private getTabIndex(name: string = ''): number {
    const index = _.findIndex(this.switchers, (tab: { name: string }) => tab.name === name);
    return index === -1 ? 0 : index;
  }

  private initialzeMatchesTabs(): void {
    this.matchesTabs = (_.find(this.sport.sportConfig.tabs, (tab: ISportConfigTab) => {
      return tab.subTabs;
    }) as ISportConfigTab).subTabs;
    this.switchers = _.map(this.matchesTabs, (switcher: { onClick: Function, name: string }) => {
      switcher.onClick = () => this.selectTab(switcher.name);
      return switcher;
    }) as ISportConfigSubTab[];
    this.indexPage = this.getTabIndex(this.tab);
  }
}
