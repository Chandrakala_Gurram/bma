import { Injectable } from '@angular/core';

// tslint:disable-next-line:max-line-length
import { UpgradeAccountDialogService as BaseUpgradeAccountDialogService } from '@ladbrokesMobile/retail/services/upgradeAccountDialog/upgrade-account-dialog.service';

@Injectable()
export class UpgradeAccountDialogService extends BaseUpgradeAccountDialogService {
  protected modulePath = '@ladbrokesDesktop/retail/retail.module#RetailModule';
}
