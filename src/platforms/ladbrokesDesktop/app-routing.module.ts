import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DesktopHomeComponent } from '@ladbrokesDesktop/bma/components/home/home.component';
import { LogoutResolver } from '@app/vanillaInit/services/logout/logout.service';
import { LoggedInGuard } from '@core/guards/logged-in-guard.service';
import { NotFoundComponent } from '@bma/components/404/404.component';
import { MaintenanceComponent } from '@shared/components/maintenance/maintenance.component';
import { ILadbrokesRetailConfig } from '@ladbrokesMobile/core/services/cms/models/system-config';
import { IRouteData } from '@app/core/models/route-data.model';
import { SignUpRouteGuard } from '@core/guards/sign-up-guard.service';
import { ContactUsComponent } from '@bma/components/contactUs/contact-us.component';
import { LadbrokesAzSportsPageComponent } from '@ladbrokesDesktop/shared/components/azSportPage/az-sports-page.component';
import { GermanSupportGuard } from '@root/app/core/guards/german-support-guard.service';
import { StaticComponent } from '@bma/components/static/static.component';
import { LazyRouteGuard } from '@core/guards/lazy-route-guard.service';
import { RootComponent } from '@ladbrokesDesktop/app.component';
import { LABELHOST_FEATURES_ROUTES, routeData } from '@labelhost/core/features';
import { ProductSwitchResolver } from '@app/host-app/product-switch.resolver';
import { SiteRootGuard } from '@vanilla/core/core';
import { VANILLA_ROUTES } from '@vanilla/core/features';
import { ROOT_APP_ROUTES } from '@app/vanillaInit/root-routes.definitions';
import { CustomPreloadingStrategy } from '@app/vanillaInit/custom.preload.strategy';
import { routes as SbRoutes } from '@sbModule/sb-routing.module';
import { routes as OlympicsRoutes } from '@ladbrokesDesktop/olympics/olympics-route.module';
import { routes as BigCompetitionsRoutes } from '@ladbrokesDesktop/bigCompetitions/big-competitions-routing.module';
import { routes as BetFinderRoutes } from '@ladbrokesDesktop/bf/betfinder-routing.module';
import { routes as RacingRoutes } from '@ladbrokesDesktop/racing/racing-routing.module';
import { MaintenanceGuard } from '@core/guards/maintenance-guard.service';
import { DesktopLiveStreamWrapperComponent } from '@ladbrokesDesktop/bma/components/liveStream/live-stream-wrapper.component';
import { DepositRedirectGuard } from '@coreModule/guards/deposit-redirect.guard';
import { NotFoundPageGuard } from '@core/guards/not-found-page-guard.service';
import { IRetailConfig } from '@core/services/cms/models/system-config';

const appRoutes: Routes = [
  {
    path: '',
    component: RootComponent,
    data: {
      ...routeData({ allowAnonymous: true }),
      product: 'host',
      segment: 'main'
    },
    runGuardsAndResolvers: 'always',
    resolve: {
      __p: ProductSwitchResolver
    },
    canActivate: [SiteRootGuard],
    children: [
      {
        path: 'en',
        data: {
          segment: 'vanilla'
        },
        children: [
          ...VANILLA_ROUTES,
          ...LABELHOST_FEATURES_ROUTES,
          ...ROOT_APP_ROUTES,
          {
            path: '**',
            redirectTo: '/'
          }
        ]
      },
      ...SbRoutes,
      ...RacingRoutes,
      ...OlympicsRoutes,
      ...BigCompetitionsRoutes,
      ...BetFinderRoutes,
      {
        path: '',
        component: DesktopHomeComponent,
        data: {
          segment: 'home'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            loadChildren: '@featuredModule/featured.module#FeaturedModule',
            data: {
              segment: 'home'
            }
          }
        ]
      }, {
        path: 'live-stream',
        component: DesktopLiveStreamWrapperComponent,
        data: {
          segment: 'liveStream'
        }
      }, {
        path: 'az-sports',
        component: LadbrokesAzSportsPageComponent,
        data: {
          segment: 'azSports'
        }
      }, {
        path: 'logout',
        component: DesktopHomeComponent,
        resolve: {
          logout: LogoutResolver
        },
        data: {
          segment: 'logout'
        }
      }, {
        path: 'settings',
        loadChildren: '@ladbrokesDesktop/bma/components/userSettings/user-settings.module#UserSettingsModule',
        canActivate: [LoggedInGuard],
        runGuardsAndResolvers: 'always',
        data: {
          segment: 'settings'
        }
      }, {
        path: 'contact-us',
        component: ContactUsComponent,
        data: {
          segment: 'contactUs'
        }
      },
      {
        path: 'static/:static-block',
        component: StaticComponent,
        data: {
          segment: 'static'
        }
      }, {
        path: 'freebets',
        loadChildren: '@freebetsModule/freebets.module#FreebetsModule',
        canActivate: [LoggedInGuard],
        data: {
          segment: 'freebets'
        }
      }, {
        path: '404',
        component: NotFoundComponent,
        data: {
          segment: '404'
        }
      }, {
        path: 'under-maintenance',
        component: MaintenanceComponent,
        resolve: { data: MaintenanceGuard },
        canDeactivate: [MaintenanceGuard]
      }, {
        path: 'deposit',
        component: DesktopHomeComponent,
        canActivate: [DepositRedirectGuard]
      },
      // Modules lazy loaded by routes
      // Yourcall
      {
        path: 'yourcall-lazy-load',
        loadChildren: '@yourCallModule/your-call.module#YourCallModule',
        canActivate: [LazyRouteGuard]
      },
      {
        path: 'tote-information',
        canActivate: [GermanSupportGuard],
        redirectTo: 'tote/information'
      }, {
        path: 'tote',
        canActivate: [GermanSupportGuard],
        loadChildren: '@ladbrokesDesktop/tote/tote.module#ToteModule'
      }, {
        path: 'bet-finder',
        canActivate: [GermanSupportGuard],
        loadChildren: '@betFinderModule/betfinder.module#BetFinderModule',
        data: {
          feature: 'raceBetFinder'
        } as IRouteData<ILadbrokesRetailConfig>
      }, {
        path: 'virtual-sports',
        loadChildren: '@ladbrokesDesktop/vsbr/vsbr.module#VsbrModule'
      }, {
        path: 'bet-finder',
        canActivate: [GermanSupportGuard],
        loadChildren: '@betFinderModule/betfinder.module#BetFinderModule',
        data: {
          feature: 'raceBetFinder'
        } as IRouteData<ILadbrokesRetailConfig>
      }, {
        path: 'in-play',
        loadChildren: '@inplayModule/inplay.module#InplayModule',
        data: {
          preload: true
        }
      }, {
        path: 'byb-module',
        loadChildren: '@bybHistoryModule/byb-history.module#LazyBybHistoryModule',
        canActivate: [ LazyRouteGuard ]
      }, {
        path: 'betslip/add/:outcomeId',
        loadChildren: '@betslipModule/betslip.module#BetslipModule'
      }, {
        path: 'betslip/unavailable',
        loadChildren: '@betslipModule/betslip.module#BetslipModule'
      },
      {
        path: 'competitions/:sport/:className/:typeName',
        data: {
          segment: 'competitionTypeEvents'
        },
        loadChildren: '@lazy-modules-module/competitionsSportTab/competitionsSportTab.module#CompetitionsTabModule'
      },
      // Odds Boost module lazy loading
      {
        path: 'oddsboost',
        loadChildren: '@oddsBoostModule/odds-boost.module#OddsBoostModule'
      },
      // Bet History module lazy loading
      {
        path: 'cashout',
        loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule'
      }, {
        path: 'open-bets',
        loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule'
      }, {
        path: 'bet-history',
        loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule'
      }, {
        path: 'in-shop-bets',
        loadChildren: '@betHistoryModule/bet-history.module#BetHistoryModule'
      },
      // Lotto module lazy loading
      {
        path: 'lotto-results',
        canActivate: [GermanSupportGuard],
        redirectTo: 'lotto/results'
      }, {
        path: 'lotto',
        canActivate: [GermanSupportGuard],
        loadChildren: '@ladbrokesDesktop/lotto/lotto.module#LottoModule'
      },
      // Registration module lazy loading
      {
        path: 'signup',
        canActivate: [SignUpRouteGuard],
        children: []
      },
      // Retail module lazy loading
      {
        path: 'retail',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'menu'
        } as IRouteData<IRetailConfig>
      },
      {
        path: 'retail-upgrade',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'upgrade'
        } as IRouteData<IRetailConfig>
      },
      {
        path: 'bet-filter',
        loadChildren: '@ladbrokesDesktop/retail/retail.module#RetailModule',
        data: {
          feature: 'footballFilter'
        } as IRouteData<ILadbrokesRetailConfig>
      },
      {
        path: 'bet-filter/:child',
        loadChildren: '@ladbrokesDesktop/retail/retail.module#RetailModule',
        data: {
          feature: 'footballFilter'
        } as IRouteData<ILadbrokesRetailConfig>
      },
      {
        path: 'bet-filter/:child/:child',
        loadChildren: '@ladbrokesDesktop/retail/retail.module#RetailModule',
        data: {
          feature: 'footballFilter'
        } as IRouteData<ILadbrokesRetailConfig>
      },
      {
        path: 'bet-filter',
        loadChildren: '@ladbrokesDesktop/retail/retail.module#RetailModule',
        data: {
          feature: 'digitalCoupons'
        } as IRouteData<ILadbrokesRetailConfig>
      },
      {
        path: 'bet-tracker',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'shopBetTracker'
        } as IRouteData<IRetailConfig>
      },
      {
        path: 'shop-locator',
        loadChildren: '@retailModule/retail.module#RetailModule',
        data: {
          feature: 'shopLocator'
        } as IRouteData<IRetailConfig>
      },
      // Freebets lazy loading
      {
        path: 'freebets-lazy-load',
        loadChildren: '@freebetsModule/freebets.module#FreebetsModule',
        canActivate: [LazyRouteGuard]
      },
      // Quickbet
      {
        path: 'quickbet-lazy-load',
        loadChildren: '@quickbetModule/quickbet.module#QuickbetModule',
        canActivate: [LazyRouteGuard]
      },
      // Quick Deposit
      {
        path: 'quick-deposit-lazy-load',
        loadChildren: '@quickDepositModule/quick-deposit.module#QuickDepositModule',
        canActivate: [ LazyRouteGuard ]
      },
      // Bet radar lazy loading
      {
        path: 'bet-radar-lazy-load',
        loadChildren: '@lazy-modules-module/betRadarProvider/bet-radar.module#BetRadarLadbrokesModule',
        canActivate: [ LazyRouteGuard ]
      },
       // Receipt header lazy loading
       {
        path: 'receipt-header-lazy-load',
        loadChildren: '@lazy-modules-module/receiptHeader/receipt-header.module#ReceiptHeaderModule',
        canActivate: [ LazyRouteGuard ]
      },
      // Runner-spotlight lazy loading
      {
        path: 'runner-spotlight-lazy-load',
        loadChildren: '@lazy-modules-module/runnerSpotlight/runner-spotlight.module#RunnerSpotlightModule',
        canActivate: [LazyRouteGuard]
      },
      {
        path: 'forecast-tricast-lazy-load',
        loadChildren: '@lazy-modules-module/forecastTricast/forecastTricast.module#ForecastTricastModule',
        canActivate: [ LazyRouteGuard ]
      },
      {
        path: 'event-video-stream-lazy-load',
        loadChildren: '@lazy-modules-module/eventVideoStream/event-video-stream.module#LazyEventVideoStreamModule',
        canActivate: [ LazyRouteGuard ]
      },
      {
        path: 'promotions',
        loadChildren: '@promotionsModule/promotions.module#PromotionsModule',
        data: {
          path: 'promotions/retail',
          feature: 'promotions'
        }
      },
      // EDP
      {
        path: 'edp-lazy-load',
        loadChildren: '@edpModule/edp.module#EdpModule',
        canActivate: [LazyRouteGuard]
      },
      {
        path: '1-2-free',
        pathMatch: 'full',
        loadChildren: 'app/oneTwoFree/one-two-free.module#OneTwoFreeModule',
      },
      {
        path: 'qe/:sourceId',
        loadChildren: '@ladbrokesDesktop/questionEngine/question-engine.module#QuestionEngineModule',
        data: {
          segment: 'question-engine'
        }
      },
      {
        path: '**',
        component: NotFoundComponent,
        canActivate: [ NotFoundPageGuard ]
      }]
  }];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        useHash: false,
        paramsInheritanceStrategy: 'always',
        onSameUrlNavigation: 'reload',
        preloadingStrategy: CustomPreloadingStrategy
        // enableTracing: true  // <-- debugging purposes only
      }
    )
  ],
  exports: [
    RouterModule
  ],
  declarations: [],
  providers: [
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    CustomPreloadingStrategy
  ]
})
export class AppRoutingModule { }
