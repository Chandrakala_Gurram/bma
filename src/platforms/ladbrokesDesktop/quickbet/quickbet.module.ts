import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '@sharedModule/shared.module';
import { QuickbetRunService } from '@app/quickbet/services/quickbetRunService/quickbet-run.service';
import { QuickbetYourcallWrapperComponent } from '@app/quickbet/components/quickbetYourcallWrapper/quickbet-yourcall-wrapper.component';
import { LadbrokesBetSummaryComponent } from '@ladbrokesMobile/quickbet/components/betSummary/bet-summary.component';
import { LadbrokesQuickbetReceiptComponent } from '@ladbrokesMobile/quickbet/components/quickbetReceipt/quickbet-receipt.component';
import { LadbrokesQuickbetPanelComponent } from '@ladbrokesMobile/quickbet/components/quickbetPanel/quickbet-panel.component';
import { LadbrokesQuickbetSelectionComponent } from '@ladbrokesMobile/quickbet/components/quickbetSelection/quickbet-selection.component';
import { LadbrokesQuickStakeComponent } from '@ladbrokesMobile/quickbet/components/quickStake/quick-stake.component';
import { LadbrokesQuickbetInfoPanelComponent } from '@ladbrokesMobile/quickbet/components/quickbetInfoPanel/quickbet-info-panel.component';


@NgModule({
  imports: [
    SharedModule,
    FormsModule,
  ],
  providers: [
    CurrencyPipe
  ],
  declarations: [
    QuickbetYourcallWrapperComponent,
    LadbrokesBetSummaryComponent,
    LadbrokesQuickbetSelectionComponent,
    LadbrokesQuickbetReceiptComponent,
    LadbrokesQuickStakeComponent,
    LadbrokesQuickbetInfoPanelComponent,
    LadbrokesQuickbetPanelComponent
  ],
  entryComponents: [
    QuickbetYourcallWrapperComponent,
    LadbrokesBetSummaryComponent,
    LadbrokesQuickbetSelectionComponent,
    LadbrokesQuickbetReceiptComponent,
    LadbrokesQuickbetInfoPanelComponent,
    LadbrokesQuickbetPanelComponent,
    LadbrokesQuickStakeComponent
  ],
  exports: [],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class QuickbetModule {
  static entry = {
    LadbrokesQuickbetPanelComponent,
    QuickbetYourcallWrapperComponent,
    LadbrokesQuickStakeComponent
  };

  constructor(quickbetRunService: QuickbetRunService) {
    quickbetRunService.init();
  }
}
