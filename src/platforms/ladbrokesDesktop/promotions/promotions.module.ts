import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '@sharedModule/shared.module';
import { ModalModule } from 'angular-custom-modal';
import { PromotionDetailsComponent } from '@promotions/components/promotionDetails/promotion-details.component';
import { RetailPromotionsPageComponent } from '@promotions/components/retailPromotionsPage/retail-promotions-page.component';
import {
  DesktopRetailPromotionsPageComponent
} from '@ladbrokesDesktop/promotions/components/retailPromotionsPage/retail-promotions-page.component';
import { PromotionsRoutingModule } from './promotionsl-routing.module';

// Overridden
import {
  DesktopSinglePromotionPageComponent
} from '@ladbrokesDesktop/promotions/components/singlePromotionPage/single-promotion-page.component';
import { DesktopAllPromotionsPageComponent } from '@ladbrokesDesktop/promotions/components/allPromotionsPage/all-promotions-page.component';
import { LadbrokesDesktopPromotionsComponent } from '@ladbrokesDesktop/promotions/components/promotion/promotions.component';
import { DesktopModule } from '@desktop/desktop.module';

@NgModule({
  imports: [
    SharedModule,
    DesktopModule,
    ModalModule,
    PromotionsRoutingModule
  ],
  exports: [
    PromotionDetailsComponent
  ],
  declarations: [
    RetailPromotionsPageComponent,
    PromotionDetailsComponent,

    // Overridden
    DesktopRetailPromotionsPageComponent,
    DesktopAllPromotionsPageComponent,
    DesktopSinglePromotionPageComponent,
    LadbrokesDesktopPromotionsComponent
  ],
  entryComponents: [
    RetailPromotionsPageComponent,
    PromotionDetailsComponent,

    // Overridden
    DesktopRetailPromotionsPageComponent,
    DesktopAllPromotionsPageComponent,
    DesktopSinglePromotionPageComponent,
    LadbrokesDesktopPromotionsComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class PromotionsModule {}
