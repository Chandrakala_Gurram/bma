import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule, NgModuleFactory, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { DynamicLoaderModule } from '@app/dynamicLoader/dynamic-loader.module';
import { AppRoutingModule } from '@ladbrokesDesktop/app-routing.module';
import { RootComponent } from '@ladbrokesDesktop/app.component';
import { BmaModule } from '@bmaModule/bma.module';
import { lazyComponentsManifests } from '@ladbrokesDesktop/lazy-components.manifests';
import { SharedModule } from '@sharedModule/shared.module';
import { CoreModule } from '@coreModule/core.module';
import { DesktopModule } from '@desktop/desktop.module';
import { LocaleService } from '@core/services/locale/locale.service';
import * as sbDesktopLangData from '@app/lazy-modules/locale/translations/en-US/sbdesktop.lang.ts';
import { runOnAppInit, STORE_PREFIX, VanillaCoreModule } from '@vanilla/core/core';
import { VanillaCommonModule } from '@vanilla/core/common';
import { AppComponent, VanillaFeaturesModule } from '@vanilla/core/features';
import { LabelHostFeaturesModule, LH_HTTP_ERROR_HANDLERS } from '@labelhost/core/features';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VanillaInitModule } from '@vanillaInitModule/vanilla-init.module';
import { CoralSportsClientConfigModule } from '@app/client-config/client-config.module';
import { LabelHostHttpErrorHandler } from '@app/vanillaInit/services/labelHostErrorHandler/labelHostHttpErrorHandler.service';
import { HostAppBootstrapper } from '@app/host-app/host-app-bootstrapper.service';
import { RoutingState } from '@shared/services/routingState/routing-state.service';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    VanillaCoreModule.forRoot(),
    VanillaCommonModule.forRoot(),
    VanillaFeaturesModule.forRoot(),
    LabelHostFeaturesModule.forRoot(),
    CoreModule,
    VanillaInitModule,
    AppRoutingModule,
    SharedModule.forRoot(),
    BmaModule,
    DesktopModule,
    DynamicLoaderModule.forRoot(lazyComponentsManifests),
    CoralSportsClientConfigModule.forRoot()
  ],
  declarations: [
    RootComponent
  ],
  providers: [
    { provide: STORE_PREFIX, useValue: 'coralsports.' },
    { provide: LH_HTTP_ERROR_HANDLERS, useClass: LabelHostHttpErrorHandler, multi: true },
    runOnAppInit(HostAppBootstrapper)
  ],
  bootstrap: [
    AppComponent
  ],
  entryComponents: [
    RootComponent
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {
  constructor(private loader: NgModuleFactoryLoader,
    private injector: Injector,
    private localeService: LocaleService,
    private routingState: RoutingState) {
    this.routingState.loadRouting();
    const paths = ['src/app/lazy-modules/locale/translation.module#TranslationModule',
      'src/app/lazy-modules/performanceMark/performanceMark.module#PerformanceMarkModule'];
    paths.forEach((path) => {
      this.loader
        .load(path)
        .then((moduleFactory: NgModuleFactory<any>) => moduleFactory.create(this.injector));
    });
    this.localeService.setLangData(sbDesktopLangData);
  }
}
