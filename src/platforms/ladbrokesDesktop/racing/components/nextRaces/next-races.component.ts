import { Component, OnInit, OnDestroy } from '@angular/core';

import { NextRacesModuleComponent as CoralNextRacesModuleComponent } from '@root/app/racing/components/nextRaces/next-races.component';
import { PubSubService } from '@root/app/core/services/communication/pubsub/pubsub.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { Location } from '@angular/common';
import { RoutingHelperService } from '@root/app/core/services/routingHelper/routing-helper.service';
import { NextRacesService } from '@root/app/core/services/racing/nextRaces/next-races.service';
import { EventService } from '@root/app/sb/services/event/event.service';
import { CommandService } from '@root/app/core/services/communication/command/command.service';
import { GermanSupportService } from '@root/app/core/services/germanSupport/german-support.service';
import { RacingPostService } from '@core/services/racing/racingPost/racing-post.service';

@Component({
  selector: 'next-races-module',
  templateUrl: 'next-races.component.html',
  styleUrls: ['./next-races.component.less']
})

export class LadbrokesNextRacesModuleComponent extends CoralNextRacesModuleComponent implements OnInit, OnDestroy {

  isGermanUser: boolean = false;

  constructor(
    protected pubSubService: PubSubService,
    protected cmsService: CmsService,
    protected location: Location,
    protected routingHelperService: RoutingHelperService,
    protected nextRacesService: NextRacesService,
    protected eventService: EventService,
    protected commandService: CommandService,
    protected racingPostService: RacingPostService,
    private germanSupportService: GermanSupportService
  ) {
    super(pubSubService, cmsService, location, routingHelperService,
      nextRacesService, eventService, commandService, racingPostService);
  }
  ngOnInit() {
    super.ngOnInit();
    this.isGermanUser = this.germanSupportService.isGermanUser();
    this.pubSubService.subscribe('LadbrokesNextRacesModuleComponent',
      [this.pubSubService.API.SUCCESSFUL_LOGIN, this.pubSubService.API.SESSION_LOGIN, this.pubSubService.API.SESSION_LOGOUT], () => {
        this.isGermanUser = this.germanSupportService.isGermanUser();
      }
    );
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.pubSubService.unsubscribe('LadbrokesNextRacesModuleComponent');
  }
}
