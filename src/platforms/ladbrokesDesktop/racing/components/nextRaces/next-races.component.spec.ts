import { LadbrokesNextRacesModuleComponent } from '@ladbrokesDesktop/racing/components/nextRaces/next-races.component';

describe('NextRacesModuleComponent', () => {
  let component: LadbrokesNextRacesModuleComponent;
  let pubSubService, cmsService, location, routingHelperService,
    nextRacesService, eventService, commandService, racingPostService, germanSupportService;

  beforeEach(() => {
    pubSubService = {};
    cmsService = {};
    location = {};
    routingHelperService = {};
    nextRacesService = {};
    eventService = {};
    commandService = {};
    racingPostService = {};
    germanSupportService = {};

    createComponent();
  });

  function createComponent() {
    component = new LadbrokesNextRacesModuleComponent(
      pubSubService,
      cmsService,
      location,
      routingHelperService,
      nextRacesService,
      eventService,
      commandService,
      racingPostService,
      germanSupportService
    );
  }

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
