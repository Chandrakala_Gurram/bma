import { Component, OnInit, OnDestroy } from '@angular/core';
import { BuildYourRaceCardPageService } from '@ladbrokesDesktop/racing/components/buildYourRaceCardPage/build-your-race-card-page.service';
import { RoutingState } from '@sharedModule/services/routingState/routing-state.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ISportEvent } from '@core/models/sport-event.model';
import { RoutingHelperService } from '@app/core/services/routingHelper/routing-helper.service';
import { from, Subscription } from 'rxjs';


@Component({
  selector: 'build-your-race-card-page',
  templateUrl: 'build-your-race-card-page.component.html'
})
export class BuildYourRaceCardPageComponent implements OnInit, OnDestroy {
  events: ISportEvent[];
  eventsIds: string;
  sport: string = 'horseracing';
  racingDefaultPath: string;

  protected eventsSubscription: Subscription;

  constructor(
    private buildYourRaceCardPageService: BuildYourRaceCardPageService,
    private routingState: RoutingState,
    private route: ActivatedRoute,
    private router: Router,
    private routingHelperService: RoutingHelperService
  ) {
  }

  ngOnInit(): void {
    this.routingHelperService.formSportUrl('horseracing', 'featured').subscribe((url: string) => {
      this.racingDefaultPath = url;
    });
    this.eventsIds = this.routingState.getRouteParam('ids', this.route.snapshot);
    this.eventsSubscription = from(this.buildYourRaceCardPageService.getEvents(this.eventsIds))
    .subscribe((events: ISportEvent[]) => {
      // Subscription from liveServe PUSH updates
      this.buildYourRaceCardPageService.subscribeForUpdates(events);
      this.events = events;
    });
  }
  /**
   * Check for market with antepost flag
   * @param event
   * @return {boolean}
   */
  isAntepostMarket(event): boolean {
    return event &&
      event.markets &&
      event.markets[0] &&
      event.markets[0].isAntepost === 'true';
  }

  trackById(index: number, event: ISportEvent): string {
    return `${index}${event.id}`;
  }

  ngOnDestroy(): void {
    // unSubscription from liveServe PUSH updates
    this.eventsSubscription && this.eventsSubscription.unsubscribe();
    this.buildYourRaceCardPageService.unSubscribeForUpdates();
  }

  goToDefaultPage(): void {
    this.router.navigateByUrl(this.racingDefaultPath);
  }

  /**
   * Click on Horse Block.
   *
   * Toggle Horse Information Area.
   *
   * param {array} summary of expanded and collapsed areas.
   * param {number} market index.
   * param {number} outcome index.
   *
   */
  onExpand(expandedSummary, mIndex, oIndex) {
    const temp = !expandedSummary[mIndex][oIndex];

    for (let i = 0; i < expandedSummary[mIndex].length; i++) {
      expandedSummary[mIndex][i] = false;
    }

    expandedSummary[mIndex][oIndex] = temp;
  }
}
