import { BuildYourRaceCardPageComponent } from './build-your-race-card-page.component';
import { of } from 'rxjs';

describe('LadbrokesBuildYourRaceCardPageComponent', () => {
  let component: BuildYourRaceCardPageComponent;
  let buildYourRaceCardPageService;
  let routingState;
  let route;
  let router;
  let routingHelperService;

  beforeEach(() => {
    buildYourRaceCardPageService = {
      getEvents: jasmine.createSpy().and.returnValue( of({}).toPromise() ),
      subscribeForUpdates: jasmine.createSpy(),
      unSubscribeForUpdates: jasmine.createSpy()
    };
    routingState = {
      getRouteParam: jasmine.createSpy()
    };
    route = {};
    router = {};
    routingHelperService = {
      formSportUrl: jasmine.createSpy('formSportUrl').and.returnValue(of(''))
    };

    component = new BuildYourRaceCardPageComponent(buildYourRaceCardPageService, routingState, route, router, routingHelperService);
  });

  describe('onInit', () => {
    it('should save subscription', () => {
      component.ngOnInit();
      expect(component['eventsSubscription']).toBeDefined();
    });
  });

  describe('onDestroy', () => {
    it('should unsubscribe subscriptions', () => {
      component.ngOnInit();
      spyOn(component['eventsSubscription'], 'unsubscribe');
      component.ngOnDestroy();
      expect(component['eventsSubscription'].unsubscribe).toHaveBeenCalled();
    });

    it('should not call unsubscribe', () => {
      component.ngOnDestroy();
      expect(component['eventsSubscription']).toBeUndefined();
    });
  });

});
