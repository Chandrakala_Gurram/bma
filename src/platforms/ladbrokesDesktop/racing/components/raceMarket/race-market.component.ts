import { Component } from '@angular/core';
import { LadbrokesRaceMarketComponent } from '@ladbrokesMobile/racing/components/raceMarket/race-market.component';

@Component({
  selector: 'race-market-component',
  templateUrl: './race-market.component.html',
  styleUrls: ['race-market.less']
})
export class RaceMarketComponent extends LadbrokesRaceMarketComponent {}
