import { LadbrokesDesktopRacingOutcomeCardComponent } from './racing-outcome-card.component';

describe('LadbrokesDesktopRacingOutcomeCardComponent', () => {
  let component: LadbrokesDesktopRacingOutcomeCardComponent;
  let gtmService;
  let filterService;
  let raceOutcomeDetailsService;

  beforeEach(() => {
    gtmService = {
      push: jasmine.createSpy('push')
    };
    filterService = {
      distance: jasmine.createSpy('distance').and.returnValue('test distance'),
      date: jasmine.createSpy('date').and.returnValue('2018-10-30')
    };
    raceOutcomeDetailsService = {
      isNumberNeeded: jasmine.createSpy('isNumberNeeded').and.returnValue(true),
      isGreyhoundSilk: jasmine.createSpy('isGreyhoundSilk').and.returnValue(false),
      getSilkStyle: jasmine.createSpy('getSilkStyle').and.returnValue('')
    };

    component = new LadbrokesDesktopRacingOutcomeCardComponent(
      raceOutcomeDetailsService,
      filterService,
      gtmService
    );
  });

  it('#addTrackEvent', () => {
    const expectedParams = ['trackEvent', {
      eventCategory: 'horse racing',
      eventAction: 'race card',
      eventLabel: 'details'
    }];

    component.addTrackEvent();
    expect(gtmService.push).toHaveBeenCalledWith(...expectedParams);
  });
});
