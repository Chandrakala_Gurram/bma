import { HttpClientModule } from '@angular/common/http';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { QuantumLeapComponent } from '@racing/components/quantumLeap/quantum-leap.component';
import { RacingTabYourcallComponent } from '@racing/components/racingTabYourcall/racing-tab-yourcall.component';
import { TimeFormSummaryComponent } from '@racing/components/timeformSummary/time-form-summary.component';
import { SortByOptionsService } from '@racing/services/sortByOptions/sort-by-options.service';
import { SharedModule } from '@sharedModule/shared.module';
import { RacingGaService } from '@racing/services/racing-ga.service';
import { RoutesDataSharingService } from '@racing/services/routesDataSharing/routes-data-sharing.service';
import { DesktopRacingMainComponent } from './components/racingMain/racing-main.component';
import { RacingTabsMainComponent } from '@ladbrokesMobile/racing/components/racingTabsMain/racing-tabs-main.component';
import { DailyRacingModuleComponent } from '@racing/components/dailyRacing/daily-racing.component';
import { RacingEnhancedMultiplesService } from '@racing/components/racingEnhancedMultiples/racing-enhanced-multiples.service';
import { RacingEnhancedMultiplesComponent
} from '@ladbrokesDesktop/racing/components/racingEnhancedMultiples/racing-enhanced-multiples.component';
import { SbModule } from '@sbModule/sb.module';
import { RacingRunService } from '@racing/services/racingRunService/racing-run.service';
import { RacingSpecialsCarouselComponent } from '@racing/components/racingSpecialsCarousel/racing-specials-carousel.component';
import { DesktopHorseracingTabsComponent } from '@ladbrokesDesktop/racing/components/horseracingTabs/horseracing-tabs.component';
import { DesktopGreyhoundsTabsComponent } from '@ladbrokesDesktop/racing/components/greyhoundsTabs/greyhounds-tabs.component';
import { DesktopModule } from '@desktop/desktop.module';
import { BuildRaceCardComponent } from '@ladbrokesDesktop/racing/components/buildRaceCard/build-race-card.component';
import { DesktopRacingAntepostTabComponent } from '@ladbrokesDesktop/racing/components/racingAntepostTab/racing-antepost-tab.component';
import { DesktopRacingEventComponent } from '@ladbrokesDesktop/racing/components/racingEventComponent/racing-event.component';
import { DesktopRacingEventModelComponent } from '@ladbrokesDesktop/racing/components/racingEventModel/racing-event-model.component';
import { BannersModule } from '@banners/banners.module';
import { GreyhoundFutureTabComponent } from '@racing/components/greyhound/greyhoundFutureTab/greyhound-future-tab.component';
import { QuickNavigationComponent } from '@racing/components/quickNavigation/quick-navigation.component';
import { LadbrokesRacingPostWidgetComponent } from '@ladbrokesMobile/racing/components/racingPostWidget/racing-post-widget.component';
import { RacingPostVerdictComponent } from '@ladbrokesMobile/racing/components/racingPostVerdict/racing-post-verdict.component';
import { RacingPostVerdictLabelComponent } from '@ladbrokesMobile/racing/components/racingPostVerdict/racing-post-verdict-label.component';
import { RacingResultsService } from '@core/services/sport/racing-results.service';
import { LadbrokesRacingEventMainComponent } from '@ladbrokesMobile/racing/components/racingEventMain/racing-event-main.component';
import { ForecastTricastGuard } from '@racing/guards/forecast-tricast-guard.service';
import { RacingPostPickComponent } from '@racing/components/racingPostPick/racing-post-pick.component';
// Platform app components / services
// tslint:disable: max-line-length
import {
  BuildYourRaceCardPageComponent
} from '@ladbrokesDesktop/racing/components/buildYourRaceCardPage/build-your-race-card-page.component';
import { BuildYourRaceCardPageService } from '@ladbrokesDesktop/racing/components/buildYourRaceCardPage/build-your-race-card-page.service';
import { StickyBuildCardDirective } from '@ladbrokesDesktop/racing/directives/sticky-build-card.directive';
import {
  DesktopRacingYourcallSpecialsComponent
} from '@ladbrokesDesktop/racing/components/racingYourcallSpecials/racing-yourcall-specials.component';
import {
  DesktopRacingSpecialsTabComponent
} from '@ladbrokesDesktop/racing/components/racingSpecialsTab/racing-specials-tab.component';
import { OffersAndFeaturedRacesComponent } from '@ladbrokesDesktop/racing/components/offersAndFeaturedRaces/offers-and-featured-races.component';
import { GreyhoundNextRacesTabGuard } from '@ladbrokesMobile/racing/guards/greyhound-next-races-tab-guard.service';
import { NextRacesTabGuard } from '@ladbrokesMobile/racing/guards/next-races-tab-guard.service';
import {
  DesktopRacingEventResultedComponent
} from '@ladbrokesDesktop/racing/components/racingEventResultedComponent/racing-event-resulted.component';
import {
  DesktopRacingOutcomeResultedCardComponent
} from '@ladbrokesDesktop/racing/components/racingOutcomeResultedCard/racing-outcome-resulted-card.component';
import { GreyhoundSpecialsTabComponent } from '@ladbrokesDesktop/racing/components/greyhoundSpecialsTab/greyhound-specials-tab.component';
import { RaceMarketComponent } from '@ladbrokesDesktop/racing/components/raceMarket/race-market.component';
import { LadbrokesRaceCardsControlsComponent } from '@ladbrokesDesktop/racing/components/race-cards-controls/race-cards-controls.component';

@NgModule({
  declarations: [
    // Overridden app components
    DesktopHorseracingTabsComponent,
    DesktopGreyhoundsTabsComponent,
    DesktopRacingMainComponent,
    DesktopRacingAntepostTabComponent,
    DesktopRacingEventComponent,
    RaceMarketComponent,
    // Platform app components
    DesktopRacingEventModelComponent,
    BuildYourRaceCardPageComponent,
    DesktopRacingYourcallSpecialsComponent,
    DesktopRacingSpecialsTabComponent,
    // Main app components
    GreyhoundSpecialsTabComponent,
    TimeFormSummaryComponent,
    RacingSpecialsCarouselComponent,
    QuickNavigationComponent,
    DailyRacingModuleComponent,
    RacingEnhancedMultiplesComponent,
    DesktopHorseracingTabsComponent,
    DesktopGreyhoundsTabsComponent,
    QuantumLeapComponent,
    LadbrokesRaceCardsControlsComponent,
    RacingTabYourcallComponent,
    RacingTabsMainComponent,
    RacingPostVerdictComponent,
    RacingPostVerdictLabelComponent,
    LadbrokesRacingEventMainComponent,
    DesktopRacingOutcomeResultedCardComponent,
    DesktopRacingEventResultedComponent,
    LadbrokesRacingPostWidgetComponent,
    BuildRaceCardComponent,
    StickyBuildCardDirective,
    OffersAndFeaturedRacesComponent,
    GreyhoundFutureTabComponent,
    RacingPostPickComponent,
  ],
  imports: [
    HttpClientModule,
    SharedModule,
    SbModule,
    DesktopModule,
    BannersModule
  ],
  exports: [
    // Overridden app components
    DesktopHorseracingTabsComponent,
    DesktopGreyhoundsTabsComponent,
    DesktopRacingMainComponent,
    DesktopRacingAntepostTabComponent,
    DesktopRacingEventComponent,
    RaceMarketComponent,
    // Platform app components
    DesktopRacingEventModelComponent,
    BuildYourRaceCardPageComponent,
    DesktopRacingYourcallSpecialsComponent,
    DesktopRacingSpecialsTabComponent,
    // Main app components
    GreyhoundSpecialsTabComponent,
    TimeFormSummaryComponent,
    RacingSpecialsCarouselComponent,
    QuickNavigationComponent,
    DailyRacingModuleComponent,
    RacingEnhancedMultiplesComponent,
    DesktopHorseracingTabsComponent,
    QuantumLeapComponent,
    LadbrokesRaceCardsControlsComponent,
    RacingTabYourcallComponent,
    RacingTabsMainComponent,
    RacingPostVerdictLabelComponent,
    RacingPostVerdictComponent,
    LadbrokesRacingEventMainComponent,
    DesktopRacingOutcomeResultedCardComponent,
    DesktopRacingEventResultedComponent,
    LadbrokesRacingPostWidgetComponent,
    StickyBuildCardDirective,
    OffersAndFeaturedRacesComponent,
    RacingPostPickComponent,
  ],
  providers: [
    RacingGaService,
    RoutesDataSharingService,
    RacingEnhancedMultiplesService,
    RacingRunService,
    SortByOptionsService,
    NextRacesTabGuard,
    ForecastTricastGuard,
    // Platform app services
    BuildYourRaceCardPageService,
    GreyhoundNextRacesTabGuard,
    RacingResultsService
  ],
  entryComponents: [
    // Overridden app components
    DesktopHorseracingTabsComponent,
    DesktopGreyhoundsTabsComponent,
    DesktopRacingMainComponent,
    DesktopRacingAntepostTabComponent,
    DesktopRacingEventComponent,
    RaceMarketComponent,
    // Platform app components
    DesktopRacingEventModelComponent,
    BuildYourRaceCardPageComponent,
    DesktopRacingYourcallSpecialsComponent,
    DesktopRacingSpecialsTabComponent,
    // Main app components
    GreyhoundSpecialsTabComponent,
    TimeFormSummaryComponent,
    LadbrokesRaceCardsControlsComponent,
    RacingSpecialsCarouselComponent,
    QuickNavigationComponent,
    DailyRacingModuleComponent,
    RacingEnhancedMultiplesComponent,
    QuantumLeapComponent,
    RacingTabYourcallComponent,
    BuildRaceCardComponent,
    RacingTabsMainComponent,
    LadbrokesRacingEventMainComponent,
    RacingPostVerdictComponent,
    RacingPostVerdictLabelComponent,
    DesktopRacingOutcomeResultedCardComponent,
    DesktopRacingEventResultedComponent,
    LadbrokesRacingPostWidgetComponent,
    OffersAndFeaturedRacesComponent,
    GreyhoundFutureTabComponent,
    RacingPostPickComponent,
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class RacingModule {
  constructor(racingRunService: RacingRunService) {
    racingRunService.run();
  }
}
