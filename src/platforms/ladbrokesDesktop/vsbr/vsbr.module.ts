import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from '@sharedModule/shared.module';
import { VsbrRoutingModule } from '@vsbrModule/vsbr-routing.module';
import { VirtualSportsService } from '@app/vsbr/services/virtual-sports.service';
import { LocalStorageMapperService } from '@app/vsbr/services/local-storage-mapper.service';
import { PanelStateService } from '@app/vsbr/services/panel-state.service';
import { EventProvider } from '@app/vsbr/services/event.provider';
import { VsVideoStreamComponent } from '@app/vsbr/components/vsVideoStream/vs-video-stream.component';
import { VirtualSportClassesComponent } from '@ladbrokesMobile/vsbr/components/virtualSportClasses/virtual-sport-classes.component';
import { VsOddsCardComponent } from '@ladbrokesMobile/vsbr/components/vsOddsCard/vs-odds-card.component';

// Overriden components
import { VirtualSportsPageComponent } from '@ladbrokesDesktop/vsbr/components/virtualSportsPage/virtual-sports-page.component';
import { VirtualCarouselMenuComponent } from '@ladbrokesDesktop/vsbr/components/virtualCarouselMenu/virtual-carousel-menu.component';
import { VirtualSportsMapperService } from '@app/vsbr/services/virtual-sports-mapper.service';
import { VirtualMenuDataService } from '@app/vsbr/services/virtual-menu-data.service';
import {
  VirtualCarouselSubMenuComponent
} from '@ladbrokesDesktop/vsbr/components/virtualCarouselSubMenu/virtual-carousel-sub-menu.component';
import { CountdownHeaderComponent } from '@root/app/vsbr/components/countdownHeader/countdown-header.component';


@NgModule({
  declarations: [
    VsVideoStreamComponent,
    CountdownHeaderComponent,
    // Overridden
    VirtualSportsPageComponent,
    VirtualCarouselMenuComponent,
    VirtualSportClassesComponent,
    VsOddsCardComponent,
    VirtualCarouselSubMenuComponent
  ],
  entryComponents: [
    VsVideoStreamComponent,
    CountdownHeaderComponent,
    // Overridden
    VirtualSportsPageComponent,
    VirtualCarouselMenuComponent,
    VirtualSportClassesComponent,
    VsOddsCardComponent,
    VirtualCarouselSubMenuComponent
  ],
  exports: [
    // Overridden
    VirtualSportsPageComponent,
    VirtualCarouselMenuComponent,
    VirtualSportClassesComponent,
    VsOddsCardComponent,
    VirtualCarouselSubMenuComponent
  ],
  imports: [
    SharedModule,
    VsbrRoutingModule
  ],
  providers: [
    EventProvider,
    PanelStateService,
    LocalStorageMapperService,
    VirtualSportsService,
    VirtualSportsMapperService,
    VirtualMenuDataService
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class VsbrModule {}
