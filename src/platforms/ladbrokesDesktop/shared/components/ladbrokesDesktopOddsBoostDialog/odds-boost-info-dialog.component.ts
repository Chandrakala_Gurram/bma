import { Component } from '@angular/core';

import { DeviceService } from '@core/services/device/device.service';
import { OddsBoostInfoDialogComponent } from '@shared/components/oddsBoostInfoDialog/odds-boost-info-dialog.component';
import { LocaleService } from '@core/services/locale/locale.service';
import { WindowRefService } from '@core/services/windowRef/window-ref.service';
import { StorageService } from '@root/app/core/services/storage/storage.service';
import { UserService } from '@root/app/core/services/user/user.service';

@Component({
  selector: 'odds-boost-info-dialog',
  templateUrl: './odds-boost-info-dialog.component.html',
  styleUrls: ['./odds-boost-info-dialog.component.less']
})
export class DesktopOddsBoostInfoDialogComponent extends OddsBoostInfoDialogComponent {

  constructor(
    device: DeviceService,
    localeService: LocaleService,
    windowRef: WindowRefService,
    storageService: StorageService,
    userService: UserService
  ) {
    super(
      device,
      localeService,
      windowRef,
      storageService,
      userService
    );
  }

}
