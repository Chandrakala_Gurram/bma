import { Component } from '@angular/core';
import {
  RacingStatusComponent as LadbrokesRacingStatusComponent
} from '@ladbrokesMobile/shared/components/racingPanel/racing-status.component';

@Component({
  selector: 'racing-status',
  templateUrl: '../../../../ladbrokesMobile/shared/components/racingPanel/racing-status.component.html',
  styleUrls: ['./racing-status.component.less']
})
export class RacingStatusComponent extends LadbrokesRacingStatusComponent {}

