import { Component, ViewEncapsulation } from '@angular/core';
import { LadbrokesAccordionComponent } from '@ladbrokesMobile/shared/components/accordion/accordion.component';

@Component({
  selector: 'accordion',
  templateUrl: 'accordion.component.html',
  styleUrls: ['accordion.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class AccordionComponent extends LadbrokesAccordionComponent {
}

