import { RaceCardHomeComponent } from './race-card-home.component';

describe('RaceCardHomeComponent', () => {
  let component: RaceCardHomeComponent,
      nextRacesHomeService,
      router,
      localeService,
      raceDataMock,
      pubSubService,
      sbFiltersService;

  const raceOutcomeDetails = {} as any,
        routingHelperService = {} as any,
        filtersService = {} as any,
        eventService = {} as any,
        virtualSharedService = { isVirtual: () => false } as any,
        datePipe = { transform: () => ''} as any;

  beforeEach(() => {
    raceDataMock = [
      {
        markets: [
          {
            id: '12345',
            outcomes: []
          }
        ]
      }
    ];
    nextRacesHomeService = jasmine.createSpyObj(['trackNextRace']);
    router = jasmine.createSpyObj(['navigateByUrl']);
    localeService = {
      getString: jasmine.createSpy('getString').and.returnValue('More')
    };
    sbFiltersService = {
      orderOutcomeEntities: jasmine.createSpy('orderOutcomeEntities').and.returnValue([{
        id: 'outcome1'
      },
        {
          id: 'outcome2'
        }])
    };
    pubSubService = {
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: {
        OUTCOME_UPDATED: 'OUTCOME_UPDATED'
      }
    };

    component = new RaceCardHomeComponent(
      raceOutcomeDetails,
      routingHelperService,
      nextRacesHomeService,
      localeService,
      sbFiltersService,
      filtersService,
      pubSubService,
      router,
      eventService,
      virtualSharedService,
      datePipe
    );
    (component['_raceData'] as any) = raceDataMock;
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    component.ngOnInit();
    expect(component['viewFullRaceText']).toBe('More');
  });

  describe('trackEvent', () => {
    const entity = {} as any;

    beforeEach(() => {
      spyOn(component, 'formEdpUrl').and.returnValue('formEdpUrl');
      component.moduleType = 'moduleType';
    });

    it(`should run input track function`, () => {
      component.trackFunction = jasmine.createSpy();

      component.trackEvent(entity);

      expect(component.trackFunction).toHaveBeenCalledWith(entity);
      expect(nextRacesHomeService.trackNextRace).not.toHaveBeenCalled();
      expect(router.navigateByUrl).not.toHaveBeenCalled();
    });

    it(`should trackNextRace`, () => {
      component.trackEvent(entity);

      expect(nextRacesHomeService.trackNextRace).toHaveBeenCalledWith(entity, component.moduleType);
    });

    it(`should formEdpUrl`, () => {
      component.trackEvent(entity);

      expect(component.formEdpUrl).toHaveBeenCalledWith(entity);
    });

    it(`should navigate to formatted link`, () => {
      component.trackEvent(entity);

      expect(router.navigateByUrl).toHaveBeenCalledWith('formEdpUrl');
    });
  });

  it('formatEventTerms', () => {
    expect(component.formatEventTerms('Each Way: 1/4 odds - places 1,2')).toEqual('E/W 1/4 Places 1-2');
  });
});
