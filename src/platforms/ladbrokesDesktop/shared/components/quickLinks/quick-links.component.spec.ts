import { QuickLinksComponent } from './quick-links.component';
import { IDesktopQuickLink } from '@core/services/cms/models';
import { of } from 'rxjs';

describe('LadbrokesQuickLinksComponent', () => {
  let cms, component;

  const quickLinks: IDesktopQuickLink[] = [
    {
      title: 'Football',
      target: 'sport/football'
    },
    {
      title: 'BasketBall',
      target: '/sport/basketball'
    }
  ] as IDesktopQuickLink[];

  beforeEach(() => {
    cms = {
      getDesktopQuickLinks: jasmine.createSpy('cmsQuickLinks').and.returnValue(of(quickLinks))
    };

    component = new QuickLinksComponent(cms);
  });

  describe('onInit', () => {
    it('should load and sanitize quicklinks', () => {
      component.ngOnInit();
      expect(component.quickLinks[0].target).toEqual('/sport/football');
      expect(component.quickLinks[1].target).toEqual('/sport/basketball');
    });
  });
});
