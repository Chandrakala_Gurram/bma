import { map } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { ModuleExtensionsStorageService } from '@core/services/moduleExtensionsStorage/module-extensions-storage.service';
import { CasinoLinkService } from '@core/services/casinoLink/casino-link.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { AzSportsPageComponent } from '@app/shared/components/azSportPage/az-sports-page.component';
import { IVerticalMenu } from '@root/app/core/services/cms/models';
import { GermanSupportService } from '@app/core/services/germanSupport/german-support.service';
import { PubSubService } from '@app/core/services/communication/pubsub/pubsub.service';

interface IAZData {
  azItems: IVerticalMenu[];
  topItems: IVerticalMenu[];
}

@Component({
  selector: 'az-sports-page',
  templateUrl: './az-sports-page.component.html',
  styleUrls: ['./az-sports-page.component.less']
})
export class LadbrokesAzSportsPageComponent extends AzSportsPageComponent implements OnInit, OnDestroy {
  constructor(
    protected moduleExtensionsStorageService: ModuleExtensionsStorageService,
    protected cmsService: CmsService,
    protected casinoLinkService: CasinoLinkService,
    private germanSupportService: GermanSupportService,
    private pubSubService: PubSubService
  ) {
    super(moduleExtensionsStorageService, cmsService, casinoLinkService);
  }
  ngOnInit(): void {
    this.loadAzData();
    this.pubSubService.subscribe('LadbrokesAzSportsPageComponent',
      [this.pubSubService.API.SESSION_LOGIN, this.pubSubService.API.SESSION_LOGOUT], () => {
        this.loadAzData();
      }
    );
  }

  loadAzData(): void {
    this.cmsService.getMenuItems().pipe(
      map(this.processMenu))
      .subscribe((data: IAZData) => {
        this.topItems = data.topItems.length ? this.filterRestrictedSports(data.topItems) : null;
        this.azItems = data.azItems.length ? this.filterRestrictedSports(data.azItems) : null;
      });
  }

  filterRestrictedSports(items: IVerticalMenu[]): IVerticalMenu[] {
    return this.germanSupportService.toggleItemsList(items, 'filterRestrictedSports');
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe('LadbrokesAzSportsPageComponent');
   }
}
