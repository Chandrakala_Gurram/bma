import { RootComponent } from './app.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('AppComponent', () => {
  let component;
  let vanillaPortalModuleLoadNotifier;
  let appRef;
  let ngZone;
  let pubSubService;
  let router;

  beforeEach(() => {
    router = {
      events: {
        subscribe: jasmine.createSpy('subscribe')
      }
    };

    pubSubService = {
      subscribe: jasmine.createSpy('subscribe'),
      API: pubSubApi
    };
    vanillaPortalModuleLoadNotifier = {
      loadPortalModule: jasmine.createSpy('loadPortalModule')
    };
    appRef = {};
    ngZone = {};

    component = new RootComponent(
      vanillaPortalModuleLoadNotifier,
      appRef,
      ngZone,
      pubSubService,
      router
    );
  });

  it('ngAfterViewInit', () => {
    component.ngAfterViewInit();
    expect(vanillaPortalModuleLoadNotifier.loadPortalModule).toHaveBeenCalled();
  });
});
