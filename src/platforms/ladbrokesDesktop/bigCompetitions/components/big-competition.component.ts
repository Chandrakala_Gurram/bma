import { Component } from '@angular/core';

import { BigCompetitionComponent } from '@app/bigCompetitions/components/bigCompetition/big-competition.component';

@Component({
  selector: 'big-competition',
  templateUrl: 'big-competition.component.html'
})
export class DesktopBigCompetitionComponent extends BigCompetitionComponent {}
