import { of as observableOf } from 'rxjs';
import { LeftMenuComponent } from '@ladbrokesDesktop/desktop/components/leftMenu/left-menu.component';
import { category } from './category.mock';

describe('LeftMenuComponentLadbrokes', () => {
  let component;
  let navigationService;
  let leftMenuService;
  let germanSupportService;
  let pubSubService;
  let changeDetectorRef;

  beforeEach(() => {
    germanSupportService = {
      toggleItemsList: jasmine.createSpy('toggleItemsList').and.returnValue([category])
    };
    leftMenuService = {
      getMenuItems: jasmine.createSpy('getMenuItems').and.returnValue(observableOf([category]))
    };
    navigationService = jasmine.createSpyObj(['openUrl']);
    changeDetectorRef = {
      markForCheck: jasmine.createSpy('markForCheck')
    };
    pubSubService = {
      subscribe: jasmine.createSpy('subscribe').and.callFake((arg1, arg2, callback) => callback()),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: {
        SESSION_LOGIN: 'SESSION_LOGIN',
        SESSION_LOGOUT: 'SESSION_LOGOUT'
      }
    };

    component = new LeftMenuComponent(
      navigationService,
      leftMenuService,
      germanSupportService,
      pubSubService,
      changeDetectorRef);
  });

  it('should create component instance', () => {
    expect(component).toBeTruthy();
  });

  it('should use OnPush strategy', () => {
    expect(LeftMenuComponent['__annotations__'][0].changeDetection).toBe(0);
  });

  it('#ngOnInit', () => {
    component['setTargetUriParts'] = jasmine.createSpy().and.callFake((item) => item);
    component.filterRestrictedSports = jasmine.createSpy();
    component.ngOnInit();
    expect(leftMenuService.getMenuItems).toHaveBeenCalled();
    expect(component['setTargetUriParts']).toHaveBeenCalledWith(category);
    expect(component.filterRestrictedSports).toHaveBeenCalledWith(component.menuItems);
    expect(component.menuItems).toEqual([category]);
  });

  it('#ngOnDestroy', () => {
    component.ngOnDestroy();
    expect(pubSubService.unsubscribe).toHaveBeenCalledWith('LeftMenuComponent');
  });

  it('#filterRestrictedSports, should call correct methods', () => {
    component.filterRestrictedSports([category]);
    expect(component.menuItems).toEqual([category]);
    expect(germanSupportService.toggleItemsList).toHaveBeenCalledTimes(2);
    expect(pubSubService.subscribe).toHaveBeenCalledWith(
      'LeftMenuComponent',
      jasmine.arrayContaining([pubSubService.API.SESSION_LOGIN, pubSubService.API.SESSION_LOGOUT]),
      jasmine.any(Function)
    );
    expect(component['changeDetectorRef'].markForCheck).toHaveBeenCalled();
  });

  it('#trackById, should return correct result', () => {
    const menuItem = { id: '14' } as any;
    const index = 3;
    expect(component.trackById(index, menuItem)).toBe('14');
  });

  it('#setTargetUriParts, should extend category with targetUriParts property', () => {
    const result = component['setTargetUriParts'](category);
    expect(result.targetUriParts).toEqual(jasmine.arrayContaining(['/', 'sport', 'american-football']));
  });
});
