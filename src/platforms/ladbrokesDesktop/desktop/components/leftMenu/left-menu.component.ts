import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { map } from 'rxjs/operators';

import { ISportCategory } from '@core/services/cms/models/sport-category.model';
import { LeftMenuService } from './left-menu.service';
import { NavigationService } from '@core/services/navigation/navigation.service';
import { GermanSupportService } from '@root/app/core/services/germanSupport/german-support.service';
import { PubSubService } from '@root/app/core/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.less'],
  providers: [
    LeftMenuService,
    GermanSupportService
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LeftMenuComponent implements OnInit, OnDestroy {

  menuItems: ISportCategory[];
  private readonly title = 'LeftMenuComponent';

  constructor(
    public navigationService: NavigationService,
    private leftMenuService: LeftMenuService,
    private germanSupportService: GermanSupportService,
    private pubSubService: PubSubService,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.leftMenuService.getMenuItems().pipe(
      map((items: ISportCategory[]) => items.map(item => this.setTargetUriParts(item)))
    ).subscribe((menuItems: ISportCategory[]) => {
      this.menuItems = menuItems;
      this.filterRestrictedSports(this.menuItems);
    });
  }

  ngOnDestroy(): void {
    this.pubSubService.unsubscribe(this.title);
  }

  /**
   * Hide HR/GH sports for German Users
   */

  filterRestrictedSports(menuItems: ISportCategory[]): void {
    this.menuItems = this.germanSupportService.toggleItemsList(menuItems, 'filterRestrictedSports');
    this.changeDetectorRef.markForCheck();
    this.pubSubService.subscribe(this.title,
    [this.pubSubService.API.SESSION_LOGIN, this.pubSubService.API.SESSION_LOGOUT], () => {
      this.menuItems = this.germanSupportService.toggleItemsList(menuItems, 'filterRestrictedSports');
        this.changeDetectorRef.markForCheck();
      }
    );
  }

  /**
   * ngFor trackBy function
   * @param {number} index
   * @param {ISportCategory} menuItem
   * @return {number}
   */
  trackById(index: number, menuItem: ISportCategory): string {
    return menuItem.id;
  }

  /**
   * extends category object with targetUriParts property
   * @param category
   */
  private setTargetUriParts(category: ISportCategory): ISportCategory {
    return {
      ...category,
      targetUriParts: [
        '/',
        ...category.targetUri.split('/').filter(param => param)
      ]
    };
  }
}
