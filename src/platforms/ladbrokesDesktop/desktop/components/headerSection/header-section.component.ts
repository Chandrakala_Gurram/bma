import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { IHeaderSubMenu } from '@core/services/cms/models';

import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { CmsService } from '@coreModule/services/cms/cms.service';
import { FiltersService } from '@core/services/filters/filters.service';
import { NavigationService } from '@core/services/navigation/navigation.service';
import { GermanSupportService } from '@core/services/germanSupport/german-support.service';

@Component({
  selector: 'header-section',
  templateUrl: './header-section.component.html',
  styleUrls: [ './header-section.component.less' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderSectionComponent implements OnInit, OnDestroy {
  headerSubLinks: IHeaderSubMenu[];
  oddsFormat: string;
  headerSubMenuIsExists = false;

  private allSubLinks: IHeaderSubMenu[] = [];
  private unsubscribe: Subject<void> = new Subject();

  constructor(
    private filtersService: FiltersService,
    private cmsService: CmsService,
    private pubsub: PubSubService,
    private germanSupportService: GermanSupportService,
    private navigationService: NavigationService,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.cmsService.getHeaderSubMenu().pipe(takeUntil(this.unsubscribe))
      .subscribe((data: IHeaderSubMenu[]) => {
        this.allSubLinks = data;
        this.filterHeaderData();
      });

    this.pubsub.subscribe('HeaderSectionComponent',
      [this.pubsub.API.SESSION_LOGIN, this.pubsub.API.SESSION_LOGOUT], () => {
        this.filterHeaderData();
      });
  }

  ngOnDestroy(): void {
    this.pubsub.unsubscribe('HeaderSectionComponent');
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  trackByLink(item: IHeaderSubMenu): string {
    return item.linkTitle;
  }

  /**
   * Pre-redirect callback function before redirect.
   * @param {string} url // if url from coral portal then we need add get params before redirect.
   * @param {boolean} inApp (true/false)
   * @param {string} linkTitle
   * @param {string} eventAction
   */
  goToURL(url: string, inApp: boolean, linkTitle: string, eventAction: string = 'header'): void {
    this.navigationService.openUrl(url, inApp);
    this.navigationService.trackGTMEvent(eventAction, linkTitle);
  }

  /**
   * Filter set correct links
   * @private
   */
  private filterLinks(): void {
    if (!this.headerSubMenuIsExists) {
      return;
    }
    this.headerSubLinks = this.headerSubLinks.map((headerSubLink: IHeaderSubMenu) => {
      headerSubLink.targetUri = this.filtersService.filterLink(headerSubLink.targetUri);
      return headerSubLink;
    });
  }

  private filterHeaderData(): void {
    this.headerSubLinks = this.germanSupportService.toggleItemsList(this.allSubLinks, 'filterRestrictedSports');
    this.headerSubMenuIsExists = this.headerSubLinks && !!this.headerSubLinks.length;
    this.filterLinks();
    this.changeDetectorRef.detectChanges();
  }
}
