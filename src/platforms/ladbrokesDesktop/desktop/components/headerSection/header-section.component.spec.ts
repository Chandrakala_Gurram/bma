import { of  } from 'rxjs';
import { HeaderSectionComponent } from './header-section.component';
import { IHeaderSubMenu } from '@core/services/cms/models';
import { fakeAsync, tick } from '@angular/core/testing';

describe('LDHeaderSectionComponent', () => {
  let component: HeaderSectionComponent;
  let headerSubMenu: IHeaderSubMenu[];

  let cmsService, filtersService,
    germanSupportService, navigationService, pubsub, changeDetectorRef;

  beforeEach(() => {
    headerSubMenu = [{
      disabled: false,
      lang: 'lang',
      linkTitle: 'linkTitle',
      linkTitle_brand: 'linkTitle_brand',
      sortOrder: 0,
      targetUri: 'targetUri',
      inApp: false,
      id: 'id',
      brand: 'brand',
      createdBy: 'createdBy',
      createdAt: 'createdAt',
      updatedBy: 'updatedBy',
      updatedAt: 'updatedAt',
      updatedByUserName: 'updatedByUserName',
      createdByUserName: 'createdByUserName',
      target: 'target',
      targetUriCopy: 'targetUriCopy',
      sportName: 'sportName',
      relUri: false,
      svgId: 'svgId',
    }];

    cmsService = {
      getHeaderSubMenu: jasmine.createSpy('getHeaderSubMenu').and.returnValue(of(headerSubMenu))
    } as any;

    filtersService = {
      filterLink: jasmine.createSpy('filterLink').and.returnValue('lorem'),
    } as any;

    germanSupportService = {
      toggleItemsList: jasmine.createSpy('toggleItemsList').and.returnValue(headerSubMenu),
    };

    navigationService = {
      doRedirect: jasmine.createSpy('doRedirect'),
      sendToGTM: jasmine.createSpy('sendToGTM'),
      trackGTMEvent: jasmine.createSpy('trackGTMEvent'),
      openUrl: jasmine.createSpy('openUrl')
    } as any;

    pubsub = {
      subscribe: jasmine.createSpy('subscribe').and.callFake((subscriberName, channel, cb) => cb()),
      unsubscribe: jasmine.createSpy('unsubscribe'),
      API: {
        SESSION_LOGIN: 'SESSION_LOGIN',
        SESSION_LOGOUT: 'SESSION_LOGOUT',
      },
    } as any;

    changeDetectorRef = {
      detectChanges: jasmine.createSpy('detectChanges'),
    } as any;

    component = new HeaderSectionComponent(
      filtersService,
      cmsService,
      pubsub,
      germanSupportService,
      navigationService,
      changeDetectorRef
    );
  });

  describe('#ngOnInit', () => {
    it('should execute ngOnInit, and headerSubMenuIsExists should be truthy', fakeAsync(() => {
      component.ngOnInit();
      tick();

      expect(germanSupportService.toggleItemsList).toHaveBeenCalledWith(headerSubMenu, 'filterRestrictedSports');
      expect(component.headerSubLinks).toEqual(headerSubMenu);
      expect(component.headerSubMenuIsExists).toBeTruthy();

      expect(pubsub.subscribe).toHaveBeenCalledWith('HeaderSectionComponent',
        [component['pubsub'].API.SESSION_LOGIN, component['pubsub'].API.SESSION_LOGOUT], jasmine.any(Function));
    }));

    it('should execute ngOnInit, and headerSubMenuIsExists should be falsy', () => {
      germanSupportService.toggleItemsList.and.returnValue(of([]));
      component.ngOnInit();

      expect(component.headerSubMenuIsExists).toBeFalsy();
    });

  });

  it('#ngOnDestroy, should call unsubscribe', () => {
    component['unsubscribe'].next = jasmine.createSpy();
    component['unsubscribe'].complete = jasmine.createSpy();
    component.ngOnDestroy();

    expect(component['pubsub'].unsubscribe).toHaveBeenCalled();
    expect(component['unsubscribe'].next).toHaveBeenCalled();
    expect(component['unsubscribe'].complete).toHaveBeenCalled();
  });

  describe('getToURL', () => {
    it('should delegate url opening to service (inApp)', () => {
      component.goToURL('', true, '');

      expect(navigationService.openUrl).toHaveBeenCalledWith('', true);
    });

    it('should delegate url opening to service', () => {
      component.goToURL('foo', false, '');

      expect(navigationService.openUrl).toHaveBeenCalledWith('foo', false);
    });

    it('should delegate tracking to service (default action)', () => {
      component.goToURL('', true, 'title');

      expect(navigationService.trackGTMEvent).toHaveBeenCalledWith('header', 'title');
    });

    it('should delegate tracking to service (custom action)', () => {
      component.goToURL('', true, 'title', 'main');

      expect(navigationService.trackGTMEvent).toHaveBeenCalledWith('main', 'title');
    });
  });

  it('#trackByLink, should return link title', () => {
    expect(component.trackByLink(headerSubMenu[0])).toEqual(headerSubMenu[0].linkTitle);
  });

  it('#filterLinks, should set correct link', () => {
    component.headerSubLinks = [{} as any];

    component['filterLinks']();
    expect(component.headerSubLinks[0].targetUri).toBe(undefined);

    component.headerSubLinks = headerSubMenu;
    component.headerSubMenuIsExists = true;
    component['filterLinks']();
    expect(component.headerSubLinks[0].targetUri).toBe('lorem');
  });
});
