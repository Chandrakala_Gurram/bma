import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'angular-custom-modal';

import { DYNAMIC_SLIDE_OUT_BETSLIP } from '@app/dynamicLoader/dynamic-loader-manifest';
import { SharedModule } from '@sharedModule/shared.module';

import { BetslipRoutingModule } from '@app/betslip/betslip-routing.module';
import { InitBetslipService } from '@app/betslip/services/initBetslip/init-betslip.service';

import { BetslipDigitKeyboardDirective } from '@app/betslip/directives/betslip-digit-keyboard.directive';
import { BetslipBannerComponent } from '@app/betslip/components/betslipBanner/betslip-banner.component';
import { BetslipLimitationDialogComponent } from '@betslipModule/components/betslipLimitationDialog/betslip-limitation-dialog.component';
import { BsNotificationComponent } from '@app/betslip/components/bsNotification/bs-notification.component';
import { MaxStakeDialogComponent } from '@betslipModule/components/maxStakeDialog/max-stake-dialog.component';
import { OverAskNotificationDialogComponent } from '@betslip/components/overaskNotificationDialog/over-ask-notification-dialog.component';
import { OveraskOfferNotificationComponent } from '@betslip/components/overaskOfferNotification/over-ask-offer-notification.component';
import { ToteBetReceiptComponent } from '@app/betslip/components/toteBetReceipt/tote-bet-receipt.component';
import {
  LadbrokesToteBetReceiptItemComponent
} from '@ladbrokesMobile/betslip/components/toteBetReceiptItem/tote-bet-receipt-item.component';
import { VoucherComponent } from '@app/betslip/components/voucher/voucher.component';
import { SlideOutBetslipComponent } from '@app/betslip/components/slideOutBetslip/slide-out-betslip.component';
import { BetslipContainerComponent } from '@app/betslip/components/betslipContainer/betslip-container.component';
import { IncorrectPatternComponent } from '@app/betslip/components/incorrectPattern/incorrect-pattern.component';
import { AddToBetslipComponent } from '@app/betslip/components/addToBetslip/add-to-betslip.component';
import { BetslipFctcListComponent } from '@app/betslip/components/betslipFctcList/betslip-fctc-list.component';
import {
  LadbrokesBetslipSinglesReceiptComponent
} from '@ladbrokesMobile/betslip/components/betslipSinglesReceipt/betslip-singles-receipt.component';
import {
  LadbrokesBetslipMultiplesReceiptComponent
} from '@ladbrokesMobile/betslip/components/betslipMultiplesReceipt/betslip-multiples-receipt.component';

import { LadbrokesBetslipComponent } from '@ladbrokesMobile/betslip/components/betslip/betslip.component';
import { LadbrokesBetslipReceiptComponent } from '@ladbrokesMobile/betslip/components/betslipReceipt/betslip-receipt.component';
import {
  LadbrokesBetslipTotalWrapperComponent
} from '@ladbrokesMobile/betslip/components/betslipTotalWrapper/betslip-total-wrapper.component';
import {
  LadbrokesBetslipSubheaderComponent
} from '@ladbrokesMobile/betslip/components/betslipSubheader/betslip-subheader.component';
import {
  LadbrokesEmptyBetslipComponent
} from '@ladbrokesMobile/betslip/components/emptyBetslip/empty-betslip.component';
import {
  BetslipReceiptSubheaderComponent
} from '@ladbrokesMobile/betslip/components/betslipReceiptSubheader/betslip-receipt-subheader.component';
import {
  LadbrokesSelectionInfoDialogComponent
} from '@ladbrokesMobile/betslip/components/selectionInfoDialog/selection-info-dialog.component';
import {
  BetslipOverlayNotificationComponent
} from '@ladbrokesMobile/betslip/components/betslipOverlayNotification/betslip-overlay-notification.component';
import { OveraskHoldingDrawerComponent } from '@betslip/components/overask-holding-drawer/overask-holding-drawer.component';
import { BetslipOfferedDataComponent } from '@ladbrokesMobile/betslip/components/betslip-offered-data/betslip-offered-data.component';
import {
  AccaInsuranceSubtitleComponent
} from '@ladbrokesMobile/betslip/components/accaInsuranceSubtitle/acca-insurance-subtitle.component';
import { BetslipMultipleBetPartsComponent } from '@betslip/components/betslipMultipleBetPart/betslip-multiple-bet-parts.component';
import {
  BetslipDepositErrorContainerComponent
} from '@ladbrokesMobile/betslip/components/betslipDepositErrorContainer/betslip-deposit-error-container.component';
import { ReceiptHeaderModule } from '@ladbrokesDesktop/lazy-modules/receiptHeader/receipt-header.module';
import { QuickDepositService } from '@betslipModule/services/quickDeposit/quick-deposit.service';
import { DeclinedBetComponent } from './components/declined-bet/declined-bet.component';

@NgModule({
  declarations: [
    BetslipDigitKeyboardDirective,
    BetslipBannerComponent,
    BetslipLimitationDialogComponent,
    BsNotificationComponent,
    MaxStakeDialogComponent,
    OverAskNotificationDialogComponent,
    OveraskOfferNotificationComponent,
    ToteBetReceiptComponent,
    VoucherComponent,
    BetslipContainerComponent,
    SlideOutBetslipComponent,
    LadbrokesToteBetReceiptItemComponent,
    LadbrokesBetslipComponent,
    LadbrokesBetslipReceiptComponent,
    AddToBetslipComponent,
    IncorrectPatternComponent,
    LadbrokesBetslipTotalWrapperComponent,
    LadbrokesBetslipSubheaderComponent,
    LadbrokesEmptyBetslipComponent,
    LadbrokesBetslipSinglesReceiptComponent,
    LadbrokesBetslipMultiplesReceiptComponent,
    LadbrokesSelectionInfoDialogComponent,
    BetslipReceiptSubheaderComponent,
    BetslipFctcListComponent,
    BetslipOfferedDataComponent,
    OveraskHoldingDrawerComponent,
    BetslipMultipleBetPartsComponent,
    BetslipOverlayNotificationComponent,
    DeclinedBetComponent,
    AccaInsuranceSubtitleComponent,
    BetslipDepositErrorContainerComponent
  ],
  exports: [],
  entryComponents: [
    BetslipBannerComponent,
    BetslipLimitationDialogComponent,
    BsNotificationComponent,
    MaxStakeDialogComponent,
    BetslipContainerComponent,
    OverAskNotificationDialogComponent,
    OveraskOfferNotificationComponent,
    ToteBetReceiptComponent,
    VoucherComponent,
    SlideOutBetslipComponent,
    LadbrokesToteBetReceiptItemComponent,
    LadbrokesBetslipComponent,
    LadbrokesBetslipReceiptComponent,
    AddToBetslipComponent,
    IncorrectPatternComponent,
    LadbrokesBetslipTotalWrapperComponent,
    LadbrokesBetslipSubheaderComponent,
    LadbrokesEmptyBetslipComponent,
    LadbrokesBetslipSinglesReceiptComponent,
    LadbrokesBetslipMultiplesReceiptComponent,
    LadbrokesSelectionInfoDialogComponent,
    BetslipOverlayNotificationComponent,
    BetslipReceiptSubheaderComponent,
    BetslipFctcListComponent,
    BetslipMultipleBetPartsComponent,
    AccaInsuranceSubtitleComponent,
    BetslipDepositErrorContainerComponent,
    DeclinedBetComponent
  ],
  imports: [
    SharedModule,
    ModalModule,
    FormsModule,
    BetslipRoutingModule,
    ReceiptHeaderModule
  ],
  providers: [
    { provide: DYNAMIC_SLIDE_OUT_BETSLIP, useValue: SlideOutBetslipComponent },
    QuickDepositService
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class BetslipModule {
  static entry = {
    BetslipContainerComponent
  };

  constructor(initBetslip: InitBetslipService) {
    initBetslip.init();
  }
}
