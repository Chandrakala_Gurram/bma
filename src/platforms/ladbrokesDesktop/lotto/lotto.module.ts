import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'angular-custom-modal';

import { SharedModule } from '@sharedModule/shared.module';
import { DesktopModule } from '@ladbrokesDesktop/desktop/desktop.module';

import { SimpleFiltersService } from '@ss/services/simple-filters.service';
import { SegmentDataUpdateService } from '@app/lotto/services/segmentDataUpdate/segment-data-update.service';
import { LottoResultsService } from '@app/lotto/services/lottoResults/lotto-results.service';
import { LottoBetService } from '@app/lotto/services/lottoBet/lotto-bet.service';
import { LottoReceiptService } from '@app/lotto/services/lottoReceipt/lotto-receipt.service';
import { MainLottoService } from '@app/lotto/services/mainLotto/main-lotto.service';
import { SiteServerLottoService } from '@app/lotto/services/siteServerLotto/site-server-lotto.service';
import { BuildLotteriesService } from '@app/lotto/services/buildLotteries/build-lotteries.service';
import { NumberSelectorComponent } from '@app/lotto/components/numberSelector/number-selector.component';
import { LottoSegmentPageComponent } from '@app/lotto/components/lottoSegmentPage/lotto-segment-page.component';
import { LottoResultsFilterPageComponent } from '@app/lotto/components/lottoResulsFilterPage/lotto-results-filter-page.component';
import { LottoResultsPageComponent } from '@app/lotto/components/lottoResultsPage/lotto-results-page.component';
import { LottoReceiptComponent } from '@app/lotto/components/lottoReceipt/lotto-receipt.component';
import { LottoResultsComponent } from '@app/lotto/components/lottoResults/lotto-results.component';
import { LottoNumberSelectorComponent } from '@lottoModule/components/lottoNumberSelectorDialog/lotto-number-selector-dialog.component';
import { BannersModule } from '@banners/banners.module';

// Desktop
import { LottoRoutingModule } from './lotto-routing.module';
import { DesktopLottoMainComponent } from '@ladbrokesDesktop/lotto/components/lottoMain/lotto-main.component';

@NgModule({
  declarations: [
    LottoResultsComponent,
    LottoReceiptComponent,
    LottoResultsPageComponent,
    LottoResultsFilterPageComponent,
    LottoSegmentPageComponent,
    NumberSelectorComponent,
    LottoNumberSelectorComponent,

    // Overridden
    DesktopLottoMainComponent,
  ],
  imports: [
    ModalModule,
    SharedModule,
    FormsModule,
    LottoRoutingModule,
    DesktopModule,
    BannersModule
  ],
  exports: [
    LottoResultsComponent,
    LottoReceiptComponent,
    LottoResultsPageComponent,
    LottoResultsFilterPageComponent,
    LottoSegmentPageComponent,
    NumberSelectorComponent,
    LottoNumberSelectorComponent,

    // Overridden
    DesktopLottoMainComponent,
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  entryComponents: [
    LottoResultsComponent,
    LottoReceiptComponent,
    LottoResultsPageComponent,
    LottoResultsFilterPageComponent,
    LottoSegmentPageComponent,
    NumberSelectorComponent,
    LottoNumberSelectorComponent,

    // Overridden
    DesktopLottoMainComponent,
  ],
  providers: [
    BuildLotteriesService,
    SiteServerLottoService,
    MainLottoService,
    LottoReceiptService,
    LottoBetService,
    LottoResultsService,
    SegmentDataUpdateService,
    SimpleFiltersService
  ],
})
export class LottoModule {}
