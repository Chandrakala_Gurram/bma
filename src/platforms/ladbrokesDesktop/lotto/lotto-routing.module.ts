import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LottoSegmentPageComponent } from '@app/lotto/components/lottoSegmentPage/lotto-segment-page.component';
import { LottoReceiptComponent } from '@app/lotto/components/lottoReceipt/lotto-receipt.component';
import { LottoResultsFilterPageComponent } from '@app/lotto/components/lottoResulsFilterPage/lotto-results-filter-page.component';
import { LottoResultsPageComponent } from '@app/lotto/components/lottoResultsPage/lotto-results-page.component';

// Overridden
import { DesktopLottoMainComponent } from '@ladbrokesDesktop/lotto/components/lottoMain/lotto-main.component';

const routes: Routes = [{
  path: '',
  children: [{
    path: 'results',
    component: LottoResultsPageComponent,
    data: { segment: 'lotto-results' },
    children: [{
      path: ':filter',
      component: LottoResultsFilterPageComponent,
      data: { segment: 'lotto-results.filter' }
    }]
  }, {
    path: '',
    component: DesktopLottoMainComponent,
    data: { segment: 'lotto' },
    children: [{
      path: 'lottery-receipt',
      component: LottoReceiptComponent,
      data: { segment: 'lotto.lottery-receipt' },
    }, {
      path: '',
      component: LottoSegmentPageComponent,
      data: { segment: 'lotto'},
    }, {
      path: ':lottery',
      component: LottoSegmentPageComponent,
      data: { segment: 'lotto.lottery'},
    }]
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class LottoRoutingModule { }
