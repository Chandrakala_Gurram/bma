import { Component } from '@angular/core';

import {
  BannersSectionComponent as CoralBannersSectionComponent
} from '@app/banners/bannersSection/banners-section.component';

@Component({
  selector: 'banners-section',
  templateUrl: 'banners-section.component.html',
  styleUrls: [
    '../../../../app/banners/bannersSection/styles/banners-section.component.less',
    'styles/lc-carousel.less'
  ]
})

export class BannersSectionComponent extends CoralBannersSectionComponent {}
