import { Component } from '@angular/core';

import { QuestionEngineService } from '@app/questionEngine/services/question-engine/question-engine.service';
import { Router } from '@angular/router';
import { PubSubService } from '@core/services/communication/pubsub/pubsub.service';
import { DeviceService } from '@core/services/device/device.service';
import { NewRelicService } from '@core/services/newRelic/new-relic.service';
import { LocaleService } from '@core/services/locale/locale.service';

import {
  QuestionEngineMainComponent as AppQuestionEngineMainComponent
} from '@app/questionEngine/components/questionEngineMain/question-engine-main.component';

@Component({
  selector: 'question-engine-main',
  templateUrl: './question-engine-main.component.html',
  styleUrls: ['./question-engine-main.component.less'],
  providers: [QuestionEngineService]
})
export class QuestionEngineMainComponent extends AppQuestionEngineMainComponent {
  quizName: string;

  constructor(
    protected pubSubService: PubSubService,
    protected deviceService: DeviceService,
    protected questionEngineService: QuestionEngineService,
    protected newRelicService: NewRelicService,
    protected localeService: LocaleService,
    protected router: Router
  ) {
    super(pubSubService, deviceService, questionEngineService, newRelicService, localeService, router);
    this.quizName = this.getQuizName();
  }

  public getQuizName(): string {
    const urlParts: string[] = this.router.url.split('/').filter(e => e !== '');
    return urlParts.length > 1 ? urlParts[1] : urlParts[0] || '';
  }
}
