import { Component } from '@angular/core';

import { UpsellComponent as AppUpsellComponent } from '@app/questionEngine/components/resultsPage/upsell/upsell.component';

@Component({
  selector: 'upsell',
  templateUrl: '../../../../../../app/questionEngine/components/resultsPage/upsell/upsell.component.html',
  styleUrls: ['./upsell.component.less'],
})

export class UpsellComponent extends AppUpsellComponent {
}
