import { of } from 'rxjs';
import { DesktopHomeComponent } from './home.component';
import { pubSubApi } from '@core/services/communication/pubsub/pubsub-api.constant';

describe('#DesktopHomeComponent', () => {
  let component: DesktopHomeComponent;
  let cms, dynamicComponentLoader, germanSupportService, pubSubService;
  const futureTimeStamp = ((new Date()).getTime() + 10000000000);
  const futureIsoTime = (new Date(futureTimeStamp)).toISOString();

  const statsDataMock = {
    getRibbonModule: [
      {
        directiveName: 'Featured',
        id: 'tab-featured',
        showTabOn: 'both',
        title: 'Featured',
        url: '/home/featured',
        visible: true
      },
      {
        directiveName: 'EventHub',
        id: 'tab-eventhub-4',
        showTabOn: 'both',
        displayFrom: '2019-02-18T13:12:01Z',
        displayTo: '2019-02-18T15:12:01Z',
        title: 'hub 4',
        url: '/home/eventhub/4',
        visible: true
      },
      {
        directiveName: 'EventHub',
        id: 'tab-eventhub-5',
        showTabOn: 'both',
        displayFrom: '2019-02-18T13:12:01Z',
        displayTo: futureIsoTime,
        title: 'hub 5',
        url: '/home/eventhub/5',
        visible: true
      }
    ] as any,
    getMMOutcomesByEventType: { },
  };

  beforeEach(() => {
    cms = {
      getRibbonModule: jasmine.createSpy('cms.getRibbonModule'),
      getToggleStatus: jasmine.createSpy('getToggleStatus').and.returnValue(of(true))
    } as any;
    dynamicComponentLoader = {};
    germanSupportService = {
      isGermanUser: jasmine.createSpy().and.returnValue(false)
    };
    pubSubService = {
      API: pubSubApi,
      subscribe: jasmine.createSpy().and.callFake((p1, p2, cb) => {
        cb({data: [1]});
      }),
      unsubscribe: jasmine.createSpy('unsubscribe'),
    };

    component = new DesktopHomeComponent(
      cms,
      dynamicComponentLoader,
      germanSupportService,
      pubSubService
    );
  });
  it('ngOnInit', () => {
    spyOn(component, 'hideSpinner');
    cms.getRibbonModule.and.returnValue(of(statsDataMock));
    component.ngOnInit();
    expect(germanSupportService.isGermanUser).toHaveBeenCalledTimes(2);
    expect(pubSubService.subscribe).toHaveBeenCalledWith('HomeComponent',
    [
      pubSubService.API.SUCCESSFUL_LOGIN, pubSubService.API.SESSION_LOGIN,
      pubSubService.API.SESSION_LOGOUT],
      jasmine.any(Function));
  });
  it('ngOnDestroy', () => {
    component.ngOnDestroy();
    expect(pubSubService.unsubscribe).toHaveBeenCalledWith('HomeComponent');
  });
});

