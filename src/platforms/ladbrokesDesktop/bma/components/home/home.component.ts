import { Component, OnInit, OnDestroy } from '@angular/core';

import { DynamicLoaderService } from '@app/dynamicLoader/dynamic-loader.service';
import { CmsService } from '@coreModule/services/cms/cms.service';

import * as _ from 'underscore';
import { HomeComponent } from '@bma/components/home/home.component';
import { GermanSupportService } from '@root/app/core/services/germanSupport/german-support.service';
import { PubSubService } from '@root/app/core/services/communication/pubsub/pubsub.service';

@Component({
  selector: 'home',
  templateUrl: 'home.component.html'
})

export class DesktopHomeComponent extends HomeComponent implements OnInit, OnDestroy {
  isGermanUser: boolean;

  constructor(
      protected cmsService: CmsService,
      protected dynamicComponentLoader: DynamicLoaderService,
      protected germanSupportService: GermanSupportService,
      protected pubSubService: PubSubService
  ) {
    super(cmsService, dynamicComponentLoader);

  }

  ngOnInit() {
    super.ngOnInit();
    this.isGermanUser = this.germanSupportService.isGermanUser();
    this.pubSubService.subscribe('HomeComponent',
      [
        this.pubSubService.API.SUCCESSFUL_LOGIN, this.pubSubService.API.SESSION_LOGIN,
        this.pubSubService.API.SESSION_LOGOUT
      ], () => {
        this.isGermanUser = this.germanSupportService.isGermanUser();
      }
    );
  }

  ngOnDestroy() {
    this.pubSubService.unsubscribe('HomeComponent');
  }

  /**
   * Says whether to show ribbon module or not
   * @return {boolean}
   */
  showRibbon(): boolean {
    const id: string = 'tab-build-your-bet';
    const ribbon = _.findWhere(this.ribbon, { id });

    return ribbon ? ribbon.visible && (ribbon.showTabOn === 'both' || ribbon.showTabOn === 'desktop') : false;
  }
}
