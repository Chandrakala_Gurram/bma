import { DesktopBmaMainComponent } from './components/bmaMain/bma-main.component';
import { FormsModule } from '@angular/forms';
import { APP_INITIALIZER, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ModalModule } from 'angular-custom-modal';
import { DesktopModule } from '@ladbrokesDesktop/desktop/desktop.module';
import { SharedModule } from '@sharedModule/shared.module';
import { FavouritesModule } from '@favouritesModule/favourites.module';
import { ContactUsComponent } from '@app/bma/components/contactUs/contact-us.component';
import { LogoutResolver } from '@app/vanillaInit/services/logout/logout.service';
import { DesktopHomeComponent } from '@ladbrokesDesktop/bma/components/home/home.component';
import { NotFoundComponent } from '@bma/components/404/404.component';
import {
  RightColumnWidgetWrapperComponent } from '@app/bma/components/rightColumn/rightColumnWidgetWrapper/right-column-widget-wrapper.component';
import { WBetslipComponent } from '@app/bma/components/rightColumn/wBetslip/w-betslip.component';
import { WMatchCentreComponent } from '@app/bma/components/rightColumn/wMatchCentre/w-match-centre.component';
import { InplayLiveStreamModule } from '@inPlayLiveStream/inplay-live-stream.module';
import { BmaRunService } from '@bma/services/bmaRunService/bma-run.service';
import { BmaInit } from '@bma/services/bmaRunService/bma-init';
import { BannersModule } from '@banners/banners.module';
import { InplayHomeTabComponent } from '@bma/components/inlayHomeTab/inplay-home-tab.component';
import { LocaleService } from '@core/services/locale/locale.service';
import { StaticComponent } from '@bma/components/static/static.component';

import {
  RightColumnWidgetItemComponent
} from '@ladbrokesDesktop/bma/components/rightColumn/rightColumnWidgetItem/right-column-widget-item.component';
import * as criticalLangData from '@ladbrokesDesktop/lazy-modules/locale/en-US/critical-lang-data';
import { RightColumnWidgetComponent } from '@ladbrokesDesktop/bma/components/rightColumn/rightColumnWidget/right-column-widget.component';
import { DesktopLiveStreamWrapperComponent } from '@ladbrokesDesktop/bma/components/liveStream/live-stream-wrapper.component';
import { WMiniGamesIframeComponent } from '@bma/components/rightColumn/wMiniGames/w-mini-games-iframe.component';

@NgModule({
  imports: [
    SharedModule,
    ModalModule,
    FormsModule,
    FavouritesModule,
    DesktopModule,
    BannersModule,
    InplayLiveStreamModule
  ],
  providers: [
    BmaRunService,
    {
      provide: APP_INITIALIZER,
      useFactory: BmaInit,
      deps: [BmaRunService],
      multi: true
    },
    LogoutResolver
  ],
  declarations: [
    ContactUsComponent,
    DesktopHomeComponent,
    DesktopBmaMainComponent,
    NotFoundComponent,
    RightColumnWidgetWrapperComponent,
    RightColumnWidgetComponent,
    RightColumnWidgetItemComponent,
    WBetslipComponent,
    WMatchCentreComponent,
    InplayHomeTabComponent,
    StaticComponent,
    DesktopLiveStreamWrapperComponent,
    WMiniGamesIframeComponent
  ],
  entryComponents: [
    ContactUsComponent,
    DesktopHomeComponent,
    DesktopBmaMainComponent,
    NotFoundComponent,
    RightColumnWidgetWrapperComponent,
    RightColumnWidgetComponent,
    RightColumnWidgetItemComponent,
    WBetslipComponent,
    WMatchCentreComponent,
    InplayHomeTabComponent,
    StaticComponent,
    DesktopLiveStreamWrapperComponent,
    WMiniGamesIframeComponent
  ],
  exports: [
    ContactUsComponent,
    DesktopBmaMainComponent,
    NotFoundComponent,
    RightColumnWidgetWrapperComponent,
    RightColumnWidgetComponent,
    RightColumnWidgetItemComponent,
    WBetslipComponent,
    WMatchCentreComponent,
    InplayHomeTabComponent,
    StaticComponent,
    DesktopLiveStreamWrapperComponent,
    WMiniGamesIframeComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class BmaModule {
  constructor(private localeService: LocaleService) {
    this.localeService.setLangData(criticalLangData);
  }
}
