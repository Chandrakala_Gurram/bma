import { DesktopSportEventPageComponent } from '@ladbrokesDesktop/edp/components/sportEventPage/sport-event-page.component';

describe('LadbrokesDesktopSportEventPageComponent', () => {
  let component: DesktopSportEventPageComponent;
  let activatedRoute;
  let sportEventPageProviderService;
  let templateService;
  let router;
  let pubSubService;
  let storageService;
  let smartBoostsServiceStub;
  let footballExtensionService;
  let tennisExtantionService;
  let routingHelperService;
  let locationService;
  let filtersService;
  let sportsConfigService;
  let changeDetectorRef;
  let windowRefService;

  const fakeObservable = {
    unsubscribe: jasmine.createSpy()
  };

  beforeEach(() => {

    activatedRoute = {
      snapshot: {
        paramMap: {
          get: jasmine.createSpy()
        }
      }
    };

    sportEventPageProviderService = {
      sportData: {
        subscribe: jasmine.createSpy().and.returnValue(fakeObservable),
        unsubscribe: jasmine.createSpy()
      }
    };

    templateService = {
      sortOutcomesByPrice: jasmine.createSpy('sortOutcomesByPrice')
    };

    router = {
      events: {
        subscribe: jasmine.createSpy().and.returnValue(fakeObservable),
        unsubscribe: jasmine.createSpy()
      }
    };

    pubSubService = {
      API: {
        DELETE_MARKET_FROM_CACHE: 'DELETE_MARKET_FROM_CACHE'
      },
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy('unsubscribe')
    };

    storageService = {
      remove: jasmine.createSpy('remove')
    };

    smartBoostsServiceStub = {
      isSmartBoosts: jasmine.createSpy(),
      parseName: jasmine.createSpy(),
    };
    filtersService = {};
    locationService = {};
    routingHelperService = {};
    tennisExtantionService = {};
    footballExtensionService = {};
    sportsConfigService = {};

    changeDetectorRef = {
      detectChanges: jasmine.createSpy('detectChanges')
    };

    windowRefService = {
      document: {
        querySelector: jasmine.createSpy('querySelector')
      }
    };

    component = new DesktopSportEventPageComponent(
      router,
      activatedRoute,
      sportEventPageProviderService,
      templateService,
      footballExtensionService,
      tennisExtantionService,
      routingHelperService,
      pubSubService,
      locationService,
      filtersService,
      smartBoostsServiceStub,
      storageService,
      sportsConfigService,
      changeDetectorRef,
      windowRefService
    );
  });

  it('should create component', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });

  it('should check default component properties', () => {
    component.ngOnInit();
    expect(component.openedMarketTabsCountByDefault).toEqual(4);
    expect(component['openMarketTabs']).toEqual([]);
  });

  describe('isSameEvent', () => {
    it('should return false if event is not available', () => {
      component.eventEntity = null;

      expect(component['isSameEvent']()).toBeFalsy();
    });

    it('should return false if there are different ids in stored event and url', () => {
      activatedRoute.snapshot.paramMap.get.and.returnValue('2');
      component.eventEntity = { id: 1 } as any;

      expect(component['isSameEvent']()).toBeFalsy();
    });

    it('should return true if there are same ids in stored event and url', () => {
      const id = '2';

      activatedRoute.snapshot.paramMap.get.and.returnValue(id);
      component.eventEntity = { id: +id } as any;

      expect(component['isSameEvent']()).toBeTruthy();
    });
  });
});
