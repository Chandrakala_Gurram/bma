import { DesktopSportEventPageComponent } from '@ladbrokesDesktop/edp/components/sportEventPage/sport-event-page.component';
import { BehaviorSubject, throwError } from 'rxjs';

describe('DesktopSportEventPageComponent', () => {
  let component: DesktopSportEventPageComponent;
  let activatedRoute;
  let sportEventPageProviderService;
  let templateService;
  let footballExtensionService;
  let tennisExtensionService;
  let routingHelperService;
  let location;
  let filtersService;
  let router;
  let pubSubService;
  let storageService;
  let smartBoostsServiceStub;
  let sportsConfigService;
  let changeDetectorRef;
  let windowRefService;

  const fakeObservable = {
    unsubscribe: jasmine.createSpy()
  };

  beforeEach(() => {
    activatedRoute = {
      snapshot: {
        paramMap: {
          get: jasmine.createSpy()
        }
      }
    };

    sportEventPageProviderService = {
      sportData: {
        subscribe: jasmine.createSpy().and.returnValue(fakeObservable),
        unsubscribe: jasmine.createSpy()
      }
    };

    templateService = {
      sortOutcomesByPrice: jasmine.createSpy('sortOutcomesByPrice')
    };

    footballExtensionService = {};

    tennisExtensionService = {};

    routingHelperService = {};

    location = {};

    filtersService = {};

    router = {
      events: {
        subscribe: jasmine.createSpy().and.returnValue(fakeObservable),
        unsubscribe: jasmine.createSpy()
      }
    };

    pubSubService = {
      API: {
        DELETE_MARKET_FROM_CACHE: 'DELETE_MARKET_FROM_CACHE'
      },
      subscribe: jasmine.createSpy('subscribe'),
      unsubscribe: jasmine.createSpy('unsubscribe')
    };

    storageService = {
      remove: jasmine.createSpy('remove')
    };

    smartBoostsServiceStub = {
      isSmartBoosts: jasmine.createSpy(),
      parseName: jasmine.createSpy(),
    };
    footballExtensionService = {};
    tennisExtensionService = {};
    routingHelperService = {};
    location = {};
    filtersService = {};
    sportsConfigService = {};

    changeDetectorRef = {
      detectChanges: jasmine.createSpy('changeDetectorRef')
    };

    windowRefService = {
      document: {
        querySelector: jasmine.createSpy('querySelector')
      }
    };

    component = new DesktopSportEventPageComponent(
      router,
      activatedRoute,
      sportEventPageProviderService,
      templateService,
      footballExtensionService,
      tennisExtensionService,
      routingHelperService,
      pubSubService,
      location,
      filtersService,
      smartBoostsServiceStub,
      storageService,
      sportsConfigService,
      changeDetectorRef,
      windowRefService
    );
  });

  it('should create component', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });

  it('should check default component properties', () => {
    component.ngOnInit();
    expect(component.openedMarketTabsCountByDefault).toEqual(4);
    expect(component['openMarketTabs']).toEqual([]);
  });

  it ('@ngOnInit should call error handler', () => {
    router.events.subscribe = jasmine.createSpy();
    sportEventPageProviderService.sportData = new BehaviorSubject<any>(null);
    sportEventPageProviderService.sportData.error(throwError('err'));
    component.ngOnInit();

    expect(component.state.loading).toBe(false);
    expect(component.state.error).toBe(true);
  });
});
