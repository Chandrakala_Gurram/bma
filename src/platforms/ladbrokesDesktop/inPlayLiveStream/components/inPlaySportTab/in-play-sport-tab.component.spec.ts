import { InPlaySportTabComponent } from '@ladbrokesDesktop/inPlayLiveStream/components/inPlaySportTab/in-play-sport-tab.component';

describe('#InPlaySportTabComponent', () => {
  let component: InPlaySportTabComponent;
  let userService;
  let inplayMainService;
  let inplayHelperService;

  beforeEach(() => {
    userService = {};
    inplayHelperService = {
      subscribeForLiveUpdates: jasmine.createSpy(),
      unsubscribeForLiveUpdates: jasmine.createSpy(),
    };

    inplayMainService = {
      isCashoutAvailable: jasmine.createSpy()
    };

    component = new InPlaySportTabComponent(
      inplayMainService,
      inplayHelperService,
      userService
    );
  });

  describe('@ngOnInit', () => {
    it('should set gtmDataLayer if it is in-play tab', () => {
      component.ngOnInit();
      expect(component.gtmDataLayer).toEqual({
        eventAction: 'in-play',
        eventLabel: 'more markets'
      });
    });

    it('should set gtmDataLayer if it is in-play tab', () => {
      component.liveStreamTab = true;
      component.ngOnInit();
      expect(component.gtmDataLayer).toEqual({
        eventAction: 'live stream',
        eventLabel: 'more markets'
      });
    });
  });
});
