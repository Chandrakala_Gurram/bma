import de0Config from './environment.dev0';

import * as merge from 'lodash.merge';

const prodConfig = JSON.parse(JSON.stringify(de0Config));

export default merge(prodConfig, {
  PIROZHOK_API_ENDPOINT: 'https://bpp-dev1.ladbrokesoxygen.dev.cloud.ladbrokescoral.com/Proxy',
  REMOTEBETSLIPMS: 'wss://remotebetslip-dev1.ladbrokesoxygen.dev.cloud.ladbrokescoral.com'
});
