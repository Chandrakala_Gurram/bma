import productionConfig from './environment.production';

import * as merge from 'lodash.merge';
const prodConfig = JSON.parse(JSON.stringify(productionConfig));

export default merge(prodConfig, {
  ENVIRONMENT: 'hiddenprod',
  PIROZHOK_API_ENDPOINT: 'https://hl-bpp.ladbrokes.com/Proxy',
  IMAGES_RACE_ENDPOINT: 'https://aggregation-hlv0.ladbrokes.com/silks/racingpost',
  SURFACE_BETS_URL: 'https://surface-bets-hlv0.ladbrokes.com/cms/api',
  CMS_ENDPOINT: 'https://cms-hl.ladbrokes.com/cms/api',
  CMS_ROOT_URI: 'https://cms-hl.ladbrokes.com/cms',
  CMS_LINK: 'https://cms-api-ui-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com',
  OPT_IN_ENDPOINT: 'https://optin-hlv0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  IG_MEDIA_TOTE_ENDPOINT: 'https://optin-hlv0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com/api/igamemedia',
  IG_MEDIA_ENDPOINT: 'https://optin-hlv0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com/api/video/igame',
  FEATURED_SPORTS: 'wss://featured-sports-hlv0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  INPLAYMS: 'wss://inplay-publisher-hlv0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  LIVESERVEMS: 'wss://liveserve-publisher-hlv0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  REMOTEBETSLIPMS: 'wss://remotebetslip-hlv0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  EDPMS: 'https://edp-hlv0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  BIG_COMPETITION_MS: 'https://bigcompetition-hlv0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com/competition',
  TIMEFORM_ENDPOINT: 'https://timeform-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com',
  ONE_TWO_FREE_ENDPOINT: 'https://otf-hlv0.ladbrokes.com/',
  NOTIFICATION_CENTER_ENDPOINT: 'https://notification-center-hlv0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  BYB_CONFIG: {
    uri: 'https://buildyourbet-hl.ladbrokes.com/api'
  },
  GAMING_URL: ['https://beta-www.ladbrokes.com/en/games'],
  AEM_CONFIG: {
    server: 'https://banners-cms.ladbrokes.com',
    at_property: 'd01b458f-4505-86d5-3d58-64113bb70196',
    betslip_at_property: '4b720044-1be9-de72-e197-8c8b530537a6'
  },
  CASHOUT_MS: 'https://cashout-hlv0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  OPTA_SCOREBOARD: {
    CDN: 'https://opta-scoreboards-hl.ladbrokes.com',
    ENV: 'prod'
  },
  QUESTION_ENGINE_ENDPOINT: 'https://question-engine-hlv0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com/api',
  TIMELINE_MS: 'wss://timeline-api-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com'
});
