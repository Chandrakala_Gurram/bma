import de0Config from './environment.dev0';

import * as merge from 'lodash.merge';

const prodConfig = JSON.parse(JSON.stringify(de0Config));

export default merge(prodConfig, {
  production: false,
  ENVIRONMENT: 'dev',
  SURFACE_BETS_URL: 'https://cms-dev2.coralsports.dev.cloud.ladbrokescoral.com/cms/api',
  CMS_ENDPOINT: 'https://cms-dev2.ladbrokes.com/cms/api',
  CMS_ROOT_URI: 'https://cms-dev2.ladbrokes.com/cms',
  CMS_LINK: 'https://cms-api-ui-dev2.coralsports.dev.cloud.ladbrokescoral.com',
  FEATURED_SPORTS: 'wss://featured-sports-dev2.coralsports.dev.cloud.ladbrokescoral.com',
  INPLAYMS: 'wss://inplay-publisher-dev2.coralsports.dev.cloud.ladbrokescoral.com'
});
