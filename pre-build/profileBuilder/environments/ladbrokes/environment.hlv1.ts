import productionConfig from './environment.production';

import * as merge from 'lodash.merge';

const prodConfig = JSON.parse(JSON.stringify(productionConfig));

/* tslint:disable */
export default merge(prodConfig, {
  ENVIRONMENT: 'hiddenprod',
  brand: 'ladbrokes',
  PIROZHOK_API_ENDPOINT: 'https://bpp-gib.ladbrokes.com/Proxy',
  SURFACE_BETS_URL: 'https://surface-bets-hlv1.ladbrokes.com/cms/api',

  CMS_ENDPOINT: 'https://cms-hlv1.ladbrokes.com/cms/api',
  CMS_ROOT_URI: 'https://cms-hlv1.ladbrokes.com/cms',
  CMS_LINK: 'https://cms-api-ui-hlv1.coralsports.nonprod.cloud.ladbrokescoral.com',

  IMAGES_RACE_ENDPOINT: 'https://aggregation-hlv1.ladbrokes.com/silks/racingpost',

  OPT_IN_ENDPOINT: 'https://optin-hlv2.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  IG_MEDIA_TOTE_ENDPOINT: 'https://optin-hlv2.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com/api/igamemedia',
  IG_MEDIA_ENDPOINT: 'https://optin-hlv2.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com/api/video/igame',
  FEATURED_SPORTS: 'wss://featured-sports-hlv2.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  INPLAYMS: 'wss://inplay-publisher-hlv2.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  LIVESERVEMS: 'wss://liveserve-publisher-hlv2.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  REMOTEBETSLIPMS: 'wss://remotebetslip-hlv2.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  EDPMS: 'https://edp-hlv2.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  BIG_COMPETITION_MS: 'https://bigcompetition-hlv2.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com/competition',
  TIMEFORM_ENDPOINT: 'https://timeform-hlv2.coralsports.nonprod.cloud.ladbrokescoral.com',
  ONE_TWO_FREE_ENDPOINT: 'https://otf-hlv0.ladbrokes.com/',
  NOTIFICATION_CENTER_ENDPOINT: 'https://notification-center-hlv1.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  BYB_CONFIG: {
    HR_YC_EVENT_TYPE_ID: 15031,
    uri: 'https://buildyourbet-stg0.ladbrokes.com/api'
  },
  AEM_CONFIG: {
    server: 'https://banners-cms-stg.ladbrokes.com',
    at_property: 'abaf3459-6df6-2b23-a718-f37546afeac0',
    betslip_at_property: '1b7662de-2a43-49f7-ec35-3afa39ad6b4a'
  },
  OPTA_SCOREBOARD: {
    CDN: 'https://opta-scoreboards-hl.ladbrokes.com',
    ENV: 'stage'
  },
  CASHOUT_MS: 'https://cashout-hlv2.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',

  RACING_POST_API_ENDPOINT: 'https://sb-api-stg.ladbrokes.com/v4/sportsbook-api',
  RACING_POST_API_KEY: 'LDb1f382b9d06a4d50985829d59994cdf3',

  SITESERVER_DNS: 'https://ss-aka-ori-gib.ladbrokes.com',
  SITESERVER_BASE_URL: 'https://ss-aka-ori-gib.ladbrokes.com/openbet-ssviewer',
  SITESERVER_ENDPOINT: 'https://ss-aka-ori-gib.ladbrokes.com/openbet-ssviewer/Drilldown/2.31',
  SITESERVER_COMMON_ENDPOINT: 'https://ss-aka-ori-gib.ladbrokes.com/openbet-ssviewer/Common/2.31',
  SITESERVER_LOTTERY_ENDPOINT: 'https://ss-aka-ori-gib.ladbrokes.com/openbet-ssviewer/Lottery/2.31',
  SITESERVER_HISTORIC_ENDPOINT: 'https://ss-aka-ori-gib.ladbrokes.com/openbet-ssviewer/HistoricDrilldown/2.31',
  SITESERVER_COMMENTARY_ENDPOINT: 'https://ss-aka-ori-gib.ladbrokes.com/openbet-ssviewer/Commentary/2.31',

  LIVESERV: {
    DOMAIN: 'ladbrokes.com',
    PUSH_COMMON_SUBDOMAIN: 'ladbrokes.com',
    PUSH_LOCATION_HANDLER: '',
    PUSH_URL: 'https://push-gib.ladbrokes.com',
    PUSH_INSTANCE_URL: 'push-gib.ladbrokes.com',
    PUSH_INSTANCE_PORT: '443',
    PUSH_INSTANCE_TYPE: 'AJAX',
    PUSH_IFRAME_ID: 'push_iframe',
    PUSH_VERSION: 'e8382d8b7866e9a11d3529d8fe438008'
  },
  GAMING_URL: ['https://beta-www.ladbrokes.com/en/games'],
  QUESTION_ENGINE_ENDPOINT: 'https://question-engine-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com/api',
  TIMELINE_MS: 'wss://timeline-api-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com'
});
/* tslint:enable */
