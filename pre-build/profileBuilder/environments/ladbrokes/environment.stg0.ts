import { lotteriesConfig } from '../configs/lotteriesLadbrokesConfig';
import { toteClasses } from '../configs/toteClasses';
import productionConfig from './environment.production';

import * as merge from 'lodash.merge';

const prodConfig = JSON.parse(JSON.stringify(productionConfig));

export default merge(prodConfig, {
  ENVIRONMENT: 'stg',
  PIROZHOK_API_ENDPOINT: 'https://stg-bpp.ladbrokes.com/Proxy',
  SITESERVER_BASE_URL: 'https://stg-ss-aka-ori.ladbrokes.com/openbet-ssviewer',
  SITESERVER_DNS: 'https://stg-ss-aka-ori.ladbrokes.com',
  SITESERVER_ENDPOINT: 'https://stg-ss-aka-ori.ladbrokes.com/openbet-ssviewer/Drilldown/2.31',
  SITESERVER_COMMON_ENDPOINT: 'https://stg-ss-aka-ori.ladbrokes.com/openbet-ssviewer/Common/2.31',
  SITESERVER_LOTTERY_ENDPOINT: 'https://stg-ss-aka-ori.ladbrokes.com/openbet-ssviewer/Lottery/2.31',
  SITESERVER_HISTORIC_ENDPOINT:
    'https://stg-ss-aka-ori.ladbrokes.com/openbet-ssviewer/HistoricDrilldown/2.31',
  SITESERVER_COMMENTARY_ENDPOINT:
    'https://stg-ss-aka-ori.ladbrokes.com/openbet-ssviewer/Commentary/2.31',
  SURFACE_BETS_URL: 'https://surface-bets-stg0.ladbrokes.com/cms/api',
  CMS_ENDPOINT: 'https://cms-stg.ladbrokes.com/cms/api',
  CMS_ROOT_URI: 'https://cms-stg.ladbrokes.com/cms',
  CMS_LINK: 'https://cms-api-ui-stg0.coralsports.nonprod.cloud.ladbrokescoral.com/',
  CMS_DYNAMIC_ENDPOINT: 'https://stg-bpp.ladbrokes.com/Proxy/cms',
  ATR_END_POINT: 'https://bwt1.attheraces.com/ps/api',
  API_KEY: '4EaZxV5ZaxVzVW1G!dbACRGc!2G$R$EqFFzEQxWGtfwT%st@rRXZXEb%qszcxFQ!',
  LIVE_SIM_ENDPOINT: 'https://www.racemodlr.com/coral-demo/visualiser/',
  QUANTUMLEAP_SCRIPT_ENDPOINT: 'https://www.racemodlr.com/ladbrokesStageV2/live_sim/mobile/',
  VISUALIZATION_ENDPOINT: 'https://vis-tst2-coral.symphony-solutions.eu',
  QUANTUMLEAP_BOOKMAKER: 'Ladbrokes-Stage',
  ACCOUNT_WITHDRAWAL_THRESHOLD: 100,
  VISUALIZATION_IFRAME_URL: 'https://vis-static-tst2.coral.co.uk',
  VISUALIZATION_PREMATCH_URL: 'https://vis-static-tst2.coral.co.uk/football/pre-match.html',
  DIGITAL_SPORTS_IFRAME_URL: 'https://betbuilder.digitalsportstech.com/?sb=coraldev',
  DIGITAL_SPORTS_SYSTEM_ID: 982,
  STATS_CENTRE_ENDPOINT: 'https://spark-br-tst.symphony-solutions.eu/api',
  OPT_IN_ENDPOINT: 'https://optin-stg0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  ONE_TWO_FREE_ENDPOINT: 'https://otf-stg0.coral.co.uk/',
  IG_MEDIA_TOTE_ENDPOINT: 'https://optin-stg0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com/api/igamemedia',
  IG_MEDIA_ENDPOINT: 'https://optin-stg0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com/api/video/igame',
  APOLLO: {
    API_ENDPOINT: 'https://apollo-tst2.coral.co.uk',
    CWA_ROUTE: 'cwa_api_index.php',
    UCMS_ROUTE: 'ucms_api_index.php'
  },
  BET_FINDER_ENDPOINT: 'https://api.racemodlr.com/cypher/ladbrokesStage/0/',
  LIVESERV: {
    PUSH_URL: 'https://stg-push-lcm.ladbrokes.com',
    PUSH_INSTANCE_URL: 'stg-push-lcm.ladbrokes.com',
    PUSH_VERSION: 'e95460e14c00a85c132637426ef177bc',
    DOMAIN: 'ladbrokes.com',
    PUSH_COMMON_SUBDOMAIN: 'ladbrokes.com',
    PUSH_LOCATION_HANDLER: '',
  },
  BR_TYPE_ID: ['3048', '3049', '3123'],
  LOTTERIES_CONFIG: lotteriesConfig.stg,
  TOTE_CLASSES: toteClasses.tst,
  FEATURED_SPORTS: 'wss://featured-sports-stg0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  INPLAYMS: 'wss://inplay-publisher-stg0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  LIVESERVEMS: 'wss://liveserve-publisher-stg0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  REMOTEBETSLIPMS: 'wss://remotebetslip-stg0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  EDPMS: 'https://edp-stg0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  BIG_COMPETITION_MS: 'https://bigcompetition-stg0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com/competition',
  AEM_CONFIG: {
    server: 'https://banners-cms-stg.ladbrokes.com',
    at_property: 'abaf3459-6df6-2b23-a718-f37546afeac0',
    betslip_at_property: '1b7662de-2a43-49f7-ec35-3afa39ad6b4a'
  },
  TIME_ENDPOINT: 'https://hydra-prd0.coralsports.prod.cloud.ladbrokescoral.com',
  TIMEFORM_ENDPOINT: 'https://timeform-dev0.coralsports.dev.cloud.ladbrokescoral.com',
  RACING_POST_API_ENDPOINT: 'https://sb-api-stg.ladbrokes.com/v4/sportsbook-api',
  RACING_POST_API_KEY: 'LDb1f382b9d06a4d50985829d59994cdf3',
  GAMING_URL: ['https://wpl-stg4-admin-gaming.ladbrokes.com'],
  IGAME_MEDIA_STREAM_ENDPOINT: 'https://player-test.igamemedia.com',
  IGM_STREAM_SERVICE_ENDPOINT: 'https://player-test.igamemedia.com/lib/v1.0/streamservice.js',
  VS_IGAME_MEDIA_STEAM_ID: '30473', // Temporary pointed to coral
  VS_QUOLITY_MAP: {
    desktop: 'adpt',
    tablet: 'adpt',
    mobile: 'adpt'
  },
  OPTA_SCOREBOARD: {
    CDN: 'https://opta-scoreboards-hl.ladbrokes.com',
    ENV: 'stage'
  },
  EXTERNAL_URL: {
    accountone: 'https://accountone-stg.ladbrokes.com',
    gaming: 'https://wpl-stg4-admin-gaming.ladbrokes.com',
    help: 'https://help.ladbrokes.com',
    'accountone-stg': 'https://accountone-stg.ladbrokes.com'
  },
  VIP_USERS_EXTERNAL_URL: 'https://wpl-stg4-admin-gaming.ladbrokes.com/vip',
  DESKTOP_VIP_USERS_EXTERNAL_URL: 'https://casino.ladbrokes.com/en/vip/new-benefits',
  NOTIFICATION_CENTER_ENDPOINT: 'https://notification-center-stg0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  BYB_CONFIG: {
    HR_YC_EVENT_TYPE_ID: 15031,
    uri: 'https://buildyourbet-stg0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com/api'
  },
  CASHOUT_MS: 'https://cashout-stg0.ladbrokesoxygen.nonprod.cloud.ladbrokescoral.com',
  BET_FILTER_ENDPOINT: 'https://modules-stg.coral.co.uk/coupon-buddy/oxygen-grid/'
});
