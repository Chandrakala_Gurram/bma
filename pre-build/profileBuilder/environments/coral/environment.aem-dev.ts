import oxyConfig from './environment.dev0';
import * as merge from 'lodash.merge';
const prodConfig = JSON.parse(JSON.stringify(oxyConfig));
export default merge(prodConfig, {
  googleTagManagerID: 'GTM-W6NBMJL',
  AEM_CONFIG: {
    server: 'https://35.176.108.76',
    at_property: '63909eaa-a987-2833-6631-85b5ced26e68',
    betslip_at_property: '0c727079-a884-88b9-1fd8-faa96c1a6fa7'
  },
  CMS_ENDPOINT: 'https://cms-dev0.coral.co.uk/cms/api',
  CMS_ROOT_URI: 'https://cms-dev0.coral.co.uk/cms',
  CMS_LINK: 'https://cms-dev0.coralsports.dev.cloud.ladbrokescoral.com',
});
