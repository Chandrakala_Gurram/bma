import hiddenProdConfig from './environment.hlv0';

import * as merge from 'lodash.merge';
const prodConfig = JSON.parse(JSON.stringify(hiddenProdConfig));

export default merge(prodConfig, {
  OPTA_SCOREBOARD: {
    ENV: 'stage'
  },
});
