import { lotteriesConfig } from '../configs/lotteriesConfig';
import { toteClasses } from '../configs/toteClasses';

import productionConfig from './environment.production';
import * as merge from 'lodash.merge';
const prodConfig = JSON.parse(JSON.stringify(productionConfig));

export default merge(prodConfig, {
  production: true,
  ENVIRONMENT: 'tst2',
  PIROZHOK_API_ENDPOINT: 'https://bpp-tst0.coralsports.nonprod.cloud.ladbrokescoral.com/Proxy',
  SITESERVER_BASE_URL: 'https://ss-tst2.coral.co.uk/openbet-ssviewer',
  SITESERVER_DNS: 'https://ss-tst2.coral.co.uk',
  SITESERVER_ENDPOINT: 'https://ss-tst2.coral.co.uk/openbet-ssviewer/Drilldown/2.31',
  SITESERVER_COMMON_ENDPOINT: 'https://ss-tst2.coral.co.uk/openbet-ssviewer/Common/2.31',
  SITESERVER_LOTTERY_ENDPOINT: 'https://ss-tst2.coral.co.uk/openbet-ssviewer/Lottery/2.31',
  SITESERVER_HISTORIC_ENDPOINT: 'https://ss-tst2.coral.co.uk/openbet-ssviewer/HistoricDrilldown/2.31',
  SITESERVER_COMMENTARY_ENDPOINT: 'https://ss-tst2.coral.co.uk/openbet-ssviewer/Commentary/2.31',
  SURFACE_BETS_URL: 'https://surface-bets-tst0.coral.co.uk/cms/api',
  CMS_ENDPOINT: 'https://cms-tst0.coral.co.uk/cms/api',
  CMS_ROOT_URI: 'https://cms-tst0.coral.co.uk/cms',
  CMS_LINK: 'https://cms-api-ui-tst0.coralsports.nonprod.cloud.ladbrokescoral.com',
  ATR_END_POINT: 'https://bwt1.attheraces.com/ps/api',
  QUANTUMLEAP_SCRIPT_ENDPOINT: 'https://www.racemodlr.com/coral-stage/live_sim/mobile/',
  VISUALIZATION_ENDPOINT: 'https://vis-tst2-coral.symphony-solutions.eu',
  VISUALIZATION_TENNIS_ENDPOINT: 'https://coral-vis-tennis-tst2.symphony-solutions.eu',
  QUANTUMLEAP_BOOKMAKER: 'Coral-Test-2',
  TOTE_CLASSES: toteClasses.tst,
  LOTTERIES_CONFIG: lotteriesConfig.tst,
  VISUALIZATION_IFRAME_URL: 'https://vis-static-tst2.coral.co.uk',
  VISUALIZATION_PREMATCH_URL: 'https://vis-static-tst2.coral.co.uk/football/pre-match.html',
  DIGITAL_SPORTS_IFRAME_URL: 'https://betbuilder.digitalsportstech.com/?sb=coraldev',
  DIGITAL_SPORTS_SYSTEM_ID: 982,
  STATS_CENTRE_ENDPOINT: 'https://stats-centre-tst0.coral.co.uk/api',
  OPT_IN_ENDPOINT: 'https://optin-tst0.coralsports.nonprod.cloud.ladbrokescoral.com',
  IG_MEDIA_TOTE_ENDPOINT: 'https://optin-tst0.coralsports.nonprod.cloud.ladbrokescoral.com/api/igamemedia',
  IG_MEDIA_ENDPOINT: 'https://optin-tst0.coralsports.nonprod.cloud.ladbrokescoral.com/api/video/igame',
  VS_QUOLITY_MAP: {
    desktop: 'web',
    tablet: 'mobhi',
    mobile: 'moblo'
  },
  IGM_STREAM_SERVICE_ENDPOINT: 'https://player-test.igamemedia.com/lib/v1.0/streamservice.js',
  APOLLO: {
    API_ENDPOINT: 'https://apollo-tst2.coral.co.uk',
    CWA_ROUTE: 'cwa_api_index.php',
    UCMS_ROUTE: 'ucms_api_index.php'
  },
  BET_FINDER_ENDPOINT: 'https://api.racemodlr.com/cypher/coralTest2/0/',
  BET_FILTER_ENDPOINT: 'https://modules-tst2.coral.co.uk/coupon-buddy/oxygen/',
  BET_TRACKER_ENDPOINT: 'https://modules-tst2.coral.co.uk/coupon-buddy/bettracker/',
  NOTIFICATION_CENTER_ENDPOINT: 'https://notification-center-dev0.coralsports.dev.cloud.ladbrokescoral.com',
  LIVESERV: {
    PUSH_URL: 'https://push-tst2.coral.co.uk',
    PUSH_INSTANCE_URL: 'push-tst2.coral.co.uk',
    PUSH_VERSION: 'c3e5103c451ba491d75a024811deba7f'
  },
  BR_TYPE_ID: ['3048', '3049', '3123'],
  FEATURED_SPORTS: 'wss://featured-sports-tst0.coralsports.nonprod.cloud.ladbrokescoral.com',
  INPLAYMS: 'wss://inplay-publisher-tst0.coralsports.nonprod.cloud.ladbrokescoral.com',
  LIVESERVEMS: 'wss://liveserve-publisher-tst0.coralsports.nonprod.cloud.ladbrokescoral.com',
  REMOTEBETSLIPMS: 'wss://remotebetslip-tst0.coralsports.nonprod.cloud.ladbrokescoral.com',
  EDPMS: 'https://edp-dev0.coralsports.dev.cloud.ladbrokescoral.com',
  BIG_COMPETITION_MS: 'https://bigcompetition-tst0.coralsports.nonprod.cloud.ladbrokescoral.com/competition',
  IMAGES_RACE_ENDPOINT: 'https://aggregation-dev0.coralsports.dev.cloud.ladbrokescoral.com/silks/racingpost',
  RACING_POST_API_ENDPOINT: 'https://sb-api-dev.coral.co.uk/v4/sportsbook-api',
  RACING_POST_API_KEY: 'CD11beae13fa47459ba472e0b743822846',
  OPTA_SCOREBOARD: {
    CDN: 'https://opta-scoreboards-dev0.coral.co.uk',
    ENV: 'dev'
  },
  BET_GENIUS_SCOREBOARD: {
    scorecentre: 'https://ladbrokescoral-uat.betstream.betgenius.com/betstream-view/page/ladbrokescoral/basketballscorecentre',
    api: 'https://ladbrokescoral-uat.betstream.betgenius.com/betstream-view/public/bg_api.js'
  },
  TIMEFORM_ENDPOINT: 'https://timeform-tst0.coralsports.nonprod.cloud.ladbrokescoral.com',
  GAMING_URL: ['https://mcasino-tst2.coral.co.uk'],

  BYB_CONFIG: {
    HR_YC_EVENT_TYPE_ID: 15031,
    uri: 'https://buildyourbet-tst0.coral.co.uk/api'
  },
  CASHOUT_MS: 'https://cashout-tst0.coralsports.nonprod.cloud.ladbrokescoral.com',
  AEM_CONFIG: {
    server: 'https://banners-cms-stg.coral.co.uk',
    at_property: '63909eaa-a987-2833-6631-85b5ced26e68',
    betslip_at_property: '0c727079-a884-88b9-1fd8-faa96c1a6fa7'
  },
  QUESTION_ENGINE_ENDPOINT: 'https://question-engine-dev0.coralsports.dev.cloud.ladbrokescoral.com/api'
});
