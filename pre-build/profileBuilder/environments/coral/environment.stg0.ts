import { lotteriesConfig } from '../configs/lotteriesConfig';
import { toteClasses } from '../configs/toteClasses';
import productionConfig from './environment.production';

import * as merge from 'lodash.merge';
const prodConfig = JSON.parse(JSON.stringify(productionConfig));

export default merge(prodConfig, {
  production: false,
  ENVIRONMENT: 'stg',
  EVENT_CATEGORIES_WITH_WATCH_RULES: ['21', '19', '152'],
  PIROZHOK_API_ENDPOINT: 'https://bp-stg2.coral.co.uk/Proxy',
  SITESERVER_BASE_URL: 'https://ss-aka-ori-stg2.coral.co.uk/openbet-ssviewer',
  SITESERVER_DNS: 'https://ss-aka-ori-stg2.coral.co.uk',
  SITESERVER_ENDPOINT: 'https://ss-aka-ori-stg2.coral.co.uk/openbet-ssviewer/Drilldown/2.31',
  SITESERVER_COMMON_ENDPOINT: 'https://ss-aka-ori-stg2.coral.co.uk/openbet-ssviewer/Common/2.31',
  SITESERVER_LOTTERY_ENDPOINT: 'https://ss-aka-ori-stg2.coral.co.uk/openbet-ssviewer/Lottery/2.31',
  SITESERVER_HISTORIC_ENDPOINT: 'https://ss-aka-ori-stg2.coral.co.uk/openbet-ssviewer/HistoricDrilldown/2.31',
  SITESERVER_COMMENTARY_ENDPOINT: 'https://ss-aka-ori-stg2.coral.co.uk/openbet-ssviewer/Commentary/2.31',
  SURFACE_BETS_URL: 'https://surface-bets-stg0.coral.co.uk/cms/api',
  CMS_ENDPOINT: 'https://cms-stg.coral.co.uk/cms/api',
  CMS_ROOT_URI: 'https://cms-stg.coral.co.uk/cms',
  CMS_LINK: 'https://cms-api-ui-stg0.coralsports.nonprod.cloud.ladbrokescoral.com',
  BP_URI: 'https://bp.stg2.coral.co.uk',
  ATR_END_POINT: 'https://bwt1.attheraces.com/ps/api',
  QUANTUMLEAP_SCRIPT_ENDPOINT: 'https://www.racemodlr.com/coral-stage/live_sim/mobile/',
  VISUALIZATION_ENDPOINT: 'https://vis-stg2-coral.symphony-solutions.eu',
  VISUALIZATION_TENNIS_ENDPOINT: 'https://coral-vis-tennis-stg2.symphony-solutions.eu',
  QUANTUMLEAP_BOOKMAKER: 'Coral-Stage',
  VISUALIZATION_IFRAME_URL: 'https://vis-static-stg2.coral.co.uk',
  VISUALIZATION_PREMATCH_URL: 'https://vis-static-stg2.coral.co.uk/football/pre-match.html',
  DIGITAL_SPORTS_IFRAME_URL: 'https://betbuilder.digitalsportstech.com/?sb=coralstg',
  DIGITAL_SPORTS_SYSTEM_ID: 983,
  STATS_CENTRE_ENDPOINT: 'https://stats-centre-stg0.coral.co.uk/api',
  OPT_IN_ENDPOINT: 'https://optin-stg0.coralsports.nonprod.cloud.ladbrokescoral.com',
  IG_MEDIA_TOTE_ENDPOINT: 'https://optin-stg0.coralsports.nonprod.cloud.ladbrokescoral.com/api/igamemedia',
  IG_MEDIA_ENDPOINT: 'https://optin-stg0.coralsports.nonprod.cloud.ladbrokescoral.com/api/video/igame',
  APOLLO: {
    API_ENDPOINT: 'https://apollo-mobile-stg.coral.co.uk',
    CWA_ROUTE: 'cwa_api_index.php',
    UCMS_ROUTE: 'index.php'
  },
  BET_FINDER_ENDPOINT: 'https://api.racemodlr.com/cypher/coralStage/0/',
  BET_FILTER_ENDPOINT: 'https://modules-stg.coral.co.uk/coupon-buddy/oxygen/',
  BET_TRACKER_ENDPOINT: 'https://modules-stg.coral.co.uk/coupon-buddy/bettracker/',
  NOTIFICATION_CENTER_ENDPOINT: 'https://notification-center-stg0.coralsports.nonprod.cloud.ladbrokescoral.com',
  LIVESERV: {
    PUSH_URL: 'https://push-stg2.coral.co.uk',
    PUSH_INSTANCE_URL: 'push-stg2.coral.co.uk',
    PUSH_VERSION: 'c3e5103c451ba491d75a024811deba7f'
  },
  BR_TYPE_ID: ['16576', '16575', '16602'],
  LOTTERIES_CONFIG: lotteriesConfig.tst,
  TOTE_CLASSES: toteClasses.stg,
  OPTA_SCOREBOARD: {
    CDN: 'https://opta-scoreboards-dev0.coral.co.uk',
    ENV: 'stage'
  },
  FEATURED_SPORTS: 'wss://featured-sports-stg0.coralsports.nonprod.cloud.ladbrokescoral.com',
  INPLAYMS: 'wss://inplay-publisher-stg0.coralsports.nonprod.cloud.ladbrokescoral.com',
  LIVESERVEMS: 'wss://liveserve-publisher-stg0.coralsports.nonprod.cloud.ladbrokescoral.com',
  REMOTEBETSLIPMS: 'wss://remotebetslip-stg0.coralsports.nonprod.cloud.ladbrokescoral.com',
  EDPMS: 'https://edp-stg0.coralsports.nonprod.cloud.ladbrokescoral.com',
  BIG_COMPETITION_MS: 'https://bigcompetition-stg0.coralsports.nonprod.cloud.ladbrokescoral.com/competition',
  BET_GENIUS_SCOREBOARD: {
    scorecentre: 'https://ladbrokescoral-uat.betstream.betgenius.com/betstream-view/page/ladbrokescoral/basketballscorecentre',
    api: 'https://ladbrokescoral-uat.betstream.betgenius.com/betstream-view/public/bg_api.js'
  },
  AEM_CONFIG: {
    server: 'https://banners-cms-stg.coral.co.uk',
    at_property: '63909eaa-a987-2833-6631-85b5ced26e68',
    betslip_at_property: '0c727079-a884-88b9-1fd8-faa96c1a6fa7'
  },
  IMAGES_RACE_ENDPOINT: 'https://aggregation.coral.co.uk/silks/racingpost',
  TIMEFORM_ENDPOINT: 'https://timeform-stg0.coralsports.nonprod.cloud.ladbrokescoral.com',
  GAMING_URL: ['https://wpl-stg-admin-coral.coral.co.uk', 'https://wpl-stg-public-coral.coral.co.uk'],
  VS_QUOLITY_MAP: {
    desktop: 'web',
    tablet: 'mobhi',
    mobile: 'moblo'
  },
  RACING_POST_API_ENDPOINT: 'https://sb-api-stg.coral.co.uk/v4/sportsbook-api',
  RACING_POST_API_KEY:'CD3DDB282FF116480AB3BB3113AAF316FE',
  BYB_CONFIG: {
    HR_YC_EVENT_TYPE_ID: 28337,
    uri: 'https://buildyourbet-stg0.coral.co.uk/api'
  },
  TOTE_CATEGORY_ID: '152',
  CASHOUT_MS: 'https://cashout-stg0.coralsports.nonprod.cloud.ladbrokescoral.com'
});
