import de0Config from './environment.dev0';

import * as merge from 'lodash.merge';

const prodConfig = JSON.parse(JSON.stringify(de0Config));

export default merge(prodConfig, {
  production: false,
  ENVIRONMENT: 'dev',
  SURFACE_BETS_URL: 'https://cms-dev2.coralsports.dev.cloud.ladbrokescoral.com/cms/api',
  CMS_ENDPOINT: 'https://cms-dev2.coral.co.uk/cms/api',
  CMS_ROOT_URI: 'https://cms-dev2.coral.co.uk/cms',
  CMS_LINK: 'https://cms-api-ui-dev2.coralsports.dev.cloud.ladbrokescoral.com',
  FEATURED_SPORTS: 'wss://featured-sports-dev2.coralsports.dev.cloud.ladbrokescoral.com',
  INPLAYMS: 'wss://inplay-publisher-dev2.coralsports.dev.cloud.ladbrokescoral.com',
  EDPMS: 'https://edp-dev1-coral.symphony-solutions.eu',
  BIG_COMPETITION_MS: 'https://bigcompetition-dev1.coralsports.dev.cloud.ladbrokescoral.com/competition',
  AEM_CONFIG: {
    server: 'https://banners-cms.coral.co.uk',
    at_property: '63909eaa-a987-2833-6631-85b5ced26e68',
    betslip_at_property: '0c727079-a884-88b9-1fd8-faa96c1a6fa7'
  },
  TIMEFORM_ENDPOINT: 'https://timeform-dev1.coralsports.dev.cloud.ladbrokescoral.com',
  CASHOUT_MS: 'https://cashout-dev1.coralsports.dev.cloud.ladbrokescoral.com'
});
