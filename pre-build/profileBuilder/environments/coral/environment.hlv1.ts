import * as merge from 'lodash.merge';
import productionConfig from './environment.production';
const prodConfig = JSON.parse(JSON.stringify(productionConfig));
export default merge(prodConfig, {
  googleTagManagerID: ['GTM-KW37JJ9'],
  production: true,
  ENVIRONMENT: 'hiddenprod',
  SITESERVER_BASE_URL: 'https://ss-aka-ori-gib.coral.co.uk/openbet-ssviewer',
  SITESERVER_DNS: 'https://ss-aka-ori-gib.coral.co.uk',
  SITESERVER_ENDPOINT: 'https://ss-aka-ori-gib.coral.co.uk/openbet-ssviewer/Drilldown/2.31',
  SITESERVER_COMMON_ENDPOINT: 'https://ss-aka-ori-gib.coral.co.uk/openbet-ssviewer/Common/2.31',
  SITESERVER_LOTTERY_ENDPOINT: 'https://ss-aka-ori-gib.coral.co.uk/openbet-ssviewer/Lottery/2.31',
  SITESERVER_HISTORIC_ENDPOINT:
    'https://ss-aka-ori-gib.coral.co.uk/openbet-ssviewer/HistoricDrilldown/2.31',
  SITESERVER_COMMENTARY_ENDPOINT: 'https://ss-aka-ori-gib.coral.co.uk/openbet-ssviewer/Commentary/2.31',
  PIROZHOK_API_ENDPOINT: 'https://bp-gib.coral.co.uk/Proxy',
  BP_URI: 'https://bp-gib.coral.co.uk',
  LIVESERV: {
    DOMAIN: 'coral.co.uk',
    PUSH_COMMON_SUBDOMAIN: 'coral.co.uk',
    PUSH_LOCATION_HANDLER: '',
    PUSH_URL: 'https://push-gib.coral.co.uk',
    PUSH_INSTANCE_URL: 'push-gib.coral.co.uk',
    PUSH_INSTANCE_PORT: '443',
    PUSH_INSTANCE_TYPE: 'AJAX',
    PUSH_IFRAME_ID: 'push_iframe',
    PUSH_VERSION: 'e8382d8b7866e9a11d3529d8fe438008'
  },
  IMAGES_RACE_ENDPOINT: 'https://aggregation-hlv1.coral.co.uk/silks/racingpost',
  RACING_POST_API_ENDPOINT: 'https://sb-api-stg.coral.co.uk/v4/sportsbook-api',
  RACING_POST_API_KEY: 'CDd2396372409341029e905faba6117138',
  SURFACE_BETS_URL: 'https://surface-bets-hlv1.coral.co.uk/cms/api',
  CMS_ENDPOINT: 'https://cms-hlv1.coral.co.uk/cms/api',
  CMS_ROOT_URI: 'https://cms-hlv1.coral.co.uk/cms/',
  CMS_LINK: 'https://cms-hlv1.coral.co.uk/',
  OPT_IN_ENDPOINT: 'https://optin-hlv1.coralsports.nonprod.cloud.ladbrokescoral.com',
  IG_MEDIA_TOTE_ENDPOINT: 'https://optin-hlv1.coralsports.nonprod.cloud.ladbrokescoral.com/api/igamemedia',
  IG_MEDIA_ENDPOINT: 'https://optin-hlv1.coralsports.nonprod.cloud.ladbrokescoral.com/api/video/igame',
  FEATURED_SPORTS: 'wss://featured-sports-hlv1.coralsports.nonprod.cloud.ladbrokescoral.com',
  INPLAYMS: 'wss://inplay-publisher-hlv1.coralsports.nonprod.cloud.ladbrokescoral.com',
  LIVESERVEMS: 'wss://liveserve-publisher-hlv1.coralsports.nonprod.cloud.ladbrokescoral.com',
  REMOTEBETSLIPMS: 'wss://remotebetslip-hlv1.coralsports.nonprod.cloud.ladbrokescoral.com',
  EDPMS: 'https://edp-hlv1.coralsports.nonprod.cloud.ladbrokescoral.com',
  BIG_COMPETITION_MS: 'https://bigcompetition-hlv1.coralsports.nonprod.cloud.ladbrokescoral.com/competition',
  CASHOUT_MS: 'https://cashout-hlv1.coralsports.nonprod.cloud.ladbrokescoral.com',
  NOTIFICATION_CENTER_ENDPOINT:
    'https://notification-center-hlv1.coralsports.nonprod.cloud.ladbrokescoral.com',
  VISUALIZATION_ENDPOINT: 'https://vis-stg2-coral.symphony-solutions.eu',
  VISUALIZATION_TENNIS_ENDPOINT: 'https://coral-vis-tennis-stg2.symphony-solutions.eu',
  VISUALIZATION_IFRAME_URL: 'https://vis-static-stg2.coral.co.uk',
  VISUALIZATION_PREMATCH_URL: 'https://vis-static-stg2.coral.co.uk/football/pre-match.html',
  STATS_CENTRE_ENDPOINT: 'https://statscenter-hlv1.coralsports.nonprod.cloud.ladbrokescoral.com/api',
  AEM_CONFIG: {
    server: 'https://banners-cms-stg.coral.co.uk',
    at_property: '63909eaa-a987-2833-6631-85b5ced26e68',
    betslip_at_property: '0c727079-a884-88b9-1fd8-faa96c1a6fa7'
  },
  GAMING_URL: ['https://beta-www.coral.co.uk/en/games'],
  BYB_CONFIG: {
    HR_YC_EVENT_TYPE_ID: 34799,
    uri: 'https://buildyourbet-stg0.coral.co.uk/api'
  },
  OPTA_SCOREBOARD: {
    CDN: 'https://opta-scoreboards-hl.coral.co.uk',
    ENV: 'stage'
  },
  BET_FINDER_ENDPOINT: 'https://api.racemodlr.com/cypher/coralDublin/0/',
  BET_FILTER_ENDPOINT: 'https://modules-stg.coral.co.uk/coupon-buddy/oxygen/',
  QUESTION_ENGINE_ENDPOINT: 'https://question-engine-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com/api/v1'
});
