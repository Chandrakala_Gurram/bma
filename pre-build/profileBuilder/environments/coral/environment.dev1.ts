import de0Config from './environment.dev0';

import * as merge from 'lodash.merge';

const prodConfig = JSON.parse(JSON.stringify(de0Config));

export default merge(prodConfig, {
  production: false,
  ENVIRONMENT: 'dev',
  googleTagManagerID: 'GTM-W6NBMJL',
  EVENT_CATEGORIES_WITH_WATCH_RULES: ['21', '19', '161'],
  PIROZHOK_API_ENDPOINT: 'https://bpp-dev1.coralsports.dev.cloud.ladbrokescoral.com/Proxy',
  IMAGES_RACE_ENDPOINT: 'https://aggregation-ms-dev.symphony-solutions.eu/racingpost',
  SURFACE_BETS_URL: 'https://cms-dev1.coralsports.dev.cloud.ladbrokescoral.com/cms/api',
  CMS_ENDPOINT: 'https://cms-dev1.coral.co.uk/cms/api',
  CMS_ROOT_URI: 'https://cms-dev1.coral.co.uk/cms',
  CMS_LINK: 'https://cms-api-ui-dev1.coralsports.dev.cloud.ladbrokescoral.com',
  NOTIFICATION_CENTER_ENDPOINT: 'https://notification-center-dev1.coralsports.dev.cloud.ladbrokescoral.com',
  FEATURED_SPORTS: 'wss://featured-sports-dev1.coralsports.dev.cloud.ladbrokescoral.com',
  INPLAYMS: 'wss://inplay-publisher-dev1.coralsports.dev.cloud.ladbrokescoral.com',
  LIVESERVEMS: 'wss://liveserve-publisher-dev1.coralsports.dev.cloud.ladbrokescoral.com',
  REMOTEBETSLIPMS: 'wss://remotebetslip-dev1.coralsports.dev.cloud.ladbrokescoral.com',
  EDPMS: 'https://edp-dev1.coralsports.dev.cloud.ladbrokescoral.com',
  BIG_COMPETITION_MS: 'https://bigcompetition-dev1.coralsports.dev.cloud.ladbrokescoral.com/competition',
  AEM_CONFIG: {
    server: 'https://35.178.99.134:5433',
    at_property: '63909eaa-a987-2833-6631-85b5ced26e68',
    betslip_at_property: '0c727079-a884-88b9-1fd8-faa96c1a6fa7'
  },
  TIMEFORM_ENDPOINT: 'https://timeform-dev1.coralsports.dev.cloud.ladbrokescoral.com',
  BYB_CONFIG: {
    HR_YC_EVENT_TYPE_ID: 15031,
    uri: 'https://buildyourbet-dev1.coralsports.dev.cloud.ladbrokescoral.com/api'
  },
  CASHOUT_MS: 'https://cashout-dev1.coralsports.dev.cloud.ladbrokescoral.com'
});
