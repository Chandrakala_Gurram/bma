import productionConfig from './environment.production';
import * as merge from 'lodash.merge';

const prodConfig = JSON.parse(JSON.stringify(productionConfig));

export default merge(prodConfig, {
  googleTagManagerID: ['GTM-KW37JJ9'],
  production: true,
  ENVIRONMENT: 'hiddenprod',
  PIROZHOK_API_ENDPOINT: 'https://bp-hl.coral.co.uk/Proxy',
  INPLAYMS_ENDPOINT: 'https://bp-stg-coral.symphony-solutions.eu:444',
  SURFACE_BETS_URL: 'https://surface-bets-hlv0.coral.co.uk/cms/api',
  IMAGES_RACE_ENDPOINT: 'https://aggregation-hlv0.coral.co.uk/silks/racingpost',
  CMS_ENDPOINT: 'https://cms-hl.coral.co.uk/cms/api',
  CMS_ROOT_URI: 'https://cms-hl.coral.co.uk/cms',
  CMS_LINK: 'https://cms-hl.coral.co.uk/cms',
  QUANTUMLEAP_SCRIPT_ENDPOINT: 'https://www.racemodlr.com/coral-stage/live_sim/mobile/',
  OPT_IN_ENDPOINT: 'https://optin-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com',
  IG_MEDIA_TOTE_ENDPOINT: 'https://optin-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com/api/igamemedia',
  IG_MEDIA_ENDPOINT: 'https://optin-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com/api/video/igame',
  ONE_TWO_FREE_ENDPOINT: 'https://otf-hlv0.coral.co.uk/',
  FEATURED_SPORTS: 'wss://featured-sports-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com',
  INPLAYMS: 'wss://inplay-publisher-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com',
  LIVESERVEMS: 'wss://liveserve-publisher-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com',
  REMOTEBETSLIPMS: 'wss://remotebetslip-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com',
  EDPMS: 'https://edp.stress.coral.co.uk',
  BIG_COMPETITION_MS: 'https://bigcompetition.stress.coral.co.uk/competition',
  TIME_ENDPOINT: 'https://hydra.stress.coral.co.uk',
  TIMEFORM_ENDPOINT: 'https://timeform-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com',
  NOTIFICATION_CENTER_ENDPOINT: 'https://notification-center-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com',
  BYB_CONFIG: {
    uri: 'https://buildyourbet.stress.coral.co.uk/api'
  },
  GAMING_URL: ['https://beta-www.coral.co.uk/en/games'],
  AEM_CONFIG: {
    server: 'https://banners-cms.coral.co.uk',
    at_property: 'b637a0ef-3583-855d-e895-29b3eb3894e3',
    betslip_at_property: 'b7d701f3-01c8-23b7-2b1f-48b4423f6c8b'
  },
  CASHOUT_MS: 'https://cashout-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com',
  OPTA_SCOREBOARD: {
    CDN: 'https://opta-scoreboards-hl.coral.co.uk',
    ENV: 'prod'
  },
  QUESTION_ENGINE_ENDPOINT: 'https://question-engine-hlv0.coralsports.nonprod.cloud.ladbrokescoral.com/api'
});
